import paho.mqtt.client as mqtt
from datetime import datetime
from TestActions import runChatInterventionTest,runBlockInterventionTest,runMapInterventionTest

import os, math, json, sys, logging, time, random


class MqttManager():

    # Check runtime args
    # runtimeArgs = sys.argv[1:]
    # if len(runtimeArgs):
    #     if ('-v' in runtimeArgs) or ('--verbose' in runtimeArgs):
    #         logLevel = logging.DEBUG
    #     elif ('-p' in runtimeArgs) or ('--production' in runtimeArgs):
    #         logLevel = logging.WARNING
    #     else:
    #         logLevel = logging.INFO
    # else:
    #     logLevel = logging.INFO

    # # Setup basic handler for logging, sending everything to stderr
    # logging.basicConfig(format = '%(asctime)s :: %(module)s :: %(levelname)s Log: %(message)s',
    #                     level  = logLevel,
    #                     stream = sys.stderr)
    

    # log = logging.getLogger(__name__)

    # log.debug("***** Logging level is: {0} *****".format(logLevel))

    connectionCount = 0

    
    def __init__(self,mqttHost:str,mqttPort:int):
      
        print('Initializing Mqtt Client Connection')
        
        self.client = mqtt.Client(client_id='TEST')
        self.client.is_connected = False
        self.client.on_connect = self.on_connect
        self.client.on_disconnect = self.on_disconnect
        self.client.on_message = self.on_message
        
        try:                      
            print( 'Connecting to '+ mqttHost + ':' + str(mqttPort) + '.')
            self.client.connect(mqttHost, mqttPort, 60) 
            self.client.loop_start()             
            
        except Exception as e:
            print(e)    
         

    def on_connect(self, client, userdata, flags, rc):    
        
        print('Connected with result code ' + str(rc))
        print('Mqtt Looping forever on it\'s own thread') 

        client.subscribe('agent/intervention/test/+')       
        
        client.is_connected = True

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self,client, userdata, msg):
        
        print("Message received on: " + msg.topic + ". Attempting to process.")           

        # payload = json.loads(msg.payload.decode('utf-8'))
        
        # topicBreakdown = msg.topic.split('/')    

        if(msg.topic == 'agent/intervention/test/chat_test'):

            runChatInterventionTest(client)

        elif(msg.topic == 'agent/intervention/test/block_test'):

            runBlockInterventionTest(client)

        elif(msg.topic == 'agent/intervention/test/map_test'):

            runMapInterventionTest(client)    

      

    def on_disconnect(self, client, userdata, msg):    
        
        print("DISCONNECTED FROM BROKER")
        
        client.connected_flag=False  

 
    
    
        
                  

        
            


