# TestAgent Instructions

## Building Docker container
- All containers can be built by running either the testbed_build.sh, testbed_build.cmd, or testbed_build.ps1 scripts from the Local directory. Choose whichever is suitable for you operating system and environment. 

- If you wish to build the container by itself follow the steps below:

### Building the Docker Container By Itself
- Navigate to the root (top level) of the TestAgent directory, this is the same level that the dockerfile appears in
- Open a shell window and run *docker build -t asist:test_agent .*

## Updating Docker Container After a New Release --> Building From Source
- Navigate to the root (top level) of the TestAgent directory, this is the same level that the dockerfile appears in
- Open a shell window in this directory and run *docker build -t asist:test-agent --build-arg CACHE_BREAKER=somestring .*
- You can replace the word "somestring" with any string of your choice, as long as it is unique each time
- This build is significantly faster, as the majority of the image layers are already built, and only new layers will be updated

## Configuration

- The Test Agent can be run in one of two configurations, as either a standalone python system, or inside of docker as a container

### Standalone

- To run the TestAgent as a stand alone system, navigate to ReferenceAgents/TestAgent/src. Here you will see the Initialize.py file.
- In this directory run the command *pip install -r .\requirements.txt* to install the basic requirements for the system
- The syntax for running the Test Agent is as follows : *python Initialize.py <mqtthost> <mqttport> <triggered-by-mqtt>*
- Start the TestAgent in 1 of 2 modes : 
    
    1. Listening for an mqtt message to trigger the tests with argument 3 (triggered-by-mqtt) set to 1 --> *python Initialize.py localhost 1883 1*
    2. Sending all intervention on program initialization with argument 3 (triggered-by-mqtt) set to 0 --> *python Initialize.py localhost 1883 0*

### Docker

- To run the TestAgent in Docker, make sure your agent has been successfully built, and insure the service is uncommented in Local/docker-compose.asistmod.yml.
- You may set the mqtthost and mqttbroker in the environment variable section of the service in Local/docker-compose.asistmod.yml.
- The TestAgent container will be brought up along with all the other services associated with the Local/docker-compose.asistmod.yml file.

### JSON File

- The TestAgent pulls it's test intervention data from the Test_Interventions.json file. This file has 3 top-level keys: "chat","block",and "map".
- Each top-level key represents a json array holding an arbitrary number of interventions associated with each of the types mentioned.
- You may change this file during the TestAgent's operation, and any subsequent calls to the system will show these changes - the agent does not need to be restarted for  changes to take affect.

### Triggering tests in Triggered-By-MQTT mode

- If the agent is set to "Triggered-By-MQTT" mode, or mode 1, you can trigger intervention events by sending ANY (including a blank) mqtt message to the following topics
- CHAT ---> "agent/intervention/test/chat_test"
- BLOCK --> "agent/intervention/test/block_test"
- MAP --> agent/intervention/test/map_test