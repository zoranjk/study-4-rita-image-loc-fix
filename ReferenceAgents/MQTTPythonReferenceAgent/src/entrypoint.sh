#!/bin/bash

# check if the agent crashed with an error exit code and restart
until python3 MQTTPythonReferenceAgent.py; do
     echo "Reference agent crashed with exit code $?. Restarting.." >&2
     sleep 1
done