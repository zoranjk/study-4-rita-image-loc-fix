package com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.CallSigns;
import com.asist.asistmod.MissionSpecific.MissionA.UtilityStructures.Position;
import com.asist.asistmod.datamodels.PlayerStateChange.PlayerStateChangeModel;
import com.asist.asistmod.item.ItemBombPPE;
import com.asist.asistmod.item._Items;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.potion.Potion;
import net.minecraft.potion.PotionEffect;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;


public class Player {
	
	enum affectedStates{
		NORMAL,
		FROZEN,
		SLOW,
		FAST
	}	
	

	public EntityPlayer player;
	public String participant_id;
	public String playerName;
	public String callSign = null;
	public int callSignCode = -1;
	public long disconnectedTimestampMilli = 0;
	
	public boolean armorIsEquipped = false;
	public boolean isFrozen = false;
	
	public String lastDamagedById = null;
	public String lastDamagedByType = null;
	public BlockPos lastDamagedByPosition = null;
	
	public String lastBombId = null;
	public String lastBombType = null;
	public BlockPos lastBombPosition = null;
	
	public String lastBeaconId = null;
	public String lastBeaconType = null;
	public BlockPos lastBeaconPosition = null;
	
	public ConcurrentHashMap<String,String> attributes = new ConcurrentHashMap<String,String>();
	
	public Player(EntityPlayer player, int callSignCode, String participant_id) {
		
		this.player = player;
		this.participant_id = participant_id;
		playerName = player.getName();
		this.callSign = CallSigns.masterCallSignArray[callSignCode];
		this.callSignCode =callSignCode;
		//System.out.println("Player created : " + this.playerName);
		//System.out.println("CallSign : " + this.callSign);
		//System.out.println("CallSignCode : " + this.callSignCode);
		//System.out.println("PID : " + this.participant_id);
		
		attributes.put("is_frozen", Boolean.toString(false));
		attributes.put("ppe_equipped",Boolean.toString(false));	
		attributes.put("health", "20.0");
		
	}
	
	
	
	public EntityPlayer getEntityPlayer() {
		return player;
	}
	

	
	public String getParticipantId() {
		return participant_id;
	}	

	
	public String getName() {
		return playerName;
	}

	public void teleport(Position pos, MinecraftServer server) {
		String tpCommand = "tp " + player.getName() + " " + pos.getString();
		server.commandManager.executeCommand(server, tpCommand);
		//System.out.println(tpCommand);
	}
	
	
	public String getCallSign() {
		return callSign;
	}
	
	public int getCallSignCode() {
		return callSignCode;
	}
	
	public void giveEffect(Potion effect) {
		player.clearActivePotions();
		PotionEffect potionEffect = new PotionEffect(effect, 100000, 1, true, false);		
		player.addPotionEffect(potionEffect);		
		player.getFoodStats().setFoodLevel(6);	
	}	
	
	public void removeEffects() {
		player.clearActivePotions();		
		player.getFoodStats().setFoodLevel(6);	
	}
	
	public void clearInventory() {
		player.inventory.clear();
	}
	
	public void giveItem(String item, MinecraftServer server) {
		this.player.inventory.clear();
		String command = "/replaceitem entity " + playerName + " slot.hotbar.0" + item;
		server.commandManager.executeCommand(server, command);
	}
	
	public void giveItems(String[] items) {
		
		// GET PREVIOUSLY EQUIPPED ITEMS
		//List<String> prev_equipped_items = attributes.get("equipped_items");
		
		int slotNum = 0;
		for(String item:items) {
			String command = "/replaceitem entity " + playerName + " slot.hotbar." + slotNum + item;
			AsistMod.server.commandManager.executeCommand(AsistMod.server, command);
			slotNum++;	
			//attributes.get("equipped_items").add(item);
		}
		
		// DO BOOKKEEPING FOR EQUIPMENT UPDATE MESSAGES HERE		
		
		 
	}



	// if armor given is PPE will remove and restore any previous armor sets however,
	// if armor being removed isn't PPE related it'll just remove that armor slot
	// TODO: Test
	public void removePlayerArmor()
	{
		EntityPlayer player = this.getEntityPlayer();
		EntityEquipmentSlot entityequipmentslot = EntityLiving.getSlotForItemStack(ItemType.BOMB_PPE.getItemStack());
		ItemStack itemstack1 = player.getItemStackFromSlot(entityequipmentslot);

		if(itemstack1.getItem() == _Items.PPE_HEAD ||
				itemstack1.getItem() == _Items.PPE_CHEST ||
				itemstack1.getItem() == _Items.PPE_LEGS ||
				itemstack1.getItem() == _Items.PPE_FEET ) {

			ItemBombPPE ppe = (ItemBombPPE) itemstack1.getItem();
			//If the armor is PPE related clear it out and don't save, because we're toggling off the PPE
			ppe.clearCurrentArmorSet(player, false);

			//If there is previous armor that is saved we want to restore it.
			if (ppe.previousArmorSaved) {
				//this function will go through each element; head, chest, legs, and feet.
				//it will then check to see if there is any stored armor and then restore it.
				//while setting the previous variable to null.
				ppe.restorePreviousArmorSet(player);
			}
		}
		else
		{
			itemstack1.setCount(0);
		}
	}
		
	public void givePlayerItemsViaJsonCommandString( String[] itemsCommandString) {
		
		// SET ITEMS
		//String[] items = {
				// PRODUCTION
				//wirecutterItemString(this.callSign.toString().toLowerCase()),
				// DEBUG
		//		wirecutterItemString("red"),
		//		wirecutterItemString("green"),
		//		wirecutterItemString("blue"),
		//		" asistmod:block_beacon_basic 1 0 "+canPlaceOn
		
		//};	
		
		giveItems(itemsCommandString);
	}	
	
	public void setArmorEquipped(boolean eq, String pid, BlockPos playerPos,String sourceId, String sourceType, 
			BlockPos sourcePos ) {
		
		this.armorIsEquipped = eq;
		
		Map<String,String> oldAttributes = new ConcurrentHashMap<String,String>(this.attributes);
		
		this.attributes.put("ppe_equipped", Boolean.toString(armorIsEquipped));
		
		Map<String,String[]>changedAttributes = AttributedObjectManager.returnAttributeDiff(oldAttributes, this.attributes);
		
		PlayerStateChangeModel model = new PlayerStateChangeModel(pid,playerPos,sourceId,sourceType,sourcePos,changedAttributes,this.attributes);
		
		InternalMqttClient.publish(model.toJsonString(), model.getTopic());
		
		//System.out.println(participant_id + " armored : " + this.armorIsEquipped);
	}
	
	public void setPlayerFrozen(boolean eq, String pid, BlockPos playerPos,String sourceId, String sourceType, 
			BlockPos sourcePos  ) {
		
		this.isFrozen = eq;
		
		Map<String,String> oldAttributes = new ConcurrentHashMap<String,String>(this.attributes);
		
		this.attributes.put("is_frozen", Boolean.toString( isFrozen));
		
		Map<String,String[]>changedAttributes = AttributedObjectManager.returnAttributeDiff(oldAttributes, this.attributes);
		
		PlayerStateChangeModel model = new PlayerStateChangeModel(pid,playerPos,sourceId,sourceType,sourcePos,changedAttributes,this.attributes);
		
		InternalMqttClient.publish(model.toJsonString(), model.getTopic());
		
		//System.out.println(participant_id + " frozen : " + this.isFrozen);
	}
	
	public void giveArmor() {
		
		int color = 0;
		String lowerCase = callSign.toLowerCase();
		
		if ( lowerCase.contentEquals("red") ) {
			color = 11546150;
		}
		else if ( lowerCase.contentEquals("green") ) {
			color = 6192150;
		}
		else if( lowerCase.contentEquals("blue") ) {
			color = 3949738;
		}
		//11546150, ItemType.MEDICALKIT),
		//SS		(2, "search", 6192150, ItemType.STRETCHER),
		//HS		(3, "hammer", 3949738, ItemType.HAMMER),
		//int color = this.playerRole.getColor();
		String[] armors = new String[] 
				{	" slot.armor.head minecraft:leather_helmet 1 0 {display:{color:" + color + "}}",
					" slot.armor.chest minecraft:leather_chestplate 1 0 {display:{color:" + color + "}}",
					" slot.armor.legs minecraft:leather_leggings 1 0 {display:{color:" + color + "}}",
					" slot.armor.feet minecraft:leather_boots 1 0 {display:{color:" + color + "}}" };
		
		for(String armor:armors) {
			String command = "/replaceitem entity " + this.playerName + armor;
			AsistMod.server.commandManager.executeCommand(AsistMod.server, command);
		}
	}
	
	public String printPlayerSummary() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.callSign);
		sb.append("\n");
		sb.append(this.callSignCode);
		sb.append("\n");
		sb.append(this.participant_id);
		sb.append("\n");
		sb.append(this.playerName);
		sb.append("\n");
		return sb.toString();
	}

}
