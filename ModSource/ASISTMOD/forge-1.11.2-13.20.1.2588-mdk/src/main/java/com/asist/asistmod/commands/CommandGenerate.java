package com.asist.asistmod.commands;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

public class CommandGenerate extends CommandBase {
		
	MinecraftServer server;

	public CommandGenerate() {		
	
	}
		
	@Override
	public String getName() {
		return "generate";
	}

	@Override
    /**
     * Return the required permission level for this command.
     */
    public int getRequiredPermissionLevel()
    {
        return 2;
    }
	
	@Override
	public String getUsage(ICommandSender sender) {
		return "generate";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		  if (args.length < 1)	{
	            throw new WrongUsageException("/generate bombs", new Object[0]);
	      }
	      else {
	    	
	    	String input = args[0]; //args does not include "generate"	    	  
	    	
			switch(input) {
			
			case "bombs" :
				char[] players = {'R', 'G', 'B'};
				//Clears out the bombs. Temp solution
				server.commandManager.executeCommand(server, "/fill 0 52 0 50 55 150 minecraft:air 0 replace asistmod:block_bomb_neutral");
				server.commandManager.executeCommand(server, "/fill 0 55 0 50 58 150 minecraft:air 0 replace asistmod:block_bomb_neutral");
				//Spawn bombs
				//BombManager.populateBombs(5, 5, 5, players);
				break;
			
			default:
				//System.out.println("Can only generate bombs.");
				server.commandManager.executeCommand(server, "/tell @a Can only generate bombs.");
			break;
			}		  	
		}
	}
	
	/**
     * Get a list of options for when the user presses the TAB key
     *  
     * @param server The server instance
     * @param sender The ICommandSender to get tab completions for
     * @param args Any arguments that were present when TAB was pressed
     * @param targetPos The block that the player's mouse is over, <tt>null</tt> if the mouse is not over a block
     */
    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos)
    {
        return args.length > 0 && args.length <= 1 ? getListOfStringsMatchingLastWord(args, new String[] {"victims", "rubble", "map", "gasleaks"}) : ( args.length > 1 ? getListOfStringsMatchingLastWord(args, new String[] {"m2_"}) : Collections.<String>emptyList());
    }
}