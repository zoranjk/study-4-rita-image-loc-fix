//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.asist.asistmod.item;

import com.asist.asistmod.MissionSpecific.MissionA.FireManagement.FireManager;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs.ASISTModTabs;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.block._Blocks;

import com.asist.asistmod.block.perturbation.BlockFireCustom;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.*;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class ItemFlintSteel extends Item {

    
    public ItemFlintSteel() {
        this.maxStackSize = 1;
        this.setMaxDamage(64);
        this.setCreativeTab(ASISTModTabs.ASISTItems);
    }

    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float p_onItemUse_6_, float p_onItemUse_7_, float p_onItemUse_8_) {

        pos = pos.offset(facing);
        ItemStack itemstack = player.getHeldItem(hand);
        IBlockState firBlockState = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("asistmod:block_fire_custom")).getDefaultState();

        if (!player.canPlayerEdit(pos, facing, itemstack))
        {
            return EnumActionResult.FAIL;
        }
        else
        {
            if (world.isAirBlock(pos))
            {
                world.playSound(player, pos, SoundEvents.ITEM_FLINTANDSTEEL_USE, SoundCategory.BLOCKS, 1.0F, itemRand.nextFloat() * 0.4F + 0.8F);
                world.setBlockState(pos, firBlockState, 11);
            }

            itemstack.damageItem(1, player);
            return EnumActionResult.SUCCESS;
        }

    }
}
