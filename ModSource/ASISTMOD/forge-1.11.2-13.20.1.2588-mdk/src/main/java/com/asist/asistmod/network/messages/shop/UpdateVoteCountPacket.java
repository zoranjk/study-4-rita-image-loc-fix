package com.asist.asistmod.network.messages.shop;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class UpdateVoteCountPacket extends MessagePacket<UpdateVoteCountPacket> {
	
	int voteCount = 0;
	
	
	public UpdateVoteCountPacket() {
		
	}
	
	 /** Constructor for server-side
	 * @param displayShop  */
	public UpdateVoteCountPacket(int voteCount) {
		this.voteCount = voteCount;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		voteCount = buf.readInt();
		
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		
		buf.writeInt(voteCount);
		
		
	}

	@Override
	public void handleClientSide(UpdateVoteCountPacket message, EntityPlayer player) {
		
		//System.out.println("Received Store Votes : " + message.voteCount);
		RenderOverlayEventHandler.storeVotes = message.voteCount;
	
	}

	@Override
	public void handleServerSide(UpdateVoteCountPacket message, EntityPlayer player) {
		
	}
}
