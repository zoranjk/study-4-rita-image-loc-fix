package com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay;

import com.asist.asistmod.MissionCommon.InterventionManagement.ChatInterventions.ChatInterventionManager;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;

import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager.MissionStage;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.AgentInterventionChatPacket;
import com.asist.asistmod.network.messages.AgentInterventionResponsePacket;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.util.ResourceLocation;

public class Version3MainGui extends Gui  {

	String responseKeyColor = "2AE311";
	String responseColor = "ffffff";

	Minecraft mc;
	ScaledResolution resolution;
	
	MissionStage stage = null;
	
	int width;	
	int height;	

	public int minute = 0;
	public int second = 0;
	public String callSign = "";
	public int bombsDefused = 0;
	public int bombsExploded = 0;
	public int bombsRemaining = 0;
	public int storeVotes = 0;
	public int teamScore = 0;
	public int teamBudget = 0;
	public AgentInterventionChatPacket currentIntervention = null;
	public String centerOfScreenMessage = null;
	
	
	int actualScaledWidth;
	int actualScaledHeight;			
	
	int widthScaledBackTo1;
	int heightScaledBackTo1;
	
	
	
	public Version3MainGui(Minecraft mc, MissionStage stage, String callSign, int minute, int second, int defused, 
			int exploded, int remaining, int storeVotes, int teamScore, int teamBudget, AgentInterventionChatPacket currentIntervention,
			String centerOfScreenMessage)
	
	{
		
		this.mc = mc;
		this.resolution = new ScaledResolution(mc);
		this.width = resolution.getScaledWidth();
		this.height = resolution.getScaledHeight();
		this.stage = stage;
		this.callSign = callSign;
		this.minute = minute;
		this.second = second;
		this.bombsDefused = defused;
		this.bombsExploded = exploded;
		this.bombsRemaining = remaining;
		this.storeVotes = storeVotes;
		this.teamScore = teamScore;
		this.teamBudget = teamBudget;
		this.currentIntervention = currentIntervention;
		this.centerOfScreenMessage = centerOfScreenMessage;
		
		//draws the screen once per frame update with a new instance of this class
		draw();
		
	}
	
	// This method is important for scaling TEXT only NOT Rectangles
	public float setScaleFactorHeight( float trueScaleFactor) {
		
		int defaultFontHeight = mc.fontRendererObj.FONT_HEIGHT;
		int windowHeight = mc.displayHeight;
		
		int innateScaleFactor = this.resolution.getScaleFactor();		
		
		// Set innate scaleFactor to 1 to undo any autoscaling
		float resetScaleFactor = (1.0F/innateScaleFactor);
		
		float factorForFontToMatchHeight =  windowHeight/defaultFontHeight;
		
		// 1 character is the same height as the screen
		float scaleFactorFor1to1=resetScaleFactor*factorForFontToMatchHeight;
		
		float scaleFactor = scaleFactorFor1to1*trueScaleFactor;
		/*
		if( ((float)mc.displayHeight/mc.displayWidth)<.75f ) {
			
			return resetScaleFactor*3;
			
		}
		else if( ((float)mc.displayHeight/mc.displayWidth)>1.25f ){
			
			return resetScaleFactor*3;
		}
		*/
		return scaleFactor;
	}
	
	// This method is important for scaling TEXT only NOT Rectangles
		public float setScaleFactorWidth( float trueScaleFactor) {			
			
			int defaultFontHeight = mc.fontRendererObj.FONT_HEIGHT;
			int windowWidth = mc.displayWidth;
			
			int innateScaleFactor = this.resolution.getScaleFactor();	
			
			float resetScaleFactor = (1.0F/innateScaleFactor);
			
			float factorForFontToMatchHeight =  windowWidth/defaultFontHeight;
			
			// 1 character is the same height as the screen
			float scaleFactorFor1to1=resetScaleFactor*factorForFontToMatchHeight;
			
			float scaleFactor = scaleFactorFor1to1*trueScaleFactor;
			/*
			if( ((float)mc.displayHeight/mc.displayWidth)<.75f ) {
				
				return resetScaleFactor*3;
				
			}
			else if( ((float)mc.displayHeight/mc.displayWidth)>1.25f ){
				
				return resetScaleFactor*3;
			}
			*/
			return scaleFactor;
			
		}
	
	
	public void draw() {
		
		
		try {
			
			if(mc!=null) {				
				GLContext.useContext(mc);
				
				//int sf = this.resolution.getScaleFactor();
				
				//System.out.println( "ScaleFactor :" + sf);
				//System.out.println( mc.fontRendererObj.FONT_HEIGHT);
				//System.out.println( "ScaledWidth/ScaledHeight/Ratio :" +this.resolution.getScaledWidth()+"/"+this.resolution.getScaledHeight()+":"+((float)this.resolution.getScaledWidth()/(float)this.resolution.getScaledHeight()));
				//System.out.println( "Width/Height/Ratio :" +this.width/sf+"/"+this.height/sf+":"+((float)(this.width/sf)/((float)this.height/sf)));
				//float scaleFactor = setScaleFactor(2f);
				float scaleWidth = setScaleFactorWidth(.017f);
				float scaleHeight = setScaleFactorHeight(.017f);
				GlStateManager.pushMatrix();
				// This is a transformation matrix, so all referenced width and height values within the pushMatrix and popMatrix commands can be considered scaled to the
				// degree below. You cannot get this scaled value by accessing the width and height variable, as it seems they are transformed AFTER being passed to drawString
				// hence the "scaledBackTo1 variables -> which are the UNSCALED coordinates of where to place SCALED strings ... confusing I know
				
				GlStateManager.scale(scaleWidth, scaleHeight, 1);
				
				
				// Store Votes
				this.drawAtScaledPosition("Store Votes : ", 0.78f, 0.10f, "2AE311",scaleWidth,scaleHeight);
				this.drawAtScaledPosition(Integer.toString(this.storeVotes), 0.94f, 0.10f, "FFFFFF",scaleWidth,scaleHeight);
				
				// Callsign
				this.drawAtScaledPosition("Callsign : ", 0.78f, 0.15f, "2AE311",scaleWidth,scaleHeight);
				this.drawAtScaledPosition(this.callSign, 0.94f, 0.15f, "FFFFFF",scaleWidth,scaleHeight);
				
				// TIMER 			
				this.drawAtScaledPosition("Mission Timer : ", 0.78f, 0.20f, "2AE311",scaleWidth,scaleHeight);
				this.drawAtScaledPosition(getTimeString(), 0.94f, 0.20f, "FFFFFF",scaleWidth,scaleHeight);
				
				// Bombs Defused
				this.drawAtScaledPosition("Defused : ", 0.78f, 0.25f, "2AE311",scaleWidth,scaleHeight);
				this.drawAtScaledPosition(Integer.toString(bombsDefused), 0.94f, 0.25f, "FFFFFF",scaleWidth,scaleHeight);
							
				// Bombs Exploded			
				this.drawAtScaledPosition("Exploded : ", 0.78f, 0.30f, "2AE311",scaleWidth,scaleHeight);
				this.drawAtScaledPosition(Integer.toString(bombsExploded), 0.94f, 0.30f, "FFFFFF",scaleWidth,scaleHeight);	
				
				// Bombs Remaining			
				this.drawAtScaledPosition("Remaining : ", 0.78f, 0.35f, "2AE311",scaleWidth,scaleHeight);
				this.drawAtScaledPosition(Integer.toString(bombsRemaining), 0.94f, 0.35f, "FFFFFF",scaleWidth,scaleHeight);			

				//TeamScore
				this.drawAtScaledPosition("Team Score : ", 0.78f, 0.40f, "2AE311",scaleWidth,scaleHeight);
				this.drawAtScaledPosition(Integer.toString(teamScore), 0.94f, 0.40f, "FFFFFF",scaleWidth,scaleHeight);
				
				//TeamScore
				this.drawAtScaledPosition("Team Budget : ", 0.78f, 0.45f, "2AE311",scaleWidth,scaleHeight);
				this.drawAtScaledPosition(Integer.toString(teamBudget), 0.94f, 0.45f, "FFFFFF",scaleWidth,scaleHeight);
				GlStateManager.popMatrix();
				
				scaleWidth = setScaleFactorWidth(.02f);
				scaleHeight = setScaleFactorHeight(.02f);
				
				if ( stage == null) {
					
					this.drawRectAtScaledPosition(0.00f, 0.47f, 1f, 0.99f, 0xBB666666, 1f);
					this.drawRectAtScaledPosition(0.01f, 0.48f, 0.99f, 0.98f, 0xBB241A16, 1f);
					
									
					GlStateManager.pushMatrix();
					GlStateManager.scale(scaleWidth,scaleHeight, 1);					
					this.drawAtScaledPosition("Welcome To The SocialAI.Games Bomb Disposal Experiment!", 0.20f, 0.50f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("You are about to enter the \"Recon Phase\" of the mission.", 0.03f, 0.56f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("Look around to see your fellow teammates as they log in.", 0.03f, 0.59f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("During Recon, you will scout the map and place Bomb and Hazard Beacons in the field. ", 0.03f, 0.65f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("Bomb Beacons are used by right clicking on bombs when equipped.", 0.03f, 0.68f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("Hazard Beacons are used by right clicking on the ground when equipped.", 0.03f, 0.71f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("Once a Hazard Beacon is placed, select a Communication Option to broadcast it.", 0.03f, 0.74f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("You can press the \"ESC\" key to use the ClientMap on the left of your screen.", 0.03f, 0.80f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("This webpage will show your Minecraft position and the beacons your team has placed.", 0.03f, 0.83f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("Additionally, the ClientMap is where you will plan and purchase tools.", 0.03f, 0.86f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("Remember, Minecraft Chat is disabled for this experiment! Use the provided chat", 0.03f, 0.89f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("widgets in the ClientMap \"Planning\" and \"Shop\" tabs to chat with your team.", 0.03f, 0.92f, "FFFFFF", scaleWidth,scaleHeight);
					this.drawAtScaledPosition("Recon will begin 30 seconds after all players have logged into Minecraft.", 0.03f, 0.95f, "FFFFFF", scaleWidth,scaleHeight);
					GlStateManager.popMatrix();	
				}
				else {	
					
					if( stage.compareTo( MissionStage.RECON_STAGE)==0 ) {
						// TIMER 			
						//this.drawAtScaledPosition("Recon Timer : ", 0.78f, 0.55f, "2AE311",scaleFactor);
						//this.drawAtScaledPosition(getTimeString(), 0.94f, 0.55f, "FFFFFF",scaleFactor);							
						
					}
					else if( stage.compareTo( MissionStage.SHOP_STAGE)==0 ) {										
						/*
						GlStateManager.pushMatrix();	
						GlStateManager.scale(scaleWidth,scaleHeight, 1);
						this.drawAtScaledPosition("This is the \"Shop Phase\" of the mission and your player is frozen in place.", 0.02f, 0.35f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Press the ESC key to use the Web-Based - ClientMap on the left of your screen. ", 0.02f, 0.40f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Keep an eye out for advice from your AI advisor. Answering the provided AI prompts will clear them.", 0.02f, 0.45f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("This is the time to plan and purchase tools. Use the Planning and Shop tabs to develop a good strategy.", 0.02f, 0.5f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("The Chat widgets  in teh CleintMap are there for you to strategize with your team.", 0.02f, 0.55f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Use a combination of text chat and flag placement (Planning Tab) to develop a good plan.", 0.02f, 0.60f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("When you are done, press the \"Finish Planning and Shop Session\" button on the Shop tab.", 0.02f, 0.65f, "FFFFFF", scaleWidth,scaleHeight);
						GlStateManager.popMatrix();
						*/
						
						this.drawRectAtScaledPosition(0.00f, 0.47f, 1f, 0.92f, 0xBB666666, 1f);
						this.drawRectAtScaledPosition(0.01f, 0.48f, 0.99f, 0.91f, 0xBB241A16, 1f);
						
										
						GlStateManager.pushMatrix();
						GlStateManager.scale(scaleWidth,scaleHeight, 1);					
						this.drawAtScaledPosition("You are currently in the SHOP Phase of the Experiment!", 0.20f, 0.50f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Please press the \"ESC\" key to use the ClientMap on the left of your screen.", 0.03f, 0.56f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Use the ClientMap Planning and Shop tabs to plan and buy tools with your teammates.", 0.03f, 0.59f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("The ClientMap will show your Minecraft position and the various beacons your team ", 0.03f, 0.62f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("has placed while in the field. You can click or hover over the icons to get more info.", 0.03f, 0.65f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Keep an eye out for advice from your AI advisor.", 0.03f, 0.71f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Answering the provided AI prompts will clear them out and help train them.", 0.03f, 0.74f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("The Chat widgets in the ClientMap are there for you to strategize with your team.", 0.03f, 0.80f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Use a combination of chat and flag placement (Planning Tab) to develop a strategy.", 0.03f, 0.83f, "FFFFFF", scaleWidth,scaleHeight);
						this.drawAtScaledPosition("When finished, press the \"Finish Planning and Shop Session\" button in the Shop tab.", 0.03f, 0.89f, "FFFFFF", scaleWidth,scaleHeight);
						
						GlStateManager.popMatrix();	
					}
					else {
						
						GlStateManager.pushMatrix();
						GlStateManager.scale(scaleWidth,scaleHeight, 1);
						this.drawAtScaledPosition("Press the \"E\" key", 0.76f, 0.53f, "FDDA0D",scaleWidth,scaleHeight);
						this.drawAtScaledPosition("to vote to go to the", 0.76f, 0.56f, "FDDA0D",scaleWidth,scaleHeight);
						this.drawAtScaledPosition("shop!", 0.76f, 0.59f, "FDDA0D",scaleWidth,scaleHeight);
						GlStateManager.popMatrix();
					}
				
				}
						
				
				if(currentIntervention != null ) {
					
					
					GlStateManager.pushMatrix();
					GlStateManager.scale(scaleWidth,scaleHeight, 1);
					float messageChunkOffset = 0.03f;
					String[] message = currentIntervention.interventionContent.split("\\n");
					for(String messageChunk: message)
					{
						this.drawAtScaledPosition(messageChunk, 0.02f, messageChunkOffset, "FFFFFF",scaleWidth,scaleHeight);
						messageChunkOffset += 0.03f;
					}					
					
					this.drawAtScaledPosition("Use your keyboard to select a response:",
							0.02f, 0.1f + messageChunkOffset, responseColor,scaleWidth,scaleHeight);
					float optionsStartPosition = 0.15f + messageChunkOffset;

					if(currentIntervention.responseOptions == null || currentIntervention.responseOptions.length == 0)
					{
						this.drawAtScaledPosition("( O )", .02f, optionsStartPosition, responseKeyColor,scaleWidth,scaleHeight);
						this.drawAtScaledPosition("Ok", .08f, optionsStartPosition, responseColor,scaleWidth,scaleHeight);
					}
					else
					{
						String[] responseKeys = {"I","O","P"};
						float percent_from_left = .02f;
						for(int i=0; i<currentIntervention.responseOptions.length; i++) {
							if(i<3) {
								
								this.drawAtScaledPosition("( " +(responseKeys[i])+" ) ", percent_from_left, optionsStartPosition, responseKeyColor, scaleWidth,scaleHeight);
								String response = ChatInterventionManager.parseInterventionMessage(currentIntervention.responseOptions[i], 35);
								percent_from_left += .06f;
								String[] response_message = response.split("\\n");
								for(String messageChunk: response_message)
								{
									this.drawAtScaledPosition(messageChunk, percent_from_left, optionsStartPosition, "FFFFFF",scaleWidth,scaleHeight);
									optionsStartPosition += 0.03f;
								}
								optionsStartPosition += 0.03f;
								percent_from_left = .02f;
								
							}							
						}
					}
					
					GlStateManager.popMatrix();
					
				}
				
				if( this.centerOfScreenMessage != null && this.centerOfScreenMessage.length()>0 ) {
					
					
					GlStateManager.pushMatrix();
					GlStateManager.scale(scaleWidth,scaleHeight, 1);
					
					this.drawAtScaledPosition("GAME OVER!", 0.40f, 0.35f, "efeb00",scaleWidth,scaleHeight);
					this.drawAtScaledPosition(centerOfScreenMessage, 0.30f, 0.45f, "efeb00",scaleWidth,scaleHeight);
					
					GlStateManager.popMatrix();
					
				}
				
				
				GL11.glColor4f(1, 1, 1, 1);
				mc.renderEngine.bindTexture(new ResourceLocation("minecraft", "textures/gui/icons.png"));
				
				getKeyboardEvent();
			    
			}		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	public void drawAtPosition(String value, float percentFromLeft, float percentFromTop, String colorCode) {
		
		//drawString(mc.fontRendererObj, value, (int)( widthScaledBackTo1 - ( widthScaledBackTo1 * xFloatPercentFromRight ) ), (int)( heightScaledBackTo1 - (heightScaledBackTo1*yFloatPercentFromTop) ), Integer.parseInt(colorCode, 16));
		drawString(mc.fontRendererObj, value, (int)(width*percentFromLeft), (int)(height*percentFromTop), Integer.parseInt(colorCode, 16));
	}
	
	public void drawAtScaledPosition(String value, float percentFromLeft, float percentFromTop, String colorCode, float scaleFactorX,float scaleFactorY) {
		
		//drawString(mc.fontRendererObj, value, (int)( widthScaledBackTo1 - ( widthScaledBackTo1 * xFloatPercentFromRight ) ), (int)( heightScaledBackTo1 - (heightScaledBackTo1*yFloatPercentFromTop) ), Integer.parseInt(colorCode, 16));
		drawString(mc.fontRendererObj, value, (int)(width*percentFromLeft*(1/scaleFactorX)), (int)(height*percentFromTop*(1/scaleFactorY)), Integer.parseInt(colorCode, 16));
	}
	
	public void drawRectAtScaledPosition(float percentFromLeft, float percentFromTop,float percentFromRight, float percentFromBottom, int colorCode, float scaleFactor) {
		
		int left = (int)(width*percentFromLeft*(1/scaleFactor));
		int top = (int)(height*percentFromTop*(1/scaleFactor));
		int right = (int)(width*percentFromRight*(1/scaleFactor));
		int bottom = (int)(height*percentFromBottom*(1/scaleFactor));
		
		//System.out.println(left+","+top+","+right+","+bottom);
		
		Version3MainGui.drawRect(left,top, right, bottom, colorCode);
	}
	
	public String getTimeString() {
		
		StringBuilder sb = new StringBuilder("");
		
		// MINUTE
		if ( this.minute < 10) {			
			sb.append(Integer.toString(0));						
		}
		
		sb.append(Integer.toString(minute));
		sb.append(":");
		
		// SECOND
		if ( this.second < 10) {			
			sb.append(Integer.toString(0));						
		}
		
		sb.append(Integer.toString(second));
		
		
		return sb.toString();
	}
	
    public void getKeyboardEvent()
    {
    	if(currentIntervention != null) {
    		// from Keyboard Class
        	int I_CODE = 23;
        	int O_CODE = 24;
        	int P_CODE = 25;
        	
        	if( Keyboard.isKeyDown(I_CODE) ) {
        		//System.out.println("I Key pressed.");
        		NetworkHandler.sendToServer( new AgentInterventionResponsePacket(currentIntervention,0) );
        		RenderOverlayEventHandler.currentIntervention = null;
        		
        	}
        	else if ( Keyboard.isKeyDown(O_CODE) ) {
        		//System.out.println("O Key pressed.");
        		NetworkHandler.sendToServer( new AgentInterventionResponsePacket(currentIntervention,1) );
        		RenderOverlayEventHandler.currentIntervention = null;
        	}
        	else if ( Keyboard.isKeyDown(P_CODE) ) {
        		//System.out.println("P Key pressed.");
        		NetworkHandler.sendToServer( new AgentInterventionResponsePacket(currentIntervention,2) );
        		RenderOverlayEventHandler.currentIntervention = null;
        	}
    	}
    	
    }

	

			

}
