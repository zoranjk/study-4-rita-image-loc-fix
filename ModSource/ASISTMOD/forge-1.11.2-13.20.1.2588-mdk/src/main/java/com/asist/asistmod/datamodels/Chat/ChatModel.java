package com.asist.asistmod.datamodels.Chat;

import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.google.gson.Gson;

public class ChatModel {
	
	public HeaderModel header = new HeaderModel();
	public ChatMessageModel msg = new ChatMessageModel();
	public ChatDataModel data = new ChatDataModel();
	
	public String toJsonString() { 
		
		Gson gson = new Gson();
		return gson.toJson(this); 
		
	}

}
