package com.asist.asistmod.network.messages.shop;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class AssignCallSignPacket extends MessagePacket<AssignCallSignPacket> {
	
	String[] callSigns;
	int [] callSignLengths;
	int callSignCode;
	int numCallSigns;
	
	
	public AssignCallSignPacket() {
		
	}
	
	 /** Constructor for server-side
	 * @param displayShop  */
	public AssignCallSignPacket(int callSignCode,String[] callSigns) {
		this.callSigns = callSigns;
		this.callSignCode = callSignCode;
		this.callSignLengths = new int[callSigns.length];
		for(int i=0;i<callSigns.length;i++) {
			callSignLengths[i] = callSigns[i].length();
		}
		this.numCallSigns = callSigns.length;
		
		
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		
		callSignCode=buf.readInt();
		numCallSigns = buf.readInt();
		
		// lengths array
		int[] cslengths = new int[numCallSigns];		
		for( int i=0; i<cslengths.length; i++) {
			cslengths[i] = buf.readInt();
		}
		this.callSignLengths = cslengths;
		
		//callsigns array
		String[] cs = new String[numCallSigns];
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<cs.length;i++) {
			for(int j=0;j<cslengths[i];j++) {
				sb.append(buf.readChar());
			}
			cs[i]=sb.toString();
			sb.setLength(0);
		}
		this.callSigns = cs;
		
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(callSignCode);
		buf.writeInt(numCallSigns);
		for(int i=0;i<callSignLengths.length;i++) {			
			buf.writeInt(callSignLengths[i]);
		}
		for(int i=0;i<callSigns.length;i++) {
			for( int j=0; j<callSigns[i].length(); j++) {
				buf.writeChar(callSigns[i].charAt(j));
			}			
		}
	}

	@Override
	public void handleClientSide(AssignCallSignPacket message, EntityPlayer player) {		
		
		RenderOverlayEventHandler.callSign = message.callSigns[message.callSignCode];		
		
	}

	@Override
	public void handleServerSide(AssignCallSignPacket message, EntityPlayer player) {
		
	}
}
