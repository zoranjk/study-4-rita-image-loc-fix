package com.asist.asistmod.network.messages;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BombCountUpdatePacket  extends MessagePacket<BombCountUpdatePacket> {	
	
	 
	int bombsDefused=0;
	int bombsExploded=0;
	int bombsRemaining=0;

	String bomb_updated_id;

	double x =0;
	double y =0;
	double z =0;

	
	public BombCountUpdatePacket() {}
	
	public BombCountUpdatePacket(int defused, int exploded, int remaining) {
		
		
		this.bombsDefused = defused;
		this.bombsExploded = exploded;
		this.bombsRemaining = remaining;
	}

	public BombCountUpdatePacket(int defused, int exploded, int remaining, String bomb_id, double x, double y, double z) {


		this.bombsDefused = defused;
		this.bombsExploded = exploded;
		this.bombsRemaining = remaining;

		this.bomb_updated_id = bomb_id;
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	

	@Override
	public void fromBytes(ByteBuf buf) {
		// TODO Auto-generated method stub
		this.bombsDefused = buf.readInt();
		this.bombsExploded = buf.readInt();		
		this.bombsRemaining = buf.readInt();

		/*
		byte[] bytes = new byte[buf.readableBytes()];
		ByteBuf buf2 = buf.readBytes(bytes);


		this.bomb_updated_id = buf2.toString();

		this.x = buf.readDouble();
		this.y = buf.readDouble();
		this.z = buf.readDouble();

		 */

	}

	@Override
	public void toBytes(ByteBuf buf) {
		// TODO Auto-generated method stub
		buf.writeInt(this.bombsDefused);
		buf.writeInt(this.bombsExploded);
		buf.writeInt(this.bombsRemaining);

		/*
		buf.writeBytes(this.bomb_updated_id.getBytes());

		buf.writeDouble(x);
		buf.writeDouble(y);
		buf.writeDouble(z);

		 */

	}

	@Override
	@SideOnly(Side.CLIENT)
	public void handleClientSide(BombCountUpdatePacket message, EntityPlayer player) {
		// TODO Auto-generated method stub			
		
		RenderOverlayEventHandler.onScoreboardChange(message.bombsDefused,message.bombsExploded,message.bombsRemaining);


		
		
	}



	@Override
	@SideOnly(Side.SERVER)
	public void handleServerSide(BombCountUpdatePacket message, EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

}
