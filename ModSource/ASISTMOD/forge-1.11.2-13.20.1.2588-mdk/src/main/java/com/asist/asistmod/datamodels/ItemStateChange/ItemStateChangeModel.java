package com.asist.asistmod.datamodels.ItemStateChange;

import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;

public class ItemStateChangeModel {	
	
	
	public HeaderModel header = new HeaderModel();
		
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public ItemStateChangeData data = new ItemStateChangeData();
		
		
	public ItemStateChangeModel( String item_id, String item_type, String item_owner, Map<String,String[]> changedAttributes, Map<String,String> currAttributes) {
		
		data.item_id = item_id;
		data.item_name = item_type;
		data.owner = item_owner;
		data.changedAttributes = changedAttributes;
		data.currAttributes = currAttributes;
				
	}
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.ITEM_STATE_CHANGE; 
		
	}
	
	public String getTopic() { 		
		
		return getMessageTopicEnum().getTopic(); 
		
	}
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this);		
	}

}
