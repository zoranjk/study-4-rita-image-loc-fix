package com.asist.asistmod.network.messages.shop;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class TeamBudgetUpdatePacket extends MessagePacket<TeamBudgetUpdatePacket> {

	
	int budget = 0;

	
	public TeamBudgetUpdatePacket() {
		
	}	
	
	 /** Constructor for client
	 * @param coins
	 */
	public TeamBudgetUpdatePacket(int budget) {
		this.budget = budget;
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		

		
		this.budget = buf.readInt();
	}
	
	@Override
	public void toBytes(ByteBuf buf) {

		
		buf.writeInt(budget);
	}

	@Override
	public void handleClientSide(TeamBudgetUpdatePacket message, EntityPlayer player) {
		//System.out.println("ClientSide TEAM BUDGET PACKET: ");		
			//System.out.println("ClientSide TEAM BUDGET PACKET: ");
			RenderOverlayEventHandler.teamBudget = message.budget;
		
		
	}

	@Override
	public void handleServerSide(TeamBudgetUpdatePacket message, EntityPlayer player) {
		
	}
}
