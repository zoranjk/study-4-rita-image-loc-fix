package com.asist.asistmod.MissionSpecific.MissionA.ScoreboardManagement;


import java.util.HashMap;
import java.util.Map;

import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.Scoreboard.ScoreboardModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.TeamScoreUpdatePacket;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ScoreboardManager {

    public static Map<String, Integer> playerScores = new HashMap<String, Integer>();
    public static int teamScore = 0;

    public static int PlayerHelpedBombValue = 10;

    public static void updateAllPlayers() {
        PlayerManager.players.forEach(player -> {
            NetworkHandler.sendToClient(new TeamScoreUpdatePacket(teamScore), player.getEntityPlayer());
        });
    }
    
   
    //This will increase the designated player score based on id and the given value
    //and send MQTT
   
    @SideOnly(Side.SERVER)
    public static void decreasePlayerScore(String pid, int value)
    {
    	//System.out.println("Decreasing Score by " + value);
        if(playerScores.containsKey(pid))
        {
            playerScores.put(pid, playerScores.get(pid) - value);
        }
        else
        {
            playerScores.put(pid, 0-value);
        }
        
        decreaseTeamScore(value);
        updateAllPlayers();
        sendMQTT();
        
    }


    @SideOnly(Side.SERVER)
    private static void decreaseTeamScore(int value)
    {
        teamScore -= value;       
      
    }


    //This will increase the player score given id and value will update scores. SendMQTT is set false to prevent
    //redundancy when increasing both player and team score at the same time
    @SideOnly(Side.SERVER)
    public static void increasePlayerScore(String id, int value)
    {
        if(playerScores.containsKey(id))
        {
            playerScores.put(id, playerScores.get(id) + value);
        }
        else
        {
            playerScores.put(id, value);
        }        
        increaseTeamScore(value);
        updateAllPlayers();
        sendMQTT();
       
    }
    
    @SideOnly(Side.SERVER)
    public static void increaseTeamScore(int value)
    {
    	teamScore += value; 
    }


    @SideOnly(Side.SERVER)
    public static void sendMQTT()
    {
        ScoreboardModel scoreboardChangedModel = new ScoreboardModel(
                ScoreboardManager.getPlayerScores(),
                ScoreboardManager.getTeamScore()
        );

        InternalMqttClient.publish(scoreboardChangedModel.toJsonString(), scoreboardChangedModel.getTopic());
    }

    public static Map<String, Integer> getPlayerScores()
    {
        return playerScores;
    }

    public static int getTeamScore()
    {
        return teamScore;
    }

}
