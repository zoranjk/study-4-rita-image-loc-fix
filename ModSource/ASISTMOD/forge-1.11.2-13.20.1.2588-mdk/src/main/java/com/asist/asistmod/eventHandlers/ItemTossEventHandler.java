package com.asist.asistmod.eventHandlers;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumHand;
import net.minecraftforge.event.entity.item.ItemTossEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ItemTossEventHandler {
	
	
	MinecraftServer server;
	
	public ItemTossEventHandler(MinecraftServer server) {
		
		this.server = server;
	}
	
	@SubscribeEvent	
	public void onPlayerTossItem( ItemTossEvent event) {
		event.setCanceled(true);
		EntityItem item = event.getEntityItem();
		ItemStack itemStack = item.getEntityItem();
		EntityPlayer player = event.getPlayer();
		player.setHeldItem(EnumHand.MAIN_HAND, itemStack);
	}
}
