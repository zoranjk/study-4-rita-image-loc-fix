package com.asist.asistmod.item;

import net.minecraft.item.ItemPickaxe;

public class WireCuttersRed extends ItemPickaxe {
		
	public WireCuttersRed(String name, ToolMaterial material) {
		super(material);
	    this.setUnlocalizedName(name);
	    this.setMaxDamage(2);
	    
	    
	}
}
