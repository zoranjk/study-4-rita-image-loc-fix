package com.asist.asistmod.eventHandlers;

import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.Deprecated.ItemEquipped.ItemEquippedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.item.ItemStack;
import net.minecraftforge.event.entity.living.LivingEquipmentChangeEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class LivingEquipmentChangeEventHandler {
	
	@SubscribeEvent	
	public void onLivingEquipmentChange(LivingEquipmentChangeEvent event){
		
		Player player = PlayerManager.getPlayerByName(event.getEntity().getName());
		ItemStack stack = event.getTo();
		String registryName = stack.getItem().getRegistryName().toString();
		//System.out.println(registryName);
		ItemType itemType = ItemType.getItemTypeByRegistryName(registryName);
		if(itemType.compareTo(ItemType.NULL)==0) {
			
		}
		else {
			ItemEquippedModel itemEquippedModel = new ItemEquippedModel();
			itemEquippedModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
			itemEquippedModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
			//itemEquippedModel.data.playername = name;
			itemEquippedModel.data.participant_id = player.participant_id;
			itemEquippedModel.data.equippeditemname = itemType.getName();
			InternalMqttClient.publish(itemEquippedModel.toJsonString(), "observations/events/player/itemequipped",player.getName());
		}
		
	}

}
