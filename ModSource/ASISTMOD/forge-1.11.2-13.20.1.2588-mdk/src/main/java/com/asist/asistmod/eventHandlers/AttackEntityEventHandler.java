package com.asist.asistmod.eventHandlers;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.ModSettings.ModSettings_v3;
import com.asist.asistmod.datamodels.PlayerStateChange.PlayerStateChangeModel;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.event.entity.player.AttackEntityEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class AttackEntityEventHandler {
	
	MinecraftServer server;
	ModSettings_v3 modSettings;
	
	public AttackEntityEventHandler( MinecraftServer server, ModSettings_v3 modSettings){
		this.server = server;
		this.modSettings = modSettings;
	}
	
	@SubscribeEvent	
	public void onEntityAttacked( AttackEntityEvent event) {
		
		// Cancelling the event will remove any damage dealt and deal with the
		// attackee being launched after multiple hits bug, the function will still run after line 33
		
		event.setCanceled(true);
		
		// Get ATTACKER and ATTACKEE
		
		String attacker = event.getEntityPlayer().getName();
		String attackee = event.getTarget().getName();
		Player playerHit = PlayerManager.getPlayerByName(attackee);
		Player playerHitting = PlayerManager.getPlayerByName(attacker);
		// check if attacker is medic
		if( playerHit != null && playerHitting != null  ) {
			
			Entity e = event.getTarget();
			EntityPlayer ep = (EntityPlayer)e;
			ep.clearActivePotions();
			ep.setHealth(20.0f);			
			//System.out.println("UnFreezing Player");			
			playerHit.setPlayerFrozen(false, playerHit.getParticipantId(), event.getEntity().getPosition(), 
					playerHitting.getParticipantId(), "player", playerHitting.getEntityPlayer().getPosition());
			
			publishHealthStateChange(playerHit, 20.0f);
		}
		
	}
	
	public void publishHealthStateChange(Player player, float remainingHealth) {
			
		Map<String,String> oldAttributes = new ConcurrentHashMap<String,String>(player.attributes);
		
		player.attributes.put("health", Float.toString(remainingHealth));
		
		Map<String,String[]>changedAttributes = AttributedObjectManager.returnAttributeDiff(oldAttributes, player.attributes);
		
		PlayerStateChangeModel model = new PlayerStateChangeModel(
				player.getParticipantId(),
				player.getEntityPlayer().getPosition(),
				player.lastDamagedById,
				player.lastDamagedByType,
				player.lastDamagedByPosition,
				changedAttributes,
				player.attributes);
		
		InternalMqttClient.publish(model.toJsonString(), model.getTopic());
	}

}
