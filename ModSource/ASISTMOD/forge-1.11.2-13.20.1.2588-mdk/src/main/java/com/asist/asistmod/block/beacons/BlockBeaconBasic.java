package com.asist.asistmod.block.beacons;

import java.util.Random;

import javax.annotation.Nullable;

import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectType;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs.ASISTModTabs;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.EnvironmentCreated.Single.EnvironmentCreatedSingleModel;
import com.asist.asistmod.datamodels.ItemUsed.ItemUsedModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.datamodels.ToolUsed.ToolUsedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.BeaconTileEntity;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockBeaconBasic extends BlockContainer {
	

    public BlockBeaconBasic(Material materialIn) {		
		super(Material.CARPET);        
        this.setTickRandomly(true);
        this.setCreativeTab(ASISTModTabs.ASISTBlocks);
	}

	protected static final AxisAlignedBB CARPET_AABB = new AxisAlignedBB(0.0D, 0.0D, 0.0D, 1.0D, 0.0625D, 1.0D);

	// DONT DROP ANY ITEMS
 	@Override public int quantityDropped(Random par1Random){ return 0;}
    
    @Override
    public boolean isFullyOpaque(IBlockState state) {
        return false;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        return CARPET_AABB;
    }

    @Nullable
    @Override
    public AxisAlignedBB getCollisionBoundingBox(IBlockState blockState, IBlockAccess worldIn, BlockPos pos) {
        return CARPET_AABB;
    }

    @SuppressWarnings("deprecation")
	@SideOnly(Side.CLIENT)
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos, EnumFacing side)
    {
        return side == EnumFacing.UP ? true : (blockAccess.getBlockState(pos.offset(side)).getBlock() == this ? true : super.shouldSideBeRendered(blockState, blockAccess, pos, side));
    }
    

 // MAKES THE RENDERER WORK, OTHERWISE BLOCK IS INVISIBLE
 	@Override
 	public EnumBlockRenderType getRenderType(IBlockState state) {
 		return EnumBlockRenderType.MODEL;
 	}

    
    
    /**
     * Called by ItemBlocks after a block is set in the world, to allow post-place logic
     */
    
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    {    
    	////System.out.println("--- BLOCKBEACONBASIC.ONBLOCKPLACEDBY ----- BEACON PLACED ------");
    	if(!worldIn.isRemote) {    	
    		
    		Player player = PlayerManager.getPlayerByName(placer.getName());
    		String participant_id = player.participant_id;
    		String itemName = ItemType.getItemTypeByRegistryName( stack.getItem().getRegistryName().toString() ).getName();
    		String itemStackId = stack.getTagCompound().getTag("item_stack_id").toString();
        	itemStackId = itemStackId.replaceAll("\"", "");
        	String itemId = AttributedObjectType.GENERIC_ITEM.getPrefix()+Integer.toString(stack.getCount());
    		String idForMessage = itemStackId+'_'+itemId;
        	////System.out.println( "ID from onBlockPacedBy  ... item id :" + idForMessage);
        	
        	BeaconTileEntity te = (BeaconTileEntity) worldIn.getTileEntity(pos);
        	te.setBeaconId(idForMessage);   
    		
			ToolUsedModel toolUsedModel = new ToolUsedModel();
			toolUsedModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
			toolUsedModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
			//toolUsedModel.data.playername = placer.getName();
			toolUsedModel.data.participant_id = participant_id;
			toolUsedModel.data.tool_type = itemName;
			toolUsedModel.data.durability = 0;
			toolUsedModel.data.target_block_x = pos.getX();
			toolUsedModel.data.target_block_y = pos.getY();
			toolUsedModel.data.target_block_z = pos.getZ();
			toolUsedModel.data.target_block_type = worldIn.getBlockState(pos).getBlock().getRegistryName().toString().split(":")[1];
			InternalMqttClient.publish(toolUsedModel.toJsonString(), "observations/events/player/tool_used",placer.getName());
			
			ItemUsedModel itemUsedModel = new ItemUsedModel();
			itemUsedModel.data.participant_id = participant_id;						
			itemUsedModel.data.item_id = idForMessage;
			itemUsedModel.data.item_name = itemName;
			itemUsedModel.data.input_mode = "RIGHT_MOUSE";
			itemUsedModel.data.target_x = pos.getX();;
			itemUsedModel.data.target_y = pos.getY();;
			itemUsedModel.data.target_z = pos.getZ();;
			
			InternalMqttClient.publish(itemUsedModel.toJsonString(), itemUsedModel.getTopic(), placer.getName());
			
			ObjectStateChangeData data = new ObjectStateChangeData(true);
        	data.id = idForMessage;
        	data.triggering_entity = participant_id;
    		data.type = this.getRegistryName().toString().split(":")[1];
    		data.x = pos.getX();
    		data.y = pos.getY();
    		data.z = pos.getZ();
    		data.currAttributes = AttributedObjectManager.getItemAttributes(itemStackId, itemId);
        	EnvironmentCreatedSingleModel environmentCreatedModel = new EnvironmentCreatedSingleModel(participant_id,data);
        	InternalMqttClient.publish(environmentCreatedModel.toJsonString(), environmentCreatedModel.getTopic());
			
			player.lastBeaconId = idForMessage;
			player.lastBeaconType = this.getRegistryName().toString().split(":")[1];
			player.lastBeaconPosition = pos;			
			
    	}
    }
    
    
    @Override
	public TileEntity createNewTileEntity(World worldIn, int meta ) {

		BeaconTileEntity tileEntity = new BeaconTileEntity();

		return tileEntity;
	}
    
    /******************* CONTROLS THE BLOCK FALLING DUE TO GRAVITY *************************/
    
   

}
