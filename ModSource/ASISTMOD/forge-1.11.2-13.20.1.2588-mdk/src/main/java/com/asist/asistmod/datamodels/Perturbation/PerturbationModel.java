package com.asist.asistmod.datamodels.Perturbation;

import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.mqtt.MessageTopic;
import com.google.gson.Gson;

public class PerturbationModel{
	
	public HeaderModel header = new HeaderModel();
	
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public PerturbationDataModel data = new PerturbationDataModel();
		
		
	public PerturbationModel(String type,int x,int y,int z, String id) {
		
		data.type = type;
		data.additional_info = new PerturbationAdditionalInfoFire( x,y,z,id);
				
	}	
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this);		
	}
	
	public MessageTopic getMessageTopicEnum() {
		return MessageTopic.MISSION_PERTURBATION_START;
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	public String getEventName() {
		return getMessageTopicEnum().getEventName();
	}
	
	public String getVersion() {
		return getMessageTopicEnum().getVersion();
	}
	
	public void publish() {
		InternalMqttClient.publish(this.toJsonString(), this.getTopic());
	}

}
