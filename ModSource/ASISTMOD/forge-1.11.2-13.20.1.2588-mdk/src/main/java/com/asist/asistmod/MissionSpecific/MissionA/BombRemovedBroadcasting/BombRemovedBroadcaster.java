package com.asist.asistmod.MissionSpecific.MissionA.BombRemovedBroadcasting;

import java.util.concurrent.CopyOnWriteArrayList;

public class BombRemovedBroadcaster {

    public static boolean isInitialized = false;

    public static CopyOnWriteArrayList<BombRemovedListener> listeners = new CopyOnWriteArrayList<BombRemovedListener>();

    public static void init()
    {
        isInitialized = true;
    }

    public static void bombRemoved(String bombID)
    {
        BombRemovedBroadcaster.listeners.forEach( (listener) ->{
            listener.onBombRemoved(bombID);
        } );
    }

    public  static void subscribeToOnRemoved( BombRemovedListener listener) {
        BombRemovedBroadcaster.listeners.add(listener);
    }

    public  static void unsubscribeToOnRemoved( BombRemovedListener listener) {
        BombRemovedBroadcaster.listeners.remove(listener);
    }
}
