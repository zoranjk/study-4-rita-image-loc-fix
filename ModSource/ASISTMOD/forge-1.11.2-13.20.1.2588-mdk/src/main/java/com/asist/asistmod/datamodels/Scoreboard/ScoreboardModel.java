package com.asist.asistmod.datamodels.Scoreboard;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;
import com.google.gson.Gson;
import scala.Int;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ScoreboardModel{	
	 
	
	public HeaderModel header = new HeaderModel();
	
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public ScoreboardData data = new ScoreboardData();

	public ScoreboardModel(Map<String, Integer> playerScores, Integer teamScore)
	{
		data.playerScores = new ConcurrentHashMap<>(playerScores);
		data.teamScore = teamScore;
	}
	
	public String toJsonString() {

		return AsistMod.gson.toJson(this);

	}

	public MessageTopic getMessageTopicEnum() {
		return MessageTopic.SCORE_CHANGE;
	}

	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}

	public String getEventName() {
		return getMessageTopicEnum().getEventName();
	}

	public String getVersion() {
		return getMessageTopicEnum().getVersion();
	}

}
