package com.asist.asistmod.datamodels.Deprecated.GroundTruth.FreezeBlockList;

public class FreezeBlockListItem {
	
	public Double x = null;
	public Double y = null;
	public Double z = null;
    public String block_type = null;
    public String room_name = null;
    public String feature_type = null;

}
