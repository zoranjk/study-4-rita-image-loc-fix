package com.asist.asistmod.eventHandlers;

import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemManager;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.datamodels.ToolDepleted.ToolDepletedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.event.entity.player.PlayerDestroyItemEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class ToolBreakEventHandler {
	
	
	MinecraftServer server;
	
	public ToolBreakEventHandler(MinecraftServer server) {
		
		this.server = server;
	}
	
	@SubscribeEvent	
	public void onPlayerItemBreak( PlayerDestroyItemEvent event ) {
		
		EntityPlayer player = event.getEntityPlayer();
		String playerName = player.getName();
		ItemStack item = event.getOriginal();
		ItemType heldModItem = ItemManager.getEnum(item);
		
		ToolDepletedModel toolDepletedModel = new ToolDepletedModel();
		toolDepletedModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
		toolDepletedModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
		//toolDepletedModel.data.playername = playerName;
		toolDepletedModel.data.participant_id = InternalMqttClient.name_to_pid(playerName);
		toolDepletedModel.data.tool_type = heldModItem.getName();
		InternalMqttClient.publish(toolDepletedModel.toJsonString(), "observations/events/player/tool_depleted", playerName);
		
		
	}
}
