package com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay;

import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager.MissionStage;
import com.asist.asistmod.network.messages.AgentInterventionChatPacket;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class RenderOverlayEventHandler
{
	// public static Version2ScoreboardGui testGui = null;
	
	public static Version3MainGui gui = null;
	public static String callSign = "Not Set";
	public static int minute = 0;
	public static int second = 0;
	public static int bombsDefused = 0;
	public static int bombsExploded = 0;
	public static int bombsRemaining = 0;
	public static int storeVotes = 0;	
	public static int teamScore = 0;
	public static int teamBudget = 0;
	public static AgentInterventionChatPacket currentIntervention = null;
	public static String centerOfScreenMessage = null;
	
	// SET THIS BACK TO NULL FOR PRODUCTION
	public static MissionStage stage = null;
	
	public static Minecraft mc = null;
	
	public RenderOverlayEventHandler() {		
		
	}
	
    @SubscribeEvent
    public void onRenderGui(RenderGameOverlayEvent.Pre event)
    {
    	if(mc == null ) {
    		mc = Minecraft.getMinecraft();
    	}
		gui = new Version3MainGui(
				mc,
				stage,
				callSign,
				minute,
				second,
				bombsDefused,
				bombsExploded,
				bombsRemaining,
				storeVotes,
				teamScore,
				teamBudget,
				currentIntervention,
				centerOfScreenMessage
			); 
    } 
    
	public static void onMissionTimeChange (int m, int s) {
		
		minute = m;
		second = s;		
	
	}
	
	//public void onFieldTimeChange (int m, int s) {
		
	//	stage_minute = m;
	//	stage_second = s;		
	
	//}
	
	public static void onTeamScoreChange (int s) {		
		teamScore = s;
	}
	
	
	public static void onScoreboardChange (int defused,int exploded, int remaining) {		
		
		bombsDefused = defused;
		bombsExploded = exploded;
		bombsRemaining = remaining;
	
	}
	
	public static void onScoreboardChange(int ts)
	{
		teamScore = ts;
	}
	
	public static void onCallSignAssignment(String c) {
		
		callSign = c;
	}
    
    

}