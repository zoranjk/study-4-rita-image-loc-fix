package com.asist.asistmod.mqtt;
import java.time.Clock;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.BlockPlacement.MapBlockManager;
import com.asist.asistmod.MissionCommon.BlockPlacement.MapBlockManager.MapBlockMode;
import com.asist.asistmod.MissionCommon.InterventionManagement.ChatInterventions.ChatInterventionManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.CallSigns;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager.MissionStage;
import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ServerSideShopManager;
import com.asist.asistmod.datamodels.AgentChatIntervention.AgentChatInterventionModel;
import com.asist.asistmod.datamodels.ItemDiscard.ItemDiscardModel;
import com.asist.asistmod.datamodels.ModSettings.ModSettings_v3;
import com.asist.asistmod.datamodels.TrialInfo.TrialInfoModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLLog;

public class InternalMqttClient {

	static int qos = 2;
	static String broker = "Not Set";
	static String clientId = "MinecraftInternalMqttClient";
	static MqttClient client;
	
	static MinecraftServer server;
	
	public static List<String> authorizedPlayers = new ArrayList<String>(); 
	
	public static TrialInfoModel currentTrialInfo = new TrialInfoModel();
	
	static Gson gson = new Gson();

	public static ModSettings_v3 modSettings;
	
	static int headsUpChatMessageId = 0;

	static MemoryPersistence persistence = new MemoryPersistence();
	
	public static boolean isInitialized = false;
	
	public static boolean blockLoadingSuccessful = false;	
	
	public static boolean prematureMissionStopNonCrash = false;
	public static String prematureMissionStopReason = "NOT_SET_IN_MINECRAFT";
	
	public static boolean throwAwayInstance = true;
	
	public static void init() {
		
		try {
			client = new MqttClient("tcp://localhost:1883",clientId, persistence);
			
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);			
			//System.out.println("Connecting to broker: " + broker);

			// connect
			client.connect(connOpts);
			//System.out.println("Connected");
			isInitialized = true;

		} catch (MqttException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
	}
	
	public static void init ( ModSettings_v3 ms ) {
		
		modSettings = ms;
		broker = ms.mqtthost;
		
		FMLLog.log.info("-------------------- Attempting to start mqtt client----------------------------------");
		
		try {

			// create connection object
			client = new MqttClient(broker,clientId, persistence);

			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);
			//System.out.println("Connecting to broker: " + broker);

			// connect
			
			client.connect(connOpts);
			
			
			//System.out.println("Connected");
			
			isInitialized = true;
			
		}
		
		catch (MqttException me) {

			//System.out.println("reason " + me.getReasonCode());
			//System.out.println("msg " + me.getMessage());
			//System.out.println("loc " + me.getLocalizedMessage());
			//System.out.println("cause " + me.getCause());
			//System.out.println("excep " + me);
			me.printStackTrace();
		}
		
	}
	public static void linkServer ( MinecraftServer s ) {
		
		server = s;		
		FMLLog.log.info("-------------------- Linking Minecraft Server object to mqtt client----------------------------------");
	
		try {	
			client.subscribe("control/status/premature_mission_stop", qos, (topic, message) -> {
				
				prematureMissionStopNonCrash = true;
				
				String stopMessageString= new String(message.getPayload());
				
				System.out.println(stopMessageString);
				
				JsonObject jsonObject = gson.fromJson(stopMessageString, JsonObject.class);
				
				prematureMissionStopReason = jsonObject.get("reason").getAsString();
				
				AsistMod.server.commandManager.executeCommand(AsistMod.server, "stop");
				
			});
			client.subscribe("control/response/getTrialInfo", qos, (topic, message) -> {			
				
				
				try {
					
					blockLoadingSuccessful = false;
					
					// turn off random ticks while placing blocks to try and stop ticking world error 
					server.commandManager.executeCommand(server, "gamerule randomTickSpeed 0");
					
					String trial_info = new String(message.getPayload());			
					
					currentTrialInfo = gson.fromJson(trial_info, TrialInfoModel.class);
					
					// link to the central class for logical access from anywhere, like ModSettings
					AsistMod.currentTrialInfo = currentTrialInfo;
					
					List<String> callSignsList = new ArrayList<String>();
					
					System.out.print( "Callsigns : ");
					System.out.print("\n");					
					
					currentTrialInfo.callsigns.forEach((k,v)->{
						System.out.println(k+":"+v);
						authorizedPlayers.add(k);
						callSignsList.add(v);
					});
					
					String[] callSignsArray = new String[callSignsList.size()];
					CallSigns.init(callSignsList.toArray(callSignsArray));
					ServerSideShopManager.init( CallSigns.masterCallSignArray.length);					
					
					System.out.print( "Participant IDs : ");
					System.out.print("\n");
					
					currentTrialInfo.participant_ids.forEach((k,v)->{
						System.out.println(k+":"+v);
					});
					
					System.out.print( "Observers : ");
					System.out.print("\n");
					
					currentTrialInfo.observer_info.forEach( n ->{
						System.out.print(n);
						authorizedPlayers.add(n);
					});
					
					System.out.print("\n");				

					FMLLog.log.info("Experiment_ID : " + currentTrialInfo.experiment_id + " Trial_ID : " + currentTrialInfo.trial_id + " Mission_Name : " + currentTrialInfo.mission_name
							+ " Map_Name : " + currentTrialInfo.map_name + " MapBlockFile : " + currentTrialInfo.map_block_filename + " MapInfoFile : " + currentTrialInfo.map_info_filename);
					
					if( currentTrialInfo.map_block_filename.contains("csv") ) {
						throwAwayInstance = false;
					} 
					
					System.out.println("Throw Away Placeholder Instance ? " + InternalMqttClient.throwAwayInstance);

					try {
						if( currentTrialInfo.map_block_filename != null && currentTrialInfo.map_block_filename.length()>0 ) {
							//System.out.println("Placing blocks");
							MapBlockManager.addBlocksFromFile("./mods/"+currentTrialInfo.map_block_filename, server.worlds[0],MapBlockMode.MISSION_START);					
						}
					}
					catch(Exception e) {
						//System.out.println( " We couldn't find the MAPBLOCK FILE!!!! LOADING ORION csv AS A BACKUP");
						//MapBlockManager.addBlocksFromFile("./mods/MapBlocks_Orion_1.0.csv", server.worlds[0],MapBlockMode.MISSION_START);
					}
					
					
					// turn ticks back on to default 3
					server.commandManager.executeCommand(server, "gamerule randomTickSpeed 3");
					
					if (currentTrialInfo.active_agents != null ) {
						currentTrialInfo.active_agents.forEach( n -> {
							System.out.print("Active Agent: " + n);
							subscribeToInterventions(n);
						});
					}
					
					
				}catch(Exception e){
					//System.out.println("MQTTCLIENT ERROR - GET TRIAL INFO");
					e.printStackTrace();
				}
					////System.out.println(VictimLocations.printBeepMap());				
					
				Thread errorThread = new Thread() {			
					
					public void run() {				
						
						long elapsedMilliseconds = 0;
						long startTime = Clock.systemDefaultZone().millis();
						// WAIT 5 SECONDS TO VERIFY BLOCK LOADING CRASH DID NOT OCCUR
						while (elapsedMilliseconds <= 10000 ) {					
							try { 
								Thread.sleep(100); 
							} 
							catch (InterruptedException e) {
								e.printStackTrace();
							}						
							elapsedMilliseconds = Clock.systemDefaultZone().millis() - startTime;						
						}
						
						InternalMqttClient.blockLoadingSuccessful = true;
						System.out.println("Successfully Loaded MapBlocks File. No Tick Errors");
						InternalMqttClient.publish("{\"loaded\":\"100\"}", "status/minecraft/loading");						
					}			
				}; // END THREAD DEFINITION
				
				errorThread.start();				
			
			}); // END SUBSCRIBE TO TOPIC
			
			client.subscribe("ui/click", qos, (topic, message) -> {
				
				//System.out.println("Received ui click message");
				
				String ui_click_message = new String(message.getPayload());
				
				//System.out.println(ui_click_message);
				
				JsonObject jsonObject = gson.fromJson(ui_click_message, JsonObject.class);
				
				JsonObject data = jsonObject.get("data").getAsJsonObject();
				
				JsonObject additional_info = data.get("additional_info").getAsJsonObject();
				
				String meta_action = additional_info.get("meta_action").getAsString();
				
				if( meta_action.contains("PROPOSE_TOOL_PURCHASE") ) {
					
					//String[] split = meta_action.split("_");
					char action = meta_action.charAt(meta_action.length()-1);
					int itemCode = additional_info.get("item_code").getAsInt();
					int callSignCode = additional_info.get("call_sign_code").getAsInt();
					
					ServerSideShopManager.updateProposedToolSelection(action, itemCode, callSignCode);
				}
				else if( meta_action.contentEquals("VOTE_LEAVE_STORE") ) {
					
					int callSignCode = additional_info.get("call_sign_code").getAsInt();
					
					ServerSideShopManager.setPlayerReadyToProceedToField(CallSigns.masterCallSignArray[callSignCode]);
				}
				
				//JsonObject jsonObject = gson.fromJson(trial_info, JsonObject.class);
				
				//Map<String, Object> attributes = new HashMap<String, Object>();
		        //Set<Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
		        //for(Map.Entry<String,JsonElement> entry : entrySet){
		          //attributes.put(entry.getKey(), jsonObject.get(entry.getKey()));
		        //}
	
		        //for(Map.Entry<String,Object> att : attributes.entrySet()){
		            ////System.out.println("key >>> "+att.getKey());
		            ////System.out.println("val >>> "+att.getValue());
		            //} 
				
			});

		} // END TRY
		
		catch (MqttException me) {

			//System.out.println("reason " + me.getReasonCode());
			//System.out.println("msg " + me.getMessage());
			//System.out.println("loc " + me.getLocalizedMessage());
			//System.out.println("cause " + me.getCause());
			//System.out.println("excep " + me);
			me.printStackTrace();
		}
		
		//server.theProfiler.endSection();
		//server.theProfiler.endSection();
	}
	
	

	// NO OBSERVER FILTERING
	public static void publish(String msg, String topic) {
		if(AsistMod.server != null) {
			//AsistMod.server.theProfiler.startSection("publish");
		}
		if(InternalMqttClient.isInitialized) {
			
			MqttMessage message = new MqttMessage(msg.getBytes());
			message.setQos(qos);
			try {
				client.publish(topic, message);
			} catch (MqttPersistenceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (MqttException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
	}
	
	// OBSERVER AND INTRUDER FILTERING	
	public static void publish(String msg, String topic, String playerName) {
		
		
		if(InternalMqttClient.isInitialized) {
			
			if ( !AsistMod.isObserver(playerName) && (InternalMqttClient.isAuthorized(playerName) || playerName.contentEquals("Server")) ) {
				
				MqttMessage message = new MqttMessage(msg.getBytes());
				message.setQos(qos);
				try {
					client.publish(topic, message);
				} catch (MqttPersistenceException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (MqttException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}			
		}		
	}
	
	public static boolean isAuthorized(String name) {
		
		return authorizedPlayers.contains(name);
		
	}
	
	public static String name_to_pid( String name ) {
		
		String pid = currentTrialInfo.participant_ids.get(name);
		
		if(pid != null ) {return pid;}
		
		return "Not Set";
	}

	public static String pid_to_name (String pid) {
		
		
		AtomicReference<String> name = new AtomicReference<String>();
		
		name.set("Not Set");
		
		InternalMqttClient.currentTrialInfo.participant_ids.forEach((k,v) -> {
			if ( v.contentEquals(pid) ) {name.set(k);}
		});
		
		return name.get();
	}
	
	// do the same for callsign
	
	public static String name_to_callsign( String name ) {	
		
		String cs = currentTrialInfo.callsigns.get(name);
		
		if(cs != null ) {return cs;}
		
		return "Not Set";
	}
	
	public static String callsign_to_name (String cs) {
		
		AtomicReference<String> name = new AtomicReference<String>();
		
		name.set("Not Set");
		
		InternalMqttClient.currentTrialInfo.callsigns.forEach((k,v) -> {
			if ( v.contentEquals(cs) ) {name.set(k);}
		});
		
		return name.get();
	}
	
	public static void subscribeToInterventions(String agentName) {
		
		try {
			
			//System.out.println( "Subscribing to interventions from : " + agentName);
			client.subscribe("item/discard", qos, (topic, message)-> {
				
				String payload = new String(message.getPayload());
				ItemDiscardModel model = gson.fromJson(payload,ItemDiscardModel.class);
				ServerSideShopManager.discardToolsInSlot(model.data.participant_id, model.data.item_name);
				
			});
			
			client.subscribe("agent/intervention/"+agentName+"/chat", qos, (topic, message) -> {
				//System.out.println("HIT"  );
				String payload = new String(message.getPayload());
				//System.out.println("Topic : " + topic);
				//System.out.println("Message : " + payload );
				
				try {
					
					if(MissionManager.stage.compareTo(MissionStage.FIELD_STAGE)==0 || MissionManager.stage.compareTo(MissionStage.RECON_STAGE)==0 ) {
						//System.out.println(" Received intervention during the field stage. Displaying in Minecraft");
						Gson gson = new Gson();				
						AgentChatInterventionModel model = gson.fromJson(payload, AgentChatInterventionModel.class);
						
						if (model.data.content != null && !model.data.content.isEmpty()) {	
							ChatInterventionManager.addChatIntervention(model);						
						}											
					}
					// MINECRAFT OVERLAY
					//if (agentInterventionModel.data != null && agentInterventionModel.data.renderer != null && agentInterventionModel.data.renderer.contentEquals("Minecraft_Overlay")
							//&& agentInterventionModel.data.content != null && !agentInterventionModel.data.content.isEmpty()
							//&& agentInterventionModel.data.receiver != null && !agentInterventionModel.data.receiver.isEmpty()) {
						//EntityPlayer player = server.worlds[0].getPlayerEntityByName(agentInterventionModel.data.receiver);
						//if (player != null) {
							////System.out.println("Minecraft_Overlay");
							//NetworkHandler.sendToClient( new HeadsUpChatPacket(agentInterventionModel.data, headsUpChatMessageId++ ), player );
						//}
					//}
					
				}catch(Exception e) {
					//System.out.println("--MQTTCLIENT ERROR - SUBSCRIBING TO INTERVENTIONS");
					e.printStackTrace();
				}				
			});
		}catch (MqttException me) {

			//System.out.println("reason " + me.getReasonCode());
			//System.out.println("msg " + me.getMessage());
			//System.out.println("loc " + me.getLocalizedMessage());
			//System.out.println("cause " + me.getCause());
			//System.out.println("excep " + me);
			me.printStackTrace();
		}
		
		//client.subscribe("agent/intervention/+/block", qos, (topic, message) -> {
			
			//String payload = new String(message.getPayload());
			////System.out.println("Topic : " + topic);
			////System.out.println("Message : " + payload );
			
			//try {
				
				//Gson gson = new Gson();
			
				//AgentInterventionModel agentInterventionModel = gson.fromJson(payload, AgentInterventionModel.class);			
			
				//if (agentInterventionModel.data != null && agentInterventionModel.data.renderer.contentEquals("Minecraft_Block")
						//&& agentInterventionModel.data.block_type != null && !agentInterventionModel.data.block_type.isEmpty()
						//&& agentInterventionModel.data.receiver != null && !agentInterventionModel.data.receiver.isEmpty() ) {
					
					//int x  = agentInterventionModel.data.block_x;
					//int y  = agentInterventionModel.data.block_y;
					//int z  = agentInterventionModel.data.block_z;
					
					//BlockPos pos = new BlockPos(x,y,z);
					
					//String startTime = agentInterventionModel.data.start;
					
					//String endTime = agentInterventionModel.data.end;
					
					//String block_type = agentInterventionModel.data.block_type;
					
					////System.out.println(" Received Minecraft_Block Intervention! ");
					
					//IBlockState oldBlockState = server.worlds[0].getBlockState(pos);
					//IBlockState newBlockState = ForgeRegistries.BLOCKS.getValue(new ResourceLocation("asistmod", block_type)).getDefaultState();
					
					//MarkingBlockManager.addAgentMarkingBlock(new BlockPos(x,y,z), newBlockState, oldBlockState, startTime,endTime);
					
					
					
					
				//}
				
				//if (agentInterventionModel.data != null && agentInterventionModel.data.renderer != null && agentInterventionModel.data.renderer.contentEquals("Minecraft_Overlay")
				//		&& agentInterventionModel.data.content != null && !agentInterventionModel.data.content.isEmpty()
				//		&& agentInterventionModel.data.receiver != null && !agentInterventionModel.data.receiver.isEmpty()) {
				//	EntityPlayer player = server.worlds[0].getPlayerEntityByName(agentInterventionModel.data.receiver);
				//	if (player != null) {
				//		//System.out.println("Minecraft_Overlay");
				//		NetworkHandler.sendToClient( new HeadsUpChatPacket(agentInterventionModel.data, headsUpChatMessageId++ ), player );
				//	}
				//}
				
			//}catch(Exception e) {
				//e.printStackTrace();
			//}				
		//});
	}

}
