package com.asist.asistmod.item;

import java.util.HashMap;
import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.FireManagement.FireManager;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs.ASISTModTabs;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemManager;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.block.perturbation.BlockFireCustom;
import com.asist.asistmod.datamodels.EnvironmentRemoved.Single.EnvironmentRemovedSingleModel;
import com.asist.asistmod.datamodels.ItemStateChange.ItemStateChangeModel;
import com.asist.asistmod.datamodels.ItemUsed.ItemUsedModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeModel;
import com.asist.asistmod.datamodels.ToolUsed.ToolUsedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.FireCustomTileEntity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockFire;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.command.CommandException;
import net.minecraft.command.CommandFill;
import net.minecraft.command.ServerCommandManager;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.lwjgl.Sys;


public class FireExtinguisher extends Item {

	int RANGE = 1;

	int RANGE_Y = 1;
        
	public FireExtinguisher(String name) {

            this.setUnlocalizedName(name);
            this.setMaxDamage(200);
            this.setCreativeTab(ASISTModTabs.ASISTItems);
            this.setMaxStackSize(64);
        }

        //Right click
        @Override
        public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos_in, EnumHand hand, EnumFacing facing, float hit_x, float hit_y, float hit_z) {

			boolean successful = false;
			ItemStack itemStack = player.getHeldItemMainhand();
			
			//System.out.println("onItemUseRemote : " + world.isRemote);

			FireManager.printFireIds();

			for (BlockPos pos : BlockPos.getAllInBox(pos_in.add(-RANGE, -RANGE_Y, -RANGE), pos_in.add(RANGE, RANGE_Y, RANGE))) {
				Block block = world.getBlockState(pos).getBlock();
				Block upperBlock = world.getBlockState(pos.up()).getBlock();
				//if(world.getBlockState(hitPos).getBlock() instanceof BlockBombBase){
				//IBlockState upperBlockState = world.getBlockState(pos.up());
				//upperBlockState.getMaterial() == Material.FIRE

				if (upperBlock instanceof BlockFire) {
					String id = "NOT_SET";
					FireCustomTileEntity te = (FireCustomTileEntity)( world.getTileEntity( pos.up() ) );					
					if( te != null ) {
						id = te.id;	
						te.unsubscribeToTimerTicks();
					}
					
					FireManager.removeFire(world, pos.up());					

					// Build Message for server
					if(!world.isRemote) {
						String playerName = player.getName();
						String pid = PlayerManager.getPlayerByName(playerName).getParticipantId();						
						ObjectStateChangeModel objectStateChangeModel = new ObjectStateChangeModel(
								id,
								upperBlock.getRegistryName().toString().split(":")[1],
								pid,
								pos.getX(),
								pos.getY(),
								pos.getZ(),
								null,
								null
						);


						// EnvironmentRemovedSingle
						EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel(pid, objectStateChangeModel.data);
						InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());
						publishItemUsedEvents(playerName, pid, pos, block, itemStack);
					}

					successful = true;
				}


			}

			//Final Steps for Server to only shrink once
			if(!world.isRemote) {


				AsistMod.server.commandManager.executeCommand(AsistMod.server,
						"particle droplet " + pos_in.getX() + " " + (pos_in.getY() + 1) + " " + pos_in.getZ() +
								" " + RANGE + " " + 0.3 + " " + RANGE + " 1 5000");


				///fill 30 52 137 20 52 150 minecraft:air 0 replace asistmod:block_fire_custom 0
				///fill ~ ~ ~ ~5 ~ ~5 asistmod:block_fire_custom
				//fill ~ ~ ~ ~5 ~ ~5 minecraft:air 0 replace asistmod:block_fire_custom 0

				if (successful) {

					itemStack.shrink(1);
				}


			}
			
			FireManager.printFireIds();

			/*
			BlockPos bottomRight = pos_in.up().north().west();
			BlockPos topLeft = pos_in.up().south().east();
			MinecraftServer server = world.getMinecraftServer();

			String command = "fill " + bottomRight.getX() + " " + bottomRight.getY() + " " + bottomRight.getZ() + " "
					+ topLeft.getX() + " " + topLeft.getY() + " " + topLeft.getZ() +
					" air 0 replace asistmod:block_fire_custom 0";

			server.commandManager.executeCommand(server, command);


			String[] command = new String[] {
					bottomRight.getX() + "",
					bottomRight.getY() + "",
					bottomRight.getZ() + "",
					topLeft.getX() + "",
					topLeft.getY() + "",
					topLeft.getZ() + "",
					"air",
					"0",
					"replace",
					"asistmod:block_fire_custom",
					"0"
			};

			CommandFill fill = new CommandFill();
			try {
				fill.execute(server, server, command);
				world.markBlockRangeForRenderUpdate(bottomRight, topLeft);
			} catch (CommandException e) {
				throw new RuntimeException(e);
			}

			*/

            return EnumActionResult.SUCCESS;

        }


    //on Left Click
    @Override
    public boolean onEntitySwing(EntityLivingBase entity, ItemStack itemstack) {
/*
        World world = entity.getEntityWorld();

        EntityPlayer player = (EntityPlayer)entity;

        if(!player.capabilities.isCreativeMode) {
            itemstack.shrink(1);
        }

        ItemStack waterStack = new ItemStack(Items.SPLASH_POTION);
        waterStack = PotionUtils.addPotionToItemStack(waterStack, PotionTypes.WATER);
        //String debug = getItemStackDisplayName(waterStack);

        if (!world.isRemote) {
            EntityPotion entityPotion = new EntityPotion(world, player, waterStack.copy());
            entityPotion.setHeadingFromThrower(player, player.rotationPitch, player.rotationYaw, -20.0F, 0.5F, 1.0F);
            world.spawnEntity(entityPotion);
        }


*/
        return false;
    }
    
    public void publishItemUsedEvents(String playerName, String pid, BlockPos pos, Block block, ItemStack itemStack) {
    	
    	String registryName = this.getRegistryName().toString();
    	ItemType heldModItem = ItemManager.getItemTypeFromRegistryName(registryName);
    	String itemName = heldModItem.getName();
		String itemStackId = itemStack.getTagCompound().getTag("item_stack_id").toString();
		////System.out.println(itemStackId);
		itemStackId = itemStackId.replaceAll("\"", "");
		////System.out.println(itemStackId);
		String itemId = "ITEM"+Integer.toString(itemStack.getCount());
		String idForMessage = itemStackId+'_'+itemId;
		//System.out.println("ITEM ID FOR MESSAGE : "+idForMessage);
    	
		// TOOL USED MESSAGE
    	ToolUsedModel toolUsedModel = new ToolUsedModel();
		toolUsedModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
		toolUsedModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
		//toolUsedModel.data.playername = playerName;
		toolUsedModel.data.participant_id = pid;
		toolUsedModel.data.tool_type = itemName;
		toolUsedModel.data.durability = 0;
		toolUsedModel.data.target_block_x = pos.getX();
		toolUsedModel.data.target_block_y = pos.getY();
		toolUsedModel.data.target_block_z = pos.getZ();
		toolUsedModel.data.target_block_type = block.getRegistryName().toString().split(":")[1];
		InternalMqttClient.publish(toolUsedModel.toJsonString(), "observations/events/player/tool_used",playerName);
		
		// ITEM USED MESSAGE
		ItemUsedModel itemUsedModel = new ItemUsedModel();
		itemUsedModel.data.participant_id = pid;						
		itemUsedModel.data.item_id = idForMessage;
		itemUsedModel.data.item_name = itemName;
		itemUsedModel.data.input_mode = "RIGHT_MOUSE";
		itemUsedModel.data.target_x = pos.getX();
		itemUsedModel.data.target_y = pos.getY();
		itemUsedModel.data.target_z = pos.getZ();		
		InternalMqttClient.publish(itemUsedModel.toJsonString(), itemUsedModel.getTopic(), playerName);
		
		
		// ITEM STATE CHANGE MESSAGE
		Map<String,String> currAttr = AttributedObjectManager.getItemAttributes(itemStackId,itemId);
		int usesRemaining = Integer.parseInt(currAttr.get("uses_remaining"));
		usesRemaining--;
		Map<String,String> newAttr = new HashMap<String,String>();
		newAttr.put("uses_remaining", Integer.toString(usesRemaining) );
		Map<String, String[]> changedAttributes = AttributedObjectManager.updateItemAttributes(itemStackId, itemId, newAttr);
		// for all single use items
		ItemStateChangeModel itemStateChangeModel = new ItemStateChangeModel(idForMessage,heldModItem.getName(),pid,changedAttributes,currAttr);
		InternalMqttClient.publish(itemStateChangeModel.toJsonString(), itemStateChangeModel.getTopic(), playerName);
		
    }

}
