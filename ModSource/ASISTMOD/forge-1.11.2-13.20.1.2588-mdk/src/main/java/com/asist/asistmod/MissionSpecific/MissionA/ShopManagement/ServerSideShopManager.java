package com.asist.asistmod.MissionSpecific.MissionA.ShopManagement;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.CallSigns;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.PlayerInventoryUpdate.PlayerInventoryUpdateModel;
import com.asist.asistmod.datamodels.TeamBudgetUpdate.TeamBudgetUpdateModel;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.shop.TeamBudgetUpdatePacket;
import com.asist.asistmod.network.messages.shop.UpdateVoteCountPacket;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.item.ItemStack;

public class ServerSideShopManager {

	static boolean productionMode = true;
	
	
	public static enum PageType {
		
		PAGE_0,
		PAGE_1,
		PAGE_2
		
	}
	
	public static ConcurrentMap<String,Integer>[] proposedPurchases = null;
	
	public static int teamBudget = 0;
	public static int votesToGoToShop = 0;
	public static int votesToGoToField = 0;
	
	public static boolean[] playersReadyToProceedToField ;
	public static boolean[] playersReadyToProceedToShop;
	
	public static int currentGuiPageIndex = 0;
	
	@SuppressWarnings("unchecked")
	public static void init( int num_callSigns ) {
		
		int markerCount = AsistMod.modSettings.markers_on_start;
		playersReadyToProceedToField = new boolean[num_callSigns];
		playersReadyToProceedToShop = new boolean[num_callSigns];
		for(int i=0; i<num_callSigns;i++) {
			playersReadyToProceedToField[i] = false;
			playersReadyToProceedToShop[i] = false;
		}
		
		proposedPurchases = new ConcurrentMap[num_callSigns];
		if(productionMode) {
			// FOR PRODUCTION
			for (int i = 0; i < proposedPurchases.length; i++) {
				proposedPurchases[i] = new ConcurrentHashMap<String, Integer>();				
				proposedPurchases[i].put(ItemType.WIRECUTTERS_RED.getName(), 0);
				proposedPurchases[i].put(ItemType.WIRECUTTERS_GREEN.getName(), 0);
				proposedPurchases[i].put(ItemType.WIRECUTTERS_BLUE.getName(), 0);				
				proposedPurchases[i].put(ItemType.BOMB_BEACON.getName(), markerCount+5);
				proposedPurchases[i].put(ItemType.HAZARD_BEACON.getName(), markerCount);
				proposedPurchases[i].put(ItemType.BOMB_SENSOR.getName(), 0);	
				proposedPurchases[i].put(ItemType.FIRE_EXTINGUISHER.getName(), 0);
				proposedPurchases[i].put(ItemType.BOMB_DISPOSER.getName(), 0);
				proposedPurchases[i].put(ItemType.BOMB_PPE.getName(), 0);
				

			}
		}
		else {


			// FOR TESTING ONLY
			proposedPurchases = new ConcurrentMap[num_callSigns];
			for (int i = 0; i < proposedPurchases.length; i++) {
				proposedPurchases[i] = new ConcurrentHashMap<String, Integer>();
				proposedPurchases[i].put(ItemType.WIRECUTTERS_RED.getName(), 10);
				proposedPurchases[i].put(ItemType.WIRECUTTERS_GREEN.getName(), 10);
				proposedPurchases[i].put(ItemType.WIRECUTTERS_BLUE.getName(), 10);				
				proposedPurchases[i].put(ItemType.BOMB_BEACON.getName(), 10);
				proposedPurchases[i].put(ItemType.HAZARD_BEACON.getName(), 10);
				proposedPurchases[i].put(ItemType.BOMB_SENSOR.getName(), 10);
				proposedPurchases[i].put(ItemType.FIRE_EXTINGUISHER.getName(), 10);
				proposedPurchases[i].put(ItemType.BOMB_DISPOSER.getName(), 10);
				proposedPurchases[i].put(ItemType.BOMB_PPE.getName(), 10);
			}
		}

				
		
		resetPlayerReadyToProceedMaps();
		teamBudget = AsistMod.modSettings.team_budget;
		
	}


	public static int addToTeamBudget(int valueToAdd)
	{
		teamBudget = teamBudget + valueToAdd;
		PlayerManager.players.forEach(player ->{			
			NetworkHandler.sendToClient(new TeamBudgetUpdatePacket(ServerSideShopManager.teamBudget), player.getEntityPlayer());			
		});				

		TeamBudgetUpdateModel message = new TeamBudgetUpdateModel(teamBudget);
		message.publish();
		
		return teamBudget;
		
	}
	
	public static void publishInventoryState() {
		Map<String, Map<String, Integer>> currInv = new HashMap<String,Map<String,Integer>>();
		
		PlayerManager.players.forEach(player ->{			
			currInv.put(player.participant_id,new HashMap<String,Integer>());
			InventoryPlayer inventory = player.getEntityPlayer().inventory;
			// 9 hotbar slots
			for( int i=0; i<ItemType.values().length-1 ; i++) {
				ItemStack itemStack = inventory.getStackInSlot(i);
				String itemNameInSlot = itemStack.getItem().getRegistryName().toString();
				String itemName = ItemType.getItemTypeByRegistryName( itemNameInSlot ).getName();
				int stackCount;
				// if it's not an asist item
				if( itemName.contentEquals("NULL") ) {
					itemName = ItemType.getItemBySlotIndex(i).getName();
					stackCount = 0;
				}
				else{					
					stackCount = itemStack.getCount();
				}
				currInv.get(player.participant_id).put(itemName, stackCount);
			}
		});		
		PlayerInventoryUpdateModel model = new PlayerInventoryUpdateModel(currInv);
		model.publish();
	}
	
	public static void resetPlayerReadyToProceedMaps() {
		
		//System.out.println("Resetting playersReadyToProceedMap to False");
		
		for(int i=0; i<playersReadyToProceedToField.length; i++) {
			playersReadyToProceedToField[i] = false;
		}
		for(int i=0; i<playersReadyToProceedToShop.length; i++) {
			playersReadyToProceedToShop[i] = false;
		}
		
		votesToGoToShop = 0;
		votesToGoToField = 0;
		PlayerManager.players.forEach(player ->{
			NetworkHandler.sendToClient(new UpdateVoteCountPacket(votesToGoToShop), player.getEntityPlayer());
		});
	}
	
	
	// returns the new budget
	public static int updateProposedToolSelection(char action,int itemCode,int callSignCode) {		
		
		ItemType itemType = ItemType.values()[itemCode];
		String itemKey=itemType.name();	
		
		int costOfTool = AsistMod.modSettings.tool_cost.get(itemKey);
		int costToBudget = 0;
		
		//System.out.println("Proposal : |"+action+"|"+itemKey+"|"+callSignCode+"|"+costOfTool+"|");
		
		int v= proposedPurchases[callSignCode].get(itemKey);
		if(action =='+') {	
			proposedPurchases[callSignCode].put(itemKey,v+1);
			costToBudget+=costOfTool;
		}
		else if (action == '-') {
			if(v>0) {
				proposedPurchases[callSignCode].put(itemKey,v-1);
				costToBudget-=costOfTool;
			}				
		}
		
		int budget = addToTeamBudget(-costToBudget);
		
		return budget;
	}
	
	
	
	public static void setPlayerReadyToProceedToField(String callSign){
		
		int callSignCode = CallSigns.getCallSignCode(callSign);		
		
		playersReadyToProceedToField[callSignCode] = true;
		
		if( checkAllPlayersReadyToProceedToField() ) {
			//System.out.println("All Players Ready To Proceed to Field.");
			
			resetPlayerReadyToProceedMaps();
			giveAllPlayersAllPurchasedTools();
			// just before RECON
			if( MissionManager.transitionsToField == 0 ) {
				//System.out.println("Transitioning All Players to Recon Stage.");
				//MissionManager.transitionToRecon();
			}
			// after RECON
			else {
				//System.out.println("Transitioning All Players to Field Stage.");
				MissionManager.transitionToField();
			}			
		};
	}
	
	public static void setPlayerReadyToProceedToShop(String callSign){
		
		int callSignCode = CallSigns.getCallSignCode(callSign);		
		playersReadyToProceedToShop[callSignCode] = true;
		
		
		if( checkAllPlayersReadyToProceedToShop() ) {
			//System.out.println("All Players Ready To Proceed to Shop.");			
			resetPlayerReadyToProceedMaps();
			//System.out.println("Transitioning All Players to Shop Stage.");
			MissionManager.transitionToShop();
			
		};		
	}
	
	public static boolean checkAllPlayersReadyToProceedToField() {

		for(int i = 0; i<playersReadyToProceedToField.length; i++) {
			if(playersReadyToProceedToField[i] == false) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean checkAllPlayersReadyToProceedToShop() {
		
		int votes = 0;
		for(int i = 0; i<playersReadyToProceedToShop.length; i++) {
			if(playersReadyToProceedToShop[i] == true) {
				votes++;
			}
		}
		
		votesToGoToShop = votes;
		PlayerManager.players.forEach(player ->{
			NetworkHandler.sendToClient(new UpdateVoteCountPacket(votesToGoToShop), player.getEntityPlayer());
		});
		
		return votes == playersReadyToProceedToShop.length;
	}
	
	public static void voteToLeaveShop(String callSign) {
		
		setPlayerReadyToProceedToField(callSign);
		
	}
	
	public static void voteToGoToShop(String callSign) {
		
		setPlayerReadyToProceedToShop(callSign);
		
	}
	
	public static void giveAllPlayersAllPurchasedTools() {
		try{
			//System.out.println("Giving All Players Their Tools.");
			PlayerManager.players.forEach( (p) ->{
				
				int callSignCode = p.getCallSignCode();
				InventoryPlayer inventory = p.getEntityPlayer().inventory;
				equipPurchasedToolsFromMap(proposedPurchases[callSignCode],inventory);		
				
			});			
			
			//AttributedObjectManager.printAllAttributedItems();	
		}
		catch(Exception e) {
			e.printStackTrace();
		}		
	}
	
	public static void equipPurchasedToolsFromMap ( ConcurrentMap<String,Integer> map, InventoryPlayer inventory ) {
		//System.out.println("Updating Tools for Individual Map : ");
		
		for(int i = 0; i< ItemType.values().length-1; i++) {
			ItemType itemType = ItemType.getItemBySlotIndex(i);
			String itemName = itemType.getName();
			ItemStack existingStack = inventory.getStackInSlot(i);
			int existingCount = existingStack.getCount();
			int purchasedCount = map.get(itemName);
			ItemStack purchasedStack = itemType.getItemStackCopyWithNBTAndAmount(itemName,existingCount+purchasedCount);
			inventory.setInventorySlotContents(i, purchasedStack);
			//System.out.println(itemName + " : Existing:"+existingCount+" Purchased:"+ purchasedCount + "Final count :" + purchasedStack.getCount() + "Slot : " + i);
		}
		
		
		// The keyset does not preserve order when adding to it, so we should iterate over the slots in the hotbar
		// Filling only the empties in the order of the keyset instead of what we do now
		//Iterator<String> iterator = map.keySet().iterator();
		
		// WE HAVE TO CHANGE THIS TO FILL EMPTY SLOTS INSTEAD OF WHAT IT
		//HOLD THE EMPTIES
		//List<Integer> emptyInventorySlots = new ArrayList<Integer>();
		
		
		//int keyIndex = 0;
		//while(iterator.hasNext()) {
		//	String key = iterator.next();
		//	ItemStack existingStack = inventory.getStackInSlot(keyIndex);
		//	int existingCount = existingStack.getCount();
		//	int purchasedCount = map.get(key);			
		//	ItemStack purchasedStack = ItemManager.getItemTypeFromName(key).getItemStackCopyWithNBTAndAmount(key,existingCount+purchasedCount);
		//	inventory.setInventorySlotContents(keyIndex, purchasedStack);
		//	//System.out.println(key + " : Existing:"+existingCount+" Purchased:"+ purchasedCount + "Final count :" + purchasedStack.getCount() + "Slot : " + keyIndex);					
		//	keyIndex++;
		//}
		
		// reset all counts to zero
		map.forEach((k,v)->{
			map.put(k, 0);
		});
		
	}
	
	public static void discardToolsInSlot(String pid , String itemName ) {
		
		ItemType itemType =  ItemType.getItemTypeByEnumName(itemName);
		int slotIndex = itemType.getSlotIndex();
		Player player = PlayerManager.getPlayerByPid(pid);
		EntityPlayer ep = player.getEntityPlayer();
		InventoryPlayer inventory = ep.inventory;
		ItemStack itemStack = inventory.getStackInSlot(slotIndex);
		itemStack.setCount(0);
		ep.renderBrokenItemStack(itemStack);
		
	}
}