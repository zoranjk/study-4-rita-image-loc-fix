package com.asist.asistmod.tile_entity;

import java.util.concurrent.ConcurrentHashMap;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectType;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class DisposerBlockTileEntity extends TileEntity implements ITickable {

    String id = "NOT SET";

    private boolean countingDown = false;


    //private IBlockState blockState = null;


    public double timer = 3;

    public boolean defused = false;
    public  boolean defusedAudioPlayed = false;



    ConcurrentHashMap<String,String> attributes = new ConcurrentHashMap<String,String>();


    public DisposerBlockTileEntity() {
        countingDown = true;

        timer = 20 * timer;
    }


    // Called once per tick ( 20 tps )
    @Override
    public void update(){

        if(!world.isRemote) {

            if (countingDown) {


                    if (timer > 0) {
                        timer -= 1;
                    } else if (timer <= 0 && !defusedAudioPlayed) {
                        defused = true;
                    }

                    if (defused && !defusedAudioPlayed) {
                        double y_pos = pos.getY() + 1;
                        AsistMod.playSound(this.world,"block.anvil.fall",this.pos);
                        AsistMod.server.commandManager.executeCommand(AsistMod.server,
                                "particle largesmoke " + pos.getX() + " " + y_pos + " " + pos.getZ() + " 1 1 1 1 100");

                        defusedAudioPlayed = true;

                        world.removeTileEntity(pos);
                    }




            }
        }
    }











    public void beginCountDown()
    {
        countingDown = true;

    }

    //This will pause/unpause the countdown depending on it's current state
    public void pauseCountDown()
    {
        countingDown = !countingDown;
    }



    @Override
    public boolean shouldRefresh(World world, BlockPos pos, IBlockState oldState, IBlockState newSate)
    {
        return (oldState.getBlock() != newSate.getBlock());
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {

        //System.out.println("Writing NBT");

        return super.writeToNBT(compound);
    }

    public NBTTagCompound createNBTTagCompound() {

        NBTTagCompound compound = new NBTTagCompound();




        compound.setString("id", this.id );

        compound.setBoolean("countingDown", this.countingDown);


        compound.setDouble("timer", this.timer);
        compound.setBoolean("defused", this.defused);
        compound.setBoolean("defusedAudioPlayed", this.defusedAudioPlayed);

        return compound;
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {

        //System.out.println("Reading NBT");
        super.readFromNBT(compound);


        this.id = compound.getString("id");

        this.countingDown = compound.getBoolean("countingDown");

        this.timer = compound.getDouble("timer");
        this.defused = compound.getBoolean("defused");
        this.defusedAudioPlayed = compound.getBoolean("defusedAudioPlayed");

    }

    public boolean compareBlockPos( BlockPos pos) {
        return ( pos.getX() == this.pos.getX() ) && ( pos.getY() == this.pos.getY() ) && ( pos.getZ() == this.pos.getZ() );
    }



    public String setDisposerID(int i) {
        this.id = AttributedObjectType.GENERIC_ITEM.getPrefix()+ i;

        return this.id;
    }


    public String getBombId() {


        return this.id;
    }

    public boolean isCountingDown() {


        return this.countingDown;
    }

}
