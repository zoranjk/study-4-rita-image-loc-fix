package com.asist.asistmod.datamodels.CommunicationChat;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class CommunicationChatData {
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public String sender_id = null;
	public String[] recipients = null;
	public String message = null;
	public String message_id = null;
}
