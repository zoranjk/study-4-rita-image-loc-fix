package com.asist.asistmod.datamodels.TeamBudgetUpdate;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.mqtt.MessageTopic;

public class TeamBudgetUpdateModel{
	
    public HeaderModel header = new HeaderModel();
	
    public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public TeamBudgetUpdateData data = new TeamBudgetUpdateData();
	
	
	public TeamBudgetUpdateModel( int teamBudget ) {
		
		header.message_type = "event";
		data.team_budget = teamBudget;
		
	}
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.TEAM_BUDGET_UPDATE; 
		
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this); 
		
	}
	
	public void publish() {
		InternalMqttClient.publish(this.toJsonString(), this.getTopic());
	}	
	
}
