package com.asist.asistmod.datamodels.EnvironmentRemoved.List;

import java.util.List;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.mqtt.MessageTopic;

public class EnvironmentRemovedListModel {
	
	
	public HeaderModel header = new HeaderModel();
		
	public MsgModel msg = new MsgModel(getEventName(),getEnumWrapper().getVersion());
	
	public EnvironmentRemovedListData data = new EnvironmentRemovedListData();		
		
	public EnvironmentRemovedListModel( String triggeringEntity, List<ObjectStateChangeData> list) {
		
		data.triggering_entity = triggeringEntity;
		data.list = list;
				
	}	
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this);		
	}
	
	public MessageTopic getEnumWrapper() {
		return MessageTopic.ENVIRONMENT_REMOVED_LIST;
	}
	
	public String getTopic() {
		return getEnumWrapper().getTopic();
	}
	
	public String getEventName() {
		return getEnumWrapper().getEventName();
	}
	
	public String getVersion() {
		return getEnumWrapper().getVersion();
	}
	
	

}
