package com.asist.asistmod.commands;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.block.state.IBlockState;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.CommandResultStats;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.init.Blocks;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.StructureBoundingBox;

/**
*  A command that takes two coordinates and generates a csv file of all of the block locations within the volume of these coordinates. 
*  csv file will be named "Mission" and located in the "run" folder of your eclipse workspace when the command is executed. 
*  Current blockPosA & blockPosB currently have the coordinates of the corner of the map.
*  If blockContext is true then the csv file will have a column called "Block Context" 
*/
public class CommandScanBombSpawns extends CommandBase {	
	
		final String FILENAME = "BombSpawnLocations.csv";
		public BlockPos blockPosA;
		public BlockPos blockPosB;
		public boolean useArgs;
		
		String header = "X,Y,Z";
		
		public CommandScanBombSpawns(BlockPos blockPosA, BlockPos blockPosB, boolean useArgs) {
			this.blockPosA = blockPosA;
			this.blockPosB = blockPosB;
			this.useArgs = useArgs;
		}
		
		//Formatting for any Minecraft command	
		@Override
		/**
	     * Gets the name of the command
	     */
	    public String getName()
	    {
	        return "scanbombspawns";
	    }

		@Override
	    /**
	     * Return the required permission level for this command.
	     */
	    public int getRequiredPermissionLevel()
	    {
	        return 2;
	    }

		@Override
	    /**
	     * Gets the usage string for the command.
	     *  
	     * @param sender The ICommandSender who is requesting usage details
	     */
	    public String getUsage(ICommandSender sender)
	    {
	        return "scanbombspawns x1 y1 z1 x2 y2 z2";
	    }

		@Override
	    /**
	     * Callback for when the command is executed
	     *  
	     * @param server The server instance
	     * @param sender The sender who executed the command
	     * @param args The arguments that were passed
	     */
		
/* The Scan command is a custom Minecraft command that can be entered as any normal command. 
 * It takes two coordinates for blocks in the world and then creates a bounding box between those two points.
 * Then it "counts" up the blocks using a for loop, and each time it "counts" blockpos4 is used to store the location of the block, the type of block, and
 * it can also store NBT Data or other information if needed, these parameters can be defined in BlockStateAndPos.java and then passed as arguments here.
 * blockpos4 is then added to 3 lists which are then combined into list3. The Scan command is based off of the Clone command from Minecraft, and the clone command
 * used different lists for tileentities, full blocks, and everything else. While the Scan command doesn't necessarily need these lists, it may be nice in the future to 
 * generate a list of just tileentities if we want to scan just the victim blocks for example.  
 * Once list3 is generated, it is written to a file called Mission which will be located in the run folder of your eclipse workspace. */
		
	    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
	        if (useArgs && args.length < 6) {
	            throw new WrongUsageException("/scanbombspawns x1 y1 z1 x2 y2 z2", new Object[0]);
	        }
	        else if (!useArgs && (args.length > 1)) {
	        	throw new WrongUsageException("/scanbombspawns", new Object[0]);	
	        }        	
            //sender.setCommandStat(CommandResultStats.Type.AFFECTED_BLOCKS, 0);
            
            BlockPos blockpos = null;
            BlockPos blockpos1 = null;
            
            //useArgs refers to arguments put in Minecraft console. This can be done in the server console also.  	            
            if (useArgs) {
            	blockpos = parseBlockPos(sender, args, 0, false);
            	blockpos1 = parseBlockPos(sender, args, 3, false);	           	            	
            	//System.out.println("Using arguments for scan");
            }
            //blockPosA and B are defined in the AsistMod.java. This is if you want to input the coordinates of the scan without entering Minecraft. 
            else {
            	blockpos = new BlockPos(blockPosA.getX(), blockPosA.getY(), blockPosA.getZ());
	            blockpos1 = new BlockPos(blockPosB.getX(), blockPosB.getY(), blockPosB.getZ());
            	//System.out.println("Using blockPosA and B for scan");
            }
            
            StructureBoundingBox structureboundingbox = new StructureBoundingBox(blockpos, blockpos1);
            int i = structureboundingbox.getXSize() * structureboundingbox.getYSize() * structureboundingbox.getZSize();

            /*i is the volume of the boundingbox, and the game checks this to make sure users won't crash the game if they try to create a boundingbox too large.
             * The default limit of i > 32768. I increased this to 500,000 so we could scan the whole space, but I have not tested the command on a space so large, so your game 
             * might crash if you scan something too large. If you ever need to scan larger spaces, change the i value here. */
            
            if (i > 500000) {
                throw new CommandException("Scan area was too large.", new Object[] {Integer.valueOf(i), Integer.valueOf(500000)});
            }
            else {
                if (structureboundingbox.minY >= 0 && structureboundingbox.maxY < 256)
                {
                    World world = sender.getEntityWorld();

                    if (world.isAreaLoaded(structureboundingbox))
                    {                        	
                        List<BlockStateAndPos> spawns = Lists.<BlockStateAndPos>newArrayList();
                        	                          
                        for (int k = structureboundingbox.minY; k <= structureboundingbox.maxY; ++k)
                        {
                            for (int l = structureboundingbox.minX; l <= structureboundingbox.maxX; ++l)
                            {
                                for (int j = structureboundingbox.minZ; j <= structureboundingbox.maxZ; ++j)
                                {
                                    BlockPos possibleSpawn = new BlockPos(l, k, j);
                                    BlockPos blockAbove = new BlockPos(l, k + 1, j);
                                    
                                    IBlockState possibleSpawnBlockState = world.getBlockState(possibleSpawn);
                                    IBlockState blockAboveState = world.getBlockState(blockAbove);

                                    if (possibleSpawnBlockState.getBlock() == Blocks.GRASS && blockAboveState.getBlock() == Blocks.AIR) {
                                    	spawns.add(new BlockStateAndPos (possibleSpawn, possibleSpawnBlockState));
                                    }
                                }
                            }
                        } //END FOR LOOPS                        
                        bombsToCSV(spawns, world);	 	                            	 	                                           	                		           			
            			
            			//Exceptions in case the command does not work. 
                        if (i <= 0)
                        {
                            throw new CommandException("No blocks were scanned", new Object[0]);
                        }
                        else
                        {
                            sender.setCommandStat(CommandResultStats.Type.AFFECTED_BLOCKS, i);
                            notifyCommandListener(sender, this, "Scan successful", new Object[] {Integer.valueOf(i)});
                        }
                    }
                    else
                    {
                        throw new CommandException("Unable to scan out of world", new Object[0]);
                    }
                }
                else
                {
                    throw new CommandException("Unable to scan out of world", new Object[0]);
                }
            }	       
        } //End Execute

		/**
	     * Get a list of options for when the user presses the TAB key
	     *  
	     * @param server The server instance
	     * @param sender The ICommandSender to get tab completions for
	     * @param args Any arguments that were present when TAB was pressed
	     * @param targetPos The block that the player's mouse is over, <tt>null</tt> if the mouse is not over a block
	     */
	    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos)
	    {
	        return args.length > 0 && args.length <= 1 ? getListOfStringsMatchingLastWord(args, new String[] {"victims", "rubble", "map"}) : ( args.length > 1 ? getListOfStringsMatchingLastWord(args, new String[] {"m1_", "m2_"}) : Collections.<String>emptyList());
	    }
	    
	    /**
	     * Writes the bomb spawn locations to mission csv file.
	     * @param blockList - list of blocks
	     * @param world
	     * @throws CommandException
	     */   
	    private void bombsToCSV(List<BlockStateAndPos> blockList, World world) throws CommandException {
	    	
	    	try {
				FileWriter missionblocks = new FileWriter(FILENAME);	                		
				//missionblocks.write(header);	            
				//missionblocks.write("\n");
				AtomicInteger i = new AtomicInteger(0);
				
				blockList.forEach(object ->{
				i.set(i.get() + 1);
					try {
						missionblocks.write("" + object.blockPos.getX()); //X
						missionblocks.write(",");
						
						int y = object.blockPos.getY() + 1; 
						missionblocks.write("" + y); //Y	                					
						missionblocks.write(",");
						
						missionblocks.write("" + object.blockPos.getZ()); //Z
						 
						missionblocks.write("\n"); //Next line  				    	
					}
					catch (Exception e) {
						 //System.out.println("Error writing an object out of spawns at: " + i.get());
						}
				});
				missionblocks.close();	         
			} catch (IOException e) {
				throw new CommandException("Writing to text file failed due to an exception.", new Object[0]);	
			}
	    }	
}