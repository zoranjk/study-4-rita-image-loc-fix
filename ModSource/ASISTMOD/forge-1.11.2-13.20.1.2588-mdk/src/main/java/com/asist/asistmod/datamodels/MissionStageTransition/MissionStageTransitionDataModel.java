package com.asist.asistmod.datamodels.MissionStageTransition;

import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class MissionStageTransitionDataModel {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public String mission_stage = MissionManager.stage.name();
	public int transitionsToField = MissionManager.transitionsToField;
	public int transitionsToShop = MissionManager.transitionsToShop;
	public int team_budget = 0;
		
}
