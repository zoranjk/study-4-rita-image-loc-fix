package com.asist.asistmod.network.messages.shop;

import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ClientSideShopManager;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class DisplayShopPacket extends MessagePacket<DisplayShopPacket> {
	
	int callSignLength = 0;
	String callSign="";
	int guiPageId = 0;
	
	public DisplayShopPacket() {
		
	}
	
	 /** Constructor for server-side
	 * @param displayShop  */
	public DisplayShopPacket(String callSign, int guiPageId) {
		this.callSign = callSign;
		this.callSignLength = callSign.length();
		this.guiPageId = guiPageId;
		
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		callSignLength=buf.readInt();
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<callSignLength;i++) {
			sb.append(buf.readChar());
		}
		callSign = sb.toString();
		guiPageId = buf.readInt();
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(callSignLength);
		for(int i=0;i<callSignLength;i++) {
			buf.writeChar(callSign.toCharArray()[i]);
		}
		buf.writeInt(guiPageId);
	}

	@Override
	public void handleClientSide(DisplayShopPacket message, EntityPlayer player) {
		//System.out.println("CLIENTSIDE DISPLAY SHOP PACKET");
		//if(AsistMod.side == Side.CLIENT) {
			//System.out.println("CLIENTSIDE DISPLAY SHOP PACKET");
			ClientSideShopManager.displayShopGui(0);
		//}
		
	}

	@Override
	public void handleServerSide(DisplayShopPacket message, EntityPlayer player) {
		//May need to set the display shop to false. If so, make the packet here. 
	}
}
