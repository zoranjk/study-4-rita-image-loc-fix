package com.asist.asistmod.eventHandlers;

import com.asist.asistmod.block._Blocks;

import net.minecraft.block.Block;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.DamageSource;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.MathHelper;
import net.minecraft.world.World;
import net.minecraft.world.WorldServer;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class PlayerEventHandler {
	
	@SubscribeEvent	
	public void onPlayerEvent(PlayerEvent.NameFormat event) {
		
		if(event instanceof PlayerEvent.NameFormat) {
			//Player player = PlayerManager.getPlayerByEntityPlayer(event.getEntityPlayer());
			//event.setDisplayname("Test");			
			//event.getDisplayname();
			//event.getEntity().setCustomNameTag("Test");
			//event.getEntity().setAlwaysRenderNameTag(true);		
			
		}		
		//event.setCanceled(true);
	}

	/**
	 * This function takes in a living update event and checks if it's a player entity. If it is we need to check
	 * on the update if the player is within the bounds of fire. Backend MC does it this way however their functions
	 * to do this are protected and private meaning you need to mimic this behavior if you want fire to burn the player.
	 * @param event
	 */
	@SubscribeEvent
	public void onPlayerMove(LivingEvent.LivingUpdateEvent event)
	{
		if(event.getEntityLiving() instanceof EntityPlayer)
		{
			EntityPlayer player = (EntityPlayer)event.getEntityLiving();

			if (isFlammableWithin(player.world, player.getEntityBoundingBox().contract(0.001D)))
			{
				dealFireDamage(player,1);
			}
		}
	}


	/**
	 * This is a simple check that if the player isn't immune to fire it damages them.
	 * NOTE: in the MC backend they have a fire timer here that tests how long the player is in the fire
	 * and then sets them ON_FIRE. While IN_FIRE you take damage, while ON_FIRE you take damage
	 * for the purposes of ASIST we don't need that ON_FIRE call so we can bypass any timers.
	 * @param player
	 * @param amount
	 */
	//EVENT HELPERS === mimicked from World.java, and Entity.Java for doing custom fire damage
	public void dealFireDamage(EntityPlayer player, int amount)
	{
		if (!player.isImmuneToFire())
		{
			player.attackEntityFrom(DamageSource.IN_FIRE, (float)amount);
		}
	}

	/**
	 * Taking in a world MD object and a bounding box, this is essentially making a hitbox for the fire to check if the
	 * user is inside it. This is because MC fire doesn't/does have a hitbox (confusing I know) In the code the
	 * hitbox is returned as NULL so it doesn't have one meaning Entites can go through fire, however,
	 * MC still wants to detect if a living entity or arrow passes through to do some things to it. So buried within
	 * the code MC does custom hitbox calculation like this to register those collision events.
	 *
	 * We also HAVE to override this segment of code because the default
	 * @see #isFlammableWithin
	 * call is checking against MC specific FIRE, LAVA, and FLOWING_LAVA, which we don't care about for the purposes of ASIST, and our custom fire.
	 *
	 * @param world
	 * @param bb
	 * @return
	 */
	public boolean isFlammableWithin(World world, AxisAlignedBB bb)
	{
		int i = MathHelper.floor(bb.minX);
		int j = MathHelper.ceil(bb.maxX);
		int k = MathHelper.floor(bb.minY);
		int l = MathHelper.ceil(bb.maxY);
		int i1 = MathHelper.floor(bb.minZ);
		int j1 = MathHelper.ceil(bb.maxZ);

		if (isAreaLoaded(world, i, k, i1, j, l, j1, true))
		{
			BlockPos.PooledMutableBlockPos blockpos$pooledmutableblockpos = BlockPos.PooledMutableBlockPos.retain();

			for (int k1 = i; k1 < j; ++k1)
			{
				for (int l1 = k; l1 < l; ++l1)
				{
					for (int i2 = i1; i2 < j1; ++i2)
					{
						Block block = world.getBlockState(blockpos$pooledmutableblockpos.setPos(k1, l1, i2)).getBlock();

						if (block == _Blocks.blockFireCustom)
						{
							blockpos$pooledmutableblockpos.release();
							return true;
						}
						else if (block.isBurning(world, new BlockPos(k1, l1, i2)))
						{
							blockpos$pooledmutableblockpos.release();
							return true;
						}
					}
				}
			}
			blockpos$pooledmutableblockpos.release();
		}
		return false;
	}

	/**
	 * This function is a chuck loading check that default MC does that we HAVE to override because our custom
	 * @see #isFlammableWithin
	 * function relies on it and base MC protects this by default.
	 * @param world
	 * @param xStart
	 * @param yStart
	 * @param zStart
	 * @param xEnd
	 * @param yEnd
	 * @param zEnd
	 * @param allowEmpty
	 * @return
	 */
	private boolean isAreaLoaded(World world, int xStart, int yStart, int zStart, int xEnd, int yEnd, int zEnd, boolean allowEmpty)
	{
		if (yEnd >= 0 && yStart < 256)
		{
			xStart = xStart >> 4;
			zStart = zStart >> 4;
			xEnd = xEnd >> 4;
			zEnd = zEnd >> 4;

			for (int i = xStart; i <= xEnd; ++i)
			{
				for (int j = zStart; j <= zEnd; ++j)
				{
					if(!world.isRemote) {
						if (!isChunkLoadedServer((WorldServer) world, i, j, allowEmpty))
						{
							return false;
						}
					}
					else
					{
						if (!isChunkLoadedClient((WorldClient) world, i, j, allowEmpty))
						{
							return false;
						}
					}
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	//Client

	/**
	 * Now this section of code is funky, By default isChunkLoaded is an abstract function that is to be overriden by
	 * child classes, in this case, WorldClient, and WorldServer. However, by default we typically only interact with
	 * the base class "world" throughout Forge modding. However, for the function above
	 * @see #isAreaLoaded
	 * the chunk is checked both on client and server and we need to have both checks here.
	 * @param world
	 * @param x
	 * @param z
	 * @param allowEmpty
	 * @return
	 */
	@SideOnly(Side.CLIENT)
	protected boolean isChunkLoadedClient(WorldClient world, int x, int z, boolean allowEmpty)
	{
		return allowEmpty || !world.getChunkProvider().provideChunk(x, z).isEmpty();
	}

	/**
	 * This function functions the same as
	 * @see #isChunkLoadedClient
	 * but instead is the client specific call.
	 * @param world
	 * @param x
	 * @param z
	 * @param allowEmpty
	 * @return
	 */
	//Server
	@SideOnly(Side.SERVER)
	protected boolean isChunkLoadedServer(WorldServer world, int x, int z, boolean allowEmpty)
	{
		return world.getChunkProvider().chunkExists(x, z);
	}
}
