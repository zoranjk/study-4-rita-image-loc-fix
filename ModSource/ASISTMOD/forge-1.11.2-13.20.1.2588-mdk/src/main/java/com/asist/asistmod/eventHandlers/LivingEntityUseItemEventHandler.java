package com.asist.asistmod.eventHandlers;

import net.minecraft.server.MinecraftServer;
import net.minecraftforge.event.entity.living.LivingEntityUseItemEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class LivingEntityUseItemEventHandler {
	
	
	MinecraftServer server;
	
	public LivingEntityUseItemEventHandler(MinecraftServer server) {
		
		this.server = server;
	}
	
	
	// Typicall an event that occurs when holding the right mouse button
	@SubscribeEvent	
	public void onItemUse( LivingEntityUseItemEvent.Finish event) {	
		

	}
	
	
}
