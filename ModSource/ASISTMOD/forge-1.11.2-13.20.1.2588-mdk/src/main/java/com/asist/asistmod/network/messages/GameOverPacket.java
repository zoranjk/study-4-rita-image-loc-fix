package com.asist.asistmod.network.messages;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class GameOverPacket extends MessagePacket<GameOverPacket> {
	
	String gameOverMessage;
	int gameOverMessageLength;
	
	public GameOverPacket(  )  {}

	public GameOverPacket( String gameOverMessage)  {
		this.gameOverMessage = gameOverMessage;
		this.gameOverMessageLength = gameOverMessage.length();

	}

	@Override
	public void fromBytes(ByteBuf buf) {
		
		// 0 - SHOP_STAGE, 1 - FIELD STAGE		
		int gameOverMessageLength = buf.readInt();
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<gameOverMessageLength; i++) {
			sb.append(buf.readChar());
		}
		gameOverMessage = sb.toString();
	}

	@Override
	public void toBytes(ByteBuf buf) {		
		
		buf.writeInt(this.gameOverMessageLength);
		for(int i=0; i<gameOverMessageLength; i++) {
			buf.writeChar(gameOverMessage.charAt(i));
		}
	}

	@Override
	public void handleClientSide(GameOverPacket message, EntityPlayer player) {
		// TODO Auto-generated method stub
		
		//System.out.println("Handle GameOverPacket ClientSide : " + message.gameOverMessage);
		
		RenderOverlayEventHandler.centerOfScreenMessage = message.gameOverMessage;
		
	}

	@Override
	public void handleServerSide(GameOverPacket message, EntityPlayer player) {
		// TODO Auto-generated method stub
		
	}

}
