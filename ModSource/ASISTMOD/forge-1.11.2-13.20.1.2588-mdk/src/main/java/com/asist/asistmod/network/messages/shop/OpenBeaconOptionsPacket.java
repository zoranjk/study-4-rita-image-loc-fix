package com.asist.asistmod.network.messages.shop;

import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ClientSideShopManager;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class OpenBeaconOptionsPacket extends MessagePacket<OpenBeaconOptionsPacket> {
	
	String[] callSigns;
	int [] callSignLengths;
	int callSignCode;
	int numCallSigns;
	String beaconId;
	int beaconIdLength;
	
	public OpenBeaconOptionsPacket() {
		
	}
	
	 /** Constructor for server-side
	 * @param displayShop  */
	public OpenBeaconOptionsPacket(int callSignCode,String[] callSigns, String beaconId) {
		this.callSigns = callSigns;
		this.callSignCode = callSignCode;
		this.callSignLengths = new int[callSigns.length];
		for(int i=0;i<callSigns.length;i++) {
			callSignLengths[i] = callSigns[i].length();
		}
		this.numCallSigns = callSigns.length;
		this.beaconId = beaconId;
		this.beaconIdLength = beaconId.length();
		
		
	}
	
	@Override
	public void fromBytes(ByteBuf buf) {
		
		callSignCode=buf.readInt();
		numCallSigns = buf.readInt();
		
		// lengths array
		int[] cslengths = new int[numCallSigns];		
		for( int i=0; i<cslengths.length; i++) {
			cslengths[i] = buf.readInt();
		}
		this.callSignLengths = cslengths;
		
		//callsigns array
		String[] cs = new String[numCallSigns];
		StringBuilder sb = new StringBuilder();
		for(int i=0;i<cs.length;i++) {
			for(int j=0;j<cslengths[i];j++) {
				sb.append(buf.readChar());
			}
			cs[i]=sb.toString();
			sb.setLength(0);
		}
		this.callSigns = cs;
		sb.setLength(0);
		this.beaconIdLength = buf.readInt();
		for(int i=0;i<beaconIdLength;i++) {
			sb.append(buf.readChar());
		}
		this.beaconId = sb.toString();
		
	}
	
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(callSignCode);
		buf.writeInt(numCallSigns);
		for(int i=0;i<callSignLengths.length;i++) {			
			buf.writeInt(callSignLengths[i]);
		}
		for(int i=0;i<callSigns.length;i++) {
			for( int j=0; j<callSigns[i].length(); j++) {
				buf.writeChar(callSigns[i].charAt(j));
			}			
		}
		buf.writeInt(beaconIdLength);
		for(int i=0;i<beaconIdLength;i++) {
			buf.writeChar(beaconId.charAt(i));
		}
	}
	
	@Override
	public void handleClientSide(OpenBeaconOptionsPacket message, EntityPlayer player) {
			ClientSideShopManager.displayBeaconGui(message.callSignCode,message.callSigns, message.beaconId);
	}

	@Override
	public void handleServerSide(OpenBeaconOptionsPacket message, EntityPlayer player) {
		//May need to set the display shop to false. If so, make the packet here. 
	}
}
