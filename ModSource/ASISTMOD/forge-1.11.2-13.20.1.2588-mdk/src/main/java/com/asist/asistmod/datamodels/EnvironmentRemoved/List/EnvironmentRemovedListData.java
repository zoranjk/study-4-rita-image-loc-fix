package com.asist.asistmod.datamodels.EnvironmentRemoved.List;

import java.util.List;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;

public class EnvironmentRemovedListData {
	
	public String triggering_entity = "NOT_SET";
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();	
	public List<ObjectStateChangeData> list;
}
