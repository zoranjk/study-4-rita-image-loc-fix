package com.asist.asistmod.MissionSpecific.MissionA.FireManagement;

public class FirePlacingThread {
	
	Thread thread;
	boolean shouldRun = true;
	
	public FirePlacingThread() {
		thread = new Thread( ()->{
			while( shouldRun ) {
				try {
					//System.out.println( " Fire thread running!");
					if( !FireManager.fireQueue.isEmpty()) {
						int size = 20;
						for( int i = 0; i<size; i++) {						
							QueuedFire fire = FireManager.fireQueue.poll();
							if( fire != null) {
								FireManager.placeFire(fire.worldIn, fire.pos, fire.parentId, fire.fixBeacon);
							}						
						}
					}
					
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}		
		});
		thread.start();
	}

	public void joinThread() {
		try {
			shouldRun = false;
			this.thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
