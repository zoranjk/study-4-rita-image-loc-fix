package com.asist.asistmod.tile_entity;

import net.minecraftforge.fml.common.registry.GameRegistry;

public final class _TileEntities {

    public static void init() {
        
    	GameRegistry.registerTileEntity(BombBlockTileEntity.class, "bomb_block_tile_entity");
    	GameRegistry.registerTileEntity(BeaconTileEntity.class, "beacon_tile_entity");
        GameRegistry.registerTileEntity(FireCustomTileEntity.class, "fire_custom_tile_entity");

    }

}
