package com.asist.asistmod.datamodels.EnvironmentCreated.Single;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;

//THIS CLASS IS NOT CURRENTLY USED - WE USE ObjectStateChange(true) instead
public class EnvironmentCreatedSingleData {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public String triggering_entity = "NOT_SET";
	public ObjectStateChangeData obj = new ObjectStateChangeData(true);

}
