package com.asist.asistmod.datamodels.Perturbation;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class PerturbationDataModel {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public String type;
	public PerturbationAdditionalInfoFire additional_info;	
		
}


