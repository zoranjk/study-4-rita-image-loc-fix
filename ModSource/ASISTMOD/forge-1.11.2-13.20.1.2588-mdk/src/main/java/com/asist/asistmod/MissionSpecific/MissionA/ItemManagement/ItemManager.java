package com.asist.asistmod.MissionSpecific.MissionA.ItemManagement;

import net.minecraft.item.ItemStack;

public class ItemManager {	
	
	static ItemType[] wireCutters = {	
			ItemType.WIRECUTTERS_RED,
			ItemType.WIRECUTTERS_GREEN,
			ItemType.WIRECUTTERS_BLUE
	};
	
	public static int idCounter=0;
	
	public final String canDestroyBombs = "{CanDestroy:[" +
			"\"asistmod:block_bomb_standard\"," +
			" \"asistmod:block_bomb_volatile\"" +
			"]}";
	
	public final String canPlaceOn = "{CanPlaceOn:[" +
			"\"minecraft:grass\"," +
			"\"minecraft:stone\"," +
			"\"minecraft:sand\"," +
			"\"minecraft:logs\"," +
			"\"minecraft:end_bricks\"," +
			"\"minecraft:quartz_block\"," +
			" \"minecraft:grass_path\"" +
			"]}";
	
	public static ItemType getEnum(ItemStack item) {
		for(ItemType type : ItemType.values()) {
			if(item.getItem() == type.getItem()) { return type; }
		}
		return ItemType.NULL;
	}
	
	public static boolean isItem(ItemStack item, ItemType modItem) {
		return item.getItem() == modItem.getItem();
	}	
	
	
	public static boolean isWireCutters(ItemType item) {
		for(ItemType wireCutters : wireCutters) {
			if(item.equals(wireCutters)) { return true; }
		}
		return false;
	}
	
	public static char wirecutterEffectiveOnColor(ItemType t) {		
		if(t == ItemType.WIRECUTTERS_RED) {
			return ('R');
		}
		else if( t == ItemType.WIRECUTTERS_GREEN ) {
			return ('G');
		}
		else if( t == ItemType.WIRECUTTERS_BLUE ) {
			return ('B');
		}
		return 'Z';
	}
	
	public static ItemType getItemTypeFromName(String name) {
		
		ItemType[] values = ItemType.values();
		for(int i=0;i<values.length;i++){
			if( values[i].name().contentEquals(name)) {
				return values[i];
			}			
		}
		return null;
		
	}
	
	public static ItemType getItemTypeFromRegistryName(String registryName) {
		
		ItemType[] values = ItemType.values();
		for(int i=0;i<values.length;i++){
			if( values[i].getRegistryName().contentEquals(registryName)) {
				return values[i];
			}			
		}
		//System.out.println("Itemtype not matched with any known item, returning ItemType.NULL");
		return ItemType.NULL;
		
	}
	
	public String wirecutterItemString(String type, int amount) {
		
		return " asistmod:item_wirecutters_"+ type  +" "+amount+" 0 " + canDestroyBombs;
	}
	
	public String beaconItemString(String type, int amount) {
		
		return " asistmod:block_beacon_basic "+amount+" 0 " + canPlaceOn;
	}

	public String bombDisposerItemString(String type, int amount) {

		return " asistmod:bomb_disposer "+amount+" 0 " + canDestroyBombs;
	}

}
