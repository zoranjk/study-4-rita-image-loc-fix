package com.asist.asistmod.MissionSpecific.MissionA.FireManagement;

import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class QueuedFire {
	
	World worldIn;
	BlockPos pos;
	String parentId;
	boolean fixBeacon;
	
	public QueuedFire(World world, BlockPos pos, String parentFireId, boolean fixBeacon) {
	
	this.worldIn = world;
	this.pos = pos;
	this.parentId = parentFireId;
	this.fixBeacon =  fixBeacon;
	}
}
