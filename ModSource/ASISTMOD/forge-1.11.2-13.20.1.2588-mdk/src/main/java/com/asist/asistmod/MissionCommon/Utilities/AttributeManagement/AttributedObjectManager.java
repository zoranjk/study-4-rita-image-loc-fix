package com.asist.asistmod.MissionCommon.Utilities.AttributeManagement;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class AttributedObjectManager {
	
	
	// KEYS ARE THE STRING ID'S OF THE OBJECT
	public static ConcurrentMap<String, Map<String, String>> attributedObjects = new ConcurrentHashMap <String, Map<String,String>>();
	
	public static ConcurrentMap<String, HashMap <String, Map<String,String>>> attributedItemStacks = new ConcurrentHashMap <String, HashMap <String, Map<String,String>>>();
	
	//public static ConcurrentMap<String, Map<String, String>> attributedItems = new ConcurrentHashMap <String, Map<String,String>>();
	
	/* Takes in the previous and current attribute set of an attributable object, and returns the diff
	 */
	
	public static Map<String, String[]> returnAttributeDiff( Map<String, String> prevAttributes, Map<String, String> currAttributes) 
	{
		Map< String, String[]> out = new HashMap< String, String[]>();
		
		prevAttributes.forEach((k,v)->{
			
			String cAttr = currAttributes.get(k);
			
			if( !v.contentEquals(cAttr) ) {
				String[] change = {v,cAttr};
				out.put(k,change);				
			}
		});
		
		return out;
	}
	
	/* Updates the attributes of an attributed object
	 */
	
	public static Map<String,String[]> updateObjectAttributes(String id, Map<String, String> updates) {
		@SuppressWarnings("unchecked")
		Map<String, String> pAttributes = (Map<String, String>) ((HashMap<String,String>) attributedObjects.get(id)).clone();
		Map<String, String> cAttributes = attributedObjects.get(id);
		if ( cAttributes != null ) {
			updates.forEach((k,v)->{					
				if( cAttributes.containsKey(k) ) {
					cAttributes.put(k,v);
				}
			});	
		}
		return AttributedObjectManager.returnAttributeDiff(pAttributes, cAttributes);		
	}
	
	public static Map<String,String[]> updateItemAttributes(String itemStackId, String itemId, Map<String, String> updates) {
		@SuppressWarnings("unchecked")
		//Map<String, String> pAttributes = (Map<String, String>) ((HashMap<String,String>) attributedObjects.get(id)).clone();
		Map<String, String> pAttributes = (Map<String, String>) ( (HashMap<String,String>) attributedItemStacks.get(itemStackId).get(itemId)).clone();
		Map<String, String> cAttributes = attributedItemStacks.get(itemStackId).get(itemId);
		if ( cAttributes != null ) {
			updates.forEach((k,v)->{					
				if( cAttributes.containsKey(k) ) {
					cAttributes.put(k,v);
				}
			});	
		}
		return AttributedObjectManager.returnAttributeDiff(pAttributes, cAttributes);		
	}
	
	
	
	
	public static Map<String,String> getObjectAttributes(String id) {
				
		return attributedObjects.get(id);				
	}
	
	public static Map<String,String> getItemAttributes(String itemStackId, String itemId) {	

		HashMap<String, Map<String, String>> stack = attributedItemStacks.get(itemStackId);
		
		if(stack != null) {
			
			return attributedItemStacks.get(itemStackId).get(itemId);	
			
		}		
		
		//System.out.println("Encountered a null ItemStackId ... are you removing stale objects from the world?");
		return null;
		
					
	}
	
	
	public static void initAttributedObject(String id, Map<String, String> attributeSet){
		
		attributedObjects.put(id,attributeSet);
		
	}
	
	public static void initAttributedItemStack(String id){
		
		attributedItemStacks.put(id,new HashMap<String, Map<String, String>>());
		
	}

	public static void initAttributedItem(String itemStackId, String itemId, Map<String, String> attributeSet){		
	
		attributedItemStacks.get(itemStackId).put(itemId,attributeSet);
		
		////System.out.println( " ITEM ATTRIBUTES FOR : " + itemStackId + "_" +itemId);
		//attributedItemStacks.get(itemStackId).get(itemId).forEach( (k,v)->{
		//	//System.out.println(k);
		//	//System.out.println(v);
		//});
	
	}
	
	public static int returnNumberOfObjectsOfType( AttributedObjectType type) {
		
		String prefix = type.getPrefix();
		int count = 0;
		
		Set<String> keys = attributedObjects.keySet();
		Iterator<String> iterator = keys.iterator();
		
		while(iterator.hasNext()) {			
			String id = iterator.next();
			if( id != null && id.contains(prefix)) {
				count++;						
			}			
		}
		
		return count;
	}
	
		
	public static void printAllAttributedObjects() {
		
		StringBuilder sb = new StringBuilder();
		Set<String> keys = attributedObjects.keySet();
		Iterator<String> iterator = keys.iterator();
		
		while(iterator.hasNext()) {
			String key = iterator.next();
			sb.append(key);
			sb.append(": { \n");
			Map<String, String> attributes = attributedObjects.get( key );
			Set<String> keys0 = attributes.keySet();
			Iterator<String> iterator0 = keys0.iterator();
			while(iterator0.hasNext()) {
				String key0 = iterator0.next();
				sb.append(key0);
				sb.append(": ");
				sb.append(attributes.get(key0));
				sb.append(",\n");
			}
			sb.append(" }");
		}
		
		//System.out.println(sb.toString());
		
	}
	
	public static void printAllAttributedItems() {
		
		StringBuilder sb = new StringBuilder();
		Set<String> keys = attributedObjects.keySet();
		Iterator<String> iterator = keys.iterator();
		
		while(iterator.hasNext()) {
			String key = iterator.next();
			if(key.contains("ITEM")) {
				sb.append(key);
				sb.append(": { \n");
				Map<String, String> attributes = attributedObjects.get( key );
				Set<String> keys0 = attributes.keySet();
				Iterator<String> iterator0 = keys0.iterator();
				while(iterator0.hasNext()) {
					String key0 = iterator0.next();
					sb.append("\t");
					sb.append(key0);
					sb.append(": ");
					sb.append(attributes.get(key0));
					sb.append(",\n");
				}
				sb.append(" }\n");
			}
			
		}
		
		//System.out.println(sb.toString());
		
	}
	
	
}
