package com.asist.asistmod.network.messages;

import java.util.Arrays;

import com.asist.asistmod.MissionCommon.InterventionManagement.ChatInterventions.ChatInterventionManager;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.datamodels.AgentChatIntervention.AgentChatInterventionModel;
import com.asist.asistmod.network.MessagePacket;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;

public class AgentInterventionChatPacket extends MessagePacket<AgentInterventionChatPacket> {
	
	
	public String interventionId;
	public int interventionIdLength;
	public String agentId;
	public int agentIdLength;
	public String interventionContent;
	public int interventionContentLength;
	public String[] responseOptions;
	public int responseOptionsLength = 0;
	public int[] optionLengths;

	
	public AgentInterventionChatPacket() {}
	
	public AgentInterventionChatPacket( AgentChatInterventionModel agentInterventionModel) {
		
		interventionId = agentInterventionModel.data.id;
		interventionIdLength = interventionId.length();
		agentId = agentInterventionModel.msg.source;
		agentIdLength = agentId.length();
		interventionContent = agentInterventionModel.data.content;
		interventionContentLength = interventionContent.length();
		responseOptions = agentInterventionModel.data.response_options;
		if(responseOptions != null) {
			responseOptionsLength = responseOptions.length;
			optionLengths = new int[responseOptionsLength];
			for(int i=0; i<responseOptionsLength; i++) {
				optionLengths[i]=responseOptions[i].length();
			}
		}
		else {
			responseOptionsLength = 0;
			optionLengths = new int[0];			
		}
		//System.out.println("Intervention ID : " + interventionId);
		//System.out.println("Intervention ID Length : " + interventionIdLength);
		//System.out.println("Agent ID : " + agentId);
		//System.out.println("Agent ID Length : " + agentIdLength);
		//System.out.println("intervention Content : " + interventionContent);
		//System.out.println("intervention Content Length : " + interventionContentLength);
		//System.out.println("Response Options : " + Arrays.toString(responseOptions));
		//System.out.println("Response Options Length : " + responseOptionsLength);
		//System.out.println("Options Lengths : " + Arrays.toString(optionLengths));
	}

	// NEEDS TO BE IN SAME ORDER AS toBytes
	@Override
	public void fromBytes(ByteBuf buf) {
		interventionIdLength = buf.readInt();
		StringBuilder sb = new StringBuilder();
		for(int i=0; i<interventionIdLength; i++) {
			sb.append(buf.readChar());
		}
		interventionId = sb.toString();
		sb.setLength(0);
		agentIdLength = buf.readInt();		
		for(int i=0; i<agentIdLength; i++) {
			sb.append(buf.readChar());
		}
		agentId = sb.toString();
		sb.setLength(0);
		interventionContentLength = buf.readInt();		
		for(int i=0; i<interventionContentLength; i++) {
			sb.append(buf.readChar());
		}
		interventionContent = sb.toString();
		sb.setLength(0);
		
		//options
		responseOptionsLength = buf.readInt();
		responseOptions = new String[responseOptionsLength];
		optionLengths = new int[responseOptionsLength];
		for(int i=0; i<responseOptionsLength; i++) {
			optionLengths[i]=buf.readInt();
		}
		for(int i=0;i<responseOptionsLength;i++) {
			for(int j=0; j<optionLengths[i]; j++) {
				sb.append(buf.readChar());
			}
			responseOptions[i]=sb.toString();
			sb.setLength(0);
		}
	}

	// NEEDS TO BE IN SAME ORDER AS fromBytes
	@Override
	public void toBytes(ByteBuf buf) {
		buf.writeInt(interventionIdLength);		
		for(int i=0;i<interventionIdLength;i++) {
			buf.writeChar(interventionId.charAt(i));
		}
		buf.writeInt(agentIdLength);		
		for(int i=0;i<agentIdLength;i++) {
			buf.writeChar(agentId.charAt(i));
		}
		buf.writeInt(interventionContentLength);		
		for(int i=0;i<interventionContentLength;i++) {
			buf.writeChar(interventionContent.charAt(i));
		}		
		// RESPONSE OPTIONS
		buf.writeInt(responseOptionsLength);
		for(int i=0;i<responseOptionsLength;i++) {
			buf.writeInt(optionLengths[i]);
		}		
		for(int i=0; i<responseOptionsLength; i++) {
			//System.out.println("Length :" + responseOptionsLength);
			//System.out.println("index : " + i);
			for(int j=0; j<responseOptions[i].length();j++) {
				buf.writeChar(responseOptions[i].charAt(j));
			}			
		}		
	}

	@Override
	public void handleClientSide(AgentInterventionChatPacket message, EntityPlayer player) {
		message.interventionContent = ChatInterventionManager.parseInterventionMessage(message.interventionContent);
		//System.out.println("AgentInterventionChatPacket: Received message");
		//System.out.println(message.interventionId);
		//System.out.println(message.agentId);
		//System.out.println(message.interventionContent);
		if(message.responseOptions != null ) {
			//System.out.println(Arrays.toString(message.responseOptions));
		}

		RenderOverlayEventHandler.currentIntervention = message;
		
	}

	@Override
	public void handleServerSide(AgentInterventionChatPacket message, EntityPlayer player) {					
					
	}

}
