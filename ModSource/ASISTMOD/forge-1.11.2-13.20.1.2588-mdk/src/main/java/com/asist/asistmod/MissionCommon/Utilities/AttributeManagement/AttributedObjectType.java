package com.asist.asistmod.MissionCommon.Utilities.AttributeManagement;

import java.util.Map;

public enum AttributedObjectType {
	
	BOMB( "BOMB", AttributeSetCreator.createBombAttributeSet("false","NOT_SET","0","INACTIVE", null )),
	BOMB_BEACON( "ITEM", AttributeSetCreator.createBombBeaconAttributeSet("", 1)),	
	HAZARD_BEACON( "ITEM", AttributeSetCreator.createHazardBeaconAttributeSet("", 1)),
	GENERIC_ITEM( "ITEM", AttributeSetCreator.createGenericItemAttributeSet(1)),
	;
	
	Map<String,String> defaultAttributes;
	String idPrefix;
	
	AttributedObjectType( String idPrefix, Map<String,String> defaultAttributes ) {
		this.defaultAttributes = defaultAttributes;
		this.idPrefix = idPrefix;
	}
	
	public String getPrefix() {
		return this.idPrefix; 
	}
	
	public Map<String,String> getDefaultAttributes() {
		return this.defaultAttributes;
	}

}
