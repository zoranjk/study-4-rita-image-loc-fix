package com.asist.asistmod.block.beacons;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectType;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.CommunicationEnvironment.CommunicationEnvironmentModel;
import com.asist.asistmod.datamodels.EnvironmentCreated.Single.EnvironmentCreatedSingleModel;
import com.asist.asistmod.datamodels.ItemStateChange.ItemStateChangeModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.BeaconTileEntity;
import com.asist.asistmod.tile_entity.BombBlockTileEntity;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockBeaconBomb extends BlockBeaconBasic {
    public BlockBeaconBomb(Material materialIn) {
        super(materialIn);
    }
    
    /**
     * Called by ItemBlocks after a block is set in the world, to allow post-place logic
     */
   
    // This function extends the standard BlockBeaconBasic to send a communication environment message from this beacon to the clientmap, about the bomb specs it was associated with
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    {
    	// Handle all the normal beacon functionality
    	super.onBlockPlacedBy(worldIn, pos, state, placer, stack);    	
    	
    	if(!worldIn.isRemote) {
    		// handles sending the bomb information to the clientmap
        	String beaconId = ((BeaconTileEntity) worldIn.getTileEntity(pos)).getBeaconId();
        	
        	////System.out.println("Placing a Bomb Beacon @ : " + pos.toString() );
        	
        	Player player = PlayerManager.getPlayerByName(placer.getName());
        	
        	////System.out.println("BOMB ID : " + player.lastBombId );
        	////System.out.println("BOMB TYPE : " + player.lastBombType );
        	////System.out.println("BOMB POS : " + player.lastBombPosition );
        	
        	BombBlockTileEntity te = (BombBlockTileEntity)worldIn.getTileEntity( player.lastBombPosition);
        	
        	String stringSeq = Arrays.toString(te.getDefuseSequence());		
    		
    		Iterator<Player> iterator = PlayerManager.players.iterator();
    		String[] pids = new String[PlayerManager.players.size()];
    		int i = 0;
    		while(iterator.hasNext()) {
    			pids[i] = iterator.next().participant_id;
    			i++;
    		}
    		
    		StringBuilder sb = new StringBuilder();
    		
    		String bombId = te.getBombId();    		
    		String chainedId = te.getChainedId();
    		String fuseStartTime = Integer.toString(te.getFuseStartTime());
    		
    		sb.append("BOMB_ID: ");
    		sb.append(bombId);
    		sb.append("\n");
    		sb.append("CHAINED_ID: ");
    		sb.append(chainedId);
    		sb.append("\n");
    		sb.append("FUSE_START_MINUTE: ");
    		sb.append(fuseStartTime);
    		sb.append("\n");
    		sb.append("SEQUENCE: ");
    		sb.append(stringSeq);
    		
    		String message = sb.toString();
    		
    		Map<String,String> additionalInfo = new HashMap<String,String>();
    		additionalInfo.put("bomb_id", bombId);
    		additionalInfo.put("chained_id", chainedId);
    		additionalInfo.put("remaining_sequence", stringSeq);
    		additionalInfo.put("fuse_start_minute", fuseStartTime);
    		
    		// communicates to all players
    		CommunicationEnvironmentModel comEnvModel = new CommunicationEnvironmentModel(
    				beaconId,
    				this.getRegistryName().toString().split(":")[1],
    				pids,
    				message,
    				pos.getX(),
    				pos.getY(),
    				pos.getZ(),
    				additionalInfo);
    		InternalMqttClient.publish(comEnvModel.toJsonString(), comEnvModel.getTopic());
    		
    		String participant_id = player.participant_id;
    		String itemName = ItemType.getItemTypeByRegistryName( stack.getItem().getRegistryName().toString() ).getName();
    		String itemStackId = stack.getTagCompound().getTag("item_stack_id").toString();
        	itemStackId = itemStackId.replaceAll("\"", "");
        	String itemId = AttributedObjectType.BOMB_BEACON.getPrefix()+Integer.toString(stack.getCount());
    		String idForMessage = itemStackId+'_'+itemId;
        	//System.out.println( "ID from onBlockPacedBy  ... item id :" + idForMessage);
    		
    		Map<String,String> currAttr = AttributedObjectManager.getItemAttributes(itemStackId,itemId);    	
    		
			int usesRemaining = Integer.parseInt(currAttr.get("uses_remaining"));
			////System.out.println("PlayerInteractionEvent:uses_remaining_pre " +usesRemaining );
			usesRemaining--;
			////System.out.println("PlayerInteractionEvent:uses_remaining_pre " +usesRemaining );
			Map<String,String> newAttr = new HashMap<String,String>();
			newAttr.put("uses_remaining", Integer.toString(usesRemaining) );
			newAttr.put("bomb_id", bombId );
			Map<String, String[]> changedAttributes = AttributedObjectManager.updateItemAttributes(itemStackId, itemId, newAttr);
			
    		
			// for all single use items
			ItemStateChangeModel itemStateChangeModel = new ItemStateChangeModel(idForMessage,itemName,participant_id,changedAttributes,currAttr);
			InternalMqttClient.publish(itemStateChangeModel.toJsonString(), itemStateChangeModel.getTopic(), player.playerName);
			
			
    	}
    	   	
    }
    

}
