package com.asist.asistmod;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager.StateChangeOutcome;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerDisconnectEventHandler;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.RegionManagement.RegionManager;
import com.asist.asistmod.block._Blocks;
import com.asist.asistmod.commands.CommandGenerate;
import com.asist.asistmod.commands.CommandScan;
import com.asist.asistmod.commands.CommandScanBombSpawns;
import com.asist.asistmod.datamodels.Deprecated.Observation.ObservationModel;
import com.asist.asistmod.datamodels.MissionState.MissionStateModel;
import com.asist.asistmod.datamodels.ModSettings.ModSettings_v3;
import com.asist.asistmod.datamodels.PlayerSprinting.PlayerSprintingModel;
import com.asist.asistmod.datamodels.PlayerState.PlayerStateModel;
import com.asist.asistmod.datamodels.PlayerSwinging.PlayerSwingingModel;
import com.asist.asistmod.datamodels.TrialInfo.TrialInfoModel;
import com.asist.asistmod.eventHandlers.AttackEntityEventHandler;
import com.asist.asistmod.eventHandlers.BlockBreakEventHandler;
import com.asist.asistmod.eventHandlers.ChatEventHandler;
import com.asist.asistmod.eventHandlers.ClientKeyboardInputEventHandler;
import com.asist.asistmod.eventHandlers.CommandBlockInterceptEventHandler;
import com.asist.asistmod.eventHandlers.CommandEventHandler;
import com.asist.asistmod.eventHandlers.EntityItemDropEventHandler;
import com.asist.asistmod.eventHandlers.EntityItemPickupEventHandler;
import com.asist.asistmod.eventHandlers.ItemTossEventHandler;
import com.asist.asistmod.eventHandlers.LivingDeathEventHandler;
import com.asist.asistmod.eventHandlers.LivingEntityAttackedHandler;
import com.asist.asistmod.eventHandlers.LivingEntityUseItemEventHandler;
import com.asist.asistmod.eventHandlers.LivingEquipmentChangeEventHandler;
import com.asist.asistmod.eventHandlers.LivingHurtEventHandler;
import com.asist.asistmod.eventHandlers.LivingJumpEventHandler;
import com.asist.asistmod.eventHandlers.PlayerEventHandler;
import com.asist.asistmod.eventHandlers.PlayerInteractionEventHandler;
import com.asist.asistmod.eventHandlers.PlayerJoinEventHandler;
import com.asist.asistmod.eventHandlers.TickEventHandler;
import com.asist.asistmod.eventHandlers.ToolBreakEventHandler;
import com.asist.asistmod.eventHandlers.WorldLoadEventHandler;
import com.asist.asistmod.item._Items;
import com.asist.asistmod.missionhelpers_v2.RoomManager.RoomManager;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.tile_entity._TileEntities;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

import net.minecraft.command.ICommandManager;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLLog;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppedEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = AsistMod.MODID, version = AsistMod.VERSION)
public class AsistMod {
	
	public static final String MODID = "asistmod";
	public static final String VERSION = "4.3.1";
	
	public static Side side = null;
	
	// Why do we declare this here?
	public static final int ENTITY_SHOPKEEPER = 120; //id associated with this entity

	public Thread thread;

	public static boolean executing = true;

	public int count = 0;

	public int obCount = 0;
	
	// has to be MinecraftServer because DedicatedServer does not exist on Client
	public static MinecraftServer server;

	public static ModSettings_v3 modSettings;
	
	public static TrialInfoModel currentTrialInfo;
	
	public static Gson gson = new Gson();

	@Instance
	public static AsistMod instance;

	@EventHandler
	public void preInit(FMLPreInitializationEvent e) throws Exception {	
		
		//System.out.println("Test");

		// LOAD CUSTOM BLOCKS AND ITEMS INTO THE WORLD
		// //////////////////////////////////////////

		_Blocks.commonPreInit();
		_Items.commonPreInit();				
		_TileEntities.init();		
		NetworkHandler.init();
		
		
		try {
			
			FileReader fileReader  = new FileReader("./mods/ModSettings_v3.json");
			JsonReader jr = new JsonReader(fileReader);
			jr.setLenient(true);			
			Gson gson = new Gson();
			modSettings = gson.fromJson(jr, ModSettings_v3.class);		
				
			if (modSettings != null && e.getSide() == Side.SERVER) {					

				side = Side.SERVER;
				//System.out.println("______________________________Reading from settings file________________________________________");
				
				//System.out.println("Modsettings:clientSideMapBuilder = " + modSettings.clientSideMapBuilder);

				//System.out.println("Modsettings:mqtthost = " + modSettings.mqtthost);

				//System.out.println("Modsettings:observationInterval = " + modSettings.observationInterval);
				
				
				
				//System.out.println("Modsettings:authorizePlayers = " + modSettings.authorizePlayers);
				
				
				
				//System.out.println("Modsettings:time_in_field = " + modSettings.time_in_field.minute + " : " + modSettings.time_in_field.second);
				
				
				//System.out.println("Modsettings:observerNames = " + Arrays.deepToString(modSettings.observerNames));
				
				//System.out.println("Modsettings:map_boundaries = " + Arrays.toString(modSettings.map_boundaries.keySet().toArray()));
				
				//System.out.println("Modsettings:sector_boundaries = " + Arrays.toString(modSettings.sector_boundaries));
				
				//System.out.println("Modsettings:sector_gates = " + Arrays.toString(modSettings.sector_gates));
				
				//System.out.println("_______________________________________________________________________________________________");			
				
				// GIVE INTERNAL MQTTCLIENT ABILITY TO START WITHOUT REF TO SERVER - THEN GIVE IT THE SERVER IN POSTINIT
				RegionManager.init();
				InternalMqttClient.init(modSettings);
				
				
				// WE ARE NOT DOING THIS YET (SPIRAL 2) 
				// ONLY NECESSARY IF TRACKING VICTIM LOCATION ALL THE TIME - WOULD NEED COMPLETE ROOM DEFS
				if ( modSettings.useRoomDefinitionFile ) {
					
					RoomManager.readInRoomDefs(modSettings.roomDefinitionFile);
					
					RoomManager.printRoomDefs();
					
				}
				

				try {
					jr.close();
				} catch (IOException error) {				
					error.printStackTrace();
				}				
			}
			
			else if(e.getSide() == Side.CLIENT) {
				side = Side.CLIENT;
				if( modSettings != null ){
					
					//System.out.println("--------------------------CLIENTSIDE MODSETTINGS------------------------------------------");			
					
					//System.out.println("--------------------------"+ ( modSettings.clientSideMapBuilder ) +"------------------------------------------");
					
					if (modSettings !=null && modSettings.clientSideMapBuilder) {				
						
						//System.out.println("LOADING ClientSideMapBuilder!");
						
						WorldLoadEventHandler worldLoadEventHandler = new WorldLoadEventHandler( modSettings );
						MinecraftForge.EVENT_BUS.register(worldLoadEventHandler);
					}				
								
				}
			}
			
		}		
		catch(FileNotFoundException error){				
			//System.out.println("----------------------ModSettings.json is not present in the Mods folder. This is fine if you are a client.-----------------");
			//System.out.println("----------------------If you would like to use the clientSideMapBuilder option, place the ModSettings.json file in the mods folder.-----------------");
		}
		
		InternalMqttClient.publish("{\"loaded\":\"33\"}", "status/minecraft/loading");
		

		/////////////////////////////////////////////////////////////////////////////////////////
	}	

	@EventHandler
	public void init(FMLInitializationEvent e) {
		
		InternalMqttClient.publish("{\"loaded\":\"66\"}", "status/minecraft/loading");
		
	}

	@EventHandler
	public void postInit(FMLPostInitializationEvent e) throws Exception {
		
		// NON MODSETTINGS RELATED CLIENTSIDE INITIALIZATION		
		if( e.getSide() == Side.CLIENT ){
			
			_Items.clientPostInit();
			MinecraftForge.EVENT_BUS.register( new RenderOverlayEventHandler() );
			MinecraftForge.EVENT_BUS.register( new ClientKeyboardInputEventHandler() );			
			MinecraftForge.EVENT_BUS.register(new LivingDeathEventHandler());	
			//MinecraftForge.EVENT_BUS.register( new ClientRenderLivingEventHandler() );
			
		}
		
		InternalMqttClient.publish("{\"loaded\":\"90\"}", "status/minecraft/loading");

	}
	
	
	@EventHandler
	public void serverStoppingHandler(FMLServerStoppingEvent event) {
		
		//server.commandManager.executeCommand(server, "debug stop");		
		
	}
	
	
	@EventHandler
	public void serverStoppedHandler(FMLServerStoppedEvent event) {
		
		System.out.println(event.getEventType() + "------------------------------------------------------- SERVER HAS STOPPED ------------------------------------------");
		System.out.println("Throw Away Placeholder Instance ? " + InternalMqttClient.throwAwayInstance);
		if( !InternalMqttClient.throwAwayInstance) {
			
			if(InternalMqttClient.blockLoadingSuccessful) {
				System.out.println("Block Loading Successful");
				StateChangeOutcome stateChangeOutcome = null;
				boolean crash = false;
				if( MissionManager.missionStoppedSuccessfully) {
					// DO NOTHING THIS WAS A SUCCESSFUL RUN
					System.out.println("Successful Run.");
				}
				else if ( InternalMqttClient.prematureMissionStopNonCrash) {
					
					if( InternalMqttClient.prematureMissionStopReason.compareTo("DISSOLVE_TEAM_CLIENTMAP_DISCONNECT")==0) {
						System.out.println("ClientMap Disconnect Error.");
						stateChangeOutcome = MissionManager.StateChangeOutcome.MISSION_STOP_CLIENTMAP_DISCONNECT;
						 
					}
					else if( InternalMqttClient.prematureMissionStopReason.compareTo("DISSOLVE_TEAM_PLAYER_LEFT")==0) {
						System.out.println("Player Left Game Error");
						stateChangeOutcome = MissionManager.StateChangeOutcome.MISSION_STOP_PLAYER_LEFT;
						 
					 }
					
					MissionStateModel model = new MissionStateModel(AsistMod.currentTrialInfo.mission_name,"Stop",stateChangeOutcome);				
					model.publish();
					
				}
				// CRASH IS TRUE
				else {
					System.out.println("MINECRAFT CRASH.");
					crash = true;
					stateChangeOutcome = MissionManager.StateChangeOutcome.MISSION_STOP_SERVER_CRASHED;
					InternalMqttClient.publish("{}", "status/minecraft/crash");
					MissionStateModel model = new MissionStateModel(AsistMod.currentTrialInfo.mission_name,"Stop",stateChangeOutcome);				
					model.publish();
				}			
				
				InternalMqttClient.publish("{\"crash\":"+crash+"}", "status/minecraft/stopped");
				
			}
			else {
				// This will cycle the server again
				//System.out.println("InternalMqttClient.blockLoadingSuccessful : " + InternalMqttClient.blockLoadingSuccessful);
				InternalMqttClient.publish("{\"error\":"+!InternalMqttClient.blockLoadingSuccessful+"}", "status/minecraft/block_loading");
			}
			
		}
		
		
	}
	
	

	@EventHandler
	public void serverStarting(FMLServerStartingEvent event) {
			
		
		
		if (event.getSide() == Side.SERVER) {
			
			FMLLog.log.info(
					"______________________________FMLServerStartingEvent RECEIVED ON SERVER SIDE________________________________________");

			FMLLog.log.info("Working Directory = " + System.getProperty("user.dir"));

			server = event.getServer();
			
			//server.enableProfiling();
			
			// INITIALIZE CUSTOM CLIENT --> SERVER MESSAGING SYSTEM				
			
			//System.out.println( "STARTING LEVEL : " + server.worlds[0].getWorldInfo().getWorldName() );
			
			// Actual dedicated server object ... the global server object can be passed to
			// clients, but this cannot
			// and has special dedicated capabilities

			ICommandManager commandManager = server.commandManager;

			String[] serverCommands = {
					"gamerule logAdminCommands false",
					"gamerule commandBlockOutput false",
					"gamerule sendCommandFeedback false",
					"gamerule naturalRegeneration false",
					"save-off",
					"gamerule doDaylightCycle false",
					"time set 4000",
					"defaultgamemode a",
					// 2 lines below hide player names above the avatar
					// remember players need to be added to asist team when they login
					// "scoreboard teams add asist asist",
					// "scoreboard teams option asist nametagVisibility never"
			};
			
			for(String c : serverCommands) {
				commandManager.executeCommand(server,c);
			}		

			for (int i = 0; i < server.worlds.length; i++) {

				// don't save changes after each experiment
				server.worlds[i].disableLevelSaving = true;
			}
					
			
			InternalMqttClient.linkServer(server);	
			
			// FETCH ID's FROM CONTROL GUI
			System.out.println("Requesting Trial Info");
			
			InternalMqttClient.publish("{}", "control/request/getTrialInfo");
			
			
			
			// MAPBLOCKS TESTING SECTION ----------------------------------------------
			
			
			// USE MAPINFO AND MAPBLOCK LINES BELOW ONLY WHEN DEBUGGING OUTSIDE OF THE DOCKER TESTBED, OTHERWISE
			// COMMENT OUT. WHEN IN TESTBED THIS IS DONE IN InternalMqttClient.init() WHICH PULLS CONFIG VIA MQTT FROM CONTROL GUI.
			
			// MapInfoManager.addMapInfoFromFile("./mods/MapInfo_Hard.csv");
			// MapBlockManager.addBlocksFromFile("./mods/MapBlocks_Adapt_Jan12.csv", server.worlds[0]);
			
			// END MAPBLOCKS TESTING SECTION ---------------------------------------------

			
			///////////////////////////////////////////////////////
			
			
			// REGISTER EVENT HANDLERS ON FORGE	EVENT BUS		
			
			// Register the Chat EventHandler on the Forge message bus
			ChatEventHandler chatEventHandler = new ChatEventHandler();
			MinecraftForge.EVENT_BUS.register(chatEventHandler);

			// Register command EventHandler on the Forge message bus

			CommandEventHandler commandEventHandler = new CommandEventHandler();
			MinecraftForge.EVENT_BUS.register(commandEventHandler);

			// Register VictimState EventHandler on the Forge message bus

			PlayerInteractionEventHandler playerInteractionEventHandler = new PlayerInteractionEventHandler(server,modSettings);
			MinecraftForge.EVENT_BUS.register(playerInteractionEventHandler);
			
			AttackEntityEventHandler attackEntityEventHandler = new AttackEntityEventHandler(server,modSettings);
			MinecraftForge.EVENT_BUS.register(attackEntityEventHandler);

			// Register Block EventHandler on the Forge message bus

			BlockBreakEventHandler blockEventHandler = new BlockBreakEventHandler( server);
			MinecraftForge.EVENT_BUS.register(blockEventHandler);

			// Register ToolBreak EventHandler on the Forge message bus
			ToolBreakEventHandler toolBreakEventHandler = new ToolBreakEventHandler(server);
			MinecraftForge.EVENT_BUS.register(toolBreakEventHandler);
			
			// Register CommandBlockIntercept EventHandler on the Forge message bus

			CommandBlockInterceptEventHandler commandBlockInterceptEventHandler = new CommandBlockInterceptEventHandler( modSettings );
			MinecraftForge.EVENT_BUS.register(commandBlockInterceptEventHandler);

			// Register PlayerJoinEventHandler on the Forge message bus

			PlayerJoinEventHandler playerJoinEventHandler = new PlayerJoinEventHandler(server);
			MinecraftForge.EVENT_BUS.register(playerJoinEventHandler);

			// Register Player item pickup event handler on the Forge message bus
			EntityItemPickupEventHandler entityItemPickupEventHandler = new EntityItemPickupEventHandler(server);
			MinecraftForge.EVENT_BUS.register(entityItemPickupEventHandler);			

			// Register Player item drop event handler on the Forge message bus
			EntityItemDropEventHandler entityItemDropEventHandler = new EntityItemDropEventHandler( server);
			MinecraftForge.EVENT_BUS.register(entityItemDropEventHandler);			

			// Register ItemToss EventHandler on the Forge message bus
			ItemTossEventHandler itemTossEventHandler = new ItemTossEventHandler(server);
			MinecraftForge.EVENT_BUS.register(itemTossEventHandler);
			
			// Register Player item used event handler on the Forge message bus
			LivingEntityUseItemEventHandler livingEntityUseItemEventHandler = new LivingEntityUseItemEventHandler(server);
			MinecraftForge.EVENT_BUS.register(livingEntityUseItemEventHandler);			

			// Register Player jumped event handler on the Forge message bus
			LivingJumpEventHandler livingJumpEventHandler = new LivingJumpEventHandler( server);
			MinecraftForge.EVENT_BUS.register(livingJumpEventHandler);	
			
			LivingHurtEventHandler livingHurtEventHandler = new LivingHurtEventHandler( );
			MinecraftForge.EVENT_BUS.register(livingHurtEventHandler);	
			
			LivingEntityAttackedHandler livingEntityAttackedHandler = new LivingEntityAttackedHandler( );
			MinecraftForge.EVENT_BUS.register(livingEntityAttackedHandler);	
			
			LivingDeathEventHandler livingDeathEventHandler = new LivingDeathEventHandler( );
			MinecraftForge.EVENT_BUS.register(livingDeathEventHandler);	
			
			PlayerEventHandler playerEventHandler = new PlayerEventHandler( );
			MinecraftForge.EVENT_BUS.register(playerEventHandler);
			
			LivingEquipmentChangeEventHandler livingEquipmentChangeHandler = new LivingEquipmentChangeEventHandler();
			MinecraftForge.EVENT_BUS.register(livingEquipmentChangeHandler);
			// Register Player jumped event handler on the Forge message bus
			TickEventHandler tickEventHandler = new TickEventHandler( );
			MinecraftForge.EVENT_BUS.register(tickEventHandler);

			//Register Disconnect timer Handler
			PlayerDisconnectEventHandler playerDisconnectTimer = new PlayerDisconnectEventHandler();
			MinecraftForge.EVENT_BUS.register(playerDisconnectTimer);

			
			//REGISTER  Michael's Custom Server Commands
			event.registerServerCommand(new CommandScan(new BlockPos(0, 50, 0), new BlockPos(50, 64, 150), true));
			event.registerServerCommand(new CommandScanBombSpawns(new BlockPos(0, 50, 0), new BlockPos(50, 64, 150), true));
			event.registerServerCommand(new CommandGenerate());
			
			//server.commandManager.executeCommand(server, "debug start");
			
			

			//////////////////////////////////////////////////////////////////////////////////////////
			
			// on the first Server Starting event - because there are 3 for some reason
			
			if (count == 0) {

				count++;

				thread = new Thread() {
					
					public void run() {
						
											

						// holds the previous position of all players to get motionX/Y/Z bug working
						ConcurrentHashMap<String, double[]> lastPosMap = new ConcurrentHashMap<String, double[]>();

						// holds the previous sprinting state for all players
						ConcurrentHashMap<String, Boolean> lastSprintingMap = new ConcurrentHashMap<String, Boolean>();

						// holds the previous swinging state for all players
						ConcurrentHashMap<String, Boolean> lastSwingingMap = new ConcurrentHashMap<String, Boolean>();

						
						while (executing) {
							
							try {

								Thread.sleep(modSettings.observationInterval);

							} catch (InterruptedException e) {

								e.printStackTrace();
							}
																						

							for(int i = 0; i < PlayerManager.players.size(); i++) {
								
								EntityPlayer player = PlayerManager.players.get(i).player;
								
								String name = player.getName();
								
								String pid = InternalMqttClient.name_to_pid(name);
									
								obCount++;

																		
								double currX = player.posX;
								double currY = player.posY;
								double currZ = player.posZ;

								boolean lastSprinting = false;
								boolean lastSwinging = false;
								boolean isSprinting = player.isSprinting();
								boolean isSwinging = player.isSwingInProgress;

								
								// add name to map if doesn't yet exist
								if (!lastPosMap.containsKey(name)) {
									lastPosMap.put(name, new double[] { player.posX, player.posY, player.posZ });
								}
								

								if (!lastSprintingMap.containsKey(name)) {
									lastSprintingMap.put(name, isSprinting);
								}									
								else {
									lastSprinting = (boolean) lastSprintingMap.get(name);										
								}

								if (!lastSwingingMap.containsKey(name)) {
									lastSwingingMap.put(name, isSwinging);
								}									
								else {
									lastSwinging = (boolean) lastSwingingMap.get(name);										
								}
								
								
								/*-------------------PLAYER STATE     ----------------*/
								// pull last position to compare to current to see if moving
								double[] lastPosArray = (double[]) (lastPosMap.get(name));
								
								/*-------------------START DEPRECATED----------------*/
								ObservationModel observation = new ObservationModel();
								// data
								observation.data.observation_number = obCount;								
								//observation.data.playername = name;
								observation.data.participant_id = pid;
								observation.data.yaw = player.rotationYaw;
								observation.data.x = currX;
								observation.data.y = currY;
								observation.data.z = currZ;
								observation.data.pitch = player.rotationPitch;
								observation.data.id = player.getUniqueID().toString();
								observation.data.motion_x = currX - lastPosArray[0];
								observation.data.motion_y = currY - lastPosArray[1];
								observation.data.motion_z = currZ - lastPosArray[2];
								observation.data.life = player.getHealth();
								InternalMqttClient.publish(observation.toJsonString(), "observations/state");
								/*-------------------END DEPRECATED----------------*/

								PlayerStateModel playerPosition = new PlayerStateModel();
								// data
								playerPosition.data.obs_id = obCount;
								playerPosition.data.participant_id = pid;
								playerPosition.data.yaw = player.rotationYaw;
								playerPosition.data.x = currX;
								playerPosition.data.y = currY;
								playerPosition.data.z = currZ;
								playerPosition.data.pitch = player.rotationPitch;
								playerPosition.data.motion_x = currX - lastPosArray[0];
								playerPosition.data.motion_y = currY - lastPosArray[1];
								playerPosition.data.motion_z = currZ - lastPosArray[2];
								

								InternalMqttClient.publish(playerPosition.toJsonString(), playerPosition.getTopic() );
								
								// set last position for next loop comparison
								lastPosMap.replace(name, new double[] { currX, currY, currZ });
								/*-------------------PLAYER STATE     ----------------*/
								
								// Sprinting
								if (isSprinting != lastSprinting) {									
									PlayerSprintingModel playerSprintingModel = new PlayerSprintingModel();
									playerSprintingModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
									playerSprintingModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
									//playerSprintingModel.data.playername = name;
									playerSprintingModel.data.participant_id = pid;
									playerSprintingModel.data.sprinting = isSprinting;
									InternalMqttClient.publish(playerSprintingModel.toJsonString(), "observations/events/player/sprinting",name);
								}
								lastSprintingMap.replace(name, isSprinting);

								// Swinging
								if (isSwinging != lastSwinging) {									
									PlayerSwingingModel playerSwingingModel = new PlayerSwingingModel();
									playerSwingingModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
									playerSwingingModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
									//playerSwingingModel.data.playername = name;
									playerSwingingModel.data.participant_id = pid;
									playerSwingingModel.data.swinging = isSwinging;
									InternalMqttClient.publish(playerSwingingModel.toJsonString(), "observations/events/player/swinging",name);
								}
								lastSwingingMap.replace(name, isSwinging);

							}													
							
						} // END WHILE
								
					}// END RUN

				}; // End Thread Definition

				thread.start();
				//server.theProfiler.endSection();

			} // END IF COUNT

		} // End IF SERVER

	}// End serverStarting
	
	public static void sendTellRawMessage(String playerName, String tellrawArrayString) {
		
		AsistMod.server.commandManager.executeCommand(AsistMod.server, "tellraw " + playerName +" "+ tellrawArrayString);
		
	}
	public static boolean isObserver(String name) {
		
		String[] observerNamesModSettings = InternalMqttClient.modSettings.observerNames;
		
		// CHECK MODSETTINGS
		if(observerNamesModSettings != null) {
			for(String n : observerNamesModSettings) {
				if( name.contentEquals(n) ) {
					return true;
				}
			}
		}
		
		List<String> observerNamesSetAtRuntime = InternalMqttClient.currentTrialInfo.observer_info;
		
		// CHECK FROM GUI
		if(observerNamesSetAtRuntime != null && observerNamesSetAtRuntime.contains(name)) return true;
				
		return false;
	}
	public static void playSound(World world, String soundName, BlockPos pos){
		world.playSound(
				null,
				pos.getX(), 
				pos.getY(), 
				pos.getZ(), 
				SoundEvent.REGISTRY.getObject( new ResourceLocation("minecraft", soundName) ), 
				SoundCategory.BLOCKS, 
				.5f, 
				.5f
		);
	}


}
