package com.asist.asistmod.MissionSpecific.MissionA.Timer;

public interface MissionTimerListener {
	
	public abstract void onMissionTimeChange(int m, int s);

}
