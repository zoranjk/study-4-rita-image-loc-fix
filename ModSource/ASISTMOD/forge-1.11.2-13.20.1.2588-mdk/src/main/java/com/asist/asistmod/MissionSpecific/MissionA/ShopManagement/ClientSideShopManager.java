package com.asist.asistmod.MissionSpecific.MissionA.ShopManagement;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ContextCommunication.ContextCommunicator;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ShopGui.GuiShopTemplate;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;

public class ClientSideShopManager {
	
	
	public static void displayShopGui(int guiPageId) {
		
		Minecraft mc = Minecraft.getMinecraft();
		
		mc.displayGuiScreen((GuiScreen)null);

        if (mc.currentScreen == null)
        {
            mc.setIngameFocus();
        }
        ////System.out.println("Resetting Tool Counts and Chats");        
        
		Minecraft.getMinecraft().displayGuiScreen(new GuiShopTemplate(guiPageId));
		
	}
	
	public static void displayBeaconGui(int callSignCode, String[] callSigns, String beaconId) {
		
		Minecraft mc = Minecraft.getMinecraft();
		
		mc.displayGuiScreen((GuiScreen)null);

        if (mc.currentScreen == null)
        {
            mc.setIngameFocus();
        }        
		Minecraft.getMinecraft().displayGuiScreen(new ContextCommunicator(callSignCode,callSigns, beaconId));
	}
	
		
	public static void closeShopGui() {
			
		Minecraft mc = Minecraft.getMinecraft();
		mc.displayGuiScreen((GuiScreen)null);

		if (mc.currentScreen == null)
		{
			mc.setIngameFocus();
		}
	}
}
