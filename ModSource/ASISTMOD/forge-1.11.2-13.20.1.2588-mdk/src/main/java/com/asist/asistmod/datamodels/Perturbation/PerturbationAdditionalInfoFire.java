package com.asist.asistmod.datamodels.Perturbation;

public class PerturbationAdditionalInfoFire {
	
	int source_pos_x;
	int source_pos_y;
	int source_pos_z;
	String source_id;
	
	public PerturbationAdditionalInfoFire(int x,int y,int z, String id)
	{
		this.source_pos_x = x;
		this.source_pos_y = y;
		this.source_pos_z = z;
		this.source_id = id;
	}
}
