package com.asist.asistmod.block;

import java.util.HashMap;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.block.beacons.BlockBeaconBasic;
import com.asist.asistmod.block.beacons.BlockBeaconBomb;
import com.asist.asistmod.block.beacons.BlockBeaconHazard;
import com.asist.asistmod.block.beacons.BlockBeaconRally;
import com.asist.asistmod.block.bombs.BlockBombChained;
import com.asist.asistmod.block.bombs.BlockBombFire;
import com.asist.asistmod.block.bombs.BlockBombStandard;
import com.asist.asistmod.block.perturbation.BlockFireCustom;
import com.asist.asistmod.block.special.BlockBombDisposer;
import com.asist.asistmod.item._Items;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class _Blocks {
	
	public static final HashMap<Block,Item> blocks = new HashMap<Block,Item>();
			
	
	public static BlockBombStandard blockBombStandard;
	
	public static BlockBombChained blockBombChained;
	
	public static BlockBombFire blockBombFire;
	
	public static BlockBeaconBasic blockBeaconBasic;

	public static BlockBeaconRally blockBeaconRally;

	public static BlockBeaconBomb blockBeaconBomb;

	public static BlockBeaconHazard blockBeaconHazard;
	
	public static BlockBombDisposer blockBombDisposer;

	public static BlockFireCustom blockFireCustom;


	public static final void commonPreInit() {
		//BOMBS		
		_Blocks.blockBombStandard = (BlockBombStandard) registerBlock(new BlockBombStandard(), "block_bomb_standard");
		_Blocks.blockBombChained = (BlockBombChained) registerBlock(new BlockBombChained(), "block_bomb_chained");
		_Blocks.blockBombFire = (BlockBombFire) registerBlock(new BlockBombFire(), "block_bomb_fire");
		_Blocks.blockBeaconBasic = (BlockBeaconBasic) registerBlock(new BlockBeaconBasic(null), "block_beacon_basic");
		_Blocks.blockBeaconRally = (BlockBeaconRally) registerBlock(new BlockBeaconRally(null), "block_beacon_rally");
		_Blocks.blockBeaconBomb = (BlockBeaconBomb) registerBlock(new BlockBeaconBomb(null), "block_beacon_bomb");
		_Blocks.blockBeaconHazard = (BlockBeaconHazard) registerBlock(new BlockBeaconHazard(null), "block_beacon_hazard");
		_Blocks.blockBombDisposer = (BlockBombDisposer) registerBlock(new BlockBombDisposer(), "block_bomb_disposer");
		_Blocks.blockFireCustom = (BlockFireCustom) registerBlock(new BlockFireCustom(), "block_fire_custom");
	}
	
	public static final Block registerBlock(Block block, String name) {

		return registerBlock(block, name, name);
	}

	public static final Block registerBlock(Block block, String name, String block_item_name) {
		block.setUnlocalizedName(name);
		GameRegistry.register(block, new ResourceLocation(AsistMod.MODID, name));
		Item blockItem = _Items.registerItem(new ItemBlock(block), block_item_name);
		blocks.put(block, blockItem);
		return block;
	}


}
