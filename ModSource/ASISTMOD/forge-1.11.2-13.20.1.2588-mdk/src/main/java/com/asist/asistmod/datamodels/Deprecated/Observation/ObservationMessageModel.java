package com.asist.asistmod.datamodels.Deprecated.Observation;

import java.time.Clock;

import com.asist.asistmod.AsistMod;

public class ObservationMessageModel {	

	
	public String experiment_id = AsistMod.currentTrialInfo.experiment_id;;
	public String trial_id = AsistMod.currentTrialInfo.trial_id;
	public String timestamp = Clock.systemUTC().instant().toString();
	public String source = "simulator";
	public String sub_type = "state";
	public String version = "1.1";
	
}
