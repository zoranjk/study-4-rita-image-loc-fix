package com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ContextCommunication;


import java.time.Clock;

import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.MainOverlay.RenderOverlayEventHandler;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.shop.BeaconChatPacket;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;


public class ContextCommunicatorUtilities {
	
	static int lastIdPressed = 0;
	static long lastInteractionTime = 0;
	

	
	public static void processButtonClick(int buttonId, String beaconId) {
		
		long start = Clock.systemDefaultZone().millis();
		//System.out.println("Processing button Click IN Utility  Class");
		//System.out.println(buttonId); 
		
		//long currentTime = System.currentTimeMillis();
		//if( (currentTime - lastInteractionTime)>=1000 || lastInteractionTime == 0 ) {
			
			lastIdPressed = buttonId;
			
			if( buttonId == 0) {
				
				NetworkHandler.sendToServer( new BeaconChatPacket( RenderOverlayEventHandler.callSign,"I need a Red Wirecutter!",beaconId) );
				
			}
			else if ( buttonId == 1) {
				
				NetworkHandler.sendToServer( new BeaconChatPacket( RenderOverlayEventHandler.callSign,"I need a Green Wirecutter!",beaconId) );
				
			}
			else if ( buttonId == 2) {
				
				NetworkHandler.sendToServer( new BeaconChatPacket( RenderOverlayEventHandler.callSign,"I need a Blue Wirecutter!",beaconId) );
				
			}
			else if ( buttonId == 3) {
				
				NetworkHandler.sendToServer( new BeaconChatPacket( RenderOverlayEventHandler.callSign,"This is a Fire Bomb!",beaconId) );
				
			}
			else if ( buttonId == 4) {
				
				NetworkHandler.sendToServer( new BeaconChatPacket( RenderOverlayEventHandler.callSign,"This area is clear!",beaconId) );
				
			}
			else if ( buttonId == 5) {
				
				NetworkHandler.sendToServer( new BeaconChatPacket( RenderOverlayEventHandler.callSign,"Help! Things are on fire over here!",beaconId) );
				
			}
			else if ( buttonId == 6) {
				
				NetworkHandler.sendToServer( new BeaconChatPacket( RenderOverlayEventHandler.callSign,"Someone help I can't move!",beaconId) );
				
			}
			else if ( buttonId == 7) {
				
				NetworkHandler.sendToServer( new BeaconChatPacket( RenderOverlayEventHandler.callSign,"Ignore this beacon it was an accident.",beaconId) );
				
			}
			
		long delta = Clock.systemDefaultZone().millis() - start;
		//System.out.println("Time Delta : " + delta );				
			//lastInteractionTime = System.currentTimeMillis();
		closeContextGui();
		//}
	}
	
	public static void closeContextGui() {
		
		Minecraft mc = Minecraft.getMinecraft();
		mc.displayGuiScreen((GuiScreen)null);

		if (mc.currentScreen == null)
		{
			mc.setIngameFocus();
		}
	}
	
	public static void voteToGoToShop( ) {
		
		//System.out.println("Go To Shop");
	}
	
	public static void setCommunicationTarget( int buttonId) {
		
		if( buttonId == 2) {
			//System.out.println("Communicating to RED.");
		}
		else if( buttonId == 3) {
			//System.out.println("Communicating to GREEN.");
		}
		else if( buttonId == 4) {
			//System.out.println("Communicating to BLUE.");
		}
		else if( buttonId == 5) {
			//System.out.println("Communicating to ALL.");
		}
				
	}
	

	
	public static void setCommunicationContent( int buttonId) {
		
		if( buttonId == 6) {
			//System.out.println("Communicating to HELP!.");
		}
		else if( buttonId == 7) {
			//System.out.println("Communicating to SEQ.");
		}
		else if( buttonId == 8) {
			//System.out.println("Communicating to SPLIT UP.");
		}
		else if( buttonId == 9) {
			//System.out.println("Communicating to RALLY HERE.");
		}
		else if( buttonId == 10) {
			//System.out.println("Communicating to NEED TOOL.");
		}
				
	}
	
	
	
}
