package com.asist.asistmod.MissionSpecific.MissionA.BombManagement;

import java.util.ArrayList;

public class HeapAlgorithm {
	
	 /*** Generates a bomb sequence for a given number of people.
	 * 
	 * @param peopleDiffusing - An array containing the people required to defuse the bomb. Ex: An input of [R, B] will generate 
	 * all combinations red and blue can diffuse the bomb. 
	 * 
	 * @return - ArrayList containing n! char arrays containing a unique permutation/bomb sequence. */
	public static ArrayList<char[]> generateSequence(char[] peopleDiffusing) {
		ArrayList<char[]> sequence = new ArrayList<char[]>();
		heapPermutation(peopleDiffusing, sequence, peopleDiffusing.length);
		
		return sequence;
	}
	
	 /** Helper function for generating bomb sequences. Uses heap algorithm to generate (n-1)! permutations of a given set of elements
	 * @param input - input array of integers
	 * @param output - the collection of permutations of the input array 
	 * @param size - size of input */
	private static void heapPermutation(char[] input, ArrayList<char[]> output, int size) {
		//Base Case - At this point, a permutation has been generated and should be added to output
		if(size == 1) {
			output.add(input.clone());
		}
		//Recursive step
		for (int i = 0; i < size; i++) {
			heapPermutation(input, output, size - 1);
			
			//If size is odd, swap 0th i.e (first) and (size-1)th i.e (last) element
			if(size % 2 == 1) {
				char temp = input[0];
				input[0] = input[size - 1];
				input[size - 1] = temp;
			}
			//If size is even, swap ith and (size-1)th i.e last element
			else {
				char temp = input[i];
				input[i] = input[size - 1];
				input[size - 1] = temp;
			}
		}
	}
}
