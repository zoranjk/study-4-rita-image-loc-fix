package com.asist.asistmod.datamodels.Deprecated.Observation;

import java.time.Clock;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class ObservationData {	

	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds_global = MissionTimer.getElapsedMillisecondsGlobal();
	public long elapsed_milliseconds_stage = MissionTimer.getElapsedMillisecondsStage();
    public int observation_number = 0;
    public String timestamp = Clock.systemUTC().instant().toString();
	//public String playername = null;
	public String participant_id = "Not Set";	
	public Float yaw = null;
	public Double x = null;
	public Double y = null;
	public Double z = null;
	public Float pitch = null;
	public String id = null;
	public Double motion_x= null;
	public Double motion_y = null;
	public Double motion_z = null;
	public Float life = null;  

}
