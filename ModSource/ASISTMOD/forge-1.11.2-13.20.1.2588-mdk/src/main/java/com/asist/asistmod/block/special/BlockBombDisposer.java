package com.asist.asistmod.block.special;

import java.util.Random;

import javax.annotation.Nullable;

import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectType;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs.ASISTModTabs;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.EnvironmentCreated.Single.EnvironmentCreatedSingleModel;
import com.asist.asistmod.datamodels.ItemUsed.ItemUsedModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeData;
import com.asist.asistmod.datamodels.ToolUsed.ToolUsedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.BeaconTileEntity;
import com.asist.asistmod.tile_entity.DisposerBlockTileEntity;

import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockBombDisposer extends BlockContainer {


    boolean defused = false;

    boolean defusedAudioPlayed = false;

    double DelayedDefuseDuration = 3;


    double timer;

    public BlockBombDisposer() {

        super(Material.WOOD);
        this.setTickRandomly(true);
        this.setCreativeTab(ASISTModTabs.ASISTBlocks);

        timer = DelayedDefuseDuration;

    }

    // DONT DROP ANY ITEMS
    @Override public int quantityDropped(Random par1Random){ return 0;}

    // MAKES THE RENDERER WORK, OTHERWISE BLOCK IS INVISIBLE
    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return EnumBlockRenderType.MODEL;
    }

    @Nullable
    
    @Override
    public TileEntity createNewTileEntity(World world, int i) {

        DisposerBlockTileEntity tileEntity = new DisposerBlockTileEntity();

        return tileEntity;
    }
    
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    { 
    	worldIn.markBlockRangeForRenderUpdate(pos, pos);    	
    }
    
    


    /*
    @Override public void updateTick(World worldIn, BlockPos pos, IBlockState state, Random random) {

    }
    

    @Override public void randomTick(World worldIn, BlockPos pos, IBlockState state, Random random) {
        ////System.out.println("Random Tick Update! : " + pos.toString() );
    }

    @Override public int tickRate(World worldIn) {
        return 1;
    }
     */


}
