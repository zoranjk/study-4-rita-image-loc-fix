package com.asist.asistmod.block.bombs;


import com.asist.asistmod.MissionSpecific.MissionA.FireManagement.FireManager;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;


/**
 *
 * This is a "fire bomb" when exploded it will light a fire around it
 * */
public class BlockBombFire extends BlockBombBase {

    public BlockBombFire() {
        super();
        this.setHardness(10f);
        this.setTickRandomly(true);

    }


    @Override
    @SideOnly(Side.SERVER)
    public void destroyBomb(World world, BlockPos pos, String pid, String bombId, BombOutcomeEnum explodeReason) {
        
    	super.destroyBomb(world, pos, pid, bombId, explodeReason);

        FireManager.placeFire(world, pos, pid, false);

    }


}
