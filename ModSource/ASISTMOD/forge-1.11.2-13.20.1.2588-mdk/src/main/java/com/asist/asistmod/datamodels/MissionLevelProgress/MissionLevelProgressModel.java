package com.asist.asistmod.datamodels.MissionLevelProgress;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.mqtt.MessageTopic;

public class MissionLevelProgressModel{
	
    public HeaderModel header = new HeaderModel();
	
    public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public MissionLevelProgressDataModel data = new MissionLevelProgressDataModel();
	
	
	public MissionLevelProgressModel( int levelUnlocked) {
		
		header.message_type = "event";
		data.level_unlocked = levelUnlocked;
		
	}
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.MISSION_LEVEL_PROGRESS; 
		
	}
	
	public String getTopic() {
		return getMessageTopicEnum().getTopic();
	}
	
	
	public String toJsonString() { 
		
		return AsistMod.gson.toJson(this); 
		
	}
	
	public void publish() {
		InternalMqttClient.publish(this.toJsonString(), this.getTopic());
	}
}
