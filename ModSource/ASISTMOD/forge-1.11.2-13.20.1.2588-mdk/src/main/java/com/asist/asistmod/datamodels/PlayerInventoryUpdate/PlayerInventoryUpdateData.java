package com.asist.asistmod.datamodels.PlayerInventoryUpdate;

	
import java.util.Map;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;



public class PlayerInventoryUpdateData {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();	 
	public Map<String, Map<String, Integer>> currInv = null;

}
