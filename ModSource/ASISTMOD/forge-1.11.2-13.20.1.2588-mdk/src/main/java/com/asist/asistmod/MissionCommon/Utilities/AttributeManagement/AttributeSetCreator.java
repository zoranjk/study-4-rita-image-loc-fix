package com.asist.asistmod.MissionCommon.Utilities.AttributeManagement;

import java.util.HashMap;
import java.util.Map;

public class AttributeSetCreator {
	
	public static Map<String,String> createBombAttributeSet(String active, String sequence, String sequence_index, String outcome,
															String fuseStartTime){
		
		Map<String, String> out = new HashMap<String, String>(); 
		// TODO: ADD FUSE_DURATION ATTRIBUTE
		out.put("active", active);
		out.put("sequence",sequence);
		out.put("sequence_index", sequence_index);
		out.put("outcome", outcome);
		out.put("fuse_start_minute", fuseStartTime);
		
		return out;
	}
	
	public static HashMap <String, Map<String,String>> createItemStackAttributeSet(){
		
		HashMap <String, Map<String,String>> out = new HashMap <String, Map<String,String>>();
		
		return out;
	}
	
	public static Map<String,String> createGenericItemAttributeSet(int uses_remaining){
		
		Map<String, String> out = new HashMap<String, String>(); 
		
		out.put("uses_remaining", Integer.toString(uses_remaining));		
		
		return out;
	}
	
	public static Map<String,String> createHazardBeaconAttributeSet(String message, int uses_remaining){
		
		Map<String, String> out = new HashMap<String, String>(); 
		
		out.put("uses_remaining", Integer.toString(uses_remaining));
		out.put("message", message);
		
		return out;
	}
	
	public static Map<String,String> createBombBeaconAttributeSet(String bombId, int uses_remaining){
		
		Map<String, String> out = new HashMap<String, String>(); 
		
		out.put("uses_remaining", Integer.toString(uses_remaining));
		out.put("bomb_id", bombId);
		
		return out;
	}

}
