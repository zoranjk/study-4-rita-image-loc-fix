package com.asist.asistmod.commands;

import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;

/** These arguments are used the Scan command. 
 * If you want to add information to the Scan command, create the arguments here. */
public class BlockStateAndPos {

	public BlockPos blockPos;
	public IBlockState blockState;
	
	public BlockStateAndPos(BlockPos blockPos, IBlockState blockState) {
		this.blockPos = blockPos;
		this.blockState = blockState;
	}
	
	public int getX() {
		return blockPos.getX();
	}
	
	public int getY() {
		return blockPos.getY();
	}
	
	public int getZ() {
		return blockPos.getZ();
	}
	
}
