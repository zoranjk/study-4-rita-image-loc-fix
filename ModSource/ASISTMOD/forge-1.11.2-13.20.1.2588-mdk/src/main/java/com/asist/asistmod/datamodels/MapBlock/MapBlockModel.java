package com.asist.asistmod.datamodels.MapBlock;

import com.opencsv.bean.CsvBindByName;

public class MapBlockModel  {

    private String locationXYZ;

    public void setLocationXYZ(String locationXYZ) {
        this.locationXYZ = locationXYZ.trim();
    }
     
    public String getLocationXYZ() {
        return this.locationXYZ.trim();
    }    
   
    private String blockType;

    public String getBlockType() {
		return blockType.trim();
	}

	public void setBlockType(String blockType) {
		this.blockType = blockType.trim();
	}	

    private String featureType;

	public String getFeatureType() {
		return featureType.trim();
	}

	public void setFeatureType(String featureType) {
		this.featureType = featureType.trim();
	}

    private String sequence;
	
	public void setSequence(String in_sequence) {
		this.sequence = in_sequence.trim();
	}
	
	public String getSequence() {
		return this.sequence.trim();
	}
	
	private String bombID;

	public void setBombID(String in_bombID) {this.bombID = in_bombID.trim(); }

	public String getBombID() {return this.bombID.trim(); }

	/**
	 * in Minutes
	 */
	private String fuseStartTime;

	public void setFuseStartTime(String fuseStartTime) {
		this.fuseStartTime = fuseStartTime.trim(); 
	}

	public String getFuseStartTime() {return this.fuseStartTime; }

	
	private String chainedID;

	public void setChainedID(String chainedID) {
		
		if( chainedID == null || chainedID.isEmpty() ) {
			
			this.chainedID = "NONE";
			
		}
		else {
			
			this.chainedID = chainedID.trim();
			
		}
		
	
	}

	public String getChainedID() {return this.chainedID.trim(); }
}