package com.asist.asistmod.block.beacons;

import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.CallSigns;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.network.NetworkHandler;
import com.asist.asistmod.network.messages.shop.OpenBeaconOptionsPacket;

import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockBeaconHazard extends BlockBeaconBasic {
    public BlockBeaconHazard(Material materialIn) {
        super(materialIn);
    }
    
    /**
     * Called by ItemBlocks after a block is set in the world, to allow post-place logic
     */
   
    // This function extends the standard BlockBeaconBasic to send a communication environment message from this beacon to the clientmap, about the bomb specs it was associated with
    @Override
    public void onBlockPlacedBy(World worldIn, BlockPos pos, IBlockState state, EntityLivingBase placer, ItemStack stack)
    {
    	// Handle all the normal beacon functionality
    	super.onBlockPlacedBy(worldIn, pos, state, placer, stack);    	
    	
    	if(!worldIn.isRemote) {
    		
    		Player player = PlayerManager.getPlayerByName(placer.getName());    		
    		// This is the only beacon with a context menu!
			NetworkHandler.sendToClient(new OpenBeaconOptionsPacket(player.callSignCode,CallSigns.masterCallSignArray,player.lastBeaconId), player.getEntityPlayer()); 
    	}
    	   	
    }

}
