package com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs;

import com.asist.asistmod.block._Blocks;
import com.asist.asistmod.item._Items;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;

public class ASISTModTabs {

    public static final CreativeTabs ASISTItems = new CreativeTabs("ASIST Items") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(_Items.WIRECUTTERS_RED);
        }
    };
    public static final CreativeTabs ASISTBlocks = new CreativeTabs("ASIST Blocks") {
        @Override
        public ItemStack getTabIconItem() {
            return new ItemStack(_Blocks.blocks.get(_Blocks.blockBombStandard));
        }
    };

}
