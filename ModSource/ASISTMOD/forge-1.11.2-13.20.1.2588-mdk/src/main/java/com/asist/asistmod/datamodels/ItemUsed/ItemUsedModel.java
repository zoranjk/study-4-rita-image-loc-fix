package com.asist.asistmod.datamodels.ItemUsed;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.MessageTopic;

public class ItemUsedModel {
	
	
	
	public HeaderModel header = new HeaderModel();
	
	public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public ItemUsedData data = new ItemUsedData();
	
	public ItemUsedModel() {		
	}
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.ITEM_USED; 
		
	}
	
	public String getTopic() { 		
		
		return getMessageTopicEnum().getTopic(); 
		
	}
	
	public String toJsonString() { 		
		
		return AsistMod.gson.toJson(this); 
		
	}

}
