package com.asist.asistmod.datamodels.MissionState;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.MissionManager;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.mqtt.MessageTopic;

public class MissionStateModel {
	
    public HeaderModel header = new HeaderModel();
	
    public MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	
	public MissionStateDataModel data = new MissionStateDataModel();
	
	
	public MissionStateModel(String missionName, String missionStartStop, MissionManager.StateChangeOutcome stateChangeOutcome) {
		
		header.message_type = "event";
		data.mission = missionName;
		data.mission_state = missionStartStop;
		data.state_change_outcome = stateChangeOutcome.name();
	}
	
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.MISSION_STATE; 
		
	}
	
	public String getTopic() { 		
		
		return getMessageTopicEnum().getTopic(); 
		
	}
	
	public String toJsonString() { 		
		
		return AsistMod.gson.toJson(this); 
		
	}
	
	public void publish() {
		InternalMqttClient.publish(this.toJsonString(), this.getTopic());
	}
}
