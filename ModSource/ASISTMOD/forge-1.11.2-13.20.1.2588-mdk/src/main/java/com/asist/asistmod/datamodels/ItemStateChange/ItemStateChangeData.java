package com.asist.asistmod.datamodels.ItemStateChange;

import java.util.Map;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class ItemStateChangeData {	

	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();	
	public String item_id = "NOT_SET";
	public String item_name = "NOT_SET";
	public String owner = "NOT_SET";
	public Map< String, String[]> changedAttributes = null; 
	public Map<String, String> currAttributes = null;
	
	
}
