package com.asist.asistmod.datamodels.PlayerInventoryUpdate;

import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.datamodels.Header.HeaderModel;
import com.asist.asistmod.datamodels.Msg.MsgModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.mqtt.MessageTopic;

public class PlayerInventoryUpdateModel {
	
	
	HeaderModel header = new HeaderModel();
	MsgModel msg = new MsgModel(getMessageTopicEnum().getEventName(),getMessageTopicEnum().getVersion());
	public PlayerInventoryUpdateData data = new PlayerInventoryUpdateData();
	
	public PlayerInventoryUpdateModel(Map< String,Map<String,Integer> > currInv  ) {
		 
		data.currInv = currInv;
	}
	
	public MessageTopic getMessageTopicEnum() { 		
		
		return MessageTopic.PLAYER_INVENTORY_UPDATE; 
		
	}
	
	public String getTopic() { 		
		
		return getMessageTopicEnum().getTopic(); 
		
	}
	
	public String toJsonString() { 		
		
		return AsistMod.gson.toJson(this); 
		
	}
	
	public void publish() {
		InternalMqttClient.publish(this.toJsonString(), this.getTopic());
	}

}
