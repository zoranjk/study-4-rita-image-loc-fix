package com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement;

import java.util.concurrent.CopyOnWriteArrayList;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.MissionManagement.CallSigns;

import net.minecraft.entity.player.EntityPlayer;

public class PlayerManager {

	public static CopyOnWriteArrayList<Player> players = new CopyOnWriteArrayList<Player>();

	public static Player createPlayerManagerRecord(EntityPlayer player, String name){
		//System.out.println("CreatePlayerManagerRecord0");
		Player p = null;
		String callSignString = null;
		try {
			callSignString = AsistMod.currentTrialInfo.callsigns.get(name);
			
		}
		catch(Exception e) {
			//System.out.println( " CallSigns not found in TrialInfo ... was it sent?");
		}
		//System.out.println("CreatePlayerManagerRecord1");
		
		int callSignCode = -1;
		System.out.println("CallSignString : " + callSignString);
		if( callSignString != null) {
			for( int i=0; i<CallSigns.masterCallSignArray.length; i++) {
				System.out.println(callSignString + " == " + CallSigns.masterCallSignArray[i]);
				if(CallSigns.masterCallSignArray[i].contentEquals(callSignString)) {
					System.out.println("MATCH");
					callSignCode = i;
					break;
				}
			}
		}
		//System.out.println("CreatePlayerManagerRecord2");
		
		String pid = null;
		try {
			pid = AsistMod.currentTrialInfo.participant_ids.get(name);
		}
		catch(Exception e) {
			System.out.println( " PIDs not found in TrialInfo ... was it sent?");
		}
		
		
		//System.out.println("CreatePlayerManagerRecord3");
		if( callSignCode != -1 && pid != null) {
			p = PlayerManager.addPlayer(player, callSignCode, pid);
			p.removePlayerArmor();
			player.inventory.clear();
			//p.giveArmor();
			//p.givePlayerItems();				
		}
		else {
			//System.out.println("PlayerJoinEventHandler.createPlayerManagerRecord - ERROR - CALLSIGN OR PID WAS NULL FOR PLAYER : "+ name);
		}
		//System.out.println("CreatePlayerManagerRecord4");
		return p;
		
			
	}
	
	
	public static Player addPlayer(EntityPlayer p, int callSignCode, String participant_id) {
		
		p.clearActivePotions();
		p.setHealth(20.0f);
		Player player = new Player(p,callSignCode,participant_id);
		players.add(player);
		
		return player;
		
	}
	
	public static Player getPlayerByPid(String pid) {
		for(Player player:players) {
			if(player.participant_id.contentEquals(pid)) {
				return player;
			}
		}
		//System.out.println("You tried to get a player by pid but no player matched!");
		return null;
	}
	
	public static Player getPlayerByEntityPlayer(EntityPlayer entityPlayer) {
		
		String playerName = entityPlayer.getName();
		for(Player player:players) {
			if(player.getName().equals(playerName)) {
				return player;
			}
		}		
		return null;		
	}
	
	public static Player getPlayerByName(String name) {		
		Player out = null;		
		for(Player player:players) {
			if(player.getName().contentEquals(name)) {
				out = player;
			}
		}		
		return out;
	}
	
	public static Player getPlayerByCallsign(String cs) {		
		Player out = null;		
		for(Player player:players) {
			if(player.callSign == cs) {
				out = player;
			}
		}		
		return out;
	}

}
