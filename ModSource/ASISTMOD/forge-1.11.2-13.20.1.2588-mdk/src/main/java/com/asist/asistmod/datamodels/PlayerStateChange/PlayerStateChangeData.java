package com.asist.asistmod.datamodels.PlayerStateChange;

	
import java.util.Map;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;



public class PlayerStateChangeData {
	
	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();	
	public String participant_id = "NOT SET";	
	public int player_x;
	public int player_y;
	public int player_z;
	public String source_id = "NOT SET";
	public String source_type = "NOT SET";
	public int source_x;
	public int source_y;
	public int source_z;
	public Map<String, String[]> changedAttributes = null; 
	public Map<String, String> currAttributes = null;

}
