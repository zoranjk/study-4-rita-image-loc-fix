package com.asist.asistmod.MissionSpecific.MissionA.BombManagement;

import com.asist.asistmod.block.bombs.BlockBombBase;

import net.minecraft.block.Block;
import net.minecraft.world.World;

public class GhostBombCorrectorThread {
	
	public static boolean isInitialized = true;
	public static long timerSleepInterval = 1000;
	public static World world = null;
	public static Thread thread = null;
	
	public GhostBombCorrectorThread() {
		
	}
	public static void startNewThread(World w) {
		world = w;
		thread = new Thread() {			
			public void run() {	
				System.out.println("Ghost Bomb Corrector thread running ... ");
				while (isInitialized) {					
					try { 
						Thread.sleep(timerSleepInterval); 
					} 
					catch (InterruptedException e) {
						e.printStackTrace();
					}					
					loop();
				}
			}			
		};
		thread.start();
	}
	
	public static void loop() {
		
		//System.out.println("Looping ...");
		BombBookkeeper.bombsRemainingMap.forEach(  (k,v) ->{
			Block block = world.getBlockState(v).getBlock();
			if (block instanceof BlockBombBase) {
				
			}
			else {				
				System.out.println("Found Ghost Bomb : " + k + " : "+ v.toString() );
				System.out.println("Actual bomb type : " + block.getRegistryName().toString() );
				System.out.println("Removing it ..." );	
				BombBookkeeper.correctGhostBomb(k);
				world.removeTileEntity(v);
			}
		});
		
	}

}
