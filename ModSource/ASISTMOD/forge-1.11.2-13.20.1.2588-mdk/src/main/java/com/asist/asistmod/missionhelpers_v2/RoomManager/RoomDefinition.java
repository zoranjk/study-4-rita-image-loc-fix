package com.asist.asistmod.missionhelpers_v2.RoomManager;

public class RoomDefinition{
	
	public int min_x;
	public int min_z;
	public int max_x;
	public int max_z;
	
	public RoomDefinition(int min_x, int min_z, int max_x, int max_z) {
		this.min_x = min_x;
		this.min_z = min_z;
		this.max_x = max_x;
		this.max_z = max_z;
			
	}
}
