package com.asist.asistmod.item;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs.ASISTModTabs;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemManager;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.Player;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.datamodels.ItemStateChange.ItemStateChangeModel;
import com.asist.asistmod.datamodels.ItemUsed.ItemUsedModel;
import com.asist.asistmod.datamodels.ToolUsed.ToolUsedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;

import net.minecraft.block.Block;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.IItemPropertyGetter;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBombPPE extends ItemArmor {

    public ItemArmor previousHead;
    public int headColor;
    public ItemArmor previousChest;
    public int chestColor;
    public ItemArmor previousLegs;
    public int legsColor;
    public ItemArmor previousFeet;
    public int feetColor;

    public boolean previousArmorSaved = false;

    public boolean isEquipped = false;
    
    public ItemBombPPE(ArmorMaterial p_i46750_1_, int p_i46750_2_, EntityEquipmentSlot p_i46750_3_, String unlocalizedName) {
        
    	super(p_i46750_1_, p_i46750_2_, p_i46750_3_);
        this.setUnlocalizedName(unlocalizedName);
        this.setCreativeTab(ASISTModTabs.ASISTItems);
        this.setMaxStackSize(64);
        

        this.addPropertyOverride(new ResourceLocation(AsistMod.MODID,"isEquipped"), new IItemPropertyGetter()
        {

            public float apply(ItemStack stack, @Nullable World worldIn, @Nullable EntityLivingBase entityIn)
            {
                return 0.0f;
                /*
                if (entityIn == null && !stack.isOnItemFrame())
                {
                    return 0.0F;
                }
                else
                {
                    //return isEquipped? 1f : 0f;
                    return 0f;
                }
                */


            }
        });
    }

    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
    	
    	ItemStack itemstack = player.getHeldItem(hand);
        // These are equipment slots not to be confused with hotbar slots   ... ie HEAD, CHEST, etc ...
    	EntityEquipmentSlot entityequipmentslot = EntityLiving.getSlotForItemStack(itemstack);
        ItemStack itemstack1 = player.getItemStackFromSlot(entityequipmentslot);


        //Check if current slot is open
        if (itemstack1.isEmpty()) {

            //we don't know if there is any armor saved so set to false
            previousArmorSaved = false;

            //clear out all slots, if any armor is saved, save the item
            clearCurrentArmorSet(player, true);

            //We are going to be equipping armor in the next step so set this to true.
            isEquipped = true;

            //Set the current armor piece to the chest
            player.setItemStackToSlot(entityequipmentslot, itemstack.copy());            

            //Then depending on what armor piece we set fill out the rest of the set.
            autoSetAdditionalArmor(player, entityequipmentslot);
            
            if(!world.isRemote) {
            	String playerName = player.getName();
            	Player asistPlayer = PlayerManager.getPlayerByName(playerName);            	
            	String pid = asistPlayer.getParticipantId();
            	asistPlayer.setArmorEquipped(true,pid, player.getPosition(),pid,"player",player.getPosition());
            	publishItemUsedEvents(playerName, pid, player.getPosition(), null, itemstack);            	
            }
            itemstack.shrink(1);
            
            return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
        }
        else
        {
        	
            //if current slot isn't empty check if the item is PPE related.
            
        	if(itemstack1.getItem() == _Items.PPE_HEAD ||
            itemstack1.getItem() == _Items.PPE_CHEST ||
            itemstack1.getItem() == _Items.PPE_LEGS ||
            itemstack1.getItem() == _Items.PPE_FEET )
            {/*
            	I commented this out so participants don't accidentally throw away their armor when attempting to inspect bombs
            
                //If the armor is PPE related clear it out and don't save, because we're toggling off the PPE
                clearCurrentArmorSet(player, false);

                //If there is previous armor that is saved we want to restore it.
                if(previousArmorSaved)
                {
                    //this function will go through each element; head, chest, legs, and feet.
                    //it will then check to see if there is any stored armor and then restore it.
                    //while setting the previous variable to null.
                    restorePreviousArmorSet(player);
                }
                
                if(!world.isRemote) {
                	PlayerManager.getPlayerByName(player.getName()).setArmorEquipped(false);
                }
            */
            }
            //If the armor isn't PPE related we need to clear and save and set it to PPE
            else
            {

                clearCurrentArmorSet(player, true);

                isEquipped = true;

                player.setItemStackToSlot(entityequipmentslot, itemstack.copy());
                
                autoSetAdditionalArmor(player, entityequipmentslot);
                
                if(!world.isRemote) {
                	String playerName = player.getName();
                	Player asistPlayer = PlayerManager.getPlayerByName(playerName);            	
                	String pid = asistPlayer.getParticipantId();
                	asistPlayer.setArmorEquipped(true,pid, player.getPosition(),pid,"player",player.getPosition());
                	publishItemUsedEvents(playerName, pid, player.getPosition(), null, itemstack);                	
                }
                itemstack.shrink(1);
                return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, itemstack);
            }

            isEquipped = false;
            return new ActionResult<ItemStack>(EnumActionResult.FAIL, itemstack);
        }
        
       
        


    }
    
    public void publishItemUsedEvents(String playerName, String pid, BlockPos pos, Block block, ItemStack itemStack) {
    	
    	String registryName = this.getRegistryName().toString();
    	ItemType heldModItem = ItemManager.getItemTypeFromRegistryName(registryName);
    	String itemName = heldModItem.getName();
		String itemStackId = itemStack.getTagCompound().getTag("item_stack_id").toString();
		////System.out.println(itemStackId);
		itemStackId = itemStackId.replaceAll("\"", "");
		////System.out.println(itemStackId);
		String itemId = "ITEM"+Integer.toString(itemStack.getCount());
		String idForMessage = itemStackId+'_'+itemId;
		//System.out.println("ITEM ID FOR MESSAGE : "+idForMessage);
    	
		// TOOL USED MESSAGE
    	ToolUsedModel toolUsedModel = new ToolUsedModel();
		toolUsedModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
		toolUsedModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
		//toolUsedModel.data.playername = playerName;
		toolUsedModel.data.participant_id = pid;
		toolUsedModel.data.tool_type = itemName;
		toolUsedModel.data.durability = 0;
		toolUsedModel.data.target_block_x = pos.getX();
		toolUsedModel.data.target_block_y = pos.getY();
		toolUsedModel.data.target_block_z = pos.getZ();
		toolUsedModel.data.target_block_type = "player";
		InternalMqttClient.publish(toolUsedModel.toJsonString(), "observations/events/player/tool_used",playerName);
		
		// ITEM USED MESSAGE
		ItemUsedModel itemUsedModel = new ItemUsedModel();
		itemUsedModel.data.participant_id = pid;						
		itemUsedModel.data.item_id = idForMessage;
		itemUsedModel.data.item_name = itemName;
		itemUsedModel.data.input_mode = "RIGHT_MOUSE";
		itemUsedModel.data.target_x = pos.getX();
		itemUsedModel.data.target_y = pos.getY();
		itemUsedModel.data.target_z = pos.getZ();		
		InternalMqttClient.publish(itemUsedModel.toJsonString(), itemUsedModel.getTopic(), playerName);
		
		
		// ITEM STATE CHANGE MESSAGE
		Map<String,String> currAttr = AttributedObjectManager.getItemAttributes(itemStackId,itemId);
		int usesRemaining = Integer.parseInt(currAttr.get("uses_remaining"));
		usesRemaining--;
		Map<String,String> newAttr = new HashMap<String,String>();
		newAttr.put("uses_remaining", Integer.toString(usesRemaining) );
		Map<String, String[]> changedAttributes = AttributedObjectManager.updateItemAttributes(itemStackId, itemId, newAttr);
		// for all single use items
		ItemStateChangeModel itemStateChangeModel = new ItemStateChangeModel(idForMessage,itemName,pid,changedAttributes,currAttr);
		InternalMqttClient.publish(itemStateChangeModel.toJsonString(), itemStateChangeModel.getTopic(), playerName);
		
    }





    public void restorePreviousArmorSet(EntityPlayer player)
    {

        if(previousHead != null) {
            if(previousHead.getArmorMaterial() == ArmorMaterial.LEATHER)
                autoSetArmorItemInSlot(player, previousHead, headColor);
            else
                autoSetArmorItemInSlot(player, previousHead);
        }
        if(previousChest != null) {
            if(previousChest.getArmorMaterial() == ArmorMaterial.LEATHER)
                autoSetArmorItemInSlot(player, previousChest, chestColor);
            else
                autoSetArmorItemInSlot(player, previousChest);
        }
        if(previousLegs != null) {
            if(previousLegs.getArmorMaterial() == ArmorMaterial.LEATHER)
                autoSetArmorItemInSlot(player, previousLegs, legsColor);
            else
                autoSetArmorItemInSlot(player, previousLegs);
        }
        if(previousFeet != null) {
            if(previousFeet.getArmorMaterial() == ArmorMaterial.LEATHER)
                autoSetArmorItemInSlot(player, previousFeet, feetColor);
            else
                autoSetArmorItemInSlot(player, previousFeet);
        }


    }


    public void clearCurrentArmorSet(EntityPlayer player, boolean save)
    {

        ItemStack headStack = player.getItemStackFromSlot(EntityEquipmentSlot.HEAD);
        if(save && headStack.getItem() != Item.getItemFromBlock(Blocks.AIR)) {
            previousHead = (ItemArmor) headStack.getItem();
            if (previousHead.getArmorMaterial() == ItemArmor.ArmorMaterial.LEATHER)
            {
                headColor = previousHead.getColor(headStack);
            }
            previousArmorSaved = true;
        }
        headStack.setCount(0);

        ItemStack chestStack = player.getItemStackFromSlot(EntityEquipmentSlot.CHEST);
        if(save && chestStack.getItem() != Item.getItemFromBlock(Blocks.AIR)) {
            previousChest = (ItemArmor) (chestStack.getItem());
            if (previousChest.getArmorMaterial() == ItemArmor.ArmorMaterial.LEATHER)
            {
                chestColor = previousChest.getColor(chestStack);
            }
            previousArmorSaved = true;
        }
        chestStack.setCount(0);

        ItemStack legStack = player.getItemStackFromSlot(EntityEquipmentSlot.LEGS);
        if(save && legStack.getItem() != Item.getItemFromBlock(Blocks.AIR)) {
            previousLegs =  (ItemArmor)legStack.getItem();
            if (previousLegs.getArmorMaterial() == ItemArmor.ArmorMaterial.LEATHER)
            {
                legsColor = previousLegs.getColor(legStack);
            }
            previousArmorSaved = true;
        }
        legStack.setCount(0);

        ItemStack feetStack = player.getItemStackFromSlot(EntityEquipmentSlot.FEET);
        if(save && feetStack.getItem() != Item.getItemFromBlock(Blocks.AIR)) {
            previousFeet =  (ItemArmor)feetStack.getItem();
            if (previousFeet.getArmorMaterial() == ItemArmor.ArmorMaterial.LEATHER)
            {
                feetColor = previousFeet.getColor(feetStack);
            }
            previousArmorSaved = true;
        }
        feetStack.setCount(0);

    }




    public void autoSetAdditionalArmor(EntityPlayer player, EntityEquipmentSlot playerUsedArmor)
    {

        switch (playerUsedArmor)
        {
            case HEAD:
                autoSetArmorItemInSlot(player, _Items.PPE_CHEST);
                autoSetArmorItemInSlot(player, _Items.PPE_LEGS);
                autoSetArmorItemInSlot(player, _Items.PPE_FEET);
                break;
            case CHEST:
                autoSetArmorItemInSlot(player, _Items.PPE_HEAD);
                autoSetArmorItemInSlot(player, _Items.PPE_LEGS);
                autoSetArmorItemInSlot(player, _Items.PPE_FEET);
                break;
            case LEGS:
                autoSetArmorItemInSlot(player, _Items.PPE_HEAD);
                autoSetArmorItemInSlot(player, _Items.PPE_CHEST);
                autoSetArmorItemInSlot(player, _Items.PPE_FEET);
                break;
            case FEET:
                autoSetArmorItemInSlot(player, _Items.PPE_HEAD);
                autoSetArmorItemInSlot(player, _Items.PPE_CHEST);
                autoSetArmorItemInSlot(player, _Items.PPE_LEGS);
                break;
        }
    }

    public boolean autoSetArmorItemInSlot(EntityPlayer player, Item armor_item)
    {

        ItemStack itemStack = new ItemStack(armor_item);
        EntityEquipmentSlot entityequipmentslot = EntityLiving.getSlotForItemStack(itemStack);

        ItemStack itemstack1 = player.getItemStackFromSlot(entityequipmentslot);
        if(itemstack1.isEmpty()) {
            player.setItemStackToSlot(entityequipmentslot, itemStack.copy());
            //itemStack.setCount(0);
            return true;
        }
        else
        {
            return false;
        }

    }

    public boolean autoSetArmorItemInSlot(EntityPlayer player, ItemArmor armor_item, int color)
    {

        ItemStack itemStack = new ItemStack(armor_item);
        ItemArmor armor = (ItemArmor)itemStack.getItem();
        armor.setColor(itemStack, color);
        EntityEquipmentSlot entityequipmentslot = EntityLiving.getSlotForItemStack(itemStack);

        ItemStack itemstack1 = player.getItemStackFromSlot(entityequipmentslot);
        if(itemstack1.isEmpty()) {
            player.setItemStackToSlot(entityequipmentslot, itemStack.copy());
            //itemStack.setCount(0);
            return true;
        }
        else
        {
            return false;
        }

    }







    @Override
    @SideOnly(Side.CLIENT)
    public ModelBiped getArmorModel(EntityLivingBase entityLiving, ItemStack itemStack, EntityEquipmentSlot armorSlot, ModelBiped _default)
    {
        if(itemStack != ItemStack.EMPTY)
        {
            if(itemStack.getItem() instanceof ItemArmor)
            {
                BombPPE_ModelBase model = new BombPPE_ModelBase();

                //DEFAULT BEHAVIOR

                model.bipedHead.showModel = armorSlot == EntityEquipmentSlot.HEAD;
                model.bipedBody.showModel = model.bipedLeftArm.showModel = model.bipedRightArm.showModel = armorSlot == EntityEquipmentSlot.CHEST;
                model.bipedLeftLeg.showModel = model.bipedRightLeg.showModel = armorSlot == EntityEquipmentSlot.LEGS;



                //MODDED BEHAVIOR, SHOW ALL ARMOR REGARDLESS OF WHAT ITEM IS ON
                /*
                boolean check_if_item_is_worn =
                        armorSlot == EntityEquipmentSlot.HEAD ||
                        armorSlot == EntityEquipmentSlot.CHEST ||
                        armorSlot == EntityEquipmentSlot.LEGS ||
                        armorSlot == EntityEquipmentSlot.FEET;


                model.bipedHead.showModel = check_if_item_is_worn;
                model.bipedBody.showModel = check_if_item_is_worn;
                model.bipedLeftArm.showModel = check_if_item_is_worn;
                model.bipedRightArm.showModel = check_if_item_is_worn;
                model.bipedLeftLeg.showModel = check_if_item_is_worn;
                model.bipedRightLeg.showModel = check_if_item_is_worn;

                 */


                model.isChild = _default.isChild;
                model.isRiding = _default.isRiding;
                model.isSneak = _default.isSneak;
                model.rightArmPose = _default.rightArmPose;
                model.leftArmPose = _default.leftArmPose;

                return model;
            }
        }
        return null;
    }


}
