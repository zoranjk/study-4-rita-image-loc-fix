package com.asist.asistmod.MissionSpecific.MissionA.RegionManagement;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ServerSideShopManager;

import net.minecraft.util.math.BlockPos;


public class RegionManager {
	
	public static boolean isInitialized = false;
	public static ArrayList<String>[] bombsPerRegion = null;
	public static int[] regionTotals = null;
	public static boolean[] regionsUnlocked = null;
	static Map<String, Integer>[]sectorBounds = null;
	static Map<String, Integer>[]sectorGateBounds = null;
	
	@SuppressWarnings("unchecked")
	public static void init() {
		sectorBounds = AsistMod.modSettings.sector_boundaries;
		sectorGateBounds = AsistMod.modSettings.sector_gates;
		int len = sectorBounds.length;
		bombsPerRegion = new ArrayList[len];
		regionTotals = new int[len];
		regionsUnlocked = new boolean[len];
		for(int i=0;i<len;i++) {
			bombsPerRegion[i]= new ArrayList<String>();
			if(i==0) {
				regionsUnlocked[i]=true;
			}
			else {
				regionsUnlocked[i]=false;
			}
		}
		
		//sector_boundaries = (ConcurrentMap<String, Integer>[]) AsistMod.modSettings.sector_boundaries;
	}
	
	public static int checkRegionFromPos(BlockPos pos) {
		
		int out = 0;		
		int x = pos.getX();
		int z = pos.getZ();
		for(int i =0; i<sectorBounds.length; i++) {			
			if( (x >= sectorBounds[i].get("fromX"))  && (x <= sectorBounds[i].get("toX")) ) {
				if( (z >= sectorBounds[i].get("fromZ"))  && (z <= sectorBounds[i].get("toZ")) ) {
					out = i;
					break;
				}
				
			}			
		}		
		return out;
		
	}
	
	public static void addBombToRegion(String id, BlockPos pos) {
		
		int sectorIndex = checkRegionFromPos(pos);
		//System.out.println(id +":"+sectorIndex);
		bombsPerRegion[sectorIndex].add(id);
		printBombsPerRegion();
		
		
	}
	
	public static void setRegionTotals() {
		
		for(int i=0;i<bombsPerRegion.length;i++) {
			regionTotals[i]=bombsPerRegion[i].size();
		}
		
	}
	
	public static void removeBombFromRegion(String id, BlockPos pos) {
		
		int sectorIndex = checkRegionFromPos(pos);
		bombsPerRegion[sectorIndex].remove(id);
		printBombsPerRegion();
		
	}
	
	/*
	 * returns a int[] of size 3.
	 * isGate,meetsThreshhold,GateIndex 
	 * 0, 0, 0 = Not a gate, no message
	 * 1, 0, 0 = A Gate, region not yet unlocked, deliver message, cancel event
	 * 1, 1, 0 = A Gate, region is unlocked
	 */
	public static int[] checkDoor( BlockPos pos) {
		
		int[] out = {0,0,0};
		int index = -1;		
		int x = pos.getX();
		int z = pos.getZ();
		for(int i = 0; i<sectorGateBounds.length; i++) {			
			if( (x >= sectorGateBounds[i].get("fromX"))  && (x <= sectorGateBounds[i].get("toX")) ) {
				if( (z >= sectorGateBounds[i].get("fromZ"))  && (z <= sectorGateBounds[i].get("toZ")) ) {
					index = i;
					out[0]=1;
					out[2]=i;
					break;
				}
			}			
		}
		
		// this door is already unlocked so treat it like any other door
		if( regionsUnlocked[index+1] == true) {
			return new int[]{0,0,0};
		}
		// the door is not unlocked, so do the unlock logic
		else if (index>=0) {
			int bombsRemaining = bombsPerRegion[index].size();
			if( bombsRemaining <= (regionTotals[index]/2.0f) ) {
				// UNLOCK DOOR
				out[1]=1;
				// 2 gates, 3 regions, gate 0 unlocks region 1
				regionsUnlocked[index+1]=true;
				ServerSideShopManager.teamBudget+=30;
			}
			else {
				// KEEP DOOR LOCKED AND SEND MESSAGE
			}
		}
		
		return out;
	}
	
	public static void printBombsPerRegion() {
		//System.out.println(Arrays.toString( bombsPerRegion));
	}

}
