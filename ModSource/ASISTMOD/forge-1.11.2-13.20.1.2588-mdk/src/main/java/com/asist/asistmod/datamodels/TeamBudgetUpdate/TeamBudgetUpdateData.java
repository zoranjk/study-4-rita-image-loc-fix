package com.asist.asistmod.datamodels.TeamBudgetUpdate;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class TeamBudgetUpdateData {	

	public String mission_timer = MissionTimer.getMissionTimeString();
	public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public int team_budget = 0;
		
}
