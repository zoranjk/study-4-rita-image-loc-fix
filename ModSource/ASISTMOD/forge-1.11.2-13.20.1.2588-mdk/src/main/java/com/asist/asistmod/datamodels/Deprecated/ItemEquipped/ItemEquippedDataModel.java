package com.asist.asistmod.datamodels.Deprecated.ItemEquipped;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class ItemEquippedDataModel {
	
	public String mission_timer = MissionTimer.getMissionTimeString();public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public String playername = null;
	public String participant_id = "Not Set";
	public String equippeditemname = null;
}
