package com.asist.asistmod.item;

import java.util.HashMap;
import java.util.Map;

import com.asist.asistmod.AsistMod;
import com.asist.asistmod.MissionCommon.Utilities.AttributeManagement.AttributedObjectManager;
import com.asist.asistmod.MissionSpecific.MissionA.BombManagement.BombBookkeeper;
import com.asist.asistmod.MissionSpecific.MissionA.BombRemovedBroadcasting.BombRemovedBroadcaster;
import com.asist.asistmod.MissionSpecific.MissionA.GuiManagement.ASISTCreativeTabs.ASISTModTabs;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemManager;
import com.asist.asistmod.MissionSpecific.MissionA.ItemManagement.ItemType;
import com.asist.asistmod.MissionSpecific.MissionA.PlayerManagement.PlayerManager;
import com.asist.asistmod.MissionSpecific.MissionA.RegionManagement.RegionManager;
import com.asist.asistmod.MissionSpecific.MissionA.ScoreboardManagement.ScoreboardManager;
import com.asist.asistmod.MissionSpecific.MissionA.ShopManagement.ServerSideShopManager;
import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;
import com.asist.asistmod.block.bombs.BlockBombBase;
import com.asist.asistmod.datamodels.EnvironmentCreated.Single.EnvironmentCreatedSingleModel;
import com.asist.asistmod.datamodels.EnvironmentRemoved.Single.EnvironmentRemovedSingleModel;
import com.asist.asistmod.datamodels.ItemStateChange.ItemStateChangeModel;
import com.asist.asistmod.datamodels.ItemUsed.ItemUsedModel;
import com.asist.asistmod.datamodels.ObjectStateChange.ObjectStateChangeModel;
import com.asist.asistmod.datamodels.ToolUsed.ToolUsedModel;
import com.asist.asistmod.mqtt.InternalMqttClient;
import com.asist.asistmod.tile_entity.BombBlockTileEntity;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemPickaxe;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.registry.ForgeRegistries;

public class BombDisposer extends ItemPickaxe {


    public BombDisposer(String name, ToolMaterial material) {
        super(material);
        this.setUnlocalizedName(name);
        this.setMaxDamage(2);
        this.setCreativeTab(ASISTModTabs.ASISTItems);
        this.setMaxStackSize(64);

    }

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing,
                                      float hit_x, float hit_y, float hit_z) {


		if(world.getBlockState(pos).getBlock() instanceof BlockBombBase) {

			ItemStack itemStack = player.getHeldItemMainhand();

			IBlockState iblockstate = world.getBlockState(pos);
			Block block = iblockstate.getBlock();
			AsistMod.playSound(world,"block.anvil.destroy",pos);

			//Server Side Bookkeeping
			if (!world.isRemote) {

				String playerName = player.getName();
				String pid = PlayerManager.getPlayerByName(playerName).getParticipantId();
				BombBlockTileEntity te = (BombBlockTileEntity) world.getTileEntity(pos);
				String id = te.getBombId();
				te.pauseCountDown();
				//Bookkeeping
				BombBookkeeper.addBombToDefused(id);
				//RegionManager.removeBombFromRegion(id, pos);
				BombRemovedBroadcaster.bombRemoved(id);

				char[] seq = te.getDefuseSequence();
				// START UPDATE ATTRIBUTES AND SEND OBJECT_STATE_CHANGE MESSAGE
				StringBuilder sb = new StringBuilder();
				for (char c : seq) {
					sb.append(c);
				}
				Map<String, String> newAttr = new HashMap<String, String>();
				newAttr.put("sequence", sb.toString());
				newAttr.put("sequence_index", Integer.toString(te.getDefuseIndex()));
				newAttr.put("active", Boolean.toString(te.isCountingDown()));
				newAttr.put("outcome", BlockBombBase.BombOutcomeEnum.DEFUSED_DISPOSER.name());

				Map<String, String[]> changedAttributes = AttributedObjectManager.updateObjectAttributes(id, newAttr);

				// Build Message
				ObjectStateChangeModel objectStateChangeModel = new ObjectStateChangeModel(
						id,
						block.getRegistryName().toString().split(":")[1],
						pid,
						pos.getX(),
						pos.getY(),
						pos.getZ(),
						changedAttributes,
						AttributedObjectManager.getObjectAttributes(id)
				);

				InternalMqttClient.publish(objectStateChangeModel.toJsonString(), objectStateChangeModel.getTopic());

				MissionTimer.unsubscribeToTimerTicks(te);
				world.removeTileEntity(pos);
				//remove any beacon thats on top
				BlockBombBase.removeAssociatedBeacon(world, pos, block, pid);

				// EnvironmentRemovedSingle
				EnvironmentRemovedSingleModel envRemovedModel = new EnvironmentRemovedSingleModel(pid, objectStateChangeModel.data);
				InternalMqttClient.publish(envRemovedModel.toJsonString(), envRemovedModel.getTopic());

				

				// reuse objectStateChange model to deliver the new disposed block
				objectStateChangeModel.data.currAttributes = null;
				objectStateChangeModel.data.changedAttributes = null;
				objectStateChangeModel.data.type = "block_bomb_disposer";

				//Score Adjustments
				ScoreboardManager.increasePlayerScore(pid, ScoreboardManager.PlayerHelpedBombValue);
				ServerSideShopManager.addToTeamBudget(1);

				// EnvironmentCreatedSingle
				EnvironmentCreatedSingleModel envCreatedModel = new EnvironmentCreatedSingleModel(pid, objectStateChangeModel.data);
				InternalMqttClient.publish(envCreatedModel.toJsonString(), envCreatedModel.getTopic());

				publishItemUsedEvents(playerName, pid, pos, block, itemStack);

				itemStack.shrink(1);
			}

			//Handle Regular OnItemUse Here

			IBlockState bombReplacement = ForgeRegistries.BLOCKS.getValue(
					new ResourceLocation("asistmod", "block_bomb_disposer")).getDefaultState();
			
			world.setBlockState(pos, bombReplacement, 3);
			return EnumActionResult.SUCCESS;
			//Block block = world.getBlockState(pos).getBlock();
			//block.updateTick(world, pos, bombReplacement, itemRand);

		}
        return EnumActionResult.PASS;
    }
    
    public void publishItemUsedEvents(String playerName, String pid, BlockPos pos, Block block, ItemStack itemStack) {
    	
    	String registryName = this.getRegistryName().toString();
    	ItemType heldModItem = ItemManager.getItemTypeFromRegistryName(registryName);
    	String itemName = heldModItem.getName();
		String itemStackId = itemStack.getTagCompound().getTag("item_stack_id").toString();
		////System.out.println(itemStackId);
		itemStackId = itemStackId.replaceAll("\"", "");
		////System.out.println(itemStackId);
		String itemId = "ITEM"+Integer.toString(itemStack.getCount());
		String idForMessage = itemStackId+'_'+itemId;
		//System.out.println("ITEM ID FOR MESSAGE : "+idForMessage);
    	
		// TOOL USED MESSAGE
    	ToolUsedModel toolUsedModel = new ToolUsedModel();
		toolUsedModel.msg.experiment_id = InternalMqttClient.currentTrialInfo.experiment_id;
		toolUsedModel.msg.trial_id = InternalMqttClient.currentTrialInfo.trial_id;
		//toolUsedModel.data.playername = playerName;
		toolUsedModel.data.participant_id = pid;
		toolUsedModel.data.tool_type = itemName;
		toolUsedModel.data.durability = 0;
		toolUsedModel.data.target_block_x = pos.getX();
		toolUsedModel.data.target_block_y = pos.getY();
		toolUsedModel.data.target_block_z = pos.getZ();
		toolUsedModel.data.target_block_type = block.getRegistryName().toString().split(":")[1];
		InternalMqttClient.publish(toolUsedModel.toJsonString(), "observations/events/player/tool_used",playerName);
		
		// ITEM USED MESSAGE
		ItemUsedModel itemUsedModel = new ItemUsedModel();
		itemUsedModel.data.participant_id = pid;						
		itemUsedModel.data.item_id = idForMessage;
		itemUsedModel.data.item_name = itemName;
		itemUsedModel.data.input_mode = "RIGHT_MOUSE";
		itemUsedModel.data.target_x = pos.getX();
		itemUsedModel.data.target_y = pos.getY();
		itemUsedModel.data.target_z = pos.getZ();		
		InternalMqttClient.publish(itemUsedModel.toJsonString(), itemUsedModel.getTopic(), playerName);
		
		
		// ITEM STATE CHANGE MESSAGE
		Map<String,String> currAttr = AttributedObjectManager.getItemAttributes(itemStackId,itemId);
		int usesRemaining = Integer.parseInt(currAttr.get("uses_remaining"));
		usesRemaining--;
		Map<String,String> newAttr = new HashMap<String,String>();
		newAttr.put("uses_remaining", Integer.toString(usesRemaining) );
		Map<String, String[]> changedAttributes = AttributedObjectManager.updateItemAttributes(itemStackId, itemId, newAttr);
		// for all single use items
		ItemStateChangeModel itemStateChangeModel = new ItemStateChangeModel(idForMessage,itemName,pid,changedAttributes,currAttr);
		InternalMqttClient.publish(itemStateChangeModel.toJsonString(), itemStateChangeModel.getTopic(), playerName);
		
    }
}
