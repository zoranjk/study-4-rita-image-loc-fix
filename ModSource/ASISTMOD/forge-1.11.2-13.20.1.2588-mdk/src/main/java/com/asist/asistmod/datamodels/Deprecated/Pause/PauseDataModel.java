package com.asist.asistmod.datamodels.Deprecated.Pause;

import com.asist.asistmod.MissionSpecific.MissionA.Timer.MissionTimer;

public class PauseDataModel {
	
	public String mission_timer = MissionTimer.getMissionTimeString();public long elapsed_milliseconds = MissionTimer.getElapsedMillisecondsGlobal();
	public Boolean paused;
		
}
