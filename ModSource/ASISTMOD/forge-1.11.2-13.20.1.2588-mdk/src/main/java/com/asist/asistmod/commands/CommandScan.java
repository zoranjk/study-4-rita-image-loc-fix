package com.asist.asistmod.commands;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import javax.annotation.Nullable;

import com.google.common.base.Predicate;
import com.google.common.collect.Lists;

import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.CommandResultStats;
import net.minecraft.command.ICommandSender;
import net.minecraft.command.WrongUsageException;
import net.minecraft.init.Blocks;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntitySign;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.gen.structure.StructureBoundingBox;

/**
*  A command that takes two coordinates and generates a csv file of all of the block locations within the volume of these coordinates. 
*  csv file will be named "Mission" and located in the "run" folder of your eclipse workspace when the command is executed. 
*  Current blockPosA & blockPosB currently have the coordinates of the corner of the map.
*  If blockContext is true then the csv file will have a column called "Block Context" 
*/
public class CommandScan extends CommandBase {	
	
		final String FILENAME = "Mission.csv";
		public BlockPos blockPosA;
		public BlockPos blockPosB;
		public boolean useArgs;
		
		String header = "X,Y,Z,BlockType,BlockContext,DataValue,SignText";

		String projectName = "ASIST_MAP"; //CSVLoader.getProjectName();
		
		public CommandScan(BlockPos blockPosA, BlockPos blockPosB, boolean useArgs) {
			this.blockPosA = blockPosA;
			this.blockPosB = blockPosB;
			this.useArgs = useArgs;
		}
		
		//Formatting for any Minecraft command	
		@Override
		/**
	     * Gets the name of the command
	     */
	    public String getName()
	    {
	        return "scan";
	    }

		@Override
	    /**
	     * Return the required permission level for this command.
	     */
	    public int getRequiredPermissionLevel()
	    {
	        return 2;
	    }

		@Override
	    /**
	     * Gets the usage string for the command.
	     *  
	     * @param sender The ICommandSender who is requesting usage details
	     */
	    public String getUsage(ICommandSender sender)
	    {
	        return "scan x1 y1 z1 x2 y2 z2";
	    }

		@Override
	    /**
	     * Callback for when the command is executed
	     *  
	     * @param server The server instance
	     * @param sender The sender who executed the command
	     * @param args The arguments that were passed
	     */
		
/* The Scan command is a custom Minecraft command that can be entered as any normal command. 
 * It takes two coordinates for blocks in the world and then creates a bounding box between those two points.
 * Then it "counts" up the blocks using a for loop, and each time it "counts" blockpos4 is used to store the location of the block, the type of block, and
 * it can also store NBT Data or other information if needed, these parameters can be defined in BlockStateAndPos.java and then passed as arguments here.
 * blockpos4 is then added to 3 lists which are then combined into list3. The Scan command is based off of the Clone command from Minecraft, and the clone command
 * used different lists for tileentities, full blocks, and everything else. While the Scan command doesn't necessarily need these lists, it may be nice in the future to 
 * generate a list of just tileentities if we want to scan just the victim blocks for example.  
 * Once list3 is generated, it is written to a file called Mission which will be located in the run folder of your eclipse workspace. */
		
	    public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
	        if (useArgs && args.length < 6) {
	            throw new WrongUsageException("/scan x1 y1 z1 x2 y2 z2", new Object[0]);
	        }
	        else if (!useArgs && (args.length > 1)) {
	        	throw new WrongUsageException("/scan", new Object[0]);	
	        }        	
            //sender.setCommandStat(CommandResultStats.Type.AFFECTED_BLOCKS, 0);
            
            BlockPos blockpos = null;
            BlockPos blockpos1 = null;
            
            //useArgs refers to arguments put in Minecraft console. This can be done in the server console also.  	            
            if (useArgs) {
            	blockpos = parseBlockPos(sender, args, 0, false);
            	blockpos1 = parseBlockPos(sender, args, 3, false);	           	            	
            	//System.out.println("Using arguments for scan");
            }
            //blockPosA and B are defined in the AsistMod.java. This is if you want to input the coordinates of the scan without entering Minecraft. 
            else {
            	blockpos = new BlockPos(blockPosA.getX(), blockPosA.getY(), blockPosA.getZ());
	            blockpos1 = new BlockPos(blockPosB.getX(), blockPosB.getY(), blockPosB.getZ());
            	//System.out.println("Using blockPosA and B for scan");
            }
            
            StructureBoundingBox structureboundingbox = new StructureBoundingBox(blockpos, blockpos1);
            int i = structureboundingbox.getXSize() * structureboundingbox.getYSize() * structureboundingbox.getZSize();

            /*i is the volume of the boundingbox, and the game checks this to make sure users won't crash the game if they try to create a boundingbox too large.
             * The default limit of i > 32768. I increased this to 500,000 so we could scan the whole space, but I have not tested the command on a space so large, so your game 
             * might crash if you scan something too large. If you ever need to scan larger spaces, change the i value here. */
            
            if (i > 500000) {
                throw new CommandException("Scan area was too large.", new Object[] {Integer.valueOf(i), Integer.valueOf(500000)});
            }
            else {
                Block block = null;
                Predicate<IBlockState> predicate = null;

                    if (structureboundingbox.minY >= 0 && structureboundingbox.maxY < 256)
                    {
                        World world = sender.getEntityWorld();

                        if (world.isAreaLoaded(structureboundingbox))
                        {
                        	
                            List<BlockStateAndPos> list = Lists.<BlockStateAndPos>newArrayList();
                            List<BlockStateAndPos> list1 = Lists.<BlockStateAndPos>newArrayList();
                            List<BlockStateAndPos> list2 = Lists.<BlockStateAndPos>newArrayList();	                            
                            	                          
                            for (int k = structureboundingbox.minY; k <= structureboundingbox.maxY; ++k)
                            {
                                for (int l = structureboundingbox.minX; l <= structureboundingbox.maxX; ++l)
                                {
                                    for (int j = structureboundingbox.minZ; j <= structureboundingbox.maxZ; ++j)
                                    {
                                        BlockPos blockpos4 = new BlockPos(l, k, j);
                                        IBlockState iblockstate = world.getBlockState(blockpos4);

                                        if ((iblockstate.getBlock() != Blocks.AIR) && (block == null || iblockstate.getBlock() == block && (predicate == null || predicate.apply(iblockstate))))
                                        {
                                            TileEntity tileentity = world.getTileEntity(blockpos4);

                                            if (tileentity != null)
                                            {
                                                tileentity.writeToNBT(new NBTTagCompound());
                                                list1.add(new BlockStateAndPos(blockpos4, iblockstate));
                                            }
                                            else if (!iblockstate.isFullBlock() && !iblockstate.isFullCube())
                                            {
                                            	list2.add(new BlockStateAndPos(blockpos4, iblockstate));	                                                
                                            }
                                            else
                                            {
                                            	list.add(new BlockStateAndPos(blockpos4, iblockstate));
                                            }
                                        }
                                    }
                                }
                            }
                            //List 3 contains all of the info for scan. 
                            List<BlockStateAndPos> list3 = Lists.<BlockStateAndPos>newArrayList();
                            list3.addAll(list);
                            list3.addAll(list1);
                            list3.addAll(list2);
                            
                            writeMapToCSV(list3, world);	 	                            	 	                                           	                		           			
                			
                			//Exceptions in case the command does not work. 
                            if (i <= 0)
                            {
                                throw new CommandException("No blocks were scanned", new Object[0]);
                            }
                            else
                            {
                                sender.setCommandStat(CommandResultStats.Type.AFFECTED_BLOCKS, i);
                                notifyCommandListener(sender, this, "Scan successful", new Object[] {Integer.valueOf(i)});
                            }
                        }
                        else
                        {
                            throw new CommandException("Unable to scan out of world", new Object[0]);
                        }
                    }
                    else
                    {
                        throw new CommandException("Unable to scan out of world", new Object[0]);
                    }
            	}	       
        } //End Execute

		/**
	     * Get a list of options for when the user presses the TAB key
	     *  
	     * @param server The server instance
	     * @param sender The ICommandSender to get tab completions for
	     * @param args Any arguments that were present when TAB was pressed
	     * @param targetPos The block that the player's mouse is over, <tt>null</tt> if the mouse is not over a block
	     */
	    public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos)
	    {
	        return args.length > 0 && args.length <= 1 ? getListOfStringsMatchingLastWord(args, new String[] {"victims", "rubble", "map"}) : ( args.length > 1 ? getListOfStringsMatchingLastWord(args, new String[] {"m1_", "m2_"}) : Collections.<String>emptyList());
	    }
	    
	    /**
	     * Writes the main mission csv file.
	     * @param blockList - list of blocks
	     * @param world
	     * @throws CommandException
	     */   
	    private void writeMapToCSV(List<BlockStateAndPos> blockList, World world) throws CommandException {
			//Lists for block context				
			List<String> blocksDecoration = createDecorList();
			List<String> blocksWalls = createWallList();
			List<String> blocksDoors = createDoorList();
			List<String> blocksTransparent = createTransparentList();
			
			//Strings for block context.
			String floor = "minecraft:stone";		
			String rubble = "minecraft:gravel";
			String banner = "minecraft:wall_banner";
	    	
	    	try {
				FileWriter missionblocks = new FileWriter(FILENAME);	                		
				missionblocks.write(header);	            
				missionblocks.write("\n");
				AtomicInteger i = new AtomicInteger(0);
				
				blockList.forEach(object ->{
				i.set(i.get() + 1);
					try {
						missionblocks.write("" + object.blockPos.getX()); //X
						missionblocks.write(",");
						
						missionblocks.write("" + object.blockPos.getY()); //Y	                					
						missionblocks.write(",");
						
						missionblocks.write("" + object.blockPos.getZ()); //Z
						missionblocks.write(",");
						
						missionblocks.write(object.blockState.getBlock().getRegistryName().toString()); //BlockType
						missionblocks.write(",");						
						
						//BlockContext
						String name = object.blockState.getBlock().getRegistryName().toString();					
							if (name.equals(floor)) {							
								missionblocks.write("floor");						
							}
							else if (blocksWalls.contains(name)) {
								missionblocks.write("wall");
							}
							else if (blocksDecoration.contains(name)) {
								missionblocks.write("decoration");
							}
							else if (blocksDoors.contains(name)) {
								missionblocks.write("door");
							}
							else if (blocksTransparent.contains(name)) {
								missionblocks.write("transparent");
							}
							else if(name.equals(rubble))  {
								missionblocks.write("rubble");
							}
							else if (name.equals(banner))  {
								missionblocks.write("banner");
							}
							else {
								missionblocks.write("other");
							}						
						missionblocks.write(",");
						
						//DataValue
						missionblocks.write((Integer.toString(object.blockState.getBlock().getMetaFromState(object.blockState)))); //Data value
						missionblocks.write(",");
						
						//SignText
						//Copy down text on the signs so we can add it in later when placing the signs
						 if (object.blockState.getBlock().getRegistryName().toString().equals("minecraft:standing_sign") || object.blockState.getBlock().getRegistryName().toString().equals("minecraft:wall_sign")) {
							String text0 = ((TileEntitySign)world.getTileEntity(object.blockPos)).signText[0].toString(); //This has a lot of unwanted info (that contains commas)
							String text1 = ((TileEntitySign)world.getTileEntity(object.blockPos)).signText[1].toString();
							String text2 = ((TileEntitySign)world.getTileEntity(object.blockPos)).signText[2].toString();
							String text3 = ((TileEntitySign)world.getTileEntity(object.blockPos)).signText[3].toString();
							String text = text0 + text1 + text2 + text3; 
							String[] actualText = text.split("'"); //Every odd number of this string[] will contain just the text that is on the sign. This is just how it is formatted
							missionblocks.write(actualText[1] + "!"); //Exclamation point is a delimiter that will hopefully not be used by anything else
							missionblocks.write(actualText[3] + "!");
							missionblocks.write(actualText[5] + "!");
							missionblocks.write(actualText[7]);
							//missionblocks.write(text0 + text1 +text2 + text3);
						} 
						missionblocks.write("\n"); //Next line  				    	
					}
					catch (Exception e) {
						 //System.out.println("Error writing an object out of list3 at: " + i.get());
						}
				});
				missionblocks.close();	         
			} catch (IOException e) {
				throw new CommandException("Writing to text file failed due to an exception.", new Object[0]);	
			}
	    }
	    
	    private List<String> createDecorList() {
	    	List<String> decor = new ArrayList<String>();
	    	
    		decor.add("minecraft:gold_block");
    		decor.add("minecraft:diamond_block");
    		decor.add("minecraft:wooden_slab");
    		decor.add("minecraft:chest");
    		decor.add("minecraft:birch_stairs");
    		decor.add("minecraft:spruce_stairs");
    		decor.add("minecraft:stone_brick_stairs");
    		decor.add("minecraft:stone_pressure_plate");
    		decor.add("minecraft:stone_slab");
    		decor.add("minecraft:cauldron");
    		decor.add("minecraft:lever");
    		decor.add("minecraft:quartz_stairs");
    		decor.add("minecraft:furnace");
    		decor.add("minecraft:jukebox");
    		decor.add("minecraft:brewing_stand");
    		decor.add("minecraft:trapped_chest");
    		decor.add("minecraft:flower_pot");
    		decor.add("minecraft:log");
    		decor.add("minecraft:wool");
    		decor.add("minecraft:crafting_table");
    		decor.add("minecraft:sea_lantern");
    		decor.add("minecraft:bookshelf");
    		decor.add("minecraft:spruce_fence");
    		decor.add("minecraft:dark_oak_fence");

	    	return decor;
	    }
	    
	    private List<String> createWallList() {
	    	List<String> walls = new ArrayList<String>();
	    	
	    	walls.add("minecraft:iron_block");
	    	walls.add("minecraft:stained_hardened_clay");
	    	walls.add("minecraft:hardened_clay");
	    	walls.add("minecraft:wood");
	    	walls.add("minecraft:planks");
	    	walls.add("minecraft:quartz_block");
	    	walls.add("minecraft:brick_block");
	    	walls.add("minecraft:bone_block");
	    	walls.add("minecraft:bedrock");	    	
	    	walls.add("minecraft:stonebrick");
	    	
	    	return walls;
	    }
	    
	    private List<String> createDoorList() {
	    	List<String> doors = new ArrayList<String>();
	    	
	    	doors.add("minecraft:birch_door");
	    	doors.add("minecraft:dark_oak_door");
	    	doors.add("minecraft:wooden_door");
	    	doors.add("minecraft:spruce_door");
	    	doors.add("minecraft:acacia_door");
	    	doors.add("minecraft:iron_door");
	    	
	    	return doors;
	    }
	    
	    private List<String> createTransparentList() {
	    	List<String> transparent = new ArrayList<String>();
	    	
	    	transparent.add("minecraft:glass");
	    	transparent.add("minecraft:glass_pane");
	    	transparent.add("minecraft:stained_glass_pane");
	    	transparent.add("minecraft:stained_glass");
	    	
	    	return transparent;
	    }
}