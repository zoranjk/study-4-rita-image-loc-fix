# Data Subtype: Field of View Message Format
This data message subtype is used to communicate a summary of blocks in the player's Field of View from the PyGLFoVAgent to any component on the message bus that is interested in it. 

## TOPIC

agent/pygl_fov/player/3d/summary

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format section
| msg | object | From the Common Event Message Format section 
| data.observation | integer | An observation number that corresponds to that in a PlayerState message
| data.participant_id | string | The id of the entity whose FoV is summarized
| data.blocks | list | A list consisting of summary information for each block in the Field of View

### Blocks Fields

| Field Name    | Type   | Description
|---------------|--------|-------------
| location      | list   | The (x,y,z) location of the block in Minecraft coordinates
| type          | str    | The type of the block
| number_pixels | int    | The number of pixels the block occupies in the player's FoV
| bounding_box  | object | The boundary (i.e., (x_min, x_max) and (y_min, y_max)) of the block in the player's FoV
| attributes    | object | Current observable attributes of the block


## Message Example

```json
{
"header": {
    "timestamp": "2020-07-10T14:48:52.041004Z",
    "message_type": "observation", 
    "version": "0.5" 
    }, 
"msg": {
    "experiment_id": "3a99e95e-11ab-4668-932a-55c645579ea4", 
    "trial_id": "f955eed1-8a82-4919-86d7-3c5652977470", 
    "timestamp": "2020-07-10T14:48:52.041004Z",
    "source": "PyGL_FoV_Agent", 
    "sub_type": "FoV", 
    "version": "0.5"
    }, 
"data": {
	"participant_id": "Player18", 
	"observation": 10797, 
	"blocks": [
		    { 	"type": "block_bomb_standard",
		    	"location": [34, 63, 76],
		    	"number_pixels": 40,
		    	"bounding_box": {
		    		"x": [304, 310],
		    		"y": [14,18]
		    	},
		    	"attributes": {
		    		"testbed_id": "BOMB15",
		    		"triggering_entity": null,
		    		"sequence_index": "0",
		    		"sequence": "BG",
		    		"active": "false",
		    		"outcome": "INACTIVE"
		    	}
    		}, 
			{ 	"type": "block_bomb_volatile",
		    	"location": [40, 52, 142],
		    	"number_pixels": 2548,
		    	"bounding_box": {
		    		"x": [794, 850],
		    		"y": [162, 222]
		    	},
		    	"attributes": {
		    		"testbed_id": "BOMB27",
		    		"triggering_entity": null,
		    		"sequence_index": "0",
		    		"sequence": "RG",
		    		"active": "false",
		    		"outcome": "INACTIVE"
		    	}
    		}
        ]
    }
}
```
