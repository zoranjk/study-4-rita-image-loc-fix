# Trial Message Format
A Request message requests Exeperiment and Trial ID's from the Control Microservice
# MQTT Topic
"control/request/getTrialInfo"
## Message Fields
There are no fields, this is an empty json object.
## Message Examples
```json
{ }
```
