# LogMessage Message Format
Base64 encoded log message.  

## TOPIC

metadata/log

## Message Fields

| Field Name | Type | Description|
 --- | --- | ---
| encoded_string | string | Base64 encoded log message.

## Message Examples
```json
{
  "encoded_string": "MTk6MDM6MjEuNzAyIFtNUVRUIENhbGw6IGU4ZmI4MmMyLTY3YWEtMTFlYS1iYzU1LTAyNDJhYzEzMDAwM10gRVJST1IgbWV0YWRhdGEuYXBwLnNlcnZpY2UuRGVmYXVsdEV4cGVyaW1lbnRTZXJ2aWNlIC0gZHVwbGljYXRlIGtleSB2YWx1ZSB2aW9sYXRlcyB1bmlxdWUgY29uc3RyYWludCAiZXhwZXJpbWVudF9pZF91cSI="
}
```
