# Trial Message Format
Auto export message.  

## TOPICS

export/auto

## Message Fields

| Field Name                | Type        | Description                                            |
| ------------------------- | ----------- | ------------------------------------------------------ |
| header                    | object      | From Common Header Format section                      |
| msg                       | object      | From the Common Event Message Format section           |
| data.state                | string enum | the new state of the mission ['begin', 'end' ]         |

## Message Examples
```json
{
  "header": {
    "timestamp": "2019-12-26T14:05:02.3412Z",
    "message_type": "export",
    "version": "0.5"
  },
  "msg": {
    "sub_type": "auto",
    "source": "matadata-app",
    "experiment_id": "946d4567-ab65-cfe7-b208-426305dc1234",
    "trial_id": "123e4567-e89b-12d3-a456-426655440000",
    "timestamp": "2019-12-26T15:01:00.325Z",
    "version": "1.0"
  },
  "data": {
	"state": "begin",
  }
}
```