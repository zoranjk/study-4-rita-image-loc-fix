# Client Info Message Format
Client info parameters assigned by experimenter.

## TOPICS

trial
## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| callsign | string | (optional) The callsign of the player 
| participant_id | string | (optional) The participantid of the player  


## TOPIC
trial

## Message Example

```json
{
      "callsign" : "Bravo",
      "participant_id" : "foo"

}

```