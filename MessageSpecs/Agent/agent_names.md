### Agent Names
This file list all of the registered agents that run in the testbed.  All agents that want to collaborate within the testbed should be listed here and the name they use.  All agent names should be unique

| Agent name | Description | Deployment Method | Responsible Group |
| --- | --- | --- | --- |
| gui | ASIST Control Center | Built from gitlab source repo | Aptima |
| IHMCLocationMonitorAgent | Location monitor agent | Built from gitlab source repo | IHMC Roger Carff|
| metadata-web | Import/Export dashboard | Built from gitlab source repo | Aptima |
| PyGL_FoV_Agent | Field of View Agent | Pull from gitlab container registry | CMU Dana
| simulator | Minecraft ASIST Mod | Built from gitlab source repo | Aptima |
| tomcat_speech_analyzer | Speech analyzer | Pull from gitlab container registry | UAZ Adarsh
| Aptima_reference_agent | Reference implementation of a message bus agent | Built from gitlab source repo | Aptima |
| CMU TA2 TED AC |  Team Effectiveness Diagnostic AC  | Built from gitlab source repo | SIFT/CMU TA2 |
| CMU TA2 BEARD AC | Background of Experience, Affect, and Resources Diagnostic AC | Built from gitlab source repo | SIFT/CMU TA2 |