{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",    
  "type": "object",
  "title": "The Mission Stage Transition Schema",
  "version": "1.0.0",
  "required": [
    "header",
    "msg",
    "data"
  ],
  "properties": {
      "header":{
          "type": "object",
          "title": "ASIST message common header Schema",
          "version": "1.2",
          "required": [
              "timestamp",
              "message_type",
              "version"
            ],
            "properties": {
              "timestamp": {
                "$id": "#/properties/header/properties/timestamp",
                "type": "string",
                "title": "The Timestamp Schema",
                "examples": [
                  "2019-12-26T12:47:23.1234Z"
                ],
                "pattern": "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}[.]?[0-9]{0,}?Z"
              },
              "message_type": {
                "$id": "#/properties/header/properties/message_type",
                "type": "string",
                "title": "The Message_type Schema",
                "enum": [ "control","simulator_event", "status", "metadata", "agent" ],
                "examples": [
                  "observation"
                ],
                "pattern": "^([a-z_]*?)$"
              },
              "version": {
                "$id": "#/properties/header/properties/version",
                "type": "string",
                "title": "The message format version",
                "examples": [ "1.0", "2.3.1" ],
                "pattern": "^([0-9]+\\.)?([0-9]+\\.)?([0-9]+)$"
              }
            }
      },
      "msg":{
          "type": "object",
          "title": "The Event Message Schema",
          "version": "0.7",
          "required": [ "trial_id","experiment_id","source", "sub_type", "version" ],
          "properties": {
              "experiment_id": {
                  "$id": "#/properties/msg/properties/experiment_id",
                  "type": "string",
                  "title": "The Experiment Id Schema",
                  "default": "",
                  "examples": [ "123e4567-e89b-12d3-a456-426655440000" ],
                  "pattern": "(([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})|Not Set|Null|null)"
              },
              "trial_id": {
                  "$id": "#/properties/msg/properties/trial_id",
                  "type": "string",
                  "title": "The Trial Id Schema",
                  "default": "",
                  "examples": [ "123e4567-e89b-12d3-a456-426655440000" ],
                  "pattern": "(([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})|Not Set|null|Null)"
              },
              "source": {
                  "$id": "#/properties/msg/properties/source",
                  "type": "string",
                  "title": "The Source Schema",
                  "default": "",
                  "examples": [ "simulator" ],
                  "pattern": "^(.*)$"
              },
              "sub_type": {
                  "$id": "#/properties/msg/properties/sub_type",
                  "type": "string",
                  "title": "The Sub Type Schema",
                  "default": "",
                  "examples": [ "Event:MissionStageTransition" ],
                  "pattern": "^(.*)$"
              },
              "version": {
                  "$id": "#/properties/msg/properties/version",
                  "type": "string",
                  "title": "The Version Schema",
                  "default": "",
                  "examples": [ "0.1", "1.0.1" ],
                  "pattern": "^([.]*)|(0|[1-9]\\d*)\\.(0|[1-9]\\d*)\\.(0|[1-9]\\d*)(?:-((?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\\.(?:0|[1-9]\\d*|\\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\\+([0-9a-zA-Z-]+(?:\\.[0-9a-zA-Z-]+)*))?$"
              },
              "replay_parent_type": {
                  "$id": "#/properties/msg/properties/replay_parent_type",
                  "type": ["string","null"],
                  "title": "The Replay Parent Type Schema",
                  "enum": ["TRIAL", "REPLAY", null]
              },
              "replay_parent_id": {
                  "$id": "#/properties/msg/properties/replay_parent_id",
                  "type": ["string", "null"],
                  "title": "The Replay Parent Id Schema",
                  "default": "",
                  "examples": [ "123e4567-e89b-12d3-a456-426655440000" ],
                  "pattern": "(([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})|Not Set|null|Null)"
              },
              "replay_id": {
                  "$id": "#/properties/msg/properties/replay_id",
                  "type": ["string", "null"],
                  "title": "The Replay Id Schema",
                  "default": "",
                  "examples": [ "123e4567-e89b-12d3-a456-426655440000" ],
                  "pattern": "(([0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12})|Not Set|null|Null)"
              }
          }
      },
    "data": { 
      "type": "object",
      "$id":"#/properties/data",
      "title": "The Object State Data Schema",
      "version": "0.0.0",
      "required": [  
        "team_budget",          
        "mission_stage",
        "transitionsToShop",
        "mission_timer",
        "elapsed_milliseconds" 
      ],
      "properties": {
        "mission_timer":{
          "$id":"#/properties/data/properties/mission_timer",
          "type": "string",
          "title": "The Mission Timer Schema",          
          "examples": [
            "8 : 36"
          ],
          "pattern": "^(.*)$"
        },
        "elapsed_milliseconds":{
          "$id":"#/properties/data/properties/elapsed_milliseconds",
          "type": "number",
          "title": "The Elapsed Milliseconds Schema",          
          "examples": [
            5000, 77459, 689457
          ],
          "pattern": "^[0-9]+$"
        },
        "mission_stage":{
          "$id":"#/properties/data/properties/mission_stage",
          "type": "string",
          "title": "The Mission Stage Schema",
          "enum": [ "RECON_STAGE","SHOP_STAGE", "FIELD_STAGE"],
          "examples": [                
            "SHOP_STAGE"
          ]          
        },
        "transitionsToShop":{
          "$id":"#/properties/data/properties/transitionsToShop",
          "type": "number",
          "title": "The Transitions To Shop Schema",
          "examples":[
            10
          ],
          "pattern": "^([0-9]+?)$"
        },
        "transitionsToField":{
          "$id":"#/properties/data/properties/transitionsToField",
          "type": "number",
          "title": "The Transitions To Field Schema",
          "examples":[
            10
          ],
          "pattern": "^([0-9]+?)$"
        },
        "team_budget":{
          "$id":"#/properties/data/properties/team_budget",
          "type": "number",
          "title": "The Team Budget Schema",
          "examples":[
            100
          ],
          "pattern": "^([0-9]+?)$"
        }
      }      
    }      
  }
}  

