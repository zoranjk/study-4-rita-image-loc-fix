## TOPIC

observations/events/stage_transition

## Message Fields

| Field Name                | Type        | Description                                            |
| ------------------------- | ----------- | ------------------------------------------------------ |
| header                    | object      | From Common Header Format section                      |
| msg                       | object      | From the Common Event Message Format section           |
| data.mission_timer        | string      | the mission time of the event                          |
| data.elapsed_milliseconds | number      | the number of elapsed milliseconds since mission start |
| data.mission_stage        | string      | SHOP_STAGE,FIELD_STAGE              |
| data.transitionsToField    | number | The number of times players have transitions to the Field - (INCLUDING THE RECON STAGE - meaning they spawn in the shop, so the first transition to the Field is RECON) |
| data.transitionsToShop    | number | The number of times players have transitions to the SHOP - (NOT INCLUDING SPAWNING INTO  THE MAP IN THE SHOP) |
| data.team_budget          | number      | The current team budget             |

## Message Example

```json
{
  "header": {
    "timestamp": "2019-12-26T12:47:23.1234Z",
    "message_type": "event",
    "version": "0.5"
  },
  "msg": {
    "experiment_id": "563e4567-e89b-12d3-a456-426655440000",
    "trial_id": "123e4567-e89b-12d3-a456-426655440000",
    "timestamp": "2019-12-26T14:05:02.1412Z",
    "source": "simulator",
    "sub_type": "Event:MissionStageTransition",
    "version": "1.0.0"
  },
  "data": {
    "mission_timer": "17 : 03",
    "elapsed_milliseconds": 1,
    "mission_stage": "SHOP_STAGE",
    "transitionsToField": 1,
    "transitionsToShop": 1,
    "team_budget": 100
  }
}
```

## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
| 1.0.0 | 03/13/2022 | Added transitionsToFeild key, added [RECON_STAGE] to mission_stage enum 
| 0.0.0 | 10/12/2022 | Initial MessageSpec created
