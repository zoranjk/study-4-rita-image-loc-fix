## TOPIC

item/discard

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format |  
| msg | object | From Common Message Format |  
| data.mission_timer | string | the mission time of the event |
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start |
| data.item_name | string | The registered name of the item in synthetic environment |
| data.participant_id | string | Which user input key was pressed to use this item |
| data.amount_discarded | integer | the amount discarded |


## Message Example

```json
{  
  "header": {
    "timestamp": "2023-01-10T01:26:50.077Z",
    "message_type": "simulator_event",
    "version": "1.2"
  },
  "msg": {
    "experiment_id": "946d4567-ab65-cfe7-b208-426305dc1234",
    "trial_id": "85ed4567-ab65-cfe7-b208-426305dc1234",
    "source": "MINECRAFT_SERVER",
    "sub_type": "Event:ItemUsed",
    "version": "0.0.0"
  },
  "data": {
    "participant_id": "P000008",
    "item_name":"WIRECUTTERS_RED",
    "amount_discarded":3
  }
}

```
## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
| 0.0.0 | 03/22/2023 | Initial MessageSpec created |