# HOW TO USE THIS MESSAGE

This JSON structure serves as a utility to package perspective surveys into.  It is NOT the structure that will be used to 
communicate survey results, but is instead a convenient way to represent survey data in template form. Any surveys that will
be presented to participants should be provided to TA3 in this form.


## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| participant_id | string | The pid of the player taking the survey
| surveys.survey_name | string | This is the unique name identifying this survey against other surveys
| surveys.survey_id | string | The unique survey identifier
| data | array of survey_items | This represents the main survey data
| survey_item.id | string | A string representation of an id for this survey item
| survey_item.text | string | A string representation of the survey question
| survey_item.input_type | enum |From ENUM --> [ "MULTI_CHOICE,TEXT,SLIDER"]
| survey_item.image_asset | the filepath of an optional image associated with this survey_item
| survey_item.labels | an array, going from lowest value to highest value. representing the labels placed on MULTI_CHOICE survey_items
## Message Example

```json
{ "participant_id":"p100",
  "surveys":[
  {
    "survey_name":"Environmental Spatial Ability",
    "survey_id":"SPATIAL",
    "data":[
      { 
        "qid":"1",
        "text":"Is a hot dog a sandwich?",
        "input_type": "MULTI_CHOICE",
        "image_asset":null,
        "labels":[
          "Yes",
          "No"
        ]
      },
      { 	
        "qid":"2",
        "text":"What do you see?",
        "input_type": "TEXT",
        "image_asset":"test.png"
      },
      { 	
        "qid":"3",
        "text":"On a scale of Not Good to Amazing, rate yourself!",
        "input_type": "MULTI_CHOICE",
        "image_asset":null,
        "labels":[
          "Not Good",
          "Meh",
          "Middling",
          "My mother says I'm great",
          "Amazing"
        ]
      }
    ]
  }
  ]  
}

```

## CHANGE HISTORY

| VERSION | DATE | DETAILS |
| --- | --- | ---|
| 0.0.1 | 01/14/2022 | Added survey_id and turned structure into array of surveys |
| 0.0.0 | 11/18/2022 | Initial Schema Creation |
