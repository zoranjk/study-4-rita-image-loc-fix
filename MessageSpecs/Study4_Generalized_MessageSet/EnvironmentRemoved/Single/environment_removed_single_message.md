## TOPIC

environment/removed/single

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format section
| msg | object | From the Common Event Message Format section 
| data.mission_timer | string | the mission time of the event
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start
| data | An Object State | Object state representing whichever object is being removed from the physical rendering of the game level - triggered by either a plyaer or the server responding to custom protocol ( like perturbations )


## Message Example

```json

"header": {

    "message_type": "simulator_event",
    "version": "1.2",
    "timestamp": "2022-12-05T21:53:31.566Z"

},
"msg": {

    "trial_id": "20046ec2-919e-4828-9c7f-82bae4f2fafb",
    "experiment_id": "7fc30a4a-a0cd-4294-ab3d-e043b15cf0ae",
    "sub_type": "Event:EnvironmentRemovedSingle",
    "source": "MINECRAFT_SERVER",
    "version": "1.0.0"
 },
"data": { 
        
    "triggering_entity": "P00011", 
    "mission_timer":"8 : 36",
    "elapsed_milliseconds": 7000,
    "obj":{           
        "elapsed_milliseconds": 7000,
        "mission_timer":"8 : 36",
        "x": 28,
        "y": 52,
        "z": 107,
        "id": "ITEMSTACK0_ITEM10",
        "type": "block_beacon_bomb",
            "currAttributes": {
                "uses_remaining": "0"
        }
    }       
}

```

## CHANGE HISTORY

VERSION | DATE | DETAILS|
| --- | --- | --- |
| 1.0.0 | 02/10/2022 | "triggeringEntity" key changed to "triggering_entity" for uniform json casing
| 0.0.0 | 02/03/2023 | Initial MessageSpec created|
