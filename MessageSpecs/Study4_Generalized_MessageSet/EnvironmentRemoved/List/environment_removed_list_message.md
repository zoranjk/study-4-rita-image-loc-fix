## TOPIC

environment/removed/list

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format section
| msg | object | From the Common Event Message Format section 
| data.mission_timer | string | the mission time of the event
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start
| data.list | Array of Object States | An array of object states representing whichever objects are being removed from physical instantiation in the gamemap, either by the player, or by the custom game protocol


## Message Example

```json

"header": {
        "message_type": "simulator_event",
        "version": "1.2",
        "timestamp": "2022-12-05T21:53:31.566Z"
    },
"msg": {
        "trial_id": "20046ec2-919e-4828-9c7f-82bae4f2fafb",
        "experiment_id": "7fc30a4a-a0cd-4294-ab3d-e043b15cf0ae",
        "sub_type": "Event:EnvironmentRemovedList",
        "source": "MINECRAFT_SERVER",
        "version": "1.0.0"
    },
"data": {
    "mission_timer":"8 : 36",
	"elapsed_milliseconds": 15113,
    "triggering_entity": "SERVER",
        "list": [
            {
                "currentAttributes": {
                    "sequence": "RGB",
                    "sequence_index": "0",
                    "active": "false",
                    "outcome": "INACTIVE"
                },
                "elapsed_milliseconds": 0,
                "x": 28,
                "y": 52,
                "z": 107,
                "id": "BOMB1",
                "type": "block_bomb_standard"
            },
            {
                "currentAttributes": {
                    "sequence": "RGB",
                    "sequence_index": "0",
                    "active": "false",
                    "outcome": "INACTIVE"
                },
                "elapsed_milliseconds": 0,
                "x": 26,
                "y": 52,
                "z": 108,
                "id": "BOMB2",
                "type": "block_bomb_volatile"
            },
            {
                "currentAttributes": {
                    "sequence": "B",
                    "sequence_index": "0",
                    "active": "false",
                    "outcome": "INACTIVE"
                },
                "elapsed_milliseconds": 0,
                "x": 18,
                "y": 52,
                "z": 148,
                "id": "BOMB3",
                "type": "block_bomb_standard"
            },
            {
                "currentAttributes": {
                    "sequence": "GR",
                    "sequence_index": "0",
                    "active": "false",
                    "outcome": "INACTIVE"
                },
                "elapsed_milliseconds": 0,
                "x": 42,
                "y": 52,
                "z": 127,
                "id": "BOMB4",
                "type": "block_bomb_standard"
            }

        ]
    }
```

## CHANGE HISTORY

VERSION | DATE | DETAILS |
| --- | --- | --- |
| 1.0.0 | 02/10/2022 | "triggeringEntity" key changed to "triggering_entity" for uniform json casing
| 0.0.0 | 02/03/2022 | Initial MessageSpec created |
