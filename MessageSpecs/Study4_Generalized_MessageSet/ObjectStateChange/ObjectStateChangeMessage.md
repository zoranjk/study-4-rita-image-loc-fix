## TOPIC

object/state/change

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format |  
| msg | object | From Common Message Format |  
| data.mission_timer | string | the mission time of the event |
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start |
| data.type | string | which entity affected the change |
| data.triggering_entity | string | the x location of the object |
| data.x | integer | the x location of the object |
| data.y | integer | the y location of the object |
| data.z | integer | the z location of the object |
| data.id | string | the unique id of the object |
| data.changedAttributes | object | Map<String,String[]> representing any changed attributes in their previous and current values |
| data.currentAttributes | object | Map<String,String> representing the current attribute state of the object |

## Message Example

```json
{
    "header": {
        "timestamp": "2019-12-26T12:47:23.1234Z",
        "message_type": "simulator_event",
        "version": "0.4"
	},
    "msg": { 
        "experiment_id": "523e4567-e89b-12d3-a456-426655440000",
        "trial_id": "123e4567-e89b-12d3-a456-426655440000",	
        "source": "minecraft_server",
        "sub_type": "Event:ObjectStateChange",
        "version": "1.0.2"
    },
    "data": {
        "mission_timer":"8 : 36",
        "elapsed_milliseconds": 15113,
        "type": "bomb_block_standard",	
        "id": "BOMB_25",
        "triggering_entity":"P0000111",	
        "x": -2185,
        "y":28,
        "z":198,
        "changedAttributes":{
            "active":["false","true"],
            "outcome":["INACTIVE","TRIGGERED_ADVANCE_SEQUENCE"],
            "sequence":["RGB","GB"],
            "sequence_index":["0","1"]
        },
        "currAttributes":{
            "active":"true",
            "outcome":"TRIGGERED_ADVANCE_SEQUENCE",
            "sequence":"GB",
            "sequence_index":"1",
            "fuse_start_minute":"7"
        }	
	}
}

```

## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
| 1.0.2 | 03/31/2023 | "fuse_start_minute" field to bomb_state objects
| 1.0.1 | 03/15/2023 | Added two BOMB_STATE outcome enums [EXPLODE_FIRE,PERTURBATION_FIRE_TRIGGER]
| 1.0.0 | 02/10/2022 | Added "triggering_entity" to determine which entity affected the change
| 0.0.0 | 12/05/2022 | Initial MessageSpec created