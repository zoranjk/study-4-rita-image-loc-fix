## TOPIC

communication/environment

## USAGE
Fired when a rendered environment object emits a message to the players

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format section |
| msg | object | From the Common Event Message Format section |
| data.mission_timer | string | the mission time of the event |
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start |
| data.message_id | string | The unique id of this message |
| data.sender_id | string | The id of the object sending the message |
| data.recipients | string[] | An array of ids of the recipients of the message |
| data.message | string | The raw form of the message |
| data.sender_x | int | The x position of the object sending the message |
| data.sender_y | int | The y position of the object sending the message |
| data.sender_z | int | The z position of the object sending the message |
| data.additional_info | json object (nullable) | additional info that may be of use when processing the object


## Message Example

```json

{
  "header": {
    "timestamp": "2023-02-10T22:26:37.364Z",
    "message_type": "simulator_event",
    "version": "1.2"
  },
  "msg": {
    "experiment_id": "946d4567-ab65-cfe7-b208-426305dc1234",
    "trial_id": "85ed4567-ab65-cfe7-b208-426305dc1234",
    "source": "MINECRAFT_SERVER",
    "sub_type": "Event:CommunicationChat",
    "version": "0.0.0"
  },
  "data": {
    "mission_timer": "0 : 10",
    "elapsed_milliseconds": 49453,
    "message_id":"COMM_ENV0",
    "sender_id": "P000007",
    "sender_type":"block_beacon_hazard",
    "recipients": [
      "P000001","P000004"
    ],
    "sender_x":30,
    "sender_y":51,
    "sender_z":100,
    "message": "I need a Green Wirecutter",
    "additional_info":{
      "text": "I need a Green Wirecutter"
    }
  }
}
```

## CHANGE HISTORY

| VERSION | DATE | DETAILS |
| --- | --- | --- |
| 1.0.1 | 03/29/2023 | Added fuse_start_minute field to bombBeaconInfo communication |
| 1.0.0 | 03/22/2023 | Corrected various inconsistencies:  Added message_id, sender_x, sender_y, sender_z, sender_type, removed environment key as these are all in the environment |
| 0.0.1 | 03/15/2023 | Added bombBeaconInfo and hazardBeaconInfo models as additional_info refs |
| 0.0.0 | 02/10/2023 | Initial Spec Created |

