## TOPIC

agent/intervention/<unique_agent_name>/response


## Message Fields

| Field Name | Type | Description
| --- | --- | --- |
| header | object | See the Common Header Directory |
| msg | object | From the Common Event Message Format section |
| data.intervention_id | id | a unique id for the interventions |
| data.sender_id | string | the participant_id of the responding player
| data.agent_id | string | the unique id of the agent for which the response it meant
| data.response_index | number | the integer index of the option selected by the player as a response



## Message Example

```json
{ 
  "header": {
    "timestamp": "2019-12-26T14:05:02.3412Z",
    "message_type": "agent",
    "version": "0.1"
  },
  "msg": {
    "trial_id": "123e4567-e89b-12d3-a456-426655440000",
    "timestamp": "2019-12-26T14:05:02.1412Z",
    "replay_id": "a44120f7-b5ba-47a3-8a21-0c76ede62f75",
    "replay_root_id": "123e4567-e89b-12d3-a456-426655440000",
    "source": "tom_generator:1.0",
    "sub_type": "Event:InterventionResponse",
    "version": "0.0.0"
  },
  "data": {      
    "intervention_id":"34a",
    "participant_id": "P000007",
    "response_index": 2,
    "agent_id": "ASI_DOLL_TA1_Rita"
   }
}
```


## Version Change History
VERSION | DATE | DETAILS
| --- | --- | --- |
| 0.0.0 | 2/20/2022 | Initial Message Spec created. 

