## TOPIC

agent/intervention/<unique_agent_name>/chat


## Message Fields

| Field Name | Type | Description
| --- | --- | --- |
| header | object | See the Common Header Directory |
| msg | object | From the Common Event Message Format section |
| data.id | id | a unique id for the interventions
| data.created | string timestamp | the time when this intervention was created in a ZULU/UCT time string
| data.start | integer | The time the prediction/intervention is effective. Specified in mission elapsed time in milliseconds.  If the start time is a number less than 0 like -1, the intervention should start immediately.
| data.duration | integer | The length of time in milliseconds the prediction/intervention remains valid. In the case of a block, this is the duration the block should remain in the world before being removed
| data.response_options | string array | provides an array of possible responses the participant may select in response to an agent chat message
| data.explanation | json object | this field contains what ever the agent wants to include with this specific intervention that can be used for post experiment analysis.
| data.content | string | The message from the agent to display to the player/user if using Intervention:Chat 
| data.receivers | string array | The participant ids who should receive the chat message.  Each participant id in the list will receive the intervention message.
| data.type | enum | Type of data to be displayed. ["string"]
| data.renderers | string array |  The renderer used to display the content. ["Minecraft_Chat"]



## Message Example

```json
{ 
  "header": {
    "timestamp": "2019-12-26T14:05:02.3412Z",
    "message_type": "agent",
    "version": "0.1"
  },
  "msg": {
    "trial_id": "123e4567-e89b-12d3-a456-426655440000",
    "timestamp": "2019-12-26T14:05:02.1412Z",
    "replay_id": "a44120f7-b5ba-47a3-8a21-0c76ede62f75",
    "replay_root_id": "123e4567-e89b-12d3-a456-426655440000",
    "source": "ASI_TEST_AGENT",
    "sub_type": "Intervention:Chat",
    "version": "1.0.0"
  },
  "data": {
      "id": "<uuid for the intervention>",
      "created": "2019-12-26T14:05:02.3412Z",
      "start": -1,
      "duration": 10000,
      "content": "Agent 1: Victim needs assistance near you",
      "receivers": ["P000132"],
      "explanation": "{<agent custom json object>}",
      "response_options":[
            "That's great advice!",
            "That's really unhelpful.",
            "Not right now."
          ]        
   }
}
```


## Version Change History
VERSION | DATE | DETAILS
| --- | --- | --- |
| 1.0.0 | 2/20/2022 | Added optional "response_options" key, of type string array, to represent reponses the participant may make to an intervention.  Removed "type" and "renderers" from required keys as rendering is out of the agent's control.  Changed sub_type to Event:InterventionChat
| 0.6.0 | 1/25/2022 | fixed error in documentation where the table showed the source property in the data section when it should be in the msg section
| 0.5.0 | 3/1/2021 | Ported from Adapt

