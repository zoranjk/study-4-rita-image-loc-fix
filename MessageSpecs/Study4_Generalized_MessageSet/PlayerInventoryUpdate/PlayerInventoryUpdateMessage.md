## TOPIC

player/inventory/update

## Message Fields

| Field Name | Type | Description
| --- | --- | ---|
| header | object | From Common Header Format |  
| msg | object | From Common Message Format |  
| data.mission_timer | string | the mission time of the event |
| data.elapsed_milliseconds | number | the number of elapsed milliseconds since mission start |
| data.currInv | Map\<String,Map\<String,String\>\> | A Map, where the keys are the player PID's, and the values are other maps with keys represneted as item names and values represented as counts

## Message Example

```json
{
  "header": {
    "timestamp": "2023-02-28T15:47:40.616Z",
    "message_type": "simulator_event",
    "version": "1.2"
  },
  "msg": {
    "experiment_id": "946d4567-ab65-cfe7-b208-426305dc1234",
    "trial_id": "85ed4567-ab65-cfe7-b208-426305dc1234",
    "source": "MINECRAFT_SERVER",
    "sub_type": "0.0.0",
    "version": "Event:InventoryUpdate"    
  },
  "data": {
    "mission_timer": "0 : 47",
    "elapsed_milliseconds": 118232,
    "currInv": {
      "P000007": {
        "BOMB_SENSOR": 10,
        "FIRE_EXTINGUISHER": 1,
        "BOMB_PPE": 10,
        "WIRECUTTERS_RED": 10,
        "HAZARD_BEACON": 10,
        "WIRECUTTERS_GREEN": 10,
        "WIRECUTTERS_BLUE": 10,
        "BOMB_BEACON": 10,
        "BOMB_DISPOSER": 10
      },
      "P000008": {
        "BOMB_SENSOR": 10,
        "FIRE_EXTINGUISHER": 1,
        "BOMB_PPE": 10,
        "WIRECUTTERS_RED": 10,
        "HAZARD_BEACON": 10,
        "WIRECUTTERS_GREEN": 10,
        "WIRECUTTERS_BLUE": 10,
        "BOMB_BEACON": 10,
        "BOMB_DISPOSER": 10
      },
      "P000009": {
        "BOMB_SENSOR": 10,
        "FIRE_EXTINGUISHER": 1,
        "BOMB_PPE": 10,
        "WIRECUTTERS_RED": 10,
        "HAZARD_BEACON": 10,
        "WIRECUTTERS_GREEN": 10,
        "WIRECUTTERS_BLUE": 10,
        "BOMB_BEACON": 10,
        "BOMB_DISPOSER": 10
      }
    }
  }
}

```

## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
| 0.0.0 | 03/01/2022 | Initial MessageSpec created