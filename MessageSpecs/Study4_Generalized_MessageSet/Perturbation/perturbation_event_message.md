## TOPIC

mission/perturbation/start

## Message Fields

| Field Name                | Type        | Description                                            |
| ------------------------- | ----------- | ------------------------------------------------------ |
| header                    | object      | From Common Header Format section                      |
| msg                       | object      | From the Common Event Message Format section           |
| data.mission_timer        | string      | the mission time of the event                          |
| data.elapsed_milliseconds | number      | the number of elapsed milliseconds since mission start |
| data.type                 | string enum | The name of the perturbation type ['fire']             |
| data.additional_info      | json object | Additional information about the perturbation event |
| data.additional_info.source_pos_x | number | The x position of the source of the perturbation |
| data.additional_info.source_pos_y | number | The y position of the source of the perturbation |
| data.additional_info.source_pos_z | number | The z position of the source of the perturbation |
| data.additional_info.source_id | string | The id of the source object if there is one         |

## Message Example

```json
{
  "header": {
    "timestamp": "2019-12-26T12:47:23.1234Z",
    "message_type": "event",
    "version": "0.5"
  },
  "msg": {
    "experiment_id": "563e4567-e89b-12d3-a456-426655440000",
    "trial_id": "123e4567-e89b-12d3-a456-426655440000",
    "timestamp": "2019-12-26T14:05:02.1412Z",
    "source": "simulator",
    "sub_type": "Event:Perturbation",
    "version": "2.0"
  },
  "data": {
    "mission_timer": "8 : 36",
    "elapsed_milliseconds": 15113,
    "type": "fire",
    "additional_info": {
      "x": 30,
      "y": 51,
      "z": 120,
      "source_id":"BOMB6"
    }
  }
}
```

## CHANGE HISTORY

VERSION | DATE | DETAILS
| --- | --- | --- |
3.0 | 3/15/2023 | Removed mission_state key, type will be ['fire] for Study 4, added additional_info
2.2 | 1/24/2021 | Updated the Message Fields section "state" key to "mission_state" in this file. Also made mission_state a required key as opposed to state
2.1 | 12/23/2021 | Added enum type ['rubble'] to type field
2.0 | 11/30/2021 | Initial spec created
