from enum import Enum, auto
from collections import defaultdict

class Participant:

    def __init__(self, id, callsign=None):
        self.__id = id
        self.__callsign = callsign if callsign is not None else id
        self.inventory = Inventory()
        self.jags = {}

    @property
    def id(self):
        return self.__id

    @property
    def callsign(self):
        return self.__callsign

    @property
    def is_frozen(self):
        return False

    def jags_by_urn_and_input(self, urn, inputs):
        return [jag for jag in self.jags.values() if jag.matches(urn, inputs)]

    def get_instance_data(self):
        return {
            'id': self.__id,
            'inventory': self.inventory.get_instance_data(),
            'frozen': self.is_frozen,
        }

    #def __str__(self):
        #return f"{self.participant_id} {self.callsign:<5} - {self.__location[0]:.2f}, {self.__location[1]:.2f}, {self.__location[2]:.2f}"

class Item(Enum):
    FIRE_EXTINGUISHER = auto()
    HAZARD_BEACON = auto()
    BOMB_BEACON = auto()
    BOMB_DISPOSER = auto()
    BOMB_PPE = auto()
    BOMB_SENSOR = auto()
    WIRECUTTERS_RED = auto()
    WIRECUTTERS_GREEN = auto()
    WIRECUTTERS_BLUE = auto()

class Inventory:

    def __init__(self):
        # initializes the item store with a dictionary defaulting item count to 0
        self.__items = defaultdict(int)
        for item in Item:
            self.set_item_count(item.name, 0)

    def get_item_count(self, item):
        return self.__items[item]

    # sets the specified item to the specified count
    # if count is less than 0 then the item count is set to 0
    def set_item_count(self, item, count):
        self.__items[item] = count if count > 0 else 0
        return self.__items[item]
    
    def use_item(self, item):
        if self.__items[item] > 0:
            self.__items[item] -= 1
            return True, self.__items[item]

        return False, 0

    def get_instance_data(self):
        return self.__items;

    #@property
    #def x(self):
        #return self.__location[0]

    #@property
    #def y(self):
        #return self.__location[1]

    #@property
    #def z(self):
        #return self.__location[2]

    #@x.setter
    #def x(self, value):
        #self.__location[0] = value

    #@y.setter
    #def y(self, value):
        #self.__location[1] = value

    #@z.setter
    #def z(self, value):
        #self.__location[2] = value

    #@property
    #def location(self):
        #return self.__location
