import uuid
import logging
import pprint as pp

class Jag:

    def __init__(self, urn, inputs=None):
        self.uid = uuid.uuid4()
        self.urn = urn
        self.inputs = {} if inputs is None else dict(inputs)

        self.children = {}

        # self.outputs = dict[str, any]

        self.__logger = logging.getLogger(__name__)
    
    def matches(self, urn, inputs):
        # if specified urn differs from this jag's urn
        if urn != self.urn:
            return False

        for key, value in inputs.items():

            if key not in self.inputs:
                return False

            if value != self.inputs[key]:
                return False

        # all inputs are present and match
        return True

    def __repr__(self):
        return f"{self.urn}\n\tinputs:\n\t\t{self.inputs}\n\tchildren:\n\t\t{self.children}"

    def get_instance_data(self):
        data = {
            'urn': self.urn,
            'id': str(self.uid),
            'inputs': self.inputs,
            'outputs': {},
            'children': [],
        }

        for child in self.children:
            child_data = child.get_instance_data()
            data['children'].append(child_data)

        return data

    # def __hash__(self):
        # return hash((self.urn, str(self.inputs), str(self.outputs)))

    # def __eq__(self, other):
        # if not isinstance(other, type(self)): return NotImplemented
        # return self.urn == other.urn and self.inputs == other.inputs and self.outputs == other.outputs
