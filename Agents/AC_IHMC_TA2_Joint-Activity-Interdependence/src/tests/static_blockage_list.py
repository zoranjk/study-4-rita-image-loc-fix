blockages = (
    {
        "block_type": "gravel",
        "feature_type": "FeatureType",
        "room_name": "NA",
        "x": -2217,
        "y": 60,
        "z": -2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 61,
        "z": -2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 62,
        "z": -2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2216,
        "y": 60,
        "z": -2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2216,
        "y": 61,
        "z": -2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 62,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2201,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2200,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2200,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2199,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2199,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2199,
        "y": 62,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 62,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 62,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 62,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2142,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2142,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2141,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2141,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2129,
        "y": 60,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2129,
        "y": 61,
        "z": -1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 62,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 62,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 62,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2141,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2141,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2130,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2130,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2130,
        "y": 62,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2129,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2129,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2129,
        "y": 62,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2115,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2115,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 62,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 62,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2111,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2111,
        "y": 61,
        "z": 0
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 61,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 62,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2206,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2206,
        "y": 61,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 61,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 62,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 62,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 61,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 62,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 61,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 61,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 62,
        "z": 1
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 60,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 61,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2206,
        "y": 60,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2206,
        "y": 61,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 62,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 60,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 61,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 62,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 60,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 61,
        "z": 2
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 62,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2206,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2206,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2205,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2205,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2205,
        "y": 62,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 62,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2195,
        "y": 62,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 3
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 60,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 61,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 60,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 61,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 62,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 62,
        "z": 4
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 62,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2223,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2223,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2222,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2222,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2222,
        "y": 62,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2221,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2221,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 62,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2146,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2146,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 5
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 60,
        "z": 6
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 61,
        "z": 6
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 6
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 6
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 62,
        "z": 6
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 6
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 6
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 62,
        "z": 6
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 60,
        "z": 7
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 61,
        "z": 7
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 7
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 7
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 62,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 62,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2171,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2171,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2171,
        "y": 62,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2166,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2166,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2166,
        "y": 62,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 62,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 62,
        "z": 8
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 9
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 9
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 62,
        "z": 9
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 60,
        "z": 9
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 61,
        "z": 9
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 62,
        "z": 9
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 9
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 9
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 10
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 10
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 60,
        "z": 10
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 61,
        "z": 10
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 60,
        "z": 10
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 61,
        "z": 10
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 62,
        "z": 10
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 60,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 61,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2224,
        "y": 62,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2154,
        "y": 60,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2154,
        "y": 61,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2154,
        "y": 62,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 60,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 61,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 62,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 60,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 61,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 62,
        "z": 11
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 62,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2157,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2156,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2155,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 61,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 61,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 62,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 60,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 61,
        "z": 12
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2218,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2218,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 62,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 62,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2088,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2088,
        "y": 61,
        "z": 13
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 62,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2169,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2169,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2168,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2168,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 62,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2093,
        "y": 60,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2093,
        "y": 61,
        "z": 14
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 62,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 62,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2177,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2177,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2177,
        "y": 62,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2176,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2176,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 62,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 62,
        "z": 15
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 62,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2168,
        "y": 60,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2168,
        "y": 61,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 62,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2093,
        "y": 60,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2093,
        "y": 61,
        "z": 16
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 62,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2157,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2156,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2155,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2127,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 62,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2125,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2125,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 62,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2111,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2111,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2109,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 62,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2093,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2093,
        "y": 61,
        "z": 17
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 62,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2218,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2218,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 62,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 62,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 62,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2193,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2193,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2192,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2192,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2192,
        "y": 62,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2097,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2093,
        "y": 60,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2093,
        "y": 61,
        "z": 18
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 60,
        "z": 19
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 61,
        "z": 19
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 19
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 19
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 60,
        "z": 19
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 61,
        "z": 19
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 19
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 60,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 61,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 62,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 62,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 60,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 61,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 62,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 62,
        "z": 20
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 60,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 61,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 60,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2194,
        "y": 61,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 60,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 61,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 21
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 60,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 61,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2219,
        "y": 62,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2218,
        "y": 60,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2218,
        "y": 61,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 60,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 61,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2217,
        "y": 62,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 60,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 61,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 62,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 62,
        "z": 22
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 60,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 61,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 62,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 23
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 61,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 62,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 61,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 62,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2154,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2154,
        "y": 61,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2154,
        "y": 62,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2149,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2148,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 61,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2125,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2125,
        "y": 61,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2125,
        "y": 62,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 62,
        "z": 24
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2168,
        "y": 60,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2168,
        "y": 61,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2168,
        "y": 62,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 60,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 61,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 62,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 62,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 25
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2157,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2156,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2155,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2132,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2132,
        "y": 61,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 61,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 62,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2130,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2130,
        "y": 61,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 62,
        "z": 26
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 27
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 27
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 27
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 27
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 62,
        "z": 27
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 60,
        "z": 27
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2095,
        "y": 61,
        "z": 27
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2109,
        "y": 60,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 60,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2107,
        "y": 61,
        "z": 28
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 60,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 61,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 62,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 60,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 61,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2162,
        "y": 62,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 29
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 62,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 62,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2167,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2167,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2167,
        "y": 62,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2127,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2125,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2125,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2125,
        "y": 62,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 62,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2111,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 62,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 61,
        "z": 30
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 60,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 61,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2184,
        "y": 60,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2184,
        "y": 61,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2183,
        "y": 60,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2183,
        "y": 61,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2127,
        "y": 60,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 60,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 61,
        "z": 31
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 62,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 60,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 61,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 62,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 62,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 60,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 61,
        "z": 32
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2209,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2209,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2209,
        "y": 62,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 62,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 62,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2124,
        "y": 62,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 62,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2090,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2090,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2090,
        "y": 62,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 61,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2089,
        "y": 62,
        "z": 33
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 62,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 60,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 61,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2202,
        "y": 62,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2199,
        "y": 60,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2199,
        "y": 61,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2199,
        "y": 62,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 60,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 61,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 62,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 62,
        "z": 34
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2172,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2172,
        "y": 61,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2172,
        "y": 62,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 61,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 62,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2138,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2138,
        "y": 61,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 62,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 62,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 35
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2203,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2203,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2203,
        "y": 62,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2200,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2200,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2192,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2192,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 62,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 62,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2139,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2139,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2139,
        "y": 62,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2138,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2138,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 61,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2094,
        "y": 62,
        "z": 36
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2208,
        "y": 62,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2203,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2203,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2200,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2200,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2200,
        "y": 62,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2173,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2173,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2172,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2172,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2172,
        "y": 62,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2166,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2166,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2166,
        "y": 62,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 62,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 62,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 62,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2106,
        "y": 60,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2106,
        "y": 61,
        "z": 37
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 62,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 60,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 61,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2099,
        "y": 60,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2099,
        "y": 61,
        "z": 38
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2173,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2173,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2173,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2171,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2171,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2171,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2115,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2115,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2115,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2109,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2109,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2106,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2106,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2106,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 61,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 62,
        "z": 39
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 61,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2132,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2132,
        "y": 61,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 61,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2131,
        "y": 62,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2127,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2127,
        "y": 61,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2127,
        "y": 62,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 61,
        "z": 40
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 60,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 61,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 62,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 60,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 61,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2165,
        "y": 62,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 62,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 60,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 61,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 62,
        "z": 41
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 60,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 61,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 62,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2139,
        "y": 60,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2139,
        "y": 61,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 60,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 61,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 60,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 61,
        "z": 42
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 60,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 61,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2174,
        "y": 62,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2146,
        "y": 60,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2146,
        "y": 61,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 60,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 61,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2145,
        "y": 62,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2144,
        "y": 60,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2144,
        "y": 61,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 60,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 61,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2143,
        "y": 62,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 60,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 61,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 62,
        "z": 43
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 60,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 61,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2181,
        "y": 62,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 60,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2180,
        "y": 61,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 60,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 61,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 62,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2169,
        "y": 60,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2169,
        "y": 61,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2169,
        "y": 62,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 60,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 61,
        "z": 44
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 60,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 61,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2140,
        "y": 62,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2138,
        "y": 60,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2138,
        "y": 61,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2138,
        "y": 62,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 60,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2137,
        "y": 61,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2136,
        "y": 60,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2136,
        "y": 61,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2136,
        "y": 62,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 60,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 61,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 60,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 61,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2096,
        "y": 62,
        "z": 45
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 60,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 61,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2179,
        "y": 62,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 60,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 61,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2178,
        "y": 62,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2177,
        "y": 60,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2177,
        "y": 61,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2177,
        "y": 62,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2176,
        "y": 60,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2176,
        "y": 61,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2176,
        "y": 62,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 60,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 61,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 62,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 60,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 61,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 46
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2197,
        "y": 62,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2161,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2161,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2160,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2160,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 62,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 61,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 62,
        "z": 47
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 60,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 61,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 62,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 60,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 61,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 62,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 60,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2128,
        "y": 61,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 62,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 60,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 61,
        "z": 48
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 60,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 61,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 60,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2133,
        "y": 61,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 60,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 61,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 62,
        "z": 49
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2222,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2222,
        "y": 61,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2222,
        "y": 62,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2221,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2221,
        "y": 61,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 61,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2220,
        "y": 62,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2182,
        "y": 61,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 61,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 62,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 60,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 61,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 60,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2126,
        "y": 61,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 60,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 61,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 62,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2092,
        "y": 60,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2092,
        "y": 61,
        "z": 51
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2159,
        "y": 60,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 60,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 61,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 62,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 60,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 61,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2092,
        "y": 60,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2092,
        "y": 61,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2092,
        "y": 62,
        "z": 52
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 60,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 61,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2204,
        "y": 62,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2160,
        "y": 60,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2159,
        "y": 60,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 60,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2147,
        "y": 61,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 60,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2108,
        "y": 61,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 60,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 61,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2098,
        "y": 62,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2092,
        "y": 60,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2092,
        "y": 61,
        "z": 53
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 60,
        "z": 55
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 61,
        "z": 55
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2114,
        "y": 62,
        "z": 55
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 60,
        "z": 55
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 61,
        "z": 55
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2113,
        "y": 62,
        "z": 55
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 60,
        "z": 55
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2112,
        "y": 61,
        "z": 55
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2218,
        "y": 60,
        "z": 57
    },
    {
        "block_type": "gravel",
        "feature_type": "obstruction",
        "room_name": "NA",
        "x": -2218,
        "y": 60,
        "z": 58
    }
)

victims = (
    {
        "block_type": "block_victim_1b",
        "unique_id": 1,
        "room_name": "B7",
        "x": -2205,
        "y": 60,
        "z": 0
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 2,
        "room_name": "A4",
        "x": -2221,
        "y": 60,
        "z": 1
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 3,
        "room_name": "B7",
        "x": -2205,
        "y": 60,
        "z": 2
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 4,
        "room_name": "I6",
        "x": -2099,
        "y": 60,
        "z": 3
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 5,
        "room_name": "A4",
        "x": -2223,
        "y": 60,
        "z": 4
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 6,
        "room_name": "E6",
        "x": -2163,
        "y": 60,
        "z": 5
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 7,
        "room_name": "I6",
        "x": -2099,
        "y": 60,
        "z": 7
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 8,
        "room_name": "E6",
        "x": -2159,
        "y": 60,
        "z": 8
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 9,
        "room_name": "G7",
        "x": -2132,
        "y": 60,
        "z": 11
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 10,
        "room_name": "G7",
        "x": -2132,
        "y": 60,
        "z": 13
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 11,
        "room_name": "B6",
        "x": -2204,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 12,
        "room_name": "B6",
        "x": -2202,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 13,
        "room_name": "B6",
        "x": -2200,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 14,
        "room_name": "B6",
        "x": -2198,
        "y": 60,
        "z": 15
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 15,
        "room_name": "B6",
        "x": -2205,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 16,
        "room_name": "B6",
        "x": -2196,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 17,
        "room_name": "E3",
        "x": -2169,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 18,
        "room_name": "G7",
        "x": -2132,
        "y": 60,
        "z": 17
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 19,
        "room_name": "B6",
        "x": -2205,
        "y": 60,
        "z": 19
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 20,
        "room_name": "B6",
        "x": -2196,
        "y": 60,
        "z": 19
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 21,
        "room_name": "B6",
        "x": -2202,
        "y": 60,
        "z": 21
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 22,
        "room_name": "B6",
        "x": -2200,
        "y": 60,
        "z": 21
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 23,
        "room_name": "G5",
        "x": -2130,
        "y": 60,
        "z": 22
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 24,
        "room_name": "G5",
        "x": -2132,
        "y": 60,
        "z": 23
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 25,
        "room_name": "C4",
        "x": -2184,
        "y": 60,
        "z": 30
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 26,
        "room_name": "H4",
        "x": -2109,
        "y": 60,
        "z": 33
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 27,
        "room_name": "D1",
        "x": -2173,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 28,
        "room_name": "D1",
        "x": -2171,
        "y": 60,
        "z": 35
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 29,
        "room_name": "B2",
        "x": -2207,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 30,
        "room_name": "H3",
        "x": -2111,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 31,
        "room_name": "H4",
        "x": -2109,
        "y": 60,
        "z": 36
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 32,
        "room_name": "D2",
        "x": -2166,
        "y": 60,
        "z": 38
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 33,
        "room_name": "H3",
        "x": -2111,
        "y": 60,
        "z": 38
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 34,
        "room_name": "F1",
        "x": -2137,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 35,
        "room_name": "I2",
        "x": -2099,
        "y": 60,
        "z": 39
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 36,
        "room_name": "F1",
        "x": -2139,
        "y": 60,
        "z": 40
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 37,
        "room_name": "F1",
        "x": -2145,
        "y": 60,
        "z": 41
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 38,
        "room_name": "C2",
        "x": -2182,
        "y": 60,
        "z": 42
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 39,
        "room_name": "C2",
        "x": -2179,
        "y": 60,
        "z": 42
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 40,
        "room_name": "H1",
        "x": -2112,
        "y": 60,
        "z": 46
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 41,
        "room_name": "B1",
        "x": -2195,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 42,
        "room_name": "G1",
        "x": -2132,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 43,
        "room_name": "I1",
        "x": -2092,
        "y": 60,
        "z": 47
    },
    {
        "block_type": "block_victim_proximity",
        "unique_id": 44,
        "room_name": "A2",
        "x": -2221,
        "y": 60,
        "z": 48
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 45,
        "room_name": "C1",
        "x": -2181,
        "y": 60,
        "z": 50
    },
    {
        "block_type": "block_victim_1",
        "unique_id": 46,
        "room_name": "H2",
        "x": -2109,
        "y": 60,
        "z": 51
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 47,
        "room_name": "D4",
        "x": -2160,
        "y": 60,
        "z": 52
    },
    {
        "block_type": "block_victim_1b",
        "unique_id": 48,
        "room_name": "A1",
        "x": -2223,
        "y": 60,
        "z": 57
    }
)
