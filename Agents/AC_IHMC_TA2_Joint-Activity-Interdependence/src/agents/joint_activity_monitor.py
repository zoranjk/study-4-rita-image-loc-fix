#!/usr/bin/env python3

"""
IHMC Interdependence Analytic Component

author: Micael Vignati
email: mvignati@ihmc.org
"""

__author__ = 'mvignati'

import uuid
import json
import logging
import traceback
import re
import pprint as pp

from asistagenthelper import ASISTAgentHelper

from ..models.events import JagEvent
from ..models.jags.jag import Jag
from ..models.participant import Participant

JAG_EVENT_TOPIC = 'observations/events/player/jag'
JAG_EVENT_VERSION = "1.2.9"
JAG_EVENT_TYPE = 'event'
JAG_EVENT_SUB_TYPES = {
    JagEvent.BASE: 'Event:JagEvent',
    JagEvent.DISCOVERED: 'Event:Discovered',
    JagEvent.AWARENESS: 'Event:Awareness',
    JagEvent.PREPARING: 'Event:Preparing',
    JagEvent.ADDRESSING: 'Event:Addressing',
    JagEvent.COMPLETION: 'Event:Completion',
    JagEvent.SUMMARY: 'Event:Summary',
}


class JointActivityMonitor(ASISTAgentHelper):
# class JointActivityMonitor():

    def __init__(self):
        super().__init__(self.__on_message_handler)
        self.__logger = logging.getLogger(__name__)
        self.participants: dict[str, Participant] = {}

        self.__topic_handlers = {
            'trial': self.handle_trial,
            'player/state': self.__handle_player_state,
            'object/state/change': self.handle_object_state_change,
            'communication/environment': self.handle_communication_environment,
        }

    def __on_message_handler(self, topic, header, message, data, mqtt_message):
        try:
            # handles explicit topics first
            if topic in self.__topic_handlers:
                self.__topic_handlers[topic](header, message, data)
        except Exception as error:
            error_string = f"\n" \
                           f"error: {str(error)}\n" \
                           f"\tmqtt:\n" \
                           f"\t\ttopic: {topic}\n" \
                           f"\t\theader: {header}\n" \
                           f"\t\tmessage: {message}\n" \
                           f"\t\tdata: {data}\n" \
                           f"\ttraceback: {traceback.format_exc()}"
            self.__logger.error(error_string)

    def handle_trial(self, header, message, data):
        if message['sub_type'] == 'stop':
            self.participants.clear();
            return

        for client_info in data['client_info']:
            participant_id = client_info['participant_id']
            participant_callsign = client_info['callsign']
            participant = Participant(participant_id, participant_callsign)
            self.participants[participant_id] = participant
            self.__logger.info(f"initializing participant {participant}")

        if len(self.participants) != 3:
            self.__logger.info(f"__handle_trial: should have 3 players, but have {len(self.participants)}")

    def __handle_player_state(self, header, message, data):
        participant_id = data['participant_id']
        participant = self.participants[participant_id]

        participant.x = data['x']
        participant.y = data['y']
        participant.z = data['z']
        # self.__logger.info(f"[__handle_player_state] updated participant location {participant}")

    def handle_object_state_change(self, header, message, data):
        participant_id = data['triggering_entity']
        if participant_id not in self.participants:
            return

        participant = self.participants[participant_id]

        elapsed_milliseconds = data['elapsed_milliseconds']

        bomb_id = data['id']
        bomb_sequence = data['currAttributes']['sequence']
        bomb_step = int(data['currAttributes']['sequence_index'])
        outcome = data['currAttributes']['outcome']

        self.__logger.info(f"[__handle_object_state_change] bomb update\n\tid: {bomb_id:8<} seq: {bomb_sequence} step: {bomb_step} outcome {outcome}")

        inputs = {'bomb_id': bomb_id}
        jags = participant.jags_by_urn_and_input('urn:asist:defuse-bomb', inputs)

        # haven't tagged the bomb
        if len(jags) <= 0:
            # should create the bomb jag and continue as normal
            return

        defuse_bomb = jags[0]

        if outcome == 'TRIGGERED_ADVANCE_SEQ':
            completed_cut_wire_jag = defuse_bomb.children[bomb_step - 1]

            # add inventory usage
            self.__publish_message(elapsed_milliseconds, 'success', participant, completed_cut_wire_jag)

            # starts next cut if necessary 
            if bomb_step < len(defuse_bomb.children):
                current_cut_wire_jag = defuse_bomb.children[bomb_step]
                self.__publish_message(elapsed_milliseconds, 'start', participant, current_cut_wire_jag)

        elif outcome == 'EXPLODE_TOOL_MISMATCH':
            self.__publish_message(elapsed_milliseconds, 'failure', participant, defuse_bomb)
            for i in range(bomb_step, len(defuse_bomb.children)):
                self.__publish_message(elapsed_milliseconds, 'failure', participant, defuse_bomb.children[i])

        elif outcome == 'DEFUSED':
            self.__publish_message(elapsed_milliseconds, 'success', participant, defuse_bomb)

    def handle_communication_environment(self, header, message, data):
        if data['sender_type'] != 'block_beacon_bomb':
            return

        for p in data['recipients']:
            participant = self.participants[p]
            elapsed_milliseconds = data['elapsed_milliseconds']
            bomb_id, sequence, fuse_start = self.unpack_bomb_info(data['additional_info'])

            # check if exists already
            inputs = {
                    'bomb_id': bomb_id,
                    'sequence': sequence,
                    'fuse_start': fuse_start
            }

            jags = participant.jags_by_urn_and_input('urn:asist:defuse-bomb', inputs)

            if len(jags) != 0:
                self.__logger.info(f"[handle_communication_environment] jag for this bomb already exists\n\tid: {bomb_id:8<} seq: {sequence}")
                return

            jag = self.create_defuse_bomb_jag(bomb_id, sequence, fuse_start)
            participant.jags[jag.uid] = jag

            self.__publish_message(elapsed_milliseconds, 'start', participant, jag)
            self.__publish_message(elapsed_milliseconds, 'start', participant, jag.children[0])

    def unpack_bomb_info(self, info):
        bomb_id = info['bomb_id']
        # why is the remaining sequence a string formatted like an array...
        sequence = re.findall(r'R|G|B', info['remaining_sequence'])
        fuse_start = info['fuse_start_minute']
        return bomb_id, sequence, fuse_start

    def create_cut_wire_jag(self, bomb_id, wire):
        return Jag('urn:asist:cut-wire', {
            'bomb_id': bomb_id,
            'wire': wire,
            })

    def create_defuse_bomb_jag(self, bomb_id, sequence, fuse_start):
        cuts = [self.create_cut_wire_jag(bomb_id, wire) for wire in sequence]
        jag = Jag('urn:asist:defuse-bomb', {
            'bomb_id': bomb_id,
            'sequence': sequence,
            'fuse_start': fuse_start
            })
        jag.children = cuts
        return jag

    def __publish_message(self, elapsed_milliseconds, status, participant, jag):
        message_data = {
            'participant': participant.get_instance_data(),
            'elapsed_milliseconds': elapsed_milliseconds,
            'status': status,
            'priority': None,
            'jag': jag.get_instance_data(),
        }
        self.__publish_jag_event(JagEvent.BASE, message_data)

    def __publish_jag_event(self, event_sub_type, data):
        event_sub_type_string = JAG_EVENT_SUB_TYPES[event_sub_type]
        tb = str.join("", traceback.format_stack()[10:])
        event_string = f"\n" \
            f"topic: {JAG_EVENT_TOPIC}\n" \
            f"event: {JAG_EVENT_TYPE}:{event_sub_type_string}:{JAG_EVENT_VERSION}\n" \
            f"data: {data}\n" \
            f"traceback: {tb}\n"
        self.__logger.info(event_string)

        # print(json.dumps(data, indent=' '))
        self.send_msg(JAG_EVENT_TOPIC, JAG_EVENT_TYPE, event_sub_type_string, JAG_EVENT_VERSION, data=data)

    def stop(self):
        self.stop_agent_loop_thread()

    def start(self):
        self.set_agent_status(ASISTAgentHelper.STATUS_UP)
        self.start_agent_loop_thread()
