import unittest

from src.models.jags.jag import Jag
from src.models.participant import Participant, Inventory, Item

PARTICIPANT_ID = 'P000123'
PARTICIPANT_CALLSIGN = 'Alpha'

class ParticipantDefaultsTest(unittest.TestCase):

    def setUp(self):
        self.participant = Participant(PARTICIPANT_ID, PARTICIPANT_CALLSIGN)

    def test_participant_id(self):
        self.assertEqual(self.participant.id, PARTICIPANT_ID)

    def test_participant_callsign(self):
        self.assertEqual(self.participant.callsign, PARTICIPANT_CALLSIGN)

    def test_participant_frozen_state(self):
        self.assertFalse(self.participant.is_frozen)

    def test_participant_jags(self):
        self.assertEqual(len(self.participant.jags), 0)

class ParticipantTest(unittest.TestCase):

    def test_participant_no_callsign(self):
        p = Participant(PARTICIPANT_ID)
        self.assertEqual(p.callsign, PARTICIPANT_ID)

    def test_participant_add_jag(self):
        p = Participant(PARTICIPANT_ID)
        urn = 'urn:test:test-jag'
        jag = Jag(urn)
        p.jags[jag.uid] = jag
        self.assertIs(p.jags.get(jag.uid), jag)

    def test_get_jag_by_urn_and_input_one_jag(self):
        p = Participant(PARTICIPANT_ID)
        urn = 'urn:test:test-jag'
        inputs = {}
        jag= Jag(urn, inputs)
        p.jags[jag.uid] = jag

        self.assertEqual(p.jags_by_urn_and_input(urn, inputs), [jag])

    def test_get_jag_by_urn_and_input_two_different_jags(self):
        p = Participant(PARTICIPANT_ID)
        urn_1 = 'urn:test:test-jag-1'
        urn_2 = 'urn:test:test-jag-2'

        jag_1 = Jag(urn_1)
        jag_2 = Jag(urn_2)

        p.jags[jag_1.uid] = jag_1
        p.jags[jag_2.uid] = jag_2

        self.assertEqual(p.jags_by_urn_and_input(urn_1, {}), [jag_1])
        self.assertEqual(p.jags_by_urn_and_input(urn_2, {}), [jag_2])

    def test_get_jag_by_urn_and_input_two_jags_different_inputs(self):
        p = Participant(PARTICIPANT_ID)
        urn = 'urn:test:test-jag'

        test_input_key = 'test-input'

        inputs_1 = {test_input_key: 'value-1'}
        inputs_2 = {test_input_key: 'value-2'}

        jag_1 = Jag(urn, inputs_1)
        jag_2 = Jag(urn, inputs_2)

        p.jags[jag_1.uid] = jag_1
        p.jags[jag_2.uid] = jag_2

        self.assertEqual(p.jags_by_urn_and_input(urn, inputs_1), [jag_1])
        self.assertEqual(p.jags_by_urn_and_input(urn, inputs_2), [jag_2])


class InventoryDefaultTest(unittest.TestCase):

    def setUp(self):
        self.inventory = Inventory()

    def test_inventory_content(self):
        inv = self.inventory
        for item in Item:
            with self.subTest(i=item):
                self.assertEqual(inv.get_item_count(item), 0)

class InventoryManipulationTest(unittest.TestCase):

    def setUp(self):
        self.inventory = Inventory()

    def test_set_item_count(self):
        for item in Item:
            with self.subTest(i=item):
                returned_count = self.inventory.set_item_count(item, 1)
                self.assertEqual(returned_count, 1)
                self.assertEqual(self.inventory.get_item_count(item), 1)

                returned_count = self.inventory.set_item_count(item, 2)
                self.assertEqual(returned_count, 2)
                self.assertEqual(self.inventory.get_item_count(item), 2)

    def test_set_negative_item_count(self):
        for item in Item:
            with self.subTest(i=item):
                with self.subTest(i='for default value'):
                    self.inventory.set_item_count(item, -1)
                    self.assertEqual(self.inventory.get_item_count(item), 0)

                with self.subTest(i='with set value'):
                    self.inventory.set_item_count(item, 2)
                    self.inventory.set_item_count(item, -1)
                    self.assertEqual(self.inventory.get_item_count(item), 0)

    def test_use_available_item(self):
        for item in Item:
            with self.subTest(i=item):
                starting_item_count = 1
                self.inventory.set_item_count(item, starting_item_count)
                was_used, remaining_item_count = self.inventory.use_item(item)
                self.assertTrue(was_used)
                self.assertEqual(remaining_item_count, starting_item_count - 1)
                self.assertEqual(self.inventory.get_item_count(item), starting_item_count - 1)

    def test_use_unavailable_item(self):
        for item in Item:
            with self.subTest(i=item):
                self.inventory.set_item_count(item, 0)
                was_used, remaining_item_count = self.inventory.use_item(item)
                self.assertFalse(was_used)
                self.assertEqual(remaining_item_count, 0)
                self.assertEqual(self.inventory.get_item_count(item), 0)
