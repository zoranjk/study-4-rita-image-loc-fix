import json
import unittest

from src.agents.joint_activity_monitor import JointActivityMonitor
from src.models.participant import Participant

def unpack(message):
    return message['header'], message['msg'], message['data']

class HandleTrialTest(unittest.TestCase):

    def test_handle_trial_start(self):
        jam = JointActivityMonitor()

        header, msg, data = unpack(TRIAL_START)
        jam.handle_trial(header, msg, data)

        self.assertEqual(len(jam.participants), 3)

        self.assertIn("P000001", jam.participants)
        self.assertIn("P000002", jam.participants)
        self.assertIn("P000003", jam.participants)

        self.assertEqual(jam.participants["P000001"].callsign, "Alpha")
        self.assertEqual(jam.participants["P000002"].callsign, "Bravo")
        self.assertEqual(jam.participants["P000003"].callsign, "Delta")

    def test_handle_trial_stop(self):
        jam = JointActivityMonitor()

        header, msg, data = unpack(TRIAL_START)
        jam.handle_trial(header, msg, data)

        self.assertEqual(len(jam.participants), 3)

        header, msg, data = unpack(TRIAL_STOP)
        jam.handle_trial(header, msg, data)

        self.assertEqual(len(jam.participants), 0)

TRIAL_START = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"replay_parent_type": null,
		"sub_type": "start",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"replay_id": null,
		"source": "gui",
		"replay_parent_id": null,
		"version": "0.1",
		"timestamp": "2023-04-19T19:42:06.4786Z"
	},
	"@timestamp": "2023-04-19T19:42:06.489Z",
	"data": {
		"date": "2023-04-19T19:42:06.4789Z",
		"client_info": [
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000001",
				"callsign": "Alpha",
				"playername": "FloopDeeDoop",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			},
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000002",
				"callsign": "Bravo",
				"playername": "Aaronskiy1",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			},
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000003",
				"callsign": "Delta",
				"playername": "RED_ASIST1",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			}
		],
		"experiment_name": "[object Object]_[object Object]_[object Object]_Wed, 19 Apr 2023 19:42:06 GMT",
		"notes": [],
		"experiment_mission": "Study 4 MISSION A",
		"testbed_version": "V3.2.0-01_11_2023-667-g1e77cc1d4",
		"subjects": [
			"P000001",
			"P000002",
			"P000003"
		],
		"map_name": "Dragon_1.0_3D",
		"experimenter": "ADMINLESS",
		"experiment_author": "adminless",
		"study_number": "NOT SET ",
		"condition": "NOT SET ",
		"group_number": "NOT SET ",
		"map_block_filename": "MapBlocks_Orion_1.3.csv",
		"name": "P000001_P000002_P000003_0",
		"trial_number": "NOT SET ",
		"intervention_agents": [
			"ASI_CMU_TA1_ATLAS"
		],
		"experiment_date": "2023-04-19T19:42:06.4790Z",
		"observers": []
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "trial",
	"header": {
		"message_type": "trial",
		"version": "0.6",
		"timestamp": "2023-04-19T19:42:06.4785Z"
	}
}''')

TRIAL_STOP = json.loads('''{
	"msg": {
		"trial_id": "295ec4af-3c41-4d06-bf21-9eed3cc654e2",
		"replay_parent_type": null,
		"sub_type": "stop",
		"experiment_id": "7091e4f2-f000-4f0a-afcd-888743c6bc0c",
		"replay_id": null,
		"source": "gui",
		"replay_parent_id": null,
		"version": "0.1",
		"timestamp": "2023-04-19T20:01:43.6699Z"
	},
	"@timestamp": "2023-04-19T20:01:43.671Z",
	"data": {
		"date": "2023-04-19T19:42:06.4789Z",
		"client_info": [
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000001",
				"callsign": "Alpha",
				"playername": "FloopDeeDoop",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			},
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000002",
				"callsign": "Bravo",
				"playername": "Aaronskiy1",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			},
			{
				"unique_id": "NOT SET ",
				"participant_id": "P000003",
				"callsign": "Delta",
				"playername": "RED_ASIST1",
				"markerblocklegend": "NOT SET ",
				"staticmapversion": "NOT SET "
			}
		],
		"experiment_name": "[object Object]_[object Object]_[object Object]_Wed, 19 Apr 2023 19:42:06 GMT",
		"notes": [],
		"experiment_mission": "Study 4 MISSION A",
		"testbed_version": "V3.2.0-01_11_2023-667-g1e77cc1d4",
		"subjects": [
			"P000001",
			"P000002",
			"P000003"
		],
		"map_name": "Dragon_1.0_3D",
		"experimenter": "ADMINLESS",
		"experiment_author": "adminless",
		"study_number": "NOT SET ",
		"condition": "NOT SET ",
		"group_number": "NOT SET ",
		"map_block_filename": "MapBlocks_Orion_1.3.csv",
		"name": "P000001_P000002_P000003_0",
		"trial_number": "NOT SET ",
		"intervention_agents": [
			"ASI_CMU_TA1_ATLAS"
		],
		"experiment_date": "2023-04-19T19:42:06.4790Z",
		"observers": []
	},
	"@version": "1",
	"host": "858eedecf5a9",
	"topic": "trial",
	"header": {
		"message_type": "trial",
		"version": "0.6",
		"timestamp": "2023-04-19T20:01:43.6699Z"
	}
}''')
