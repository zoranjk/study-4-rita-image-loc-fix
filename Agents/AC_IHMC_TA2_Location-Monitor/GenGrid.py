Rows = ["A", "B", "C", "D", "E", "F", "G", "H", "I"]
Columns = ["1", "2", "3", "4", "5", "6", "7"]

StartPt = [ 1.2, 50.0,   1.2]
EndPt   = [50.8, 80.0, 150.8]

deltaX = (EndPt[0] - StartPt[0]) / len(Columns)
deltaY = EndPt[1] - StartPt[1]
deltaZ = (EndPt[2] - StartPt[2]) / len(Rows)

z = StartPt[2]
y = StartPt[1]
y2 = y + deltaY
for rn in Rows:
    x = StartPt[0]
    z2 = round(z + deltaZ, 2)
    for cn in Columns:
        x2 = round(x + deltaX, 2)
        print ('"' + rn + cn + '": [['+str(x)+', '+str(y)+', '+str(z)+'],['+str(x2)+', '+str(y2)+', '+str(z2)+']],')
        x = x2
    z = z2
