# CMU FMS Group Cognitive Load Agent

This agent, written in Python, computes the current cognitive load
for each player, and for the overall team, as well as a related probability
of forgetting, again by player and by the whole team.
It publishes its results every ten seconds.

## Message data format

The agent publishes one flavor of message to the bus,
on the topic `agent/measure/AC_CMUFMS_TA2_Cognitive/load`, with
message type `agent` and sub_type `Measure:cognitive_load`.

    {
      "id": "2d56546f-c3c7-4f97-8d2d-e4719e6ba980",
      "agent": "AC_CMUFMS_TA2_Cognitive",
      "created": "2023-05-04T23:54:42.861713Z",
      "elapsed_milliseconds": 61648,
      "Alpha": {
        "cognitive_load": {
          "value": 1.6883838122760675,
          "confidence": 0.6033700230125375
        },
        "probability_of_forgetting": {
          "value": 0.7990850580656113,
          "confidence": 0.827094093958985
        }
      },
      "Bravo": {
        "cognitive_load": {
          "value": 2.6883838122760675,
          "confidence": 0.7033700230125375
        },
        "probability_of_forgetting": {
          "value": 0.8990850580656113,
          "confidence": 0.927094093958985
        }
      },
      "Delta": {
        "cognitive_load": {
          "value": 0.6883838122760675,
          "confidence": 0.4033700230125375
        },
        "probability_of_forgetting": {
          "value": 0.6990850580656113,
          "confidence": 0.727094093958985
        }
      },
      "Team": {
        "cognitive_load": {
          "value": 1.0883838122760675,
          "confidence": 0.7033700230125375
        },
        "probability_of_forgetting": {
          "value": 0.7000850580656113,
          "confidence": 0.757094093958985
        }
      }
    }

There is a JSON object for each player by call sign, and one for the `Team`.
Each of the `cognitive_load` and the `probability_of_forgetting` members are
sub-objects with two members, the value of those sub-members being the
value of the measure and a confidence estimate of that value. All
these values are non-negative, real numbers.

## Exception handling

Note that this agent is built using the `ihmc-python-agent-helper-package`. The
`on_message` implementation in this helper of exception handling is keeps
this agent up and running as well as can be expected should exceptions be encountered.

## Adaptation for alternative tasks

To adapt the AC for alternative tasks

- Work out which objects players need to keep track of. For the current, experiment 4 task, this is bombs.
For the previous, experiment 3 task, this was victims. There is no reason there cannot be multiple types
of entities that need to be kept track of.

- Decide witch message(s) will identify when such an entity is first encountered; which message(s)
will identify when an already seen entity is re-encountered; and whichh message(s) will identify
when that entity’s status becomes resolved and no longer of interest.

- Enter the topics used for the messages, above, in `ConfigFoler/config.json`.

- In `src/cmu_fms_cognitive_load_ac.py` modify the `CMUFMSCognitiveLoadAC.on_message()` method to
call the agent’s `memory.learn()` method supplying a chunk description containing the identifiers
for the entity, and for the player observing or interacting with it. This will create or reinforce
the chunk as appropriate.

- Similarly, enter the entity’s identifier in the agent’s `resolved` set when it is no longer of interest.
