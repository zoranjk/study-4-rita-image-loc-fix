
To build the testagent :

From the directory of this README file run:

LINUX/MAC: "docker build -t testagent:latest --build-arg CACHE_BREAKER=$(date +%s) ."
WINDOWS: "docker build -t testagent:latest --build-arg CACHE_BREAKER=$(Get-Date) ."

then run

"docker compose up" or "docker compose up -d" for no console hijacking

You can check the Dozzle logs at http://localhost:9999 in your browser