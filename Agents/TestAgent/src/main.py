from asistagenthelper import ASISTAgentHelper

import sys

print("Hello!")

def on_message(topic, header, msg, data, mqtt_message):
    print("Received a message on the topic: ", topic)
helper = ASISTAgentHelper(on_message)
# Set the agent's status to 'up' and start the Main loop (which does not return)!!!
helper.set_agent_status(helper.STATUS_UP)
helper.run_agent_loop()
# # Define the method to call when subscribed to messages are received
# def on_message(topic, header, msg, data, mqtt_message):
#     print("Received a message on the topic: ", topic)

# def main():
#     sys.stdout.write("Hello")

#     # Initialize the helper class and tell it our method to call
#     helper = ASISTAgentHelper(on_message)

#     # Set the agent's status to 'up' and start the Main loop (which does not return)!!!
#     helper.set_agent_status(helper.STATUS_UP)
#     helper.run_agent_loop() 

# if __name__ == '__main__':
#     main()





