# Table of Contents

[Gallup Events Manager (GEM) Agent](#gem "Click to jump to the section")

[Gallup Emergent Leadership Prediction (GELP) Agent](#gelp "Click to jump to the section")

[Gallup Agent GELP Runtime Instructions](#gelpre "Click to jump to the section")

[Gallup - Emergent Leadership Prediction (GELP) - Event Message Format](#gelpmsg "Click to jump to the section")

[Gallup Object Library Distributor (GOLD) Agent](#gold "Click to jump to the section")

[Gallup Agent GOLD Runtime Instructions](#goldre "Click to jump to the section")

[Gallup Object Library Distributor (GOLD) - Event Message Format](#goldmsg "Click to jump to the section")

# Gallup Events Manager (GEM) Agent <a name="gem"></a>

## About

The Gallup Event Manager (GEM) Agent

* Handles inbound messages from the message bus for trial related information and pre processing.
* Maintains the flow of control between the two agents, GELP and GOLD
* Handles publication to the message bus.

In addition to GELP and GOLD messages, GEM also handles requests from other agents. For example, GEM publishes the status of the trial with the topic "status/ac_gallup_ta2_gem/heartbeats" and version with the topic "agent/ac_gallup_ta2_gem/versioninfo" as requested by the heartbeats and status check processes.

# Gallup Emergent Leadership Prediction (GELP) Agent <a name="gelp"></a>

## About

The Gallup - Emergent Leadership Prediction (GELP) Agent publishes data messages of topic "agent/gelp", subtype "event" to the message bus for reference and use by other agents.

Published are emergent leadership score predictions for participants during the trial.

Scores represent predictions of the likely final scores calculated from similar Gallup Emergent Leadership sections of the trial survey (separately administered), and feature both an overall score and scores for each of the 8 components.

Prediction scores will be issued at roughly each elapsed minute of the trial following trial start.

Predictions are based on an assortment of audio transcription, NLP, competency scores, pre-trial survey info, and other relevant message types subscribed to on the general message bus.

Additional notes:

- The higher the predicted score for overall and/or associated components, the higher the potential for the respective participant to emerge as an understood leader without necessarily being directly appointed.
- Scores do not necessarily reflect on leadership quality, i.e. a "good" or "bad" leader.  Instead, predictive scores tend toward accuracy of determining leadership, similar to a human's initial perception of leadership.
- Predicted scores improve over time. As trial progresses and more cumulative data becomes available, confidence intervals for predicted scores tend to shrink.
- Overall score is expressed on a 10-point scale, while individual component scores are expressed on a 7-point scale. This is a reflection of the associated survey question response scales.

### Survey Data Consumed

Pre-trial and baseline survey data for each team is utilized for predictive modeling, collected via subscription and review of message bus event topics such as those found in "player/data/aggregated" and psychometric scales noted below.

When available, at least one set of responses per participant is leveraged to generate scores. Multiple score sets per participant enhance scoring accuracy, as additional responses increase the available question topics useful to the model that do not require imputation.

**Examples of Survey response questions utilized include (but are not limited to):**

| Question ID | ImportId    | Topic                                                             |
| ----------- | ----------- | ----------------------------------------------------------------- |
| 'DEMO_1'    | 'QID1_TEXT' | 'What is your age in years?'                                      |
| 'DEMO_3'    | 'QID4'      | 'What is your English proficiency level?'                         |
| 'DEMO_4'    | 'QID6'      | 'The highest education level you have achieved (or equivalent):'  |
| 'PSY_COL_1' | 'QID13_1'   | 'I preferred to work in those groups rather than working alone.'  |
| 'PSY_COL_3' | 'QID13_3'   | 'I wanted to work with those groups as opposed to working alone.' |
|             |             | ...                                                               |

**Psychometric Scales Considered (but not limited to):**

- Video Game Experience Measure
- Santa Barbara Sense of Direction
- Reading the Mind in the Eyes Test
- Minecraft Proficiency/knowledge
- PANAS
- Psychological Collectivism
- ...

Note that survey question and scale availability may change during testbed development and this list may be adapted.

### Generated Measures

Scores represent per-participant predictions of the likely final scores calculated from similar Gallup Emergent Leadership sections of the trial survey (separately administered), and feature both an overall score and scores for each of the 8 components: ideas, focus, coordinate, monitor, share, plan, agree, help.

Upper and lower control bounds are also provided for each participant score, reflective of the amount of available information for each participant that does not require imputation.

### Measures as Predictor of Emergent Leadership

Results published back to the message bus represent predictors of leadership, with higher scores being predictive of each participant's potential emergence from the group as an understood leader without necessarily having first been directly appointed.

- As defined: Emergent leaders assume leadership behaviors when there is no defined or appointed leader in a group (Carte & Chidabaram, 2006).
- Why it matters: Our study 2 results show that emergent leaders are more likely to engage in behaviors that contribute to team effectiveness, including explicit coordination, deliberate planning, use of transactive memory systems, and motivating/confidence building behaviors.
- For more information, please refer to the "[TA2_Gallup_Study2_Findings](https://docs.google.com/document/d/1IvIEt3MbpfFRbbcP3XRYQeZtscTLh2sNX8nXaHwyH8A/edit?usp=sharing)" within the ASIST Google Drive and the README.md file within the repository folder [Agents/AC_GALLUP_TA2_GEM](https://gitlab.asist.aptima.com/asist/testbed/-/tree/develop/Agents/gallup_agent_gelp).

### GELP Output Message Format

Information related to the structure and formatting of GELP message bus publications (schema, formatting, examples) can be found in the gallup_gelp_message.md and .json files within the repository folder [MessageSpecs/GallupAgentGELP](https://gitlab.asist.aptima.com/asist/adminless-testbed/-/tree/develop/MessageSpecs/GallupAgentGELP).

# Gallup Agent GELP Runtime Instructions `<a name="gelpre"></a>`

## Configuration

- The configuration files and maps are located in the `adminless-testbed/Agents/AC_GALLUP_TA2_GEM/ConfigFolder`.
- The configuration file, `settings.env`, contains the settings for the GEM Host and the agent's version_info.

## Running Locally

The scripts `run_locally.sh` and `run_locally.cmd` enable running the GELP Agent locally without having

to build and run a docker image.  For the code to work you will need to have Python 3.8 or higher installed

on your system, a working version of `git`, and then install the `ASISTAgentHelper` library using the

`requirements.txt` file with `pip`.  You will also need to make sure the `host` value in the `config.json`

file points to the location of the MQTT bus.

## Building the Docker container

- In a shell window, navigate to the `adminless-testbed/Agents/AC_GALLUP_TA2_GEM` directory, this is the same level

as this README.md file, and run:

    - linux:`./agent.sh build`

    - Windows:`agent.cmd build`

## Running the Docker container

- In a shell window, navigate to the `adminless-testbed/Agents/AC_GALLUP_TA2_GEM` directory, this is the same level

as this README.md file, and run the agent command with either `up` or `upd`.  `up` will run the docker container

and output the log to the console.  `upd` will run the docker container in the background:

    - linux:`./agent.sh up` or `./agent.sh upd`

    - Windows:`agent.cmd up` or `agent.cmd upd`

## Stopping the Docker container

- Press **Ctrl-C** to stop the container if started with the `up` option.
- To clean up docker and stop the Agent (if started with `upd`), navigate to the `adminless-testbed/Agents/AC_GALLUP_TA2_GEM`

directory, this is the same level as this README.md file, and run:

    - linux:`./agent.sh down`

    - Windows:`agent.cmd down`

# Gallup - Emergent Leadership Prediction (GELP) - Event Message Format <a name="gelpmsg"></a>

Please refer to message schema documentation and JSON examples, which can be found by navigating to the `adminless-testbed/MessageSpecs/GallupAgentGELP` directory

Please note document draft status. Revisions are anticipated, above information and examples are not yet final and subject to change.

**Revision Notes:**

[2023-06-14] Updated this readme to include Study4 updates

[2021-12-23] Updated this readme to include requested documentation on topics (1) what survey data the AC consumes; (2) what measure it generates; (3) whether the measure is a predictor of teamwork process

# Gallup Object Library Distributor (GOLD) Agent <a name="gold"></a>

## About

The Gallup - Object Library Distributor (GOLD) Agent publishes data messages of topic "agent/gold", subtype "event" to the message bus for reference and use by other agents.

GOLD will publish a variety of small factor, component data messages to the message bus for reference. These will be published respective to participant_id. Published messages will be comprised of various features engineered from raw event data, gleaned from various points along the Gallup data and modeling pipeline.

Items published by GOLD fall into three groupings, two of them are identified by their prefix, and the third one has self explanatory names.

- "agg_" prefix indicates _aggregate_ features compiled from Automatic Speech Recognition (ASR) speech-to-text transcription and Natural Language Processing (NLP) events as subscribed to from the message bus. Aggregated features represent quantitative, semantic feature counts by rule type as of a given publication time. Example agg_ features include speaking, dominant, and first-word types. Also included in this grouping are features echoing survey responses according to participant, QID.
- "mod_" prefix indicates _modeled_ features are generated from a series of 7 categories of speech models, where the inputs are ASR events and outputs are labelled predictions. Minute-by-minute predictions for each category model are stated according to participant_id and include perspective of the generated score vs. study2 and study4 average of cumulative sum for each label. Speech model categories include: Motivation, Compensatory Helping, Contingent Planning, Deliberative Planning, Role Clarification, Reactive Planning, and Transactive Memory.
- Other - other features have self explanatory names, e.g. `Explicit Coordination_adj_zscore`, `Explicit Coordination_adj_zscore`, etc. The feature names ending in "_count" are ismple counts from the text. The feature names ending in "_adj_zscore" or "_zscore" are calulated scores from the speech models, where the inputs are ASR events and outputs are labelled predictions. These feature names are left as is to distinguish between whether the features are used in downstream models or not.

For more information, please refer to the "[TA2_Gallup_Study2_Findings](https://docs.google.com/document/d/1IvIEt3MbpfFRbbcP3XRYQeZtscTLh2sNX8nXaHwyH8A/edit?usp=sharing)" within the ASIST Google Drive and the README.md file within the repository folder [Agents/AC_GALLUP_TA2_GEM](https://gitlab.asist.aptima.com/asist/testbed/-/tree/develop/Agents/gallup_agent_gold).

Additional Notes:

- GOLD features are invariant with respect to time. Predictions do not become more or less accurate as trials continue.
- Models leveraged within GOLD execute on captured trial data from the message bus and are then compared post-aggregation to scores produced from study2 event data.
- Although being produced in concert with Gallup Emergent Leadership modeling and predictive activity, resulting object data published back to the message bus by this agent may or may not also used directly within Emergent Leadership predictions.
- Some GOLD features had to be named as *"feature name_per_min"* due to the internal working mechanism between Gelp and Gold agents dated from previous studies. The GOLD features ending in *"per_min"* are actually z score of the feature as of the latest minute, calculated per minute. E.g. `mod_lgb_Deliberate_Planning_per_min` is actually `Deliberate Planning z score`, calculated as of the latest minute of the trial.

### GOLD Feature Reference Information

Planning Types

- Deliberate: Develop and communicate a primary course of action.
- Contingency: Specifies, in advance, a backup or alternative course of action the team will potentially follow if needed (DeChurch & Haas, 2008; Marks et al., 2001).
- Reactive: Strategy adjustment. Modify an initial plan because of feedback or changes in the performance environment (DeChurch & Haas, 2008; Marks et al., 2001).
- Explicit Coordination: Requires team members communicate in order to articulate plans, define responsibilities, negotiate deadlines, and seek information (Rico et al., 2008).

Plan Quality

- Plan Quality: A plan that defines a sequence of actions that succeeds in accomplishing a goal. More specifically, Stout et al., 1999 employed a list of 9 planning behaviors to judge planning quality, including:
- Clarifying roles: Communication that is used to delineate or explain who will take on which role.
- Clarifying information to be traded: Communication that is used to arrange who has what information native to them or information that they plan to gain and then share. - Clarifying sequencing and timing: Communication that is focused on temporal instances of the trial.

### GOLD Output Message Format

Information related to the structure and formatting of GOLD message bus publications (schema, formatting, examples) can be found in the gallup_gold_message.md and .json files within the repository folder [MessageSpecs/GallupAgentGOLD](https://gitlab.asist.aptima.com/asist/adminless-testbed/-/tree/develop/MessageSpecs/GallupAgentGOLD).

# Gallup Agent GOLD Runtime Instructions <a name="goldre"></a>

## Configuration

- The configuration files and maps are located in the `adminless-testbed/Agents/AC_GALLUP_TA2_GEM/ConfigFolder`.
- The configuration file, `settings.env`, contains the settings for the MQTT Host and the agent's version_info.

## Running Locally

The scripts `run_locally.sh` and `run_locally.cmd` enable running the GOLD Agent locally without having

to build and run a docker image.  For the code to work you will need to have Python 3.8 or higher installed

on your system, a working version of `git`, and then install the `ASISTAgentHelper` library using the

`requirements.txt` file with `pip`.  You will also need to make sure the `host` value in the `config.json`

file points to the location of the MQTT bus.

## Building the Docker container

- In a shell window, navigate to the `adminless-testbed/Agents/AC_GALLUP_TA2_GEM` directory, this is the same level

as this README.md file, and run:

    - linux:`./agent.sh build`

    - Windows:`agent.cmd build`

## Running the Docker container

- In a shell window, navigate to the `adminless-testbed/Agents/AC_GALLUP_TA2_GEM` directory, this is the same level

as this README.md file, and run the agent command with either `up` or `upd`.  `up` will run the docker container

and output the log to the console.  `upd` will run the docker container in the background:

    - linux:`./agent.sh up` or `./agent.sh upd`

    - Windows:`agent.cmd up` or `agent.cmd upd`

## Stopping the Docker container

- Press **Ctrl-C** to stop the container if started with the `up` option.
- To clean up docker and stop the Agent (if started with `upd`), navigate to the `adminless-testbed/Agents/AC_GALLUP_TA2_GEM`

directory, this is the same level as this README.md file, and run:

    - linux:`./agent.sh down`

    - Windows:`agent.cmd down`

# Gallup Object Library Distributor (GOLD) - Event Message Format <a name="goldmsg"></a>

Please refer to message schema documentation and JSON examples, which can be found by navigating to the `adminless-testbed/MessageSpecs/GallupAgentGOLD` directory

Please note document draft status. Revisions are anticipated, above information and examples are not yet final and subject to change.

**Revision Notes:**

[2023-06-14] Updated this readme to include Study4 updates

[2022-03-10] Update About section description for agent, where to find additional reference materials. Also cleaned up a couple of "GELP" references with "GOLD".

[2022-01-31] Begin.

Notes:

- 2022-12-16 NAK: This folder is where any agent code .py files should be placed, whether "formal" (registered as Agents that will publish back to the message bus via GEM) or intermediary (facilitating event digestion and/or passing objects back to GEM or each other for internal functionality).
- assuming entrypoint.sh should only need to appear once, purpose is to facilitate operation of the AGENT_HELPER. The code contained therein may need to be upgraded for Study4 when Roger Carff (or someone else in charge) re-introduces/updates how it works. Agent helper is also referred to during container setup in /AC_GALLUP_TA2_GEM/dockerfile:11 and in requirements.txt
