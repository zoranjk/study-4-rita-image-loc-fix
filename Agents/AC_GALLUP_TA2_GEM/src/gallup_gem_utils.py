#!/usr/bin/env python3

"""
Gallup Event Management Utilities (GEMUtils)

Authors: Nathan Kress, Maxim van Klinken, Anirban Pal
email: nathan_kress@gallup.com, max_vanklinken@gallup.com, anirban_pal@gallup.com
"""
import os
import pandas as pd
from pandas.core.frame import DataFrame
import datetime as dt
import json
import sys
import logging
import traceback
import shutil
import uuid
import numpy as np
import gensim.downloader as api
import nltk
# from asistagenthelper import ASISTAgentHelper
import sklearn as skl #not used but you need it installed for the pickled model
from functools import reduce
import spacy
from spacy.matcher import Matcher
from spacy.matcher import PhraseMatcher

nltk.download('punkt')
logger = logging.getLogger("Gallup_GEM")    # this should piggyback onto existing logging handler of the same name from gallup_wrapper_gem.py

__author__ = 'Gallup'

class GEMUtils():

    def digest_events(self, recon_obj, calc_minute):
        """
        Accepts an object of mixed event types, filtering for additional formatting by event type, then repackaging
        as dataframe for easier use downstream.
        
        All non-NLP extraction events will be retained as is and in order.
        
        Usage: 
        df_new_recon_obj = digest_events(recon_obj)
        - note: assign to a new variable so can retain original recon_obj for later additions

        calc_minute: current minute of trial when this function is called (used to help annotate collected agent/dialog 
        events by current publication minute so don't appear as duplicate events)
        
        ref: "Event Metadata Files - ODIN NLP Event Breakdown 20211027.ipynb"
        """
        global logger

        new_recon_obj = []
        extractions_obj = []
        
        logger.info(f"Begin digest_events function: {dt.datetime.now()} ")
    
        # filter events in received object for routing
        #  ref: recon_obj.append({"message_topic": topic, "artifact": "True/False", "message_timestamp": curr_ts, 
        #                              "message_elapsed_ms": elapsed_ms, "message": msg_info})
        d_tracker = ""
        for index, record in enumerate(recon_obj):
            
            if record["message_topic"] == "agent/dialog":

                # split to new object for later handling
                extractions_obj.append(record)

                # add only those since last calc minute as-is for reference (for Les improvements 12/20), modify event name first
                #  - Also, filter out any where artifact="True"
                if ( (record["curr_minute"] >= (calc_minute - 1)) and (record["artifact"] == "False") ):
                    mod_record = record.copy()
                    mod_record["message_topic"] = "agent/dialog_raw"
                    new_recon_obj.append(mod_record)
                    
                    d_tracker += "D"  # Added agent/dialog_raw to new_recon_obj

                else:
                    d_tracker += "-"  # Skipped adding agent/dialog_raw to new_recon_obj
            

            else:
                # route to new_recon_object as-is
                new_recon_obj.append(record)
                d_tracker += "+"      # Added non-dialog event as-is
        
        logger.info("digest_events log: " + d_tracker)

        # logger.info(d_message)
        
        # convert new_recon_obj to dataframe for easier handling
        df_new_recon_obj = pd.DataFrame(new_recon_obj)
            
        # placeholder for additional event handling if needed
        logger.info(f"End digest_events function: {dt.datetime.now()} ")

        return df_new_recon_obj

    ## - Model Facilitation
    def __flatten(self, df, do_lists = True) -> DataFrame:
        
        logger.info(f"Begin __flatten function: {dt.datetime.now()} ")

        temp = df.copy()
        for column in df.columns:
            
            if do_lists and str(temp[column].iloc[0])[0] == '[' :

                logger.info(f'flattening {column} list')
                
                if type(temp[column].iloc[0]) == str:
                    temp[column] = [eval(item) for item in temp[column]]
                    
                temp = temp.explode(column=column).reset_index()
                
            elif len(str(temp[column].iloc[0])) > 0 and str(temp[column].iloc[0])[0] == '{':

                logger.info(f'flattening {column} object')
                
                if type(temp[column].iloc[0]) == dict:
                    
                    sub_df = pd.DataFrame([item if item is not np.NaN else {} for item in temp[column]]).set_index(temp.index)
                    sub_df.columns = [column + '_' +  col for col in sub_df.columns]
                    
                else:
                    sub_df = pd.DataFrame([eval(item) for item in temp[column]]).set_index(temp.index)
                    sub_df.columns = [column + '_' +  col for col in sub_df.columns]
                    
                temp = pd.concat([temp,sub_df],axis=1)
                temp = temp.drop(columns=[column],axis=1)

        logger.info(f"End __flatten function: {dt.datetime.now()} ")
        return temp

    def __split_input_by_topic(self, raw_data, agent_name) -> dict:

        assert 'message_topic' in raw_data
        assert raw_data.message_topic.nunique() > 0
        
        
        logger.info(f"Begin __split_input_by_topic function: {dt.datetime.now()} ")
        
        output = {}
        for message_topic in raw_data.message_topic.unique():
            subset = raw_data[raw_data.message_topic == message_topic]
            if message_topic == 'agent/asr/final':
                output[message_topic.replace("/","_")]=__flatten(__flatten(__flatten(subset)),False)
            elif message_topic == agent_name+'_internal/participant_lookup':
                output[message_topic.replace("/","_")]=__flatten(__flatten(subset))
            elif message_topic == 'player/data/aggregated':
                output[message_topic.replace("/","_")]=__flatten(__flatten(subset),False)
            else:
                output[message_topic.replace("/","_")]=__flatten(subset)
        
        logger.info( f"End __split_input_by_topic function: {dt.datetime.now()} ")
        return output

    def survey_clean(self, full_df, survey_var_list, prefix):

        
        logger.info(f"Begin survey_clean function: {dt.datetime.now()} ")

        ident_list = ['trial', 'message_participant_id', 'message_values_uniqueid']
        subset_list = ident_list + survey_var_list
        survey = full_df[subset_list]
        
        survey_without_duplicates = survey.drop_duplicates(['trial', 'message_participant_id'])

        survey_wodups_ignorena_uids = survey_without_duplicates[~pd.isna(survey_without_duplicates['message_values_uniqueid'])] # ignores any record that has at least 1 null in specified column (inverts the filter)
            
        survey_dropna_on_survey_items = survey_wodups_ignorena_uids.dropna(subset=survey_var_list, how='all')

        survey_final = survey_dropna_on_survey_items.drop(['message_values_uniqueid'], axis=1)
        survey_final = survey_final.rename(columns={
                                        col: prefix + str(col).replace("message_values_","") for col in survey_final.columns if col not in ident_list})

        logger.info( f"End survey_clean function: {dt.datetime.now()} ")
        return survey_final

    ## - End Model Facilitation

    # truncate nanosecond precision timestamps to microseconds (9 digit precision to 6 digit)
    def trunc_nano_to_micro(self, given_ts):
        """
        requires timestamp submitted as string
        
        NOTE: This may not be absolutely necessary to push to Testbed, but based on Hackathon 2022-01-10 pre-trial start
        events were being parsed where timestamps had additional precision, resulting in non-fatal "extra data" errors
        
        """
        # given_ts="2021-06-04T22:30:30.353241000"
        given_ts_len = len(given_ts)

        nano_split = given_ts.split(".")
        fix_triggered = False
        
        if len(nano_split) > 1:
            
            msg_builder = f"[function: trunc_nano_to_micro] Given ts: {given_ts} , "
            
            nano_split_len = len(nano_split[1])

            if nano_split_len>6: 
                given_ts = given_ts[:(given_ts_len - (nano_split_len - 6))]
                msg_builder += "Reducing precision to max 6 digits (nano to micro)"
                fix_triggered = True
            
            if nano_split_len<6 and nano_split_len>0:
                pad_fix = nano_split[1].ljust(6,"0")
                given_ts = f"{nano_split[0]}.{pad_fix}"
                msg_builder += f"Zero-padding precision to 6 digits ({nano_split[1]} --> {pad_fix})"
                fix_triggered = False  # silencing, this happens a lot.
                
            if fix_triggered == True:
                logger.info(msg_builder)

        return given_ts

    # facilitates checking for np.nan, pd.NA, pd.NaT, None, etc. values (nulls) in an object where strings are expected
    def is_nan(self, x):
        return pd.isna(x) or (x != x)

    # recursively purges NaN-valued items from dict object, removing keys them with json-friendly null values
    def cleanNullTerms(self, d,level=0):
        
        logger.info(f"Begin cleanNullTerms function: {dt.datetime.now()} ")

        if level == 0:
        
            if isinstance(d, dict):
                clean = {}
                for k, v in d.items():
                    
                    if not is_nan(v) and v != None:
                        nested = cleanNullTerms(v,(level+1))
                        clean[k] = nested
                    else:
                        clean[k] = "REMOVE_ME"
                        
                for w in clean.copy():
                    if clean[w] == "REMOVE_ME":
                        del clean[w]
                        
            elif isinstance(d, list):
                
                clean = []
                for i, v in enumerate(d):
                    
                    if not is_nan(v) and v != None:
                        nested = cleanNullTerms(v,(level+1))
                        clean.append(nested)
                    else:
                        clean.append("REMOVE_ME")
                        
                for x, z in enumerate(clean):
                    if z=="REMOVE_ME":
                        nulled = clean.pop(x)
                    
            elif isinstance(d, str):
                
                clean = ""
                if not is_nan(d):
                    clean = d
                else:
                    clean = "REMOVE_ME"
                    
            elif isinstance(d, float):
                
                clean = np.nan
                if not is_nan(d):
                    clean = d
                else:
                    clean = "REMOVE_ME"
                    
            elif isinstance(d, int):
                
                clean = np.nan
                if not is_nan(d):
                    clean = d
                else:
                    clean = "REMOVE_ME"
            
            elif isinstance(d, bool):             
                
                clean = None
                if not is_nan(d) and d != None:
                    clean = d
                else:
                    clean = "REMOVE_ME"        
        
        if level == 0 and clean == None:
            logger.info( "Nothing found to clean, returning...")
        
        if level == 0:
            logger.info( f"End cleanNullTerms function: {dt.datetime.now()} ")

        return clean

    ## Functions enabling Trial event continuity (gelp_recon_obj artifact importable for matching teams on subsequent trials)
    # check to see if recon_obj_hist resource is available, has been loaded
    def check_recon_obj_hist(self, curr_participant_lst, RECON_OBJ_HIST, artifact_file_path):
        """Checks to see if RECON_OBJ_HIST is None, and if so if recon_obj_artifact.json file is available to be loaded"""
        # global RECON_OBJ_HIST, artifact_file_path   # this doesn't appear to transfer as it did when we weren't using class structure, adding these as parameters
        croh_RECON_OBJ_HIST = RECON_OBJ_HIST
        croh_artifact_file_path = artifact_file_path
        
        logger.info(f"Begin curr_participant_lst function: {dt.datetime.now()} ")
        
        # ensure sorted participant list
        curr_participant_lst = sorted(curr_participant_lst, reverse=False)

        returnState = False
        
        if croh_RECON_OBJ_HIST is None:
            # look for file existence in /resource directory
            if os.path.isfile(croh_artifact_file_path):
                tmp_recon_obj = []
                
                with open(croh_artifact_file_path, 'r') as recon_file:
                    
                    for he_i, hist_event in enumerate(recon_file):
                        
                        if he_i == 0:

                            # header row has associated participant list
                            firstline = hist_event        
                            art_header = json.loads(firstline)

                            art_participant_lst = sorted(art_header["participant_lst"], reverse=False)

                            logger.info(f"Current participant list, sorted: " + ",".join([x for x in curr_participant_lst]))
                            logger.info(f"Artifact participant list, sorted: " + ",".join([x for x in art_participant_lst]))
                                        
                            if art_participant_lst == curr_participant_lst:
                                # artifact file available and participants match, trial continuance parameters for learning met
                                logger.info("Success: recon_obj artifact from prior trial found, matches to participants...")
                                
                                returnState = True
                                
                            else:
                                # artifact file available, but not a participant match -- assuming a different team is now engaged
                                #  by study rules (as of 2022-01-11) we can't learn from priors.
                                logger.info("Note: A recon_obj artifact file was found, but participants do not match.")

                                returnState = False
                                
                                break       # exit loop through file lines
                        
                        if ((he_i > 0) and (returnState == True)):
                            # shouldn't trigger if False due to break above, but just in case
                            # proceed to add all lines in recon_file (except header) into tmp_recon_obj
                            
                            try:
                                # Adding resilience to drop offending line(s) instead of entire artifact file... data drift and encoding problems can mess up json parser
                                tmp_event = json.loads(hist_event)
                            
                                tmp_event[0]["artifact"] = "True"
                            
                                tmp_recon_obj.append(tmp_event[0])
                                
                            except:
                                # output line and trace so we can troubleshoot if necessary
                                traceback.print_exc()
                                sys.stdout.flush()
                                logger.info(f"Artifact file parse error at line {he_i}: record dropped.")

                    
                    logger.info("Flagged all artifact events so distinguishable from current trial calculation minutes...")

                    # assign artifact object to constant for use
                    croh_RECON_OBJ_HIST = tmp_recon_obj
                    
                    # print("RECON_OBJ_HIST constant assigned and ready for prepend to current gold_recon_obj.")
                    # sys.stdout.flush()
                    logger.info("RECON_OBJ_HIST constant assigned and ready for prepend to current gem_recon_obj.")

                    
                    # back up artifact file
                    curr_ts = helper.generate_timestamp()          # in ASIST-happy format using agent helper class (appends a Z)

                    shutil.copy(croh_artifact_file_path, (croh_artifact_file_path[:-4] + "_" + curr_ts + ".txt") )     # copy to new file with .txt extension instead of original .json
                    
                    try:
                        os.chmod( (croh_artifact_file_path[:-4] + "_" + curr_ts + ".txt"), 0o0777)        
                    except Exception as e:
                        logger.info("Unable to change file permissions of backed up artifact file.")
                        # traceback.print_exc()

                    # remove former artifact file
                    os.remove(croh_artifact_file_path)

                    logger.info("Backed up artifact file and removed original to facilitate current trial.")

            else:
                # artifact file not available
                logger.info("Note: No recon_obj artifact file found.")
                
                returnState = False
            
        else:
            # File exists and has already been loaded, return False so script doesn't repeat prepend to curr_obj in progress
            logger.info("Note: RECON_OBJ_HIST exists and has already been loaded.")
            
            returnState = False  
        
        logger.info(f"End check_recon_obj_hist function: {dt.datetime.now()} ")

        return returnState

    # write current recon_obj to artifact file for later use
    def write_recon_obj_hist(self, curr_recon_obj_item, participant_lst, RECON_OBJ_HIST, gem_recon_obj, artifact_file_path):
        """
        Checks to see if there is an existing recon_obj_artifact.json file.
        
        If none: starts new one by creating file with header row, first subscribed to and/or relevant event
        
        If exists: 
        - (if at startup)
        - backs up prior artifact file to .txt with timestamp
        - creates new artifact file with new header
        - (if matching partipants:)
            - injects events from former artifact file
            - injects events into current recon_obj
        - (if in-trial)
        - appends current event
        
        appends subscribed to and/or relevant event
        
        Note: Assumption that upon testbed initialization resource directory will be empty. Just the same, upon reading the
        artifact file should take care to match participants else not use.

        """
        
        wroh_RECON_OBJ_HIST = RECON_OBJ_HIST
        wroh_gem_recon_obj = gem_recon_obj
        wroh_artifact_file_path = artifact_file_path

        # check to see if file exists
        # Notes: should NOT exist the first time this is called in any trial, even if same team+participants because if
        #        existing artifact file was found it was read and backed up (orig deleted).
        #        As such, if RECON_OBJ_HIST constant is populated at time of call need to inject (once) after header is
        #        rewritten

        gem_rec_obj = curr_recon_obj_item
        
        logger.info(f"Begin write_recon_obj_hist function: {datetime.datetime.now()} ")

        if not os.path.isfile(wroh_artifact_file_path):
            
            logger.info("Creating new artifact file with header listing current participants...")
            
            # sort for later ease of use
            participant_lst = sorted(participant_lst, reverse=False)

            curr_ts = helper.generate_timestamp()       # in ASIST-happy format using agent helper class (appends a Z)
            
            artifact_obj_header = {"id": "GEM Recon Object Artifact",
                                "purpose": "Given participant match which assumes same team, enables learning across trials as allowed in ASIST parameters.",
                                "participant_lst": participant_lst,
                                "render_ts": curr_ts
                                }
        
            # create new artifact file with header row
            try:
                with open(wroh_artifact_file_path, 'w', newline=None) as file:
                    file.write(json.dumps(artifact_obj_header))

            except Exception as e:
                logger.info(f"Error creating new artifact file: {str(e)}")
                # traceback.print_exc()

            else:
                logger.info(f"Successfully created new artifact file.")

                try:
                    os.chmod(wroh_artifact_file_path, 0o0777)        
                except Exception as e:
                    logger.info("Unable to change file permissions of new artifact file.")
                    # traceback.print_exc()
                else:
                    logger.info("Successfully changed file permissions of backed up artifact file.")
                    
            # if RECON_OBJ_HIST is populated, inject now into current artifact file, current gold_recon_obj
            if wroh_RECON_OBJ_HIST is not None:
                # scrub first
                logger.info(f"Scrubbing old recon_obj (length: {len(wroh_RECON_OBJ_HIST)} lines)...")

                recon_obj_hist_temp = []
                
                try:
                    # for hist_item in RECON_OBJ_HIST:
                    for hi_i, hist_item in enumerate(wroh_RECON_OBJ_HIST):
                        
                        recon_obj_hist_temp.append(hist_item)
                        
                except Exception as e:
                    logger.info(f"Error scrubbing prior RECON_OBJ_HIST at line {hi_i}: {str(e)}")
                    # traceback.print_exc()
                    
                else:
                    logger.info(f"Successfully scrubbed RECON_OBJ_HIST.")           

                # write recon_obj_hist_temp lines to new recon_obj artifact file in progress
                logger.info("Injecting recon_obj_hist into current artifact file...")
                    
                with open(wroh_artifact_file_path, 'a', newline=None) as file:
                    try:
                        for hi_i, hist_item in enumerate(recon_obj_hist_temp):

                            file.write(os.linesep + "[" + json.dumps(hist_item) + "]")
                            
                    except Exception as e:

                        logger.info(f"Error injecting RECON_OBJ_HIST item into current artifact file at line {hi_i}: {str(e)}")
                        # traceback.print_exc()
                        
                    else:

                        logger.info(f"Successfully injected RECON_OBJ_HIST into current artifact file.")
                    
                # push lines to new recon_obj
                logger.info(f"Injecting recon_obj_hist into current gem_recon_obj...")
                
                injection_errors = 0
                for hi_i, hist_item in enumerate(recon_obj_hist_temp):
                    
                    try:
                        gem_recon_obj.append(hist_item)
                        
                    except Exception as e:
                        injection_errors += 1

                        logger.info(f"Error injecting RECON_OBJ_HIST item into current gem_recon_obj at line {hi_i}: {str(e)}")
                        # traceback.print_exc()

                logger.info(f"Completed injecting RECON_OBJ_HIST into current gem_recon_obj with {injection_errors} line errors.")
            
            else:
                logger.info(f"RECON_OBJ_HIST remains empty, no former events available to inject into new artifact file or current gem_recon_obj.")
                
        # append current event to artifact file
        logger.info("Adding current recon_obj event to artifact file...")

        try:
            with open(wroh_artifact_file_path, 'a', newline=None) as file:    
                file.write(os.linesep + "[" + json.dumps(curr_recon_obj_item) + "]")
                                        
        except Exception as e:
            print(f"Error writing current recon_obj event to artifact file: {str(e)}")
            sys.stdout.flush()
            logger.info(f"End check_recon_obj_hist function: {dt.datetime.now()} ")
            # traceback.print_exc()
            return False

        else:
            logger.info(f"Success writing current recon_obj event to artifact file.")
            logger.info(f"End check_recon_obj_hist function: {dt.datetime.now()} ")
            return True

    ## End Functions enabling Trial event continuity

    def post_process_statistic_generator(self, original_data, ac_df,minute):
        logger.info(f"Start post_process_statistic_generator function: {dt.datetime.now()} ")

        merged = original_data.merge(ac_df, how = 'left', on = 'verbatim') # Merge distinct predictions back to the entire dataset
        merged['label'] = np.where(merged['mean'] > .6, 1, 0) # Creates dichotomous var to represent label assignment
        agg = merged.groupby('participant_id')['label'].sum().reset_index() # Sums total labels for each trial, participant, and minute
        max = agg.groupby('participant_id')['label'].max().reset_index() # Grab max minute and number of labels for each trial and participant
        max['labels_per_min'] = max['label'] / minute # Compute per minute labels for each participant 
        avg_labels_per_minute = max['labels_per_min'].mean() # Grab mean of labels per minute
        std_labels_per_minute = max['labels_per_min'].std() # Grab sd of labels per minute 
        
        logger.info(f"End post_process_statistic_generator function: {dt.datetime.now()} ")

        return(merged, max, avg_labels_per_minute, std_labels_per_minute)

    # Decoupled functions not generic to GEM
    def agent_publish(self, msg_out, subtype, agent_name, helper):

        # global helper, logger
        ap_helper = helper
        # ap_logger = logger     # already invoked at class intantiation 

        # Publish, minding json schema
        agent_msg = "agent/"+agent_name

        logger.info(f"GEM publishing broadcast message to bus on behalf of agent {agent_name}...")

        # ### GOLD FUNCTIONALITY RELATED, turn on later ###
        ### if agent_name == "gold":            
            
        ###     if subtype=="standard":
        ###         # add a blank gold_results object
        ###         msg_out["gold_results"] = []        
        ###     else:
        ###         # add reference to feature inventory previously published in sub-type=standard
        ###         msg_out["gold_feature_inventory"] = []
            
        # send_msg(self, topic, message_type, sub_type, sub_type_version, timestamp=None, data=None, trial_key=None, msg_version="1.1", qos=0):
        
        # Testing 20230206: javascript error interval problem on roll call (?) messages. 
        #     Theory: Perhaps due to our timestamp generation and different format than expected (maybe python version related, maybe not)
        #     Note: Edward mentioned 0207 that roll call messages may be deprecated or change form because study 4 is no longer admin UI based (hence 'adminless_testbed')
        # !! output dict of ap_helper here to ensure it came across 
        # !! supply a properly formatted timestamp, such as a hard coded "2023-02-06T20:18:56.467Z"  # note trunc_nano_to_micro supplies 6-precision, thus multiple variable versions to try
        #     then try:  trunc_nano_to_micro(datetime.utcnow())[:-3] + "Z" , etc.
        
        test_ts = dt.datetime.utcnow()
        test_ts_str = test_ts.strftime("%Y-%m-%dT%H:%M:%S.%f3")
        test_ts_str_micro = self.trunc_nano_to_micro(test_ts.strftime("%Y-%m-%dT%H:%M:%S.%f3")) # + 'Z'
        test_ts_str_micro_z = test_ts_str_micro + 'Z'

        # print(f"Test timestamp generated: {test_ts_str}")
        # sys.stdout.flush()

        # print(f"Methods/Variables available in current scope: {dir()}")
        # sys.stdout.flush()

        # print(f"Dumping ap_helper methods: {dir(ap_helper)}")
        # sys.stdout.flush()

        try:
            ap_helper.send_msg(agent_msg,  
                            "event",
                            subtype,
                            "0.2.0", timestamp=test_ts_str_micro_z,   # test_ts_str , test_ts_str_micro , test_ts_str_micro_z
                            data=msg_out)
        except Exception as e:
            logger.info(f"Something went wrong at message publication. Error: {str(e)}")
            traceback.print_exc()
            sys.stdout.flush()
        else:
            logger.info(f"Message successfully published.")

    ## Function to add additional capability to gold
    # Extract lower level NLP and later merge with gold object for further processing
    def extract_lower_nlp(self, base_recon_df):
        # Lower Level NLP: Part 1

        # total data container to hold all NLP calculations
        total_cols = ['trial','participant_id','curr_minute']
        total_data = base_recon_df.groupby(total_cols).count().reset_index()[total_cols]
        
        try:
            logger.info("Begin lower level NLP calculation.")

            # calculate utterances
            utterances = base_recon_df.groupby(['trial','participant_id', 'curr_minute']).size().reset_index(name='num_utterances')
            # first word
            fw = base_recon_df.copy().sort_values(by=['trial', 'curr_minute', 'message_timestamp'])
            fw[['date', 'time']] = fw['message_timestamp'].str.split("T", n=1, expand=True)
            time_fix = fw['time'].str.split(".", n=1, expand=True)
            fw['time'] = time_fix[0]
            fw['time'] = pd.to_datetime(fw['time'], format='%H:%M:%S')
            fw['time_sec'] = pd.to_timedelta(fw['time'].dt.strftime('%H:%M:%S')).dt.total_seconds().astype(int)
            fw['grouped_diff'] = fw.groupby(['trial', 'curr_minute'])['time_sec'].diff().fillna(0)
            distinct_fw = fw.drop_duplicates('message')
            mean_of_diff = distinct_fw['grouped_diff'].mean()
            sd_of_diff = distinct_fw['grouped_diff'].std()
            big_wait_stat = mean_of_diff + (.05)*sd_of_diff
            fw['first_word'] = np.where(fw['grouped_diff'] >= big_wait_stat, 1, 0)
            fw = fw.sort_values(by=['trial', 'curr_minute', 'message_timestamp'])
            fw['lag_first_word'] = fw.groupby(['trial', 'curr_minute'])['first_word'].shift(-1).fillna(0)
            fw['last_word'] = np.where((fw['first_word'] != 1) & (fw['lag_first_word'] == 1), 1, 0)
            first_last_word = fw.groupby(['trial', 'participant_id', 'curr_minute'])[['first_word', 'last_word']].sum().reset_index()
            first_last_word['dominant_speaker'] = first_last_word['first_word'] / first_last_word['last_word']
            first_last_word.replace([np.inf, -np.inf], np.nan, inplace=True)
            first_last_word['dominant_speaker'] = first_last_word['dominant_speaker'].fillna(0)
            total_ms_speaking = base_recon_df.groupby(['trial', 'participant_id', 'curr_minute'])['message_elapsed_ms'].sum().reset_index(name='time_spent_speaking')

            # calculate average word counts per participants per trial per minute
            counts = base_recon_df.copy()
            counts['word_count'] = counts['message'].str.split().str.len()
            avg_word_freq = counts.groupby(['trial', 'participant_id', 'curr_minute'])['word_count'].mean().reset_index(name='avg_num_words')

            # calculate long utterance metrics
            long_utt = base_recon_df.copy()
            long_utt['word_count'] = long_utt['message'].str.split().str.len()
            long_utt_temp = long_utt.drop_duplicates('message')
            avg_wc = long_utt_temp['word_count'].mean()
            sd_wc = long_utt_temp['word_count'].std()
            long_value = avg_wc + (0.5)*sd_wc
            long_utt['long_utterance'] = np.where(long_utt['word_count'] > long_value, 1, 0)
            long_utt_agg = long_utt.groupby(['trial', 'participant_id', 'curr_minute'])['long_utterance'].sum().reset_index(name='long_utterance')

            total_data = reduce(lambda left, right: pd.merge(left, right, on=['trial', 'participant_id', 'curr_minute'], how='left'),
                                            [total_data, utterances, avg_word_freq, total_ms_speaking, first_last_word, long_utt_agg])                      
        except Exception:
            traceback.print_exc()
            sys.stdout.flush()
            logger.info('Chat data not available (section: extract_lower_nlp, part 1)')

        # Lower Level NLP: Part 2
        try:
            qual_df = base_recon_df[['message_topic','message_timestamp','curr_minute','message_elapsed_ms','participant_id','message','trial']].drop_duplicates()
            qual_df["text"] = [str(x) for x in qual_df["message"]]
            nlp = spacy.load("en_core_web_sm")
            tok_matcher = Matcher(nlp.vocab)
            matcher = PhraseMatcher(nlp.vocab)
            ent_ruler = nlp.add_pipe("entity_ruler")

            approval = ["yes", "ok", "okay", "sure", "alright", "good", "excellent", 
                                "great", "terrific", "copy",  "sounds good", "i agree", "copy that"]
            responsibility = ["ill", "can do", "will do", "take care", "i will", "let me", "ill do"]
            contingent = ["if", "then"]
            planning = ["plan", "goal", "bring", "come", "gather", "get", "be", "should", "need", "lets", "let us"]
            deliberate = ["will", "will go", "to be", "am going"]
            roles = ["medic", "medical", "hammer", "specialist", "search", "doctor"]
            coordination = ["could", "would", "can", "help", "help me"]
            entities = [{"label": "entity", "pattern": [{"LOWER": "i"}]},
                        {"label": "entity", "pattern": [{"LOWER": "me"}]},
                        {"label": "entity", "pattern": [{"LOWER": "red"}]},
                        {"label": "entity", "pattern": [{"LOWER": "blue"}]},
                        {"label": "entity", "pattern": [{"LOWER": "green"}]}]
            action_pattern = [{'POS': 'VERB', 'OP': '?'},
                            {'POS': 'ADV', 'OP': '*'},
                            {'POS': 'AUX', 'OP': '*'},
                            {'POS': 'VERB', 'OP': '+'}]

            matcher.add("approval_terms", [nlp.make_doc(text) for text in approval])
            matcher.add("responsibility_terms", [nlp.make_doc(text) for text in responsibility])
            matcher.add("contingent_terms", [nlp.make_doc(text) for text in contingent])
            matcher.add("planning_terms", [nlp.make_doc(text) for text in planning])
            matcher.add("deliberate_terms", [nlp.make_doc(text) for text in deliberate])
            matcher.add("role_terms", [nlp.make_doc(text) for text in roles])
            matcher.add("coordination_terms", [nlp.make_doc(text) for text in coordination])
            tok_matcher.add("action_terms", [action_pattern])
            ent_ruler.add_patterns(entities)

            doc_codes_vec = []
            actions = []

            for text in qual_df["text"]:

                doc_codes = []
                doc = nlp(text.lower())
                matches = matcher(doc)

                for match_id, start, end in matches:
                    string_id = nlp.vocab.strings[match_id]
                    span = doc[start:end]
                    doc_codes.append(string_id)

                for ent in doc.ents:
                    doc_codes.append(ent.label_)

                tok_matches = tok_matcher(doc)
                actions.append(len([doc[start:end] for _, start, end in tok_matches]))

                doc_codes_vec.append(list(doc_codes))

            qual_df["codes"] = doc_codes_vec
            qual_df["actions"] = actions

            part_codes = qual_df.groupby(["participant_id", "trial", "curr_minute"])["codes"].apply(list)
            action_count = qual_df.groupby(["participant_id", "trial", "curr_minute"])["actions"].sum()

            part_code_vec = []

            for i in part_codes:
                part_code_vec.append(pd.Series(sum(i, [])).explode().value_counts())

            agg_data = pd.concat([pd.DataFrame(part_codes.index),pd.DataFrame(part_code_vec,)], axis = 1).reset_index().fillna(value = 0) 
            action_df = pd.concat([pd.DataFrame(action_count.index, columns = ["group_id"]),pd.DataFrame(action_count.values, columns = ["action_terms"])], axis = 1).fillna(value = 0)

            agg_data[["participant_id", "trial", "curr_minute"]] = pd.DataFrame(agg_data[0].tolist(), index = agg_data.index)
            action_df[["participant_id", "trial", "curr_minute"]] = pd.DataFrame(action_df["group_id"].tolist(),index = action_df.index)

            for key in ["contingent_terms", "deliberate_terms", "PERSON", "entity", "approval_terms", "role_terms", "coordination_terms"]:
                if key not in agg_data:
                    agg_data[key] = 0

            agg_data["planning_all"] = agg_data["contingent_terms"] + agg_data["deliberate_terms"]
            agg_data["entities_all"] = agg_data["PERSON"] + agg_data["entity"]
            agg_data = agg_data[["trial", "participant_id", "curr_minute", "planning_all", "approval_terms","role_terms", "entities_all", "coordination_terms"]]
            agg_data = pd.merge(agg_data, action_df,  how = "left", on = ["participant_id", "trial", "curr_minute"]).drop("group_id", axis = 1)
            # merge term based findings into total_data
            total_data = pd.merge(total_data, agg_data, how = "left", on = ["participant_id", "trial", "curr_minute"])            
            logger.info("End lower level NLP calculation.")
            return total_data
        except Exception:
            traceback.print_exc()
            sys.stdout.flush()
            logger.info('Chat data not available (section: extract_lower_nlp, part 2)')