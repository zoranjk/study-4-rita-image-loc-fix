#!/usr/bin/env python3

"""
Gallup Event Management (GEM)

Authors: Nathan Kress, Maxim van Klinken, Anirban Pal
email: nathan_kress@gallup.com, max_vanklinken@gallup.com, anirban_pal@gallup.com
"""
import sys
import traceback
import pickle
import logging
import os
import re
import json
import math
import csv
import uuid
import pandas as pd
from glob import glob
from pandas.core.frame import DataFrame
from asistagenthelper import ASISTAgentHelper
from datetime import datetime
import spacy
from spacy.matcher import Matcher
from spacy.matcher import PhraseMatcher

from gallup_gem_utils import GEMUtils
from gallup_agent_gold import gold_prediction
from gallup_agent_gelp import Gelp
from GEM_Feature_Engineering import create_and_clean_data

gold = gold_prediction()
gelp = Gelp()
gutils = GEMUtils()

__author__ = 'Gallup'

# This is the function which is called when a message is received for a topic which this Agent is subscribed to.
def on_message(topic, header, msg, data, mqtt_message):
    global helper, extra_info, logger, filter_topics, info_only_event_lst, calculable_events_lst, gem_trial_start, \
        gem_mission_start, events_collected, calculable_events, gem_recon_obj, gold_published_tracker, \
        gelp_published_tracker, ref_participant_lookup, trial_in_progress, study_batch, model_goodies, \
        gelp_interval_sec, gold_interval_sec, elapsed_sec, lst_topic
    # AP: gem - trial start, mission start, recon object, mod pickle file lst
    # AP: published tracker - gelp/gold
    # reset msg_info object to capture, structure necessary info from each relevant message
    msg_info = {}

    time_only_event = False

    if (calculable_events):
        pass
    else:
        calculable_events = 0

    # Test block of survey issue
    topic_subtopic = 'topic: '+topic +' sub_topic: '+ msg['sub_type']
    if topic_subtopic in lst_topic:
        pass
    else:
        lst_topic.append(topic_subtopic)
        print(lst_topic)
    # Now handle the message based on the topic.  Refer to Message Specs for the contents of header, msg, and data
    if topic == 'trial' and msg['sub_type'] == 'start':
        # handle the start of a trial. Note: player lookup info collected at this instant, compatible with following elifs
        logger.info("GEM agent notes Trial Started with Mission set to: " + data['experiment_mission'])
        sys.stdout.flush()
        # log the gem trial start timestamp for future usage
        tmp_ts = re.sub("[Zz]+","", msg["timestamp"])
        tmp_ts = tmp_ts.replace('"', '')
        gem_trial_start = gutils.trunc_nano_to_micro(tmp_ts)

        curr_ts = gem_trial_start
        
        elapsed_ms = 0
        
        # clear any instance of RECON_OBJ_HIST
        RECON_OBJ_HIST = None     

        # clear gold_published_tracker   -- reset at trial start to prevent against subsequent trials not triggering calcs due to 'prior' 
        gold_published_tracker = {"pub_intervals": [], "pub_messages": []}

        # clear gelp_published_tracker   -- reset at trial start to prevent against subsequent trials not triggering calcs due to 'prior' 
        gelp_published_tracker = {"pub_intervals": [], "pub_messages": []}

        trial_in_progress = datetime.utcnow().strftime('%Y%m%d_%H%M%S')    # to facilitate pickled model function calls, will be updated upon each trial start event
        
        participant_info = data["client_info"]            
        # AP: gem
        logger.info(f"Trial Started At: {gem_trial_start}")
        
        logger.info("gold_published_tracker and gelp_published_tracker objects cleared to prevent calc opportunity skips in current trial due to prior pubminute event.")  
        
        logger.info("RECON_OBJ_HIST object cleared for good measure.") 

        logger.info(f"Participant Info: {participant_info}")
        
        # Determine current participants from this event message       
        active_participants = [str(x["participant_id"]) for x in participant_info]       
        
        # check to see if recon_obj_artifact exists, function will load into RECON_OBJ_HIST constant if exists/matches participant list
        loaded_artifact = False

        loaded_artifact = gutils.check_recon_obj_hist(active_participants, RECON_OBJ_HIST, artifact_file_path)
        
        logger.info("Routing participant info to event as topic 'gem_internal/participant_lookup'.")
        
        # update global variable for future reference
        ref_participant_lookup = participant_info
        
        # for update of gem_recon_obj in a few steps
        topic = "gem_internal/participant_lookup"
        msg_info = participant_info

        # TEMPORARY: Publish GELP, GOLD hello world messages
        msg_out_text = "{} says 'Hello, Agent {} reporting for duty!'"
        msg_out = {"message": msg_out_text.format("GOLD","GOLD")}
        gutils.agent_publish(msg_out, "standard", "gold", helper)    # providing subtype is optional, defaults to "bullion" for indicating normal event
        msg_out = {"message": msg_out_text.format("GELP","GELP")}
        gutils.agent_publish(msg_out, "standard", "gelp", helper)    # providing subtype is optional, only makes a differentce right now if GOLD

    if topic == 'trial' and msg['sub_type'] == 'stop':           # Note: DO NOT combine this if statement with above as elif... need prior statement to estalsh initial timestamp
        # handle the stoppage of a trial. Note: no additional info collected at this instant due to elifs
        tmp_ts = re.sub("[Zz]+","", msg["timestamp"])
        tmp_ts = tmp_ts.replace('"', '')        
        curr_ts = gutils.trunc_nano_to_micro(tmp_ts)
        # AP: gem
        logger.info(f"GEM agent notes Trial Stopped at {curr_ts}.")
        sys.stdout.flush()
        
        return False

    elif topic in filter_topics:
        logger.info("Received a message on the topic: " + topic)
        
        if not msg_info: msg_info = data   # allows for passthrough of start-handled event data, such as 'gem_internal/participant_lookup'
        # capture calculable event count for agent kick off later
        if topic in calculable_events_lst:
            calculable_events += 1

        tmp_ts = re.sub("[Zz]+","", header["timestamp"])
        tmp_ts = tmp_ts.replace('"', '')        
        curr_ts = gutils.trunc_nano_to_micro(tmp_ts)

        try:
            diff = abs(datetime.strptime(curr_ts, "%Y-%m-%dT%H:%M:%S.%f") - datetime.strptime(gem_trial_start, "%Y-%m-%dT%H:%M:%S.%f"))
        except:
            diff = abs(datetime.strptime(curr_ts, "%Y-%m-%dT%H:%M:%S") - datetime.strptime(gem_trial_start, "%Y-%m-%dT%H:%M:%S.%f"))
            # traceback.print_exc()

        # register event timestamp to assist with timing of GEM calculation, publication events
        elapsed_ms = diff.total_seconds() * 1000
        curr_minute = math.floor(elapsed_ms / 1000 / 60)
        # Since events (e.g. chat) can happen at any time, if they have happened before 1st minute, roll them to first minute (1)
        if curr_minute==0: 
            curr_minute=1

        # Handle new survey here
        if topic=="player/data/aggregated" and msg["sub_type"]=="Event:PlayerDataAggregated":
            
            # We are only considering Pre Trial surveys
            survey_section = 'PreTrialSurveys'
            print(f"Collecting survey information from {survey_section}")
            
            # Create list of required qids to be filtered
            qid_lst = ['DEMO_1','DEMO_3','DEMO_4']
            for i in range(1,16):
                psy = f'PSY_COL_{i}'
                qid_lst.append(psy)

            # Study 4 to Sudy 3 Mapping for GELP
            qid_map = {'DEMO_1':'QID1_TEXT', 'DEMO_3':'QID4', 'DEMO_4':'QID6', 'PSY_COL_0':'QID13_1', 'PSY_COL_1':'QID13_1',
                    'PSY_COL_2':'QID13_2', 'PSY_COL_3':'QID13_3', 'PSY_COL_4':'QID13_4', 'PSY_COL_5':'QID13_5',
                    'PSY_COL_6':'QID13_6', 'PSY_COL_7':'QID13_7', 'PSY_COL_8':'QID13_8', 'PSY_COL_9':'QID13_9',
                    'PSY_COL_10':'QID13_10', 'PSY_COL_11':'QID13_11', 'PSY_COL_12':'QID13_12', 'PSY_COL_13':'QID13_13', 
                    'PSY_COL_14':'QID13_14', 'PSY_COL_15':'QID13_15'}

            # Select Pre Trial survey only, as that's the point of interest 
            msg_info = msg_info.pop(survey_section)
            msg_info['section'] = survey_section

            # Create unique qid by combining survey id and qid
            for survey in msg_info['surveys']:
                for data in survey['data']:        
                    if data['qid'] is not None:
                        data['qid'] = survey['survey_id']+'_'+data['qid']

            # Create final output dictionary with only required fields
            output_dict = {'participant_id': msg_info['participant_id']}

            # Create final dict as participant, specific question and response
            for survey in msg_info['surveys']:
                if survey['complete']:
                    for item in survey['data']:
                        if item['qid'] in qid_lst:
                            output_dict[item['qid']] = item['response']
                        else:
                            pass

            survey_dict = {qid_map.get(key, key): value for key, value in output_dict.items()}
            # Add the section for GELP requirement
            survey_dict['surveyname'] = survey_section
            # Wrap everything in 'values' for GELP requirement
            survey_dict = {'values': survey_dict}
            # Add response id for GELP expectation
            survey_dict["responseId"] = str(uuid.uuid4())

            survey_item = {"message_topic": topic, "artifact": "False", "message_timestamp": curr_ts, 
                    "curr_minute": curr_minute, "message_elapsed_ms": elapsed_ms, "message": survey_dict}
                       
            # Update topic to old topic for GELP processing
            survey_item["message_topic"] = "status/asistdataingester/surveyresponse"
            # Add to recon. "player/data/aggregated" would be captured in recon down in filter_topics section
            gem_recon_obj.append(survey_item)
    else:
        # register event timestamp to assist with timing of GEM calculation, publication events
        tmp_ts = re.sub("[Zz]+","", header["timestamp"])
        tmp_ts = tmp_ts.replace('"', '')        
        curr_ts = gutils.trunc_nano_to_micro(tmp_ts)
        
        try:
            diff = datetime.strptime(curr_ts, "%Y-%m-%dT%H:%M:%S.%f") - datetime.strptime(gem_trial_start, "%Y-%m-%dT%H:%M:%S.%f")
        except:
            try:
                diff = datetime.strptime(curr_ts, "%Y-%m-%dT%H:%M:%S") - datetime.strptime(gem_trial_start, "%Y-%m-%dT%H:%M:%S.%f")
            except Exception as e:
                print(f"Time stamp format issue: curr_ts={curr_ts}, gem_mission_start={gem_trial_start}. Continuing as intended and setting event as time_only_event=True with diff=0")
                # traceback.print_exc()
                placeholder_ts = datetime.now()
                diff = placeholder_ts - placeholder_ts

        time_only_event = True

    if gem_trial_start:
        # Determine current participants from object stored in related global constant
        active_participants = [str(x["participant_id"]) for x in ref_participant_lookup]

        elapsed_ms = diff.total_seconds() * 1000
        elapsed_sec = math.trunc(diff.total_seconds())
        curr_minute = math.floor(elapsed_ms / 1000 / 60)
        # Since events (e.g. chat) can happen at any time, if they have happened before 1st minute, roll them to first minute (1)
        if curr_minute==0: 
            curr_minute=1

        if time_only_event == False:
            logger.info(f"---\nmsg: {topic}. Elapsed time in ms: {elapsed_ms}")

            # construct curr_data_obj
            curr_data_obj = {"timestamp": curr_ts, "elapsed_milliseconds": elapsed_ms, "topic": topic, "message": msg_info}

            logger.info(f"<output curr_data_obj: <currently silenced>")  # {curr_data_obj}")

            # if we can yield a participant_id, append event to gem_recon_obj            
            found_pid = False
            yielded_pid = None

            logger.info(f"Variable type for curr_data_obj['message']: {type(curr_data_obj['message'])}")

            # setup if elif else
            if topic=="communication/chat":
                if ("sender_id" in curr_data_obj["message"].keys()):
                    # rturns actual participant_id
                    yielded_pid = curr_data_obj["message"]["sender_id"]                                        
                    # set flag
                    found_pid = True
            elif topic=="agent/AC_UAZ_TA1_DialogAgent":                
                
                if ("participant_id" in curr_data_obj["message"].keys()):
                    # returns a handle or call sign, not actual participant id, e.g. red, blue, etc.
                    yielded_pid = curr_data_obj["message"]["participant_id"] 
                    # from global var, find the corresponding actual pid (PXXXXXX) for that call sign and assign back
                    yielded_pid = next(item for item in ref_participant_lookup if item["callsign"] == yielded_pid)["participant_id"]
                    # update the value of participant id in current message, in stead of showing call sign
                    curr_data_obj["message"].update(participant_id=yielded_pid)
                    # set flag
                    found_pid = True
            else:
                logger.info("Participant id generic handler triggered.")

                if isinstance(curr_data_obj["message"], list):          
                    for p_rec in curr_data_obj["message"]:
                        if "unique_id" in p_rec.keys():
                            if ((p_rec["unique_id"] is not None) and (p_rec["unique_id"] != "")):
                                found_pid = True
                                yielded_pid = p_rec["unique_id"]
                        if "participant_id" in p_rec.keys():
                            if ((p_rec["participant_id"] is not None) and (p_rec["participant_id"] != "")): 
                                found_pid = True
                                yielded_pid = p_rec["participant_id"]
                            
                if isinstance(curr_data_obj["message"], dict):           
                    if ("participant_id" in curr_data_obj["message"].keys()):
                        if ((curr_data_obj["message"]["participant_id"] is not None) and (curr_data_obj["message"]["participant_id"] != "")): 
                            found_pid = True
                            yielded_pid = curr_data_obj["message"]["participant_id"]

            # if our event type is 'status/asistdataingester/surveyresponse', for convenience we need to inject a 'participant_id'
            #   at the root level to match the payload contents (given 'uniqueid' or otherwise)
            logger.info("Completed type check.")

            # assign raw event data
            
            if found_pid == False:
                # no participant_id, fail event and continue
                logger.info("FAIL on participant_id, uniqueid checks.")
                return False
            else:
                if topic not in info_only_event_lst: events_collected += 1

                logger.info(f"- found/determined a participant_id for this event: {yielded_pid}")

            # update gem_obj
            if topic in filter_topics:
                # for 'communication/chat' message, rename sender_id to participant_id to maintain uniform structure
                if topic=="communication/chat":
                    msg_info = {"participant_id" if k == "sender_id" else k:v for k,v in msg_info.items()}                    

                new_event_item = {"message_topic": topic, "artifact": "False", "message_timestamp": curr_ts, 
                                    "curr_minute": curr_minute, "message_elapsed_ms": elapsed_ms, "message": msg_info}

                gem_recon_obj.append(new_event_item)

                logger.info("- Event added to recon_obj.")

            logger.info(f"elapsed_ms: {elapsed_ms} ; curr_minute: {curr_minute} ; events_collected: {events_collected} ; gem_trial_start: {gem_trial_start} ; gold_published_tracker items: {len(gold_published_tracker['pub_intervals'])} ; gelp_published_tracker items: {len(gelp_published_tracker['pub_intervals'])}")
        sys.stdout.flush()

       # AP: separate gelp and gold condition
        # gold specific logic
        # - temp output GOLD calculation trigger formula components to help troubleshoot
        # if topic in filter_topics:
        #     print(f"GOLD: gem_trial_start ? | {gem_trial_start} | {(gem_trial_start is not None)}")
        #     sys.stdout.flush()
        #     print(f"GOLD: elapsed_sec > 0 ? | {elapsed_sec} > 0 | {(elapsed_sec > 0)}")
        #     sys.stdout.flush()
        #     print(f"GOLD: curr_minute > 0 ? | {curr_minute} > 0 | {(curr_minute > 0)}")
        #     sys.stdout.flush()
        #     print(f"GOLD: events_collected > 0 ? | {events_collected} > 0 | {(events_collected > 0)}")
        #     sys.stdout.flush()
        #     print(f"GOLD: calculable_events > 0 ? | {calculable_events} > 0 | {(calculable_events > 0)}")
        #     sys.stdout.flush()
        #     print(f"GOLD: curr_minute not in gold_published_tracker['pub_intervals'] ? | {curr_minute} not in [{','.join(str(x) for x in gold_published_tracker['pub_intervals'])}] | {(curr_minute not in gold_published_tracker['pub_intervals'])}")
        #     sys.stdout.flush()

        # if ( (gem_trial_start) and (elapsed_sec > 0) and (elapsed_sec % gold_interval_sec == 0) and (events_collected > 0) and (calculable_events > 0) \
        #      and (elapsed_sec not in gold_published_tracker["pub_intervals"]) ):

        # Placeholder condition. Revisit if interval changes from 60 seconds in future
        if ( (gem_trial_start) and (elapsed_sec > 0) and (events_collected > 0) and (calculable_events > 0) and (curr_minute not in gold_published_tracker["pub_intervals"]) ):
            # Note: recall that info only events (info_only_event_lst) aren't counted in events_collected            
            logger.info(f"GOLD calcs triggered at elapsed_sec {elapsed_sec} for minute {curr_minute}.")

            msg_out = ""
            gold_result = None

            # convert recon object from list to df to send to model
            base_recon_df = pd.DataFrame(gem_recon_obj)
            # GOLD is dealing with ONLY communication/chat messages
            base_recon_df = base_recon_df[base_recon_df.message_topic=='communication/chat']
            # GOLD needs only certain columns
            gold_cols_to_keep = ['message_topic','curr_minute','participant_id','message']            
            # find the max minute for each participant when they talked, and combine the verbatims till then
            base_recon_df = pd.concat([base_recon_df.drop('message', axis=1), base_recon_df['message'].apply(pd.Series)], axis=1)
            base_recon_df['trial'] = -999
            base_recon_df['team'] = -999
            gold_recon_df = base_recon_df[gold_cols_to_keep]
            gold_recon_df = gold_recon_df.groupby('participant_id').agg({'curr_minute': 'max','message': lambda x: '[' + ','.join(['"{}"'.format(s) for s in x]) + ']'})
            # since GOLD code is expecting certain names for the columns, change the column names accordingly
            gold_recon_df = gold_recon_df.rename(columns={'curr_minute': 'file time','message': 'text'})            
            gold_recon_df.reset_index(drop=False, inplace=True)   
            # Get output from lower level NLP processing
            llnlp_df = gutils.extract_lower_nlp(base_recon_df)            
            
            # filter only the current minute data since that will be joined to gold output
            # The curr_minute at variable level at this point could be leading from the curr_minute
            # inside the object. This is thoughtfully used while filtering lower level nlp df.
            # E.g. when curr_min var=8, nlp.curr_min = gold_recon_df.file_time = 7. Here, nlp data at min=7 is filtered
            # And then it is assigned with curr_min var (8) (similar to gold output), so that the new time gets added to gem_recon_obj.            
            llnlp_df = llnlp_df.sort_values(by='curr_minute').groupby('participant_id').last()
            # llnlp_df = llnlp_df[llnlp_df.curr_minute==gold_recon_df['file time'].max()]            
            llnlp_df['curr_minute'] = curr_minute
            # trial at this point is just a placeholder and not used in GOLD. Dropping for now with future reconsideration
            llnlp_df = llnlp_df.drop(["trial"], axis=1)
            # Get GOLD output
            get_all_zscores, get_all_adj_zscores = gold.prediction_zscore(gold_recon_df)
            logger.info("  [Now returned to OnMessage function]")

            # check for returned object from gold models
            if get_all_zscores and get_all_adj_zscores and isinstance(get_all_zscores, dict) and isinstance(get_all_adj_zscores, dict):
                z_df = pd.DataFrame()
                az_df = pd.DataFrame()
                # from the dictionary returned, if the value is a dataframe, capture gold stats and append to corresponding datafarme
                # Convert the dictionary to a single dataframe
                z_df = pd.concat(get_all_zscores.values(), keys=get_all_zscores.keys())
                # Reset the index
                z_df = z_df.reset_index(level=1, drop=True).reset_index()
                # Rename the columns
                z_df.columns = ['attribute', 'participant_id', 'zscore', 'count']
                # Convert the dictionary to a single dataframe
                az_df = pd.concat(get_all_adj_zscores.values(), keys=get_all_adj_zscores.keys())
                # Reset the index
                az_df = az_df.reset_index(level=1, drop=True).reset_index()
                # Rename the columns
                az_df.columns = ['attribute', 'participant_id', 'adj_zscore']
                # left join z score and adjusted z score to create resultant dictionary
                cols = ['participant_id','attribute','zscore','adj_zscore','count']       
                tmp_gold_df = pd.merge(z_df, az_df, on=['participant_id', 'attribute'], how='left').reindex(cols, axis=1) 
                tmp_gold_df['curr_minute'] = curr_minute
                # Add lower level NLP with GOLD output (higher level NLP)
                # pre-processing of gold from long to wide since nlp is in wide + downstream requirement
                # 1. pivot the dataframe
                tmp_gold_df_wide = tmp_gold_df.pivot(index=['participant_id','curr_minute'], columns='attribute')
                # 2. flatten the column names
                tmp_gold_df_wide.columns = [f'{col[1]}_{col[0]}' for col in tmp_gold_df_wide.columns]
                # 3. reset the index
                tmp_gold_df_wide.reset_index(inplace=True)
                # join gold with nlp
                gold_df = pd.merge(tmp_gold_df_wide, llnlp_df, on=['participant_id', 'curr_minute'], how='left').set_index(tmp_gold_df_wide.index)
                # attribute names: map between what returned from model to what needed by gelp
                gold_mapping = {'Compensatory Helping_zscore': 'mod_lgb_Compensatory_Helping_per_min',
                                'Contingent Planning_zscore': 'mod_lgb_Contingent_Planning_per_min',
                                'Deliberate Planning_zscore': 'mod_lgb_Deliberate_Planning_per_min',
                                'Explicit Coordination_zscore': 'mod_lgb_Explicit_Coordination_per_min', #new code
                                'Motivating Confidence Building_zscore': 'mod_lgb_Motivation_per_min',
                                'Planning Map Coverage_zscore': 'mod_lgb_Planning_Map_Coverag_per_min', #new code
                                'Reactive Planning_zscore': 'mod_lgb_Reactive_Planning_per_min',
                                'Transactive Memory_zscore': 'mod_lgb_Transactive_Memory_per_min',
                                'confirmation_zscore': 'mod_lgb_confirmation', #new code
                                'question answered_zscore': 'mod_lgb_question_answered', #new code
                                'question asked_zscore': 'mod_lgb_question_asked', #new code
                                'time_zscore': 'mod_lgb_time', #new code
                                'num_utterances': 'agg_num_utterances',
                                'avg_num_words': 'agg_avg_num_words',
                                'time_spent_speaking': 'agg_time_spent_speaking',
                                'first_word': 'agg_first_word',
                                'last_word': 'agg_last_word',
                                'dominant_speaker': 'agg_dominant_speaker',
                                'long_utterance': 'agg_long_utterance',
                                'planning_all': 'agg_planning_all',
                                'approval_terms': 'agg_approval_terms',
                                'role_terms': 'agg_role_terms',
                                'entities_all': 'agg_entities_all',
                                'coordination_terms': 'agg_coordination_terms',
                                'action_terms': 'agg_action_terms',
                                'Deliberate Planning_count': 'mod_lgb_Deliberate_Planning_count',
                                'Transactive Memory_count': 'mod_lgb_Transactive_Memory_count'}
                gold_df = gold_df.rename(columns=gold_mapping)
                gold_df = gold_df.fillna(0)
                gold_dict = gold_df.to_dict('records')                

                # probably not needed
                gold_gr = tmp_gold_df.groupby(['participant_id','attribute'])

                gold_op = {}
                gold_op["gold_msg_id"] = str(uuid.uuid4())
                gold_op["gold_pub_minute"] = curr_minute
                gold_op["created_ts"] = curr_ts
                # converting message to string as GELP needs it downstream                
                gold_op["gold_results"] = gold_dict
            else:
                print("GOLD objects not generated properly.")
                sys.stdout.flush()

            if gold_op:

                agent_name = "gold"

                # copy the gold data in message object to maintain same output format as gelp in the message bus
                msg_out = gold_op.copy()

               # publish, minding json schema
                logger.info(f"GOLD publishing broadcast message to bus.")

                #   Note: this call will be different due to class assignments when live
                gutils.agent_publish(msg_out, "event", agent_name, helper)

                # note publication to gold_published_tracker
                gold_published_tracker["pub_intervals"].append(curr_minute)
                gold_published_tracker["pub_messages"].append(msg_out)      

                if gem_recon_obj:
                    gold_result_item = {"message_topic": "agent/gold", "artifact": "False", "message_timestamp": curr_ts, 
                    "curr_minute": curr_minute, "message_elapsed_ms": elapsed_ms, "message": json.dumps(gold_op)}
                    gem_recon_obj.append(gold_result_item)

        # gelp specific logic
        # - temp output GELP calculation trigger formula components to help troubleshoot
        # if topic in filter_topics:
        #     print(f"GELP: gem_trial_start ? | {gem_trial_start} | {(gem_trial_start is not None)}")
        #     sys.stdout.flush()
        #     print(f"GELP: elapsed_sec > 0 ? | {elapsed_sec} > 0 | {(elapsed_sec > 0)}")
        #     sys.stdout.flush()
        #     print(f"GELP: curr_minute > 0 ? | {curr_minute} > 0 | {(curr_minute > 0)}")
        #     sys.stdout.flush()
        #     print(f"GELP: events_collected > 0 ? | {events_collected} > 0 | {(events_collected > 0)}")
        #     sys.stdout.flush()
        #     print(f"GELP: calculable_events > 0 ? | {calculable_events} > 0 | {(calculable_events > 0)}")
        #     sys.stdout.flush()
        #     print(f"GELP: curr_minute not in gelp_published_tracker['pub_intervals'] ? | {curr_minute} not in [{','.join(str(x) for x in gelp_published_tracker['pub_intervals'])}] | {(curr_minute not in gelp_published_tracker['pub_intervals'])}")
        #     sys.stdout.flush()

        # if ( (gem_trial_start) and (elapsed_sec > 0) and (elapsed_sec % gelp_interval_sec == 0) and (events_collected > 0) and (calculable_events > 0) \
        #      and (elapsed_sec not in gelp_published_tracker["pub_intervals"]) ):

        # Placeholder condition. Revisit if interval changes from 60 seconds in future
        if ( (gem_trial_start) and (elapsed_sec > 0) and (events_collected > 0) and (calculable_events > 0) and (curr_minute not in gelp_published_tracker["pub_intervals"]) ):
            # Note: test criteria, should fire every 10 seconds after trial start

            # calculate
            logger.info(f"GELP calcs simulated with well-formatted test return for interval {elapsed_sec}.")

            gelp_result = None

            # convert recon object from list to df to send to model
            gelp_recon_df = pd.DataFrame(gem_recon_obj) 
            # converting the atrifact column from string to boolean
            mapping = {'True': True, 'False': False}
            # apply the mapping to the 'artifact' column using the apply method
            gelp_recon_df['artifact'] = gelp_recon_df['artifact'].apply(lambda x: mapping[x])
            # convert message data type back from list to string since Gelp expects it as string
            gelp_recon_df['message'] = gelp_recon_df["message"].astype(str)
            # adding dummy index so that it is procesed properly in gelp
            gelp_recon_df.reset_index(drop=False, inplace=True)

            # placeholder for now
            team_name = -999
            trial_id = -999 

            gelp_clean = create_and_clean_data(gelp_recon_df,team_name,trial_id,curr_minute)

            # store gelp output
            gelp_result = {}
            gelp_result["gelp_msg_id"] = str(uuid.uuid4())
            gelp_result["gelp_pub_minute"] = curr_minute
            gelp_result["created_ts"] = curr_ts              
            gelp_result["gelp_results"] = gelp.predict(gelp_clean)

            logger.info("  [Now returned to OnMessage function]")

            if gelp_result:

                agent_name = "gelp"

                msg_out = gelp_result

                # publish, minding json schema
                logger.info(f"GELP publishing broadcast message to bus.")

                #   Note: this call will be different due to class assignments when live
                gutils.agent_publish(msg_out, "event", agent_name, helper)

                # note publication to gelp_published_tracker
                gelp_published_tracker["pub_intervals"].append(curr_minute)
                gelp_published_tracker["pub_messages"].append(msg_out)

                if gem_recon_obj:
                    gelp_result_item = {"message_topic": "agent/gelp", "artifact": "False", "message_timestamp": curr_ts, 
                    "curr_minute": curr_minute, "message_elapsed_ms": elapsed_ms, "message": gelp_result}
                    gem_recon_obj.append(gelp_result_item)

# GEM
### MAIN
# Agent Initialization
study_batch = ""

gem_recon_obj = []
artifact_file_path = os.path.join("resource", "recon_obj_artifact.json") 
RECON_OBJ_HIST = None
gem_trial_start = None
gem_mission_start = None
events_collected = 0
calculable_events = 0
elapsed_sec = None
gelp_interval_sec = 60
gold_interval_sec = 60
gelp_published_tracker = {"pub_intervals": [], "pub_messages": []}       
gold_published_tracker = {"pub_intervals": [], "pub_messages": []}  
ref_participant_lookup = {}
lst_topic = []

trial_in_progress = datetime.utcnow().strftime('%Y%m%d_%H%M%S')    

# note specific event topics we want to collect
filter_topics = [#"status/asistdataingester/surveyresponse",
                 "gem_internal/participant_lookup",
                 "communication/chat",
                 "agent/AC_UAZ_TA1_DialogAgent",
                 "player/data/aggregated"]

# note which event topics are information only and although collected, shouldn't count toward triggering GELP calculator
#  - some event types have initiation/heartbeat/shutdown messages, those will have to be handled individually to determine what to ignore (ex: agent/dialog)
info_only_event_lst = [#"status/asistdataingester/surveyresponse",
                       "player/data/aggregated",
                       "gem_internal/participant_lookup"]
 
calculable_events_lst = ["communication/chat",
                         "agent/AC_UAZ_TA1_DialogAgent"]

helper = ASISTAgentHelper(on_message)

# Set the helper's logging level to INFO
LOG_HANDLER = logging.StreamHandler()
LOG_HANDLER.setFormatter(logging.Formatter("%(asctime)s | %(name)s | %(levelname)s — %(message)s"))
helper.get_logger().setLevel(logging.INFO)
helper.get_logger().addHandler(LOG_HANDLER)


# Create our own logger for the Agent
# logger = logging.getLogger("Gallup_Agent_GELP")
logger = logging.getLogger("Gallup_GEM")
logger.setLevel(logging.INFO)
logger.addHandler(LOG_HANDLER)


# init here


# Subscribe to relevant message bus events by topic
#   ex: helper.subscribe('observations/events/player/tool_used')
#   ex: helper.unsubscribe('observations/events/player/triage', 'event', 'Event:Triage')
helper.subscribe('status/asistdataingester/surveyresponse')   # injected survey responses
helper.subscribe('communication/chat')  # chat messages
helper.subscribe('agent/AC_UAZ_TA1_DialogAgent')  # transcription rules

# Set the agents status to 'up' and start the agent loop on a separate thread
helper.set_agent_status(helper.STATUS_UP)
logger.info("Starting Gallup GEM Wrapper Loop on a separate thread.")
helper.start_agent_loop_thread()
logger.info("Gallup GEM is now running...")

curr_version = "0.0.14"
logger.info("   ____       _ _                        ____ _____ __  __  ")
logger.info("  / ___| __ _| | |_   _ _ __            / ___| ____|  \/  | ")
logger.info(" | |  _ / _` | | | | | | '_ \   _____  | |  _|  _| | |\/| | ")
logger.info(" | |_| | (_| | | | |_| | |_) | |_____| | |_| | |___| |  | | ")
logger.info("  \____|\__,_|_|_|\__,_| .__/           \____|_____|_|  |_| ")
logger.info(f"                       |_|               -- v {curr_version} --  ")

# Stop the agent thread if desired/necessary
# helper.stop_agent_loop_thread()