#!/usr/bin/env python3

import os
import json
import numpy as np
import math
import copy
import logging
from asistagenthelper import ASISTAgentHelper
from operator import itemgetter

trial_infos = {}

NUM_PLAYERS = 3
VERSION = '4.0.0'
RISK_DISTANCE = 10

team_score = {}
player_locations = {}
object_interaction_list = []
trial_m2_values = []
successful_defuses = 0
total_objects = 0
player_error_count = 0
chat_messages = []
intervention_messages = []
planning_phase_actions = []
planning_phase_chat = []
beacons_placed = []
current_phase = ''
env_list = []
pre_perturbation_score_available = 0
pre_perturbation_score = 0
pre_perturbation_period = 0
post_perturbation_score_available = 0
post_perturbation_score = 0
post_perturbation_period = 0
risk_value = 0

# This is the function which is called when a message is received for a to
# topic which this Agent is subscribed to.
def on_message(topic, header, msg, data, mqtt_message):

    global team_score
    global object_interaction_list
    global trial_m2_values
    global successful_defuses
    global total_objects
    global player_error_count
    global chat_messages
    global intervention_messages
    global planning_phase_actions
    global planning_phase_chat
    global current_phase
    global beacons_placed
    global env_list
    global pre_perturbation_score_available
    global pre_perturbation_score
    global pre_perturbation_period
    global post_perturbation_score_available
    global post_perturbation_score
    global post_perturbation_period
    global risk_value

    # Now handle the message based on the topic.  Refer to Message Specs for the contents of header, msg, and data
    if topic == 'trial':
        if msg['sub_type'] == 'start':
            print('trial started')
            # handle the start of a trial!!
            trial_info = data
            trial_info['experiment_id'] = msg['experiment_id']
            trial_info['trial_id'] = msg['trial_id']
            trial_info['replay_id'] = msg['replay_id'] if 'replay_id' in msg.keys() else None
            trial_info['replay_root_id'] = msg['replay_root_id'] if 'replay_root_id' in msg.keys() else None

            trial_key = trial_info['trial_id']
            if 'replay_id' in trial_info and not trial_info['replay_id'] is None:
                trial_key = trial_key + ":" + trial_info['replay_id']
            trial_infos[trial_key] = trial_info
            
            # reset measures at trial start
            team_score.clear()
            player_locations.clear()
            successful_defuses = 0
            total_objects = 0
            player_error_count = 0
            chat_messages = []
            intervention_messages= []
            object_interaction_list = []
            trial_m2_values = []
            planning_phase_actions = []
            planning_phase_chat = []
            beacons_placed = []
            current_phase = ''
            env_list = []
            pre_perturbation_score_available = 0
            pre_perturbation_score = 0
            pre_perturbation_period = 0
            post_perturbation_score_available = 0
            post_perturbation_score = 0
            post_perturbation_period = 0
            risk_value = 0


        if msg['sub_type'] == 'stop':
            print('trial stopped')
            trial_key = msg['trial_id']
            if 'replay_id' in msg and not msg['replay_id'] is None:
                trial_key = trial_key + ":" + msg['replay_id']
            if trial_key in trial_infos.keys():
                trial_info = trial_infos[trial_key]

                team_score.clear()
                player_locations.clear()
                successful_defuses = 0
                total_objects = 0
                player_error_count = 0
                chat_messages = []
                intervention_messages= []
                object_interaction_list = []
                trial_m2_values = []
                planning_phase_actions = []
                planning_phase_chat = []
                beacons_placed = []
                env_list = []
                pre_perturbation_score_available = 0
                pre_perturbation_score = 0
                pre_perturbation_period = 0
                post_perturbation_score_available = 0
                post_perturbation_score = 0
                post_perturbation_period = 0
                risk_value = 0




    elif topic == 'observations/events/mission':
        if data['mission_state'] == 'Stop':
            print('Mission stopped')
            #m1
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M1',
                measure_value=team_score['teamScore'], measure_description='Mission Score', datatype='integer', data=team_score)

            #m2
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M2',
                measure_value=np.around(np.mean(trial_m2_values), 4), measure_description='Synchronization', datatype='double', data={})
            #m3
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M3',
                measure_value=np.around(np.mean(successful_defuses/total_objects), 4)*100, measure_description='Error Rate', datatype='double', data={})
            #m3-2
            interaction_count = 0
            for obj in object_interaction_list:
                if obj['interacted']:
                    interaction_count = interaction_count + 1
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M3.2',
                measure_value=np.around(np.mean(player_error_count/interaction_count), 4)*100, measure_description='Error Rate 2', datatype='double', data={})

            #m4    (post-perturbation period score/post-perturbation period mins)/(points available to score at start of post-perturbation period) / 
            #       (pre-perturbation period score/pre-perturbation period mins)/(points available to score at start of pre-perturbation period)
            post_perturbation_score = team_score['teamScore'] - pre_perturbation_score
            post_perturbation_period = int(data['mission_timer'].split(':')[0].strip()) - pre_perturbation_period


            if post_perturbation_period and post_perturbation_score_available and pre_perturbation_period and pre_perturbation_score_available:
                value = ((post_perturbation_score/post_perturbation_period)/post_perturbation_score_available)/((pre_perturbation_score/pre_perturbation_period)/pre_perturbation_score_available)
            else:
                value = 0

            m4_details = {
                'pre_perturbation_score': pre_perturbation_score,
                'pre_perturbation_period': pre_perturbation_period,
                'pre_perturbation_score_available': pre_perturbation_score_available,
                'post_perturbation_score': post_perturbation_score,
                'post_perturbation_period': post_perturbation_period,
                'post_perturbation_score_available': post_perturbation_score_available
            }

            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M4',
                measure_value=round(value, 4), measure_description='Adaptation / Resilience', datatype='double', data=m4_details)

            #m5
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M5',
                measure_value=len(chat_messages), measure_description='Coordinative Comms', datatype='integer', data=chat_messages)
            #m8
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M8',
                measure_value=len(intervention_messages), measure_description='Count of ASI Interventions', datatype='integer', data=intervention_messages)

            #m14
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M14',
                measure_value=risk_value, measure_description='Risk', datatype='integer', data={})

            #m16
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M16',
                measure_value=len(planning_phase_actions), measure_description='Team Engagement in Planning', datatype='integer', data=planning_phase_actions)

            #m17
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M17',
                measure_value=len(planning_phase_chat), measure_description='Planning Communication Count', datatype='integer', data=planning_phase_chat)
            
            #m18
            publish_measure(header['timestamp'], elapsed_milli=data['elapsed_milliseconds'], event_type='event', message_type='trial', sub_type='stop',measure_id='ASI-M18',
                measure_value=len(beacons_placed), measure_description='Recon Communication Count', datatype='integer', data=beacons_placed)


            team_score.clear()
            player_locations.clear()
            successful_defuses = 0
            total_objects = 0
            player_error_count = 0
            chat_messages = []
            intervention_messages= []
            object_interaction_list = []
            trial_m2_values = []
            planning_phase_actions = []
            planning_phase_chat = []
            beacons_placed = []
            current_phase = ''
            env_list = []
            pre_perturbation_score_available = 0
            pre_perturbation_score = 0
            pre_perturbation_period = 0
            post_perturbation_score_available = 0
            post_perturbation_score = 0
            post_perturbation_period = 0
            risk_value = 0



    # Calculate ASI-M1
    elif topic == 'score/change':
        print('Scoreboard updated - TeamScore: ', data['teamScore'])
        trial_key = msg['trial_id']
        if 'replay_id' in msg and not msg['replay_id'] is None:
            trial_key = trial_key + ":" + msg['replay_id']
        if trial_key in trial_infos.keys():
            trial_info = trial_infos[trial_key]
            team_score = data


    # Calculate ASI-M2/M3
    elif topic == 'object/state/change':
        trial_key = msg['trial_id']
        if 'replay_id' in msg and not msg['replay_id'] is None:
            trial_key = trial_key + ":" + msg['replay_id']
        if trial_key in trial_infos.keys():
            trial_info = trial_infos[trial_key]

            new_obj = True
            initiated = data['currAttributes']['outcome']
            defused = data['currAttributes']['outcome'] == 'DEFUSED'
            disposed = data['currAttributes']['outcome'] == 'DEFUSED_DISPOSER'
            exploded = data['currAttributes']['outcome'] == 'EXPLODE_TIME_LIMIT' or data['currAttributes']['outcome'] == 'EXPLODE_TOOL_MISMATCH' or data['currAttributes']['outcome'] == 'EXPLODE_CHAINED_ERROR'
            player_error = data['currAttributes']['outcome'] == 'EXPLODE_TOOL_MISMATCH' or data['currAttributes']['outcome'] == 'EXPLODE_CHAINED_ERROR'
            if defused or disposed:
                successful_defuses = successful_defuses + 1
                print('ASI-M3 ' + data['id'] + ' defused successfully. total count: {}/{}'.format(successful_defuses, total_objects))
            if player_error:
                player_error_count = player_error_count + 1
                print('ASI-M3-2 player error ' + data['id'] + ' exploded. count: {}'.format(player_error_count))
            for obj in object_interaction_list:
                if obj['id'] == data['id']:
                    # assume obj interaction occurs if sequence is incremented
                    obj['interacted'] = data['currAttributes']['sequence_index'] != '0'
                    if defused: # dont add the defused message
                        obj['complete'] = defused
                    else:
                        obj['steps'].append(data)
                        obj['complete'] = data['currAttributes']['active'] == 'false' and not exploded
                    #calc average step
                    if obj['complete'] and len(obj['steps']) > 1:
                        step_times = []
                        avg_time = 0
                        for i, step in enumerate(obj['steps'][1:]):
                            step_times.append(obj['steps'][i+1]['elapsed_milliseconds'] - obj['steps'][i]['elapsed_milliseconds'])
                        avg_time = np.around(np.mean(step_times), 4)
                        print('ASI-M2: {}, # steps: {} avg time per step {} seconds.'.format(obj['id'], len(obj['steps']), avg_time/1000))
                        trial_m2_values.append(avg_time/1000)
                    new_obj = False
            if new_obj and not defused:
                objStepDict = {
                    "id": data['id'],
                    "steps": [],
                    "complete": data['currAttributes']['active'] == 'false',
                    "avg_step": 0,
                    "interacted": data['currAttributes']['sequence_index'] != '0'
                }
                objStepDict['steps'].append(data)
                object_interaction_list.append(objStepDict)
                # check risk
                if data['triggering_entity'] != 'SERVER':
                    print('risk factor')

                    a = np.array([])
                    b = np.array([])
                    c = np.array([])

                    a = np.array([player_locations[data['triggering_entity']]['x'], player_locations[data['triggering_entity']]['y'], player_locations[data['triggering_entity']]['z']])
                    for player, location in player_locations.items():
                        if player != data['triggering_entity']:
                            if b.size == 0:
                                b = np.array([location['x'], location['y'], location['z']])
                            else:
                                c = np.array([location['x'], location['y'], location['z']])

                    dist_a = np.linalg.norm(a-b)
                    dist_b = np.linalg.norm(a-c)

                    print('distance to player 1 : {}'.format(dist_a))
                    print('distance to player 2 : {}'.format(dist_b))

                    if dist_a > RISK_DISTANCE and dist_b > RISK_DISTANCE:
                        risk_value = risk_value +1
                        print('interaction deemed at risk')

    # get list of objects on map
    elif topic == 'environment/created/list' or topic == 'groundtruth/environment/created/list':
        total_objects = len(data['list'])
        if not env_list:
            for obj in data['list']:
                env_list.append(obj)
                if obj['type'].startswith('block_bomb'):
                    pre_perturbation_score_available = pre_perturbation_score_available + (len(obj['currAttributes']['sequence']) * 10) # 10 pts per step per object

    # get chat messages
    elif topic == 'communication/chat':
        chat_messages.append(data)
        if current_phase != 'FIELD_STAGE':
            planning_phase_chat.append(data)

    # get interventions
    elif topic.startswith('agent/intervention/') and topic.endswith('/chat'):
        intervention_messages.append(data)

    # get planning phase actions - dont include vote leave store and tool purchase
    elif topic == 'ui/click':
        if data['additional_info']['meta_action'] != 'VOTE_LEAVE_STORE' and not data['additional_info']['meta_action'].startswith('PROPOSE_TOOL_PURCHASE_'):
            planning_phase_actions.append(data)

    # get beacons placed
    elif topic == 'environment/created/single':
        if data['obj']['type'] == 'block_beacon_bomb':
            beacons_placed.append(data)

    elif topic == 'environment/removed/single':
        for i, obj in enumerate(env_list):
            if obj['id'] == data['obj']['id']:
                env_list.pop(i)
                print('removed {} - {}'.format(obj['id'], data['obj']['currAttributes']['outcome']))


    # get current phase
    elif topic == 'observations/events/stage_transition':
        current_phase = data['mission_stage']

    #m4    (post-perturbation period score/post-perturbation period mins)/(points available to score at start of post-perturbation period) / 
    #       (pre-perturbation period score/pre-perturbation period mins)/(points available to score at start of pre-perturbation period)
    elif topic == 'mission/perturbation/start':
        pre_perturbation_score = team_score['teamScore']
        pre_perturbation_period = int(data['mission_timer'].split(':')[0].strip())
        print('perturbation happened: len {} score {}'.format(len(env_list), pre_perturbation_score_available))
        for obj in env_list:
            if obj['type'].startswith('block_bomb'):
                post_perturbation_score_available = post_perturbation_score_available + (len(obj['currAttributes']['sequence']) * 10) # 10 pts per step per object
        print('max post pertubation points: {}'.format(post_perturbation_score_available))

    # track player location
    elif topic == 'player/state':
        # trach each players location add to risk if player is not within 10 blocks of another player
        player_locations[data['participant_id']] = data






def publish_measure(timestamp, elapsed_milli, event_type, message_type, sub_type, measure_id, measure_value, measure_description, datatype, data):

    m_data = {}
    m_data['study_version'] = "4"
    m_data['elapsed_milliseconds'] = elapsed_milli
    m_data['event_properties'] = {}
    m_data['event_properties']['qualifying_event_type'] = event_type
    m_data['event_properties']['qualifying_event_message_type'] = message_type
    m_data['event_properties']['qualifying_event_sub_type'] = sub_type
    
    m_data['measure_data'] = []

    measure_data = {}
    measure_data['measure_id'] = measure_id
    measure_data['datatype'] = datatype
    measure_data['measure_value'] = measure_value
    measure_data['description'] = measure_description
    measure_data['additional_data'] = data

    m_data['measure_data'].append(measure_data)

    helper.send_msg("agent/measures/AC_Aptima_TA3_measures",
                            "agent",
                            "measures",
                            VERSION,
                            timestamp,
                            m_data
                            )
    print(measure_id, message_type, sub_type, 'Value:', measure_value)


# Agent Initialization
helper = ASISTAgentHelper(on_message)

# Set the helper's logging level to INFO
LOG_HANDLER = logging.StreamHandler()
LOG_HANDLER.setFormatter(logging.Formatter("%(asctime)s | %(name)s | %(levelname)s — %(message)s"))
helper.get_logger().setLevel(logging.INFO)
helper.get_logger().addHandler(LOG_HANDLER)
logger = logging.getLogger("MeasuresAgent")
logger.setLevel(logging.INFO)
logger.addHandler(LOG_HANDLER)

# Set the agents status to 'up' and start the agent loop on a separate thread
helper.set_agent_status(helper.STATUS_UP)
logger.info("Starting Agent Loop on a separate thread.")
helper.start_agent_loop_thread()
logger.info("Agent is now running...")
