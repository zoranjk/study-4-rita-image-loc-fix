import paho.mqtt.client as mqtt 
from argparse import ArgumentParser
from os.path import join
import os
import time
import json


# This will take a metadata file and publish each message on the message bus to simulate a trial

client = mqtt.Client()
client.connect("localhost", 1883, 60)
parser = ArgumentParser(description='get stats from trial data')
parser.add_argument('-f', '--input_file', type=str, required=True, help='path to file')
args = parser.parse_args()

if not os.path.exists(args.input_file):
    print('File does not exist')
    
with open(args.input_file, encoding="utf8") as f:
    for line in f:
        message = json.loads(line)

        if 'topic' in message.keys():
            client.publish(message["topic"], line)
            # print("Just published message " + message["topic"])
        # time.sleep(0.01)