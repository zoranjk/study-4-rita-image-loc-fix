# Overview
This section documents metrics and procedure by which TA3 will evaluate ASI and Analytic Components (ACs). Additioanl measures are defined in documentation of AC agents. The metrics and measures described here are designed to advance the science and provide quantitative support for the program's claims that social science measures influence the design of ASI Machine Theory of Teams, which drives interventions, which influence team process, which may influence teamwork effects.

# Measure id: ASI-M1: Mission Score
## Type
- Teamwork Effects Measure
## Measurements (output)
- the total Team Score at the end of a trial.
## Observables (input)
- The scoreboard message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M2: Synchronization
## Type
- Teamwork Effects Measure
## Measurements (output)
- the average time per bomb step for bombs that were successfully diffused.
## Observables (input)
- The object interaction message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M3: Error Rate
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Percentage of bombs that are initiated but not disposed.
## Observables (input)
- The object interaction message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M3.2: Error Rate 2
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Total number of bombs disposed incorrectly (applies wrong tool resulting in explosion). Does NOT include bombs that exploded due to timeout.
## Observables (input)
- The object interaction message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M4: Adaptation / Resilience
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Proportion of pre-perturbation score attained post-perturbation.
## Observables (input)
- The scoreboard and perturbation message son the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M5: Coordinative Comms
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Number of chat messages sent during trial.
## Observables (input)
- The chat message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M8: Count of ASI Interventions
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Total number of messages sent by ASI.
## Observables (input)
- The intervention message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M14: Risk
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Number of bombs initiated alone. (no teammate within 10 blocks)
## Observables (input)
- The object interaction and player location message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M16: Team Engagement in Planning
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Total number of actions during planning phases (store).
## Observables (input)
- The ui/click message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M17: Planning Communication Count
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Total number of chat messages sent between team during planning phases (store). (possible duplicate data with M5)
## Observables (input)
- The chat message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None

# Measure id: ASI-M18: Recon Communication Count
## Type
- Teamwork Effects Measure
## Measurements (output)
- the Total number of beacons and markers placed during recon phase.
## Observables (input)
- The environment created message on the message bus.
## Measurement procedure & frequency
- TA3 takes this measure once at the end of a trial
### Bus message format
- Testbed measurement message schema
## How to generate assessments from measures 
- There is no criterion for measurements to support assessment 
## Potential applications of the measurements 
- This measure is designed to support evaluation, not ASI MToT modeling or intervention
## Prior validation of the measure 
- None
	
# Agent Use (configuration, running, etc.) 
- This agent runs automatically with the testbed. There are no configuration parameters.
