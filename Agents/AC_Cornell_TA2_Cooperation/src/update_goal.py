# This script contains functions related to calculating the goal alignment metrics of the team


import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

from utils_ac import *





def find_player_goal(topic, data,ac_data,logger):
    """"
    Player goals via a list for each player that indicates per second goals.
    Possible goals are as follows:
        Recon : indicates that the team is in RECON_STAGE
        Team : The player prioritizes collaboration by making a request to another player 
            (despite having the tools defuse the bomb sequence on their own)
            or responding to another player's request
        Ind : The player prioritizes defusing bombs on their own. 
            This goal indicates the player chooses to defuse multi-step bombs on their own
        Hazard : The player is concerned with warning or extinguishing hazard, and or dealing with frozen players
        Shop: The player has voted to go to the shop
        * Note: Defusing a one-sequence bomb will not change the player's last goal.
        * Goals can be updated retroactively, but will be published when we detect a change.
    """
    new_time = mission_timer_to_sec(data['mission_timer'])
    
    # Stage transitions -> goal update (Shop, Team, Ind)
    if topic == 'observations/events/stage_transition':
        if data["mission_stage"] == "SHOP_STAGE":
            for pl in ac_data['player_enum']:
                ac_data['player_last_goal_update_sec'][pl] = new_time
                pad_list_to_n(ac_data['player_goal'][pl],new_time+1,'Shop')
            ac_data['ac_summary']['curr_stage']['end_time'] = mission_timer_to_sec(data['mission_timer'])
        if data["mission_stage"] == "FIELD_STAGE":
            for pl in ac_data['player_enum']:
                #TODO : inventory update comes after stage transition 
                try:
                    inf_goal = 'Ind' if ac_data['player_currInv'][pl]['BOMB_DISPOSER']>0 else 'Team'
                except:
                    inf_goal = 'Team'
                pad_list_to_n(ac_data['player_goal'][pl],new_time+2,inf_goal)
                ac_data['player_last_goal_update_sec'][pl] = new_time+1
            ac_data['ac_summary']['curr_stage']['start_time'] = mission_timer_to_sec(data['mission_timer'])

    # Vote to go to shop -> goal update (Shop)
    elif topic == 'communication/environment':  
        if 'voted' in data['message']:
        # this is a shop goal update
            player = data['message'].split()[0]
            ac_data['player_goal'][player][new_time] = 'Shop'
            ac_data['player_last_goal_update_sec'][player] = new_time
        
    elif topic == 'item/state/change':
        player = get_player_callsign(ac_data, data['owner'])
        last_sec = ac_data['player_last_goal_update_sec'][player]
        # Team if you have tools but requested help
        if(data['item_name'] == 'BOMB_BEACON' or \
            (data['item_name'] == 'block_beacon_hazard' and \
             'Wirecutter' in data['currAttributes']['message'])):
            #update goal
            ac_data['player_goal'][player][last_sec+1:new_time+1] =['Team']*(new_time-last_sec)

        # Hazard if player frozen, or help with fire
        elif data['item_name'] == 'block_beacon_hazard': 
            if 'help' in data['currAttributes']['message']:
                ac_data['player_goal'][player][new_time] = 'Hazard'
            if 'Help' in data['currAttributes']['message']:
                ac_data['player_goal'][player][new_time] = 'Hazard'
                
        elif data['item_name'] == 'FIRE_EXTINGUISHER':
            ac_data['player_goal'][player][last_sec+1:new_time+1] =['Hazard']*(new_time-last_sec)
        
        ac_data['player_last_goal_update_sec'][player] = new_time
    # Ind if you defuse multi-sequence bombs yourself instead of requesting help
    # Ind if you respond to your own request
    # Team if you respond to an existing request from someone else
    elif topic == 'object/state/change':
        bomb_id = data['id']
        pl = get_player_callsign(ac_data, data['triggering_entity'])
        player_sequences = ac_data['bomb_state'][bomb_id]['player_sequence']
        mission_time =  mission_timer_to_sec(data['mission_timer'])
        last_sec = ac_data['player_last_goal_update_sec'][pl]
        if data['id'] in ac_data['open_requests']:
            # requester and responder are the same, responded to own request
            if data['currAttributes']['outcome'] == 'TRIGGERED_ADVANCE_SEQ':
                res_tool = data['changedAttributes']['sequence'][0][0]
                if ac_data['open_requests'][data['id']]['requester'] != ac_data['open_requests'][data['id']]['responder']:
                    ac_data['player_goal'][pl][last_sec+1:new_time+1] =['Team']*(new_time-last_sec)
                elif (ac_data['open_requests'][data['id']]['requested_tool']=='') or (res_tool in ac_data['open_requests'][data['id']]['requested_tool']):
                    ac_data['player_goal'][pl][last_sec+1:new_time+1] =['Ind']*(new_time-last_sec)
        else:
            if data['currAttributes']['outcome']  == 'TRIGGERED_ADVANCE_SEQ':
                # no requests, just defusing bombs on one's own
                if len(player_sequences)>1 and len(set(player_sequences))==1:
                    #find solo
                    ac_data['player_goal'][pl][last_sec+1:new_time+1] =['Ind']*(new_time-last_sec)
            elif data['currAttributes']['outcome'] == 'DEFUSED_DISPOSER':
                ac_data['player_goal'][pl][last_sec+1:new_time+1] =['Ind']*(new_time-last_sec)
    
        ac_data['player_last_goal_update_sec'][pl]=new_time

    # Team if you unfreeze a player
    elif topic == 'player/state/change':
        #update goal
        requester = get_player_callsign(ac_data, data['source_id'])
        mission_time =  mission_timer_to_sec(data['mission_timer'])
        last_sec = ac_data['player_last_goal_update_sec'][requester]
        ac_data['player_goal'][requester][last_sec+1:mission_time+1] =['Team']*(mission_time-last_sec)
        ac_data['player_last_goal_update_sec'][requester]=mission_time



    ac_message ={
        pl+'_goal': ac_data['player_goal'][pl][-1]
                    for pl in ac_data['player_enum']
    }
    return ac_message, ac_data


def compute_goal_alignment(topic, data, ac_data,logger):
    """
    using player goals in ac_data, 
    this function computes how aligned each dyad's goals are
    and how aligned the whole team is.
    the metric is to be published at the end of each stage.
    """
    if len(ac_data['player_goal'])!=3:
        return {"Err" : "Cannot compute goal alignment, we need 3 players for this."}, ac_data

    alpha_goal = ac_data['player_goal']['Alpha']
    bravo_goal = ac_data['player_goal']['Bravo']
    delta_goal = ac_data['player_goal']['Delta']

    start = ac_data['ac_summary']['curr_stage']['start_time']
    alpha_bravo = [(alpha_goal[i]==bravo_goal[i]) for i in range(start,len(alpha_goal))]
    bravo_delta = [(delta_goal[i]==bravo_goal[i]) for i in range(start,len(delta_goal))]
    alpha_delta = [(alpha_goal[i]==delta_goal[i]) for i in range(start,len(alpha_goal))]
    alpha_bravo_delta = [(alpha_goal[i]==bravo_goal[i]==delta_goal[i]) for i in range(start,len(alpha_goal))]
    ac_message = {
        'stage_start' : start,
        'stage_end' : len(alpha_goal),
        'alignment_alpha_bravo_stage' : sum(alpha_bravo)/len(alpha_bravo),
        'alignment_bravo_delta_stage' : sum(bravo_delta)/len(bravo_delta),
        'alignment_alpha_delta_stage' : sum(alpha_delta)/len(alpha_delta),
        'alignment_alpha_bravo_delta_stage' : sum(alpha_bravo_delta)/len(alpha_bravo_delta)
    }

    if topic == 'trial':
        alpha_bravo = [(alpha_goal[i]==bravo_goal[i]) for i in range(len(alpha_goal))]
        bravo_delta = [(delta_goal[i]==bravo_goal[i]) for i in range(len(bravo_goal))]
        alpha_delta = [(alpha_goal[i]==delta_goal[i]) for i in range(len(alpha_goal))]
        alpha_bravo_delta = [(alpha_goal[i]==bravo_goal[i]==delta_goal[i]) for i in range(len(alpha_goal))]

        ac_message.update({
            'alignment_alpha_bravo_trial' : sum(alpha_bravo)/len(alpha_bravo),
            'alignment_bravo_delta_trial' : sum(bravo_delta)/len(bravo_delta),
            'alignment_alpha_delta_trial' : sum(alpha_delta)/len(alpha_delta),
            'alignment_alpha_bravo_delta_trial' : sum(alpha_bravo_delta)/len(alpha_bravo_delta)})
        
    return ac_message, ac_data


