import logging
from collections import defaultdict
import math


def get_nearest_bomb_id(data,ac_data):
    closest= None
    x,y,z = data['sender_x'], data['sender_y'], data['sender_z']
    dists = []
    for k,v in ac_data['bomb_state'].items():
        bx, by, bz = v['x'],v['y'],v['z']
        dists.append((k,math.dist([x,y,z],[bx,by,bz])))
        # Check if requesting sequence is for the right bomb
    
    closest = sorted(dists,key=lambda x: x[1])[0]

    return closest[0], closest[1]

def initialize_ac():

    # set config parameters 
    # config = cfg.params    
    player_enum = []
    dyad_compliance = defaultdict(int)
    dyad_alignment = defaultdict(list)
    player_info = defaultdict(dict)
    player_goal = defaultdict(list)
    player_last_goal_update_sec = defaultdict(int)
    player_currInv = defaultdict(dict)
    player_last_visited_bomb = defaultdict(str)
    bomb_state = defaultdict(dict)
    open_requests = defaultdict(dict)
    closed_requests = defaultdict(list)
    beacon_state = defaultdict(dict)
    ac_summary = defaultdict(dict)
    mission_timer = ''
    curr_stage = ''
    ac_data={
        'player_enum' : player_enum,
        'bomb_state': bomb_state,
        'beacon_state' : beacon_state,
        'open_requests': open_requests,
        'closed_requests': closed_requests,
        'dyad_compliance' : dyad_compliance,
        'dyad_alignment': dyad_alignment,
        'ac_summary' : ac_summary,
        'player_info' : player_info,
        'player_goal' : player_goal,
        'player_last_goal_update_sec': player_last_goal_update_sec,
        'player_last_visited_bomb' : player_last_visited_bomb,
        'player_currInv': player_currInv,
        'curr_stage' : curr_stage,
        'mission_timer' : mission_timer
    }    
    return ac_data

def compile_player_info(ac_data, data):
    """
    Returns the player information and aliases.
    """
    
    for client in data['client_info']:
        # Map playername, participant_id, etc., to player role
        player_role = client['callsign']
        for key in ('playername', 'participant_id', 'participantid', 'uniqueid'):
            player_alias = client.get(key)
            if player_alias:
                ac_data['player_info'][player_alias] = player_role
                ac_data['player_enum'].append(player_role)
    return ac_data

def get_player_role(ac_data, data):
    """
    Returns the player role.
    """
    
    if data['participant_id']:
        player_id = data['participant_id']
    elif data['playername']:
        player_id = data['playername']
    else:
        assert False, 'Invalid player'
    
    return ac_data['player_info'][player_id]
    
def get_player_callsign(ac_data,player):
    return ac_data['player_info'][player]

def flatten_dict(dd, separator ='_', prefix =''):
    
    return { prefix + separator + str(k) if prefix else k : v
             for kk, vv in dd.items()
             for k, v in flatten_dict(vv, separator, kk).items()
             } if isinstance(dd, dict) else { prefix : dd }
             

def pad_list_to_n(l, n, t=None):
    if len(l) < n:
        if t is not None:
            l.extend([t] * (n - len(l)))
        else:
            l.extend([l[-1]] * (n - len(l)))
    elif t is not None:
        l[-1] = t
    return l
    

def mission_timer_to_sec(mission_timer):
    min, sec = mission_timer.split(":")
    return int(min.strip()) * 60 + int(sec.strip())  

def setup_logger(logfilename):

    LOG_LEVEL = logging.INFO
    HELPER_LOG_LEVEL = logging.WARN

    LOG_HANDLER = logging.StreamHandler()
    LOG_HANDLER.setFormatter(logging.Formatter("%(asctime)s | %(name)s | %(levelname)s — %(message)s"))

    logging.basicConfig(filename=logfilename, 
                        format='%(asctime)s %(message)s', 
                        filemode='w') 

    logger = logging.getLogger("CU_TEAM_AC")
    logger.setLevel(LOG_LEVEL)
    logger.addHandler(LOG_HANDLER)
    
    return logger
    