# AC_Cornell_TA2_Cooperation
Docker container source code for Cornell's AC component: AC_Cornell_TA2_Cooperation.


## Run the Docker container
-  `./agent.sh upd`


## Stop the Docker container
- `./agent.sh down`


## Source code

`src/` folder contains AC source code.

* `main_offline_ac.py` - the main script that reads the metadata files and runs Cornell TA2 AC in the offline mode.

* `ac_cornell_cooperation.py` - script to run the AC and calculate AC measures at each subscribed message.

* `update_reqres.py` - calculates measures related to team compliance.

* `update_goal.py` - calculates measures related to team goal alignment.

* `utils_ac.py` - contains util functions for the AC

* `utils_offline.py` - contains util functions that make the AC work offline

`ConfigFolder/` contains files needed for configuring AC

* `config.json` contains all subscribed and published topics, version info etc
