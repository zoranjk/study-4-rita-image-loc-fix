# -*- coding: utf-8 -*-
"""
.. module:: communication_environment
   :platform: Linux, Windows, OSX
   :synopsis: Parser for Item Used messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a parser for CommunicationEnvironment messages
"""

from ....messages import CommunicationEnvironment
from ..message_types import MessageType, MessageSubtype
from ..bus_header import BusHeaderParser
from ..message_header import MessageHeaderParser
from ..parser_exceptions import MissingHeaderException

import MinecraftElements

import logging


class CommunicationEnvironmentParser:
    """
    A class for parsing item used messages from MQTT bus.

    MQTT Message Fields
    -------------------


    MQTT Blockage Fields
    --------------------

    """

    topic = "communication/environment"
    MessageClass = CommunicationEnvironment

    msg_type = MessageType.simulator_event
    msg_subtype = MessageSubtype.Event_CommunicationEnvironment
    alternatives = []

    # Class-level logger
    logger = logging.getLogger("CommunicationEnvironmentParser")


    @classmethod
    def parse(cls, json_message):
        """
        Convert the a Python dictionary format of the message to a BlockageList
        instance.

        Arguments
        ---------
        json_message : dictionary
            Dictionary representation of the message received from the MQTT bus
        """

        # Make sure that there's a "header" and "msg" field in the message
        if not "header" in json_message.keys() or not "msg" in json_message.keys():
            raise MissingHeaderException(json_message)

        # Parse the header and message header
        busHeader = BusHeaderParser.parse(json_message["header"])
        messageHeader = MessageHeaderParser.parse(json_message["msg"])

        # Check to see if this parser can handle this message type, if not, 
        # then return None
        if busHeader.message_type != MessageType.simulator_event:
            return None
        if messageHeader.sub_type != MessageSubtype.Event_CommunicationEnvironment:
            return None

        # Parse the data
        data = json_message["data"]

        message = CommunicationEnvironment(**data)
        message.addHeader("header", busHeader)
        message.addHeader("msg", messageHeader)

        return message