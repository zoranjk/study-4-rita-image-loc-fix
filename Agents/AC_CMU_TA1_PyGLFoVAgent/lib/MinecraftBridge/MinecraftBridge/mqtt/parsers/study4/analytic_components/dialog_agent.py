# -*- coding: utf-8 -*-
"""
.. module:: dialog_agent
   :platform: Linux, Windows, OSX
   :synopsis: Parser for Dialog Agent messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a parser for Study 4 Dialog Agent messages
"""

from .....messages import DialogAgent
from ...message_types import MessageType, MessageSubtype
from ...bus_header import BusHeaderParser
from ...message_header import MessageHeaderParser
from ...parser_exceptions import MissingHeaderException

import MinecraftElements

import logging


class DialogAgentParser:
    """
    A class for parsing Dialog Agent messages from MQTT bus.

    MQTT Message Fields
    -------------------


    MQTT Blockage Fields
    --------------------

    """

    topic = "agent/AC_UAZ_TA1_DialogAgent"
    MessageClass = DialogAgent

    msg_type = MessageType.agent
    msg_subtype = MessageSubtype.nlu
    alternatives = []

    # Class-level logger
    logger = logging.getLogger("DialogAgentParser")


    @classmethod
    def parse(cls, json_message):
        """
        Convert the a Python dictionary format of the message to a BlockageList
        instance.

        Arguments
        ---------
        json_message : dictionary
            Dictionary representation of the message received from the MQTT bus
        """

        # Make sure that there's a "header" and "msg" field in the message
        if not "header" in json_message.keys() or not "msg" in json_message.keys():
            raise MissingHeaderException(json_message)

        # Parse the header and message header
        busHeader = BusHeaderParser.parse(json_message["header"])
        messageHeader = MessageHeaderParser.parse(json_message["msg"])

        # Check to see if this parser can handle this message type, if not, 
        # then return None
        if busHeader.message_type != MessageType.agent:
            return None
        if messageHeader.sub_type != MessageSubtype.nlu:
            return None

        # Parse the data
        data = json_message["data"]

        message = DialogAgent(text=data.get('text', ''),
                              corrected_text=data.get('corrected_text', ''),
                              participant_id=data.get('participant_id', '<UNKNOWN>'),
                              utterance_id=data.get('utterance_id', '<UNKNOWN>'))

        compact_extractions = data['compact_extractions']
        if type(compact_extractions) is not list:
            compact_extractions = [compact_extractions]

        for extraction in data['extractions']:
            message.add_extraction(extraction)
        for extraction in compact_extractions:
            if type(extraction) is str:
                extraction = {'label': extraction, 'arguments': {}}
            message.add_compact_extraction(extraction)


        message.addHeader("header", busHeader)
        message.addHeader("msg", messageHeader)

        return message