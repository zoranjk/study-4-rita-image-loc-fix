# -*- coding: utf-8 -*-
"""
.. module:: chat_analysis
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating output messages generated by the UAZ
              Dialog Agent

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating chat analysis messages.
"""


import json

from .message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from .base_message import BaseMessage


class ChatAnalysisExtraction:
    """
    A class defining an extraction from the Dialog Agent.  Extractions can be
    recursive, with recursive elements mapped in the `arguments` attribute.

    Attributes
    ----------
    span : string
        Substring of the message text corresponding to the extraction
    labels : list of strings
        All labels applied to the span, from specific to general
    arguments : map
        Mapping of rule arguments (as strings) to extractions
    start_offset : int
        Start index of the span in the message text
    end_offset : int
        End index of the span in the message text
    """

    def __init__(self, **kwargs):
        """
        """

        # Raise an exception if a keyword argument isn't provided
        for arg_name in ["span", "start_offset", "end_offset"] in kwargs:
            if not argname in kwargs:
                raise MissingMessageArgumentException(str(self), arg_name) from None

        self._span = kwargs["span"]
        self._start_offset = kwargs["start_offset"]
        self._end_offset = kwargs["end_offset"]

        # labels and arguments may have been provided
        self._labels = kwargs.get("labels", [])
        self._arguments = kwargs.get("arguments", {})

        # Indicate if the extraction has been fully formed
        self._finalized = False


    def __str__(self):
        """
        String representation of the exraction.
        """

        return self.__class__.__name__


    def addLabel(self, label):
        """
        Add a label to the extraction.

        Arguments
        ---------
        label : string
            label to add to the extraction
        """

        if self._finalized:
            raise ImmutableAttributeException(str(self), "labels (addLabel)") from None

        self._labels.append(label)


    def addArgument(self, argument_name, extraction):
        """
        Add a named argument to the extraction

        Arguments
        ---------
        argument_name : string
            Name of the argument
        extraction : ChatAnalysisExtraction
            Value of the argument
        """

        if self._finalized:
            raise ImmutableAttributeException(str(self), "arguments (addArgument)") from None

        self._arguments[argument_name] = extraction


    def finalize(self):
        """
        Indicate that the message has been completely created
        """

        self._finalized = True


    @property
    def span(self):
        """
    
        Attempting to set `span` raises an `ImmutableAttributeException`.
        """
    
    @span.setter
    def span(self, _):
        raise ImmutableAttributeException(str(self), "span") from None
    
    
    @property
    def start_offset(self):
        """
    
        Attempting to set `start_offset` raises an `ImmutableAttributeException`.
        """
    
    @start_offset.setter
    def start_offset(self, _):
        raise ImmutableAttributeException(str(self), "start_offset") from None
    
    
    @property
    def end_offset(self):
        """
    
        Attempting to set `end_offset` raises an `ImmutableAttributeException`.
        """
    
    @end_offset.setter
    def end_offset(self, _):
        raise ImmutableAttributeException(str(self), "end_offset") from None
    
    
    @property
    def labels(self):
        """
    
        Attempting to set `labels` raises an `ImmutableAttributeException`.
        """
    
    @labels.setter
    def labels(self, _):
        raise ImmutableAttributeException(str(self), "labels") from None
    
    
    @property
    def arguments(self):
        """
    
        Attempting to set `arguments` raises an `ImmutableAttributeException`.
        """
    
    @arguments.setter
    def arguments(self, _):
        raise ImmutableAttributeException(str(self), "arguments") from None
    

    def toDict(self):
        """
        Convert the extraction to a dictionary
        """

        data = { "span": self.span,
                 "start_offset": self.start_offset,
                 "end_offset": self.end_offset,
                 "labels": self.labels,
                 "arguments": { }
                }

        # Add the arguments, converted to dictionaries
        for argument_name, extraction in self.arguments.items():
            data["arguments"][argument_name] = extraction.toDict()

        return data



class ChatAnalysis(BaseMessage):
    """
    A class encapsulating chat analysis messages generated by the UAZ Dialog
    Agent.

    Attributes
    ----------
    participant_id : string
        Participant who generated the message text
    asr_message_id : string
        UUID associated with the message
    text : string
        Text processed by the dialog agent
    utterance_source_type : string
        "file" or "message_bus"
    utterance_source_name : string
        filename or message bus topic
    extractions : list of ChatAnalysisExtraction instances
        analysis of message text
    """

    def __init__(self, **kwargs):
        """

        """

        BaseMessage.__init__(self, **kwargs)

        # Check for needed arguments
        for arg_name in ["participant_id", "asr_message_id", "text", "utterance_source"]:
            if not arg_name in kwargs:
                raise MissingMessageArgumentException(str(self), arg_name) from None

        # Store the keyword arguments
        self._participant_id = kwargs["participant_id"]
        self._asr_message_id = kwargs["asr_message_id"]
        self._text = kwargs["text"]

        # Get the source of the utterance, first checking that it exists
        if not "source_type" in kwargs["utterance_source"]:
            raise MissingMessageArgumentException(str(self), "utterance_source.source_type") from None
        if not "source_name" in kwargs["utterance_source"]:
            raise MissingMessageArgumentException(str(self), "utterance_source.source_name") from None

        self._utterance_source_type = kwargs["utterance_source"]["source_type"]
        self._utterance_source_name = kwargs["utterance_source"]["source_name"]

        self._extractions = kwargs.get("extractions", [])

        # Indicate when the message has be completed
        self._finalized = False


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'ChatAnalysis')
        """

        return self.__class__.__name__


    def addExtraction(self, extraction):
        """
        Add an extraction to the sequence of extractions

        Argument
        --------
        extraction : ChatAnalysisExtraction
            Extraction from the text
        """

        if self._finalized:
            raise ImmutableAttributeException(str(self), "extractions (addExtraction)") from None

        self._extractions.append(extraction)


    def finalize(self):
        """
        Indicate that the message has been completely created
        """

        self._finalized = True

    @property
    def participant_id(self):
        """
    
    
        Attempting to set `participant_id` raises an `ImmutableAttributeException`.
        """
    
        return self._participant_id
    
    @participant_id.setter
    def participant_id(self, _):
        raise ImmutableAttributeException(str(self), "participant_id") from None
    

    @property
    def asr_message_id(self):
        """
    
    
        Attempting to set `asr_message_id` raises an `ImmutableAttributeException`.
        """
    
        return self._asr_message_id
    
    @asr_message_id.setter
    def asr_message_id(self, _):
        raise ImmutableAttributeException(str(self), "asr_message_id") from None
    

    @property
    def text(self):
        """
    
    
        Attempting to set `text` raises an `ImmutableAttributeException`.
        """
    
        return self._text
    
    @text.setter
    def text(self, _):
        raise ImmutableAttributeException(str(self), "text") from None
    

    @property
    def utterance_source_type(self):
        """
    
    
        Attempting to set `utterance_source_type` raises an `ImmutableAttributeException`.
        """
    
        return self._utterance_source_type
    
    @utterance_source_type.setter
    def utterance_source_type(self, _):
        raise ImmutableAttributeException(str(self), "utterance_source_type") from None
    

    @property
    def utterance_source_name(self):
        """
    
    
        Attempting to set `utterance_source_name` raises an `ImmutableAttributeException`.
        """
    
        return self._utterance_source_name
    
    @utterance_source_name.setter
    def utterance_source_name(self, _):
        raise ImmutableAttributeException(str(self), "utterance_source_name") from None
    

    @property
    def extractions(self):
        """
    
    
        Attempting to set `extractions` raises an `ImmutableAttributeException`.
        """
    
        return self._extractions
    
    @extractions.setter
    def extractions(self, _):
        raise ImmutableAttributeException(str(self), "extractions") from None


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ChatAnalysis.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["participant_id"] = self.participant_id
        jsonDict["data"]["asr_message_id"] = self.asr_message_id
        jsonDict["data"]["text"] = self.text
        jsonDict["data"]["utterance_source"] = { "source_type": self.utterance_source_type,
                                                 "source_name": self.utterance_source_name
                                               }
        jsonDict["data"]["extractions"] = [ extraction.toDict() for extraction in self.extractions ]

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ChatAnalysis message.
        """

        return json.dumps(self.toDict())
