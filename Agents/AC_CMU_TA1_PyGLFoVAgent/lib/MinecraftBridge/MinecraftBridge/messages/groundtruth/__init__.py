# -*- coding: utf-8 -*-
"""
.. module:: messages.groundtruth
   :platform: Linux, Windows, OSX
   :synopsis: Module defining ground truth message classes.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of message classes used by MinecraftBridge.  Classes in this module
are related to ground truth messages generated by the testbed.
"""