# -*- coding: utf-8 -*-
"""
.. module:: intervention_response
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating InterventionResponse messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating InterventionResponse messages.
"""


import json

import ciso8601

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

class InterventionResponse(BaseMessage):
    """
    A class encapsulating InterventionResponse messages.

    Attributes
    ----------
    intervention_id : string
        Unique ID for the intervention
    sender_id : string
        Participant ID of the responding player
    agent_id : string
        Unique ID of the agent that sent the intervention
    response_index : int
        Integer index of the option selected
    """

    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._intervention_id = kwargs.get("intervention_id", "<UNKNOWN>")
        self._sender_id = kwargs.get("participant_id", kwargs.get("sender_id", "<UNKNOWN>"))
        self._agent_id = kwargs.get("agent_id", "<UNKNOWN>")
        self._response_index = kwargs.get("response_index", -1)


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message
        """

        return self.__class__.__name__


    @property
    def intervention_id(self):
        return self._intervention_id

    @property
    def sender_id(self):
        return self._sender_id

    @property
    def participant_id(self):
        return self._sender_id

    @property
    def agent_id(self):
        return self._agent_id

    @property
    def response_index(self):
        return self._response_index
    
    
    
    
    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the InterventionResponse message.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["intervention_id"] = self.intervention_id
        jsonDict["data"]["participant_id"] = self.participant_id
        jsonDict["data"]["agent_id"] = self.agent_id
        jsonDict["data"]["response_index"] = self.response_index

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            InterventionResponse message.
        """

        return json.dumps(self.toDict())
