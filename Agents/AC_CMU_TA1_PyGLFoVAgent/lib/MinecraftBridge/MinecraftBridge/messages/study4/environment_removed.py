# -*- coding: utf-8 -*-
"""
.. module:: environment_created
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating environment removed messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Environment Removed Single and 
Environment Removed List messages.
"""

import json

from .environment_created import EnvironmentObject

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage


class EnvironmentRemovedSingle(BaseMessage):
    """
    A class encapsulating EnvironmentRemovedSingle messages.

    Attributes
    ----------
    mission_timer : 
        Current mission timer
    elapsed_milliseconds : int
        Number of milliseconds since mission start
    object : EnvironmentObject
        Created environment block
    """


    def __init__(self, **kwargs):
        """
        """
        

        BaseMessage.__init__(self, **kwargs)

        self._object = kwargs.get('object', kwargs.get('obj', None))


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'EnvironmentCreatedSingle')
        """

        return self.__class__.__name__


    @property
    def object(self):
        return self._object


    def toDict(self):
        """
        Generates a dictionary representation of the DoorEvent message.  
        DoorEvent information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the DoorEvent.
        """

        jsonDict = BaseMessage.toDict(self)
    
        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

#        jsonDict['data']['triggering_entity'] = self.triggering_entity
        jsonDict['data']['obj'] = self.object.toDict()

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the DoorEvent message.  DoorEvent
        information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            DoorEvent message.
        """

        return json.dumps(self.toDict())




class EnvironmentRemovedList(BaseMessage):
    """
    A class encapsulating EnvironmentCreatedList messages.

    Attributes
    ----------
    mission_timer : 
        Current mission timer
    elapsed_milliseconds : int
        Number of milliseconds since mission start
    list : List[EnvironmentObject]
        List of created environment blocks
    """


    def __init__(self, **kwargs):
        """
        """
        

        BaseMessage.__init__(self, **kwargs)

        self.__environment_objects = []

        self._triggering_entity = kwargs.get("triggering_entity", None)


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'DoorEvent')
        """

        return self.__class__.__name__


    @property
    def triggering_entity(self):
        return self._triggering_entity
    
    @property
    def objects(self):
        return self.__environment_objects


    @property
    def list(self):
        return self.objects
    

    def add_object(self, object_):
        """
        """

        self.__environment_objects.append(object_)


    def finalize(self):
        """
        """

        self.__environment_objects = tuple(self.__environment_objects)


    def toDict(self):
        """
        Generates a dictionary representation of the DoorEvent message.  
        DoorEvent information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the DoorEvent.
        """

        jsonDict = BaseMessage.toDict(self)
    
        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        jsonDict['data']['triggering_entity'] = self.triggering_entity
        jsonDict['data']['list'] = [object_.toDict() for object_ in self.objects]

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the DoorEvent message.  DoorEvent
        information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            DoorEvent message.
        """

        return json.dumps(self.toDict())
