# -*- coding: utf-8 -*-
"""
.. module:: communication_chat
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Chat Communication messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating communication messages from players.
"""

import json

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

class CommunicationChat(BaseMessage):
    """
    A class encapsulating Chat Communication event messages.

    Attributes
    ----------
    sender_id : string
        The id of the object sending the message
    recipients : tuple of strings
        The list of ids of the reciptients of the message
    message : string
        The raw form of the message
    environment : string
        A string representing the environment the chat was performed in
    additional_info : dict
        Additional information that may be used when processing the message
    """

    def __init__(self, **kwargs):
        """
        Keyword Arguments
        -----------------
        sender_id : string
            The id of the object of the sender
        recipients : tuple of strings
            The list of recipient ids
        message : string
            The raw text of the message
        environment : string
            String representation of the environment
        additional_info : dict
            Additional information that may be used 
        """

        BaseMessage.__init__(self, **kwargs)

        # Check to see if the necessary arguments have been passed, raise an 
        # exception if one is missing
        for arg_name in ['sender_id', 'recipients', 'message', 'environment']:
            if not arg_name in kwargs:
                raise MissingMessageArgumentException(str(self), 
                                                    arg_name) from None

        self._sender_id = kwargs['sender_id']

        # Cast the addressees as a tuple, so that client code cannot modify the
        # list of attendees.  Also, make sure that the addressees isn't passed
        # as a string
        if isinstance(kwargs['recipients'], str):
            raise MalformedMessageCreationException(str(self), 'recipients',
                                                    kwargs['recipients']) from None
        try:
            self._recipients = tuple(kwargs['recipients'])
        except:
            raise MalformedMessageCreationException(str(self), 'recipients',
                                                    kwargs['recipients']) from None

        self._message = kwargs['message']
        self._environment = kwargs['environment']

        self._additional_info = kwargs.get('additional_info', {})


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'ChatEvent')        
        """

        return self.__class__.__name__


    @property
    def sender_id(self):
        """
        Get the id of the object that sent the message.  Attempting to set the
        value of `sender_id` will result in an `ImmutableAttributeException`
        being raised.
        """
        return self._sender_id

    @sender_id.setter
    def sender_id(self, sender):
        raise ImmutableAttributeException(str(self), "sender_id") from None


    @property
    def recipients(self):
        """
        Get the list of recipients of the message.  Attempting to set the 
        value of`recipients` will result in an `ImmutableAttributeException`
        being raised.
        """
        return self._recipients

    @recipients.setter
    def recipients(self, addressees):
        raise ImmutableAttributeException(str(self), "recipients") from None
    

    @property
    def message(self):
        """
        Get the raw text of the message.  Attempting to set the value of 
        `message` will result in an `ImmutableAttributeException` being raised.
        """
        return self._message

    @message.setter
    def message(self, _):
        raise ImmutableAttributeException(str(self), "text") from None


    @property
    def environment(self):
        return self._environment

    @environment.setter
    def environment(self, _):
        raise ImmutableAttributeException(str(self), "environment") from None
    

    @property
    def additional_info(self):
        return self._additional_info
    

    def toDict(self):
        """
        Generates a dictionary representation of the CommunicationEnvironment message.  
        CommunicationEnvironment information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the CommunicationEnvironment
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the beep event data
        jsonDict["data"]["sender_id"] = self.sender_id
        jsonDict["data"]["recipients"] = self.recipients
        jsonDict["data"]["message"] = self.message
        jsonDict["data"]["environment"] = self.environment
        jsonDict["data"]["additional_info"] = self.additional_info

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the ChatEvent message.  ChatEvent
        information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            BeepEvent message.
        """

        return json.dumps(self.toDict())
