# -*- coding: utf-8 -*-
"""
.. module:: environment_created
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating environment creation messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Environment Created Single and 
Environment Created List messages.
"""

import json

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage


class EnvironmentObject:
    """
    Attributes
    ----------
    id : string
        Unique identifier of the object
    x : int
        x location of the object
    y : int
        y location of the object
    z : int
        z location of the object
    type : MinecraftElements.Block
        type of block created
    mission_timer : tuple[int]
        Mission timer
    elapsed_milliseconds : int
        time when object was created (?)
    triggering_entity : string, optional, default=None
        Entity that created the object
    currAttributes : dictionary
        Attributes of the object
    """

    def __init__(self, id_, location, type_, elapsed_milliseconds, **kwargs):
        """
        Create a new CreatedEnvironmentObject instance
        """

        self._id = id_
        self._location = location
        self._type = type_
        self._elapsed_milliseconds = elapsed_milliseconds
        self._triggering_entity = kwargs.get("triggering_entity", None)
        self._mission_timer = kwargs.get("mission_timer", (-1,-1))
        self._attributes = kwargs.get("attributes", {})


    @property
    def mission_timer(self):
        return self._mission_timer

    @property
    def elapsed_milliseconds(self):
        return self._elapsed_milliseconds
     
    @property
    def id(self):
        """
        Identifier of the environment object
        """
        return self._id

    @property
    def type(self):
        return self._type

    @property
    def triggering_entity(self):
        return self._triggering_entity

    @property
    def location(self):
        """
        (x,y,z) location of the object
        """
        return self._location
    
    @property
    def x(self):
        """
        x-location of the object
        """
        return self._location[0]

    @property
    def y(self):
        """
        y-location of the object
        """
        return self._location[1]

    @property
    def z(self):
        """
        z-location of the object
        """
        return self._location[2]


    def get_attribute_names(self):
        """
        Return a list of the current attributes of the object
        """

        attributes = []

        try:
            attributes = list(self._attributes.keys())
        except:
            print(f'{self}: something wierd here:  {self._attributes}')

        return attributes


    def get_attribute(self, name):
        """
        Return the attribute with the given name
        """

        if not name in self._attributes:
            # TODO:  Raise a warning
            return None

        return self._attributes[name]


    def toDict(self):
        """
        Generates a dictionary representation of the Object

        Returns
        -------
        dict
            A dictionary representation of the Environment Object.
        """

        objectDict = dict()

        objectDict['id'] = self.id
        objectDict['x'] = self.x
        objectDict['y'] = self.y
        objectDict['z'] = self.z
        objectDict['type'] = self.type
        objectDict['mission_timer'] = self.mission_timer
        objectDict['elapsed_milliseconds'] = self.elapsed_milliseconds
        objectDict['currAttributes'] = dict()
        for name, value in self._attributes.items():
            objectDict['currAttributes'][name] = value

        if self.triggering_entity is not None:
            objectDict['triggering_entity'] = self.triggering_entity

        return objectDict


class EnvironmentCreatedSingle(BaseMessage):
    """
    A class encapsulating EnvironmentCreatedSingle messages.

    Attributes
    ----------
    mission_timer : 
        Current mission timer
    elapsed_milliseconds : int
        Number of milliseconds since mission start
    object : EnvironmentObject
        Created environment block
    triggering_entity : string
        Entity that created this environment
    """


    def __init__(self, **kwargs):
        """
        """
        

        BaseMessage.__init__(self, **kwargs)

        self._object = kwargs["object"]
        self._triggering_entity = kwargs.get("triggering_entity", None)


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'EnvironmentCreatedSingle')
        """

        return self.__class__.__name__


    @property
    def object(self):
        return self._object

    @property
    def triggering_entity(self):
        return self._triggering_entity
    


    def toDict(self):
        """
        Generates a dictionary representation of the DoorEvent message.  
        DoorEvent information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the DoorEvent.
        """

        jsonDict = BaseMessage.toDict(self)
    
        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        jsonDict['data']['triggering_entity'] = self.triggering_entity
        jsonDict['data']['obj'] = self.object.toDict()

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the DoorEvent message.  DoorEvent
        information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            DoorEvent message.
        """

        return json.dumps(self.toDict())




class EnvironmentCreatedList(BaseMessage):
    """
    A class encapsulating EnvironmentCreatedList messages.

    Attributes
    ----------
    mission_timer : 
        Current mission timer
    elapsed_milliseconds : int
        Number of milliseconds since mission start
    list : List[EnvironmentObject]
        List of created environment blocks
    """


    def __init__(self, **kwargs):
        """
        """
        

        BaseMessage.__init__(self, **kwargs)

        self.__environment_objects = []

        self._triggering_entity = kwargs.get("triggering_entity", None)


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'DoorEvent')
        """

        return self.__class__.__name__


    @property
    def triggering_entity(self):
        return self._triggering_entity
    
    @property
    def objects(self):
        return self.__environment_objects


    @property
    def list(self):
        return self.objects
    

    def add_object(self, object_):
        """
        """

        self.__environment_objects.append(object_)


    def finalize(self):
        """
        """

        self.__environment_objects = tuple(self.__environment_objects)


    def toDict(self):
        """
        Generates a dictionary representation of the DoorEvent message.  
        DoorEvent information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the DoorEvent.
        """

        jsonDict = BaseMessage.toDict(self)
    
        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        jsonDict['data']['triggering_entity'] = self.triggering_entity
        jsonDict['data']['list'] = [object_.toDict() for object_ in self.objects]

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the DoorEvent message.  DoorEvent
        information is contained in a JSON object under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            DoorEvent message.
        """

        return json.dumps(self.toDict())
