# -*- coding: utf-8 -*-
"""
.. module:: player_state_change
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Player State Change messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Player State Change messages.
"""


import json

import ciso8601

from .attributes import Attributes, ChangedAttributeValues

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

class PlayerStateChange(BaseMessage):
    """
    A class encapsulating PlayerStateChange messages.

    Attributes
    ----------
    participant_id: string
        Unique identifier of participant (e.g. "P000420")
    source_type : string
        Type of entity that affected the change
    source_id : string
        The source id of the entity that affected the change
    player_location : tuple(float)
        (x,y,z) location of the player
    player_x : float
        x position of the player
    player_y : float
        y position of the player
    player_z : float
        z position of the player
    participant_id : string
        The participant id of the player
    source_location : tuple (float)
        (x,y,z) location of the source of the change
    source_x : float
        the x location of the source of the change
    source_y : float
        the y location of the osurce of the change
    source_z : float
        The z locaiton of the source of the change
    changedAttributes : Attributes
        Map of any changed attributes in their previous and current values
    currentAttributes : Attributes
        Map representing the current attribute state of the player
    """

    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)


        self._source_type = kwargs["source_type"]
        self._source_id = kwargs["source_id"]
        self._player_location = (kwargs["player_x"],
                                 kwargs["player_y"],
                                 kwargs["player_z"])
        self._participant_id = kwargs["participant_id"]
        self._source_location = (kwargs["source_x"],
                                 kwargs["source_y"],
                                 kwargs["source_z"])
        self._changedAttributes = Attributes()
        self._currentAttributes = Attributes()


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message
        """

        return self.__class__.__name__


    @property
    def source_type(self):
        return self._source_type

    @property
    def source_id(self):
        return self._source_id

    @property
    def player_location(self):
        return self._player_location

    @property
    def participant_id(self):
        return self._participant_id
    

    @property
    def player_x(self):
        return self._player_location[0]
    
    @property
    def player_y(self):
        return self._player_location[1]

    @property
    def player_z(self):
        return self._player_location[2]    

    @property
    def source_location(self):
        return self._source_location

    @property
    def source_x(self):
        return self._source_location[0]
    
    @property
    def source_y(self):
        return self._source_location[1]

    @property
    def source_z(self):
        return self._source_location[2]

    @property
    def changed_attributes(self):
        return self._changedAttributes

    @property
    def current_attributes(self):
        return self._currentAttributes


    def add_attribute(self, name, current_value, previous_value=None):
        """
        Add an attribute to the PlayerStateChange message.  If `previous_value`
        is given as `None`, then the attribute is only added to the current
        attributes.

        Arguments
        ---------
        name : string
            Name of the attribute to add
        current_value
            Value of the attribute after the change
        previous_value
            Value of the attribute prior to the change
        """

        self._currentAttributes.add_attribute(name, current_value)

        # Add the attribute to the changed attributes, if suitable.  Otherwise,
        # issue a warning that no attribute was provided
        if previous_value is not None:
            self._changedAttributes.add_attribute(name, ChangedAttributeValues(previous_value, current_value))
        else:
            # TODO: Raise a warning
            pass

    def finalize(self):
        """
        """

        self._currentAttributes.finalize()
        self._changedAttributes.finalize()


    def get_attribute_names(self):
        """
        """

        return self._currentAttributes.get_attribute_names()


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the PlayerStateChange.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["source_type"] = self.source_type
        jsonDict["data"]["source_id"] = self.source_id
        jsonDict["data"]["player_x"] = self.player_x
        jsonDist["data"]["player_y"] = self.player_y
        jsonDist["data"]["player_z"] = self.player_z
        jsonDict["data"]["participant_id"] = self.participant_id
        jsonDict["data"]["source_x"] = self.source_x
        jsonDist["data"]["source_y"] = self.source_y 
        jsonDict["data"]["source_z"] = self.source_z
        jsonDict["data"]["changedAttributes"] = { name: [self.changedAttributes[name].previous, 
                                                         self.changedAttributes[name].current] 
                                                  for name in self.changedAttributes.get_attribute_names()}
        jsonDict["data"]["currAttributes"] = self.currentAttributes.toDict()["currentAttributes"]

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            PlayerState message.
        """

        return json.dumps(self.toDict())
