# -*- coding: utf-8 -*-
"""
.. module:: mission_level_progress
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating PlayerInventoryUpdate messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating PlayerInventoryUpdate messages.
"""


import json

import ciso8601

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage


class PlayerInventoryUpdate(BaseMessage):
    """
    A class encapsulating PlayerInventoryUpdate messages.

    Attributes
    ----------
    current_inventory : Map[str,Map[str,int]]
        Mapping of player ID to dictionary of inventory
    """

    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._current_inventory = kwargs.get("currInv", {})

    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message
        """

        return self.__class__.__name__


    @property
    def current_inventory(self):
        return self._current_inventory

    @property
    def currInv(self):
        return self._current_inventory    
    

    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the PlayerInventoryUpdate message.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["currInv"] = self.current_inventory

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            PlayerInventoryUpdate message.
        """

        return json.dumps(self.toDict())
