# -*- coding: utf-8 -*-
"""
.. module:: fov
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating FoV messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Field of View messages.
"""


import json

from .message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from .base_message import BaseMessage


class FoVDelta(BaseMessage):
    """
    A class encapsulating messages indicating which blocks have entered or left
    a participant's FoV.

    Attributes
    ----------
    participant_id : string
        The participant_id whose FoV is being summarized
    observationNumber : string
        The observation number (from PlayerState) associated with the FoV summary
    blocks : list of dictionaries
        List of block summary information, consisting of a dictionary for each
        block summarized.
    """


    def __init__(self, **kwargs):

        BaseMessage.__init__(self, **kwargs)    

        # Check to see if the necessary arguments have been passed, raise an exception if one is missing
        for arg_name in ['participant_id', 'observationNumber']:
            if not arg_name in kwargs:
                raise MissingMessageArgumentException(self.__class__.__name__, arg_name) from None

        # TODO: Validate the passed arguments
        self._participant_id = kwargs['participant_id']
        self._playername = kwargs.get('playername', self._participant_id)
        self._observationNumber = kwargs['observationNumber']
        self._entered_blocks = kwargs.get('entered_blocks', [])
        self._left_blocks = kwargs.get('left_blocks', [])


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'FovSummary')
        """

        return self.__class__.__name__


    @property
    def playername(self):
        """
        Get the name of the player whose FoV is summarized.

        Attempting to set `playername` raises an `ImmutableAttributeException`.
        """

        return self._playername

    @playername.setter
    def playername(self, name):
        raise ImmutableAttributeException(str(self), "playername")


    @property
    def participant_id(self):
        """
        Get the participant_id whose FoV is summarized.

        Attempting to set `participant_id` raises an `ImmutableAttributeException`.
        """

        return self._participant_id

    @participant_id.setter
    def participant_id(self, _):
        raise ImmutableAttributeException(str(self), "participant_id")
   

    @property
    def observationNumber(self):
        """
        Get the observation number (from the PlayerState message) associated
        with this FoV message.

        Attempting to set `observationNumber` raises an `ImmutableAttributeException`.
        """

        return self._observationNumber

    @observationNumber.setter
    def observationNumber(self, number):
        raise ImmutableAttributeException(str(self), "observationNumber")


    @property
    def entered_blocks(self):
        """
        Get the list of block that entered the participant's FoV

        Attempting to set `entered_blocks` raises an `ImmutableAttributeException`.
        """

        return self._entered_blocks

    @entered_blocks.setter
    def entered_blocks(self, block_list):
        raise ImmutableAttributeException(str(self), "entered_blocks")


    @property
    def left_blocks(self):
        """
        Get the list of block that left the participant's FoV

        Attempting to set `left_blocks` raises an `ImmutableAttributeException`.
        """

        return self._left_blocks

    @left_blocks.setter
    def left_blocks(self, block_list):
        raise ImmutableAttributeException(str(self), "left_blocks")


    def addEnteredBlock(self, block):
        """
        Add a block to the list of blocks that entered the FoV.

        Arguments
        ---------
        block : FoVBlock
        """

        self._entered_blocks.append(block)


    def addLeftBlock(self, block):
        """
        Add a block to the list of blocks that left the FoV.

        Arguments
        ---------
        block : FoVBlock
        """

        self._left_blocks.append(block)


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the FoVSummary.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        jsonDict['data'] = {  'participant_id': self.participant_id,
                              'observation': self.observationNumber,
                              'entered_blocks': [block.toDict() for block in self.entered_blocks],
                              'left_blocks': [block.toDict() for block in self.left_blocks]
                            }

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            FoVSummary message.
        """

        return json.dumps(self.toDict())        
