"""
"""

import MinecraftElements

from MinecraftBridge.messages import VictimPickedUp

from dynamics_update import DynamicsUpdate

class VictimPickedUpUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return VictimPickedUp


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Make sure there is a victim in the location indicated in the message
        if not self.world_model.containsBlockAt(message.location):
            self.logger.warning("%s:    No block located in position %s", self, str(message.location))
            return

        # What is the victim block and who picked it up?
        victim_block = self.world_model.getBlockAt(message.location)
        participant = self.world_model.participants.get(message.participant_id, None)

        # Make sure it's a victim block
        if victim_block.block_type not in [MinecraftElements.Block.block_victim_1,
                                           MinecraftElements.Block.block_victim_2,
                                           MinecraftElements.Block.block_victim_1b,
                                           MinecraftElements.Block.block_victim_saved_a,
                                           MinecraftElements.Block.block_victim_saved_b,
                                           MinecraftElements.Block.block_victim_saved_c,
                                           MinecraftElements.Block.block_victim_expired,
                                           MinecraftElements.Block.block_victim_saved,
                                           MinecraftElements.Block.block_victim_proximity]:
            self.logger.warning("%s:    Non-victim block at position %s: %s", self, str(message.location), victim_block.block_type)
            return

        # Issue a warning if the worker thinks the participant is carrying a
        # victim
        if participant is not None and participant.victim_block is not None:
            self.logger.warning("%s    Participant %s currently carrying a victim: %s.  Replacing.", self, participant, participant.victim_block)

        # Hide the victim block, and give the victim block to the participant
        # (if the participant exists)
        self.world_model.hide(victim_block)
        if participant is not None:
            participant.victim_block = victim_block