"""
"""

import MinecraftElements

from MinecraftBridge.messages import LeverEvent

from dynamics_update import DynamicsUpdate

class LeverUpdate(DynamicsUpdate):
	"""
	Definition of what happens when a lever is toggled
	"""

	def __init__(self, world_model):
		"""
		"""

		super().__init__(world_model)


	def __str__(self):
		"""
		"""

		return self.__class__.__name__


	def MessageClass(self):
		"""
		"""

		return LeverEvent


	def __call__(self, message):
		"""
		"""

		self.logger.debug("%s:  Processing %s message", self, message)

		
