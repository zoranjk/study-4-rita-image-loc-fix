"""
"""



import MinecraftElements

from MinecraftBridge.messages import RubblePlacedEvent

from dynamics_update import DynamicsUpdate

class RubblePlacedUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return RubblePlacedEvent


    def __call__(self, message):
        """
        """

        print("RUBBLE PLACED")        

        self.logger.debug("%s:  Processing %s message", self, message)

        pass
