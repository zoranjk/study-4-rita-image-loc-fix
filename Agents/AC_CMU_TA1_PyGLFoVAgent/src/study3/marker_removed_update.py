"""
"""

import MinecraftElements

from MinecraftBridge.messages import MarkerRemovedEvent

from dynamics_update import DynamicsUpdate

class MarkerRemovedUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return MarkerRemovedEvent


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Location of the marker to be removed
        location = message.location

        # Check to see if a block exists there
        if not self.world_model.containsBlockAt(location):
            self.logger.error("%s:  Attempting to remove non-existant marker block at %s", self, str(location))
            return

        marker_block = self.world_model.getBlockAt(location)

        # Check to see if the block is indeed a marker block
        if marker_block.block_type is not MinecraftElements.Block.marker_block:
            self.logger.error("%s:  Attempting to remove non-marker block %s: expecting marker block", self, marker_block)

        # Hide the block
        self.world_model.hide(marker_block)

        # Restore whatever prior block was there
        if location in self.world_model.context.marker_block_replacements.keys():
            self.world_model.unhide(self.world_model.context.marker_block_replacements[location])
            del self.world_model.context.marker_block_replacements[location]

        # Drop any rubble
        self.__drop_rubble(location)


    def __drop_rubble(self, hole_location):
        """
        Function to drop any rubble immediately above the given location

        Arguments
        ---------
        hole_location : triple
            (x,y,z) location to drop rubble into
        """

        ### TODO: MAKE RECURSIVE

        # Move any rubble (or other movable blocks) down due to gravity
        # Check the spot above the recently removed rubble.  If there's a 
        # rubble block there, move that block down and repeat
        dropping_rubble = True
        
        while dropping_rubble:
            # Check immediately above the current rubble location
            check_location = (hole_location[0], hole_location[1]+1, hole_location[2])

            # Check if there's a block there
            if self.world_model.containsBlockAt(check_location):
                # Check if it's a gravel block
                check_block = self.world_model.getBlockAt(check_location)
                if check_block.block_type == MinecraftElements.Block.gravel:

                    # Move the block down
                    self.logger.debug("%s:      Moving Rubble at %s down to %s", self, str(check_block.location), str(hole_location))

                    self.world_model.removeBlock(check_block)
                    check_block.location = hole_location
                    self.world_model.add(check_block)

                    # And check the next block up
                    hole_location = check_location

                else:
                    # Not a rubble block, nothing further to check
                    dropping_rubble = False

            else:
                # No block found in the check spot
                dropping_rubble = False                