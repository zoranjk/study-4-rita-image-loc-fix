"""
"""

import MinecraftElements

from MinecraftBridge.messages import VictimsExpired

from dynamics_update import DynamicsUpdate

class VictimsExpiredUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return VictimsExpired


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        with self.world_model.feeder_lock:
            # Iterate through the blocks in the feeder, and change any of 
            # type "block_victim_2"
            for block in self.world_model.world:
                if block.block_type == MinecraftElements.Block.block_victim_2:
                    block.block_type = MinecraftElements.Block.block_victim_expired