"""
"""

import MinecraftElements

from MinecraftBridge.messages import ThreatSignList

import pygl_fov

from dynamics_update import DynamicsUpdate

class ThreatSignListUpdate(DynamicsUpdate):
    """
    Definition of what happens when a
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return ThreatSignList


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Add the freeze blocks to the feeder
        with self.world_model.feeder_lock:
            self.logger.debug("%s:    Number of blocks prior to processing: %d", self, len(self.world_model.world))

            # Start a new list of blocks for freeze block lists
            signs = []

            for sign in message.threat_signs:

                # Create the block
                block = pygl_fov.Block(sign.location,
                                       block_type=sign.block_type)

                self.logger.debug("%s      Adding Threat Sign %s at %s", self, block, str(block.location))

                # Check if there's a block to hide
                if self.world_model.containsBlockAt(block.location):
                    block_to_remove = self.world_model.getBlockAt(block.location)
                    self.logger.debug("%s:        Removing existing Block %s at %s", self, block_to_remove, str(block.location))
                    self.world_model.hide(block_to_remove)

                signs.append(block)


            # Add the freeze blocks to the world, creating a vertex store in
            # the process
            self.world_model.addBlocks(signs)

            self.logger.info("%s:      Number of Blocks after processing: %d", self, len(self.world_model.world))