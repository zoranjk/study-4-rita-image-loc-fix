"""
fovWorkerMultithread.py

Class definition of an FoV Worker that manages separate FoV workers threads for
each participant.

Author: Dana Hughes
email: danahugh@andrew.cmu.edu
"""

from fovWorker import FoVWorker


from MinecraftBridge.messages import (
    PlayerState, 
    VictimList, 
    BlockageList
)

from MinecraftBridge.utils import Loggable

import numpy as np
import time


__author__ = 'danahugh'


class FoVWorkerMultithread(Loggable):
    """
    A worker for performing FoV calculations, which manages a single FoV worker
    thread for each participant.

    Attributes
    ----------
    worker_key : tuple of strings
        Key associated with this worker, consisting of
        (experiment_id, trial_id, replay_id)
    state : FoVWorker.State
        State that the worker is currently in
    """


    def __init__(self, parent, config, key=None, map_path=None):
        """
        Create a new FoVWorker.

        Arguments
        ---------
        parent : PyGLFoVAgent
            Agent that created and is managing this worker
        key : tuple 
            Key this worker is referred to by
        map_path : stirng
            Path to JSON file containing map block information
        """

        # Create the FoV worker's name
        self.__string_name = '[FoVWorkerMultithread]'

        self.config = config
        self.parent = parent
        self.key = key
        self.map_path = map_path

        self.state = FoVWorker.State.INITIALIZING

        # Dictionary mapping participant_id to worker instance
        self.fov_workers = {}

        # Cache for victim and blockage list messages.  Victim and blockage list
        # messages will be stored until the map is created for each worker
        self.victimAndBlockageMessageCache = []

        # Create a worker for each participant
        for participant in self.parent.participants:
            self.logger.info("%s:  Creating Worker Thread for Participant: %s", self, participant.participant_id)
            self.fov_workers[participant.participant_id] = FoVWorker(self.parent, self.config, self.key, self.map_path)

        # Indicate that this worker is running
        self.state = FoVWorker.State.RUNNING        

        # Everything is initialized, and the worker is ready to be started.
        self.state = FoVWorker.State.READY


    def __str__(self):
        """
        String representation of the FoV worker)
        """

        return self.__string_name



    def is_alive(self):
        """
        """

        # Return True if _any_ worker is still alive
        alive = False

        for worker in self.fov_workers.values():
            alive = alive or worker.is_alive()

        return alive



    def join(self):
        """
        Join all the FoV workers
        """

        for worker in self.fov_workers.values():
            worker.join()


    def start(self):
        """
        """

        # Create a worker for each participant
        for participant in self.parent.participants:

###            # Send each cached message to the worker threads
###            for message in self.victimAndBlockageMessageCache:
###                self.fov_workers[participant.participant_id].addMessage(message)
###            self.victimAndBlockageMessageCache.clear()

            # Start the worker
            self.logger.info("%s:    Starting Worker Thread", self)
            self.fov_workers[participant.participant_id].start()
            # Wait for the worker to be running
            self.logger.info("%s:      Waiting for Worker Thread to be running", self)
            while self.fov_workers[participant.participant_id].state != FoVWorker.State.RUNNING:
                time.sleep(0)
            self.logger.info("%s:      Worker Thread Running", self)



        self.logger.info("%s:    Worker threads running", self)

        # Indicate that this worker is running
        self.state = FoVWorker.State.RUNNING


    def stop(self):
        """
        Indicate the thread should stop
        """

        # No need to do anything if already stopping or stopped
        if self.state in [FoVWorker.State.STOPPING, FoVWorker.State.STOPPED]:
            return

        # Stop all workers
        for worker in self.fov_workers.values():
            worker.stop()

        # Change the state to STOPPING, to indicate that there could still
        # be some messages in the queue to process.
        self.state = FoVWorker.State.STOPPING



    def kill(self):
        """
        Force a quick stop to the thread, deleting the remaining contents of
        the messageQueue
        """

        for worker in self.fov_workers.values():
            worker.kill()


    def addMessage(self, message):
        """
        Add a message to the message queue to be processed
        """

        # Don't queue any messages if this worker is done
        if self.state in [FoVWorker.State.STOPPING, FoVWorker.State.STOPPED]:
            return


        # If running, pass all non-PlayerState messages to each worker,
        # otherwise, either set the player position (and don't render), or
        # pass the PlayerState message, depending on the message's participant
        # ID
        if self.state == FoVWorker.State.RUNNING:
            if message.__class__ == PlayerState:
                for participant_id, worker in self.fov_workers.items():
                    if participant_id == message.participant_id:
                        worker.addMessage(message)
                    else:
                        worker.setPlayerPosition(message)
            else:
                for worker in self.fov_workers.values():
                    worker.addMessage(message)
###        else:
###            if message.__class__ in [VictimList, BlockageList]:
###                self.victimAndBlockageMessageCache.append(message)


    def get_profile_stats(self):
        """
        Return the profiling statistics for PlayerState processing times.

        Returns
        -------
        dictionary
            "counts": Number of times PlayerState was processed
            "average_time": Average time to process PlayerState messages
            "stdev_time": Standard deviation of time to process PlayerState messages
            "min_time": Minimum time to process PlayerState messages
            "max_time": Maximum time to process PlayerState messages
        """

        # Get the processing times from each worker
        process_player_state_times = []

        for worker in self.fov_workers.values():
            process_player_state_times += worker.process_player_state_times

        # Calculate the statistics, or provide dummy values if no method calls
        # have been made yet
        if len(process_player_state_times) == 0:
            average_time = 0.0
            stdev_time = 0.0
            min_time = 0.0
            max_time = 0.0
        else:
            average_time = np.mean(process_player_state_times)
            stdev_time = np.std(process_player_state_times)
            min_time = min(process_player_state_times)
            max_time = max(process_player_state_times)

        return { "counts": len(process_player_state_times),
                 "average_time": average_time,
                 "stdev_time": stdev_time,
                 "min_time": min_time,
                 "max_time": max_time
               }


    def reset_profile_stats(self):
        """
        """

        for worker in self.fov_workers.values():
            worker.reset_profile_stats()

