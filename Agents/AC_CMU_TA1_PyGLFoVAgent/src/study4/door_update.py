"""
"""

import MinecraftElements

from MinecraftBridge.messages import DoorEvent

from dynamics_update import DynamicsUpdate

class DoorUpdate(DynamicsUpdate):
	"""
	Definition of what happens when a door event is received
	"""

	def __init__(self, world_model):
		"""
		"""

		super().__init__(world_model)


	def __str__(self):
		"""
		"""

		return self.__class__.__name__


	def MessageClass(self):
		"""
		"""

		return DoorEvent


	def __call__(self, message):
		"""
		"""

		self.logger.debug("%s:  Processing %s message", self, message)

		with self.world_model.feeder_lock:

			location = message.position

			# Get the door block at the location, as well as the block in the
			# other half of the door, and change their `open` attribute
			door_block = self.world_model.getBlockAt(location)

			if door_block == None:
				self.logger.warning("%s:  Door does not exist at location %s in DoorEvent message", self, str(location))
				return

			if door_block.half == MinecraftElements.Half.upper:
				second_door_block = self.world_model.getBlockAt((location[0], location[1]-1, location[2]))
			else:
				second_door_block = self.world_model.getBlockAt((location[0], location[1]+1, location[2]))

			door_block.open = message.opened
			second_door_block.open = message.open
