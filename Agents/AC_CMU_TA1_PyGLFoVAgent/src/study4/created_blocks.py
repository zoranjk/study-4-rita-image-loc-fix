"""
"""

import MinecraftElements

import pygl_fov

from MinecraftBridge.messages import (
    EnvironmentCreatedList,
    EnvironmentCreatedSingle
)

from dynamics_update import DynamicsUpdate

class CreatedBlockListUpdate(DynamicsUpdate):
    """
    Definition of what happens when an EnvironmentCreatedList is received
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return EnvironmentCreatedList


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Add created blocks into the block feeder
        with self.world_model.feeder_lock:
            self.logger.debug("%s:    Number of blocks prior to processing: %s", self, len(self.world_model.world))

            # Start a new list of blocks to add to the world model
            new_blocks = []

            for environment_block in message.objects:
                block = pygl_fov.Block(environment_block.location,
                                       block_type=environment_block.type)

                # Store the block attributes
                block_attributes = { "testbed_id": environment_block.id,
                                     "triggering_entity": environment_block.triggering_entity,
                                     **{x: environment_block.get_attribute(x) 
                                        for x in environment_block.get_attribute_names()}
                                   }
                self.world_model.context.add_block_attributes(block, block_attributes)

                self.logger.debug("%s:  Added Block: %s", self, block)

                # If the block type is air, remove the current block at that location
                if block.block_type == MinecraftElements.Block.air:

                    # Hide the existing block, to avoid having to recalculate the VBO
                    self.world_model.hide(self.world_model.getBlockAt(block.location))

                else:
                    self.logger.debug("%s:      Adding block of type %s at location %s", self, block.block_type, block.location)

                    # Hide any block that happens to be in the block's location
                    if self.world_model.containsBlockAt(block.location):
                        self.logger.warning("%s:        Replacing block %s at location %s with new block %s", self, self.world_model.getBlockAt(block.location), block.location, block)
                        self.world_model.hide(self.world_model.getBlockAt(block.location))

                    new_blocks.append(block)

            self.world_model.addBlocks(new_blocks)

            self.logger.debug("%s:    Number of blocks after processing: %d", self, len(self.world_model.world))



class CreatedBlockSingleUpdate(DynamicsUpdate):
    """
    Definition of what happens when an EnvironmentCreatedSingle is received
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return EnvironmentCreatedSingle


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Add created blocks into the block feeder
        with self.world_model.feeder_lock:
            self.logger.debug("%s:    Number of blocks prior to processing: %s", self, len(self.world_model.world))

            block=pygl_fov.Block(message.object.location,
                                 block_type=message.object.type)

            # Store the block attributes
            block_attributes = { "testbed_id": message.object.id,
                                 "triggering_entity": message.object.triggering_entity,
                                 **{x: message.object.get_attribute(x) 
                                    for x in message.object.get_attribute_names()}
                               }
            self.world_model.context.add_block_attributes(block, block_attributes)


            # If the block type is air, remove the current block at that location
            if block.block_type == MinecraftElements.Block.air:

                # Hide the existing block, to avoid having to recalculate the VBO
                self.world_model.hide(self.world_model.getBlockAt(block.location))

            else:
                self.logger.debug("%s:      Adding block of type %s at location %s", self, block.block_type, block.location)

                # Hide any block that happens to be in the block's location
                if self.world_model.containsBlockAt(block.location):
                    self.logger.warning("%s:        Replacing block %s at location %s with new block %s", self, self.world_model.getBlockAt(block.location), block.location, block)
                    self.world_model.hide(self.world_model.getBlockAt(block.location))

                self.world_model.addBlocks([block])

            self.logger.debug("%s:    Number of blocks after processing: %d", self, len(self.world_model.world))