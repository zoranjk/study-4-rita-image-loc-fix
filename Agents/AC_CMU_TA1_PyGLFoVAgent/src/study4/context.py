"""
"""

class Context:
	"""
	Simple context class for ASIST Study 4 environment dynamics
	"""

	def __init__(self):
		"""
		"""

		self.__block_attributes = {}

		self._mission_stage = None


	def __str__(self):
		"""
		"""

		return self.__class__.__name__


	@property
	def mission_stage(self):
		return self._mission_stage

	@mission_stage.setter
	def mission_stage(self, stage):
		self._mission_stage = stage


	def add_block_attributes(self, block, attributes):
		"""
		Arguments
		---------
		block : pygl_fov.Block
			Block to add
		attributes : dict
			Dictionary of block attributes
		"""

		# Check to see if the block exists, and issue a warning if so
		if block in self.__block_attributes:
			# TODO:  Issue warning
			pass

		self.__block_attributes[block] = attributes


	def set_block_attribute(self, block, attribute_name, attribute_value):
		"""
		Arguments
		---------
		block : pygl_fov.Block
			Block to set attribute for
		attribute_name : string
			Attribute name to set
		attribute_value
			Value to set the attribute for
		"""

		# Check to see if the block exists, and issue a warning if not
		if not block in self.__block_attributes:
			# TODO:  Issue warning
			return

		self.__block_attributes[block][attribute_name] = attribute_value


	def get_block_attribute_names(self, block):
		"""
		Arguments
		---------
		block : pygl_fov.Block
			Block to get attribute names for
		"""

		# Return an empty list of attributes if the block isn't in there...
		if not block in self.__block_attributes:
			return []

		return self.__block_attributes[block].keys()


	def get_block_attribute(self, block, attribute_name):
		"""
		Arguments
		---------
		block : pygl_fov.Block
			Block to get an attribute for
		attribute_name : string
			Name of attribute to get
		"""

		# Check to see if the block exists, and issue a warning if not
		if not block in self.__block_attributes:
			# TODO:  Issue a warning
			return None

		# Check to see if the attribute name exists, and issue a warning if not
		if not attribute_name in self.__block_attributes[block]:
			# TODO:  Issue a warning
			return None

		return self.__block_attributes[block][attribute_name]


	def block_has_attributes(self, block):
		"""
		Arguments
		---------
		block : pygl_fov.Block
		"""

		return block in self.__block_attributes
