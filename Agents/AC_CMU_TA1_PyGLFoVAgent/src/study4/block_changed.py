"""
"""

import MinecraftElements

import pygl_fov

from MinecraftBridge.messages import ObjectStateChange

from dynamics_update import DynamicsUpdate

class BlockChangedUpdate(DynamicsUpdate):
    """
    Definition of what happens when an ObjectStateChange is received
    """

    def __init__(self, world_model):
        """
        """

        super().__init__(world_model)


    def __str__(self):
        """
        """

        return self.__class__.__name__


    def MessageClass(self):
        """
        """

        return ObjectStateChange


    def __call__(self, message):
        """
        """

        self.logger.debug("%s:  Processing %s message", self, message)

        # Get the attributes from the world context
        location = message.location

        # Check if there's a block at this location -- issue an error if not
        if not self.world_model.containsBlockAt(location):
            self.logger.error("%s:  World model does not contain block at location %s", self, str(location))
            self.logger.error("%s:  Message Contents: %s", self, message.toJson())
            return

        # Get the block and check that 1) the block exists in the context, and
        # 2) the testbed_id of the block and the one in context match
        block = self.world_model.getBlockAt(location)

        if not self.world_model.context.block_has_attributes(block):
            self.logger.warning("%s:  Block %s at location %s does not have known attributes in context", self, block, block.location)
            # Create the attributes and add to the context
            attributes = { "testbed_id": message.id,
                           "triggering_entity": None,
                           **{ x: message.current_attributes.get_attribute(x)
                                  for x in message.current_attributes.get_attribute_names() } 
                         }
            self.world_model.context.add_block_attributes(block, attributes)

        else:
            for name in message.current_attributes.get_attribute_names():
                value = message.current_attributes.get_attribute(name)
                self.world_model.context.set_block_attribute(block, name, value)
