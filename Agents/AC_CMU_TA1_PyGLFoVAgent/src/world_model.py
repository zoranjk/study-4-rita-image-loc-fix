"""

"""

import threading

from MinecraftBridge.utils import Loggable

import pygl_fov


class WorldModel(Loggable):
	"""
	This class defines a base representation of blocks and entities in the
	Minecraft world, and basic operations to create, manipulate, and destroy
	blocks.

	The purpose of this class is to decouple rendering of the world from the
	dynamics of the world, i.e., so that defining behaviors for a novel set of
	messages simply involves mapping individual messages to operations in the
	WorldModel.
	"""

	def __init__(self, context=None, participants=None, **kwargs):
		"""
		Keyword Arguments
		-----------------
		hidden_location : tuple[float], default=(0.0, -10000.0, 0.0)
			(x,y,z) location to "hide" blocks at
		"""

		self.__string_name = '[World Model]'

		self.world = None
		self.world_vertex_store = None
		self.player_vertex_store = None

		self.participants = participants

		# Dictionary of hidden blocks -- hidden blocks are moved to a 
		# pre-defined location "outside" of the world.  The corresponding
		# dictionary will map block instances to their original location.
		self.hidden_location = kwargs.get('hidden_location', (0.0, -10000.0, 0.0))
		self.hidden_blocks = {}

		# Context defines world or study specific attributes that may be
		# updated.  Note, though, that this is not needed if block and entity
		# positions are all that need to be modeled.
		self.context = context

		# Keep the feeder_lock here
		self.feeder_lock = threading.Lock()


	def __str__(self):
		"""
		String representation of the WorldModel
		"""

		return self.__string_name


	def initialize(self):
		"""
		Create the needed components for the world, including a BlockFeeder for
		the world, and vertex stores for the world and entities / participants.
		"""

		self.logger.info("%s:  Creating pygl_fov objects", self)

		# Needed components for maintaining world information for rendering
		# purposes:  a composite block feeder for storing chunks of blocks,
		# and a composite vertex store for storing vertices of each block.

		self.logger.info("%s:    Creating World Block Feeder", self)
		self.world = pygl_fov.BlockFeeder(name="World Block Feeder")
		self.logger.info("%s:    Creating World Vertex Store", self)
		self.world_vertex_store = pygl_fov.CompositeVertexStore(name="World Vertex Store")
		self.logger.info("%s:    Creating Entity Vertex Store", self)
		self.player_vertex_store = pygl_fov.CompositeVertexStore(name="Participant Vertex Store")
		self.world_vertex_store.addStore(self.player_vertex_store)


	def hide(self, block):
		"""
		Hide a block by moving it to a _default_ hidden location.  The block
		and its original location will be stored, so that it can later be 
		unhidden.

		Arguments
		---------
		block : pygl_fov.Block
			Instance of Block to hide
		"""

		self.logger.debug("%s:  Hiding block: %s", self, block)

		# Does the feeder actually contain this block?
		if not block.id in self.world:
			self.logger.warning("%s:    Attempting to hide block not in feeder: %s.  Ignoring", self, block)
			return

		if block in self.hidden_blocks.keys():
			self.logger.warning("%s:    Block already hidden, ignoring.", self)

		# Store the block and its current location
		self.hidden_blocks[block] = block.location

		# Remove the block from the world feeder
		self.world.removeBlock(block.id)

		# Change the location of the block so it is located at a pre-defined
		# hiding spot
		block.location = self.hidden_location


	def unhide(self, block, new_location=None):
		"""
		Restore a hidden block to its original position.

		Arguments
		---------
		block : pygl_fov.Block
			Instance of Block to unhide
		new_location : tuple[float], optional, default=None
			Location to place the block
		"""

		self.logger.debug("%s:  Unhiding block: %s", self, block)

		# Does the feeder actually contain this block?
		if not block in self.hidden_blocks.keys():
			self.logger.warning("%s:    Attempting to unhide non-hidden block: %s", self, block)
			return

		# Move to a new location, or the original?
		if new_location is None:
			block.location = self.hidden_blocks[block]
		else:
			block.location = new_location

		# Add the block back to the feeder
		self.world.add(block)

		# Remove the block from the hidden blocks
		del self.hidden_blocks[block]


	def isHidden(self, block):
		"""
		Determine if a block is hidden in the world.

		Arguments
		---------
		block : pygl_fov.Block
			Block to check if hidden

		Returns
		-------
		True if the block is in the collection of hidden blocks
		"""

		return block in self.hidden_blocks.keys()


	def containsBlockAt(self, location):
		"""
		Determine if a block is present in the world at the given location.

		Arguments
		---------
		location : tuple[float]
			(x,y,z) location of the block

		Returns
		-------
		True if a block is present at the given location, False otherwise
		"""

		return self.world.containsBlockAt(location)


	def getBlockAt(self, location):
		"""
		Return a block at the provided location

		Arguments
		---------
		location : tuple[float]
			(x,y,z) location of the block

		Returns
		-------
		pygl_fov.Block instance a the given location
		"""

		return self.world.getBlockAt(location)


	def add(self, block):
		"""
		Add a block to the world

		Arguments
		---------
		block : pygl_fov.Block
			Block to add to world
		"""

		self.world.add(block)


	def removeBlock(self, block):
		"""
		Remove a block from the world

		Arguments
		---------
		block : pygl_fov.Block
			Block to remove
		"""

		self.world.removeBlock(block.id)


	def addBlocks(self, blocks, vertex_store=None):
		"""
		Construct a vertex store for the provided block feeder, and add the
		feeder and vertex store to the existing world model

		Arguments
		---------
		blocks : pygl_fov.BlockFeeder
			Block feeder containing blocks to add
		vertex_store : pygl_fov.VertexStore, default=None
		"""

		self.logger.debug("%s:  Adding blocks to world from BlockFeeder: %s", self, blocks)

		# Make sure that the world and vertex_store exist before attempting to
		# add to them
		if self.world is None:
			self.logger.error("%s:    Attempting to add to non-existant world.  (Was initialize called?)", self)
			return
		if self.world_vertex_store is None:
			self.logger.error("%s:    Attempting to add to non-existant vertex store.  (Was initialize called?)", self)
			return
		
		# Create a vertex store instance for the provided block_feeder, if one
		# is not provided
		if vertex_store is None:
			self.logger.debug("%s:    Creating Vertex Store", self)
			vertex_store = pygl_fov.StaticVboStore(blocks)
		
		# Add the block_feeder to the world, and the vertex_store to the 
		# world_vertex_store
		for block in blocks:
			self.logger.debug("%s:      Adding Block %s", self, block)
			self.add(block)
		self.world_vertex_store.addStore(vertex_store)


	def addBlocksFromJson(self, path):
		"""
		Add blocks defined in a JSON file to the world.

		Arguments
		---------
		path : string
			Path to JSON file containing block information
		"""

		block_feeder = pygl_fov.BlockFeeder.loadFromJson(path)
		self.addBlocks(block_feeder)


	def addEntity(self, block, vertices):
		"""
		Add a block defining an entity (e.g., player) to the world.

		Arguments
		---------
		block : pygl_fov.Block
			Block defining the entity
		vertices : pygl_fov.VboStore
			Vertices defining the entity's model
		"""

		self.add(block)
		self.player_vertex_store.addStore(vertices)

