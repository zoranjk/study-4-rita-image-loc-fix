use color_eyre::eyre::Result;

pub trait Agent {
    fn process_message(&mut self, payload: &str) -> Result<()>;
}
