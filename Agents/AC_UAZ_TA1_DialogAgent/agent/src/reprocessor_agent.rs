use crate::{
    agent::Agent, config::Config, event_extraction::get_extractions, knowledge_base::KnowledgeBase,
    messages::nlu::NLUMessage, spelling_correction::SpellChecker,
};
use color_eyre::eyre::Result;
use log::{info, warn};
use serde_json as json;
use std::collections::HashSet;
use uuid::Uuid;

pub struct ReprocessorAgent {
    pub config: Config,
    pub kb: KnowledgeBase,
    pub replay_id: String,
    pub spellchecker: SpellChecker,

    /// Sets of trial IDs corresponding to legacy formats we want to handle.
    pub legacy_trial_sets: (HashSet<&'static str>, HashSet<&'static str>),
}

impl ReprocessorAgent {
    pub fn new(config: Config, kb: KnowledgeBase) -> Result<Self> {
        info!("Loading unigram dictionary...");

        let replay_id = Uuid::new_v4().to_string();

        // We construct sets of trial IDs to handle special cases arising due to changing data
        // formats during the ASIST Study 4 pilot runs.
        // We should probably take this part out eventually after the data format settles down, so
        // that we can use this code for non-ASIST projects without fear of UUID collisions.
        let trial_set_0: HashSet<&'static str> = HashSet::from([
            "b1532325-3869-4265-b69a-a601ad4da842",
            "616e855d-b48c-4985-9229-69432f45a3cc",
            "a29ee710-e6c4-4791-a21c-f85a6b8d6087",
            "e8ac5a06-ef96-4a70-82d9-2591d3aaa84d",
            "432f5af6-9163-4c0e-a75b-fdd931ba1430",
            "b6971d21-f810-4f16-bb63-1a9286de8d9d",
            "2554b670-dcb3-4cca-b336-0871595c8afb",
            "082f4e1f-245a-4160-b1fc-ef01f1107a3e",
            "ad596a70-510d-43d9-8162-1fc3a03ef58b",
            "f5cfa219-bdaf-4548-95a4-0d9474e2e9f9",
        ]);

        // In this set of trials:
        // - The chat topic is minecraft/chat instead of communication/chat.
        // - The .data.sender key contains the gn instead of the participant ID, and is
        //   sometimes 'Server'.
        let trial_set_1: HashSet<&'static str> = HashSet::from([
            "f5cfa219-bdaf-4548-95a4-0d9474e2e9f9",
            "56e661e3-cbaa-48e9-9444-705647f934de",
        ]);

        let spellchecker = SpellChecker::new(
            &config.custom_unigram_dict,
            &config.custom_bigram_dict,
            &config.custom_replacement_dict,
            &config.spellchecker_ignore_list,
            &config.boosted_phrase_list,
        )?;

        let agent = Self {
            config,
            kb,
            replay_id,
            spellchecker,
            legacy_trial_sets: (trial_set_0, trial_set_1),
        };

        Ok(agent)
    }
}

impl Agent for ReprocessorAgent {
    fn process_message(&mut self, payload: &str) -> Result<()> {
        let mut value: json::Value = json::from_str(&payload).unwrap();
        match value["msg"].get_mut("replay_parent_type") {
            Some(id) => {
                *id = json::Value::String("REPLAY".into());
            }
            None => {
                value["msg"]["replay_parent_type"] = json::Value::String("TRIAL".into());
            }
        }

        // Check if the .msg.replay_parent_type key exists
        match value["msg"].get_mut("replay_id") {
            Some(id) => {
                *id = json::Value::String(self.replay_id.clone());
            }
            None => {
                value["msg"]["replay_id"] = json::Value::String(self.replay_id.clone());
            }
        }

        // Set the replay parent ID
        if let Some(id) = value["msg"].get("replay_id") {
            if !id.is_null() {
                value["msg"]["replay_parent_id"] = id.clone();
            }
        }

        let trial_id = value["msg"]["trial_id"].as_str().unwrap();
        let chat_topic = if self.legacy_trial_sets.1.contains(&trial_id) {
            "minecraft/chat"
        } else {
            "communication/chat"
        };

        let topic = value["topic"].as_str();

        // To avoid duplication, we do not re-publish old messages from our agent.
        if topic != Some(&format!("agent/{}", &self.config.client_id)) {
            println!("{}", &payload);
        }

        if topic == Some("trial") {
            let sub_type = value["msg"]["sub_type"].as_str();

            if let Some("start") = sub_type {
                // Construct a mapping between gns and participant IDs.
                for client in value["data"]["client_info"].as_array().unwrap() {
                    if self.legacy_trial_sets.1.contains(trial_id) {
                        // For trials in trial_set_1, the playername is published in the
                        // .data.sender field sometimes. We construct this gn mapping to
                        // help normalize the data to consistently use the gn.
                        self.kb.callsign_mapping.insert(
                            client["playername"].as_str().unwrap().to_string(),
                            client["callsign"].as_str().unwrap().to_string(),
                        );
                    } else {
                        self.kb.callsign_mapping.insert(
                            client["participant_id"].as_str().unwrap().to_string(),
                            client["callsign"].as_str().unwrap().to_string(),
                        );
                    }
                }
            }
        }

        if topic == Some(chat_topic) {
            let (sender, text) = if self.legacy_trial_sets.0.contains(trial_id)
                || self.legacy_trial_sets.1.contains(trial_id)
            {
                (
                    value["data"]["sender"].as_str().unwrap(),
                    value["data"]["text"].as_str().unwrap(),
                )
            } else {
                (
                    value["data"]["sender_id"].as_str().unwrap(),
                    value["data"]["message"].as_str().unwrap(),
                )
            };

            // For trials in trial_set_1, the .data.sender field is set to 'Server' for
            // messages originating from the testbed rather than from players. We ignore
            // these as they are not natural language utterances.
            if self.legacy_trial_sets.1.contains(trial_id) && sender == "Server" {
                ()
            }

            // For trials in trial_set_1, the sender field usually contains the callsign, so we don't
            // have to look up the gn in the callsign_mapping struct.
            let sender = if !self.legacy_trial_sets.1.contains(trial_id) {
                self.kb.callsign_mapping.get(sender).unwrap()
            } else {
                // For trials in trial_set_1, the sender field sometimes contains the
                // playername instead of the callsign (I don't know why). So we look up the
                // playername in the gn mapping to normalize the data.
                if let Some(s) = self.kb.callsign_mapping.get(sender) {
                    s
                } else {
                    sender
                }
            };

            let experiment_id = value["msg"]["experiment_id"].as_str().unwrap();
            let utterance_id = match value["data"]["message_id"].as_str() {
                Some(id) => Some(id.to_string()),
                None => None,
            };

            // Get corrected spelling.
            let corrected = self.spellchecker.correct(text);

            let extractions = match get_extractions(&corrected, &self.config.event_extractor_url) {
                Ok(events) => Some(events),
                Err(e) => {
                    warn!("Unable to get events! Error: {:?}", e);
                    None
                }
            };

            let mut message = NLUMessage::new(
                text.to_string(),
                Some(corrected),
                // In the earlier pilot runs, the gns are fully uppercased (e.g., RED).
                // We convert them to lowercase for consistency with the later trials.
                sender.to_string().to_lowercase(),
                extractions,
                trial_id.to_string(),
                experiment_id.to_string(),
                utterance_id,
            );

            // Set the header timestamp to the same value as the input chat message header
            // timestamp.
            message.header.timestamp = value["header"]["timestamp"].as_str().unwrap().to_string();

            let mut output_value = json::to_value(message).unwrap();
            output_value["@timestamp"] = value["@timestamp"].clone();
            output_value["msg"]["replay_id"] = value["msg"]["replay_id"].clone();
            output_value["msg"]["replay_parent_id"] = value["msg"]["replay_parent_id"].clone();
            output_value["msg"]["replay_parent_type"] = value["msg"]["replay_parent_type"].clone();
            output_value["msg"]["replay_parent_id"] = value["msg"]["replay_parent_id"].clone();
            output_value["topic"] = "agent/AC_UAZ_TA1_DialogAgent".into();
            println!("{}", output_value);
        }

        Ok(())
    }
}
