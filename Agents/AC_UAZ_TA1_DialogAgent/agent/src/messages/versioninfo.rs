use crate::{
    config::Config,
    messages::common::{Header, Msg},
};
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct PubSub {
    pub topic: String,
    pub message_type: String,
    pub sub_type: String,
}

#[derive(Clone, Debug, Default, Serialize, Deserialize)]
pub struct VersionInfoData {
    pub agent_name: String,
    pub version: String,
    pub owner: String,
    pub agent_type: String,
    pub config: Vec<HashMap<String, String>>,
    pub source: Vec<String>,
    pub dependencies: Vec<String>,
    pub publishes: Vec<PubSub>,
    pub subscribes: Vec<PubSub>,
}

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct VersionInfoMessage {
    pub header: Header,
    pub msg: Msg,
    pub data: VersionInfoData,
}

impl VersionInfoMessage {
    pub fn from_config(
        cfg: &Config,
        trial_id: Option<String>,
        experiment_id: Option<String>,
    ) -> Self {
        Self {
            header: Header::default(),
            msg: Msg {
                trial_id,
                experiment_id,
                source: cfg.client_id.clone(),
                sub_type: "versioninfo".to_string(),
                version: "0.1".to_string(),
                ..Default::default()
            },
            data: VersionInfoData {
                agent_type: cfg.versioninfo.agent_type.clone(),
                version: cfg.versioninfo.version.clone(),
                owner: cfg.versioninfo.owner.clone(),
                publishes: cfg.versioninfo.publishes.clone(),
                subscribes: cfg.versioninfo.subscribes.clone(),
                ..Default::default()
            },
        }
    }
}
