use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;

// tomcat-text/src/main/scala/org/clulab/asist/attachments/Agent.scala
#[allow(non_snake_case)]
#[derive(Clone, Debug, Eq, Hash, JsonSchema, PartialEq, Serialize, Deserialize)]
pub struct AgentAttachment {
    //#[serde(skip)]
    pub labels: Vec<String>,
    pub agentType: String,
    //#[serde(skip)]
    pub text: String,
    //#[serde(skip)]
    pub span: Vec<u32>,
}

// tomcat-text/src/main/scala/org/clulab/asist/attachments/Negation.scala
#[derive(Clone, Debug, Eq, Hash, JsonSchema, PartialEq, Serialize, Deserialize)]
pub struct BooleanAttachment {
    pub negated: bool,
}

// tomcat-text/src/main/scala/org/clulab/asist/attachments/MarkerId.scala
// tomcat-text/src/main/scala/org/clulab/asist/attachments/Tense.scala
// tomcat-text/src/main/scala/org/clulab/asist/attachments/VictimType.scala
#[derive(Clone, Debug, Eq, Hash, JsonSchema, PartialEq, Serialize, Deserialize)]
pub struct StringAttachment {
    pub value: String,
}

#[derive(Clone, Debug, Eq, Hash, JsonSchema, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum Attachment {
    Agent(AgentAttachment),
    Bool(BooleanAttachment),
    String(StringAttachment),
}

#[derive(Clone, Debug, Eq, Hash, JsonSchema, PartialEq, Serialize, Deserialize)]
pub struct Extraction {
    pub attachments: Vec<Attachment>,
    pub labels: Vec<String>,
    pub span: String,
    pub arguments: Option<BTreeMap<String, Vec<Extraction>>>,
    pub start_offset: u32,
    pub end_offset: u32,
    pub rule: String,
}
