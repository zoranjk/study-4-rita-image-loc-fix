use crate::messages::chat::{Attachment, Extraction};
use crate::messages::common::{Header, Msg};
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};

#[derive(Clone, Debug, JsonSchema, Serialize, Deserialize)]
pub struct NLUData {
    pub text: String,
    pub participant_id: String,

    /// If the text was passed through a spellchecker prior to being sent to the event extractor
    /// and other downstream components, this field will contain the corrected text sent back by
    /// the spellchecker. 
    pub corrected_text: Option<String>,

    /// Verbose representations of extracted events.
    pub extractions: Option<Vec<Extraction>>,

    /// A compact representation of the events extracted from the utterance.
    pub compact_extractions: Option<CompactExtraction>,

    /// ID of the chat message or spoken utterance that was analyzed.
    pub utterance_id: Option<String>,
}

/// Compact message to represent natural language understanding system outputs.
#[derive(Clone, Debug, JsonSchema, Serialize, Deserialize)]
pub struct NLUMessage {
    pub header: Header,
    pub msg: Msg,
    pub data: NLUData,
}

impl NLUMessage {
    pub fn new(
        text: String,
        corrected_text: Option<String>,
        participant_id: String,
        extractions: Option<Vec<Extraction>>,
        trial_id: String,
        experiment_id: String,
        utterance_id: Option<String>,
    ) -> Self {
        let compact_extractions = match extractions.clone() {
            Some(xs) => Some(CompactExtraction::from(xs)),
            None => None,
        };
        Self {
            header: Header {
                version: "1.2".to_string(),
                message_type: "agent".to_string(),
                ..Default::default()
            },
            msg: Msg {
                trial_id: Some(trial_id),
                experiment_id: Some(experiment_id),
                source: "agent".to_string(),
                sub_type: "nlu".to_string(),
                ..Default::default()
            },
            data: NLUData {
                text,
                corrected_text,
                participant_id,
                extractions: extractions.clone(),
                compact_extractions,
                utterance_id,
            },
        }
    }
}

#[derive(Clone, Debug, Eq, JsonSchema, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum CompactAttachment {
    String(String),
    Vec(Vec<Attachment>),
    Object(Attachment),
}

#[derive(Clone, Debug, Default, Eq, JsonSchema, PartialEq, Serialize, Deserialize)]
pub struct CompactExtractionObject {
    pub label: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub arguments: Option<HashMap<String, CompactExtraction>>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub attachments: Option<CompactAttachment>,
}

#[derive(Clone, Debug, Eq, JsonSchema, PartialEq, Serialize, Deserialize)]
#[serde(untagged)]
pub enum CompactExtraction {
    Object(CompactExtractionObject),
    String(String),
    Vec(Vec<CompactExtraction>),
}

impl From<Extraction> for CompactExtraction {
    fn from(extraction: Extraction) -> Self {
        if extraction.arguments.is_none() && extraction.attachments.is_empty() {
            CompactExtraction::String(extraction.labels[0].clone())
        } else {
            let mut compact_extraction = CompactExtractionObject {
                label: extraction.labels[0].clone(),
                ..Default::default()
            };
            if let Some(args) = extraction.arguments {
                compact_extraction.arguments = Some(HashMap::<String, CompactExtraction>::new());
                for (k, v) in args {
                    if v.len() == 1 {
                        compact_extraction
                            .arguments
                            .as_mut()
                            .unwrap()
                            .insert(k, CompactExtraction::from(v[0].clone()));
                    } else {
                        compact_extraction.arguments.as_mut().unwrap().insert(
                            k,
                            CompactExtraction::Vec(
                                v.iter()
                                    .map(|x| CompactExtraction::from(x.clone()))
                                    .collect(),
                            ),
                        );
                    }
                }
            }
            if !extraction.attachments.is_empty() {
                compact_extraction.attachments =
                    Some(CompactAttachment::from(extraction.attachments));
            }
            CompactExtraction::Object(compact_extraction)
        }
    }
}

impl From<Attachment> for CompactAttachment {
    fn from(attachment: Attachment) -> Self {
        match attachment {
            Attachment::Agent(att) => CompactAttachment::String(att.agentType),
            _ => CompactAttachment::Object(attachment),
        }
    }
}

impl From<Vec<Attachment>> for CompactAttachment {
    fn from(attachments: Vec<Attachment>) -> Self {
        if attachments.len() == 1 {
            CompactAttachment::from(attachments[0].clone())
        } else {
            CompactAttachment::Vec(attachments)
        }
    }
}

impl From<Vec<Extraction>> for CompactExtraction {
    fn from(extractions: Vec<Extraction>) -> Self {
        if extractions.len() == 1 {
            CompactExtraction::from(extractions[0].clone())
        } else {
            // If a top-level extraction is an argument of another extraction, remove it from the
            // set of top-level extractions.
            let mut top_level_extractions: HashSet<Extraction> =
                HashSet::from_iter(extractions.clone());
            for extraction in extractions.iter() {
                if let Some(args) = &extraction.arguments {
                    for (_, extractions) in args {
                        for extraction in extractions {
                            if top_level_extractions.contains(&extraction) {
                                top_level_extractions.remove(&extraction);
                            }
                        }
                    }
                }
            }
            CompactExtraction::Vec(
                top_level_extractions
                    .into_iter()
                    .map(|x| x.into())
                    .collect(),
            )
        }
    }
}

#[test]
fn test_compactification_1() {
    use std::fs::read_to_string;
    let contents = read_to_string("./tests/data/compactification_input_1.json").unwrap();
    let extractions: Vec<Extraction> = serde_json::from_str(&contents).unwrap();
    let compact_extractions = CompactExtraction::from(extractions);
    println!(
        "{}",
        serde_json::to_string_pretty(&compact_extractions).unwrap()
    );
    assert_eq!(
        compact_extractions,
        serde_json::from_str::<CompactExtraction>(
            &read_to_string("./tests/data/compactification_output_1.json").unwrap()
        )
        .unwrap()
    )
}
