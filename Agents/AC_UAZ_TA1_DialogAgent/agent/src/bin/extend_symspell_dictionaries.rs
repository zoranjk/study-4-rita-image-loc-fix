use clap::Parser;
use color_eyre;
use color_eyre::eyre::Result;
use nom::{
    bytes::complete::{tag, take_until1},
    character::complete::char,
    combinator::opt,
    sequence::{preceded, separated_pair, tuple},
    IResult,
};
use nom_locate::LocatedSpan;
use std::{collections::BTreeMap, fs, fs::File, io::Write};

type Span<'a> = LocatedSpan<&'a str>;

fn unigram_dict_line(input: Span) -> IResult<Span, (String, u128)> {
    let (s, (word, frequency)) = preceded(
        opt(tag("\u{feff}")), // BOM (https://en.wikipedia.org/wiki/Byte_order_mark)
        separated_pair(take_until1(" "), char(' '), nom::character::complete::u128),
    )(input)?;
    Ok((s, (word.fragment().to_string(), frequency)))
}

fn bigram_dict_line(input: Span) -> IResult<Span, ((String, String), u128)> {
    let (s, (word_1, _, word_2, _, frequency)) = tuple((
        take_until1(" "),
        char(' '),
        take_until1(" "),
        char(' '),
        nom::character::complete::u128,
    ))(input)?;
    Ok((
        s,
        (
            (word_1.fragment().to_string(), word_2.fragment().to_string()),
            frequency,
        ),
    ))
}

/// Command line arguments
#[derive(Parser, Debug)]
pub struct Cli {
    /// Unigram frequency dictionary
    pub unigram_dictionary: String,

    /// Bigram frequency dictionary
    pub bigram_dictionary: String,

    /// File with clean text for extending the existing dictionaries.
    pub custom_data: String,

    /// File to write extended unigram dictionary to
    pub extended_unigram_dictionary: String,

    /// File to write extended bigram dictionary to
    pub extended_bigram_dictionary: String,
}

#[derive(Debug, Default)]
struct UnigramDictionary(BTreeMap<String, u128>);

#[derive(Debug, Default)]
struct BigramDictionary(BTreeMap<(String, String), u128>);

fn extend_dictionaries(
    unigram_dict: &mut UnigramDictionary,
    bigram_dict: &mut BigramDictionary,
    custom_data_file: &str,
    extended_unigram_dictionary: &str,
    extended_bigram_dictionary: &str,
) -> Result<()> {
    let contents = fs::read_to_string(custom_data_file)?;
    for line in contents.lines() {
        // Preprocess
        let processed_line = line
            .to_lowercase()
            .replace("#", " ")
            .replace("\"", " ")
            .replace("\\", " ")
            .replace("/", " ")
            .replace("!", " ")
            .replace("?", " ")
            .replace(",", " ");
        let words: Vec<&str> = processed_line.split_whitespace().collect();

        for (i, word) in words.iter().enumerate() {
            // Extend unigram dictionary
            unigram_dict
                .0
                .entry(word.to_string())
                .and_modify(|count| *count += 1)
                .or_insert(1);

            // Extend bigram dictionary
            if let Some(w) = words.get(i + 1) {
                bigram_dict
                    .0
                    .entry((word.to_string(), w.to_string()))
                    .and_modify(|count| *count += 1)
                    .or_insert(1);
            }
        }
    }

    let mut output_file = File::create(extended_unigram_dictionary)?;
    for (key, value) in &unigram_dict.0 {
        if key.chars().all(|c| c.is_alphabetic() || c == '\'') {
            write!(output_file, "{key} {value}\n")?;
        };
    }
    let mut output_file = File::create(extended_bigram_dictionary)?;
    for ((word_1, word_2), value) in &bigram_dict.0 {
        write!(output_file, "{word_1} {word_2} {value}\n")?;
    }

    Ok(())
}

fn main() -> Result<()> {
    // Initialize the logger from the environment
    pretty_env_logger::init();
    color_eyre::install()?;

    let args = Cli::parse();

    let unigram_dict = fs::read_to_string(args.unigram_dictionary)?;
    let mut unigram_dict = UnigramDictionary(BTreeMap::from_iter(
        unigram_dict
            .lines()
            .map(|line| unigram_dict_line(Span::new(line)).unwrap().1),
    ));

    let bigram_dict = fs::read_to_string(args.bigram_dictionary)?;
    let mut bigram_dict = BigramDictionary(BTreeMap::from_iter(
        bigram_dict
            .lines()
            .map(|line| bigram_dict_line(Span::new(line)).unwrap().1),
    ));

    extend_dictionaries(
        &mut unigram_dict,
        &mut bigram_dict,
        &args.custom_data,
        &args.extended_unigram_dictionary,
        &args.extended_bigram_dictionary,
    )?;

    Ok(())
}
