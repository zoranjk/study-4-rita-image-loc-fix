//! Program to get a single compact extraction given a text input.

use clap::Parser;
use color_eyre::eyre::Result;
use serde_json as json;
use tomcat::{
    config::Config, event_extraction::get_extractions, messages::nlu::CompactExtraction,
    spelling_correction::SpellChecker,
};

/// Command line arguments
#[derive(Parser, Debug)]
pub struct Cli {
    /// Input text
    pub text: String,

    /// Config file
    #[arg(short, long, default_value_t = String::from("config.yml"))]
    pub config: String,
}

fn main() -> Result<()> {
    color_eyre::install()?;
    let args = Cli::parse();

    let config: Config = confy::load_path(&args.config)
        .unwrap_or_else(|_| panic!("Unable to load config file {}!", &args.config));

    let spellchecker = SpellChecker::new(
        &config.custom_unigram_dict,
        &config.custom_bigram_dict,
        &config.custom_replacement_dict,
        &config.spellchecker_ignore_list,
        &config.boosted_phrase_list,
    )?;

    let corrected = spellchecker.correct(&args.text);
    println!("Corrected spelling: {}", &corrected);
    let extractions = get_extractions(&corrected, &config.event_extractor_url)?;
    let message = CompactExtraction::from(extractions);
    println!("{}", json::to_string_pretty(&message)?);
    Ok(())
}
