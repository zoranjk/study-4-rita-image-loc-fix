use crate::{
    config::Config,
    event_extraction::{get_extraction_schemas, get_extractions},
    knowledge_base::KnowledgeBase,
    messages::{
        common::{Header, Msg},
        get_message::get_message,
        heartbeat::{HeartbeatData, HeartbeatMessage},
        nlu::NLUMessage,
        trial::TrialMessage,
        versioninfo::VersionInfoMessage,
    },
    mqtt_client::MqttClient,
    spelling_correction::SpellChecker,
};
use async_std::{
    sync::{Arc, Mutex},
    task,
};
use color_eyre::eyre::Result;

use log::{debug, info, warn};
use paho_mqtt as mqtt;
use serde_json as json;
use std::time::Duration;

pub struct MqttAgent {
    mqtt_client: MqttClient,
    pub config: Config,
    pub kb: Arc<Mutex<KnowledgeBase>>,
    spellchecker: SpellChecker,
}

impl MqttAgent {
    /// Construct a new MqttAgent struct.
    pub fn new(config: Config, kb: Arc<Mutex<KnowledgeBase>>) -> Result<Self> {
        let mqtt_client = MqttClient::new(&config.mqtt.host, &config.mqtt.port, &config.client_id);
        let spellchecker = SpellChecker::new(
            &config.custom_unigram_dict,
            &config.custom_bigram_dict,
            &config.custom_replacement_dict,
            &config.spellchecker_ignore_list,
            &config.boosted_phrase_list
        )?;

        let agent = Self {
            config,
            kb,
            mqtt_client,
            spellchecker,
        };
        agent.connect()?;
        Ok(agent)
    }

    /// Process chat message.
    async fn process_chat_message(&self, payload: &str) -> Result<()> {
        let message: json::Value = json::from_str(payload).unwrap();
        let sender = message["data"]["sender_id"].as_str().unwrap();
        let text = message["data"]["message"].as_str().unwrap();
        let trial_id = message["msg"]["trial_id"].as_str().unwrap();
        let experiment_id = message["msg"]["experiment_id"].as_str().unwrap();
        let utterance_id = message["data"]["message_id"].as_str().unwrap();

        // Get the callsign of the sender.
        let callsign = match self
            .kb
            .lock()
            .await
            .callsign_mapping
            .get(sender) {
                Some(c) => c.clone(),
                None => {
                    warn!("Callsign not found for sender {sender}!");
                    sender.to_string()
                }
            };

        // Get corrected spelling
        let corrected_text = self.spellchecker.correct(text);

        let extractions = match get_extractions(&corrected_text, &self.config.event_extractor_url) {
            Ok(events) => Some(events),
            Err(e) => {
                warn!("Unable to get events! Error: {:?}", e);
                None
            }
        };

        let compact_message = NLUMessage::new(
            text.to_string(),
            Some(corrected_text),
            callsign.to_string(),
            extractions,
            trial_id.to_string(),
            experiment_id.to_string(),
            Some(utterance_id.to_string()),
        );

        let publish_topic = format!("agent/{}", self.config.client_id.as_str());

        let payload = json::to_string(&compact_message)?;
        self.publish(&payload, &publish_topic)?;

        Ok(())
    }

    /// Publish heartbeat
    pub async fn publish_heartbeat(&self) -> Result<()> {
        let publish_topic =
            "status".to_owned() + "/" + self.config.client_id.as_str() + "/heartbeats";

        let trial_id = self.kb.lock().await.trial_id.clone();
        let experiment_id = self.kb.lock().await.experiment_id.clone();

        let heartbeat = HeartbeatMessage {
            header: Header {
                message_type: "status".to_string(),
                version: "0.1".to_string(),
                ..Default::default()
            },
            msg: Msg {
                trial_id,
                experiment_id,
                source: self.config.client_id.clone(),
                sub_type: "heartbeat".to_string(),
                version: "0.3".to_string(),
                ..Default::default()
            },
            data: HeartbeatData::default(),
        };

        let payload = json::to_string(&heartbeat)?;
        self.publish(&payload, &publish_topic)?;
        Ok(())
    }

    /// Publish heartbeats
    pub async fn publish_heartbeats(&self) -> Result<()> {
        loop {
            self.publish_heartbeat().await?;
            task::sleep(Duration::from_secs(10)).await;
        }
    }

    pub fn publish(&self, payload: &str, topic: &str) -> Result<()> {
        let message = mqtt::Message::new(topic, payload, 2);
        debug!("Publishing message: {}", &message);
        self.mqtt_client.client.publish(message)?;
        Ok(())
    }

    /// Process stage transition messages.
    async fn process_stage_transition_message(&self, payload: &str) {
        let message: json::Value = json::from_str(&payload).expect(&format!(
            "Unable to interpret payload as JSON: {}",
            &payload
        ));
        let stage = message["data"]["mission_stage"].to_string();
        self.kb.lock().await.stage = stage;
    }

    /// Publish versioninfo message
    fn publish_versioninfo(
        &self,
        trial_id: Option<String>,
        experiment_id: Option<String>,
    ) -> Result<()> {
        let publish_topic =
            "agent".to_owned() + "/" + self.config.client_id.as_str() + "/versioninfo";

        let _schemas = get_extraction_schemas(&self.config.event_extractor_url);
        let version_info_message =
            VersionInfoMessage::from_config(&self.config, trial_id, experiment_id);
        let payload = json::to_string(&version_info_message)?;
        self.publish(&payload, &publish_topic)?;
        Ok(())
    }

    /// Process trial messages.
    async fn process_trial_message(&self, message: mqtt::Message) -> Result<()> {
        let message: TrialMessage = get_message(&message);
        if let "start" = message.msg.sub_type.as_str() {
            // Construct a mapping between callsigns and participant IDs.
            for client in message.data.client_info {
                self.kb
                    .lock()
                    .await
                    .callsign_mapping
                    .insert(client.participant_id.unwrap(), client.callsign.unwrap());
            }

            debug!("Callsign mapping: {:?}", &self.kb.lock().await.callsign_mapping);

            // Update trial ID
            self.kb.lock().await.trial_id = message.msg.trial_id.clone();

            // Update experiment ID
            self.kb.lock().await.experiment_id = message.msg.experiment_id.clone();

            self.publish_versioninfo(
                message.msg.trial_id.clone(),
                message.msg.experiment_id.clone(),
            )?;
        }

        // Publish a versioninfo message when the trial ends so that the data contains a record
        // that the agent was running when the trial ended.
        if let "stop" = message.msg.sub_type.as_str() {
            // Clear the callsign mapping
            self.kb.lock().await.callsign_mapping.clear();

            self.publish_versioninfo(message.msg.trial_id, message.msg.experiment_id)?;

            // Clear trial ID
            self.kb.lock().await.trial_id = None;

            // Clear experiment ID
            self.kb.lock().await.experiment_id = None;
        }
        Ok(())
    }

    async fn process_message(&self, msg: mqtt::Message) -> Result<()> {
        match msg.topic() {
            "communication/chat" => self.process_chat_message(&msg.payload_str()).await?,
            "observations/events/stage_transition" => {
                self.process_stage_transition_message(&msg.payload_str())
                    .await
            }
            "trial" => self.process_trial_message(msg).await?,
            _ => {
                warn!("Unhandled topic: {}", msg.topic());
            }
        }
        Ok(())
    }

    pub fn connect(&self) -> Result<(), mqtt::Error> {
        let _ = &self.mqtt_client.connect();
        let _ = &self
            .mqtt_client
            .subscribe(self.config.subscribe_topics.clone());
        info!("Connected to message bus!");
        Ok::<(), mqtt::Error>(())
    }

    /// Process messages
    pub async fn process_messages(&self) -> Result<()> {
        info!("Processing messages");
        // Note that we are not providing a way to cleanly shut down and
        // disconnect. Therefore, when you kill this app (with a ^C or
        // whatever) the server will get an unexpected drop and then
        // should emit the LWT message.

        for msg in self.mqtt_client.stream.iter() {
            if let Some(msg) = msg {
                self.process_message(msg).await?;
            } else if self.mqtt_client.client.is_connected() {
                break;
            }
        }
        // TODO: Implement automatic reconnection if the connection is lost.
        Ok(())
    }
}

#[test]
fn process_extraction_schemas() {
    let contents = std::fs::read_to_string("./tests/data/extraction_schemas.json")
        .expect("Unable to read file!");
    let schemas: json::Value = json::from_str(&contents).unwrap();
    let schemas = json::to_string_pretty(&schemas).unwrap();
    //println!("{}", &schemas);
}
