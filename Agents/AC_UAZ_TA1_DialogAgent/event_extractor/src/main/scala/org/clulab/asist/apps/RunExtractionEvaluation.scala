package org.clulab.asist.apps

import ai.lum.common.ConfigFactory
import org.clulab.asist.`export`.ExtractionEvaluation
import org.clulab.asist.extraction.TomcatRuleEngine
import org.clulab.odin.Mention
import org.clulab.utils.LocalFileUtils
import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.slf4j.LoggerFactory

import scala.io.Source
import scala.util.Random


object RunExtractionEvaluation extends App {

  implicit val formats: Formats = DefaultFormats
  private lazy val logger = LoggerFactory.getLogger(this.getClass())
  val config = ConfigFactory.load()
  val inputDir = config.getString("DialogAgent.inputDir")
  val reprocessedOutputDir = config.getString("DialogAgent.outputDir")
  val maxMentions = config.getInt("apps.eval.maxMentions")
  val maxFiles = config.getInt("apps.eval.maxFiles")

  val ruleEngine = new TomcatRuleEngine(rulepath = Some(config.getString("TomcatRuleEngine.masterRulesPath")))
  val files = LocalFileUtils.getFileNames(inputDir)
  val filesSubset: Seq[String] = Random.shuffle(files).slice(0, maxFiles)
  val mentions = filesSubset.par.flatMap(getMentionsFromFile)

  val outputDir = config.getString("export.ruleAnnotationDir")
  val subset: Seq[Mention] = Random.shuffle(mentions.seq).slice(0, maxMentions)
  ExtractionEvaluation.exportExtractionAnnotationSheets(subset, outputDir)


  def getMentionsFromFile(filename: String): Seq[Mention] = {
    logger.info(s"Loading mentions from file: $filename")
    val bufferedSource = Source.fromFile(filename)

    val mentions: Seq[Mention] = bufferedSource.getLines()
      .map(parse(_))
      .filter(msg => (msg \ "topic").extract[Option[String]].contains("agent/AC_UAZ_TA1_DialogAgent"))
      .map(msg => (msg \ "data" \ "corrected_text").extract[String])
      .flatMap(ruleEngine.extractFrom(_, keepText = true)).toVector

    mentions
  }

}
