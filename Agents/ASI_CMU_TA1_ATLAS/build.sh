#!/bin/bash
source ./settings.env

cache=
OPTIND=1

while getopts c name
do
    case $name in
    c)    cache=1;;
    ?)   printf "Usage: %s: [-c] args\n" $0;;
    esac
done

echo "updating the ASI_CMU_TA1 ATLAS container"

if [ ! -z "$cache" ]; then
    echo "Building with cache..."
    DOCKER_BUILDKIT=1 docker build -t ${DOCKER_IMAGE_NAME_LOWERCASE} -f dockerfile_cache .
else
    echo "Building without cache..."
    docker build -t ${DOCKER_IMAGE_NAME_LOWERCASE} --build-arg CACHE_BREAKER=$(date +%s) .
fi
