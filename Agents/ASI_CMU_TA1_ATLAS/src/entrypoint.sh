#!/bin/bash
echo "Starting ATLAS Entrypoint Script"
echo "  Starting up Redis Server"
redis-server &
sleep 2

echo "  Starting ATLAS"

python3 src/atlas.py src/ConfigFolder/config.json

echo "  Exiting Entrypoint Script"
