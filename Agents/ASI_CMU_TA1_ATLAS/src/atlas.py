# -*- coding: utf-8 -*-
"""
.. module:: atlas
   :platform: Linux, Windows, OSX
   :synopsis: CMU-TA1 ATLAS agent

.. moduleauthor:: CMU-TA1 <danahugh@andrew.cmu.edu>

This file provides the top-level definition of the ATLAS agent, with entry code
to run on the ASIST testbed or from a saved metadata file.
"""

import sys
import os
import json

# Bootstraping and bus components
import MinecraftBridge
import RedisBridge

from BaseAgent import BaseAgent, MissionContext
from BaseAgent.utils import parseCommandLineArguments

# Individual Subcomponents
from InterventionManager import InterventionManager
#from tom.ttom import TToM
#from asist_nlp import NLPManager
from ASIST_PlanSupervisor import PlanSupervisor

# Grab Version Info from Subcomponent repos
from InterventionManager import __version__ as InterventionManagerVersion
from BaseAgent import __version__ as BaseAgentVersion
from ASIST_PlanSupervisor import __version__ as PlanSupervisorVersion

MinecraftBridgeVersion = MinecraftBridge.__version__
RedisBridgeVersion = RedisBridge.__version__

from MinecraftBridge.messages import Trial
from MinecraftBridge.mqtt.parsers import MessageSubtype

from gym_dragon_search.bridge import CBSBridge


class ATLAS(BaseAgent):
    """
    A top-level ATLAS agent class.
    """

    def __init__(self, minecraft_bridge, redis_bridge, config, **kwargs):


        BaseAgent.__init__(self, minecraft_bridge, config, **kwargs)

        self.__version__ = "1.0.6"

        # Store the passed arguments
        self.redis_bridge = redis_bridge

        # Branding with terrible ASC-II art!!!
        self.logger.info(f"{self}:")
        self.logger.info(f"{self}:        ______    --== DARPA ASIST ==--")
        self.logger.info(f"{self}:       //     |   --== CMU-RI  TA1 ==--  version: {self.__version__}")
        self.logger.info(f"{self}:      //  /|  | ___________    ____   _______")
        self.logger.info(f"{self}:     //  /_|  |//__  __// /   //   | // ____/")
        self.logger.info(f"{self}:    //  ____  |  // / // /   // /\\ | \\\\__  \\")
        self.logger.info(f"{self}:   //  /  ||  | // / // /___// ___ | ___// |")
        self.logger.info(f"{self}:  //__/   ||__|//_/ //_____//_/ ||_|//____/")
        self.logger.info(f"{self}:")
        self.logger.info(f"{self}:  Submodule Versions")
        self.logger.info(f"{self}:    MinecraftBridge:      {MinecraftBridgeVersion}")
        self.logger.info(f"{self}:    RedisBridge:          {RedisBridgeVersion}")
        self.logger.info(f"{self}:    BaseAgent:            {BaseAgentVersion}")
        self.logger.info(f"{self}:    InterventionManager:  {InterventionManagerVersion}")
        self.logger.info(f"{self}:    ASIST_PlanSupervisor: {PlanSupervisorVersion}")
        self.logger.info(f"{self}:")


        # Create interfaces to the Minecraft and Redis bridges to allow for 
        # simple callback registration, and create a MissionContext to pass
        # to subcomponents.
        self.context = MissionContext(MinecraftBridge.mqtt.CallbackDecorator(self.minecraft_bridge),
                                      RedisBridge.interfaces.CallbackInterface(self.redis_bridge),
                                      self.participants,
                                      self.semantic_map,
                                      self.trial_info,
                                      self.mission_clock)


###        self.logger.info("%s: Creating TToM", self)
###        tom_config = self.config.get('ttom', {})
###        self.shared_mental_model = TToM(self.context,
###                                        name=tom_config.get('name', 'TToM'),
###                                        intent_model_path=tom_config.get('intent_model_path', 'ConfigFolder/resources/ttom/intent_model.pt'),
###                                        intent_model_2nd_order_path=tom_config.get('intent_model_2nd_order_path', 'ConfigFolder/resources/ttom/intent_model_2nd_order.pt'))

        self.logger.info(f"{self}: Creating InterventionManager")
        self.intervention_manager = InterventionManager(self.context, 
                                                        self.config.get('intervention_manager', {}),
                                                        agent_name=kwargs.get("agent_name", "ASI_CMU_TA1_ATLAS"))

###        self.profiler = InterventionLifecycleProfiler(self.intervention_manager,
###                                                      intervention_filename=config.get('intervention_profile_path', None),
###                                                      followup_filename=config.get('followup_profile_path', None))

        self.logger.info(f'{self}: Creating PlanSupervisor')
        self.plan_supervisor = PlanSupervisor(self.context, self.config.get('plan_supervisor', {}))
        self.plan_supervisor.start()

        # Haven't been able to get this functioning yet...
###        self.logger.info(f'{self}: Creating CBS Planner')
###        self.cbs_planner = CBSBridge(self.context, solution_timeout=60, mission_length=600, recon_phase_length=120)


        self.logger.info(f"{self}: ATLAS Running")




    def __str__(self):
        """
        """

        return "[ATLAS (CMU-TA1)]"


    def run(self):
        """
        """

        self.redis_bridge.start()

        BaseAgent.run(self)

        self.redis_bridge.stop()


    def _onTrialStart(self, message):
        """
        Callback when a trial is started, used to determine if the trial is
        the first or second trial.

        Arguments
        ---------
        message : MinecraftBridge.messages.Trial
            Received trial message
        """

        self.logger.info(f'{self}:  TRIAL STARTING')
        self.logger.info(f'{self}:    Experiment Mission: {message.experiment_mission}')
        self.logger.info(f'{self}:    Participant List:')
        for participant in self.participants:
            self.logger.info(f'{self}:      {participant}')

        # Reset the components
        trial_number=-1   # Gotta remove this from subcomponents
        self.intervention_manager.reset(trial_number=trial_number)
        self.plan_supervisor.reset(trial_number=trial_number)
        self.plan_supervisor.start()

        # Indicate to each component that the trial has started
        self.intervention_manager.on_trial_start()



    def _onTrialStop(self, message):
        """
        Callback when a trial is stopped.

        Arguments
        ---------
        message : MinecraftBridge.messages.Trial
            Received trial message
        """

        self.logger.info(f'{self}:  TRIAL STOPPING')

        # Indicate to each component that the trial has ended
        self.intervention_manager.on_trial_stop()

#        self.plan_supervisor.stop()

#        self.intervention_manager.reset()
#        self.plan_supervier.reset(trial_number=-1)



def get_redis_bridge(config=None):
    """
    Simple helper function for extracting the redis bridge from the config
    file, if it exists
    """

    # Return a dummy redis bridge if no config is provided
    if config is None:
        return RedisBridge.RedisBridge(use_mock_redis_server=True)

    # Return a dummy redis bridge if no "redis_bus" entry is in the config
    if not 'redis_bus' in config:
        return RedisBridge.RedisBridge(use_mock_redis_server=True)

    # Grab the host and port, use 'localhost:6379' by default
    host = config['redis_bus'].get('host', 'localhost')
    port = int(config['redis_bus'].get('port', 6379))

    return RedisBridge.RedisBridge(host=host, port=port)


def add_profiler_arguments(parser):
    """
    Add a command-line argument for setting up the profile filenames
    """

    parser.add_argument('--intervention_profile_path', '-ip', default=None, help='Path to write intervention profile to')
    parser.add_argument('--followup_profile_path', '-fp', default=None, help='Path to write followup profile to')


if __name__ == '__main__':
    """
    Parse command line and config file arguments, create an instance of the
    InterventionManagerAgent, and run it.
    """

    # Parse the command line arguments
    args, config, bridge = parseCommandLineArguments(additional_parser_configuration=add_profiler_arguments)
#    redis_bridge = get_redis_bridge(config)
    redis_bridge = get_redis_bridge()

    config['intervention_profile_path'] = args.intervention_profile_path
    config['followup_profile_path'] = args.followup_profile_path

    # Create the agent, and start it
    atlas = ATLAS(bridge, redis_bridge, config)
    atlas.run()
