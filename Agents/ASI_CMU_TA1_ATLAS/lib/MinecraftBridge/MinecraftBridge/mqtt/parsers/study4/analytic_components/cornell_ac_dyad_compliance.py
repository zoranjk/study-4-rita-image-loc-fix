# -*- coding: utf-8 -*-
"""
.. module:: cornell_ac_dyad_compliance
   :platform: Linux, Windows, OSX
   :synopsis: Parser for Cornell AC messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a parser for Study 4 Cornell AC messages
"""

from .....messages import (
    CornellDyadComplianceRequest,
    CornellDyadComplianceResponse,
    CornellDyadComplianceSummary
)
from ...message_types import MessageType, MessageSubtype
from ...bus_header import BusHeaderParser
from ...message_header import MessageHeaderParser
from ...parser_exceptions import MissingHeaderException

import MinecraftElements

import logging


class CornellDyadComplianceParser:
    """
    A class for parsing Dyad Alignment messages from MQTT bus.

    MQTT Message Fields
    -------------------


    MQTT Blockage Fields
    --------------------

    """

    topic = "agent/ac/dyad_compliance"
    MessageClass = CornellDyadComplianceSummary

    msg_type = MessageType.agent
    msg_subtype = MessageSubtype.AC_Dyad_compliance
    alternatives = []

    # Class-level logger
    logger = logging.getLogger("CornellDyadComplianceParser")


    @classmethod
    def parse(cls, json_message):
        """
        Convert the a Python dictionary format of the message to an appropriate
        Cornell AC message

        Arguments
        ---------
        json_message : dictionary
            Dictionary representation of the message received from the MQTT bus
        """

        # Make sure that there's a "header" and "msg" field in the message
        if not "header" in json_message.keys() or not "msg" in json_message.keys():
            raise MissingHeaderException(json_message)

        # Parse the header and message header
        busHeader = BusHeaderParser.parse(json_message["header"])
        messageHeader = MessageHeaderParser.parse(json_message["msg"])

        # Check to see if this parser can handle this message type, if not, 
        # then return None
        if busHeader.message_type != MessageType.agent:
            return None
        if messageHeader.sub_type != MessageSubtype.AC_Dyad_compliance:
            return None

        # Parse the data
        data = json_message["data"]

        # Need to infer the message type based on contents of the data field
        if "responder" in data:
            message = CornellDyadComplianceResponse(**data)
        elif "complied_dyad_raw" in data:
            message = CornellDyadComplianceSummary(**data)
        else:
            message = CornellDyadComplianceRequest(**data)


        message.addHeader("header", busHeader)
        message.addHeader("msg", messageHeader)

        return message