# -*- coding: utf-8 -*-
"""
.. module:: MinecraftBridge.mqtt.parsers.study4
   :platform: Linux, Windows, OSX
   :synopsis: Parsers for converting JSON messages to Message instances

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>
"""