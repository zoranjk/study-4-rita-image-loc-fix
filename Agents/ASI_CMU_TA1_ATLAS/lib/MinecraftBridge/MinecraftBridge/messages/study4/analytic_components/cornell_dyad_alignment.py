# -*- coding: utf-8 -*-
"""
.. module:: cornell_dyad_alignment
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Dyad Alignment messages

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Study 4 Dyad Alignment messages.
"""

import json

from ...message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ...base_message import BaseMessage


class CornellDyadAlignmentTrial(BaseMessage):
    """
    A class encapsulating Study 4 Dyad Alignment messages.

    Attributes
    ----------
    Alpha_goal : str
        Alpha's goal
    Bravo_goal : str
        Bravo's goal
    Delta_goal : str
        Delta's goal
    """


    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._Alpha_goal = kwargs.get("Alpha_goal", "")
        self._Bravo_goal = kwargs.get("Bravo_goal", "")
        self._Delta_goal = kwargs.get("Delta_goal", "")

    @property
    def Alpha_goal(self):
        return self._Alpha_goal

    @property
    def Bravo_goal(self):
        return self._Bravo_goal

    @property
    def Delta_goal(self):
        return self._Delta_goal
    

    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'CornellDyadAlignmentTrial')
        """

        return self.__class__.__name__


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict['data']['Alpha_goal'] = self.Alpha_goal
        jsonDict['data']['Bravo_goal'] = self.Bravo_goal
        jsonDict['data']['Delta_goal'] = self.Delta_goal

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())




class CornellDyadAlignmentStage(BaseMessage):
    """
    A class encapsulating Study 4 Dyad Alignment messages.

    Attributes
    ----------
    stage_start : int
        Second at which the given stage started
    stage_end : int
        Seconds at which the given stage ended
    alignment_alpha_bravo_stage : float
    alignment_bravo_delta_stage : float
    alignment_alpha_delta_stage : float
    alignment_alpha_bravo_delta_stage : float
    """

    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._stage_start = kwargs.get("stage_start", -1)
        self._stage_end = kwargs.get("stage_end", -1)
        self._alignment_alpha_bravo_stage = kwargs.get("alignment_alpha_bravo_stage", -1.0)
        self._alignment_bravo_delta_stage = kwargs.get("alignment_bravo_delta_stage", -1.0)
        self._alignment_alpha_delta_stage = kwargs.get("alignment_alpha_delta_stage", -1.0)
        self._alignment_alpha_bravo_delta_stage = kwargs.get("alignment_alpha_bravo_delta_stage", -1.0)


        @property
        def stage_start(self):
            return self._stage_start

        @property
        def stage_end(self):
            return self._stage_end

        @property
        def alignment_alpha_bravo_stage(self):
            return self._alignment_alpha_bravo_stage

        @property
        def alignment_bravo_delta_stage(self):
            return self._alignment_bravo_delta_stage

        @property
        def alignment_alpha_delta_stage(self):
            return self._alignment_alpha_delta_stage

        @property
        def alignment_alpha_bravo_delta_stage(self):
            return self._alignment_alpha_bravo_delta_stage


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'CornellDyadAlignmentTrial')
        """

        return self.__class__.__name__


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["stage_start"] = self.stage_start
        jsonDict["data"]["stage_end"] = self.stage_end
        jsonDict["data"]["alignment_alpha_bravo_stage"] = self.alignment_alpha_bravo_stage
        jsonDict["data"]["alignment_bravo_delta_stage"] = self.alignment_bravo_delta_stage
        jsonDict["data"]["alignment_alpha_delta_stage"] = self.alignment_alpha_delta_stage
        jsonDict["data"]["alignment_alpha_bravo_delta_stage"] = self.alignment_alpha_bravo_delta_stage

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())



class CornellDyadAlignmentFinal(BaseMessage):
    """
    A class encapsulating Study 4 Dyad Alignment messages.

    Attributes
    ----------
    stage_start : int
        Second at which the given stage started
    stage_end : int
        Seconds at which the given stage ended
    alignment_alpha_bravo_stage : float
    alignment_bravo_delta_stage : float
    alignment_alpha_delta_stage : float
    alignment_alpha_bravo_delta_stage : float
    alignment_alpha_bravo_final : float
    alignment_bravo_delta_final : float
    alignment_alpha_delta_final : float
    alignment_alpha_bravo_delta_final : float    
    """

    def __init__(self, **kwargs):
        """
        """

        BaseMessage.__init__(self, **kwargs)

        self._stage_start = kwargs.get("stage_start", -1)
        self._stage_end = kwargs.get("stage_end", -1)
        self._alignment_alpha_bravo_stage = kwargs.get("alignment_alpha_bravo_stage", -1.0)
        self._alignment_bravo_delta_stage = kwargs.get("alignment_bravo_delta_stage", -1.0)
        self._alignment_alpha_delta_stage = kwargs.get("alignment_alpha_delta_stage", -1.0)
        self._alignment_alpha_bravo_delta_stage = kwargs.get("alignment_alpha_bravo_delta_stage", -1.0)
        self._alignment_alpha_bravo_final = kwargs.get("alignment_alpha_bravo_final", -1.0)
        self._alignment_bravo_delta_final = kwargs.get("alignment_bravo_delta_final", -1.0)
        self._alignment_alpha_delta_final = kwargs.get("alignment_alpha_delta_final", -1.0)
        self._alignment_alpha_bravo_delta_final = kwargs.get("alignment_alpha_bravo_delta_final", -1.0)

        @property
        def stage_start(self):
            return self._stage_start

        @property
        def stage_end(self):
            return self._stage_end

        @property
        def alignment_alpha_bravo_stage(self):
            return self._alignment_alpha_bravo_stage

        @property
        def alignment_bravo_delta_stage(self):
            return self._alignment_bravo_delta_stage

        @property
        def alignment_alpha_delta_stage(self):
            return self._alignment_alpha_delta_stage

        @property
        def alignment_alpha_bravo_delta_stage(self):
            return self._alignment_alpha_bravo_delta_stage

        @property
        def alignment_alpha_bravo_final(self):
            return self._alignment_alpha_bravo_final

        @property
        def alignment_bravo_delta_final(self):
            return self._alignment_bravo_delta_final

        @property
        def alignment_alpha_delta_final(self):
            return self._alignment_alpha_delta_final

        @property
        def alignment_alpha_bravo_delta_final(self):
            return self._alignment_alpha_bravo_delta_final


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'CornellDyadAlignmentTrial')
        """

        return self.__class__.__name__


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the ItemUsedEvent.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["stage_start"] = self.stage_start
        jsonDict["data"]["stage_end"] = self.stage_end
        jsonDict["data"]["alignment_alpha_bravo_stage"] = self.alignment_alpha_bravo_stage
        jsonDict["data"]["alignment_bravo_delta_stage"] = self.alignment_bravo_delta_stage
        jsonDict["data"]["alignment_alpha_delta_stage"] = self.alignment_alpha_delta_stage
        jsonDict["data"]["alignment_alpha_bravo_delta_stage"] = self.alignment_alpha_bravo_delta_stage
        jsonDict["data"]["alignment_alpha_bravo_final"] = self.alignment_alpha_bravo_final
        jsonDict["data"]["alignment_bravo_delta_final"] = self.alignment_bravo_delta_final
        jsonDict["data"]["alignment_alpha_delta_final"] = self.alignment_alpha_delta_final
        jsonDict["data"]["alignment_alpha_bravo_delta_final"] = self.alignment_alpha_bravo_delta_final

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            ItemUsedEvent message.
        """

        return json.dumps(self.toDict())