# -*- coding: utf-8 -*-
"""
.. module:: player_state
   :platform: Linux, Windows, OSX
   :synopsis: Message class encapsulating Player State information

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a class encapsulating Player State messages.
"""


import json

import ciso8601

from ..message_exceptions import (
    MalformedMessageCreationException, 
    MissingMessageArgumentException, 
    ImmutableAttributeException
)
from ..base_message import BaseMessage

class PlayerState(BaseMessage):
    """
    A class encapsulating PlayerState messages.

    Attributes
    ----------
    participant_id: string
        Unique identifier of participant (e.g. "P000420")
    observation_number : int
        Sequence number of the observation, since beginning of trial run
    position : tuple of floats
        (x,y,z) position of the entity
    x : float
        x position of the entity (alias of `position[0]`)
    y : float
        y position of the entity (alias of `position[1]`)
    z : float
        z position of the entity (alias of `position[2]`)
    orientation : tuple of floats
        (pitch, yaw) of the entity
    pitch : float
        Pitch of the entity (alias of `orientation[0]`)
    yaw : float
        Yaw of the entity (alias of `orientation[1]`)
    velocity : tuple of floats
        (x,y,z) of the velocity of the entity
    motion_x : float
        x value of the entity's velocity (alias of `velocity[0]`)
    motion_y : float
        y value of the entity's velocity (alias of `velocity[1]`)
    motion_z : float
        z value of the entity's velocity (alias of `velocity[2]`)
    """

    def __init__(self, **kwargs):
        """
        participant_id : string
            Unique identifier of the participant
        position : tuple of floats
        x : float
        y : float
        z : float
            (x,y,z) position of the player
        orientation : tuple of floats
        pitch : float
        yaw : float
            (pitch, float) facing of the player
        velocity : tuple of floats
        motion_x : float
        motion_y : float
        motion_z : float
            (x,y,z) velocity of the player
        observation_number : int
            Sequence number of the observation
        """

        BaseMessage.__init__(self, **kwargs)

        # Get the participant ID and playername.  Note that, depending on the
        # provided keyword arguments, these may be one and the same.
        self._participant_id = kwargs.get('participant_id',
                               kwargs.get('playername', None))
        if self._participant_id is None:
            raise MissingMessageArgumentException(str(self),
                                                  'participant_id') from None

        self._observation_number = int(kwargs.get("observation_number", 
                                       kwargs.get("obs_id", -1)))

        # Get the position of the player: try `position` first, followed by
        # `x`, `y`, and `z`
        position = kwargs.get('position', None)

        if position is None:
            try:
                position = (kwargs['x'], kwargs['y'], kwargs['z'])
            except KeyError:
                raise MissingMessageArgumentException(str(self),
                                                      'position') from None

        # Position needs to be able to be coerced into a tuple of floats. Raise
        # an exception if not possible
        try:
            self._position = tuple([float(x) for x in position][:3])
        except:
            raise MalformedMessageCreationException(str(self), 'position',
                                                    position) from None


        # Get the velocity of the player: try `velocity` first, followed by
        # `motion_x`, `motion_y`, and `motionz`
        velocity = kwargs.get('velocity', None)

        if velocity is None:
            try:
                velocity = (kwargs['motion_x'], 
                            kwargs['motion_y'], 
                            kwargs['motion_z'])
            except KeyError:
                raise MissingMessageArgumentException(str(self),
                                                      'velocity') from None

        # Velocity needs to be able to be coerced into a tuple of floats. Raise
        # an exception if not possible
        try:
            self._velocity = tuple([float(x) for x in velocity][:3])
        except:
            raise MalformedMessageCreationException(str(self), 'velocity',
                                                    velocity) from None


        # Get the orientation of the player: try `orientation` first, followed by
        # `pitch` and `yaw`
        orientation = kwargs.get('orientation', None)

        if orientation is None:
            try:
                orientation = (kwargs['pitch'], kwargs['yaw'])
            except KeyError:
                raise MissingMessageArgumentException(str(self),
                                                      'orientation') from None

        # Orientation needs to be able to be coerced into a tuple of floats.
        # Raise an exception if not possible
        try:
            self._orientation = tuple([float(x) for x in orientation][:2])
        except:
            raise MalformedMessageCreationException(str(self), 'orientation', 
                                                    orientation) from None


    def __str__(self):
        """
        String representation of the message.

        Returns
        -------
        string
            Class name of the message (i.e., 'PlayerState')
        """

        return self.__class__.__name__


    @property
    def participant_id(self):
        """
        Get the unique identifier for the participant.  

        Attempting to set `participant_id` raises an `ImmutableAttributeException`.
        """

        return self._participant_id

    @participant_id.setter
    def participant_id(self, name):
        raise ImmutableAttributeException(str(self), "participant_id")

    @property
    def observation_number(self):
        """
        Get the sequence number of the observation.  

        Attempting to set `observation_number` raises an `ImmutableAttributeException`.
        """

        return self._observation_number

    @observation_number.setter
    def observation_number(self, observation_number):
        raise ImmutableAttributeException(str(self), "observation_number")

    @property
    def obs_id(self):
        return self._observation_number


    @property
    def position(self):
        """
        Get the position of the entity.  

        Attempting to set `position` raises an `ImmutableAttributeException`.
        """

        return self._position

    @position.setter
    def position(self, position):
        raise ImmutableAttributeException(str(self), "position")


    @property
    def x(self):
        """
        Alias of  `position[0]`.  
        """

        return self._position[0]

    @x.setter
    def x(self, x):
        raise ImmutableAttributeException(str(self), "x")


    @property
    def y(self):
        """
        Alias of  `position[1]`.  
        """

        return self._position[1]

    @y.setter
    def y(self, y):
        raise ImmutableAttributeException(str(self), "y")


    @property
    def z(self):
        """
        Alias of  `position[2]`.  
        """

        return self._position[2]

    @z.setter
    def z(self, z):
        raise ImmutableAttributeException(str(self), "z")        


    @property
    def orientation(self):
        """
        Get the orientation of the entity.  

        Attempting to set `orientation` raises an `ImmutableAttributeException`.
        """

        return self._orientation

    @orientation.setter
    def orientation(self, orientation):
        raise ImmutableAttributeException(str(self), "orientation")


    @property
    def pitch(self):
        """
        Alias of  `orientation[0]`.  
        """

        return self._orientation[0]

    @pitch.setter
    def pitch(self, angle):
        raise ImmutableAttributeException(str(self), "pitch")


    @property
    def yaw(self):
        """
        Alias of  `orientation[1]`.  
        """

        return self._orientation[1]

    @yaw.setter
    def yaw(self, angle):
        raise ImmutableAttributeException(str(self), "yaw")


    @property
    def velocity(self):
        """
        Get the velocity of the entity.  

        Attempting to set `velocity` raises an `ImmutableAttributeException`.
        """

        return self._velocity

    @velocity.setter
    def velocity(self, velocity):
        raise ImmutableAttributeException(str(self), "velocity")


    @property
    def motion_x(self):
        """
        Alias of  `velocity[0]`.  
        """

        return self._velocity[0]

    @motion_x.setter
    def motion_x(self, velocity):
        raise ImmutableAttributeException(str(self), "motion_x")


    @property
    def motion_y(self):
        """
        Alias of  `velocity[1]`.  
        """

        return self._velocity[1]

    @motion_y.setter
    def motion_y(self, velcoity):
        raise ImmutableAttributeException(str(self), "motion_y")


    @property
    def motion_z(self):
        """
        Alias of  `velocity[2]`.  
        """

        return self._velocity[2]

    @motion_z.setter
    def motion_z(self, velocity):
        raise ImmutableAttributeException(str(self), "motion_z")


    def toDict(self):
        """
        Generates a dictionary representation of the message.  Message
        information is contained in a dictionary under the key "data".
        Additional named headers may also be present.

        Returns
        -------
        dict
            A dictionary representation of the PlayerState.
        """

        jsonDict = BaseMessage.toDict(self)

        # Check to see if a "data" is in the dictionary, and add if not
        # Note that headers should have been added in jsonDict, as well as
        # common message data.
        if not "data" in jsonDict:
            jsonDict["data"] = {}

        # Add the message data
        jsonDict["data"]["obs_id"] = self.observation_number
        jsonDict["data"]["participant_id"] = self.participant_id
        jsonDict["data"]["yaw"] = self.yaw
        jsonDict["data"]["x"] = self.x
        jsonDict["data"]["y"] = self.y
        jsonDict["data"]["z"] = self.z
        jsonDict["data"]["pitch"] = self.pitch
        jsonDict["data"]["motion_x"] = self.motion_x
        jsonDict["data"]["motion_y"] = self.motion_y
        jsonDict["data"]["motion_z"] = self.motion_z

        return jsonDict


    def toJson(self):
        """
        Generates a JSON representation of the message.  Message information is
        contained in a JSON object under the key "data".  Additional named
        headers may also be present.

        Returns
        -------
        string
            A JSON string mapping header names to a JSON representation of the
            PlayerState message.
        """

        return json.dumps(self.toDict())
