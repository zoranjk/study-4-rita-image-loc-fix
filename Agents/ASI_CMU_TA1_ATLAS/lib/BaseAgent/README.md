# BaseAgent



## Installation

The BaseAgent package can be installed via `pip`.  A basic install can be done from the root folder of the package with the following command:

    pip install --user -e .

To install with all ASIST requirements:

    pip install --user -e .[all]

## ASIST Requirements

This package depends on the following repo:
- [`MinecraftBridge`](https://gitlab.com/cmu_asist/MinecraftBridge) (version >= 1.3.7)
