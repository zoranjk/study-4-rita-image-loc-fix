# -*- coding: utf-8 -*-
"""
.. module:: InterventionManager.followup
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a base Followup class
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines a base Followup class to encapsulate functionality common to all 
intervention followups.  As with Interventions, this class maintains the state
of the Followup and automates the interaction between the followup and the
intervention manager.
"""

import enum
from ..base import BaseComponent

###from collections import namedtuple



class Followup(BaseComponent):
	"""
	Definition of an abstract Followup class.  Individual instances of
	Followups are associated with an individual Intervention that has typically
	been resolved.  All concrete Followups should subclass this Followup.
	Generally, Followups are created by intervention Resolutions, and passed to
	the InterventionManager through the `initiateFollowup` method.

	Attributes
	----------
	intervention : Intervention
		The intervention that is being followed up on
	state : Followup.State
		State of the followup
	target_participants : string or None
		participant_id of the participant that this followup is monitoring.
		"TEAM" is used to indicate that the followup is monitoring the team as
		a whole (e.g., for calculating team compliance from individual
		compliance); None indicates that the followup is not associated with
		any individual or the team
	participant_complied : boolean or None
		Indication of whether the player complied to the intervention.  If the
		followup has not been completed, this takes the value of `None`.
    evidence : list
        List of Evidence instances containing information relevant to the 
        decision making process of the intervention.


	Methods
	-------
	link_followup
		Add a followup instance to the set of followups this followup may need
		to communicate with
	"""


	@enum.unique
	class State(enum.Enum):
		"""
		Enumeration of possible Followup states.  The semantics of each state
		are defined as

		INITIATED: The Followup has been created, but the manager has not 
		           connected the Followup to the internal or external buses
		ACTIVE:    The Followup is connected to the internal and external buses
		           and is receiving information
		COMPLETE:  The Followup has completed, and has been disconnected from
		           information streams.
		"""

		INITIATED = enum.auto()
		ACTIVE = enum.auto()
		COMPLETE = enum.auto()


	def __init__(self, intervention, manager, **kwargs):
		"""
		Arguments
		---------
		intervention : Intervention
			Intervention that is being followed up on
		manager : InterventionManager
			Instance of the InterventionManager managing the followup

		Keyword Arguments
		-----------------
		target_participant : string
			participant_id of the player this followup is monitoring (if any).
		"""

		BaseComponent.__init__(self)

		self.intervention = intervention
		self.manager = manager

		self.state = Followup.State.INITIATED

		# Identify the player (by participant_id) that this followup is 
		# monitoring, and indicate if the player has complied to the 
		# intervention.  This takesthe value of True, False, or None.  
		# None indicates that the followup has not been completed yet.
		self._target_participant = kwargs.get('target_participant', None)
		self._participant_complied = None

		self._state_entry_times = { s: Followup.Time(-1, (-1,-1))
		                            for s in Followup.State }

		# Evidence stores what information is relevant to the decision of the
        # intervention
		self._evidence = []

		self._linked_followups = set()


	@property
	def target_participant(self):
		"""
		Participnat id of the player this followup is observing.  If the 
		followup involves the team, this value should take on the value of 
		"TEAM".  If the followup is not monitoring anyone specifically, it
		should take on the value of None
		"""

		return self._target_participant

	@property
	def target_player(self):
		"""
		Alias of `target_participant`
		"""

		return self._target_participant
	


	@property
	def participant_complied(self):
		"""
		Returns whether or not the player complied to the intervention advice
		(True or False), or `None` if the followup was not completed, or the 
		intervention was not presented
		"""

		return self._participant_complied


	@participant_complied.setter
	def participant_complied(self, complied):
		"""
		Indicate whether the player complied or not.  This value can only be
		set once, and must be set to either `True` or `False`.

		Arguments
		---------
		complied : boolean
			True if the player complied to the intervention, False otherwise
		"""

		# Check if the argument is a boolean
		if complied not in {True, False}:
			self.logger.warning("%s:  Trying to set `participant_complied` to non-boolean value: %s", self, str(complied))
			return

		# Can only set complied if it's currently `None`
		if self._participant_complied is not None:
			self.logger.warning("%s:  `participant_complied` has already been set", self)
			return

		self._participant_complied = complied


	@property
	def player_complied(self):
		"""
		Alias of `participant_complied`
		"""

		return self._participant_complied


	@player_complied.setter
	def player_complied(self, complied):
		self.participant_complied = complied


	@property
	def parent(self):
		"""
		Returns the component that was responsible for this Followup, i.e., 
		the intervention
		"""

		return self.intervention


	@property
	def state_entry_times(self):
		"""
		Get a dictionary of times that this followup entered into the each
		state.  If the followup never entered the state, the dictionary 
		contains a defaule value of  -1 for the `elapsed_milliseconds` field
		and (-1,-1) for the `mission_timer` field of the return value.

		Returns
		-------
		Dictionary mapping Followup.State to Followup.Time
		"""		

		return self._state_entry_times


	@property
	def evidence(self):
		return self._evidence


	def addEvidence(self, evidence):
		"""
		Add a piece of evidence (e.g., a message, ToM state, etc.) relevant to 
		this intervention.  The evidence is stamped with the current elapsed
		milliseconds.
		Arguments
		---------
		evidence : Evidence
		    Evidence to add to this intervention
		"""

		# Set the time, if not currently provided, to the closest time from the
		# mission clock
		if evidence.time is None:
		    evidence.time = self.manager.mission_clock.elapsed_milliseconds
		
		self._evidence.append(evidence)


	def __str__(self):
		"""
		Provide a string representation of the object.

		Returns
		-------
		string
			The name of the class and current state
		"""

		return "%s:%s" % (self.__class__.__name__, self.state.name)


	def link_followup(self, followup):
		"""
		Add a followup that this followup may need to communicate with during
		its lifecycle

		Arguments
		---------
		followup : Followup
			Followup instance that this instance may communicate with
		"""

		self._linked_followups.add(followup)


	def complete(self):
		"""
		Indicate that the followup is complete.  Informs the manager via the
		`completeFollowup` method to disconnect the followup from the bus(es).
		"""

		self.state = Followup.State.COMPLETE
		self.manager._completeFollowup(self)


	def _setStateEntryTime(self, elapsed_milliseconds, mission_timer):
		"""
		Set the time (in terms of mission time) that this followup has 
		entered its current state.  This value is set by the intervention 
		manager, and should not be called directly by the followup.
		Arguments
		---------
		elapsed_milliseconds : int
		    Number of milliseconds since mission start
		mission_timer : tuple
		    Number of minutes and seconds on the mission timer
		"""

		self._state_entry_times[self.state] = Followup.Time(elapsed_milliseconds, mission_timer)



	def _onInitiated(self):
		"""
		Callback from the InterventionManager when the Followup is Initiated.
		Changes the state of the followup and informs the manager to activate
		it.
		"""

		self.state = Followup.State.ACTIVE
		self.manager._activateFollowup(self)


	def _onActive(self):
		"""
		Callback from the InterventionManager when the Followup is made active
		"""

		pass


	def _onComplete(self):
		"""
		Callback from the InterventionManager when the Followup is completed
		"""

		pass


