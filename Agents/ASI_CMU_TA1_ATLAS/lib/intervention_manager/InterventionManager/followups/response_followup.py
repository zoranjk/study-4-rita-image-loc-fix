# -*- coding: utf-8 -*-
"""
.. module:: InterventionManager.followup.response_followup
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a Followup for intervention responses
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines a Followup subclass to handle participant responses to presented
interventions.  This followup subclass is
 designed to be initiated if an
intervention generates a chat presentation with reponses.  Note that the
class is abstract, and needed to be subclassed to handle responses specific to
each Intervention class.
"""

from .followup import Followup

from MinecraftBridge.messages import InterventionResponse


class ResponseFollowup(Followup):
	"""
	Definition of an abstract ResponseFollowup class.  Individual instances of
	ResponseFollowups are associated with an individual Intervention that has 
	been presented with response options to the participant.

	Usage
	-----
	This class is intended to act as an abstract followup class to handling
	intervention response, allowing 1) the InterventionManager to determine if
	and when a ResponseFollowup should be spawned, and 2) removing the need to
	implement boilerplate code for processing the InterventionResponse message.

	To allow the InterventionManager to automatically spawn ResponseFollowups,
	the class attribute `_InterventionClass` should be set to the class of the
	Intervention generating the response.

	Subclassing this method involves 1) calling the superclass initializer 
	during the __init__ method, 2) calling the `set_response_handler` for each
	response the Intervention can produce, and 3) creating a method for each
	handler.  A simple example is given below::

	```
	class SomeResponseFollowup(ResponseFollowup):

		_InterventionClass = SomeIntervention

		def __init__(self, intervention, manager, participant_id):

			ResponseFollowup.__init__(self, intervention, manager, participant_id)

			self.set_response_handler(0, self.__handler1)
			self.set_response_handler(1, self.__handler2)

		def __handler1(self):
			...

		def __handler2(self):
			...
	```

	Methods
	-------
	set_response_handler
		Set a function to call for a specific response message
	"""

	# Class of the intervention that generates the repsonse.  Subclasses need
	# to override this attribute in order for the manager to understand which
	# Response Followup to instantiate for each class.
	InterventionClass = None


	def __init__(self, intervention, manager, participant_id):
		"""
		Arguments
		---------
		intervention : Intervention
			Intervention that is being monitored for response
		manager : InterventionManager
			Instance of the InterventionManager managing the followup
		participant_id : str
			ID of the participant being monitored for response
		"""

		Followup.__init__(self, intervention, manager)

		# NOTE:  The Followup class has a `target_participant` attribute, which
		#        may be suitable to use.  However, that attribute was added to
		#        handle compliance followups, and would take the value of
		#        "TEAM" for team compliance.  This implies that Individual and
		#        Team Compliance followups should have thier own subclass.
		#        Similarly, participant_complied exists in the base Followup,
		#        which doesn't apply to ResponseFollowups.
		self._participant_id = participant_id

		# Dictionary to map response contents to methods to handle the responses
		self.__response_handlers = {}

		# Indicate that the Followup should listen for InterventionResponse messages
		self.add_minecraft_callback(InterventionResponse, self.__onResponse)


	@property
	def participant_id(self):
		return self._participant_id
	

	def __onResponse(self, message):
		"""
		Callback when an InterventionResponse is received.  When one is
		received, determine if its a response to the intervention being
		monitored.

		Arguments
		---------
		message : InterventionResponse
			Received message
		"""

		self.logger.debug(f'{self}:  Received {message}')

		# Determine if the message is related to the intervention, and do 
		# nothing if not
		if message.intervention_id != self.intervention.id:
			self.logger.debug(f'{self}:    Response ID {message.intervention_id} not equal to Intervention ID {self.intervention.id}')
			return

		# In the case where an intervention was presented to multiple
		# participants, determine if the participant who created this response
		# is the one being monitored
		if message.sender_id != self.participant_id:
			self.logger.debug(f'{self}:    Response {message.intervention_id} sent by {message.sender_id}, not {self.participant_id}')
			return

		# Check to see that the text can be handled by one of the handlers
		if not message.response_index in self.__response_handlers:
			self.logger.warning(f'{self}:  No handler provided for index {message.response_index}')
			return

		# Call the handler, then indicate this followup as complete
		self.__response_handlers[message.response_index]()
		self.complete()


	def set_response_handler(self, response_index, handler):
		"""
		Set a handler method to handle a specific response text.

		Arguments
		---------
		response_index : int
		    Index of the response to handle
		handler : callable
			Method to call when the response is received
		"""

		self.logger.debug(f'{self}:  Setting handler for response "{response_index}" to {handler}')

		if response_text in self.__response_handlers:
			self.logger.debug(f'{self}:    Handler already exists for {response_index}:  Replacing with {handler}')

		self.__response_handlers[response_index] = handler
