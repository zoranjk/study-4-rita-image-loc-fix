# -*- coding: utf-8 -*-
"""
.. module:: participant_updater
   :platform: Linux, Windows, OSX
   :synopsis: Component to update participant position and inventory

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

The ParticipantUpdater updates the locaiton and inventory of a decorated
InterventionParticipant.
"""

from collections import defaultdict

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import (
    PlayerState,
    PlayerInventoryUpdate
)


class ParticipantInfo:
    """
    Simple container for participant information
    """

    def __init__(self, participant_id):
        """
        """

        self.participant_id = participant_id
        self.location = (0,0,0)
        self.velocity = (0,0,0)
        self.orientation = (0,0)
        self.inventory = defaultdict(lambda: 0)    


class ParticipantUpdater(Loggable):
    """
    ParticipantUpdater is a simple component that listens for PlayerState and
    InventoryUpdate messages, and stores updated values in the appropriate
    Participant
    """

    def __init__(self, manager):
        """
        Arguments
        ---------
        manager : InterventionManager
        """

        self._manager = manager


    def reset(self):
        """
        Clear out the position and inventory of each participant
        """

        for participant in self._manager.participants:
            self._manager.participant_info[participant.id] = ParticipantInfo(participant.id)


    def on_trial_start(self):
        """
        Connect the updater to the message bus to start receiving messages.
        """

        self._manager.minecraft_interface.register_callback(PlayerState, self.__onPlayerState)
        self._manager.minecraft_interface.register_callback(PlayerInventoryUpdate, self.__onPlayerInventoryUpdate)


    def on_trial_stop(self):
        """
        Disconnect the updater from the message bus.
        """

        self._manager.minecraft_interface.deregister_callback(self.__onPlayerState)
        self._manager.minecraft_interface.deregister_callback(self.__onPlayerInventoryUpdate)


    def __onPlayerState(self, message):
        """
        Callback when a PlayerState message is received.  Updates the 
        corresponding Participant's location with the received location
        """

        # Make sure the participant is in the manager's participant collection
        if not message.participant_id in self._manager.participants:
            self.logger.warning(f'{self}:  Participant not in particpant collection: {message.participant_id}')
            return

        # Create a new ParticipantInfo instance if one does not exist
        if not message.participant_id in self._manager.participant_info:
            self._manager.participant_info[message.participant_id] = ParticipantInfo(message.participant_id)

        self._manager.participant_info[message.participant_id].location = message.position
        self._manager.participant_info[message.participant_id].velocity = message.velocity
        self._manager.participant_info[message.participant_id].orientation = message.orientation


    def __onPlayerInventoryUpdate(self, message):
        """
        Set the inventory of each player to the inventory in the message
        """

        for p_id, inventory in message.current_inventory.items():

            # Create a new ParticipantInfo instance if one does not exist
            if not p_id in self._manager.participant_info:
                self._manager.participant_info[p_id] = ParticipantInfo(message.p_id)


            for item, count in inventory.items():
                self._manager.participant_info[p_id].inventory[item] = count