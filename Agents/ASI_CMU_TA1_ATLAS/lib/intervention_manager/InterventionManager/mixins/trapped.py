from ..components import InterventionScheduler as Scheduler
from ..utils import neighbors, to_2D

from collections import defaultdict
from copy import deepcopy
from MinecraftBridge.messages import (
    FoVSummary, PlayerState, RubbleDestroyedEvent, RubbleCollapse,
    SemanticMapInitialized, VictimPickedUp, VictimPlaced,
)
from MinecraftElements import Block



class TrappedMixin:
    """
    Mixin that reports when players and victims are trapped.

    This mixin can be used by any component that inherits from BaseComponent
    (i.e., Interventions or Followups).  Note that the __init__ method of the
    BaseComponent must be called prior to calling this mixin's __init__ method.

    Methods
    -------
    is_trapped(loc)
        Return whether or not a location is trapped by walls / rubble.

    Callbacks
    ---------
    _onPlayerTrapped(participant, loc)
        Callback for when a player becomes trapped in a room
    _onVictimTrapped(participant, victim)
        Callback for when a trapped victim is first seen by a player
    """

    # Passable blocks from basemap
    PASSABLE = {None, 'furniture'}


    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        """
        self.__manager = manager
        self.__participants = manager.participants
        self.__semantic_map = manager.semantic_map

        # Set attributes
        self.__map = deepcopy(self.__semantic_map.basemap)
        self.__player_location = defaultdict(type(None)) # indexed by participant ID
        self.__player_trapped_rooms = defaultdict(set) # indexed by participant ID
        self.__seen_trapped_victims = defaultdict(set) # indexed by participant ID
        self.__is_trapped_cache = {} # cache for `is_trapped()` results
        self.__victim_block_types = {b.name for b in Block.victims()}

        # Add Minecraft callbacks
        self.add_minecraft_callback(FoVSummary, self.__onFoV)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(RubbleCollapse, self.__onRubbleCollapse)
        self.add_minecraft_callback(RubbleDestroyedEvent, self.__onRubbleDestroyed)
        self.add_minecraft_callback(SemanticMapInitialized, self.__onSemanticMapInitialized)
        self.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUp)
        self.add_minecraft_callback(VictimPlaced, self.__onVictimPlaced)

        # Schedule checks for trapped players
        self.__scheduler = Scheduler(self)
        self.__scheduler.repeat(self._check_if_players_are_trapped, interval=5)


    def is_trapped(self, loc):
        """
        Return whether or not a location is trapped by walls / rubble,
        determined by whether or not there is a path to a door / opening.

        Arguments
        ---------
        loc : tuple
            (x, z) or (x, y, z) location tuple in absolute Minecraft coordinates
        """

        # Check cache
        loc = tuple(int(c) for c in loc)
        if loc in self.__is_trapped_cache:
            return self.__is_trapped_cache[loc]

        # Get the source room and set of door / opening locations as destinations
        room_id = self.__semantic_map.rooms[loc]
        source = to_2D(loc)
        destinations = {to_2D(loc) for loc in self.__semantic_map.connections[room_id]}
        if not destinations:
            return False

        # Run BFS to determine if a path exists from the source to any destination
        explored = {source}
        queue = [source]
        while queue:
            v = queue.pop(0)
            if v in destinations:
                self.__is_trapped_cache[loc] = False
                return False

            if self.__map[v] in TrappedMixin.PASSABLE:
                for u in neighbors(v):
                    if self.__semantic_map.is_in_bounds(u):# and self.__map[u] in TrappedMixin.PASSABLE:
                        if u not in explored:
                            explored.add(u)
                            queue.append(u)

        self.__is_trapped_cache[loc] = True
        return True


    def _onPlayerTrapped(self, participant, loc):
        """
        Callback for when a player becomes trapped in a room.

        Arguments
        ---------
        participant : BaseAgent.Participant
            The trapped participant
        loc : tuple
            (x, y, z) location of the trapped participant
        """
        pass


    def _onVictimTrapped(self, participant, victim):
        """
        Callback for when a trapped victim is first seen by a player.

        Arguments
        ---------
        participant : BaseAgent.Participant
            The participant seeing the trapped victim for the first time
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'
        """
        pass


    def _check_if_players_are_trapped(self):
        """
        Check whether any players are currently trapped.
        """
        for player_id, location in self.__player_location.items():
            if location is None or not self.__semantic_map.is_in_bounds(location):
                continue

            # Get participant and current room
            participant = self.__participants[player_id]
            room = self.__semantic_map.rooms[location]
            if room in {None, *self.__semantic_map.hallways}:
                continue

            # Check if the player is newly trapped
            if room not in self.__player_trapped_rooms[participant.id]:
                if self.is_trapped(location):
                    self.__player_trapped_rooms[participant.id].add(room)

                    # Check location of the engineer
                    try:
                        engineer = self.__participants['Engineering_Specialist']
                        engineer_room = self.__semantic_map.rooms[self.__player_location[engineer.id]]
                    except:
                        engineer_room = None

                    # Player is considered "trapped" if engineer is not in the same room
                    if room != engineer_room:
                        self._onPlayerTrapped(participant, location)


    def __onFoV(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.FoVSummary
            Instance of the FoV message
        """
        if self.__map is None:
            return

        # Get participant
        participant = self.__participants[message.playername]

        # Update map
        for block in message.blocks:
            block_location = tuple(block['location'])

            # Update internal map if block location is within bounds
            if self.__semantic_map.is_in_bounds(block_location):
                if block['type'] == 'gravel':
                    self.__map[block_location] = 'rubble'
                elif block['type'] in self.__victim_block_types:
                    self.__map[block_location] = 'victim'

        # Look for new trapped victims
        for block in message.blocks:
            if block['type'] in self.__victim_block_types:
                # Check if newly detected
                if block['id'] not in self.__seen_trapped_victims[participant.id]:
                    if self.is_trapped(block['location']):
                        self.__seen_trapped_victims[participant.id].add(block['id'])
                        self._onVictimTrapped(participant, block)


    def __onPlayerState(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """
        if message.elapsed_milliseconds <= 0:
            return

        self.__player_location[message.participant_id] = message.position


    def __onRubbleCollapse(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.RubbleCollapse
            Received RubbleCollapse message
        """
        self.__is_trapped_cache.clear()


    def __onRubbleDestroyed(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.RubbleDestroyedEvent
            Received RubbleDestroyedEvent message
        """
        self.__is_trapped_cache.clear()
        self.__map[message.location] = None


    def __onSemanticMapInitialized(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.SemanticMapInitialized
            Received SemanticMapInitialized message
        """
        self.__map = deepcopy(self.__semantic_map.basemap)


    def __onVictimPickedUp(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.VictimPickedUp
            Received VictimPickedUp message
        """
        self.__map[message.location] = None


    def __onVictimPlaced(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.VictimPlaced
            Received VictimPlaced message
        """
        self.__map[message.location] = 'victim'
