from ..utils import to_2D



class BeliefMixin:
    """
    Mixin for querying player beliefs from the ToM.

    This mixin can be used by any component that inherits from BaseComponent
    (i.e., Interventions or Followups).  Note that the __init__ method of the
    BaseComponent must be called prior to calling this mixin's __init__ method.

    Methods
    -------
    believe(players, features, location, default=False)
        Return whether all the given players believe the given feature is present
        at the specified location, according to the ToM
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        redis_timeout : float, default=1.0
            Timeout for a Redis request on the internal bridge
        belief_threshold : float, default=0.99
            The minimum expected value in the belief state that is considered "believing"
        """
        self.__manager = manager
        self.__redis_timeout = kwargs.get('redis_timeout', 1.0)
        self.__belief_threshold = kwargs.get('belief_threshold', 0.99)


    def believe(self, players, features, location, mode='all', default=False):
        """
        Return whether all the given players believe the given feature is present
        at the specified location, according to the ToM.

        For example, `self.believe(['Red', 'Blue'], 'victims', (-2200, 60, 49))` will return
        True only if Red and Blue BOTH believe there are victims (of any kind) at location
        (-2200, 60, 49).

        Arguments
        ---------
        players : string or list of string
            List of keys for relevant players (e.g. participant IDs, roles, etc)
        features : string or list of string
            List of ToM `Cell` features (e.g. 'victim_A_injured', 'victim_B_treated')
            See https://gitlab.com/cmu_asist/study3/tom/-/tree/master/tom/belief#cell
        location : string or tuple
            The given location as either a room ID or a (x, z) or (x, y, z) location tuple
        mode : one of {'all', 'any'}
            Whether to return true if ALL players believe or if ANY players believe
        default : bool
            Default value to return on error
        """
        if not isinstance(players, (tuple, list, set, type(self.__manager.participants))):
            players = [players]

        # Create belief query
        query = {
            'players': [self.__manager.participants[p].id for p in players],
            'cells': features,
        }

        # Add location to query
        if isinstance(location, (tuple, list)):
            x, z = to_2D(location)
            query.update(x=x, z=z)
        elif isinstance(location, (str, type(None))):
            query.update(room=location)
        else:
            self.logger.info(f"{self}:  Invalid location {location}.")
            return default

        # Process ToM response
        try:
            response = self.manager.request_redis(query, channel='belief', timeout=self.__redis_timeout)
            expected_values = [
                sum(belief.values()) if isinstance(belief, dict) else belief
                for player, belief in response.data.items()
            ]
            truth_values = [value < self.__belief_threshold for value in expected_values]

            if mode == 'all':
                return all(truth_values)
            elif mode == 'any':
                return any(truth_values)
            else:
                self.logger.warning(f"{self}:  Invalid mode '{mode}' for BeliefMixin.believe()")
                return default

        # Return default value on timeout
        except TimeoutError:
            return default
