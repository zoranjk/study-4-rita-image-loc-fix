from ..components import InterventionScheduler as Scheduler
from ..utils import distance

from collections import defaultdict
from MinecraftBridge.messages import (
    FoVSummary, PlayerState, TriageEvent,
    VictimEvacuated, VictimPickedUp, VictimPlaced,
)
from MinecraftElements import Block



class SawVictimMixin:
    """
    Mixin that reports when players first discover or detect victims
    of some given type or state.

    This mixin can be used by any component that inherits from BaseComponent
    (i.e., Interventions or Followups).  Note that the __init__ method of the
    BaseComponent must be called prior to calling this mixin's __init__ method.

    Callbacks
    ---------
    _onNewVictimSeen(participant_id, victim_id, victim_type)
        Callback for when a victim is first seen by a player
    _onNewVictimApproached(participant_id, victim_id, victim_type)
        Callback for when a player first approaches the location of a known victim
        that they have not yet seen
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        victim_types : list, default=Block.victims()
            List of victim types to monitor (e.g. Block.triaged_victims())
        approach_threshold : float, default=5
            Distance from player to victim such that the victim is
            considered to have been "approached"
        """
        self.__manager = manager

        # Set attributes
        self.__approach_threshold = kwargs.get('approach_threshold', 5)
        self.__approached_victim_ids = defaultdict(set) # indexed by participant ID
        self.__seen_victim_ids = defaultdict(set) # indexed by participant ID
        self.__relevant_victim_types = kwargs.get('victim_types', Block.victims())
        self.__relevant_victim_types = [b.name for b in self.__relevant_victim_types]

        # Keep track of victim information
        self.__victim_locations = {} # indexed by victim ID
        self.__victim_types = {} # indexed by victim ID

        # Add Minecraft callbacks
        self.add_minecraft_callback(FoVSummary, self.__onFoV)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(TriageEvent, self.__set_victim_location)
        self.add_minecraft_callback(VictimEvacuated, self.__remove_victim_location)
        self.add_minecraft_callback(VictimPickedUp, self.__remove_victim_location)
        self.add_minecraft_callback(VictimPlaced, self.__set_victim_location)


    def _onNewVictimSeen(self, participant_id, victim):
        """
        Callback for when a victim is first seen by a player.

        Arguments
        ---------
        participant_id : string
            ID of the participant seeing the victim for the first time
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'
        """
        pass


    def _onNewVictimApproached(self, participant_id, victim):
        """
        Callback for when a victim is first seen by a player.

        Arguments
        ---------
        participant_id : string
            ID of the participant seeing the victim for the first time
        victim : dict
            Dictionary of victim info with keys for 'id', 'location', and 'type'
        """
        pass


    def __set_victim_location(self, message):
        """
        Arguments
        ---------
        message : TriageEvent or VictimPlaced
            Instance of received message
        """
        location = (message.victim_x, message.victim_y, message.victim_z)
        self.__victim_locations[message.victim_id] = location
        self.__victim_types[message.victim_id] = message.type


    def __remove_victim_location(self, message):
        """
        Arguments
        ---------
        message : VictimEvacuated or VictimPickedUp
            Instance of received message
        """
        self.__victim_locations.pop(message.victim_id, None)


    def __onFoV(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.FoVSummary
            Instance of the FoV message
        """
        participant_id = self.__manager.participants[message.playername].id

        # Check if there are relevant victims in the FoV message, and do the following:
        # 1.  Update our victim type & location maps
        # 2.  See if the player has detected the victim before
        # 3.  Call self._onNewVictimSeen() if not
        for block in message.blocks:
            if block['type'] in self.__relevant_victim_types:
                # Update type and location
                self.__victim_types[block['id']] = block['type']
                self.__victim_locations[block['id']] = tuple(block['location'])

                # Check if newly detected
                if block['id'] not in self.__seen_victim_ids[participant_id]:
                    self.__approached_victim_ids[participant_id].add(block['id'])
                    self.__seen_victim_ids[participant_id].add(block['id'])
                    self._onNewVictimSeen(participant_id, block)


    def __onPlayerState(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.message.PlayerState
            Received PlayerState message
        """
        if message.elapsed_milliseconds <= 0:
            return

        participant_id = self.__manager.participants[message.participant_id].id

        # Check if there are relevant victims near the player, and do the following:
        # 1.  See if the player has approached the victim before
        # 2.  Call self._onNewVictimApproached() if not
        for victim_id, victim_location in self.__victim_locations.items():
            if victim_id not in self.__approached_victim_ids[participant_id]:
                # Calculate player distance from victim
                dist = distance(message.position, victim_location)

                # Check if distance is below threshold
                if dist <= self.__approach_threshold:
                    self.__approached_victim_ids[participant_id].add(victim_id)
                    self._onNewVictimApproached(participant_id, {
                        'id': victim_id,
                        'type': self.__victim_types[victim_id],
                        'location': self.__victim_locations[victim_id],
                    })
