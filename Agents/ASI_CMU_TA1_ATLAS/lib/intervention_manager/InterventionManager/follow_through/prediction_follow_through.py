# -*- coding: utf-8 -*-
"""
.. module:: InterventionManager.follow_through.prediction_follow_through
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a base Followup class
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines an abstract prediction follow through class, which creates a prediction
based on the received followup, and publishes that prediction to the internal
bus.
"""

import abc
from MinecraftBridge.utils import Loggable


class PredictionFollowThrough(abc.ABC, Loggable):
	"""
	Definition of an abstract prediction follow through class.  This class is
	responsible for ingesting a followup, and providing functionality to
	publish predictions to the Redis bus and/or the Minecraft bus.
	"""


	def __init__(self, manager, **kwargs):
		"""
		Arguments
		---------
		manager : InterventionManager
			Instance of the InterventionManager

		Keyword Arguments
		-----------------
		topic : string
			Topic to publish predictions to.  The topic should not include
			`prediction/intervention_manager/`, this will be pre-pended to 
			the topic string.
		"""

		self.manager = manager

		# Only need the interface to the redis bridge
		self._redis_interface = manager.redis_interface
		self._minecraft_interface = manager.minecraft_interface

		# Prediction topic will be `prediction/intervention_manager`.  If an
		# additional `topic` is provided, then append to the topic
		self._topic = 'prediciton/intervention_manager'
		if 'topic' in kwargs:
			self._topic += '/' + kwargs['topic']

		self._publish_to_redis = kwargs.get("publish_to_redis", True)
		self._publish_to_mqtt = kwargs.get("publish_to_mqtt", True)


	def __str__(self):
		"""
		String representation of the follow though.
		"""

		return self.__class__.__name__ + " [" + self._topic + "]"


	@abc.abstractmethod
	def predict(self, followup):
		"""
		Abstract method to make a prediction for the given followup.

		Arguments
		---------
		followup : Followup
			Received followup to make a prediction on

		Returns
		-------
		dictionary or None
			Returns followup-specific prediction, or None if no prediction
			is made
		"""

		raise NotImplementedError


	def reset(self):
		"""
		Reset the followthrough to an initial state
		"""

		pass


	def onFollowupComplete(self, followup):
		"""
		Perform a prediction on the given followup, and publish to the
		Redis bus

		Arguments
		---------
		followup : Followup
			instance of the followup to make a prediction on
		"""

		prediction = self.predict(followup)

		if prediction is not None:
			if self._publish_to_redis:
				self.publish_prediction_redis(prediction)
			if self._publish_to_mqtt:
				self.publish_prediction_mqtt(prediction)


	def publish_prediction_redis(self, prediction):
		"""
		Publish a prediction to the Redis bus

		Arguments
		---------
		prediction : dictionary
			Dictionary summarizing the prediciton
		"""

		self.logger.info("%s:  Sending prediction to Redis bus with topic: %s", self, self._topic)
		self.logger.info("%s:    %s", self, str(prediction))

		self._redis_interface.send(prediction, self._topic)


	@abc.abstractmethod
	def publish_prediction_mqtt(self, prediction):
		"""
		Publish a prediction to the MQTT bus.  Since the prediction message
		could vary based on the prediction(s) being made, this needs to be
		defined by concrete subclasses.

		Arguments
		---------
		prediction : dictionary
			Dictionary summarizing the prediction.
		"""

		raise NotImplementedError




