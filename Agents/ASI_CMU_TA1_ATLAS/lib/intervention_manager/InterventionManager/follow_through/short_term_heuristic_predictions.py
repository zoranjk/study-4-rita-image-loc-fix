# -*- coding: utf-8 -*-
"""
.. module:: InterventionManager.follow_through.short_term_heuristic_predictions
    :platform: Linux, Windows, OSX
    :synopsis: Simple prediction follow-through to issue predictions on short-
               term effects
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Defines a simple follow-through for short-term predictions.  Predictions are 
simple assertions from an intervention developer, and do not rely upon 
information within the manager.
"""

from BaseAgent.utils import DependencyInjection

from .prediction_follow_through import PredictionFollowThrough

from collections import defaultdict

from MinecraftBridge.messages import (
    AgentPredictionGroupProperty,
    AgentStatePrediction,
    AgentStatePredictionMessage,
    BusHeader,
    MessageHeader
)

from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype


class ShortTermHeuristicPredictor(PredictionFollowThrough):
    """
    Class which generates short term, pre-defined predictions based upon
    whether an intervention was complied with or not.  The predictor is
    provided with pre-defined predictions based on the Followup class that
    is received.

    Attributes
    ----------

    Methods
    -------
    """

    def __init__(self, manager, **kwargs):
        """
        Keyword Arguments
        -----------------
        predictions : dictionary
            Dictionary mapping FollowupClass (as a fully qualified class name)
            to a dictionary mapping "compliance" and "noncompliance" to 
            prediction tuples of the form ("<metric>", <predicted_change>).
        """
        
        PredictionFollowThrough.__init__(self, manager, **kwargs)

        # Keep a list of predefined predictions based on followup class
        self.__predictions = {}

        # Populate the predictions based on the contents of the `predictions`
        # keyword argument.
        for prediction in kwargs.get('predictions', []):

            _, FollowupClass = DependencyInjection.get_module_and_class(prediction.get('followup_class', None))

            compliance_predictions = [(p["metric"], p["change"]) for p in prediction.get('compliance', [])]
            noncompliace_predictions = [(p["metric"], p["change"]) for p in prediction.get('noncompliance', [])]

            self.add_prediction(FollowupClass, 
                                compliance_predictions, 
                                noncompliace_predictions)

        # The prediction duration is the default time that the prediction will
        # occur within.
        self.__prediction_duration = kwargs.get("prediction_duration", 60)


    def add_prediction(self, FollowupClass, compliance, noncompliance):
        """
        Add a prediction for a received FollowupClass.  The compliance and
        non_compliance attributes are list of tuples indicating which
        predictions should be made.

        Each tuple consists of 1) the metric being predicted (as a string,
        e.g., "ASI-M2"), and 2) a value indicating whether the prediction 
        indicates an increase or decrease in value (i.e., 1 or -1).

        Note:  For now, +1 and -1 will indicate a prediction of increase or 
               decrease in the metric.

        Attributes
        ----------
        FollowupClass : class
            Follow class the predictions are made for when received
        compliance : list
            List of predictions to be made if the followup indicates that the
            intervention was complied with
        noncompliance : list
            List of predictions to be made if the followup was not complied
            with
        """

        # TODO:  Need to make sure that compliance and non_compliance are
        #        formatted correctly.  Since the format of these are not
        #        finalized, then hold off until we do finalize this.

        self.__predictions[FollowupClass] = self.__predictions.get(FollowupClass, {"compliance": [], "noncompliance": []})

        for prediction in compliance:
            self.__predictions[FollowupClass]["compliance"].append(prediction)
        for prediction in noncompliance:
            self.__predictions[FollowupClass]["noncompliance"].append(prediction)



    def predict(self, followup):
        """
        Create a prediction for the given followup.  If the followup class is
        not in the prediction dictionary, or if the `player_compliance` 
        attribute of the followup is `None`, then the predict method returns
        `None` to indicate that no prediction should be made.

        Arguments
        ---------
        followup : Followup
            Completed followup to generate a prediction for
        """

        # Check to see if the followup was complied with or not
        if followup.player_complied is None:
            return None

        # Check to see if predictions are available for the provided class
        if not followup.__class__ in self.__predictions:
            self.logger.warning("%s:  No predictions found for %s", self, followup.__class__.__name__)
            return None

        # Get the list of predictions based on whether or not the player
        # complied with the predictions
        if followup.player_complied:
            prediction_list = self.__predictions[followup.__class__]["compliance"]
        else:
            prediction_list = self.__predictions[followup.__class__]["noncompliance"]

        # Construction the prediction dictionary
        prediction = { 'predictor': str(self),
                       'followup': str(followup),
                       'complied': followup.player_complied,
                       'predictions': []
                     }

        for metric, change in prediction_list:
            prediction['predictions'].append({'metric': metric,
                                              'change': change})

        # Don't send anything if no predictions have been made
        if len(prediction['predictions']) == 0:
            return None

        return prediction


    def publish_prediction_mqtt(self, prediction):
        """
        Publish the short term predictions to the MQTT bus.  The provided
        predictions will be converted to an AgentStatePredictionMessage.

        Arguments
        ---------
        prediction : dictionary
            Dictionary of short-term predictions
        """

        # Explanation will summarize where the prediction came from, i.e., the
        # explanation for this prediction is that Intervention XXX has been
        # complied / not complied with
        explanation = { 'predictor': prediction['predictor'],
                        'followup_class': prediction['followup'],
                        'complied': prediction['complied'] 
                      }

        # Create the Prediciton Group Property and the Prediction Message
        group_property = AgentPredictionGroupProperty(None,
                                                      self.__prediction_duration,
                                                      explanation=explanation)
        message = AgentStatePredictionMessage(group=group_property)

        # Add the predictions
        for pred in prediction['predictions']:
            message_prediction = AgentStatePrediction(subject="TM",
                                                      subject_type="team",
                                                      predicted_property=pred['metric'],
                                                      prediction=pred['change'],
                                                      probability="N/A",
                                                      probability_type="string",
                                                      confidence="N/A",
                                                      confidence_type="string"
                                                      )
            message.add(message_prediction)

        # Add needed message headers
        bus_header = BusHeader(MessageType.agent)
        msg_header = MessageHeader(MessageSubtype.Prediction_State,
                                   self.manager.trial_info["experiment_id"],
                                   self.manager.trial_info["trial_id"],
                                   self.manager._agent_name,
                                   replay_id=self.manager.trial_info["replay_id"]
                                   )

        message.addHeader("header", bus_header)
        message.addHeader("msg", msg_header)

        # Publish the message
        self._minecraft_interface.publish(message)

