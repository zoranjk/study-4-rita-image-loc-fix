# -*- coding: utf-8 -*-
"""
.. module:: explainerfactory
    :platform: Linux, Windows, OSX
    :synopsis: Definition of the top-level InterventionManager class
.. moduleauthor:: Ying Chen <yingc4@andrew.cmu.edu>

Class definition for factory to produce Explainer instances for a given class
"""

import json
import os

from explainer import Explainer

class ExplainerFactory:
    def __init__(self, expl_config_path):
        with open(expl_config_path) as expl_config_file:
            expl_configs = json.load(expl_config_file)

        self.explainers = {}
        for expl_config in expl_configs['interventions']:
            self.explainers[expl_config['intervention_class']] = Explainer(expl_config)


