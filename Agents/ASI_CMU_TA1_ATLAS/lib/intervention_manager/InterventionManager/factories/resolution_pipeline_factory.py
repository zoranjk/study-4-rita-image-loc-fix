# -*- coding: utf-8 -*-
"""
.. module:: interventions.resolution_pipeline_factory
    :platform: Linux, Windows, OSX
    :synopsis: Definition of a factory
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>
"""

import os
import sys
import pkgutil
import collections
import inspect

from MinecraftBridge.utils import Loggable

from ..interventions import Intervention
from ..triggers import InterventionTrigger
from ..followups import Followup

from ..resolution import ResolutionPipeline


class ResolutionPipelineFactory(Loggable):
    """
    A resolution pipeline factory is used by to simplify construction of a
    resolution pipeline.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager using this factory
        """

        self.logger.debug(f'{self}:  Initializing ResolutionPipelineFactory')

        self._manager = manager
        self._resolution_pipeline = None


    def __str__(self):
        """
        String representation of the factory
        """

        return self.__class__.__name__


    def load(self):
        """
        Create a ResolutionPipeline, and inject an instance of each
        transformer class defined in the configuration.
        """

        self.logger.info(f'{self}:  Creating ResolutionPipeline instance')

        self._resolution_pipeline = ResolutionPipeline(self._manager)

        return self._resolution_pipeline




    def inject(self):
        """
        Inject the loaded library components into the manager
        """

        self.logger.info(f'{self}:  Injecting Trigger instances into {self._manager}')

        for module_name in self._library:

            # Pull out all the triggers and their corresponding properties
            TriggerClass = self._library[module_name].get('TriggerClass', None)

            if TriggerClass is None:
                self.logger.warning(f'{self}:  Module {module_name} did not have `TriggerClass`')

            else:
                parameters = self._library[module_name].get('default_parameters', {}).get(TriggerClass.__name__, {})

                try:
                    # Create and add the trigger
                    trigger = TriggerClass(self._manager, **parameters)
                    self._manager.addTrigger(trigger)
                    self.logger.info(f'{self}:    Added {module_name}.{trigger}')

                except Exception as e:
                    self.logger.warning(f'{self}:    -= Exception raised when attempting to add trigger: =-')
                    self.logger.warning(f'{self}:    {str(e)}')



    def override_parameters(self, module_name, class_name, parameters):
        """
        Change the contents of the default parameters for a given module and 
        class.

        Arguments
        ---------
        module_name : str
            Module containing parameter list to override
        class_name : str
            Class name whose parameters should be overrided
        parameters : dict
            Dictionary of parameters to override
        """

        pass


    def get_intervention_classes(self):
        """
        Returns a set of intervention class names
        """

        return set.union(*[l['intervention_classes'] for l in self._library.values()])



    def get_class_by_name(self, name):
        """
        Attempts to find the class instance by the provided name.  If the name
        is qualified by the module name (i.e., is `module_name.class_name`), 
        then only searches in the module.  Otherwise, this method will search
        all modules until the class is discovered.

        In each module, the method will search for a class with the name in
        the interventions, triggers, followups, and others classes, in that
        order.

        Argument
        --------
        name : str
            String containing the class name, optionally pre-pended by the
            module name

        Returns
        -------
        First class discovered with the provided name
        """

        # Split out the module and class -- the module will be defined by the
        # second-to-last split (by '.'), class name as the last
        names = name.split('.')
        class_name = names[-1]
        module_names = self._library.keys() if len(names) < 2 else [names[-2]]

        for module_name in module_names:
            if not module_name in self._library:
                self.logger.warning(f'{self}:  Module {module_name} not in factory library')
            else:
                for class_ in self._library[module_name].get('intervention_classes', set()):
                    if class_.__name__ == class_name:
                        return class_
                for class_ in self._library[module_name].get('trigger_classes', set()):
                    if class_.__name__ == class_name:
                        return class_
                for class_ in self._library[module_name].get('followup_classes', set()):
                    if class_.__name__ == class_name:
                        return class_
                for class_ in self._library[module_name].get('other_classes', set()):
                    if class_.__name__ == class_name:
                        return class_

        return None


    def load_library(self, path):
        """
        Load the contents of the library of interventions in the provided path.

        Arguments
        ---------
        path
            Path to intervention library
        """

        self.logger.debug(f'{self}:  Loading library from path: {path}')

        # Verify existence of the path
        if not os.path.exists(path):
            self.logger.warning(f'{self}:  Path {path} does not exist')
            return

        if not os.path.isdir(path):
            self.logger.warning(f'{self}:  Path {path} is not a directory')
            return

        self.logger.debug(f'{self}:  Contents of path:  {os.listdir(path)}')

        # Add the path to the system path variable, so that Python knows where
        # to load modules from
        if not path in sys.path:
            sys.path.append(path)

        # Get a list of modules located in the path --- this will return a list
        # of directory names in the path that are modules (i.e., directories 
        # that contain an __init__.py file)
        module_names = [m[1] for m in pkgutil.iter_modules([path]) if m[2]]

        self.logger.debug(f'{self}:  Modules in path:')
        for name in module_names:
            self.logger.debug(f'{self}:    {name}')

        # Remove 'shared' if it is in the list of modules
        for name in self._modules_to_ignore:
            if name in module_names:
                module_names.remove(name)

        # For each module, extract the Intervention, Trigger, and Followup
        # classes
        for module_name in module_names:

            module_ = __import__(module_name)
            self._library[module_name] = self.__get_module_contents(module_)


    def __get_module_contents(self, module_):
        """
        Extract a dictionary of the named components of Intervention, Trigger,
        and Followup classes of the module, and the named components, i.e., 
        `InterventionClass`, `TriggerClass`, `IndividualComplianceFollowupClass`,
        and `TeamComplianceFollowupClass`

        Arguments
        ---------
        module_
            Module to load contents from
        """

        self.logger.debug(f'{self}:  Loading contents of module {module_}')

        contents = {}

        # Modules should define variables named `InterventionClass`, 
        # `TriggerClass`, `IndividualComplianceFollowupClass` and 
        # `TeamComplianceFollowupClass`.  Load these first, if they exists
        try:
            contents['InterventionClass'] = module_.InterventionClass
        except:
            self.logger.warning(f'{self}:  Module {module_} does not define InterventionClass')
            contents['InterventionClass'] = None

        try:
            contents['TriggerClass'] = module_.TriggerClass
        except:
            self.logger.warning(f'{self}:  Module {module_} does not define TriggerClass')
            contents['TriggerClass'] = None

        try:
            contents['IndividualComplianceFollowupClass'] = module_.IndividualComplianceFollowupClass
        except:
            self.logger.warning(f'{self}:  Module {module_} does not define IndividualComplianceFollowupClass')
            contents['IndividualComplianceFollowupClass'] = None

        try:
            contents['TeamComplianceFollowupClass'] = module_.TeamComplianceFollowupClass
        except:
            self.logger.warning(f'{self}:  Module {module_} does not define TeamComplianceFollowupClass')
            contents['TeamComplianceFollowupClass'] = None


        # Store the module's default_parameters dictionary.  This should be a
        # dictionary mapping class names to a list of keyward arguments.  Right
        # now, the manager only passes keyword arguments to InterventionTrigger
        # classes (hopefully changed in the future...)
        try:
            contents['default_parameters'] = module_.default_parameters
        except:
            self.logger.warning(f'{self}:  Module {module_} does not define default_parameters')
            contents['default_parameters'] = {}

        # Load all other classes in the module.  If the classes are subtypes of 
        # `Intervention`, `InterventionTrigger`, or `Followup`, put in the
        # appropriate category.  Otherwise, put in the `Other` category
        contents['intervention_classes'] = set()
        contents['trigger_classes'] = set()
        contents['followup_classes'] = set()
        contents['other_classes'] = set()

        for name, member in inspect.getmembers(module_):
            # Ignore if this isn't a class
            if not inspect.isclass(member):
                pass
            # Check if it's a subclass of Intervention, InterventionTrigger, or
            # Followup
            elif issubclass(member, Intervention):
                contents['intervention_classes'].add(member)
            elif issubclass(member, InterventionTrigger):
                contents['trigger_classes'].add(member)
            elif issubclass(member, Followup):
                contents['followup_classes'].add(member)
            # Store the class as `other`
            else:
                contents['other_classes'].add(member)

        # All done!
        return contents
