# -*- coding: utf-8 -*-
"""
.. module:: victims_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Utility class for monitoring victim states and locations

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a component to monitor victim states and location, which can be
used by Interventions / Triggers / Followups to be able to store quasi-global
information about victims.
"""


from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import (
    FoVSummary,
    TriageEvent,
    VictimPickedUp,
    VictimPlaced
)

from collections import defaultdict



class VictimsMonitor(Loggable):
    """
    A simple class to track the location and status of victims.  

    Attributes
    ----------
    victims : dictionary
    """

    class Victim:
        """
        Lightweight class for storing information about victims, which consists of
        the following attributes:

        id : int
            Unique identifier for the victim, provided by the Minecraft messages
        location : tuple
            (x,y,z) location of the victim
        victim_type : MinecraftElements.Block
            victim type
        stabilized_state : string
            Type of victim ("REGULAR", "CRITICAL", "SAVED")
        injury_type : string
            Injury type of the victim ("A", "B", "C")
        history : list
            List of messages related to the victim
        """

        def __init__(self, id=None, location=None, victim_type=None,
                           stabilized_state=None, injury_type=None):

            self.id = id
            self.location = location
            self._victim_type = victim_type
            self.stabilized_state = stabilized_state
            self.injury_type = injury_type
            self.history = []

        @property
        def victim_type(self):
            return self._victim_type

        @victim_type.setter
        def victim_type(self, _type):
            self._victim_type = _type

            components = self._victim_type.split('_')

            if 'saved' in components or 'SAVED' in components:
                self.stabilized_state = 'SAVED'
            elif 'c' in components or 'C' in components:
                self.stabilized_state = 'CRITICAL'
            elif 'a' in components or 'A' in components or 'b' in components or 'B' in components:
                self.stabilized_state = 'REGULAR'
            else:
                self.logger.warning("%s:  Unknown stabilization state from type: %s", self, self._victim_type)

            if 'a' in components or 'A' in components:
                self.injury_type = 'A'
            elif 'b' in components or 'B' in components:
                self.injury_type = 'B'
            elif 'c' in components or 'C' in components:
                self.injury_type = 'C'
            else:
                self.logger.warning("%s:  Unknown injury type from type: %s", self, self._victim_type)
        


    def __init__(self, parent_component):

        self._parent_component = parent_component

        # Mapping of victim location to Victim instance
        self._victims = defaultdict(self.__new_victim)
        self._picked_up_victims = defaultdict(self.__new_victim)

        if self._parent_component is None:
            self.logger.warning("%s:  No parent component provided", self)
            return

        # Register callbacks to receive information regarding victims
        self._parent_component.add_minecraft_callback(FoVSummary, self.__onFoV)
        self._parent_component.add_minecraft_callback(TriageEvent, self.__onTriageEvent)
        self._parent_component.add_minecraft_callback(VictimPickedUp, self.__onVictimPickedUp)
        self._parent_component.add_minecraft_callback(VictimPlaced, self.__onVictimPlaced)


    def __str__(self):
        """
        String representation of this
        """

        return self.__class__.__name__


    def __new_victim(self):
        """
        Create a new Victim instance with `None` values for the attributes
        """

        self.logger.debug("%s:  Creating new victim", self)

        return VictimsMonitor.Victim()


    @property
    def victims(self):
        return self._victims


    def __onFoV(self, message):
        """
        Callback when an FoVSummary message is received.  If a victim is in the
        FoV message, then updates the contents of the victims dictionary with 
        the position of that victim, and the victim type (regular, critical, or
        saved).

        Arguments
        ---------
        message : MinecraftBridge.message.FoVSummary
        """

        for block in message.blocks:
            if "victim" in block['type']:

                self.logger.debug("%s:  FoV: victim block of type %s at location %s", self, block["type"], block["location"])

                location = block["location"]

                victim = self._victims[tuple(location)]
                victim.location = location

                victim.history.append(message)

                # Infer what can be inferred from the block type
                if block['type'] == 'block_victim_regular':
                    victim.stabilized_state = "REGULAR"
                elif block['type'] == 'block_victim_proximity':
                    victim.victim_type = 'victim_c'
                elif block['type'] == 'block_victim_saved':
                    victim.stabilized_state = "SAVED"


    def __onTriageEvent(self, message):
        """
        Callback when a TriageEvent message is received.  If the victim is not
        known, adds the victim to the victims dictionary.

        Arguments
        ---------
        message : MinecraftBridge.message.TriageEvent
        """

        self.logger.debug("%s:  Received TriageEvent message:", self)
        self.logger.debug("%s:    victim_id: %d", self, message.victim_id)
        self.logger.debug("%s:    location: %s", self, message.victim_location)
        self.logger.debug("%s:    type: %s (%s)", self, message.type, message.type.__class__)
        self.logger.debug("%s:    triage_state: %s", self, message.triage_state)

        # Get (or create) the victim at the location
        location = message.victim_location

        victim = self._victims[location]
        victim.location = location

        victim.history.append(message)

        if victim.id is None:
            victim.id = message.victim_id
        if victim.id != message.victim_id:
            self.logger.warning("%s:  Mismatch between victim.id (%d) and message id (%d)", self, victim.id, message.victim_id)

        if message.triage_state == TriageEvent.TriageState.SUCCESSFUL:
            victim.stabilized_state = "SAVED"


    def __onVictimPickedUp(self, message):
        """
        Callback when a victim is picked up.

        Arguments
        ---------
        message : MinecraftBridge.message.VictimPicedUp
        """

        self.logger.debug("%s:  Received VictimPickedUp message:", self)
        self.logger.debug("%s:    victim_id: %d", self, message.victim_id)
        self.logger.debug("%s:    location: %s", self, message.location)
        self.logger.debug("%s:    type: %s (%s)", self, message.type, message.type.__class__)

        # Get the victim from the victims dictionary, and remove it
        if not message.location in self._victims:
            self.logger.warning("%s:  Picking up unknown victim @ %s", self, str(message.location))

        victim = self._victims[message.location]
        del self._victims[message.location]
        victim.history.append(message)

        # Set the location to None, and set the victim id based on the message
        # contents if needed.  Store the victim in the picked_up_victims
        # dictionary
        if victim.id is None:
            victim.id = message.victim_id
        if victim.id != message.victim_id:
            self.logger.warning("%s:  Mismatch between victim.id (%d) and message id (%d)", self, victim.id, message.victim_id)

        victim.location = None
        victim.victim_type = message.type

        if victim.id in self._picked_up_victims:
            self.logger.warning("%s:  Victim with ID %d already in _picked_up_victims", self, victim.id)

        self._picked_up_victims[victim.id] = victim


    def __onVictimPlaced(self, message):
        """
        Callback when a victim is placed.

        Arguments
        ---------
        message : MinecraftBridge.message.VictimPlaced
        """

        self.logger.debug("%s:  Received VictimPlaced message:", self)
        self.logger.debug("%s:    victim_id: %d", self, message.victim_id)
        self.logger.debug("%s:    location: %s", self, message.location)
        self.logger.debug("%s:    type: %s (%s)", self, message.type, message.type.__class__)


        # Get the vicitm from the picked_up_victims dictionary, and remove it
        if not message.victim_id in self._picked_up_victims:
            self.logger.warning("%s:  Placing unknown victim with ID %d", self, message.victim_id)

        victim = self._picked_up_victims[message.victim_id]
        del self._picked_up_victims[message.victim_id]
        victim.history.append(message)

        # Update the victim information from the message contents
        victim.location = message.location
        victim.victim_type = message.type

        self._victims[message.location] = victim


