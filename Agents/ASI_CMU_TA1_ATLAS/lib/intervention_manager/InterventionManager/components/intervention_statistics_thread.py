import threading

from MinecraftBridge.mqtt.parsers import MessageType, MessageSubtype
from MinecraftBridge.messages import BusHeader, MessageHeader, InterventionStatistics
from MinecraftBridge.utils import Loggable



class InterventionStatisticsThread(threading.Timer, Loggable):
    """
    Wrapper class for a thread publishing the intervention statistics message to the bus.
    This thread covers ASI-M8 for internal performance measurement.
    """

    def __init__(self, manager, minecraft_interface, publish_interval=30, agent_name='ASI_CMU_TA1_ATLAS'):
        """
        Arguments
        ---------
        manager : InterventionManager
            InterventionManager instance for which statistics are reported
        minecraft_interface : MinecraftBridge.mqtt.MinecraftInterface
            MinecraftBridge interface that allows for publishing messages
        publish_interval : int, default=30
            The interval, in (real-time) seconds, at which the statistics are published
        agent_name : string, default='ASI_CMU_TA1_ATLAS'
            Name of agent to be used in published messages
        """
        threading.Timer.__init__(self, publish_interval, self.__publish_statistics)
        self._manager = manager
        self._minecraft_interface = minecraft_interface
        self._agent_name = agent_name


    def getInterventionStatistics(self):
        """
        Return a dictionary containing the intervention statistics.
        It should contain fields for 'active', 'resolved', and 'discarded'.
        """
        return {
            'active': len(self._manager._active_queue),
            'resolved': len(self._manager._response_queue),
            'discarded': len(self._manager._discard_queue),
        }


    def __publish_statistics(self):
        """
        Helper function that creates and publishes the intervention statistics
        message.
        """
        self.logger.info(f"{self}:  Publishing intervention statistics message")

        # Create an InterventionStatistics message
        statistics = self.getInterventionStatistics()
        msg = InterventionStatistics(
            active=statistics['active'],
            resolved=statistics['resolved'],
            discarded=statistics['discarded'],
        )

        # Add headers
        bus_header = BusHeader(MessageType.agent)
        msg_header = MessageHeader(
            MessageSubtype.Intervention_Chat,
            self._manager.trial_info['experiment_id'],
            self._manager.trial_info['trial_id'],
            self._agent_name,
            replay_id=self._manager.trial_info['replay_id'],
        )
        msg.addHeader('header', bus_header)
        msg.addHeader('msg', msg_header)

        # Publish
        self._minecraft_interface.publish(msg)
