# -*- coding: utf-8 -*-
"""
.. module:: ASISTPlanSupervisor
   :platform: Linux, Windows, OSX
   :synopsis: ASISTPlanSupervisor module

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Package containing a Plan Supervision module for use in ASIST.  
"""

__author__ = "Dana Hughes"
__email__ = "danahugh@andrew.cmu.edu"
__url__ = "https://gitlab.com/cmu_aart/asist/study4/PlanSupervisor"
__version__ = "0.0.6"

from .plan_supervisor import PlanSupervisor

__all__ = []
