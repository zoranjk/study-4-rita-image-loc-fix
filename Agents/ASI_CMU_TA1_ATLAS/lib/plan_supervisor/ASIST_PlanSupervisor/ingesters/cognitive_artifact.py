# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.injesters.cognitive_artifact
   :platform: Linux, Windows, OSX
   :synopsis: Class for ingesting events from cognitive artifact interaction

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

The CognitiveArtifactInjester 
"""

# import enum

import MinecraftElements
from MinecraftBridge.messages import (
   EnvironmentCreatedSingle,
   UI_Click
)
from MinecraftBridge.utils import Loggable

from .meta_action import MetaAction

from ..representation import NullPlan
from ..representation.query import QueryBy, SortBy

from ..plan_components import PlanStep
from ..plan_components.actions import (
   GoToWaypoint,
   Search,
   DefuseBomb,
   PutOutFire,
   GroupUp
)


class CognitiveArtifactIngester(Loggable):
   """
   The CognitiveArtifactInjeseter is used to consume UI_Click messages and
   convert these to Plan Event instances.

   Attributes
   ----------
   parent : PlanSupervisor
      Top-level plan supervisor component in charge of this injester
   plan : PlanRepresentation
      PlanRepresentation instance this injester is interacting with
   """

   def __init__(self, parent, minecraft_interface, plan=None, config={}):
      """
      Arguments
      ---------
      parent : PlanSupervisor
         Top-level superviser component in charge of the ingester
      minecraft_interface : MinecraftBridge.mqtt.callback_interface
         Interface to MinecraftBridge
      plan : PlanRepresentation, default=None
         PlanRepresentation instance this injester will interact with.  If None
         is provided (default) a NullPlan will be instantiated.
      config : dict
         Dictionary of configuration values
      """

      self.logger.debug(f'{self}:  Initializing Cognitive Artifact Ingester')

      self._parent = parent
      self._minecraft_interface = minecraft_interface

      self._plan = plan

      if self._plan == None:
         self._plan = NullPlan()

      # Private variables indicating whether messages should be pretty-printed
      self.__pretty_print_ui = config.get("pretty_print_ui_messages", True)
      self.__pretty_print_beacon = config.get("pretty_print_beacon_messages", False)

      # Handlers for action creation based on beacon type
      self.__beacon_action_handlers = { MinecraftElements.Block.block_beacon_basic:  self.__createBasicBeaconAction,
                                        MinecraftElements.Block.block_beacon_bomb:   self.__createBombBeaconAction,
                                        MinecraftElements.Block.block_beacon_hazard: self.__createHazardBeaconAction,
                                        MinecraftElements.Block.block_beacon_rally:  self.__createRallyBeaconAction
                                      }


   def __str__(self):
      """
      String representation of the object
      """

      return self.__class__.__name__


   @property
   def parent(self):
      return self._parent

   @property
   def plan(self):
      return self._plan

   @plan.setter
   def plan(self, plan):
      self._plan = plan
   


   def start(self):
      """
      Called by the parent when the injester is ready to be started.
      """

      self.logger.info(f'{self}:    Starting')

      self._minecraft_interface.register_callback(UI_Click, self.__onUI_Click)
      self._minecraft_interface.register_callback(EnvironmentCreatedSingle, self.__onEnvironmentCreated)


   def stop(self):
      """
      Called by the parent when the injester is to be stopped.
      """

      self.logger.info(f'{self}:    Stopping')

      self._minecraft_interface.deregister_callback(self.__onUI_Click)
      self._minecraft_interface.deregister_callback(self.__onEnvironmentCreated)


   def __get_step_key(self, additional_info):
      """
      Construct a step identifier from the provided additional info
      """

      flag_index = additional_info.get('flag_index', -1)
      try:
         call_sign_code = additional_info.get('call_sign_code', -1)
         call_sign = ['alpha','bravo','delta'][call_sign_code]
      except:
         self.logger.warining(f'{self}:  Bad call_sign_code: {call_sign_code}')
         call_sign = "UNKNOWN"

      return f'flag_{call_sign}_{flag_index}'


   def __pretty_print_ui_message(self, message):
      """
      Debugging method to print the contents of a UI Click message.
      """

      if not self.__pretty_print_ui:
         return

      # Try to extract the meta-action from the message
      try:
         meta_action_name = MetaAction(message.additional_info['meta_action']).name
      except:
         meta_action_name = "UnknownMetaAction"

      self.logger.info(f'{self}:  Processing {meta_action_name} meta-action')
      self.logger.info(f'{self}:    participant_id: {message.participant_id}')
      self.logger.info(f'{self}:    element_id:     {message.element_id}')
      self.logger.info(f'{self}:    type:           {message.type}')
      self.logger.info(f'{self}:    x:              {message.x}')
      self.logger.info(f'{self}:    y:              {message.y}')
      self.logger.info(f'{self}:    additional_info')

      for key, value in message.additional_info.items():
         self.logger.info(f'{self}:      {key}:    {value}')

   def __pretty_print_beacon_message(self, message):
      """
      Debugging method to print the contents of a beacon message
      """

      if not self.__pretty_print_beacon:
         return

      beacon_name = message.object.type.name.split('_')[-1].capitalize()

      self.logger.info(f'{self}:  Processing {beacon_name} Beacon')
      self.logger.info(f'{self}:    participant_id: {message.triggering_entity}')
      self.logger.info(f'{self}:    entity_id: {message.object.id}')
      self.logger.info(f'{self}:    location: {(message.object.x, message.object.y, message.object.z)}')
      self.logger.info(f'{self}:    attributes')

      for name in message.object.get_attribute_names():
         self.logger.info(f'{self}      {name}:   {message.object.get_attribute(name)}')


   def __createBombBeaconAction(self, message):
      """
      Construct an appropriate action for the given bomb beacon message
      """

      # Create a BombDefusal action, and wrap it in a plan step.  The step ID
      # is the object ID, as this is the ID used later when the bomb beacon
      # is manipulated in the cognitive artifact
      return DefuseBomb((message.object.x, message.object.z))


   def __createBasicBeaconAction(self, message):
      """
      Construct an appropriate action for the given basic beacon message
      """

      return


   def __createRallyBeaconAction(self, message):
      """
      Construct an appropriate action for the given rally beacon message
      """

      return


   def __createHazardBeaconAction(self, message):
      """
      Construct an appropriate action for the given hazard beacon message
      """

      # Currently, assume that the hazard beacon is associated with a fire, and
      # that it needs to be put out
      return PutOutFire((message.object.x, message.object.z))



   def __onEnvironmentCreated(self, message):
      """
      Process an EnvironmentCreatedSingle message to determine if a bomb
      beacon block has been placed.
      """

      self.logger.debug('f{self}:  Message received {message}')

      # Is the message about a bomb beacon?
      if message.object.type in [ MinecraftElements.Block.block_beacon_basic,
                                  MinecraftElements.Block.block_beacon_rally,
                                  MinecraftElements.Block.block_beacon_bomb,
                                  MinecraftElements.Block.block_beacon_hazard ]:

         self.__pretty_print_beacon_message(message)

         # Create an action based on beacon type.  If the returned action is
         # `None`, then don't bother adding anything to the plan
         handler = self.__beacon_action_handlers.get(message.object.type, None)
         if handler is None:
            self.logger.warning(f'{self}:  Unsure how to create action for Beacon Type {message.object.type.name}')
            return

         action = handler(message)

         if action is None:
            self.logger.info(f'{self}:  No action created for Beacon Type {message.object.type.name}')
            return

         step_id = message.object.id
         plan_step = PlanStep(step_id, action, message.triggering_entity)

         self._plan.add_step(plan_step)

         self.logger.debug(f'{self}:    Added {plan_step} with action {action}.')


   def __processPlanningFlagPlaced(self, message):
      """
      Process a message with a Planning Phase Placed meta-action, producing
      a Plan Event instance.

      Arguments
      ---------
      message : MinecraftBridge.message.UI_Click
         Message to process
      """

      self.__pretty_print_ui_message(message)

      # Create a GoToWaypoint action, and wrap it in a plan step.  The step ID
      # is constructed from the call_sign_code and flag_index, as these exists
      # in related meta_action additional_info fields.  Assume that the
      # participant will be the actor performing the action.
      location = (round(message.additional_info.get("x_percentage", 0) * 50 / 100),
                  round(message.additional_info.get("y_percentage", 0) * 150 / 100))
      action = GoToWaypoint(location)
      action.add_actor(message.participant_id)

      step_id = message.element_id
      plan_step = PlanStep(step_id, action, message.participant_id)

      self._plan.add_step(plan_step)
      self.logger.info(f'{self}:    Added {plan_step} with action {action}.')



   def __createSearchAction(self, message, old_action):
      """
      Helper method to create a Search action, if the `old_action` is not 
      already a Search action.
      """

      # Do nothing if the old action is already a Search action
      if isinstance(old_action, Search):
         return old_action

      # Otherwise, create a Search action with the same location and actors
      action = Search(old_action.location)
      for actor in old_action.actors:
         action.add_actor(actor)

      return action


   def __createGroupUpAction(self, message, old_action):
      """
      Helper method to create a GroupUp action, if the `old_action` is not
      already a GroupUp action
      """

      # Do nothing if the old action is already a GroupUp action
      if isinstance(old_action, GroupUp):
         return old_action

      # Otherwise, create a GroupUp action with the same location, and all the
      # participants are actors
      action = GroupUp(old_action.location)
      for participant in self._parent.participants:
         action.add_actor(participant.id)

      return action


   def __processPlanningFlagUpdate(self, message):
      """
      Process a message with a Planning Phase Update meta-action, modifying
      a Plan Event instance.

      Arguments
      ---------
      message : MinecraftBridge.message.UI_Click
         Message to process
      """

      self.__pretty_print_ui_message(message)

      # Which plan step is being modified?
#      step_id = self.__get_step_key(message.additional_info)
      step_id = message.element_id

      self.logger.info(f'{self}:    Updating plan step with ID {step_id}')

      plan_step = self._plan.get_step(step_id)

      if plan_step is None:
         self.logger.warning(f'{self}:    Plan Step with ID {step_id} does not appear to be in the plan.  Creating.')
#         self.__processPlanningFlagPlaced(message)
         return

      # For now, check the content.  If it's something other than 'NOT_SET',
      # then change the action
      content = message.additional_info.get('content', 'NOT_SET')


      if content == 'I will search here!':

         # Replace the action in this step with a Search action
         plan_step.action = self.__createSearchAction(message, plan_step.action)

         # Update the action properties with message properties
         plan_step.action.location = (round(message.additional_info.get("x_percentage", 0) * 50 / 100),
                                      round(message.additional_info.get("y_percentage", 0) * 150 / 100))

      elif content == "Let's group up here!":

         # Replace the action in this step with a GroupUp action
         plan_step.action = self.__createGroupUpAction(message, plan_step.action)

         # Update the action properties with message properties
         plan_step.action.location = (round(message.additional_info.get("x_percentage", 0) * 50 / 100),
                                      round(message.additional_info.get("y_percentage", 0) * 150 / 100))

      else:
         self.logger.info(f'{self}:    Unsure what to do with content {content}')


      # Update the PlanStep properties from the message contents
      plan_step.schedule = (message.additional_info.get("schedule_minute",-1),
                            message.additional_info.get("schedule_second",-1))
      plan_step.prioritization = message.additional_info.get("prioritization", 1)

      # Update any responses
      for participant_id, response in message.additional_info.get('responses',{}).items():
         plan_step.responses[participant_id] = response

         if response == 'I can handle this myself!':
            plan_step.action.add_actor(participant_id)
         elif response == 'I can provide support!':
            plan_step.action.add_actor(participant_id)
         elif response == 'Someone other than me should help!':
            plan_step.action.remove_actor(participant_id)
         elif response == 'I agree with your plan!':
            pass
         elif response == 'I disagree with your plan!':
            pass
         elif response == '':
            # No response.  This is added to avoid logging the warning message
            pass
         else:
            self.logger.warning(f'{self}:  Not sure what to do with response "{response}"')



   def __processUndoPlanningFlag(self, message):
      """
      Process a message with a Planning Phase Undo meta-action, modifying
      a Plan Event instance.

      Arguments
      ---------
      message : MinecraftBridge.message.UI_Click
         Message to process
      """

      self.__pretty_print_ui_message(message)

      step_id = self.__get_step_key(message.additional_info)

      self.logger.info(f'{self}:    Removing plan step with ID {step_id}.')
      self._plan.remove_step(step_id)



   def __processPlanningBombPlaced(self, message):
      """
      Process a message with a Planning Bomb Placed meta-action.

      Arguments
      ---------
      message : MinecraftBridge.message.UI_Click
         Message to process
      """

      self.__pretty_print_ui_message(message)



   def __processPlanningBombUpdate(self, message):
      """
      Process a message with a Planning Bomb Update meta-action.

      Arguments
      ---------
      message : MinecraftBridge.message.UI_Click
         Message to process
      """

      self.__pretty_print_ui_message(message)



   def __processUndoPlanningBomb(self, message):
      """
      Process a message with a Planning Bomb Undo meta-action.

      Arguments
      ---------
      message : MinecraftBridge.message.UI_Click
         Message to process
      """

      self.__pretty_print_ui_message(message)

      self.logger.info(f'{self}:')


   def __updateBombBeacon(self, message, plan_step):
      """
      Helper function to update beacons about bomb defusal
      """

      # Populate any fields that do not exist in the step and/or action based
      # on the update
      if plan_step.action.bomb_id is None:
         plan_step.action.bomb_id = message.additional_info.get('bomb_id',None)
      plan_step.action.remaining_sequence = message.additional_info.get('remaining_sequence',None)
      if plan_step.action.fuse_start_time == (-1,-1):
         plan_step.action.fuse_start_time = (message.additional_info.get('fuse_start_minute',-1), 0)
      if plan_step.action.chained_id is None:
         plan_step.action.chained_id = message.additional_info.get('chained_id', None)


   def __updateHazardBeacon(self, message, plan_step):
      """
      Helper function to update beacons about hazards
      """

      pass

   def __processPlanningBeaconUpdate(self, message):
      """
      Process a message with a Planning Beacon Update meta-action.

      Arguments
      ---------
      message : MinecraftBridge.message.UI_Click
         Message to process
      """

      self.__pretty_print_ui_message(message)

      # Get the Plan Step associated with this beacon update
      plan_step_id = message.element_id
      plan_step = self._plan.get_step(plan_step_id)

      # Make sure the action is indeed a DefuseBomb action
      if isinstance(plan_step.action, DefuseBomb):
         self.__updateBombBeacon(message, plan_step)
      if isinstance(plan_step.action, PutOutFire):
         self.__updateHazardBeacon(message, plan_step)

      plan_step.prioritization = message.additional_info.get('prioritization',1)

      # Update any responses
      for participant_id, response in message.additional_info.get('responses',{}).items():
         plan_step.responses[participant_id] = response

         if response == 'I can handle this myself!':
            plan_step.action.actors.add(participant_id)
         elif response == 'I can provide support!':
            plan_step.action.add_actor(participant_id)
         elif response == 'Someone other than me should help!':
            plan_step.action.remove_actor(participant_id)
         elif response == 'I agree with your plan!':
            pass
         elif response == 'I disagree with your plan!':
            pass
         elif response == '':
            # No response.  This is added to avoid logging the warning message
            pass
         else:
            self.logger.warning(f'{self}:  Not sure what to do with response "{response}"')

   def __processNotSet(self, message):
      """
      Process a message with a Planning Bomb Undo meta-action.

      Arguments
      ---------
      message : MinecraftBridge.message.UI_Click
         Message to process
      """

      self.__pretty_print_ui_message(message)


   def __onUI_Click(self, message):
      """
      Callback when a UI_Click message is received

      Arguments
      ---------
      message : MinecraftBridge.messages.UI_Click
         Received message
      """

      self.logger.debug(f'{self}:  Received message {message}')

      # Extract the meta_action, if present in the message's additional_info
      meta_action = message.additional_info.get('meta_action', None)
      if meta_action is None:
         self.logger.debug(f'{self}:    No meta_action present in additional_info')
         return

      # Cast the meta_action to one of the enumerated values (if possible)
      try:
         meta_action = MetaAction(meta_action)
      except:
         self.logger.error(f'{self}:    Error when casting meta_action: {meta_action}')
         return


      # Construct an event based on the meta_action performed
      if meta_action == MetaAction.PlanningFlagPlaced:
         self.__processPlanningFlagPlaced(message)
      elif meta_action == MetaAction.UndoPlanningFlagPlaced:
         self.__processUndoPlanningFlag(message)
      elif meta_action == MetaAction.PlanningFlagUpdate:
         self.__processPlanningFlagUpdate(message)
      elif meta_action == MetaAction.PlanningBombPlaced:
         self.__processPlanningBombPlaced(message)
      elif meta_action == MetaAction.UndoPlanningBombPlaced:
         self.__processUndoPlanningBomb(message)
      elif meta_action == MetaAction.PlanningBombUpdate:
         self.__processPlanningBombUpdate(message)
      elif meta_action == MetaAction.PlanningBeaconUpdate:
         self.__processPlanningBeaconUpdate(message)
      elif meta_action == MetaAction.NotSet:
         self.__processNotSet(message)
      # Might not need to include these...
##      elif meta_action == MetaAction.VoteLeaveStore:
##         self.logger.debug(f'{self}:    Vote Leave Store')
##      elif meta_action == MetaAction.ProposeToolPurchaseIncrease:
##         self.logger.debug(f'{self}:    Propose Tool Purchase Increase')
##      elif meta_action == MetaAction.ProposeToolPurchaseDecrease:
##         self.logger.debug(f'{self}:    Propose Tool Purcase Decrease')
##      elif meta_action == MetaAction.NotSet:
##         self.logger.debug(f'{self}:    Not Set')
##      else:
##         self.logger.warning(f'{self}:    meta_action {meta_action} seems to be unsupported')

