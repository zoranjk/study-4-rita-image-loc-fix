from MinecraftBridge.utils import Loggable

class NullPlan(Loggable):
	"""
	A NullPlan is a plan representation that is not intended to be used.
	When queried, it simply raises warnings that a NullPlan is being operated
	on.
	"""

	def __init__(self, name="Null Plan"):
		"""
		Arguments
		---------
		name : str
			Unique name of this plan
		"""

		self._name = name


	def __str__(self):
		"""
		String representation of this plan
		"""

		return f'{self.__class__.__name__} [{self.name}]'


	@property
	def name(self):
		return self._name


	def add_step(self, step):
		"""
		Add a planning step to the representation
		"""

		self.logger.warning(f'{self}:  Calling `add_step` on a NullPlan instance')


	def get_step(self, id_):
		"""
		Get a step by its identifier.

		Arguments
		---------
		id_
			Step identifier
		"""

		self.logger.warning(f'{self}:  Calling `get_step` on a NullPlan instance')



	def remove_step(self, id_):
		"""
		Remove the step with the given identifier.

		Arguments
		---------
		id_
			Identifier of the step to remove
		"""

		self.logger.warning(f'{self}:  Calling `remove_step` on a NullPlan instance')


	def clone(self, name=None):
		"""
		"""

		return NullPlan()