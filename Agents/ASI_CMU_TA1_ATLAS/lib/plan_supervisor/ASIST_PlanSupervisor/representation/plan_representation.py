from MinecraftBridge.utils import Loggable

class Plan(Loggable):
	"""
	"""

	def __init__(self, name):
		"""
		Arguments
		---------
		name : str
			Unique name of this plan
		"""

		self._name = name

		self._steps = {}

		# _number_steps is an internal value that provides a unique integer
		# for each step number
		self._current_step_number = 0


	def __str__(self):
		"""
		String representation of this plan
		"""

		return f'{self.__class__.__name__} [{self.name}]'


	def __len__(self):
		"""
		Returns the number of current steps in the plan
		"""

		return len(self._steps)


	@property
	def name(self):
		return self._name


	def add_step(self, step):
		"""
		Add a planning step to the representation
		"""

		if step.id in self._steps:
			self.logger.warning(f'{self}:  Replacing existing step {self._steps[step.id]} with {step}; same step ID')

		self.logger.debug(f'{self}:  Adding {step}')

		self._steps[step.id] = step

		# Set the step's sequence number to the current step_number
		step._sequence_number = self._current_step_number
		self._current_step_number += 1


	def get_step(self, id_):
		"""
		Get a step by its identifier.

		Arguments
		---------
		id_
			Step identifier
		"""

		if not id_ in self._steps:
			self.logger.warning(f'{self}:  Step ID {id_} does not exist in this representation.')
			return None

		return self._steps[id_]	


	def remove_step(self, id_):
		"""
		Remove the step with the given identifier.

		Arguments
		---------
		id_
			Identifier of the step to remove
		"""

		if not id_ in self._steps:
			self.logger.warning(f'{self}:  Step ID {id_} does not exist in this representation.')
			return

		del self._steps[id_]


	def query(self, query, sort_by=None, descending=False):
		"""
		Return a set of steps filtered by query, optionally sorted by a given
		sort_by function

		Arguments
		---------
		query : QueryBy

		sort_by : SortBy
			Function that sorts the resulting selected steps
		"""

		queried_steps = query(self._steps.values())

		if sort_by is not None:
			queried_steps = sort_by(queried_steps, descending)

		return queried_steps


	def clone(self, name=None):
		"""
		Create a clone of this plan
		"""

		# What to name this plan?  (NOTE and TODO:  need to ensure that the 
		# clone name is unique if not provided)
		clone_name = name if name is not None else self.name + '-clone'

		clone = Plan(clone_name)

		# Copy over all the steps in this plan
		for step in self._steps:
			pass
