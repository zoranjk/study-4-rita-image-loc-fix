

class QueryBy:
	"""
	Set of functions designed to select plan steps based on some criteria.
	Each function is a static method (to maintain namespace) that produces a
	list of plan steps by selecting appropriate steps from a provided list of
	plan steps.
	"""

	@staticmethod
	def And(*functions):
		"""
		Create a function that returns the intersection of filter output

		Arguments
		---------
		functions
			List of QueryBy filters.
		"""

		def filter_function(steps):
			"""
			Calculate the intersection of filtered steps
			"""

			return list(set.intersection(*[set(f(steps)) for f in functions]))

		return filter_function


	@staticmethod
	def Or(*functions):
		"""
		Create a function that returns the union of filter output

		Arguments
		---------
		functions
			List of QueryBy filters.
		"""

		def filter_function(steps):
			"""
			Calculate the union of filtered steps
			"""

			return list(set.union(*[set(f(steps)) for f in functions]))

		return filter_function


	@staticmethod
	def Not(function):
		"""
		Create a function that returns steps that are not satisfied by the
		provided filter function

		Arguments
		---------
		funciton
			Function to calculate the difference of
		"""

		def filter_function(steps):
			"""
			Calculate the set difference of the original and filtered steps
			"""

			return list(set.difference(set(steps), set(function(steps))))

		return filter_function


	@staticmethod
	def Actor(actor_id):
		"""
		Creates a function that returns steps if the Action of the step has the
		given id in its set of actors
		"""

		def filter_function(steps):
			"""
			"""

			return [step for step in steps if actor_id in step.action.actors]

		return filter_function


	@staticmethod
	def IsComplete(steps):
		"""
		Return only steps that are complete
		"""

		return [step for step in steps if step.is_complete]


	@staticmethod
	def ActionIs(ActionClass):
		"""
		Create a function that returns steps if the type of action of the step
		matches the given ActionClass
		"""

		def filter_function(steps):
			"""
			"""

			return [step for step in steps if isinstance(step.action, ActionClass)]

		return filter_function


	@staticmethod
	def ContainsResponseText(response_text):
		"""
		Creates a function that returns any steps that contain a response whose
		text matches that of the provided response_text
		"""

		def filter_function(steps):
			"""
			"""

			return [step for step in steps if response_text in step.responses.values()]

		return filter_function





