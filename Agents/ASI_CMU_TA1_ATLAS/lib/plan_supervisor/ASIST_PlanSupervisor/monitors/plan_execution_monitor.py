# -*- coding: utf-8 -*-
"""
.. module:: monitors.plan_execution_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Class for monitoring execution of a plan an updating 

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

This class monitors events in the environment and updates the state of an
active plan.
"""

import collections
import math
import functools
import operator

from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import (
   PlayerState,
   ObjectStateChange,
   EnvironmentRemovedSingle
)

from .step_monitor import *

from ..plan_components.actions import *

from ..representation.query import QueryBy
from ..representation import NullPlan


def distance(point1, point2):
   """
   Helper function to calculate Euclidean distance
   """

   x1,y1 = point1
   x2,y2 = point2

   return math.sqrt((x2 - x1)**2 + (y2 - y1)**2)




class PlanExecutionMonitor(Loggable):
   """
   A PlanExecutionMonitor observes events in the environment, and identifies
   1) when plan steps are completed, and 2) when preconditions become satisfied
   or unsatisfied, and updates the state of an active plan representation
   accordingly.

   PlanStep Completion Criteria
   ----------------------------
   Criteria for indicating if a step is complete is indicated for each action
   below::

   * GoToWaypoint - satisfied if all actors are within a distance threshold to
                    the waypoint location

   * Search - # TODO:  figure out this one

   Parameters
   ----------
   * waypoint_distance_threshold - maximum distance to a waypoint to be 
                                   considered at the waypoint

   Attributes
   ----------
   plan
      Plan being monitored and updated

   Methods
   -------
   """

   ActionMonitorMap = { GoToWaypoint:  GoToWaypointMonitor,
                        Search:        SearchMonitor,
                        DefuseBomb:    DefuseBombMonitor,
                        AddressHazard: AddressHazardMonitor,
                        PutOutFire:    PutOutFireMonitor,
                        GroupUp:       GroupUpMonitor
   }

   def __init__(self, supervisor, plan, minecraft_interface, **kwargs):
      """
      Arguments
      ---------
      supervisor : PlanSupervisor

      plan : Plan

      minecraft_interface : MinecraftBridge.CallbackInterface
      """

      self._supervisor = supervisor
      self._plan = plan
      self._minecraft_interface = minecraft_interface

      if self._plan is None:
         self._plan = NullPlan()

      # PlanStepMonitors
      self.__plan_step_monitors = {}


###      # Store properties
###      self._waypoint_distance_threshold = kwargs.get('waypoint_distance_threshold', 5.0)
###
###      # Need to keep track of where players are, for plan step that consider
###      # multiple actors.  Default a player's position to something very far
###      # away for considering actions about distances to things.
###      self._player_positions = collections.defaultdict(lambda: (-1000,-1000))
###
###      # Keep a dictionary mapping participant id to a list of plan steps for 
###      # the participant
###      self._player_plan_steps = collections.defaultdict(list)



   @property
   def plan(self):
      return self._plan

   @plan.setter
   def plan(self, plan):
      self._plan = plan

      # This plan may have established steps, so update the list of plan steps
      # for each player
      self.__plan_step_monitors.clear()
      self.__update_plan_step_monitors()


   def __str__(self):
      """
      String representation of the object
      """

      return f'{self.__class__.__name__} <{self._plan}>'


###   def __update_participant_steps(self):
###      """
###      Update the dictionary of plan steps associated with each given player.
###      """
###
###      self.logger.debug(f'{self}:  Updating Participant Plan Steps Dictionary')
###      for participant_id in self._supervisor.participants:
###         # Get all the plan steps that involve this participant as an actor
###         query = QueryBy.And(QueryBy.Actor(participant_id),
###                             QueryBy.Not(QueryBy.IsComplete))
###         self._player_plan_steps[participant_id] = self._plan.query(query)

   def __update_plan_step_monitors(self):
      """
      Update the dictionary of plan steps associated with each given player.
      """

      self.logger.debug(f'{self}:  Updating Participant Plan Steps Dictionary')
      for participant_id in self._supervisor.participants:
         # Get all the plan steps that involve this participant as an actor
         query = QueryBy.Not(QueryBy.IsComplete)

         for step in self._plan.query(query):
            if not step in self.__plan_step_monitors:

               self.__plan_step_monitors[step] = PlanExecutionMonitor.ActionMonitorMap[step.action.__class__](step)


   def disconnect(self):
      """
      Disconnect any connections
      """

      self._minecraft_interface.deregister_callback(self.__onPlayerState)
      self._minecraft_interface.deregister_callback(self.__onObjectStateChange)
      self._minecraft_interface.deregister_callback(self.__onEnvironmentRemovedSingle)   


   def execution_mode(self):
      """
      Indicate that the plan is being executed.
      """

      self.logger.info(f'{self}:  Beginning execution monitoring of plan {self._plan}')

      self.__update_plan_step_monitors()

###      self.__update_participant_steps()

      self._minecraft_interface.register_callback(PlayerState, self.__onPlayerState)
      self._minecraft_interface.register_callback(ObjectStateChange, self.__onObjectStateChange)
      self._minecraft_interface.register_callback(EnvironmentRemovedSingle, self.__onEnvironmentRemovedSingle)      


   def development_mode(self):
      """
      Indicate that the plan is being developed.
      """

      self.logger.info(f'{self}:  Beginning development monitoring of plan {self._plan}')

      self._minecraft_interface.deregister_callback(self.__onPlayerState)
      self._minecraft_interface.deregister_callback(self.__onObjectStateChange)
      self._minecraft_interface.deregister_callback(self.__onEnvironmentRemovedSingle)      


###   def __checkGoToWaypointCompleteness(self, participant_id):
###      """
###      Checks and updates all incomplete plan steps whose action are for one or
###      more participants to go to a waypoint, and flags the step as complete if
###      the criteria is met.
###      """
###
###      # Create a temporary function to determine if a player is within the 
###      # distance to a waypoint
###      def check_distance(waypoint, p_id):
###         player_distance = distance(waypoint, self._player_positions[p_id])
###         return player_distance <= self._waypoint_distance_threshold
###
###      # Get all the plan steps that involve this player
###      for plan_step in self._player_plan_steps[participant_id]:
###
###         # Only consider incomplete plan steps whose actions are GoToWaypoint
###         if not plan_step.is_complete \
###            and isinstance(plan_step.action, GoToWaypoint):
###
###            # What's the location of the waypoint?
###            waypoint = plan_step.action.world_location
###
###            # Are all the players within the waypoint threshold?  If so, then mark
###            # the plan_step as complete
###            within_threshold = [check_distance(waypoint, actor) 
###                                for actor in plan_step.action.actors]
###
###            if functools.reduce(operator.and_, within_threshold):
###               self.logger.debug(f'{self}:  Plan Step {plan_step} is completed.')
###               plan_step.is_complete = True


   def __onPlayerState(self, message):
      """
      Callback when a PlayerState message is received.
      """

      # Let all the monitors know about the updated player state
      for monitor in self.__plan_step_monitors.values():
         monitor.onPlayerState(message)

   def __onObjectStateChange(self, message):
      """
      Callback when an ObjectStateChange message is received
      """

      # Let all the monitors know about the updated object state
      for monitor in self.__plan_step_monitors.values():
         monitor.onObjectStateChange(message)

   def __onEnvironmentRemovedSingle(self, message):
      """
      Callback when an EnvironmentRemovedSingle message is received
      """

      # Let all the monitors know about the removed environment block
      for monitor in self.__plan_step_monitors.values():
         monitor.onEnvironmentRemovedSingle(message)

###      # Store the player's position
###      self._player_positions[message.participant_id] = (message.x, message.z)
###
###      # Do all the updates
###      self.__checkGoToWaypointCompleteness(message.participant_id)





