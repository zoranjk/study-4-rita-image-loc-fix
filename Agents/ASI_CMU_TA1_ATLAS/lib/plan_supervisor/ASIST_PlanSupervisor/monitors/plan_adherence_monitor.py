# -*- coding: utf-8 -*-
"""
.. module:: monitors.plan_adherence_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Class for monitoring participant adherence to a plan

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

This monitor observes participant behaviors in the environment and identifies
when participants deviate from the current active plan.
"""

from MinecraftBridge.utils import Loggable

from MinecraftBridge.messages import PlayerState


class PlanAdherenceMonitor(Loggable):
   """
   A PlanAdherenceMonitor observes participant behaviors during execution of a
   plan, and identifies when a player's behavior deviates from the active plan.
   When deviation occurs, the monitor indicates to an attached gateway to emit
   a message indicating who deviated on which step.

   Attributes
   ----------
   plan
      Plan being monitored and updated

   Methods
   -------
   """

   def __init__(self, supervisor, plan, minecraft_interface, message_gateway):
      """
      Arguments
      ---------
      supervisor : PlanSupervisor

      plan : Plan

      minecraft_interface : MinecraftBridge.CallbackInterface

      message_gateway : 
         Message gateway to alert if deviation from the plan occurs
      """

      self._supervisor = supervisor
      self._plan = plan
      self._minecraft_interface = minecraft_interface
      self._message_gateway = message_gateway


   @property
   def plan(self):
      return self._plan

   @plan.setter
   def plan(self, plan):
      self._plan = plan


   def __str__(self):
      """
      String representation of the object
      """

      return f'{self.__class__.__name__} <{self._plan}>'


   def disconnect(self):
      """
      Disconnect any connections
      """

      self._minecraft_interface.deregister_callback(self.__onPlayerState)
      

   def execution_mode(self):
      """
      Indicate that the plan is being executed.
      """

      self.logger.info(f'{self}:  Beginning execution monitoring of plan {self._plan}')

      self._minecraft_interface.register_callback(PlayerState, self.__onPlayerState)


   def development_mode(self):
      """
      Indicate that the plan is being developed.
      """

      self.logger.info(f'{self}:  Beginning development monitoring of plan {self._plan}')

      self._minecraft_interface.deregister_callback(self.__onPlayerState)


   def __onPlayerState(self, message):
      """
      Callback when a PlayerState message is received
      """

      self.logger.debug(f'{self}:  Received {message}')


   def __onDeviation(self, participant_id, plan_stap, message_history):
      """
      Placeholder message when a participant deviates from a plan.

      Arguments
      ---------
      participant_id : str
         Participant who deviated from their next plan step
      plan_step : PlanStep
         Plan step that was deviated
      message_history : List[MinecraftBridge.message]
         List of messages that indicate the deviation
      """

      # Right now, pass on to the message gateway
      self._message_gateway.onDeviation(self, participant_id, plan_step, self._plan, message_history)
