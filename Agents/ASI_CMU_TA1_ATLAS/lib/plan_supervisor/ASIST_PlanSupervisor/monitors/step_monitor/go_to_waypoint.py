"""
"""

import math
import functools

from .plan_step_monitor import PlanStepMonitor

class GoToWaypointMonitor(PlanStepMonitor):
	"""
	"""

	@staticmethod
	def distance(p1, p2):

		return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[0])**2)

	def __init__(self, plan_step):
		"""
		"""

		PlanStepMonitor.__init__(self, plan_step)

		self.__min_distance_to_waypoint = 300.0

		# Keep track of the actors to see if they're at the waypoint.
		self.__at_waypoint = { actor: False for actor in self._plan_step.action.actors }


	def onPlayerState(self, message):
		"""
		Arguments
		---------
		message : MinecraftBridge.messages.PlayerState
			PlayerState message
		"""

		# Is this about one of the actors?
		if not message.participant_id in self.__at_waypoint:
			# Nothing to do with this
			return

		dist = GoToWaypointMonitor.distance((message.x, message.z), 
			                                self._plan_step.action.location)

		self.__at_waypoint[message.participant_id] = (dist <= self.__min_distance_to_waypoint)

		# Are all the actors accounted for?  Indicate that this step is done, 
		# and who performed the step
		if functools.reduce(lambda a,b: a and b, self.__at_waypoint.values()):
			self._plan_step.is_complete = True
			self._is_successful = True

			for actor in self.__at_waypoint.keys():
				self.add_completer(actor)





