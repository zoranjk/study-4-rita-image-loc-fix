# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.plan_components.actions.defuse_bomb
   :platform: Linux, Windows, OSX
   :synopsis: Plan Step indicating that a bomb is to be defues

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

DefuseBomb is an indicating that one or more participants plans on defusing an
identified bomb
"""

import copy

from .action import Action

class DefuseBomb(Action):
   """
   Attributes
   ----------
   bomb_id : str
      String identifier of the bomb in the Minecraft Environment
   remaining_sequence : List[str] or None
      Remaining defusal sequence, or None if unknown
   fuse_start_time : Tuple[int]
      Start time of the fuse, in minutes:seconds, (-1,-1) if unknown
   """

   def __init__(self, location, **kwargs):
      """
      Arguments
      ---------
      location : Tuple[int]
         (x,z) location
      """

      Action.__init__(self, location)

###      self._step_number = step_number
###      self._participant_id = participant_id

      self.bomb_id = None
      self.remaining_sequence = None
      self.chained_id = None
      self.fuse_start_time = (-1,-1)

###      @property
###      def bomb_id(self):
###         return self._bomb_id
###
###      @bomb_id.setter
###      def bomb_id(self, id_):
###         # TODO: Issue a warning (or do not allow?) the ID to be reset from None
###         self._bomb_id = id_

###      @property
###      def chained_id(self):
###         return self._chained_id
###
###
###      @chained_id.setter
###      def chained_id(self, id_):
###         # TODO: Issue a warning (or do not allow?) the chained_id to be reset
###         self._chained_id = id_      
###
###      @property
###      def fuse_start_time(self):
###         return self._fuse_start_time
###
###      @fuse_start_time.setter
###      def fuse_start_time(self, time):
###         self._fuse_start_time = time
      
      

###   @property
###   def step_number(self):
###      return self._step_number
###
###   @property
###   def participant_id(self):
###      return self._participant_id

   def serialize(self):
      """
      Return a dictionary representation of this action
      """

      serialized_dict = Action.serialize(self)

      serialized_dict['bomb_id'] = self.bomb_id
      serialized_dict['remaining_sequence'] = copy.copy(self.remaining_sequence)
      serialized_dict['chained_id'] = self.chained_id
      serialized_dict['fuse_start_time'] = self.fuse_start_time

      return serialized_dict


   def clone(self):
      """
      Return a copy of this action.
      """

      action = DefuseBomb(self._location)