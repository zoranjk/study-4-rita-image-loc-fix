# -*- coding: utf-8 -*-
"""
.. module:: plan_monitor.plan_components.actions.address_hazard
   :platform: Linux, Windows, OSX
   :synopsis: Plan Step indicating that a hazard is to be addressed

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

AddressHazard is an action indicating that a hazard is to be addressed.  This
may be replaced with more specific hazards.
"""

from .action import Action

class AddressHazard(Action):
   """
   """

   def __init__(self, location, **kwargs):
      """
      Arguments
      ---------
      step_number : int
         Which step number is this?
      location : Tuple[int]
         (x,z) location
      """

      Action.__init__(self, location)

###      self._step_number = step_number
###      self._participant_id = participant_id

###   @property
###   def step_number(self):
###      return self._step_number
###
###   @property
###   def participant_id(self):
###      return self._participant_id


   def clone(self):
      """
      Return a copy of this action.
      """

      raise NotImplementedError