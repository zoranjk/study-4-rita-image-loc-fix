# -*- coding: utf-8 -*-
"""
.. module:: intervention_library.leadership_transformer
    :platform: Linux, Windows, OSX
    :synopsis: Class for transforming interventions based on leadership profile
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

The LeadershipTransformer will reroute interventions to an individual if that
individual has the highest leadership score and is a sufficiently high
leadership score.
"""

from collections import defaultdict


from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import GELP

from InterventionManager.interventions import InterventionProxy


class LeadershipTransformer(Loggable):
    """
    The LeadershipTransformer monitors GELP messages to update participant
    leadership scores.  Upon receiving an intervention, if an individual is
    exhibiting high enough leadership, then messages will be transformed to 
    be rerouted to the leader.
    """

    def __init__(self, manager, **kwargs):
        """
        """

        self._manager = manager

        # Leadership scores of each participant, and the minimum leadership
        # score needed to reroute a message
        self.leadership_scores = defaultdict(lambda: 0)
        self._min_leadership_score = kwargs.get("min_leadership_score", 8.0)

        # Register to receive GEM messages in order to check on leadership
        # scores
        self._manager.minecraft_interface.register_callback(GELP, self.__onGELP)


    def __onGELP(self, message):
        """
        Callback when GELP messages are recieved.  Used to update participant
        leadership profiles of participants
        """

        pass


    def __call__(self, intervention):
        """
        """

        intervention_proxy = None

        # Is there a leader?
        leader = None
        highest_leadership_score = 0.0

        for participant_id, score in self.leadership_scores.items():
            if score >= self._min_leadership_score:
                highest_leadership_score = max(score, highest_leadership_score)
                if score == highest_leadership_score:
                    leader = participant_id

        # If there's no leader, do nothing and return
        if leader is None:
            return None


        # Otherwise, create a proxy based on the message class
        if intervention.__class__.__name__ == "ResolvePlanDisagreementIntervention":
            # Original message, broadcast to team:  "There still seems to be some disagreement with the plan, you should stay in the store to resolve it."
            proxy = InterventionProxy(intervention)
            proxy.set_recipients([leader])
            proxy.set_message("The team seems to disagree on part of the plan, I would suggest asking your teammates if they're clear on the plan, and resolve any disagreement.")
            return proxy
        elif intervention.__class__.__name__ == "CognitiveLoadStaleMarkerBlocksIntervention":
            # Original message, broadcast to team:  "There are quite a few stale hazard beacons confusing the team.  I would suggest removing excess hazard beacons."
            proxy = InterventionProxy(intervention)
            proxy.set_recipients([leader])
            proxy.set_message("There are quite a few stale hazard beacons confusing the team.  I would suggest assigning someone to removing excess hazard beacons.")
            return proxy
        elif intervention.__class__.__name__ == "":
            # Original message, broadcast to team: "Team, you seem to have come up with several plan steps that aren't reflected in the cognitive artifact.  You should update the map to indicate who will be performing the discussed tasks."
            proxy = InterventionProxy(intervention)
            proxy.set_recipients([leader])
            proxy.set_message("The team has discussed several plan steps not reflected in the cognitive artifact.  You should encourage your teammates to update the map with their proposed tasks.")
            return proxy
        else:
            return None