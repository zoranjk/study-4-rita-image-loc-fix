# -*- coding: utf-8 -*-
"""
.. module:: intervention_library.resolution_pipeline
    :platform: Linux, Windows, OSX
    :synopsis: Module containing resolution pipeline components
.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

from .leadership_transformer import LeadershipTransformer

transformers = [ 
    LeadershipTransformer,
]

filters = [

]

aggregators = [

]