# -*- coding: utf-8 -*-
"""
.. module:: ignore_bomb_due_to_fire.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention to encourage players to not run through fire, and to
              propose an alternative.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to encourage a player 
to put out fires before they reach bombs and cause them to explode.
"""

import math

from InterventionManager import Intervention

import MinecraftElements

from MinecraftBridge.messages import (
   PlayerState,
   EnvironmentRemovedSingle,
   EnvironmentRemovedList
)

class IgnoreBombDueToFireIntervention(Intervention):
   """
   A DontRunThroughFireIntervention is an Intervention which monitors the
   location of a player relative to fires, and alerts the player of alternative
   actions if it appears they're going to run through fire.
   """

   def __init__(self, manager, fire_monitor, bomb_id, bomb_location, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Manager for this intervention

      Keyword Arguments
      -----------------
      fire_spread_rate : float, default = 6.0
         Number of milliseconds until a fire advances one block
      player_velocity : float, default = 4.0
         Number of blocks a player can move per second
      """

      # Default values for intervention prioritization information
      kwargs["priority"] = kwargs.get("priority", 1)
      kwargs["expiration"] = kwargs.get("expiration", 30000)
      kwargs["view_duration"] = kwargs.get("view_duration", 15000)

      Intervention.__init__(self, manager, **kwargs)

      self._fire_monitor = fire_monitor
      self._fire_monitor.add_observer(self)

      self._bomb_id = bomb_id
      self._bomb_location = bomb_location

      self._fire_spread_rate = kwargs.get("fire_spread_rate", 15.0)
      self._player_velocity = kwargs.get("player_velocity", 4.6)

      # The `onNewFire` method cannot queue this intervention for resolution,
      # as this will cause iterator issues with the MinecraftBridge.  So, 
      # simply set a flag, and check with PlayerState messages
      self._resolve = False

      self._player_time = -1
      self._fire_time = -1

      # Register callbacks
#      self.add_minecraft_callback(PlayerState, self.__onPlayerState)
      self.add_minecraft_callback(EnvironmentRemovedList, self.__onEnvironmentRemovedList)
      self.add_minecraft_callback(EnvironmentRemovedSingle, self.__onEnvironmentRemovedSingle)


      # Intervention message and target participants.
      self._recipients = [p.id for p in self.manager.participants]
      self._message = 'I would suggest not bothering trying to defuse BOMB-??? in section ???---it will likely be destroyed by fire before you can.'


   @property
   def bomb_id(self):
      return self._bomb_id

   @property
   def bomb_location(self):
      return self._bomb_location

   @property
   def bomb_section(self):
      return self.__location_to_map_section(self.bomb_location)


   def __distance_to_bomb(self, p1):

      p0 = self._bomb_location

      return math.sqrt((p0[0] - p1[0])**2 + (p0[2] - p1[2])**2)


   def __distance_to_bomb_manhattan(self, p1):

      p0 = self._bomb_location

      return int(math.fabs(p0[0] - p1[0]) + math.fabs(p0[2] - p1[2]))


   def __location_to_map_section(self, location):
      """
      """

      row_number = max(min(int(9*location[2]/150),8),0)
      col_number = max(min(int(7*location[0]/50),6),0) + 1

      row_letter = ['A','B','C','D','E','F','G','H','I'][row_number]

      return row_letter + str(col_number)


   def __onPlayerState(self, message):
      """
      """

      if self._resolve:
         self.queueForResolution()


   def __onEnvironmentRemovedSingle(self, message):
      """
      Arguments
      ---------
      message : MinecraftBridge.messages.EnvironmentRemovedSingle
         Received EnvironmentRemovedSingle message
      """

      self.logger.debug(f"{self}: Received Message {message}")

      # Is the message 1) about a bomb, 2) whose ID is the intervention's
      # bomb ID, and 3) has exploded due to fire?
      if not message.object.type in MinecraftElements.Block.bombs():
         self.logger.debug(f'{self}:   Not about a bomb')
         return
      if message.object.id != self._bomb_id:
         self.logger.debug(f'{self}:   Message ID {message.object.id} not equal to intervention bomb id {self._bomb_id}')
         return

      # No need to consider this intervention anymore -- the bomb was removed
      self._fire_monitor.remove_observer(self)
      self.discard()


   def __onEnvironmentRemovedList(self, message):
      """
      Arguments
      ---------
      message : MinecraftBridge.messages.EnvironmentRemovedList
         Received EnvironmentRemovedList message
      """

      self.logger.debug(f"{self}: Received Message {message}")

      for object_ in message.objects:

         # Is the object 1) a bomb, 2) whose ID is the intervention's bomb ID,
         # and 3) has exploded due to fire?
         if message.object.type in MinecraftElements.Block.bombs() and \
            message.object.id == self._bomd_id:

            # No need to consider this intervention anymore
            self._fire_monitor.remove_observer(self)
            self.discard()
            return


   def onFireGroupRemoved(self, group_id):
      """
      """

      # ignore this
      pass


   def onNewFire(self, location):
      """
      Figure out how far awa the fire is from the bomb, how long until it
      reaches the bomb, and whether players can reach it in time.

      Arguments
      ---------
      location : (x,y,z) location of the fire
      """

      self.logger.debug(f'{self}:  Notified of a new fire at {location}')

      # How long until the fire reaches the bomb?
      bomb_to_fire_distance = self.__distance_to_bomb_manhattan(location)
      time_for_fire_to_reach_bomb = bomb_to_fire_distance * self._fire_spread_rate


      # Is the fire location at the bomb location?  This could be due to the 
      # fire being created when certain bombs go off, so the bomb is already
      # gone (and players would have been informed by the system)
      if bomb_to_fire_distance == 0:
         self.logger.debug(f"{self}:  Discarding:  Fire location {location} is same as bomb location {self._bomb_location}")
         self.discard()
         return

      # How long until the each player can reach the bomb?

      # TODO:  This is a very simple approach to determining player's ability 
      #        to defuse the bomb.  Would need to modify this to check that 
      #        tool requirements are satisfied.
      player_distances = [self.__distance_to_bomb(player.location) 
                          for player in self.manager.participant_info.values()]
      time_for_player_to_reach_bomb = [ distance / self._player_velocity
                                        for distance in player_distances ]
      # Who'se the closest player?
      closest_player_time = min(time_for_player_to_reach_bomb)

      # Will it take longer for the closest player to reach the bomb than
      # the fire?  Then resolve.
      if closest_player_time > time_for_fire_to_reach_bomb:

         self._fire_monitor.remove_observer(self)
         self._player_time = closest_player_time
         self._fire_time = time_for_fire_to_reach_bomb
         self._fire_location = location
#         self._resolve = True
         self.queueForResolution()



   def getInterventionInformation(self):
      """
      Provide a dictionary containing a information necessary to present the
      intervention.
      """
      return {     
         'default_recipients': self._recipients,
         'default_message': f"I would suggest not bothering trying to defuse {self._bomb_id} in section {self.bomb_section}---it will likely be destroyed by fire before you can reach it.", # [Player Time: {self._player_time}, Fire Time: {self._fire_time}, Fire Location: {self._fire_location}, Bomb Location: {self._bomb_location}]",
         'default_responses': []
      }