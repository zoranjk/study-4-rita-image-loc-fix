# -*- coding: utf-8 -*-
"""
.. module:: fire_reach_bomb
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to encourage 
      players to put out fires before they reach bombs and cause them
      to explode.

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to inform players encourage a player to encourage 
players to put out fires before they reach bombs and cause them to explode,
resulting in lower maximum score possible

* Trigger: The intervention is triggered when a fire causes a bomb to 
           explode.

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  N/A
"""

from .intervention import FireReachBombIntervention
from .trigger import FireReachBombTrigger

InterventionClass = FireReachBombIntervention
TriggerClass = FireReachBombTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []