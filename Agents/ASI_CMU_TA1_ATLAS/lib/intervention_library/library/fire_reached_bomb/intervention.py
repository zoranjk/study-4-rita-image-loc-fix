# -*- coding: utf-8 -*-
"""
.. module:: fire_reach_bomb.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention to encourage players to put out fires 
      before they reach bombs and cause them to explode.

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of an Intervention class designed to encourage a player 
to put out fires before they reach bombs and cause them to explode.
"""

import math

from InterventionManager import Intervention

import MinecraftElements

from MinecraftBridge.messages import (
   EnvironmentRemovedSingle,
   EnvironmentRemovedList
)

class FireReachBombIntervention(Intervention):
   """
   An EvacuateBombIfFailingIntervention is an Intervention which monitors the
   active bombs near an agent and encourages the agent to evacuate an active
   bomb if agent doesn't have the required tools to help defuse the bomb
   """

   def __init__(self, manager, bomb_id, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Manager for this intervention
      bomb_id : str
         Identifier of the bomb being tracked
      """

      # Default values for intervention prioritization information
      kwargs["priority"] = kwargs.get("priority", 2)
      kwargs["expiration"] = kwargs.get("expiration", 20000)
      kwargs["view_duration"] = kwargs.get("view_duration", 15000)

      Intervention.__init__(self, manager, **kwargs)

      self._bomb_id = bomb_id

      # How close does a player need to be to intervene?
      self._player_distance = kwargs.get('minimum_player_distance', 15.0)

      # Register callbacks
      self.add_minecraft_callback(EnvironmentRemovedSingle, self.__onEnvironmentRemovedSingle)
      self.add_minecraft_callback(EnvironmentRemovedList, self.__onEnvironmentRemovedList)


      # Intervention message and target participants.
      self._recipients = []
      self._message = 'A bomb near you recently exploded due to fire. Please put out fires before they reach bombs to maximize score.'


   def __distance(self, p0, p1):

      return math.sqrt((p0[0] - p1[0])**2 + (p0[2] - p1[2])**2)



   def __resolve_for_bomb(self, bomb_object):
      """
      Determine if players are close enough to the bomb to send an intervention
      to
      """

      bomb_location = bomb_object.x, bomb_object.y, bomb_object.z

      # Add any participants who are within the threshold distance to the
      # bomb
      for participant in self.manager.participants:
         participant_location = self.manager.participant_info[participant.id].location
         if self.__distance(bomb_location, participant_location) <= self._player_distance:
            self._recipients.append(participant.id)

      self.logger.debug(f'{self}:  Participants close to the bomb:  {self._recipients}')

      # If no one is close to the bomb, then discard this intervention.
      # Otherwise, queue for resolution
      if len(self._recipients) == 0:
         self.discard()
      else:
         self.queueForResolution()


   def __onEnvironmentRemovedSingle(self, message):
      """
      Arguments
      ---------
      message : MinecraftBridge.messages.EnvironmentRemovedSingle
         Received EnvironmentRemovedSingle message
      """

      self.logger.debug(f"{self}: Received Message {message}")

      # Is the message 1) about a bomb, 2) whose ID is the intervention's
      # bomb ID, and 3) has exploded due to fire?
      if not message.object.type in MinecraftElements.Block.bombs():
         self.logger.debug(f'{self}:   Not about a bomb')
         return
      if message.object.id != self._bomb_id:
         self.logger.debug(f'{self}:   Message ID {message.object.id} not equal to intervention bomb id {self._bomb_id}')
         return
      if message.object.get_attribute('outcome') != 'EXPLODE_FIRE':
         self.logger.debug(f'{self}:   Bomb did not explode due to fire')
         return

      # The bomb exploded due to fire, check to see if an intervention should
      # be issued, and to whom

      self.__resolve_for_bomb(message.object)


   def __onEnvironmentRemovedList(self, message):
      """
      Arguments
      ---------
      message : MinecraftBridge.messages.EnvironmentRemovedList
         Received EnvironmentRemovedList message
      """

      self.logger.debug(f"{self}: Received Message {message}")

      for object_ in message.objects:

         # Is the object 1) a bomb, 2) whose ID is the intervention's bomb ID,
         # and 3) has exploded due to fire?
         if message.object.type in MinecraftElements.Block.bombs() and \
            message.object.id == self._bomd_id and \
            message.object.get_attribute('outcome') == 'EXPLODE_FIRE':

            self.__resolve_for_bomb(object_)


   def getInterventionInformation(self):
      """
      Provide a dictionary containing a information necessary to present the
      intervention.
      """

      return {     
         'default_recipients': self._recipients,
         'default_message': self._message,
         'default_responses': ["I agree", "I disagree", "Skip"]
      }