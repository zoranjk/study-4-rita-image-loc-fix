# -*- coding: utf-8 -*-
"""
.. module:: evacuate_bomb_if_failing.individual_compliance
   :platform: Linux, Windows, OSX
   :synopsis: Followup to determine individual compliance to EvacuateBombIfFailing

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of a Followup class designed to determine individual compliance to
advice to encourage a player to evacuate an active bomb if player doesn't have 
the required tools to help defuse the bomb.
"""


from InterventionManager import Followup

from MinecraftBridge.messages import PlayerStateChange

class EvacuateBombIfFailingIndividualCompliance(Followup):
   """
   
   """

   def __init__(self, intervention, manager, **kwargs):
      """
      Arguments
      ---------
      intervention : EvacuateBombIfFailingIntervention
         Intervention instance that this object is followuping up on
      manager : InterventionManager
      """

      Followup.__init__(self, intervention, manager, **kwargs)

      # Listen for player leaving danger zone
      self.add_minecraft_callback(PlayerStateChange, self.__onPlayerStateChange)


   def __distance(self, position1, position2):
      """
      Simple function to determine the distance between two positions.

      Arguments
      ---------
      position1 : Tuple[float]
         (x,y,z) position of point 1
      position2 : Tuple[float]
         (x,y,z) position of point 2
      """

      return math.sqrt(sum(map(lambda x: (x[0]-x[1])**2, 
                              zip(position1, position2))))

   def __onPlayerStateChange(self, message):
      """
      Callback when a player's state has changed

      Arguments
      ---------
      message : MinecraftBridge.message.PlayerStateChange
      """

      self.logger.debug(f'{self}: Received Message: {message}')

      # Determine if this is about the participant that is in danger
      if message.participant_id == self.intervention.participant_id:

         # Check if agent's new position is at least safety distance
         if self.__distance(message.position, self.intervention.bomb_location) >= self.intervention.safety_distance:

            # Intervention presented
            if self.intervention.presented:
               self.participant_complied = True

            self.complete()
