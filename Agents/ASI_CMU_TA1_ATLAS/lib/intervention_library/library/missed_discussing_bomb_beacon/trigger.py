# -*- coding: utf-8 -*-
"""
.. module:: missed_discussing_bomb_beacon.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Missed Discussing Bomb Beacon Interventions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to monitor for
bomb beacons missing actor assignment during planning.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import MissedDiscussingBombBeaconIntervention

class MissedDiscussingBombBeaconTrigger(InterventionTrigger):
    """
    Class that generates MissedDiscussedBombBeaconInterventions.  Interventions
    are triggered at the start of the shop phases.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be spawned at the start of a Shop stage

        if message.mission_stage == MissionStage.ShopStage:
            
            intervention = MissedDiscussingBombBeaconIntervention(self.manager)
            self.logger.info(f'{self}:  Spawning {intervention}')
            self.manager.spawn(intervention)