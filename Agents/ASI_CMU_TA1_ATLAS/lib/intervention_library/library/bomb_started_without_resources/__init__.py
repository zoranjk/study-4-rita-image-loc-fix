# -*- coding: utf-8 -*-
"""
.. module:: bomb_started_without_resources
   :platform: Linux, Windows, OSX
   :synopsis: 

.. moduleauthor:: Ini Oguntola

"""

from .intervention import BombStartedWithoutResourcesIntervention
from .trigger import BombStartedWithoutResourcesTrigger

InterventionClass = BombStartedWithoutResourcesIntervention
TriggerClass = BombStartedWithoutResourcesTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []
