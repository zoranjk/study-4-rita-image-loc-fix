# -*- coding: utf-8 -*-
"""
.. module:: after_action_review_recon.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for providing after action reviews after each field
              phase

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to generate after-action reviews
of the recon phase
"""

import MinecraftElements

from InterventionManager import Intervention

from MinecraftBridge.messages import (
    MissionStageTransition, 
    MissionStage
)


class AfterActionReviewFieldIntervention(Intervention):
    """
    An AfterActionReviewFieldIntervention generates after-action reviews of the
    field phase.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 20000)
        kwargs["present_immediately"] = kwargs.get("present_immediately", True)    

        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        # Thresholds and parameters
        self._small_fire_level = 20
        self._large_fire_level = 40
        self._additional_fire_threshold = 10

        self._low_bomb_rate = 1.5
        self._high_bomb_rate = 6.0
        self._low_bomb_team_failure_rate = 0.1
        self._high_bomb_team_failure_rate = 0.5


        # Variables to keep track of
        self._num_fires_at_start = 0
        self._num_fires_at_end = 0

        self._defused_bombs_at_start = None
        self._defused_bombs_at_end = None
        self._defusal_successes_at_start = 0
        self._defusal_successes_at_end = 0
        self._defusal_failures_at_start = 0 
        self._defusal_failures_at_end = 0
        self._incorrect_tool_use_at_start = 0
        self._incorrect_tool_use_at_end = 0
        self._team_defusal_failure_rate_at_start = 0.0
        self._team_defusal_failure_rate_at_end = 0.0


        # What is the start and end times of the field phase?
        self._start_mission_time = None
        self._start_elapsed_milliseconds = None
        self._end_mission_time = None
        self._end_elapsed_milliseconds = None


        # Things to keep track of
        self._search_monitor = kwargs.get("search_monitor", None)
        self._hazard_monitor = kwargs.get("hazard_monitor", None)
        self._fire_monitor = kwargs.get("fire_monitor", None)
        self._bomb_defusal_monitor = kwargs.get("bomb_defusal_monitor", None)


        # Listen to the Minecraft bridge for FoV messages
        self.add_minecraft_callback(MissionStageTransition, self.__onTransition)


    @property
    def participant_id(self):
        return self._participant_id


    def _onActivated(self):
        """
        Use the activation time to get the mission time and elapsed
        milliseconds of when the Field phase started
        """

        self.logger.debug(f"{self}:  Intervention Activated")

        self.__get_start_stats()

        self.logger.debug(f"{self}:    Start Mission Time: {self._start_mission_time}")
        self.logger.debug(f"{self}:    Start Elapsed Milliseconds: {self._start_elapsed_milliseconds}")



    def __onTransition(self, message):
        """
        Callback when the mission transitions to another stages
        """

        self.logger.debug(f'{self}:  Recieved {message}')

        # Determine if the player's entering the shop phase
        if message.mission_stage == MissionStage.ShopStage:
            self.logger.debug(f'{self}:    Entering Shop Phase.  Resolving.')

            self.__get_end_stats()

            self.logger.debug(f"{self}:    End Mission Time: {self._end_mission_time}")
            self.logger.debug(f"{self}:    End Elapsed Milliseconds: {self._end_elapsed_milliseconds}")

            self.queueForResolution()


    def __get_start_stats(self):
        """
        Get stats at the start of the mission
        """

        self._start_mission_time = self.manager.mission_clock.mission_timer
        self._start_elapsed_milliseconds = self.manager.mission_clock.elapsed_milliseconds

        self._num_fires_at_start = self._fire_monitor.number_of_fires()

        self._defused_bombs_at_start = self._bomb_defusal_monitor.defused_bombs.copy()
        self._team_defusal_failure_rate_at_start = self._bomb_defusal_monitor.team_failure_rate()
        self._defusal_successes_at_start = self._bomb_defusal_monitor.number_of_successful_bombs_engaged(self.participant_id)
        self._defusal_failures_at_start = self._bomb_defusal_monitor.number_of_unsuccessful_bombs_engaged(self.participant_id)
        self._incorrect_tool_use_at_start = self._bomb_defusal_monitor.number_of_failed_actions(self.participant_id)


    def __get_end_stats(self):
        """
        Get stats at the end of the mission
        """

        self._end_mission_time = self.manager.mission_clock.mission_timer
        self._end_elapsed_milliseconds = self.manager.mission_clock.elapsed_milliseconds

        self._num_fires_at_end = self._fire_monitor.number_of_fires()

        self._defused_bombs_at_end = self._bomb_defusal_monitor.defused_bombs.copy()
        self._team_defusal_failure_rate_at_end = self._bomb_defusal_monitor.team_failure_rate()
        self._defusal_successes_at_end = self._bomb_defusal_monitor.number_of_successful_bombs_engaged(self.participant_id)
        self._defusal_failures_at_end = self._bomb_defusal_monitor.number_of_unsuccessful_bombs_engaged(self.participant_id)
        self._incorrect_tool_use_at_end = self._bomb_defusal_monitor.number_of_failed_actions(self.participant_id)



    def __get_bomb_defusal_advice_first_time(self):
        """
        Get advice about defusing bombs
        """

        advice = []

        # How many additional bombs did the team defuse?
        total_bombs_defused = len(self._defused_bombs_at_end) - len(self._defused_bombs_at_start)
        minutes_passed = (self._end_elapsed_milliseconds - self._start_elapsed_milliseconds) / 60000
        team_bomb_defusal_rate = total_bombs_defused / minutes_passed

        # Is the team on track?
        if team_bomb_defusal_rate < self._low_bomb_rate:
            advice.append(f"Your team is defusing bombs at a rate of {team_bomb_defusal_rate:0.2f} per minute.  You should put a bit more effort into defusing bombs.")
        elif team_bomb_defusal_rate > self._high_bomb_rate:
            advice.append(f"Your team is defusing bombs at a rate of {team_bomb_defusal_rate:0.2f} per minute.  Great job!")
        else:
            advice.append(f"Your team is defusing bombs at a rate of {team_bomb_defusal_rate:0.2f} per minute.")


        # How's the failure rate?
        if self._team_defusal_failure_rate_at_end < self._low_bomb_team_failure_rate and total_bombs_defused > 0:
            advice.append(f"Your team is doing well as successfully defusing bombs.")
        elif self._team_defusal_failure_rate_at_end > self._high_bomb_team_failure_rate and total_bombs_defused > 0:
            advice.append(f"Your team is failing at defusing pretty often.  Slow down and make sure you're getting the defusal sequence right.")


        return advice


    def __get_bomb_defusal_advice(self):
        """
        Get advice about defusing bombs
        """

        advice = []


        # How many additional bombs did the team defuse?
        total_bombs_defused = len(self._defused_bombs_at_end) - len(self._defused_bombs_at_start)
        minutes_passed = (self._end_elapsed_milliseconds - self._start_elapsed_milliseconds) / 60000
        team_bomb_defusal_rate = total_bombs_defused / minutes_passed

        # Is the team on track?
        if team_bomb_defusal_rate < self._low_bomb_rate:
            advice.append(f"Your team is defusing bombs at a rate of {team_bomb_defusal_rate:0.2f} per minute.  You should put a bit more effort into defusing bombs.")
        elif team_bomb_defusal_rate > self._high_bomb_rate:
            advice.append(f"Your team is defusing bombs at a rate of {team_bomb_defusal_rate:0.2f} per minute.  Great job!")
        else:
            advice.append(f"Your team is defusing bombs at a rate of {team_bomb_defusal_rate:0.2f} per minute.")


        # How's the failure rate?
        if self._team_defusal_failure_rate_at_end < self._low_bomb_team_failure_rate and total_bombs_defused > 0:
            advice.append(f"Your team is doing well as successfully defusing bombs.")
        elif self._team_defusal_failure_rate_at_end > self._high_bomb_team_failure_rate and total_bombs_defused > 0:
            advice.append(f"Your team is failing at defusing pretty often.  Slow down and make sure you're getting the defusal sequence right.")


        return advice



    def __get_fire_advice(self):
        """
        Get advice about fires
        """

        advice = []

        # If there were no fires at the start, and none started, then no need
        # To give advice
        if self._num_fires_at_start == 0 and self._num_fires_at_end == 0:
            return advice

        fire_delta = self._num_fires_at_end - self._num_fires_at_start

        # How much fire total is there left to take care of?
        if self._num_fires_at_end == 0:
            advice.append("Your team removed all of the existing fires, great job!")
        elif self._num_fires_at_end < self._small_fire_level:
            advice.append("There aren't too many fires at this point, but you should still attend to them.")
        elif self._num_fires_at_end < self._large_fire_level:
            advice.append("There's a good amount of fire, your team should put some more focus on putting out fires.")
        else:
            advice.append("There's a lot of fire!  It's important for your team to keep fires under control!")

        # Did the team take care of all the fires?
        if fire_delta == 0:
            advice.append("Your team took care of all the fires that started.  Good job keeping those under control!")
        elif fire_delta > self._additional_fire_threshold:
            advice.append("Fire is spreading faster than you can put it out, your team should put more effort in stopping fires from spreading.")
        elif fire_delta < self._additional_fire_threshold:
            advice.append("Your team is doing a great job taking care of the excess fire!")
        else:
            advice.append("Your team is doing a good job keeping fire from spreading.")

        return advice


    def __generate_message(self):
        """
        Generate the after action review message
        """

        advice = []


        # Create the actual message
        message = "Here's some feedback on your performance during the last Field phase:"

        # How long did the phase last?
        total_stage_seconds = int((self._end_elapsed_milliseconds - self._start_elapsed_milliseconds) / 1000)
        stage_minutes = int(total_stage_seconds / 60)
        stage_seconds = int(total_stage_seconds % 60)

        # Get any advice about fires
        advice += self.__get_fire_advice()

        if len(self._defused_bombs_at_start) == 0:
            advice += self.__get_bomb_defusal_advice_first_time()
        else:
            advice += self.__get_bomb_defusal_advice()


        for item in advice:
            message += "\n* " + item

        return message


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self.participant_id],
            'default_message': self.__generate_message(),
            'default_responses': []
        }