# -*- coding: utf-8 -*-
"""
.. module:: after_action_review_field.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for After Action Review Field Interventions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to provide after
action review after a Field phase
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import AfterActionReviewFieldIntervention

from shared.search_monitor import SearchMonitor
from shared.hazard_monitor import HazardBeaconMonitor
from shared.fire_monitor import FireMonitor
from shared.bomb_defusal_success_monitor import BombDefusalSuccessMonitor

class AfterActionReviewFieldTrigger(InterventionTrigger):
    """
    Class that generates AfterActionReviewFieldInterventions.
    Interventions are triggered at the start of each Field phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Monitor various aspects of field work
        self._hazard_monitor = HazardBeaconMonitor(self)
        self._search_monitor = SearchMonitor(self)
        self._fire_monitor = FireMonitor(self)
        self._bomb_defusal_monitor = BombDefusalSuccessMonitor(self)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be spawned at the start of a Field stage

        if message.mission_stage == MissionStage.FieldStage:

            for participant in self.manager.participants:
                intervention = AfterActionReviewFieldIntervention(self.manager, 
                                                                  participant.id, 
                                                                  search_monitor=self._search_monitor,
                                                                  hazard_monitor=self._hazard_monitor,
                                                                  fire_monitor=self._fire_monitor,
                                                                  bomb_defusal_monitor=self._bomb_defusal_monitor)
                self.logger.info(f'{self}:  Spawning {intervention}')
                self.manager.spawn(intervention)