# -*- coding: utf-8 -*-
"""
.. module:: dont_place_hazard_beacons_during_recon
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger suggesting players not use hazard
              beacons during the recon phase.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to suggest to players not to use hazard beacons

* Trigger: The intervention is triggered at the start of the Recon phase

* Discard States:  The intervention is discarded at the end of the Recon phase

* Resolve State:  The intervention is resolved when the player places a hazard
                  beacon.

* Followups:  None
"""

from .intervention import DontPlaceHazardBeaconsDuringReconIntervention
from .trigger import DontPlaceHazardBeaconsDuringReconTrigger

InterventionClass = DontPlaceHazardBeaconsDuringReconIntervention
TriggerClass = DontPlaceHazardBeaconsDuringReconTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []