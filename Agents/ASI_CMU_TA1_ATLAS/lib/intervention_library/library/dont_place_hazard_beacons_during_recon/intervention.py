# -*- coding: utf-8 -*-
"""
.. module:: dont_place_hazard_beacons_during_recon.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for suggesting to not use hazard beacons during 
              the recon phase

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to suggest players to not use
hazard beacons during the recon phase
"""

from InterventionManager import Intervention

import MinecraftElements

from MinecraftBridge.messages import (
    EnvironmentCreatedSingle,
    MissionStageTransition, 
    MissionStage
)

class DontPlaceHazardBeaconsDuringReconIntervention(Intervention):
    """
    A DontPlaceHazardBeaconsDuringReconIntervention suggests players not place
    hazard beacons during recon.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 120000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)
        
        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        # Listen to the Minecraft bridge for when a hazard beacon is created
        self.add_minecraft_callback(EnvironmentCreatedSingle, self.__onEnvironmentCreated)
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # If this isn't a Recon stage, then the intervention should be
        # discarded
        if message.mission_stage != MissionStage.ReconStage:
            self.discard()


    @property
    def participant_id(self):
        return self._participant_id


    def __onEnvironmentCreated(self, message):
        """
        Callback when an environment block is created
        """

        self.logger.debug(f"{self}:  Received {message}")
        
        # Did the participant being monitored place this?
        if message.triggering_entity != self._participant_id:
            return

        # Was the placed block a hazard beacon?
        if message.object.type == MinecraftElements.Block.block_beacon_hazard:
            self.logger.debug(f"{self}:  Player {message.triggering_entity} placed {message.object.type}.")
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """

        return {     
            'default_recipients': [self.participant_id],
            'default_message': "Hazard beacons do not provide information about bomb id, fuse length, or defusal sequence, and could be confusing to your team.  I would not suggest using hazard beacons to mark bombs during the Recon phase---you will have an opportunity to purchase more bomb beacons soon.",
            'default_responses': ["I agree", "I disagree", "Skip"]
        }