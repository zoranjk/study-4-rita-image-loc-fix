# -*- coding: utf-8 -*-
"""
.. module:: dont_run_through_fire
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to alert player not to
              run through fire

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to alert players not to run through a fire.

* Trigger: The intervention is triggered at the start of a Field phase, or 
           after an intervention is resolved.

* Discard States:  Entering Shop phase

* Resolve State:  The player is within X blocks of a fire, and whose motion
                  is towards the fire

* Followups:  
"""

from .intervention import DontRunThroughFireIntervention
from .trigger import DontRunThroughFireTrigger

InterventionClass = DontRunThroughFireIntervention
TriggerClass = DontRunThroughFireTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []