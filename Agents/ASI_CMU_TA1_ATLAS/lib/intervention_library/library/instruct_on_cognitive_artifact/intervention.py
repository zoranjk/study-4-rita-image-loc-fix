# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_cognitive_artifact.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for providing just-in-time instructions on cogntive
              artifact usage

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to provide just-in-time instruction
on the cognitive artifact when it is first encountered
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import MissionStageTransition, MissionStage

class InstructOnCognitiveArtifactIntervention(Intervention):
    """
    An InstructOnCognitiveArtifactIntervention provides a just-in-time tutorial
    on the cogntive artifact when it is first encountered.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)        

        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        # Listen to the Minecraft bridge for FoV messages
        self.add_minecraft_callback(MissionStageTransition, self.__onTransition)


    @property
    def participant_id(self):
        return self._participant_id


    def __onTransition(self, message):
        """
        Callback when the mission transitions to another stages
        """

        self.logger.debug(f'{self}:  Recieved {message}')

        # Determine if the player's entering the shop phase
        if message.mission_stage == MissionStage.ShopStage:
            self.logger.debug(f'{self}:    Entering Shop Phase.  Resolving.')
            self.queueForResolution()


    def _onActivated(self):
        """
        Callback when the intervention is activated.  Checks to see if the
        player has played before, and discards if so.
        """

        # TODO:  Query the player database to see if the player has played
        #        before
        pass


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self.participant_id],
            'default_message': 'The client map on your screen contains any bomb beacons your team placed during the recon phase.  Your team can add flags and annotate both beacons and flags to indicate your intended plan.  I highly suggest using the map to create a plan, as I will be able to understand your intentions and better assist during the Field phase.',
            'default_responses': []
        }