# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_cognitive_artifact
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for providing tutorial on the cogntive
              artifact

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to provide just-in-time tutorial on the cogntive artifact

* Trigger: The intervention is triggered at the start of the Recon phase

* Discard States:  The intervention is discarded if the player has played before

* Resolve State:  The intervention is resolved at the start of the first Shop 
                  phase

* Followups:  None
"""

from .intervention import InstructOnCognitiveArtifactIntervention
from .trigger import InstructOnCognitiveArtifactTrigger

InterventionClass = InstructOnCognitiveArtifactIntervention
TriggerClass = InstructOnCognitiveArtifactTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []