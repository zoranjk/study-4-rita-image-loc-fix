# -*- coding: utf-8 -*-
"""
.. module:: plan_rehersal
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for rehersing team plan prior to field

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to review / reherse the team plan prior when leaving the
shop phase

* Trigger: The intervention is triggered at the start of a Shop Phase

* Discard States:  N/A

* Resolve State:  The intervention is resolved on the first shop vote

* Followups:  None
"""

from .intervention import PlanRehersalIntervention
from .trigger import PlanRehersalTrigger

InterventionClass = PlanRehersalIntervention
TriggerClass = PlanRehersalTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []