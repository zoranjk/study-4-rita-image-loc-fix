# -*- coding: utf-8 -*-
"""
.. module:: plan_rehersal.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for reviewing plan with team

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to review the team's current plan
prior to entering the field.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import UI_Click
from MinecraftBridge.messages import MissionStageTransition, MissionStage


class PlanRehersalIntervention(Intervention):
    """
    A PlanRehersalIntervention is an Intervention which monitors
    the team during the shop phase, and reviews the team plan when a player
    votes to leave the shop.
    """

    """
    DescriptionTemplates map action types to a templated string that can be
    populated with a few variables.
    """
    DescriptionTemplates = {
        'GoToWaypoint': "Go to {location}{with_players}.",
        'Search': "Search around {location}{with_players}.",
        'DefuseBomb': "Defuse the bomb at {location}{with_players}.",
        'AddressHazard': "Put out the fire at {location}{with_players}.",
        'PutOutFire': "Put out the fire at {location}{with_players}.",
        'GroupUp': "Meet up with the rest of your team at {location}."
    }

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 20000)

        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self._plan_steps = []
        self._plan_step_descriptions = []
        self._full_plan_description = ""

        self._response_ids = set()

        # Default message, and relevant participants
        self._message = "Looks like you're about to leave.  Here's what you're planning on doing, if I understand correctly:"
        self.addRelevantParticipant(participant_id)

        # Listen to the Minecraft bridge for UI_Clicks that may be associated
        # with voting to leave the store, and Redis bus responses to plan
        # queries
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)
        self.add_minecraft_callback(UI_Click, self.__onUI_Click)
        self.add_redis_callback(self.__onPlanQueryResponse, 'plan_supervisor/response/plan')    


    @property
    def participant_id(self):
        return self._participant_id


    @property
    def plan_steps(self):
        return self._plan_steps


    def __onUI_Click(self, message):
        """
        Callback when a UI_Click message is received
        """

        # Is this message a vote to leave the shop?
        if message.additional_info['meta_action'] != 'VOTE_LEAVE_STORE':
            return

        # Did the participant do the voting?
        if message.participant_id != self._participant_id:
            return


        # Query the plan supervisor for any plan steps where one of the
        # participants disagrees
        message_payload = { 'name': None, 
                            'participants': [self._participant_id],
                            'conditions': [ ] 
                          }
        response_id = self.manager.request_redis(message_payload, 
                                                 "plan_supervisor/query/plan",
                                                 blocking=False)
        self._response_ids.add(response_id)

        self.logger.debug(f'{self}:  Query sent to plan supervisor, waiting for response ID {response_id}')


    def __onPlanQueryResponse(self, message):
        """
        Process a response from the Plan Supervisor
        """

        self.logger.debug(f'{self}:  Message Response Received: {message}')
        self.logger.debug(f'{self}:    Data: {message.data}')

        # Ignore any responses that show up if this intervention is no longer
        # active
        if self.state != Intervention.State.ACTIVE:
            self.logger.debug(f'{self}:  Intervention no longer active, ignoring {message}')

        # Only handle response IDs that this Intervention sent the query for
        if not message.request_id in self._response_ids:
            self.logger.debug(f'{self}:  Ignoring response with no matching request ID {message}')
            return

        # Remove the request ID from the set of request IDs to respond to
        self._response_ids.discard(message.request_id)

        self._plan_steps = message.data.get('steps', {}).get(self._participant_id, [])
        self._plan_steps = [step for step in self._plan_steps if not step['is_complete']]
        self._plan_step_descriptions = [self.__create_description(step) for step in self._plan_steps]
        self._full_plan_description = '\n'.join([f'  {n+1}. {d}' for n,d in enumerate(self._plan_step_descriptions)])

        if len(self._response_ids) > 0:
            self.logger.debug(f'{self}:  Resolving with unresolved Queries: {self._response_ids}')

        self.logger.info(f'{self}:  Resolving for {self._participant_id} with description:\n{self._full_plan_description}')

        self.queueForResolution()

    
    def __create_description(self, step):
        """
        Function to create a natural language description of the given step.

        Argument
        --------
        step : dict
            Dictionary representation of the step
        """

        action_type = step.get('action',{}).get('type', '<UNKNOWN>')
        if action_type == '<UNKNOWN>':
            self.logger.warning(f'{self}:  An action with an unknown type was received!')
            return ""

        # Get the _callsign_ of any teammates that will be helping
        teammates = [self.manager.participants[p] for p in step.get('action',{}).get('actors',[]) if p != self._participant_id]
        teammates = [teammate.callsign for teammate in teammates if teammate is not None]

        x,y = step.get('action',{}).get('location', [0,0])
        location_string = "{x}, {y}".format(x=x, y=y)

        with_string = (' with ' if len(teammates) > 0 else '') + ' and '.join(teammates)


        description = PlanRehersalIntervention.DescriptionTemplates.get(action_type, "")
        description = description.format(location=location_string,
                                         with_players=with_string)

        return description


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        if message.mission_stage == MissionStage.FieldStage:

            if len(self._response_ids) > 0:
                self.logger.debug(f'{self}:  Discarding with unresolved Queries: {self._response_ids}')

            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': '\n'.join([self._message,self._full_plan_description]),
            'default_responses': ["That's correct", "That's incorrect", "Skip"]
        }