# -*- coding: utf-8 -*-
"""
.. module:: after_action_review_recon
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for providing after-action review for
              each player after the recon phase

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to provide after-action review after the recon phase

* Trigger: The intervention is triggered at the start of the Recon phase

* Discard States:  N/A

* Resolve State:  The intervention is resolved at the start of the Shop phase

* Followups:  None
"""

from .intervention import AfterActionReviewReconIntervention
from .trigger import AfterActionReviewReconTrigger

InterventionClass = AfterActionReviewReconIntervention
TriggerClass = AfterActionReviewReconTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []