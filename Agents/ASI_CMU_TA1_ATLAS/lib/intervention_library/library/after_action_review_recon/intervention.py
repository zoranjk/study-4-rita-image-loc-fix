# -*- coding: utf-8 -*-
"""
.. module:: after_action_review_recon.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for providing after action reviews after the recon
              phase

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to generate after-action reviews
of the recon phase
"""

import MinecraftElements

from InterventionManager import Intervention

from MinecraftBridge.messages import (
    MissionStageTransition, 
    MissionStage,
    CommunicationEnvironment,
    EnvironmentCreatedSingle
)


class AfterActionReviewReconIntervention(Intervention):
    """
    An AfterActionReviewReconIntervention generates after-action reviews of the
    recon phase.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 20000)        

        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        # Parameters
        self._max_fuse_start_minute = kwargs.get("max_fuse_start_minute", 7)
        self._number_of_bomb_beacons = kwargs.get("number_of_bomb_beacons", 5)

        # Things to keep track of
        self._search_monitor = kwargs.get("search_monitor", None)
        self._bomb_beacons = {}
        self._hazard_beacons = {}
        self._fuse_start_times = {}


        # Listen to the Minecraft bridge for FoV messages
        self.add_minecraft_callback(CommunicationEnvironment, self.__onCommunication)
        self.add_minecraft_callback(EnvironmentCreatedSingle, self.__onEnvironmentCreated)
        self.add_minecraft_callback(MissionStageTransition, self.__onTransition)


    @property
    def participant_id(self):
        return self._participant_id


    def __onTransition(self, message):
        """
        Callback when the mission transitions to another stages
        """

        self.logger.debug(f'{self}:  Recieved {message}')

        # Determine if the player's entering the shop phase
        if message.mission_stage == MissionStage.ShopStage:
            self.logger.debug(f'{self}:    Entering Shop Phase.  Resolving.')
            self.queueForResolution()


    def __onCommunication(self, message):
        """
        Callback when a communication is received
        """

        self.logger.debug(f"{self}:  Received {message}")

        # Is this beacon in the list of known beacons?
        if message.sender_id in self._bomb_beacons:
            fuse_start = int(message.additional_info.get("fuse_start_minute", '0'))
            if fuse_start == 0:
                self.logger.debug("Fuse start is 0")
            self._bomb_beacons[message.sender_id]['fuse_start'] = fuse_start


    def __onEnvironmentCreated(self, message):
        """
        Arguments
        ---------
        message: MinecraftBridge.messages.MissionStageTransition
            Received EnvironmentCreatedSingle message
        """

        self.logger.debug(f"{self}:  Received message {message}")

        # Is this environment created by the participant of interest
        if message.triggering_entity == self._participant_id:

            beacon_id = message.object.id
            beacon_location = (message.object.x,
                               message.object.y,
                               message.object.z)
            message_time = message.elapsed_milliseconds

            # Is it a bomb beacon?
            if message.object.type == MinecraftElements.Block.block_beacon_bomb:

                self._bomb_beacons[beacon_id] = { 'location': beacon_location,
                                                  'message_time': message_time }

            # Is it a hazard beacon?
            if message.object.type == MinecraftElements.Block.block_beacon_hazard:
    
                self._hazard_beacons[beacon_id] = { 'location': beacon_location,
                                                    'message_time': message_time }


    def __generate_message(self):
        """
        Generate the after action review message
        """

        advice = []

        num_short_fuse_beacons = 0

        for beacon in self._bomb_beacons.values():
            if not 'fuse_start' in beacon:
                self.logger.debug("BEACON DOESN'T HAVE FUSE START!")
                self.logger.debug(beacon)
            if beacon.get('fuse_start',0) <= self._max_fuse_start_minute:
                num_short_fuse_beacons += 1

        desert_search = self._search_monitor.desert_search_count()
        village_search = self._search_monitor.village_search_count()
        forest_search = self._search_monitor.forest_search_count()

        total_search = desert_search + village_search + forest_search


        # Did the player use all of the bomb beacons?
        if len(self._bomb_beacons) < self._number_of_bomb_beacons:
            advice.append("You didn't use all of your bomb beacons.  Bomb beacons provide useful information for planning.")
        else:
            advice.append("You used all of your bomb beacons, maximizing the information available for planning.  Good job!")
        if num_short_fuse_beacons > 0.7*len(self._bomb_beacons):
            advice.append("The bomb beacons you placed were primarily on bombs with short fuses.  Good job prioritizing the more immediate tasks!")
        else:
            advice.append("You placed bomb beacons on bombs with longer fuse times.  These would have been better used to get information on short fused bombs.")
        if len(self._hazard_beacons) > 2:
            advice.append("You used many hazard beacons during the recon phase.  These are not as informative as bomb beacons.")
        else:
            advice.append("You didn't use many hazard beacons.  Good job, bomb beacons provide much more information for planning!")

        # NOTE:  These values are hard-coded based on how many squares each 
        #        player can cover in 3 minutes, assuming a movement speed of
        #        ~4 squares per second.

        if total_search > 1600:
            advice.append("As a team, you searched quite a lot of area!  Good job searching!")
        elif total_search > 1000:
            advice.append("As a team, you searched a fair amount of area.  You could have been more efficient covering the region, though.")
        elif total_search > 600:
            advice.append("As a team, you didn't search too much area.  You should try to be more aware of where your teammates are going to be more efficient.")
        else:
            advice.append("As a team, you didn't search much area at all!  What were you doing?")


        desert_count = 1.25*desert_search
        village_count = 1.0*village_search
        forest_count = 0.75*forest_search

        # What's the total count?
        total_count = desert_count + village_count + forest_count

        # If any one count is under 15% of the total, or over 70%, then
        # append to the message that search efforts should be better
        # distributed.
        if (desert_count / total_count) > 0.70:
            advice.append("Your team didn't explore the village or forest much.  Don't neglect these areas!")
        elif (village_count / total_count) > 0.70:
            advice.append("Your team didn't explore the desert or forest much.  Don't neglect these areas!")
        elif (forest_count / total_count) > 0.70:
            advice.append("Your team didn't explore the desert or village much.  Don't neglect these areas!")
        elif (desert_count / total_count) < 0.15:
            advice.append("Your team didn't explore the desert much.  Don't neglect this area.")
        elif (village_count / total_count) < 0.15:
            advice.append("Your team didn't explore the village much.  Don't neglect this area.")
        elif (forest_count / total_count) < 0.15:
            advice.append("Your team didn't explore the forest much.  Don't neglect this area.")
        else:
            advice.append("Your team covered all the regions in the map.  Great job!")

        # Create the actual message
        message = "Here's some feedback on your performance during the Recon phase:"

        for item in advice:
            message += "\n* " + item

        return message


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self.participant_id],
            'default_message': self.__generate_message(),
            'default_responses': []
        }