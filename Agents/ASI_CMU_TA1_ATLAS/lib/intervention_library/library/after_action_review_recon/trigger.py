# -*- coding: utf-8 -*-
"""
.. module:: after_action_review_recon.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for After Action Review Recon Interventions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to provide after
action review after a Recon phase
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import AfterActionReviewReconIntervention

from shared.search_monitor import SearchMonitor


class AfterActionReviewReconTrigger(InterventionTrigger):
    """
    Class that generates AfterActionReviewReconInterventions.
    Interventions are triggered at the start of the recon phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Monitor the hazard blocks
        self._search_monitor = SearchMonitor(self)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be spawned at the start of a Shop stage

        if message.mission_stage == MissionStage.ReconStage:

            for participant in self.manager.participants:
                intervention = AfterActionReviewReconIntervention(self.manager, 
                                                                  participant.id, 
                                                                  search_monitor=self._search_monitor)
                self.logger.info(f'{self}:  Spawning {intervention}')
                self.manager.spawn(intervention)