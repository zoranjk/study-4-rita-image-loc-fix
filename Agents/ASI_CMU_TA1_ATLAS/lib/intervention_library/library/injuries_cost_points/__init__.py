# -*- coding: utf-8 -*-
"""
.. module:: injuries_cost_points
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for a reminder message that getting
              injured will reduce score

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to inform players that getting injured results in points
being taken away

* Trigger: The intervention is triggered at the start of the first Field Phase

* Discard States:  N/A

* Resolve State:  The intervention is queued for a resolution after a random 
                  amount of time

* Followups:  None
"""

from .intervention import InjuriesCostPointsIntervention
from .trigger import InjuriesCostPointsTrigger

InterventionClass = InjuriesCostPointsIntervention
TriggerClass = InjuriesCostPointsTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []