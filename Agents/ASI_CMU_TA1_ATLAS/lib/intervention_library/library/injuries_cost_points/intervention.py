# -*- coding: utf-8 -*-
"""
.. module:: team_welcome_message.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for introductory message for team

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to introduce the ATLAS agent to 
the team.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import PlayerState


class InjuriesCostPointsIntervention(Intervention):
    """
    An InjuriesCostPointsIntervention is an Intervention which simply informs
    participants that getting injured will reduce their score.
    """

    def __init__(self, manager, participant_id, presentation_delay, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Who this presentation will be presented to
        presentation_delay : int
            Number of milliseconds from the start of the field stage to wait
            until presenting
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 0)
        kwargs["expiration"] = kwargs.get("expiration", 10000)
        kwargs["view_duration"] = kwargs.get("view_duration", 10000)
        
        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "Be careful, getting injured will reduce your score.")

        self._presentation_time = None
        self._presentation_delay = presentation_delay
        self._participant_id = participant_id

        # Indicate that all participants are relevant to this intervention
        self.addRelevantParticipant(participant_id)        

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)


    def __onPlayerState(self, message):
        """
        Callback when player state messages are recieved.  Used to determine if
        it is time to present the intervention
        """

        if self._presentation_time is None:
            self._presentation_time = message.elapsed_milliseconds + self._presentation_delay

        if message.elapsed_milliseconds >= self._presentation_time:
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self._participant_id],
            'default_message': self._message,
            'default_responses': []
        }