# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_ppe
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for providing tutorial on PPE

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to provide just-in-time tutorial on PPE

* Trigger: The intervention is triggered at the start of the Recon phase

* Discard States:  The intervention is discarded if the player has played before

* Resolve State:  The intervention is resolved when a player is hurt and enters
                  the shop phase

* Followups:  None
"""

from .intervention import InstructOnPPEIntervention
from .trigger import InstructOnPPETrigger

InterventionClass = InstructOnPPEIntervention
TriggerClass = InstructOnPPETrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []