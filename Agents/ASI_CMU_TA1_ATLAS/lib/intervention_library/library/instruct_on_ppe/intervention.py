# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_ppe.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for providing just-in-time instructions on PPE

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to provide just-in-time instruction
on buying PPE if the player has been hurt.
"""

from InterventionManager import Intervention

import MinecraftElements

from MinecraftBridge.messages import (
    MissionStageTransition, 
    MissionStage,
    PlayerStateChange
)

class InstructOnPPEIntervention(Intervention):
    """
    An InstructOnPPE intervention provides a just-in-time tutorial on PPE to
    avoid additional player injuries, if this is the first time a player is 
    playing the game.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 120000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)        

        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        # Flag to keep track of whether the player was hurt
        self._participant_hurt = False

        # Listen to the Minecraft bridge for FoV messages
        self.add_minecraft_callback(PlayerStateChange, self.__onPlayerStateChange)
        self.add_minecraft_callback(MissionStageTransition, self.__onTransition)


    @property
    def participant_id(self):
        return self._participant_id


    def __onPlayerStateChange(self, message):
        """
        Callback when a PlayerStateChange message is received
        """

        self.logger.debug(f'{self}:  Recieved {message}')

        # Is the message about the target participant?
        if message.participant_id != self.participant_id:
            self.logger.debug(f'{self}:  Received message about {message.participant_id}, not {self.participant_id}')
            return

        # Is the message about a change in health?
        health = message.changed_attributes.get_attribute('health')

        if health is None:
            self.logger.debug(f"{self}:  Message doesn't contain health attribute")
            return

        if (float(health.current) - float(health.previous)) >= 0:
            self.logger.debug(f"{self}:  Health hasn't decreased")
            return

        # Health has decreased, inform the participant
        self.logger.debug(f"{self}:  Health for {message.participant_id} went from {health.previous} to {health.current}.  Flaging.")
        self._participant_hurt = True


    def __onTransition(self, message):
        """
        Callback when the mission transitions to a new phase
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Check if the new state is the Shop state, and resolve if the player
        # has been hurt

        if message.mission_stage == MissionStage.ShopStage and self._participant_hurt:
            self.logger.debug(f'{self}:  Player was hurt in previous stage, resolving.')
            self.queueForResolution()


    def _onActivated(self):
        """
        Callback when the intervention is activated.  Checks to see if the
        player has played before, and discards if so.
        """

        # TODO:  Query the player database to see if the player has played
        #        before
        pass


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self.participant_id],
            'default_message': 'You were hurt in the last Field phase.  Buying PPE will protect you from getting hurt from one bomb blast.  You should consider buying and equipping this item.',
            'default_responses': ["I agree", "I disagree", "Skip"]
        }