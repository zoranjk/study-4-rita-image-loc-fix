# -*- coding: utf-8 -*-
"""
.. module:: resolve_plan_disagreement.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for encouraging team to resolve plan disagreements

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to encourage teams to resolve any
plan disagreements before returning to the field.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import UI_Click
from MinecraftBridge.messages import MissionStageTransition, MissionStage

class ResolvePlanDisagreementIntervention(Intervention):
    """
    A ResolvePlanDisagreementIntervention is an Intervention which monitors
    the team during the shop phase, and determines if there is a disagreement
    in the plan when a player votes to leave the shop.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 30000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        Intervention.__init__(self, manager, **kwargs)

        # Keep track of who's voting to leave
        self.__voting_to_leave = { p.id: False for p in self.manager.participants }

        # Need to keep track of response IDs, to know whether the response is
        # in conjunction with our given request
        self._response_ids = set()
        self._disagreement_steps = []

        # Default message, and relevant participants
        self._message = "There still seems to be some disagreement with the plan---I suggest staying in the store to resolve it."
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.id)


        # Listen to the Minecraft bridge for UI_Clicks that may be associated
        # with voting to leave the store, and Redis bus responses to plan
        # queries
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)
        self.add_minecraft_callback(UI_Click, self.__onUI_Click)
        self.add_redis_callback(self.__onPlanQueryResponse, 'plan_supervisor/response/plan')



    @property
    def disagreement_steps(self):
        return self._disagreement_steps
    


    def __onUI_Click(self, message):
        """
        Callback when a UI_Click message is received
        """

        # Is this message a vote to leave the shop?
        if message.additional_info['meta_action'] != 'VOTE_LEAVE_STORE':
            return

        self.__voting_to_leave[message.participant_id] = not self.__voting_to_leave[message.participant_id]

        # Is the participant voting to leave the store?
        if self.__voting_to_leave[message.participant_id]:
            self.logger.debug(f'{self}:  Participant {message.participant_id} is voting to leave the store')

            # Query the plan supervisor for any plan steps where one of the
            # participants disagrees
            message_payload = { 'name': None, 
                                'participants': None,
                                'conditions': [
                                    { 'type': 'contains-response',
                                      'parameters': { 
                                        'response_text': 'I disagree with your plan!' 
                                      }
                                    }
                                ] }
            response_id = self.manager.request_redis(message_payload, 
                                                     "plan_supervisor/query/plan",
                                                     blocking=False)
            self._response_ids.add(response_id)

            self.logger.debug(f'{self}:  Waiting for response ID {response_id}')


    def __onPlanQueryResponse(self, message):
        """
        Process a response from the Plan Supervisor
        """

        self.logger.debug(f'{self}:  Query Response Received: {message}')
        self.logger.debug(f'{self}:    Data: {message.data}')

        # Ignore any responses that show up if this intervention is no longer
        # active
        if self.state != Intervention.State.ACTIVE:
            self.logger.debug(f'{self}:  Intervention no longer active, ignoring {message}')

        # Only handle response IDs that this Intervention sent the query for
        if not message.request_id in self._response_ids:
            self.logger.debug(f'{self}:  Ignoring response with no matching request ID {message}')
            return

        # Remove the request ID from the set of request IDs to respond to
        self._response_ids.discard(message.request_id)

        steps = message.data.get('steps', [])

        # How many disagreements are there?
        total_disagreements = sum([len(x) for x in steps.values()])

        if total_disagreements > 0:
            self._disagreement_steps = steps

            # There is disagreement between players on one or more steps
            self.logger.debug(f'{self}:    Disagreement between players detected.  Resolving')
            self.logger.debug(f'{self}:    Steps: {steps}')

            if len(self._response_ids) > 0:
                self.logger.debug(f'{self}:  Resolving with unresolved Queries: {self._response_ids}')

            self.queueForResolution()


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Discard the intervention if left the shop

        if message.mission_stage == MissionStage.FieldStage:

            if len(self._response_ids) > 0:
                self.logger.debug(f'{self}:  Resolving with unresolved Queries: {self._response_ids}')

            self.discard()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': ["I agree", "I disagree", "Skip"]            
        }