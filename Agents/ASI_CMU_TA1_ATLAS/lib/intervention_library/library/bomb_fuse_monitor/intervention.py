# -*- coding: utf-8 -*-
"""
.. module:: remind_team_of_remaining_time.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for introductory message for team

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to help the team monitor elapsed
time.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import PlayerState



class BombFuseMonitorIntervention(Intervention):
    """
    A BombFuseMonitorIntervention is an Intervention which provides the team 
    with a reminder that bombs are about to explode
    """

    def __init__(self, manager, bomb_monitor, fuse_time, heads_up_time, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        Intervention.__init__(self, manager, **kwargs)

        # Default values for intervention prioritization informatio
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 15000)
        kwargs["view_duration"] = kwargs.get("view_duration", 10000)

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)


        self._bomb_monitor = bomb_monitor

        self._fuse_time = fuse_time
        self._resolve_time = self._fuse_time - heads_up_time

        self._message = "Bombs will explode in one minute.  I suggest returning to the shop to discuss how to best handle this."

        self.add_minecraft_callback(PlayerState, self.__onPlayerState)


    def __get_bomb_count(self):
        """
        """

        bombs = [b for b in self._bomb_monitor.bombs.items() if b[1]['fuse_time'] == self._fuse_time]

        self.logger.debug(f"{self}:  Bombs:")
        for b in bombs:
            self.logger.debug(f"{self}:    {b[0]} @ {b[1]['location']} -- {b[1]['fuse_time']}")

        return len(bombs)


    def __onPlayerState(self, message):
        """
        Callback when a PlayerState message is received.
        """

        mission_time = message.mission_timer

        if mission_time[0] == self._resolve_time and mission_time[1] == 0:
            num_bombs_to_defuse = self.__get_bomb_count()

            if num_bombs_to_defuse == 0:
                self.discard()
                return
            else:
                if num_bombs_to_defuse == 1:
                    self.__message = f"A bomb will explode in one minute.  I suggest returning to the shop to discuss how to best handle this."
                else:
                    self._message = f"{num_bombs_to_defuse} bombs will explode in one minute.  I suggest returning to the shop to discuss how to best handle these."
                self.logger.info(f"{self}:  Resolving at {mission_time} with message '{self._message}'")
                self.queueForResolution()

                shop_intervention = BombFuseMonitorShopIntervention(self.manager, self._bomb_monitor, self._fuse_time)
                self.manager.spawn(shop_intervention)


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': []
        }




class BombFuseMonitorShopIntervention(Intervention):
    """
    A BombFuseMonitorShopIntervention is an Intervention which provides the team 
    with information on which bombs are about to explode
    """

    def __init__(self, manager, bomb_monitor, fuse_time, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        Intervention.__init__(self, manager, **kwargs)

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 30000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)

        self._bomb_monitor = bomb_monitor
        self._fuse_time = fuse_time

        self._message = "You should prioritize the bombs that are about to explode."

        self._message = self.__create_message()
        self.logger.info(f'{self}:  Message: "{self._message}"')

        self.add_minecraft_callback(PlayerState, self.__onPlayerState)


    def __str__(self):
        return self.__class__.__name__


    def __location_to_map_section(self, location):
        """
        """

        row_number = max(min(int(9*location[2]/150),8),0)
        col_number = max(min(int(7*location[0]/50),6),0) + 1

        row_letter = ['A','B','C','D','E','F','G','H','I'][row_number]

        return row_letter + str(col_number)


    def __location_to_region(self, location):
        """
        """

        if location[2] > 100:
            return "the desert"
        elif location[2] > 50:
            return "the village"
        else:
            return "the forest"


    def __create_message(self):
        """
        Helper method to create message text
        """

        bombs = [b for b in self._bomb_monitor.bombs.values() if b['fuse_time'] == self._fuse_time]

        # Don't want to overwhelm with too much info, so don't provide exact 
        # locations if there are more than 4
        if len(bombs) < 4:
            locations = [self.__location_to_map_section(b['location']) for b in bombs]
        else:
            locations = [self.__location_to_region(b['location']) for b in bombs]

        regions = set(locations)
        content = []

        for region in regions:
            count = len([l for l in locations if l == region])
            content.append(f'{count} bombs in {region}')

        if len(content) == 1:
            message_content = content[0]
        elif len(content) == 2:
            message_content = f'{content[0]} and {content[1]}'
        else:
            message_content = f'{content[0]}, {content[1]}, and {content[2]}'

        return f"{message_content} will explode at {self._fuse_time}:00.  You should plan on prioritizing these when you return to the field."


    def __onPlayerState(self, message):
        """
        Callback when a PlayerState message is received.
        """

        mission_time = message.mission_timer

        # If the mission time is the fuse time (and all the bombs blew up), then
        # there's no need for presenting this.
        if mission_time[0] == self._fuse_time and mission_time[1] == 0:

            self.logger.info(f"{self}:  Didn't return to shop in time.  Discarding.")
            self.discard()


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if 1) the mission stage is the
        # Shop stage, and 2) then number of times the team has transitioned to 
        # the shop is 1

        if message.mission_stage == MissionStage.ShopStage:

            self.logger.info(f"{self}:  Returned to shop.  Resolving")
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': []
        }