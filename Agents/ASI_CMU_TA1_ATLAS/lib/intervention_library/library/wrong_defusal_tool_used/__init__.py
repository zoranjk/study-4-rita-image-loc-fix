# -*- coding: utf-8 -*-
"""
.. module:: wrong_defusal_tool_used
   :platform: Linux, Windows, OSX
   :synopsis: 

.. moduleauthor:: Ini Oguntola

"""

from .intervention import WrongDefusalToolUsedIntervention
from .trigger import WrongDefusalToolUsedTrigger

InterventionClass = WrongDefusalToolUsedIntervention
TriggerClass = WrongDefusalToolUsedTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []


