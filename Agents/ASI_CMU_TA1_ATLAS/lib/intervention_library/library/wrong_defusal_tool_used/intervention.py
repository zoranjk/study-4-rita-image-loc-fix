# -*- coding: utf-8 -*-
"""
.. module:: wrong_defusal_tool_used.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used to help players avoid getting
        frozen again in the future.

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of an Intervention class designed to help players
use the right tool in the future.
"""

from InterventionManager import Intervention


class WrongDefusalToolUsedIntervention(Intervention):
    """
    A WrongDefusalToolUsedIntervention is an Intervention which simply presents a 
    message to team members when one of their team mates is frozen.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        participant_id : string
            ID of the participant that's frozen

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 20000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)
        
        Intervention.__init__(self, manager, **kwargs)
        self.addRelevantParticipant(participant_id)
        self._participant_id = participant_id

        self._message = kwargs.get(
            'message',
            "Remember to right-click bombs with wirecutters to know the defusal order."
#            "You need to use wirecutter colors in the order given when you inspect the bomb."
        )

    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Immediately queue the Intervention for resolution.
        """
        self.queueForResolution()

    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self._participant_id],
            'default_message': self._message,
            'default_responses': []
        }
    