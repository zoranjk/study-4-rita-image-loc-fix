# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_bomb_beacon
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for providing tutorial on bomb beacons

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to provide just-in-time tutorial on bomb beacon

* Trigger: The intervention is triggered at the start of the Recon phase

* Discard States:  The intervention is discarded if the player has played before

* Resolve State:  The intervention is resolved when a bomb is observed

* Followups:  None
"""

from .intervention import InstructOnInjuriesIntervention
from .trigger import InstructOnInjuriesTrigger

InterventionClass = InstructOnInjuriesIntervention
TriggerClass = InstructOnInjuriesTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []