# -*- coding: utf-8 -*-
"""
.. module:: suggest_scouting
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for scouting mmessage

.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind players to scout the area.

* Trigger: The intervention is triggered whenever a bomb beacon is placed, and when teams enter the store

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  None
"""

from .intervention import SuggestScoutingIntervention
from .trigger import SuggestScoutingTrigger

InterventionClass = SuggestScoutingIntervention
TriggerClass = SuggestScoutingTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []