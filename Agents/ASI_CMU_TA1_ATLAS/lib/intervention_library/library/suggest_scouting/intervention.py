# -*- coding: utf-8 -*-
"""
.. module:: suggest_scouting.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for that suggests scouting for the team

.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Definition of an Intervention class designed to remind players that scouting is beneficial to them
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import PlayerState


class SuggestScoutingIntervention(Intervention):
    """
    A TeamWelcomeMessageIntervention is an Intervention which simply presents
    a welcome message to all participants in the team.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 30000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        self._presentation_time = kwargs.get("presentation_time", 10000)

        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "Remember during recon to place beacons on bombs with short fuses so I can help you better!")

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)        

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        

    def __onPlayerState(self, message):
        """
        Callback when player state messages are recieved.  Used to determine if
        it is time to present the intervention
        """

        if message.elapsed_milliseconds > self._presentation_time:
            self.queueForResolution()

###    def _onActivated(self):
###        """
###        Callback when the Intervention is activated.
###        Immediately queue the Intervention for resolution.
###        """
###        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': []            
        }