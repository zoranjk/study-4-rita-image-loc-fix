# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_hazard_beacons.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for providing just-in-time instructions on using
              hazard beacons

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to provide just-in-time instruction
on use of hazard beacons
"""

from InterventionManager import Intervention

import MinecraftElements
from MinecraftBridge.messages import (
    FoVSummary,
    PlayerInventoryUpdate
)

class InstructOnHazardBeaconsIntervention(Intervention):
    """
    An InstructOnHazardBeaconsIntervention provides a just-in-time tutorial
    on hazard beacon usage.
    """

    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant to be monitored
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 30000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        Intervention.__init__(self, manager, **kwargs)

        self._min_number_of_pixels = kwargs.get("min_number_of_pixels", 1500)

        self._has_fire_extinguishers = False

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        # Listen to the Minecraft bridge for FoV messages
        self.add_minecraft_callback(FoVSummary, self.__onFoV)
        self.add_minecraft_callback(PlayerInventoryUpdate, self.__onInventoryUpdate)


    @property
    def has_fire_extinguishers(self):
        return self._has_fire_extinguishers

    @property
    def participant_id(self):
        return self._participant_id


    def __onInventoryUpdate(self, message):
        """
        Callback when a PlayerInventoryUpdate message is received.  Used to
        determine if the player has fire extinguishers
        """

        self.logger.debug(f'{self}:  Received {message}')

        # Get this participant's current inventory
        player_inventory = message.current_inventory.get(self.participant_id, {})
        self.logger.debug(f'{self}:    Participant {self.participant_id} Inventory:')
        for item_name, count in player_inventory.items():
            self.logger.debug(f'{self}:      {item_name}:\t {count}')

        # How many fire extinguishers are in the player's inventory?  Using -1
        # to handle if the FIRE_EXTINGUISHER key is not present
        num_fire_extinguishers = player_inventory.get('FIRE_EXTINGUISHER', -1)

        if num_fire_extinguishers == 0:
            self._has_fire_extinguishers = False
        if num_fire_extinguishers > 0:
            self._has_fire_extinguishers = True


    def __onFoV(self, message):
        """
        Callback when an FoV summary message is received
        """

        self.logger.debug(f'{self}:  Recieved {message}')

        # Is there a fire in the list of blocks?
        for block in message.blocks:
            if block.type == MinecraftElements.Block.block_fire_custom and block.number_pixels >= self._min_number_of_pixels:
                # Does the player have a fire extinguisher?
                if self._has_fire_extinguishers:
                    self.discard()
                else:
                    self.logger.debug(f'{self}:    Block {block.type} seen with {block.number_pixels} pixels.  Resolving.')
                    self.queueForResolution()


    def _onActivated(self):
        """
        Callback when the intervention is activated.  Checks to see if the
        player has played before, and discards if so.
        """

        # TODO:  Query the player database to see if the player has played
        #        before
        pass


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """

        return {     
            'default_recipients': [self.participant_id],
            'default_message': "You recently saw fire on your screen.  Placing hazard beacons is important for sharing the location of the fire with your teammates.",
            'default_responses': ["I agree", "I disagree", "Skip"]
        }