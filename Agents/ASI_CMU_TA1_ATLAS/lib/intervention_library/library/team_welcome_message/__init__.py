# -*- coding: utf-8 -*-
"""
.. module:: team_welcome_message
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for introductory mmessage for team

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to introduce the ATLAS agent to the team.

* Trigger: The intervention is triggered at the start of the first Shop Phase

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  None
"""

from .intervention import TeamWelcomeMessageIntervention
from .trigger import TeamWelcomeMessageTrigger

InterventionClass = TeamWelcomeMessageIntervention
TriggerClass = TeamWelcomeMessageTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []