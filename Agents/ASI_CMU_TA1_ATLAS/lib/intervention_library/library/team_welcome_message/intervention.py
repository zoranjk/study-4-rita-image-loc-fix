# -*- coding: utf-8 -*-
"""
.. module:: team_welcome_message.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for introductory message for team

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to introduce the ATLAS agent to 
the team.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import PlayerState


class TeamWelcomeMessageIntervention(Intervention):
    """
    A TeamWelcomeMessageIntervention is an Intervention which simply presents
    a welcome message to all participants in the team.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 0)
        kwargs["expiration"] = kwargs.get("expiration", 10000)
        kwargs["view_duration"] = kwargs.get("view_duration", 10000)
        
        Intervention.__init__(self, manager, **kwargs)
        self._message = kwargs.get('message', "Hello, I'm an AI advisor that will provide advice to help improve your team performance.")

        self._presentation_time = kwargs.get("presentation_time", 3000)

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)        

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)


    def __onPlayerState(self, message):
        """
        Callback when player state messages are recieved.  Used to determine if
        it is time to present the intervention
        """

        if message.elapsed_milliseconds > self._presentation_time:
            self.queueForResolution()


###    def _onActivated(self):
###        """
###        Callback when the Intervention is activated.
###        Immediately queue the Intervention for resolution.
###        """
###        self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': []
        }