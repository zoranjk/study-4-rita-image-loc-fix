# -*- coding: utf-8 -*-
"""
.. module:: team_welcome_message.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Team Welcome Message Intervention

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn introduction interventions.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import TeamWelcomeMessageIntervention

class TeamWelcomeMessageTrigger(InterventionTrigger):
    """
    Class that generates TeamWelcomeMessageInterventions.  Interventions are 
    triggered at the start of the first shop phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if 1) the mission stage is the
        # Shop stage, and 2) then number of times the team has transitioned to 
        # the shop is 1

        if message.mission_stage == MissionStage.ReconStage and \
           message.transitionsToShop <= 1:

           intervention = TeamWelcomeMessageIntervention(self.manager, **self.kwargs)
           self.logger.info(f'{self}:  Spawning {intervention}')
           self.manager.spawn(intervention)