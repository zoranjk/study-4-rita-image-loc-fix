# -*- coding: utf-8 -*-
"""
.. module:: attend_to_shop_votes
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to monitoring shop votes

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind players to attend to votes to return to the
shop.

* Trigger: The intervention is triggered when entering the Field state

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  Determine individual and team compliance to intervention, i.e., 
              do one of the players unfreeze the frozen player?
"""

from .intervention import AttendToShopVotesIntervention
from .trigger import AttendToShopVotesTrigger
from .individual_compliance import AttendToShopVotesIndividualCompliance

InterventionClass = AttendToShopVotesIntervention
TriggerClass = AttendToShopVotesTrigger
IndividualComplianceFollowupClass = AttendToShopVotesIndividualCompliance
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []