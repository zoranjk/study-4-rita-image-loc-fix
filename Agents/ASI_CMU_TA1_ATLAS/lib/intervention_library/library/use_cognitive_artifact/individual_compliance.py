# -*- coding: utf-8 -*-
"""
.. module:: use_cognitive_artifact.individual_compliance
    :platform: Linux, Windows, OSX
    :synopsis: Followup to determine individual compliance to UseCognitiveArtifact

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Followup class designed to determine individual compliance to
advice to use the cognitive artifact
"""


from InterventionManager import Followup


class UseCognitiveArtifactIndividualCompliance(Followup):
    """

    """

    def __init__(self, intervention, manager, **kwargs):
        """
        Arguments
        ---------
        intervention : UseCognitiveArtifactIntervention
            Intervention instance that this object is followuping up on
        manager : InterventionManager
        """

        Followup.__init__(self, intervention, manager, **kwargs)

