# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_chain_bombs.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Plan Rehersal Interventions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to provide just
in time tutoring on chain bombs
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import InstructOnChainBombsIntervention

class InstructOnChainBombsTrigger(InterventionTrigger):
    """
    Class that generates InstructOnChainBombsInterventions.  Interventions
    are triggered at the start of the recon phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be spawned at the start of a Shop stage

        if message.mission_stage == MissionStage.ReconStage:

            for participant in self.manager.participants:
                intervention = InstructOnChainBombsIntervention(self.manager, participant.id)
                self.logger.info(f'{self}:  Spawning {intervention}')
                self.manager.spawn(intervention)