# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_chain_bombs
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for providing tutorial on chain bombs

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to provide just-in-time tutorial on chain bombs

* Trigger: The intervention is triggered at the start of the Recon phase

* Discard States:  The intervention is discarded if the player has played before

* Resolve State:  The intervention is resolved when a chain bomb is observed

* Followups:  None
"""

from .intervention import InstructOnChainBombsIntervention
from .trigger import InstructOnChainBombsTrigger

InterventionClass = InstructOnChainBombsIntervention
TriggerClass = InstructOnChainBombsTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []