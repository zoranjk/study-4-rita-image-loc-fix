# -*- coding: utf-8 -*-
"""
.. module:: shared.bomb_monitor
   :platform: Linux, Windows, OSX
   :synopsis: Simple class to monitor when fuse times for bombs

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

"""

import collections

import MinecraftElements

from MinecraftBridge.utils import Loggable
from MinecraftBridge.messages import (
   CommunicationEnvironment,
   EnvironmentCreatedSingle,
   EnvironmentRemovedSingle,
   ObjectStateChange
)


class BombFuseMonitor(Loggable):
   """
   The BombFuseMonitor class 

   Attributes
   ----------

   Methods
   -------

   Usage
   -----


   """


   def __init__(self, parent):
      """
      Arguments
      ---------
      parent
         Parent component that hosts this monitor.  Note that this should be a
         Trigger, to ensure that the callbacks are properly added prior to 
         events occuring in Minecraft.
      """

      self._parent = parent

      self._bombs = dict()

      # Add callbacks to monitor when bombs are marked / inspected, as well as
      # when they're destroyed
      self._parent.add_minecraft_callback(CommunicationEnvironment, self.__onCommunication)
      self._parent.add_minecraft_callback(EnvironmentRemovedSingle, self.__onEnvironmentRemoved)

      print("===== BOMB FUSE MONITOR CREATED ===")


   @property
   def bombs(self):
      return self._bombs


   def __str__(self):
      return self.__class__.__name__
   


   def __onCommunication(self, message):
      """
      Callback when an environment communication message is sent
      """

      # Do nothing if a bomb did not send this message
      if not message.sender_id.startswith("BOMB"):
         return

      # Otherwise, get the x,y,z location of the bomb, and when it will go off
      self._bombs[message.sender_id] = { 'location': message.sender_position,
                                         'fuse_time': int(message.additional_info.get('fuse_start_minute', -1)) 
                                       }

      self.logger.info(f"{self}: Bomb {message.sender_id} discovered.  Bomb Fuse Dictionary:")
      for bomb in self._bombs:
         self.logger.info(f"{self}:    {bomb} @ {self._bombs[bomb]['location']} -- {self._bombs[bomb]['fuse_time']}")


   def __onEnvironmentRemoved(self, message):
      """
      Callback when a block is removed from the environment
      """

      # Do nothing if this is not a bomb
      if not message.object.id.startswith("BOMB"):
         return

      # Raise an alert if the bomb is not in the set of bombs
      if not message.object.id in self._bombs:
         self.logger.warning(f"{self}: {message.object.id} not in dictionary of bombs")
         return

      # Otherwise, simply remove the bomb
      del self._bombs[message.object.id]

      self.logger.info(f"{self}: Bomb {message.object.id} removed.  Bomb Fuse Dictionary:")
      for bomb in self._bombs:
         self.logger.info(f"{self}:    {bomb} @ {self._bombs[bomb]['location']}  -- {self._bombs[bomb]['fuse_time']}")
