from typing import Optional
import processscheduler as ps
from z3 import And, Implies, Or, BoolVal, IntVal
from processscheduler.constraint import TaskConstraint
from processscheduler.task import FixedDurationTask
from processscheduler.constraint import ResourceConstraint
import numpy as np

class TravelTime(ResourceConstraint):
    def __init__(self, task_1, workers_1, task_2, workers_2, dist, optional: Optional[bool] = False):
        super().__init__(optional)
        for worker in workers_1.selection_dict.keys():
            if worker in workers_2.selection_dict.keys():
                asst = Implies(
                    And(
                        And(workers_1.selection_dict[worker], workers_2.selection_dict[worker]), # If the same worker
                        task_1.end <= task_2.start # And the tasks are following eachother
                    ),
                    task_1.end + IntVal(dist) <= task_2.start
                )
                self.set_z3_assertions(asst)

class SameBombPhase(TaskConstraint):
    def __init__(self, task_1, task_2, optional: Optional[bool] = False) -> None:
        super().__init__(optional)
        asst = Or(task_1.start == task_2.start, task_1.end == task_2.start)
        self.set_z3_assertions(asst)

class BombChain(TaskConstraint):
    def __init__(self, task_1, task_2, optional: Optional[bool] = False) -> None:
        super().__init__(optional)
        scheduled_assertion = task_2.start >= task_1.end
        self.set_z3_assertions(scheduled_assertion)

class AssistDefusalTask(FixedDurationTask):
    def __init__(self, name: str, duration: int, pos: tuple, bomb_id: int, work_amount: Optional[int] = 0, priority: Optional[int] = 1, optional: Optional[bool] = False,) -> None:
        self.position = pos
        self.bomb_id = bomb_id
        self.needed = BoolVal(False)
        super().__init__(name, duration, work_amount, priority, optional)

class StartTask(FixedDurationTask):
    def __init__(self, name: str, pos: tuple) -> None:
        self.position = pos
        self.needed = BoolVal(False)
        super().__init__(name, 1, 0, 1, False)