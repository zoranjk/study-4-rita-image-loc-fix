# -*- coding: utf-8 -*-
"""
.. module:: shared.scheduler
   :platform: Linux, Windows, OSX
   :synopsis: Scheduler implementation for optimal task planning

.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Definition of a Singleton planner that solves the bomb scheduling task given
the scouting efforts of the team
"""

import processscheduler as ps
import csv
import json
import numpy as np
from shared.scheduler_constraints import SameBombPhase, BombChain, AssistDefusalTask, TravelTime, StartTask
# import matplotlib.pyplot as plt
# import matplotlib.colors as mcolors
import random
# import hashids
import time
import datetime
from threading import Semaphore

from MinecraftBridge.messages import CommunicationEnvironment, EnvironmentRemovedSingle, PlayerState
from MinecraftElements import Block

SECONDS_PER_STEP = 3 # How many seconds per step?
DEFUSAL_STEPS = 2 # How many steps to defuse one bomb phase?
WALK_SPEED = 4.317 / 2 # Blocks per second
START_POSITNON = (25,52,150)
MAX_TIME = 30 # Time in seconds
MAX_BOMBS_SCHEDULED = 7 # Even if players found more than this, we will only schedule the most urgent ones (because computing this schedule takes time...)

class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Also, the decorated class cannot be
    inherited from. Other than that, there are no restrictions that apply
    to the decorated class.

    To get the singleton instance, use the `instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)


@Singleton
class OptimalTaskScheduler(object):
    """
    Scheduler main class. This class will listen for bomb beacons and bombs that have been
    removed for various reasons. 

    Note:
    The scheudler takes ~3 minutes, but runs in a separate thread. However there can only be one scheduler
    at a time, thus ATLAS will lock up if, within this planning time, bombs are added or removed, or a store is visited again.
    However, it is probably "safe to assume" that players will take some time in the store, so this shouldn't be a problem
    """
    
    def __init__(self):
        self.player_positions = {}
        self.lock = Semaphore(1)
        self.bomb_list = []
        self.player_map = {}
        self.scheduling_task_valid = False
        self._is_setup = False
        self._keep_running = False

    def setupInterface(self, interface):
        """
        This will store the minecraft interface and register the callbacks for the scheduler
        """
        if not self._is_setup:
            self._is_setup = True
            self._interface = interface
            self._interface.register_callback(CommunicationEnvironment, self._onBeaconPlaced)
            self._interface.register_callback(EnvironmentRemovedSingle, self._onObjectRemoved)
            self._interface.register_callback(PlayerState, self._onPlayerLocation)

    def _onPlayerLocation(self, message):
        """
        Player location updates are tracked here. This is needed for the "start task" planning
        """
        if message.participant_id not in self.player_positions:
            self.player_positions[message.participant_id] = (0,0,0)
        self.player_positions[message.participant_id] = (message.x, message.y, message.z)

    def _onBeaconPlaced(self, message):
        """
        If a bomb beacon is placed, we add it to the list
        """
        bomb_info = message.additional_info
        # TODO: It would be good if this message would expose the sender type...
        # So here is a hack instead...
        if "bomb_id" not in bomb_info:
            return
        
        # Here, we can be reasonably sure that we are looking at a bomb
        self._addBomb(bomb_info, message.sender_x, message.sender_y, message.sender_z)

    def checkIfBombInList(self, tid, bomb_list = None):
        """
        Let's scheck if 
        """
        if bomb_list == None:
            bomb_list = self.bomb_list
        for bomb in bomb_list:
            if bomb[0] == tid:
                return True
        return False

    def _addBomb(self, bomb_info, px, py, pz):
        '''
        When players find a new bomb, add it to our list...
        '''
        with self.lock:
            sequence = bomb_info["remaining_sequence"].strip('][').split(', ')
            ### Corner case check on chained_id -- Dana
            try:
                dependancy = 0 if bomb_info["chained_id"] == "NONE" else int(bomb_info["chained_id"][5:])
            except Exception as e:
                ### Set dependency to zero if an int cannot be extracted.
                dependancy = 0
            bid = int(bomb_info["bomb_id"][4:])

            # Make sure we don't already know about it
            if self.checkIfBombInList(bid):
                return
            # NOTE:  Added a default value to "fuse_start_minute" to avoid issue with missing key error
            self.bomb_list.append((bid, (px, py, pz), int(bomb_info.get("fuse_start_minute",999)), sequence, dependancy))
            # print("Bombs Active:", len(self.bomb_list))
            # self.resetPlanner()
    
    def _onObjectRemoved(self, message):
        if message.object.type in [Block.block_bomb_standard, Block.block_bomb_fire, Block.block_bomb_chained]:
            bid = int(message.object.id[4:])
            self._removeBomb(bid)

    def _removeBomb(self, bid):
        '''
        Remove a bomb if it exploded or defused
        '''
        with self.lock:
            if not self.checkIfBombInList(bid):
                return
            tgt_id = None
            for i, bomb in enumerate(self.bomb_list):
                if bomb[0] == bid:
                    tgt_id = i
            if tgt_id is not None:
                del self.bomb_list[tgt_id]
            # print("Bombs Active:", len(self.bomb_list))
            # self.resetPlanner()

    def resetPlanner(self):
        self._keep_running = True
        with self.lock:
            # Scenario
            self.scenario = ps.SchedulingProblem('Study4', delta_time=datetime.timedelta(seconds=SECONDS_PER_STEP))
            self.tasks = []

            self.players = [ps.Worker(k) for k, v in self.player_positions.items()]

            self.tools = [
                ["R", "G"],
                ["G", "B"],
                ["R", "B"],
            ]
            
            bomb_subset = []
            # Add bombs to the subset, but make sure only valid chains are in there...
            for bomb in self.bomb_list:
                dep = bomb[4]
                if dep == 0 or self.checkIfBombInList(dep):
                    bomb_subset.append(bomb)
            # For computational reasons, make sure we only use MAX_BOMBS_SCHEDULED bombs max...
            # In order to make a somewhat useful selection, let's grab the ones with the shortest fuses.
            # Problem is that this may remove chain dependancies
            if len(bomb_subset) >= MAX_BOMBS_SCHEDULED:
                bomb_subset = sorted(bomb_subset, key=lambda x: x[2])[:MAX_BOMBS_SCHEDULED]
                # Restore chain dependancies if necessary...
                changed = True
                while changed:
                    changed = False
                    for b in bomb_subset:
                        dep = b[4]
                        # print("    Checking Dependancy for", b[0], "->", dep)
                        # print("     ", b)
                        if dep != 0 and not self.checkIfBombInList(dep, bomb_subset): # Might violate terms, but we gotta check...
                            bomb_subset.append(self.getBombByID(dep))
                            changed = True
                            # print("  Restored bomb", dep, "as dependancy")

            # print("  Scheduling {} bombs ({} total)".format(len(bomb_subset), len(self.bomb_list)))

            self.scheduling_task_valid = False
            success = False
            if len(bomb_subset) > 0:
                self.scheduling_task_valid = True
                success = self.buildTask(bomb_subset)
            else:
                print("No bombs found, failed to create scheduling task.")
            return success
    
    def getBombByID(self, tid, bomb_list = None):
        if bomb_list is None:
            bomb_list = self.bomb_list
        for b in bomb_list:
            if b[0] == tid:
                return b
        return None

    def solve(self):
        with self.lock:
            if self.scheduling_task_valid:
                self.scenario.add_objective_makespan()
                self.scenario.add_objective_start_earliest()
                solver = ps.SchedulingSolver(self.scenario, parallel=False, max_time=MAX_TIME, debug=False)
                solution = solver.solve()
                return solution
            return None

    def buildTask(self, bomb_subset):
        data = []
        fields = []
        self.tasks = []
        self.first_step_bombs = {}
        self.bomb_dependancy = {}
        self.bomb_locations = {}
        self.bomb_workers = {}

        if not self._keep_running:
            return False

        # Add start task...
        # print("  Adding Player Location tasks...")
        for p in self.players:
            if not self._keep_running:
                return False
            t_pos = (int(self.player_positions[p.name][0]), int(self.player_positions[p.name][2]))
            # print("   ", p.name, t_pos)
            task = StartTask(pos=t_pos, name=p.name)
            w = ps.SelectWorkers([p], 1)
            task.add_required_resource(w)
            ps.TaskEndBeforeStrict(task, 2)
            self.bomb_locations[p.name] = t_pos
            self.bomb_workers[p.name] = w
            self.tasks.append(task)
        # print("  Adding Bomb Tasks...")

        for row in bomb_subset:
            if not self._keep_running:
                return False
            # print("    Adding Bomb", row)
            # Use separate tasks for each bomb color, but constrain them to be in parallel
            # Create task for bomb
            bomb_tasks = []
            for i, ph in enumerate(row[3]):
                ph = ph.upper()
                priority = 1
                if int(row[1][2]) > 100:
                    priority = 3
                elif int(row[1][2]) > 50:
                    priority = 2
                task = AssistDefusalTask(
                    "" + str(row[0]) + "_" + ph, 
                    duration=DEFUSAL_STEPS, 
                    pos=(row[1][0], row[1][2]), 
                    bomb_id=row[0],
                    priority=priority
                    )
                self.bomb_locations["" + str(row[0]) + "_" + ph] = (row[1][0], row[1][2])
                if i == 0:
                    self.first_step_bombs[row[0]] = task
                    self.bomb_dependancy[row[0]] = row[4]
                fuse_time = row[2] * 60 # Gives us seconds
                fuse_time /= float(SECONDS_PER_STEP)
                fuse_time = np.floor(fuse_time)
                ps.TaskEndBeforeStrict(task, fuse_time)
                # Assign players to the task...
                workers = []
                for p, c in zip(self.players, self.tools):
                    # This adds the single-player can do it thing
                    # if np.all([v in c for v in [*row[4]]]):
                    if ph in c:
                        workers.append(p)
                select_workers = ps.SelectWorkers(workers, 1)
                self.bomb_workers["" + str(row[0]) + "_" + ph] = select_workers
                task.add_required_resource(self.bomb_workers["" + str(row[0]) + "_" + ph])
                bomb_tasks.append(task)

            for i in range(len(bomb_tasks)-1):
                SameBombPhase(bomb_tasks[i], bomb_tasks[i+1])
            self.tasks += bomb_tasks
        
        if not self._keep_running:
            return False

        # print("    Adding chains...")
        # Add the chain bombs:
        for k, v in self.bomb_dependancy.items():
            if not self._keep_running:
                return False
            if v == 0: # 0 means no dependancy. Bomb IDs start at 1
                continue
            BombChain(self.first_step_bombs[v], self.first_step_bombs[k])

        # print("    Adding travel constraints...")
        for ta in self.tasks:
            for tb in self.tasks:
                if not self._keep_running:
                    return False
                dist = np.linalg.norm(np.asarray(ta.position) - np.asarray(tb.position))
                dist = int(np.floor(dist / (WALK_SPEED * float(SECONDS_PER_STEP))))
                TravelTime(ta, self.bomb_workers[ta.name], tb, self.bomb_workers[tb.name], dist)
        # logging.info(f"Found {len(self.tasks)} bombs")
        return True

    def plotPaths(self, solution):
        data = json.loads(solution.to_json_string())["resources"]

        factor = 2.0
        fig = plt.figure(figsize=(5/factor,15/factor))
        ax = fig.gca()
        plt.plot([0,50],[50,50], color="black", linestyle=":")
        plt.plot([0,50],[100,100], color="black", linestyle=":")

        for pname in ["Alpha", "Bravo", "Charlie"]:
            path = data[pname]["assignments"]
            path = sorted(path, key=lambda x: x[1])
            locations = []
            names = []
            for bomb in path:
                if "T-" in bomb[0]:
                    continue
                locations.append(self.bomb_locations[bomb[0]])
                if "_" in bomb[0]:
                    names.append('B'+bomb[0].split("_")[0])
                else:
                    names.append("S")
            locations = np.asarray(locations)
            plt.plot(locations[:,0], locations[:,1], label=pname, alpha=0.5)
            for i in range(0, locations.shape[0]):
                ax.add_patch(plt.Circle((locations[i,0], locations[i,1]), 3, color=mcolors.CSS4_COLORS["wheat"]))
                plt.text(locations[i,0], locations[i,1], names[i], horizontalalignment='center', verticalalignment='center')

        plt.xlim(0, 50)
        plt.ylim(0, 150)
        plt.legend()
        solution.render_gantt_matplotlib()

    def getToolsAndPath(self, solution):
        data = json.loads(solution.to_json_string())["resources"]
        # print("Tools and Path:")
        p_tools = {}
        p_paths = {}
        for pname in self.player_positions.keys():
            path = data[pname]["assignments"]
            path = sorted(path, key=lambda x: x[1])
            # print(" ",pname)
            tools = {"R": 0, "G": 0, "B": 0}
            for loc in path:
                tools["R"] += 1 if "_R" in loc[0] else 0
                tools["G"] += 1 if "_G" in loc[0] else 0
                tools["B"] += 1 if "_B" in loc[0] else 0
            p_tools[pname] = (tools["R"], tools["G"], tools["B"])
            p_paths[pname] = path[1:] # Cut away the start task...
        return p_tools, p_paths
