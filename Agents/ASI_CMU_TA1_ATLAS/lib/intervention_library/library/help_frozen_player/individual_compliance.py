# -*- coding: utf-8 -*-
"""
.. module:: help_frozen_player.individual_compliance
   :platform: Linux, Windows, OSX
   :synopsis: Followup to determine individual compliance to HelpFrozenPlayer

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Followup class designed to determine individual compliance to
advice to helping frozen players
"""


from InterventionManager import Followup

from MinecraftBridge.messages import PlayerStateChange


class HelpFrozenPlayerIndividualCompliance(Followup):
   """

   """

   def __init__(self, intervention, manager, **kwargs):
      """
      Arguments
      ---------
      intervention : HelpFrozenPlayerIntervention
         Intervention instance that this object is followuping up on
      manager : InterventionManager
      """

      Followup.__init__(self, intervention, manager, **kwargs)

      # Listen for 1) if the player is unfrozen, and 2) if a player used a tool
      # on the frozen player
      self.add_minecraft_callback(PlayerStateChange, self.__onPlayerStateChange)
      self.add_minecraft_callback(ItemUsed, self.__onItemUsed)


   def __onItemUsed(self, message):
      """
      Callback when an item is used

      Arguments
      ---------
      message : MinecraftBridge.messages.ItemUsed
      """

      self.logger.debug(f'{self}: Received Message {message}')

      # Check to see if the target participant of this followup is the one that
      # used the item.  Ignore if not.

      # TODO:  Determine what message(s) would indicate that a player has
      #        applied a tool to the frozen participant


   def participant_was_unfrozen(self):
      """
      Method invoked by other Followup instances when the player was unfrozen
      by a team mate
      """

      # This compliance is no longer relevant
      self.compete()



   def __onPlayerStateChange(self, message):
      """
      Callback when a player's state has changed

      Arguments
      ---------
      message : MinecraftBridge.message.PlayerStateChange
      """

      # Determine if this is about the frozen participant
      if message.participant_id == self.intervention.frozen_participant:

         # Did the player become unfrozen?
         is_frozen_attribute = message.changed_attributes.get_attribute('is_frozen')
         if is_frozen_attribute is not None and is_frozen_attribute.current == 'false':

            # If this message has been received, then the player's frozen state
            # has changed to unfrozen without help from and participant (if a
            # participant had unfrozen the player, it would have been indicated
            # by an ItemUsed message or participant_was_unfrozen call)

            if self.intervention.presented:
               self.participant_complied = False

            self.complete()

