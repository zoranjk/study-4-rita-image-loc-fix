# -*- coding: utf-8 -*-
"""
.. module:: sync_tool_usage
   :platform: Linux, Windows, OSX
   :synopsis: Intervention components for teams to synchronize what tool they want to use for defusing

.. moduleauthor:: Keyang Zheng <kez20@pitt.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to suggest players to show what tool they want to use for a 
multi-step bomb

* Trigger: The intervention is triggered when players are waiting to defuse a multi-step bomb

* Discard States:  the bomb is a single state bomb

* Discard States:  the bomb is active

* Resolve State:  players holding the necessary tools near the bomb

* Followups:  Determine individual compliance to intervention, i.e., does the
              player begin to follow their plan again?
"""

from .intervention import SyncToolUsageIntervention
from .trigger import SyncToolUsageTrigger

InterventionClass = SyncToolUsageIntervention
TriggerClass = SyncToolUsageTrigger
IndividualComplianceFollowupClass = None #SyncToolUsageCompliance
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []