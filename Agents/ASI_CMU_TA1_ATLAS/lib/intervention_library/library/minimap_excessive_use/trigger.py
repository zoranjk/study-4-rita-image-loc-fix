# -*- coding: utf-8 -*-
"""
.. module:: minimap_excessive_use.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for MinimapExcessiveUseIntervention Intervention

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of a Trigger class designed to spawn 
MinimapExcessiveUseIntervention interventions.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import UI_Click

from .intervention import MinimapExcessiveUseIntervention

class MinimapExcessiveUseTrigger(InterventionTrigger):
   """
   Class that generates MinimapExcessiveUseIntervention. 
   Interventions are triggered when a player has placed icons
   beyond a specified threshold.
   """
   def __init__(self, manager, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Instance of the manager in charge of this trigger

      Keyword Arguments
      -----------------
      num_icons_threshold : int, default=15
         Threshold for the number of icons
      """

      InterventionTrigger.__init__(self, manager, **kwargs)
      self.kwargs = kwargs

      # Threshold for the number of icons
      self.__num_icons_threshold = kwargs.get('num_icons_threshold', 15)

      # Track icons placed
      self.__num_icons = 0

      # Listen for UI_Clicks for adding/removing of icons
      self.add_minecraft_callback(UI_Click, self.__onUI_Click)

   def __onUI_Click(self, message):
      """
      Callback when a UI_Click message is received
      """

      self.logger.debug(f'{self}: Received Message: {message}')
      
      # Adding icons
      if message.additional_info['meta_action'] == 'PLANNING_FLAG_PLACED' or \
         message.additional_info['meta_action'] == 'PLANNING_BOMB_PLACED':

         # Increase number of icons placed
         self.__num_icons += 1
      
      # Removing icons
      elif message.additional_info['meta_action'] == 'UNDO_PLANNING_FLAG_PLACED' or \
         message.additional_info['meta_action'] == 'UNDO_PLANNING_BOMB_PLACED': 

         # Decrease number of icons placed
         self.__num_icons = max(self.__num_icons - 1, 0)

      # Check threshold
      if self.__num_icons > self.__num_icons_threshold:

         # Spawn intervention if threshold is breached
         intervention = MinimapExcessiveUseIntervention(self.manager, 
                                                        message.participant_id,
                                                        self.__num_icons,
                                                        num_icons_threshold=self.__num_icons_threshold,
                                                        **self.kwargs)
         self.logger.info(f'{self}:  Spawning {intervention}')

         self.logger.warning(f'{self}:  Would have spawned {intervention}.')
         self.logger.warning(f'{self}:  Blocking as too many interventions are issued.')
### TODO: This spawns an intervention at _each_ added or modified flag after 15
###       This results in way too many interventions, blocking other
###       interventions from being issued.
#         self.manager.spawn(intervention)