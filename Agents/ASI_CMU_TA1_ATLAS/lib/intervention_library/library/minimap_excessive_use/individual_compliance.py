# -*- coding: utf-8 -*-
"""
.. module:: minimap_excessive_use.individual_compliance
   :platform: Linux, Windows, OSX
   :synopsis: Followup to determine individual compliance to MinimapExcessiveUse

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of a Followup class designed to determine individual compliance to
advice of encouraging a player against the excessive usage of icons on minimap.
"""


from InterventionManager import Followup

from MinecraftBridge.messages import UI_Click

class MinimapExcessiveUseIndividualCompliance(Followup):
   """
   
   """

   def __init__(self, intervention, manager, **kwargs):
      """
      Arguments
      ---------
      intervention : MinimapExcessiveUseIntervention
         Intervention instance that this object is followuping up on
      manager : InterventionManager
      """

      Followup.__init__(self, intervention, manager, **kwargs)

      # Track icons placed
      self.__num_icons = intervention.num_icons

      # Listen for UI_Clicks for adding/removing of icons
      self.add_minecraft_callback(UI_Click, self.__onUI_Click)

   def __onUI_Click(self, message):
      """
      Callback when a UI_Click message is received
      """

      self.logger.debug(f'{self}: Received Message: {message}')

      # Determine if this is about the participant that is in danger
      if message.participant_id == self.intervention.participant_id:

         # Adding icons
         if message.additional_info['meta_action'] == 'PLANNING_FLAG_PLACED' or \
            message.additional_info['meta_action'] == 'PLANNING_BOMB_PLACED':

            # Increase number of icons placed
            self.__num_icons += 1
         
         # Removing icons
         elif message.additional_info['meta_action'] == 'UNDO_PLANNING_FLAG_PLACED' or \
            message.additional_info['meta_action'] == 'UNDO_PLANNING_BOMB_PLACED': 

            # Decrease number of icons placed
            self.__num_icons = max(self.__num_icons - 1, 0)

         # Check threshold
         if self.__num_icons < intervention.num_icons_threshold:

            # Intervention presented
            if self.intervention.presented:
               self.participant_complied = True

            self.complete()