# -*- coding: utf-8 -*-
"""
.. module:: minimap_excessive_use.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used to advise a player against 
      the excessive usage of icons on minimap.

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of an Intervention class designed to advise a player against 
the excessive usage of icons on minimap.
"""

from InterventionManager import Intervention

class MinimapExcessiveUseIntervention(Intervention):
   """
   An MinimapExcessiveUseIntervention is an Intervention which monitors the
   number of icons present and encourages ta player against the excessive 
   usage of icons on minimap.
   """

   def __init__(self, manager, participant_id, num_icons, **kwargs):
      """
      Arguments
      ---------
      manager : InterventionManager
         Manager for this intervention
      participant_id : str
         ID of the participant being monitored
      num_icons : int 
         Number of icons that participant has placed at point of intervention generation

      Keyword Arguments
      -----------------
      num_icons_threshold : int, default=15
         Threshold for the number of icons
      """

      # Default values for intervention prioritization information
      kwargs["priority"] = kwargs.get("priority", 1)
      kwargs["expiration"] = kwargs.get("expiration", 45000)
      kwargs["view_duration"] = kwargs.get("view_duration", 15000)

      Intervention.__init__(self, manager, **kwargs)

      # Who is being monitored
      self._participant_id = participant_id
      self.addRelevantParticipant(self._participant_id)

      # Store relevant variables for Followup
      self._participant_id = participant_id
      self._num_icons = num_icons
      self._num_icons_threshold = kwargs.get('num_icons_threshold', 15)

      # Intervention message
      self._message = f'You currently have {self.num_icons} icons placed on the minimap. Consider removing some ' + \
                      f'uninformative icons such that you have less than {self.num_icons_threshold}'

   @property
   def participant_id(self):
     return self._participant_id

   @property
   def num_icons(self):
     return self._num_icons

   @property
   def num_icons_threshold(self):
     return self._num_icons_threshold

   def _onActivated(self):
      """
      Callback when the Intervention is activated.
      Immediately queue the Intervention for resolution.
      """
      self.queueForResolution()

   def getInterventionInformation(self):
      """
      Provide a dictionary containing a information necessary to present the
      intervention.
      """
      return {     
         'default_recipients': [self._participant_id],
         'default_message': self._message,
         'default_responses': ["I agree", "I disagree", "Skip"]
      }