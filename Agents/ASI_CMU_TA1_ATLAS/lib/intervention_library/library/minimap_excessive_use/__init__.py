# -*- coding: utf-8 -*-
"""
.. module:: minimap_excessive_use
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to advise 
      a player against the excessive usage of icons on minimap.

.. moduleauthor:: Yu Quan Chong <yuquanc@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to advise a player against the excessive usage of 
icons on minimap.

* Trigger: The intervention is triggered when a player has placed icons
           beyond a specified threshold.

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  Determine individual compliance to intervention, i.e., 
              does the player remove some icons till below specified 
              threshold?
"""

from .intervention import MinimapExcessiveUseIntervention
from .trigger import MinimapExcessiveUseTrigger
from .individual_compliance import MinimapExcessiveUseIndividualCompliance

InterventionClass = MinimapExcessiveUseIntervention
TriggerClass = MinimapExcessiveUseTrigger
IndividualComplianceFollowupClass = MinimapExcessiveUseIndividualCompliance
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []