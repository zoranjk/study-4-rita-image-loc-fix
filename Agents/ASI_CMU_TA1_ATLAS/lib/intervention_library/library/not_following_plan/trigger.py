# -*- coding: utf-8 -*-
"""
.. module:: not_following_plan.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for Not Following Plan Intervention

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn NotFollowingPlan interventions
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import NotFollowingPlanIntervention

class NotFollowingPlanTrigger(InterventionTrigger):
    """
    Class that generates NotFollowingPlanInterventions.  Interventions are 
    triggered at the start of each field phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if the mission stage is the
        # Field stage

        if message.mission_stage == MissionStage.FieldStage:

            # Spawn an intervention for each participant
            for participant in self.manager.participants:
                intervention = NotFollowingPlanIntervention(self.manager, participant.id)
                self.logger.info(f'{self}:  Spawning {intervention}')
                self.manager.spawn(intervention)