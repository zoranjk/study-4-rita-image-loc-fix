# -*- coding: utf-8 -*-
"""
.. module:: not_following_plan.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for not following a plan

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to indicate to a plarticipant when
they are not following their pre-defined plan
"""

import math
import time

from InterventionManager import Intervention

from MinecraftBridge.messages import (
    MissionStageTransition, MissionStage,
    ObjectStateChange,
    PlayerState
)

class NotFollowingPlanIntervention(Intervention):
    """
    A NotFollowingPlanIntervention is an Intervention which monitors messages
    from the PlanMonitor for deviations to the plan, and provides advise to the
    participant to adhere to their plan.
    """

    @staticmethod
    def distance(p1, p2):
        """
        Calculate the distance between two points
        """

        return math.sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)


    def __init__(self, manager, participant_id, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Participant ID to monitor
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)

        self.__all_setup = False

        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self._default_message = kwargs.get("message", "You seem to be deviating from your agreed-upon plan.")

        self.logger.debug(f'{self}:  Initializing intervention for {self._participant_id}')

        self._plan_steps = []

        self._request_id = None

        # Indicate the participants relevant to this intervention
        self.addRelevantParticipant(self._participant_id)

        # Callbacks needed to monitor the player's behavior
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)
        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(ObjectStateChange, self.__onObjectStateChange)
        self.add_redis_callback(self.__onPlanQueryResponse, 'plan_supervisor/response/plan')

        # Keep track of the number of unplanned defusals, and how many need to
        # be peformed before intervening
        self._num_unplanned_defusals = 0
        self._unplanned_defusal_threshold = kwargs.get("unplanned_defusal_threshold", 1)

        # How far from location-based steps is considered acceptable?
        self._distance_threshold = kwargs.get("distance_threshold", 10)

        self.__all_setup = True


    def __str__(self):
        if not self.__all_setup:
            return self.__class__.__name__
        else:
            return "%s <%s; %s>" % (self.__class__.__name__, self._participant_id, self._request_id)


    def _onActivated(self):
        """
        Callback when the intervention is activated.  Used to trigger querying
        the plan supervisor for this participant's plan steps.
        """

        # Query the Plan Supervisor for the plan steps this player intends to follow
        message_payload = { 'name': None,
                            'participants': [self._participant_id],
                            'conditions': []
                          }

        self.logger.debug(f"{self}:  Sending query of {self._participant_id}'s plan")

        self._request_id = self.manager.request_redis(message_payload,
                                                      "plan_supervisor/query/plan",
                                                      blocking=False)

        self.logger.debug(f"{self}:  Querying {self._participant_id}'s plan sent with response ID {self._request_id}")


    @property
    def participant_id(self):
        return self._participant_id


    @property
    def plan_steps(self):
        return self._plan_steps


    def __onPlayerState(self, message):
        """
        """

        player_location = message.x, message.z

        # Update the local plan steps for GoToWaypoint, Search, and GroupUp by
        # checking if the distance from the player's location to the step
        # location is below some threshold
        for step in self._plan_steps:
            if step['action']['type'] in {'GoToWaypoint', 'Search', 'GroupUp'}:
                if self.distance(step['action'].get('location',[-10000,-10000]),
                                 player_location) < self._distance_threshold:
                    step['is_complete'] = True

        # Check for any plan disobedience
        self.__check_for_disobedience()


    def __onObjectStateChange(self, message):
        """
        Check to see if a bomb has been (attempted to be) defused
        """

        # Was the player involved in this object?
        if message.triggering_entity != self._participant_id:
            return

        # Did the bomb explode or become defused?  Discard if so
        outcome_change = message.changed_attributes.get_attribute("outcome")

        if outcome_change is not None:
            if outcome_change.current in {"DEFUSED", 
                                          "EXPLODE_TOOL_MISMATCH", 
                                          "TRIGGERED_ADVANCE_SEQ"}:

                # Go through the plan steps to try to match the bomb ID
                found_plan_step = False

                for step in self._plan_steps:
                    if step['action']['type'] == "DefuseBomb" and \
                       step['action']['bomb_id'] == message.id:

                       found_plan_step = True
                       step['is_complete'] = True

                # Was the plan step not found?  If so, then the player is
                # deviating from the plan
                if not found_plan_step:
                    self.__defused_unplanned_bomb(message)

        # Check for any plan disobedience
        self.__check_for_disobedience()


    def __onMissionStageTransition(self, message):
        """
        Callback when the mission stage changes
        """

        if message.mission_stage == MissionStage.ShopStage:
            self.logger.debug(f'{self}:  Entering Shop, discarding')
            self.discard()


    def __onPlanQueryResponse(self, message):
        """
        Callback when a response is provided to the plan query

        Arguments
        ---------
        message
            Message containing the player's plan
        """

        self.logger.debug(f'{self}:  Message Response Received: {message}')
        self.logger.debug(f'{self}:    Data: {message.data}')

        # Ignore any responses that show up if this intervention is no longer
        # active
        if self.state != Intervention.State.ACTIVE:
            self.logger.debug(f'{self}:  Intervention no longer active, ignoring {message}')

        # Only handle response IDs that this Intervention sent the query for
        if message.request_id != self._request_id:
            self.logger.debug(f'{self}:  Ignoring response with no matching request ID {message}')
            return

        # Remove the request ID from the set of request IDs to respond to
        self._request_id = None

        self._plan_steps = message.data.get('steps', {}).get(self._participant_id, [])
        self._plan_steps = [step for step in self._plan_steps if not step['is_complete']]


        self.logger.info(f"{self}:  === Plan Steps for {self.participant_id} ===")
        for i, step in enumerate(self._plan_steps):
            self.logger.info(f"{self}:    {i}. {step}")


    def __check_for_disobedience(self):
        """
        Callback to check if the player is deviating from their plan
        """
        
        # Determine if plan steps are being performed out of order
        complete_steps = [s for s in self._plan_steps if s['is_complete']]
        incomplete_steps = [s for s in self._plan_steps if s['is_complete']]

        # What's the maximum priority of the complete steps, and minimum 
        # priority of the incomplete steps?
        priority_complete = 0 if len(complete_steps) == 0 else max([s['prioritization'] for s in complete_steps])
        priority_incomplete = 100000 if len(incomplete_steps) == 0 else min([s['prioritization'] for s in incomplete_steps])

        # Are there complete steps whose priority is higher than any incomplete
        # step?
        if priority_complete > priority_incomplete:
            self._message = "You seem to be executing your plan out of order.\n" + \
                            "You should return to the shop to discuss these changes with your team."
            self.queueForResolution()


    def __defused_unplanned_bomb(self, message):
        """
        Callback when a player has defused a bomb they did not plan to defuse
        """

        self._num_unplanned_defusals += 1

        # If the player persists on not following their plan, then intervene
        if self._num_unplanned_defusals > self._unplanned_defusal_threshold:
            self._message = "You seem to be deviating from your agreed upon plan.  " + \
                            "You should return to the shop to discuss these changes with your team."
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [self._participant_id],
            'default_message': self._message,
            'default_responses': ["I agree", "I disagree", "Skip"]
        }