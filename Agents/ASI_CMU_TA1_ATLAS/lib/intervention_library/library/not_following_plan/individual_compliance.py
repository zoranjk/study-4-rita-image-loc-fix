# -*- coding: utf-8 -*-
"""
.. module:: not_follow_plan.individual_compliance
   :platform: Linux, Windows, OSX
   :synopsis: Followup to determine individual compliance to NotFollowPlan

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Followup class designed to determine individual compliance to
advice regarding following the agreed-upon plan
"""


from InterventionManager import Followup


class NotFollowPlanIndividualCompliance(Followup):
   """
   """

   def __init__(self, intervention, manager, **kwargs):
      """
      Arguments
      ---------
      intervention : HelpFrozenPlayerIntervention
         Intervention instance that this object is followuping up on
      manager : InterventionManager
      """

      Followup.__init__(self, intervention, manager, **kwargs)