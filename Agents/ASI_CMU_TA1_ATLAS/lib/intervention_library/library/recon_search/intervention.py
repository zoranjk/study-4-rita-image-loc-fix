# -*- coding: utf-8 -*-
"""
.. module:: attend_to_shop_votes.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention used to remind players of remaining recon time

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to remind players of remaining
recon time, and to encourage balanced search
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import (
    PlayerState,
    MissionStageTransition, 
    MissionStage
)


class ReconSearchIntervention(Intervention):
    """
    The ReconSearchIntervention provides an update on how much of each region
    has been covered, and suggests areas to search next at a given point in 
    time.
    """

    def __init__(self, manager, search_monitor, resolution_time, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        search_monitor : shared.search_monitor.SearchMonitor
            Monitor of how much players have searched
        resolution_time : int
            Number of elapsed milliseconds when the intervention should be
            resolved
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 0.75)
        kwargs["expiration"] = kwargs.get("expiration", 20000)
        kwargs["view_duration"] = kwargs.get("view_duration", 7000)

        Intervention.__init__(self, manager, **kwargs)

        self._resolution_time = resolution_time
        self._search_monitor = search_monitor

        self._include_responses = False

        self._message = f"There are about {int(180 - self._resolution_time/1000)} seconds of recon remaining."

        self.add_minecraft_callback(PlayerState, self.__onPlayerState)
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Callback when the mission stage transition occurs.  Used to discard the
        intervention when transitioning into the field.  Note that this should
        never be called.
        """

        if message.mission_stage == MissionStage.ShopStage:
            self.logger.info(f'{self}:  Transitioning to Shop Stage, discarding intervention')
            self.discard()


    def __onPlayerState(self, message):
        """
        Callback when a PlayerState message is received.  Used ot determine
        current elapsed_milliseconds (may be better to use scheduler?  would
        still need to reset...).
        """

        # Is it time to resolve?
        if message.elapsed_milliseconds >= self._resolution_time:
            # How much has each region been explored?  These regions will be 
            # weighted by how difficult they are to search, increasing the 
            # search count of the desert by 25%, and decreasing the forest 
            # count by 25%
            desert_count = 1.25*self._search_monitor.desert_search_count()
            village_count = 1.0*self._search_monitor.village_search_count()
            forest_count = 0.75*self._search_monitor.forest_search_count()

            # What's the total count?
            total_count = desert_count + village_count + forest_count

            # If any one count is under 15% of the total, or over 70%, then
            # append to the message that search efforts should be better
            # distributed.
            if (desert_count / total_count) > 0.70:
                self._message += '  You should put more effort into exploring the village and forest regions.'
                self._include_responses = True
            elif (village_count / total_count) > 0.70:
                self._message += '  You should put more effort into exploring the desert and forest regions.'
                self._include_responses = True
            elif (forest_count / total_count) > 0.70:
                self._message += '  You should put more effort into exploring the desert and village regions.'    
                self._include_responses = True
            elif (desert_count / total_count) < 0.15:
                self._message += '  You should put more effort into exploring the desert region.'
                self._include_responses = True
            elif (village_count / total_count) < 0.15:
                self._message += '  You should put more effort into exploring the village region.'
                self._include_responses = True
            elif (forest_count / total_count) < 0.15:
                self._message += '  You should put more effort into exploring the forest region.' 
                self._include_responses = True
            else:
                self._include_responses = False

            # And resolve
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """

        if self._include_responses:
            responses = ["I agree", "I disagree", "Skip"]
        else:
            responses = []


        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': responses
        }