# -*- coding: utf-8 -*-
"""
.. module:: recon_search
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to monitoring team 
              search efforts during the recon phase

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to remind players of remaining time during the recon
phase, and to provide suggestions on where to search nest.

* Trigger: The intervention is triggered when entering the Recon state

* Discard States:  N/A

* Resolve State:  The intervention is queued for a resolution at fixed times

* Followups:  N/A
"""

from .intervention import ReconSearchIntervention
from .trigger import ReconSearchTrigger

InterventionClass = ReconSearchIntervention
TriggerClass = ReconSearchTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []