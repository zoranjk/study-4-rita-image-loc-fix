# -*- coding: utf-8 -*-
"""
.. module:: instruct_on_fire_extinguishers
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for providing tutorial on the cogntive
              artifact

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to provide just-in-time tutorial on fire extinguishers

* Trigger: The intervention is triggered at the start of the Recon phase

* Discard States:  The intervention is discarded if the player has played 
                   before, or purchases a fire extinguisher

* Resolve State:  The intervention is resolved at the start of a fire

* Followups:  None
"""

from .intervention import InstructOnFireExtinguishersIntervention
from .trigger import InstructOnFireExtinguishersTrigger

InterventionClass = InstructOnFireExtinguishersIntervention
TriggerClass = InstructOnFireExtinguishersTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []