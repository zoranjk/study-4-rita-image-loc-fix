# -*- coding: utf-8 -*-
"""
.. module:: dont_mark_late_fuse_bombs_during_recon.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for suggesting to be efficient with using bomb
              beacons

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to suggest players to not use
bomb beacons on bombs with late fuse start times during recon phase
"""

from InterventionManager import Intervention

import MinecraftElements

from MinecraftBridge.messages import (
    MissionStageTransition, 
    MissionStage,
    CommunicationEnvironment
)

class DontMarkLateFuseBombsDuringReconIntervention(Intervention):
    """
    A DontMarkLateFuseBombsDuringReconIntervention suggests players not mark
    bombs with a late fuse start time, to ensure that there's sufficient number
    of marker blocks for the recon phase
    """

    def __init__(self, manager, participant_id, beacon_id, beacon_location,
                       message_time, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        participant_id : str
            Identifier of the participant who placed the marker
        beacon_id : str
            ID of the beacon placed
        beacon_location : Tuple[Int]
            (x,y,z) locatino of the beacon
        message_time : int
            Number of elapsed milliseconds when the beacon was placed

        Keyword Arguments
        -----------------
        max_fuse_start_time : int
            Maximum fuse start time deemed acceptable
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 120000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)
        
        Intervention.__init__(self, manager, **kwargs)

        self._participant_id = participant_id
        self.addRelevantParticipant(participant_id)

        self._beacon_id = beacon_id
        self._beacon_location = beacon_location
        self._message_time = message_time
        self._max_fuse_start_minute = kwargs.get("max_fuse_start_minute", 7)

        # Listen to the Minecraft bridge for when a hazard beacon is created
        self.add_minecraft_callback(CommunicationEnvironment, self.__onCommunication)
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    @property
    def participant_id(self):
        return self._participant_id


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should be discarded when the team enters the shop
        # phase
        if message.mission_stage != MissionStage.ReconStage:
            self.discard()


    def __onCommunication(self, message):
        """
        Callback when a communication is received
        """

        self.logger.debug(f"{self}:  Received {message}")
        
        # Did the beacon being monitored place this?
        if message.sender_id != self._beacon_id:
            return

        # What's the fuse start time?  Discard or resolve as appropriate
        fuse_start = int(message.additional_info.get("fuse_start_minute", '0'))
        if fuse_start <= self._max_fuse_start_minute:
            self.logger.debug(f"{self}:  Fuse start is {fuse_start}, discarding.")
            self.discard()
        else:
            self.logger.debug(f"{self}:  Fuse start is {fuse_start}, resolving.")
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """

        return {     
            'default_recipients': [self.participant_id],
            'default_message': f"I would suggest placing bomb beacons on bombs whose fuse times are {self._max_fuse_start_minute} or less during the recon phase.  You will have opportunity to get more bomb beacons shortly.",
            'default_responses': ["I agree", "I disagree", "Skip"]
        }