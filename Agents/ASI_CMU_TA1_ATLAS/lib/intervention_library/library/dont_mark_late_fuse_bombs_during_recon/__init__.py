# -*- coding: utf-8 -*-
"""
.. module:: dont_mark_late_fure_bombs_during_recon
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger suggesting players not use hazard
              beacons during the recon phase.

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to suggest to players to avoid marking bombs with late
fuse times.

* Trigger: The intervention is triggered when a bomb beacon is placed during
           the ReconPhase

* Discard States:  The intervention is discarded if the resulting bomb fuse
                   time is below N, or if the Recon phase ends

* Resolve State:  The intervention is resolved if the resulting bomb fuse time
                  is N or greater

* Followups:  None
"""

from .intervention import DontMarkLateFuseBombsDuringReconIntervention
from .trigger import DontMarkLateFuseBombsDuringReconTrigger

InterventionClass = DontMarkLateFuseBombsDuringReconIntervention
TriggerClass = DontMarkLateFuseBombsDuringReconTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []