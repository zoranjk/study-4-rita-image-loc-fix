# -*- coding: utf-8 -*-
"""
.. module:: player_frozen.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for PlayerFrozen Intervention

.. moduleauthor:: Ini Oguntola <ioguntol@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions to help
players avoid getting frozen again in the future.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import PlayerStateChange

from .intervention import PlayerFrozenIntervention

class PlayerFrozenTrigger(InterventionTrigger):
    """
    Class that generates PlayerFrozenIntervention instances.
    Interventions are triggered when a player becomes frozen.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(PlayerStateChange, self.__onPlayerStateChange)


    def __onPlayerStateChange(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.PlayerStateChange
            Received PlayerStateChange message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Determine if this message is due to a player being frozen
        is_frozen_attribute = message.changed_attributes.get_attribute('is_frozen')
        if is_frozen_attribute is not None and is_frozen_attribute.current == 'true':
            
            # A player has become frozen -- create and spawn an intervention
            intervention = PlayerFrozenIntervention(self.manager, message.participant_id)
            self.logger.info(f'{self}:  Spawning {intervention}')
            self.manager.spawn(intervention)
