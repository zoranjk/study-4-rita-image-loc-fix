# -*- coding: utf-8 -*-
"""
.. module:: player_frozen
   :platform: Linux, Windows, OSX
   :synopsis: 

.. moduleauthor:: Ini Oguntola

"""

from .intervention import PlayerFrozenIntervention
from .trigger import PlayerFrozenTrigger

InterventionClass = PlayerFrozenIntervention
TriggerClass = PlayerFrozenTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []

