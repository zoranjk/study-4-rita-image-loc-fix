# -*- coding: utf-8 -*-
"""
.. module:: buy_fire_extinguishers.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention informs the team of the number of fire exinguishers
              that should be bought

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to inform the team of the number
of fire exinguishers needed
"""

from InterventionManager import Intervention

class BuyFireExtinguishersIntervention(Intervention):
	"""
	A BuyFireExinguishersIntervention is an Intervention which informs the team
	of the expected number of fire extinguishers needed to put out fires in the
	environment.
	"""

	def __init__(self, manager, fire_monitor, **kwargs):
		"""
		"""

		# Default values for intervention prioritization information
		kwargs["priority"] = kwargs.get("priority", 1)
		kwargs["expiration"] = kwargs.get("expiration", 30000)
		kwargs["view_duration"] = kwargs.get("view_duration", 15000)		

		Intervention.__init__(self, manager, **kwargs)

		self._fire_monitor = fire_monitor

		self.__max_extinguisher_count = kwargs.get("max_extinguisher_count", 12)


	def _onActivated(self):
		"""
		Callback when the Intervention is activated.  Used to determine 1) if
		any fires exist, and 2) how many exinguishers would be needed.
		"""

		# Are there any fires?
		if len(self._fire_monitor._fire_locations) == 0:
			self.logger.debug(f'{self}:  No fires existing, discarding')
			self.discard()

		else:
			self.logger.debug(f'{self}:  Resolving, suggesting {self._get_num_extinguishers()} fire extinguishers')
			self.queueForResolution()


	def _get_num_extinguishers(self):
		"""
		A simple calculation is to assume fires are compact, and so one
		extinguisher can put out 9 fires.
		"""

		# The int operation will round the results down, so need to add one to
		# the results to be able to get _all_ the fires
		return int(len(self._fire_monitor._fire_locations) / 9) + 1


	def getInterventionInformation(self):
		"""
		Provide a dictionary containing information necessary to present the
		intervention.
		"""

		# How many fire extinguishers?
		how_many = self._get_num_extinguishers()
		if how_many > self.__max_extinguisher_count:
			how_many = "several"
		else:
			how_many = f"at least {how_many}"

		return {
			'default_recipients': [p.participant_id for p in self.manager.participants],
			'default_message': f'I suggest you purchase {how_many} fire extinguishers to put out existing fires.',
			'default_responses': []
#			'default_message': f'I suggest you purchase at least {self._get_num_extinguishers()} fire extinguishers to put out existing fires.'
		}