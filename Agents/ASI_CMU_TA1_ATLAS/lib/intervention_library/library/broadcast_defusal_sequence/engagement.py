# -*- coding: utf-8 -*-
"""
.. module:: broadcast_defusal_sequence.engagement
   :platform: Linux, Windows, OSX
   :synopsis: Functions used to indicate participant engagement with bombs

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Helper functions and data structures for 
"""

from collections import namedtuple
import enum


class EngagementStatus(enum.Enum):
   """
   Simple enumeration for indicating engagement state.
   """

   Engaged = "Engaged"
   Disengaged = "Disengaged"


"""
EngagementEvent is a simple namedtuple to store attributed related to when a
participant's engagement with a bomb changes.

Attibutes
---------
participant_id : string
   Identifier of the participant whose engagement status changed
status : EngagementStatus
   Indicate whether the participant engaged or disengaged from the bomb
elapsed_milliseconds : int
   Elapsed milliseconds since mission start when the event occured
message
   Message that caused the change in status
"""
EngagementEvent = namedtuple("EngagementEvent", ["participant_id", "status", "elapsed_milliseconds", "message"])

