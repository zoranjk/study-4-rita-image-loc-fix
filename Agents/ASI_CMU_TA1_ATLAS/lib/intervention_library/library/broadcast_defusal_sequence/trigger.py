# -*- coding: utf-8 -*-
"""
.. module:: broadcast_defusal_sequence.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for broadcast defusal sequence interventions

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn interventions regarding bomb
defusal sequences to engaged participants.
"""

from InterventionManager import InterventionTrigger

from .intervention import BroadcastDefusalSequenceIntervention

from shared.bomb_defusal_success_monitor import BombDefusalSuccessMonitor

from MinecraftBridge.messages import CommunicationEnvironment

class BroadcastDefusalSequenceTrigger(InterventionTrigger):
    """
    Class that generates BroadcastDefusalSequenceInterventions.  Interventions
    are triggered whenever a multi-stage bomb is identified.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        use_tom_for_engagement : boolean, default=False
            Identify whether engagement should be determined from ToM intent
            (True) or participant's distance to the bomb (False)
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self._use_tom_for_engagement = kwargs.get("use_tom_for_engagement", False)

        # List of bomb IDs with already active interventions
        # NOTE:  This *could* be replaced with a query to the manager for
        #        BroadcastDefusalSequenceInterventions that contain the bomb_id
        #        (if and when implemented)
        self.__discovered_bomb_ids = set()

        self._defusal_monitor = BombDefusalSuccessMonitor(self)
        self._current_mission_stage = None       # Not sure if this will be needed

        # Listen for when bombs emit their defusal sequence
        self.add_minecraft_callback(CommunicationEnvironment, self.__onCommunicationEnvironment)


    def __onCommunicationEnvironment(self, message):
        """
        Check the content of the communication to see if it contains 1) BOMB_ID
        and 2) SEQUENCE in its message.

        Arguments
        ---------
        message : MinecraftBridge.messages.PlanningStageEvent
            Received PlanningStageEvent message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # Parse the message, checking if it contains lines starting with BOMB_ID
        # and SEQUENCE.  If it doesn not contain both of these strings, simply
        # return
        if not "BOMB_ID:" in message.message or not "SEQUENCE:" in message.message:
            self.logger.debug(f'{self}:   Message does not contain "BOMB_ID" and/or "SEQUENCE".  Ignoring.')
            return

        message_lines = message.message.split('\n')

        bomb_id = None
        sequence = None

        for line in message_lines:
            if line.startswith("BOMB_ID:"):
                try:
                    bomb_id = line.split(':')[1].strip()
                except:
                    self.logger.warning(f'{self}: Could not extract BOMB_ID from message line: {line}')

            if line.startswith("SEQUENCE:"):
                try:
                    sequence = line.split(':')[1].strip()
                    sequence = [s.strip() for s in sequence.strip('[').strip(']').split(',')]
                    sequence = ''.join(sequence)
                except:
                    self.logger.warning(f'{self}: Could not extract SEQUENCE from message line: {line}')

        # Only proceed if a bomb_id and sequence have been extracted
        if bomb_id is None or sequence is None:
            return

        # Check if there is already an intervention about this bomb id
        if bomb_id in self.__discovered_bomb_ids:
            self.logger.debug(f'{self}: Bomb with ID {bomb_id} already discovered.  No intervention triggered.')
            return

        # Only trigger an intervention if its a multi-stage bomb
        if len(sequence) <= 1:
            self.logger.info(f'{self}: Bomb with ID {bomb_id} and sequence {sequence} not multi-staged.  No intervention triggered.')
            # Store the bomb so that it will later be ignored
            self.__discovered_bomb_ids.add(bomb_id)
            return

        # Otherwise, spawn of an intervention
        self.logger.debug(f'{self}: Discovered bomb with ID {bomb_id} and sequence {sequence}.')
        self.__discovered_bomb_ids.add(bomb_id)

        # Spawn the intervention
        intervention = BroadcastDefusalSequenceIntervention(self.manager, bomb_id, message.sender_position, sequence,
                                                            parent=self,
                                                            use_tom_for_engagement=self._use_tom_for_engagement,
                                                            is_initial=True,
                                                            defusal_monitor=self._defusal_monitor)
        self.logger.info(f'{self}:  Spawning {intervention} for Bomb ID {bomb_id} at {str(message.sender_position)}with sequence {sequence}.')
        self.manager.spawn(intervention)
