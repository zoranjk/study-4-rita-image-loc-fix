# -*- coding: utf-8 -*-
"""
.. module:: broadcast_defusal_sequence.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for broadcasting bomb defusal sequence to players

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to update participants engaged in
defusing a bomb with its change in sequence as its being defused.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import (
    PlayerState,
    ObjectStateChange,
    EnvironmentRemovedSingle,
    EnvironmentRemovedList
)

from .engagement import EngagementStatus, EngagementEvent

import math

import functools


class BroadcastDefusalSequenceIntervention(Intervention):
    """
    A BroadcaseDefusalSequenceIntervention is an Intervention which broadcasts
    the defusal sequence of a bomb to engaged participants.

    Attributes
    ----------
    bomb_id : string
        ID of the bomb
    bomb_location : Tuple[int]
        Location of the bomb being defused
    sequence : List[string]
        Current defusal sequence of the bomb
    engagement_history : List[EngagementEvent]
        The history of when the intervention believes that players became 
        engaged or disengaged with defusing the bomb.
    """

    def __init__(self, manager, bomb_id, bomb_location, sequence, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention
        bomb_id : int
            ID of the bomb being defused
        bomb_location : Tuple[int]
            Location of the bomb
        sequence : List[string]
            Sequence of defusal

        Keyword Arguments
        -----------------
        parent
            Object that spawned this intervention
        use_tom_for_engagement : boolean, default=False
            Whether engagement should be determined by ToM or player proximity
        is_initial : boolean, default=False
            Indicate whether this is the initial intervention in a chain, i.e.,
            should the intervention be resolved once the engagement of all 
            participants is known.
        engagement_distance : float, default=3.0
            Minimum distance between participant and bomb to consider the
            participant as engaging
        disengagement_distance : float, default=5.0
            Miminum distance between participant and bomb to consider the
            participant as no longer engaging
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 5000)
        kwargs["view_duration"] = kwargs.get("view_duration", 15000)
        kwargs["present_immediately"] = kwargs.get("present_immediately", True)

        Intervention.__init__(self, manager, **kwargs)

        self._bomb_id = bomb_id
        self._bomb_location = bomb_location
        self._sequence = sequence

        # Store whether this is the initial
        self._is_initial = kwargs.get("is_initial", False)

        # The intervention should keep track of the history of who is
        # engaged in defusing the bomb
        self._engaged_participants = dict()
        self._engagement_history = []

        # Distances to consider engagement and disengagement
        self._engagement_distance = kwargs.get("engagement_distance", 3.0)
        self._disengagement_distance = kwargs.get("disengagement_distance", 5.0)
        self._use_tom_for_engagement = kwargs.get("use_tom_for_engagement", False)

        self._current_mission_stage = kwargs.get("current_mission_stage", None)
        self._defusal_monitor = kwargs.get("defusal_monitor", None)
        self._min_team_failure_rate = kwargs.get("min_team_failure_rate", 0.1)

        # Default message
        self._message = "The status of this bomb is <UNKNOWN>"

        # How should this intervention be updated?
        if kwargs.get("use_tom_for_engagement", False):
            # TODO:  Setup for using ToM to determine engagement
            pass
        else:
            self.add_minecraft_callback(PlayerState, self.__onPlayerState)

        # The bomb state will need to be monitored to see if 1) the sequence
        # changes, or 2) the bomb is destroyed
        self.add_minecraft_callback(ObjectStateChange, self.__onObjectStateChange)
        self.add_minecraft_callback(EnvironmentRemovedList, self.__onEnvironmentRemovedList)
        self.add_minecraft_callback(EnvironmentRemovedSingle, self.__onEnvironmentRemovedSingle)


    @property
    def bomb_id(self):
        return self._bomb_id

    @property
    def bomb_location(self):
        return self._bomb_location

    @property
    def sequence(self):
        return self._sequence

    @property
    def engaged_participants(self):
        return self._engaged_participants
    
    @property
    def engagement_history(self):
        return self._engagement_history


    def __number_participants_engaged(self):
        """
        Helper function to indicate how many participants are currently engaged
        """
        
        return sum([v == EngagementStatus.Engaged for v in self._engaged_participants.values()])


    def __distance(self, position1, position2):
        """
        Simple function to determine the distance between two positions.

        Arguments
        ---------
        position1 : Tuple[float]
            (x,y,z) position of point 1
        position2 : Tuple[float]
            (x,y,z) position of point 2
        """

        return math.sqrt(sum(map(lambda x: (x[0]-x[1])**2, 
                                 zip(position1, position2))))


    def __onPlayerState(self, message):
        """
        Callback when a PlayerState message is received.  This is used to
        update engagement if a ToM component is not used to determine intents
        of each participant.

        Arguments
        ---------
        message : MinecraftBridge.messages.PlayerState
            Received message
        """

        self.logger.debug(f'{self}: Received Message: {message}')


        # This intervention is relevent if the team has demonstrated trouble 
        # with coordinating bomb defusal
        if self._defusal_monitor is None:
            team_failure_rate = 0.0
        else:
            team_failure_rate = self._defusal_monitor.team_failure_rate()

        if team_failure_rate < self._min_team_failure_rate:
            return


        # Get which participant the message is about and their position
        participant_id = message.participant_id
        position = message.position

        # Is the participant engaging or disengaging with the bomb?
        if self.__distance(self.bomb_location, position) < self._engagement_distance:
            if not participant_id in self._engaged_participants or self._engaged_participants[participant_id] != EngagementStatus.Engaged:
                # Participant has becomed engaged
                self._engaged_participants[participant_id] = EngagementStatus.Engaged
                self._engagement_history.append(EngagementEvent(participant_id,
                                                                EngagementStatus.Engaged,
                                                                message.elapsed_milliseconds,
                                                                message))
        if self.__distance(self.bomb_location, position) > self._disengagement_distance:
            if not participant_id in self._engaged_participants or self._engaged_participants[participant_id] != EngagementStatus.Disengaged:
                # Participant has become disengaged
                self._engaged_participants[participant_id] = EngagementStatus.Disengaged
                self._engagement_history.append(EngagementEvent(participant_id,
                                                                EngagementStatus.Disengaged,
                                                                message.elapsed_milliseconds,
                                                                message))

        # If this intervention is marked as `is_initial`, and the engagement
        # status of all the participants is known, then the message needs to be resolved
        if self._is_initial:
            # Check that all the participants are accounted for
###            all_statuses_known = True
###            for p in self.manager.participants:
###                all_statuses_known = all_statuses_known and (p.id in self._engaged_participants)
            all_statuses_known = functools.reduce(lambda x,y: x and y,
                                           [p.id in self._engaged_participants 
                                            for p in self.manager.participants])

            # Only resolve if more than one participant is engaged, otherwise,
            # discard
            if self.__number_participants_engaged() > 1:
                self.queueForResolution()
            else:
                self.discard()

            # Create an intervention to continue monitoring of the bomb
            # to provide updates when the sequence changes
            self.__spawn_next_intervention(self._sequence)


    def __onEnvironmentRemovedList(self, message):
        """
        Callback when a  list of removed environment blocks is received

        Arguments
        ---------
        message : MinecraftBridge.messages.EnvironmentRemovedList
        """

        self.logger.debug(f'{self}:  Recieved Message {message}')

        # If any of the objects are this bomb, then this intervention is done
        for object_ in message.objects:
            if object_.id == self._bomb_id:
                self.discard()
                return



    def __onEnvironmentRemovedSingle(self, message):
        """
        Callback when an environment block is removed

        Arguments
        ---------
        message : MinecraftBridge.messages.EnvironmentRemovedSingle
        """

        self.logger.debug(f'{self}:  Recieved Message {message}')

        # Was the removed object this bomb?  Then we're done
        if message.object.id == self._bomb_id:
            self.logger.info(f'{self}:  Bomb with Bomb ID {message.object.id} has been removed.')
            self.discard()


    def __onObjectStateChange(self, message):
        """
        Callback when an object's state has changed.  This is used to determine
        if the bomb of interest 1) changes its sequence, or 2) is destroyed.

        Arguments
        ---------
        message : MinecraftBridge.messages.ObjectStateChange
            Received message
        """

        self.logger.debug(f'{self}:  Received Message {message}, about {message.id}')

        # Determine if the object being updated is the bomb of interest
        if message.id != self._bomb_id:
            self.logger.debug(f'{self}:    Message is about object {message.id}, not {self._bomb_id}.  Ignoring')
            return

        # Did the bomb explode or become defused?  Discard if so
        outcome_change = message.changed_attributes.get_attribute("outcome")

        if outcome_change is not None:
            # Check if the outcome is "DEFUSED", and discard this intervention
            # if so.
            if outcome_change.current == "DEFUSED":
                self.discard()


        # Did the bomb sequence change?
        sequence_change = message.changed_attributes.get_attribute("sequence")

        if sequence_change is not None:
            # Sequence has changed --- update the bomb_sequence property of this
            # intervention
            self._sequence = sequence_change.current

            # Resolve this intervention if 1) there remains steps to dispose,
            # and 2) more than one participant is engaged.  Otherwise, discard.
            if len(self._sequence) > 0 and self.__number_participants_engaged() > 1:
                self.queueForResolution()
            else:
                self.discard()

            # Spawn the next intervention if there are still steps in the
            # sequence
            if len(self._sequence) > 0:
                self.__spawn_next_intervention(self._sequence)


    def __spawn_next_intervention(self, sequence):
        """
        Create a BroadcastDefusalSequence intervention to spawn after this one.
        """

        self.logger.info(f'{self}:  Spawning next intervention for bomb ID {self._bomb_id} and new sequence {sequence}.')

        intervention = BroadcastDefusalSequenceIntervention(self.manager, 
                                                            self._bomb_id, 
                                                            self._bomb_location,
                                                            sequence,
                                                            parent=self,
                                                            use_tom_for_engagement=self._use_tom_for_engagement,
                                                            engagement_distance=self._engagement_distance,
                                                            disengagement_distance=self._disengagement_distance,
                                                            is_initial=False)
        self.manager.spawn(intervention)


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p for p in self._engaged_participants if self._engaged_participants[p] == EngagementStatus.Engaged],
            'default_message': "The remaining sequence for this bomb is " + str(self._sequence),
            'default_responses': []
        }