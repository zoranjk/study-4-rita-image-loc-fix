# -*- coding: utf-8 -*-
"""
.. module:: remind_team_of_remaining_time.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for RemindTeamOfRemainingTime Intervention

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of a Trigger class designed to spawn time remaining interventions.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import RemindTeamOfRemainingTimeIntervention

class RemindTeamOfRemainingTimeTrigger(InterventionTrigger):
    """
    Class that generates RemindTeamOfRemainingTimeInterventions.  Interventions
    are triggered at the start of the first field phase.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        reminder_times : List[Tuple]
            List of tuples defining when to remind players of remaining time, in
            mm:ss
        """

        InterventionTrigger.__init__(self, manager, **kwargs)

        self._mission_duration = kwargs.get('mission_duration', (13,0))
#        self._reminder_times = kwargs.get('reminder_times', [(5,0),(8,0),(10,0),(12,0)]) 
        self._reminder_times = kwargs.get('reminder_times', [(12,0)]) 

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if 1) the mission stage is the
        # Shop stage, and 2) then number of times the team has transitioned to 
        # the shop is 1

        if message.mission_stage == MissionStage.FieldStage and \
           message.transitionsToShop <= 1:

            # Create an intervention for each reminder time
            for time in self._reminder_times:
                intervention = RemindTeamOfRemainingTimeIntervention(self.manager, time, end_mission_time=self._mission_duration)
                self.logger.debug(f'{self}:  Spawning {intervention}')
                self.manager.spawn(intervention)