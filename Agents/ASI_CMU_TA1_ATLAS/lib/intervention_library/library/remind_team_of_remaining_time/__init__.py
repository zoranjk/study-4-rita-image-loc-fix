# -*- coding: utf-8 -*-
"""
.. module:: remind_team_of_remaining_time
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for time monitoring

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to help the team monitor elapsed time.

* Trigger: The interventions are triggered at the start of the first Field Phase

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  None
"""

from .intervention import RemindTeamOfRemainingTimeIntervention
from .trigger import RemindTeamOfRemainingTimeTrigger

InterventionClass = RemindTeamOfRemainingTimeIntervention
TriggerClass = RemindTeamOfRemainingTimeTrigger
IndividualComplianceFollowupClass = None
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []