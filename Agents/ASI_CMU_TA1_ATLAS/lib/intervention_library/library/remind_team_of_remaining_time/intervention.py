# -*- coding: utf-8 -*-
"""
.. module:: remind_team_of_remaining_time.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for introductory message for team

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class designed to help the team monitor elapsed
time.
"""

from InterventionManager import Intervention

from MinecraftBridge.messages import PlayerState

class RemindTeamOfRemainingTimeIntervention(Intervention):
    """
    A RemindTeamOfRemainingTimeIntervention is an Intervention which simply 
    reminds the team of the remaining time at some given mission time
    """

    def __init__(self, manager, reminder_time, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 0.75)
        kwargs["expiration"] = kwargs.get("expiration", 20000)
        kwargs["view_duration"] = kwargs.get("view_duration", 7000)

        Intervention.__init__(self, manager, **kwargs)

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)

        self._message = "A gentle reminder, there is 0:00 remaining"

        self._reminder_time = reminder_time
        self._end_mission_time = kwargs.get('end_mission_time', (13,0))

        self.add_minecraft_callback(PlayerState, self.__onPlayerState)


    @property
    def reminder_time(self):
        return self._reminder_time


    def __calculate_remaining_time(self, end_time, current_time):
        """
        Helper funciton to calculate the time remaining, in minutes and
        seconds
        """

        remaining_minutes = end_time[0] - current_time[0]
        remaining_seconds = end_time[1] - current_time[1]

        if remaining_seconds < 0:
            remaining_minutes -= 1
            remaining_seconds += 60

        return remaining_minutes, remaining_seconds


    def __onPlayerState(self, message):
        """
        Callback when a PlayerState message is received.
        """

        mission_time = message.mission_timer

        # Is the mission time the same as the reminder time?
        if mission_time == self._reminder_time:
            self.logger.debug(f'{self}:  Reached reminder time {mission_time}')
            remaining_minutes, remaining_seconds = self.__calculate_remaining_time(self._end_mission_time, mission_time)
            self._message = "A gentle reminder, there is about {mm}:{ss:02d} remaining.".format(mm=remaining_minutes, ss=remaining_seconds)
            self.logger.debug(f'{self}:    Message: {self._message}')
            self.queueForResolution()


    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': []
        }