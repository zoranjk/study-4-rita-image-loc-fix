# -*- coding: utf-8 -*-
"""
.. module:: present_optimal_plan.intervention
   :platform: Linux, Windows, OSX
   :synopsis: Intervention for that suggests scouting for the team

.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Definition of an Intervention class designed to present the optimal plan to players
"""

from InterventionManager import Intervention
from shared.scheduler import OptimalTaskScheduler
from threading import Thread, Semaphore
from MinecraftBridge.messages import MissionStageTransition, MissionStage

class PresentOptimalPlanIntervention(Intervention):
    """
    A TeamWelcomeMessageIntervention is an Intervention which simply presents
    a welcome message to all participants in the team.
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Manager for this intervention

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """

        # Default values for intervention prioritization information
        kwargs["priority"] = kwargs.get("priority", 1)
        kwargs["expiration"] = kwargs.get("expiration", 60000)
        kwargs["view_duration"] = kwargs.get("view_duration", 25000)
        
        Intervention.__init__(self, manager, **kwargs)
        self._message = ""

        # Indicate that all participants are relevant to this intervention
        for participant in self.manager.participants:
            self.addRelevantParticipant(participant.participant_id)  

        # Get the planner
        self.planner = OptimalTaskScheduler.instance()
        self.planner.setupInterface(self.manager.minecraft_interface)  

        # Thread for the scheduler
        self._thread = None
        self._keep_running = True

        # Add Callbacks
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)

    def __onMissionStageTransition(self, message):
        """
        This will happen when the team leaves the store. If this intervention isn't done by that time
        (i.e. the scheduler hasn't finished anything yet), kill the scheduler and wait for the next opportunity
        """
        if message.mission_stage != MissionStage.ShopStage and self._thread is not None:
            self._keep_running = False
            self.planner._keep_running = False
            self._thread.join()
            # Discard this intervention
            print("Warning: Didn't find a solution in time. Terminating scheduler!")

    def _onActivated(self):
        """
        Callback when the Intervention is activated.
        Create a thread of the scheduler 
        """
        self._thread = Thread(target = self._runScheduler)
        self._thread.start()

    def _runScheduler(self):
        # print("#" * 80)
        # print("Resetting planner...")
        success = self.planner.resetPlanner()
        if not success:
            return
        # print("Starting planning task...")
        solution = self.planner.solve()
        if solution is False:
            print("There is no solution to the current problem...")
        if not self._keep_running:
            return 
        tools, path = self.planner.getToolsAndPath(solution)

        tool_string = ""
        for k, v in tools.items():
            tool_string += f"{k}: "
            tool_string += ", ".join([f"{q}x{c}" for q, c in zip(v, ["Red", "Green", "Blue"]) if q > 0])
            tool_string += "; "
            
        path_string = ""
        for k, v in path.items():
            path_string += f"{k}: "
            path_string += ", ".join([f"{b[0].replace('_', '')}" for b in v])
            path_string += "; "
            
        self._message = "Given your scouting efforts of {} bombs, I suggest to buy the following tools {}. I also think you should do the following defusal order for Bombs/Phases: {}. Come back to the store when you are done to get another update!".format(
            len(self.planner.bomb_list), tool_string, path_string)
        # print("INTERVENTION:", self._message)
        # print("#" * 80)
        self.queueForResolution()

    def getInterventionInformation(self):
        """
        Provide a dictionary containing a information necessary to present the
        intervention.
        """
        return {     
            'default_recipients': [p.id for p in self.manager.participants],
            'default_message': self._message,
            'default_responses': ["I agree", "I disagree", "Skip"]
        }