# -*- coding: utf-8 -*-
"""
.. module:: present_optimal_plan.trigger
   :platform: Linux, Windows, OSX
   :synopsis: Trigger for an intervetnion that suggests scouting

.. moduleauthor:: Simon Stepputtis <sstepput@andrew.cmu.edu>

Definition of a Trigger class designed to spawn scouting interventions.
"""

from InterventionManager import InterventionTrigger

from MinecraftBridge.messages import MissionStageTransition, MissionStage

from .intervention import PresentOptimalPlanIntervention

from shared.scheduler import OptimalTaskScheduler

class PresentOptimalPlanTrigger(InterventionTrigger):
    """
    Class that generates SuggestScoutingIntervention. 
    Interventions are triggered whenever there are not enough bomb beacons placed
    under the condition that there are still bombs left
    """

    def __init__(self, manager, **kwargs):
        """
        Arguments
        ---------
        manager : InterventionManager
            Instance of the manager in charge of this trigger

        Keyword Arguments
        -----------------
        message : string
            Welcome message to greet the team with
        """
        InterventionTrigger.__init__(self, manager, **kwargs)
        self.kwargs = kwargs

        # Register the trigger to receive messages when mission stage changes
        self.add_minecraft_callback(MissionStageTransition, self.__onMissionStageTransition)

        # The trigger always lives, so this is a hack to keep it around despite the intervention lifecycle 
        self.planner = OptimalTaskScheduler.instance()
        self.planner.setupInterface(self.manager.minecraft_interface)   


    def __onMissionStageTransition(self, message):
        """
        Arguments
        ---------
        message : MinecraftBridge.messages.MissionStageTransition
            Received MissionStageTransition message
        """

        self.logger.debug(f"{self}: Received Message {message}")

        # An intervention should only be spawned if 1) the mission stage is the
        # Shop stage, and 2) then number of times the team has transitioned to 
        # the shop is 1

        if message.mission_stage == MissionStage.ShopStage:
           intervention = PresentOptimalPlanIntervention(self.manager, **self.kwargs)
           self.logger.info(f'{self}:  Spawning {intervention}')
           self.manager.spawn(intervention)