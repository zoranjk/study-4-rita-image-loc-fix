# -*- coding: utf-8 -*-
"""
.. module:: help_frozen_player
   :platform: Linux, Windows, OSX
   :synopsis: Intervention and trigger for intervention to help frozen players

.. moduleauthor:: Dana Hughes <danahugh@andrew.cmu.edu>

Definition of an Intervention class and InterventionTrigger class for an 
intervention designed to inform players when a team member is frozen.

* Trigger: The intervention is triggered when a player is frozen.

* Discard States:  N/A

* Resolve State:  The intervention is immediately queued for a resolution

* Followups:  Determine individual and team compliance to intervention, i.e., 
              do one of the players unfreeze the frozen player?
"""

from .intervention import AttendToFieldVotesIntervention
from .trigger import AttendToFieldVotesTrigger
from .individual_compliance import AttendToFieldVotesIndividualCompliance

InterventionClass = AttendToFieldVotesIntervention
TriggerClass = AttendToFieldVotesTrigger
IndividualComplianceFollowupClass = AttendToFieldVotesIndividualCompliance
TeamComplianceFollowupClass = None

default_parameters = {}

AdditionalFollowupClasses = []