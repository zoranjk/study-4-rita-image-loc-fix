# Intervention Library
Ths repository is used to store Intervention classes used by ATLAS for ASIST Study 4.

## Development Status

The following table summarizes development status of Intervention modules, and who is responsible for development of each.  Refer to the Intervention Design Document for details on each intervention listed.  Please discuss any desired changes to Interventions with the individual responsible for the Intervention, and do not modify Interventions that have or are being tested.

| Intervention Module                      | Developer | Status     | Metadata | Single Player | Multiplayer |
|------------------------------------------|-----------|------------|----------|---------------|-------------|
| attend_to_field_votes                    | Dana      | tested     |          |               |             |
| attend_to_shop_votes                     | Dana      | tested     |          |               |             |
| bomb_started_without_resources           | Ini       | tested     | X        | X             |             |
| broadcast_defusal_sequence               | Dana      | tested     |          |               |             |
| buy_fire_extinguishers                   | Dana      | tested     | X        |               |             |
| cognitive_load_stale_marker_blocks       |           |            |          |               |             |
| complete_cognitive_artifact              | Dana      | tested     |          |               |             |
| dont_mark_late_fuse_bombs_during_recon   |           |            |          |               |             |
| dont_place_hazard_beacons_during_recon   |           |            |          |               |             |
| dont_run_through_fire                    | Dana      | tested     | X        | X             |             |
| evacuate_bomb_if_failing                 | Yu Quan   | tested     |          |               |             |
| fire_reached_bomb                        | Yu Quan   | tested     | X        | X             |             |
| follow_your_map                          |           | unassigned |          |               |             |
| help_frozen_player                       | Dana      | tested     |          |               |             |
| ignore_bomb_due_to_fire                  | Dana      | tested     | X        | X             |             |
| ignore_fire                              | Dana      | assigned   |          |               |             |
| ihmc_player_proximity_suggestion         |           | unassigned |          |               |             |
| inform_chain_dependency                  | Keyang    | assigned   |          |               |             |
| instruct_on_bomb_beacon                  | Dana      | tested     | X        | X             |             | 
| instruct_on_chain_bombs                  | Dana      | tested     | X        | X             |             |
| instruct_on_cognitive_artifact           | Dana      | tested     | X        |               |             |
| instruct_on_fire_extinguishers           | Dana      | tested     |          |               |             | 
| instruct_on_hazard_beacon                | Dana      | tested     | X        |               |             |
| instruct_on_injuries                     | Dana      | tested     | X        | X             |             |
| instruct_on_ppe                          | Dana      | tested     | X        |               |             |
| minimap_excessive_use                    | Yu Quan   | tested     | X        | X             |             |
| not_following_plan                       | Dana      | tested     | X        | X             |             |
| player_frozen_intervention               | Ini       | tested     | X        | X             |             |
| player_grouping_up                       | Joe       | assigned   |          |               |             |
| player_splitting_up                      | Joe       | assigned   |          |               |             |
| plan_prompting                           | Dana      | tested     |          |               |             |
|   prompt_fire                            |           |            | X        |               |             |
|   prompt_beacon                          |           |            | X        |               |             |
|   prompt_medic                           |           |            | X        |               |             |
|   prompt_disagreement                    |           |            |          |               |             |
| plan_rehersal                            | Dana      | tested     |          |               |             |
| present_optimal_plan                     | Simon     | tested     |          |               |             |
| recon_search                             | Dana      | tested     | X        | X             |             |
| refer_to_beacons_for_planning            |           |            |          |               |             |
| remind_players_of_remaining_time         | Dana      | tested     | X        | X             |             |
| remind_tool_order                        | Dana      | assigned   |          |               |             |
| resolve_plan_disagreement                | Dana      | tested     |          |               |             |
| review_plan_following                    | Dana      | assigned   |          |               |             |
| review_beacon_usage                      | Dana      | assigned   |          |               |             |
| review_bomb_defusal                      | Dana      | assigned   |          |               |             |
| share_money                              |           |            |          |               |             |
| split_tool_resources                     |           |            |          |               |             |
| suggest_scouting                         | Simon     | tested     | X        |               |             |
| suggest_store_visit                      | Simon     | assigned   |          |               |             |
| sync_tool_usage                          | Keyang    | testing    |          |               |             |
| team_welcome_message                     | Dana      | tested     | X        | X             |             |
| tool_recommendation                      | Keyang    | assigned   |          |               |             |
| use_cognitive_artifact                   | Dana      | tested     | X        |               |             |
| wrong_defusal_tool_used                  | Ini       | tested     | X        | X             |             |


## Creating Interventions

### Intervention Lifecycle

In the context of the Intervention Manager, interventions follow a lifecycle based on observations from the environment

1.  *Triggering*:  Interventions are _Triggered_ based on some observed event.  Triggering an Intervention implies the potential for advice to be given in the future, based on observations occuring after triggering

2.  *Active*:  After being triggered, Interventions are in an _Active_ state.  In this state, the Intervention observes behaviors (as event messages from the Minecraft and/or Redis message bus), and updates any internal state needed to decide if issuing advice is warranted or not.  If advice is considered warranted, then the Intervention should queue itself for resolution.  If behavior is such that advice is no longer relevant, then the Intervention should discard itself.

3.  *Queued for Resolution*:  Interventions may queue themselves to be _resolved_ in the _Active_ state, in which case the InterventionManager disconnects them from the message buses and queues them for resolution.  Interventions in this state are waiting to be resolved, which typically involve presenting advice to one or more participants.  The Intervention itself does not have direct control over presenting advice, as the manager may opt to delay, withhold, modify, or re-route advice based on team state.

4.  *Resolved*:  The Intervention has completed resolution.  Upon entering this state, the InterventionManager may issue one or more Followups

5.  *Discarded*:  Interventions may _discard_ themselves when in the _Active_ state, in which case the InterventionManager disconnects them from the message buses.  No further action is performed by the InterventionManager.


### Tutorial:  Creating an InterventionTrigger

#### Initialization

Creating an InterventionTrigger involves creating a subclass of the _InterventionTrigger_ class.  An _InterventionTrigger_ class and subclass _must_ accept an InterventionManager instance as an argument, and may accept any number of _keyword_ argument.  Since the manager only passes itself to the trigger, do not add additional positional arguments---instead, opt for keyword arguments with default values.  The trigger should call the superclass `__init__` method prior to specialized initialization, passing the manager instance and keyword arguments.

A minimally working trigger is demonstrated below:

```
    from InterventionManager import InterventionTrigger

    class MyInterventionTrigger(InterventionTrigger):

        def __init__(self, manager, **kwargs):
            # Call the superclass constructor
            InterventionTrigger.__init__(self, manager, **kwargs)

            # Other initialization code ...
```

#### Receiving Messages

InterventionTrigger instance use the `add_minecraft_callback` and `add_redis_callback` methods to indicate that they desire to receive messages of certain types / channels from the external MQTT messsage bus or internal Redis bus.  When calling these methods, the Trigger must pass a message type and callback function (for MQTT message) to the `add_minecraft_callback`., or callback function and message channel (for Redis messages) to the `add_redis_callback` method.  Typically, this is done during in the `__init__` method

```
    from InterventionManager improt InterventionTrigger

    from MinecraftBridge.messages import SomeMessage

    class MyInterventionTrigger(InterventionTrigger):

        def __init__(self, manager, **kwargs):

            # Call the superclass constructor
            InterventionTrigger.__init__(self, manager, **kwargs)

            # Register to receive MQTT and Redis messages
            self.add_minecraft_callback(SomeMessage, self.__onSomeMessage)
            self.add_redis_callback("some_channel", self.__onSomeOtherMessage)


        def __onSomeMessage(self, message):

            # What to do when a "SomeMessage" is received from the MQTT bus


        def __onSomeOtherMessage(self, message):

            # What to do when a message is received on the Redis bus on channel 
            # "some_channel"

```

#### Creating and Spawning Interventions

The role of the trigger is to create and spawn Intervention instances when some observed event occurs.  As the Trigger is responsible for creating the Intervention
instance, it may pass arbitrary arguments to the creation (see creating an Intervention below), as long as the manager is passed for the superclass initialization.

Once created, the Intervention instance is spawned using the `self.manager.spawn` method.  The complete trigger example is shown below

```
    from InterventionManager improt InterventionTrigger

    from MinecraftBridge.messages import SomeMessage

    from .my_intervention import MyIntervention

    class MyInterventionTrigger(InterventionTrigger):

        def __init__(self, manager, **kwargs):

            # Call the superclass constructor
            InterventionTrigger.__init__(self, manager, **kwargs)

            # Register to receive MQTT and Redis messages
            self.add_minecraft_callback(SomeMessage, self.__onSomeMessage)
            self.add_redis_callback("some_channel", self.__onSomeOtherMessage)


        def __onSomeMessage(self, message):

            # What to do when a "SomeMessage" is received from the MQTT bus.
            # Assume that this could include some condition being satisfied to 
            # spaawn the intervention
            if(some_condition_satisfied):
                intervention = MyIntervention(manager, arg1, arg2, ...)
                self.manager.spawn(intervention)


        def __onSomeOtherMessage(self, message):

            # What to do when a message is received on the Redis bus on channel 
            # "some_channel"

```

### Tutorial:  Creating an Intervention

#### Initialization and Receiving Messages

Creating an intervention bears much similarity to creating a tutorial:  each Intervention must inherit from the `Intervention` superclass, the InterventionManager instance must be passed to the intervention during initialization, and provided to the `Intervention` superclass to initialize.  Connection to the Minecraft and Redis buses is the same.  As a base example


#### Adding Relevant Participants

Interventions should indicate which participants the intervention is relevant for by calling the `addRelevantParticipant` method.  This provides a hint to the InterventionManager when, for instance, spawning Compliance Followups.

#### Providing Intervention Information

Each Intervention should define a `getInterventionInformation` method, which provides a dictionary containing, at a minimum, two fields:  a 'default_recipeints' field, which provides a list of participant IDs, and a 'default_message' field, which provides a string message as the default advice to provide.

#### Resolving and Discarding

Each Intervention is designed to end up in either a _Resovled_ or _Discarded_ state.  The Intervention instances themselves should indicate which state they should enter based on internal state and received messages, and can indicate which state they should enter using the `queueForResolution` or `discard` methods.  Note that an Intervention should only call one of these methods once during its lifecycle.

#### Example

```
from InterventionManager import Intervention

class MyIntervention(Intervention):

    def __init__(self, manager, arg1, arg2, **kwargs):

        Intervention.__init__(self, manager, **kwargs)

        self.addRelevantParticipant(some_participant_id)
        self.addRelevantParticipant(some_other_participant_id)

        self.add_minecraft_callback(SomeMessage, self.__onSomeMessage)
        self.add_redis_callback(self.__onSomeOtherMessage, "message_channel")

    def __onSomeMessage(self, message):

        # Received a SomeMessage instance
        if(some_resolution_condition_met):
            self.queueForResolution()
            return

        if(some_discard_condition_met):
            self.discard()
            return

    def __onSomeOtherMessage(self, message):

        # Received some Redis message on channel "message_channel"

    def getInterventionInformation(self):

        return { 'default_recipients':  [participant_1, participant_2],
                 'default_message': "Some Advice"
               }


```


## Adding an Intervention to the Library

To add an intervention to the library, create a folder with the intervention name in the `library` folder.  Each folder should have the following:

* Files containing the trigger, intervention, and (optional) individual and / or team compliance followup classes.
* An `__init__.py` file, which imports the trigger, intervention and compliance followup classes.
* The `__init__.py` file also needs to define the following variables:  
  * `InterventionClass`:  The main intervention class (not currently used)
  * `TriggerClass`:  The trigger class that will be injected into the InterventionManager
  * `IndividualComplianceFollowupClass` (optional):  A class to monitor individual compliance to the intervention
  * `TeamComplianceFollowupClass` (optional):  A class to monitor team compliance to the intervention
  * `default_parameters` (optional):  A dictionary mapping class names to keyword arguments used during construction (currently only the trigger is used).

An example `__init__.py` file is shown below

    from .my_intervention_file import MyIntervention, MyTrigger      # Note that you can have however many files within this
    from .my_other_file import MyFollowupIndividual, TeamFollowup    # folder

    InterventionClass = MyIntervention                               # This *must* be defined
    TriggerClass = MyTrigger                                         # This *must* be defined
    IndividualComplianceFollowupClass = MyFollowupIndividual         # This may be `None`, if no compliance followup exists
    TeamComplianceFollowupClass = None                               # This may also be `None`, if no compliance followup exists
    default_parameters = { "MyTrigger": { "arg1": 1, "arg2": 2, ...} }

### Shared Module

The `shared` folder in `library` is intended to act as a module to store code that may be utilized by multiple intervention modules.  This can include, for instance, classes for composition / delegation, Singletons (to the extent allowed by Python), helper functions, etc.

Intervention code should aim for locally importing, i.e., `from ..shared.<some_shared_module> import <some_shared_component>`.  

As the InterventionManager is now (or planned on) dynamically loading interventions based on class inheritance (i.e., it will dynamically load / configure itself to handle anything derived from Intervention, InterventionTrigger, or Followup).  Therefore, it is important to _not_ introduce subclasses of these classes into this folder.  As a side note, these classes are designed to handle intervention lifecycle concerns---subclassing / introducing a class hierarchy either introduces additional concerns (breaking encapsulation)

### Implementation Best Practices

#### Error Handling

By design, ATLAS catches any exceptions generated whenever dispatching a message object to a callback.  In terms of Intervention development, this means that any exception generated by an Intervention / Trigger / Followup will catch the exception and discard, minimizing any impact on the system as a whole (i.e., an exception in your Intervention won't bring the whole system crashing down).

That said, exceptions may occur during data collection from other performers (i.e., Aptima/ASU).  In most cases, generated logs will be available from every trial, so checking for and/or catching exceptions locally and issuing warning / error messages through the logger (see the Logging section below) would be helpful in identifying where and why the exception occured.  Even if your code is well behaved and catches exceptional conditions prior to throwing an exception, it is still worth issueing a warning to the logger that the exceptional condition was observed, so that the cause can be later addressed.

#### Logging

Each component (Interventions, Triggers, Followups) contain a `logger` attribute, which provides access to a class-specific logging.Logger instance (see [Logging Facility for Python][2]).  Logging levels can be set on a per-class instance in ATLAS's configuration files (e.g., ATLAS can be configured to print debug messages for certain classes, and leave other classes at info and higher levels), so the logger can an should be used judiciously.  Logging a message is as simple as

    self.logger.debug('<message>')     # Logs a debug message
    self.logger.info('<message>')      # Logs an info message
    self.logger.warning('<message>')   # Logs a warning
    self.logger.error('<message>')     # Logs an error

Also, don't use `print` statements for debugging, use `self.logger.debug` so they can all be turned off.



[1]:  https://gitlab.com/cmu_aart/asist/
[2]:  https://docs.python.org/3/library/logging.html