#!/bin/bash
shopt -s extglob # enable extended globbing features

DOCS_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
cd $DOCS_DIR

rm -r _build/html
rm !(index).rst
sphinx-apidoc -o ./ ../gym_dragon/ --separate
make html