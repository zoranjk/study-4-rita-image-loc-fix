import ray
import torch
import torch.nn as nn

from gym_dragon.envs import DragonEnv
from gym_dragon.wrappers import *
from ray import air, tune
from ray.rllib.algorithms.callbacks import DefaultCallbacks
from ray.rllib.models import ModelCatalog
from ray.rllib.models.torch.complex_input_net import ComplexInputNetwork
from ray.rllib.models.torch.torch_modelv2 import TorchModelV2
from ray.rllib.policy.policy import PolicySpec
from ray.rllib.utils.torch_utils import FLOAT_MIN
from ray.tune.registry import register_env



def masked_logits(logits: torch.Tensor, mask: torch.Tensor, float_min=FLOAT_MIN):
    """
    Apply a boolean mask to logits.

    Parameters
    ----------
    logits : torch.Tensor
        Logits tensor
    mask : torch.Tensor
        Mask tensor
    float_min : float, default=-3.4e38
        Minimum float value (to avoid `-inf`)
    """
    return logits + torch.clamp(torch.log(mask), min=float_min)


class DragonCallbacks(DefaultCallbacks):
    def on_episode_end(
        self, *, worker, base_env, policies, episode, env_index=None, **kwargs) -> None:
        env = base_env.get_sub_environments()[env_index]
        episode.custom_metrics['score'] = env.score


class Modelo(TorchModelV2, nn.Module):

    def __init__(self, obs_space, action_space, num_outputs, model_config, name):
        TorchModelV2.__init__(self, obs_space, action_space, num_outputs, model_config, name)
        nn.Module.__init__(self)

        # Models
        self.action_model = ComplexInputNetwork(
            obs_space, action_space, num_outputs, model_config, f'{name}_action')
        self.value_model = ComplexInputNetwork(
                obs_space, action_space, 1, model_config, f'{name}_value')

        # Variables
        self.value = None

    def forward(self, input_dict, state, seq_lens):
        self.value, _ = self.value_model(input_dict, state, seq_lens)
        logits, state = self.action_model(input_dict, state, seq_lens)
        logits = masked_logits(logits, input_dict['obs']['action_mask'])
        return logits, state

    def value_function(self):
        return self.value.squeeze(-1)


def make_dragon_env(env_config):
    env = DragonEnv(**env_config)
    env = ExploreReward(env, weight=0.1)
    env = ProximityReward(env, weight=0.001)
    return env

env_config = {
    'valid_regions': ['village'],
    'mission_length': 15*60,
    'obs_wrapper': FullyObservable,
}
env = make_dragon_env(env_config)

if __name__ == '__main__':
    ModelCatalog.register_custom_model('Modelo', Modelo)
    register_env('DragonEnv', make_dragon_env)
    ray.init(num_cpus=20)

    config = {
        # Basic
        'env': 'DragonEnv',
        'env_config': {
            'valid_regions': ['village'],
            'mission_length': 15*60,
            'obs_wrapper': FullyObservable,
        },
        'framework': 'torch',
        'multiagent': {
            'policies': {'policy': PolicySpec(observation_space=env.observation_space, action_space=env.action_space)},
            'policy_mapping_fn': lambda agent_id, episode, worker, **kwargs: 'policy'
        },
        'model': {'custom_model': 'Modelo'},

        # Training
        'lr': 1e-4,
        'num_gpus': 1,
        'num_workers': 2,

        # Metrics
        'callbacks': DragonCallbacks,

        # Evaluation
        'evaluation_config': {'render_env': True},
        'evaluation_num_workers': 1,
        'evaluation_interval': 50,
    }

    results = tune.Tuner(
        'IMPALA',
        param_space=config,
        run_config=air.RunConfig(
            stop={'timesteps_total': 1e8},
            verbose=1,
            checkpoint_config=air.CheckpointConfig(
                checkpoint_frequency=20,
                checkpoint_at_end=True,
            ),
        ),
    ).fit()

    ray.shutdown()
