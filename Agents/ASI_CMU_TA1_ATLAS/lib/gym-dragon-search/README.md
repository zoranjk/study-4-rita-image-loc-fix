# gym-dragon-search
The `gym-dragon-search` package implements Conflict-Based Search with Task Assignment, Precedence and Temporal Constraints (CBS-TA-PTC) to solve the task in the `gym-dragon` environment.

# Installation
```
git clone -b https://gitlab.com/cmu_aart/asist/gym-dragon
cd gym-dragon
pip install -e .
git clone -b https://gitlab.com/cmu_aart/asist/gym-dragon-search
cd gym-dragon-search
pip install -e .
```

# Demo
Set parameters in `test.py` to preference and run `python test.py`	.