# UCF Flocking Agent/AC

The UCF Flocking AC is based on the IHMC Agent Helper.

This AC computes player flocking behavior.


## Information Common to the IHMC Agent Helper

### Developing locally

The scripts `run_locally.sh` and `run_locally.cmd` have been provided for you to be able to run and develop your
agent locally without having to build and run a docker image.  For the code to work you will need to have Python
3.8 or more installed on your system, a working version of git, and then install the ASISTAgentHelper library
from the requirements.txt file using pip.  You will also need to make sure the `host` value in the `config.json`
file points to the location of the MQTT bus.

### Building and Running Your Agent with `./agent.sh` and `./agent.cmd`

The Agent scripts have been provided to help you build, start, stop, and export your agent as a docker container.
For linux systems use `agent.sh` and for Windows use `agent.cmd`.

The parameters which you can pass to these scripts are as follows (replace `./agent.sh` with `agent.cmd`
on windows systems):

    ./agent.sh build

`build` will build the docker image for your agent.  This must be done before you can run your agent and must
be re-run whenever you change `settings.env` or your agent's code.

    ./agent.sh up
`up` will start your docker image in a container and output will be sent to the console.
The container will stay running until you press Ctrl-C to stop it.

    ./agent.sh upd
`upd` will start your docker image in a container which will run in the background. Use the next
command to stop it.

    ./agent.sh down
`down` will stop your running docker container.

    ./agent.sh export
`export` will save your docker image as a tar file in the current directory.

