$image_list=
# #'asi_cmu_ta1_atlas:latest',
# 'ac_cmu_ta2_beard:latest',
# 'ac_cmu_ta2_ted:latest',
# 'ac_cmufms_ta2_cognitive:latest',
# 'ac_cornell_ta2_teamtrust:1.0.3',
# 'ac_ihmc_ta2_dyad-reporting:latest',
# 'ac_ihmc_ta2_location-monitor:latest',
# 'ac_ihmc_ta2_joint-activity-interdependence:latest',
# 'ac_ihmc_ta2_player-proximity:latest',
# 'heartbeat:2.0.2',
# 'asr_agent:4.0.4',
# 'ac_ucf_ta2_playerprofiler_img:v_0.0.8',
# 'asi_cra_ta1_psicoach:3.5.6.1'
# 'uaz_tmm_agent:3.5.8',
# 'atomic_agent:0.0.2',
# #'ac_gallup_ta2_gelp:latest',
# #'ac_gallup_ta2_gold:latest',
# 'ta1_doll_rita_rabbitmq:3.1.2022-5-12-study-3-alphonso',
#'ta1_doll_rita_clojure:3.1.2022-5-12-study-3-alphonso'
'ta1_doll_rita_tailer:3.1.2022-5-12-study-3-alphonso'
#'ta1_doll_rita_mqt2rmq:3.1.2022-5-12-study-3-alphonso'
# #'rutgers_agent_image:latest',
# 'sift_asistant:0.4.4',
# 'uaz_dialog_agent:4.1.5'



Foreach ($i in $image_list)
{
    $IMAGE_NAME = $i

    Write-Host 'Migrating '$IMAGE_NAME

    docker pull gitlab.asist.aptima.com:5050/asist/testbed/$IMAGE_NAME
    docker tag gitlab.asist.aptima.com:5050/asist/testbed/$IMAGE_NAME registry.gitlab.com/artificialsocialintelligence/study3/$IMAGE_NAME
    docker push registry.gitlab.com/artificialsocialintelligence/study3/$IMAGE_NAME

}

Write-Host 'Completed image migrations'


