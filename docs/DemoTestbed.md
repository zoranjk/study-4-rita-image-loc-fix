| **Master** [![pipeline status](https://gitlab.asist.aptima.com/asist/testbed/badges/master/pipeline.svg)](https://gitlab.asist.aptima.com/asist/testbed/commits/master) | **Develop** [![pipeline status](https://gitlab.asist.aptima.com/asist/testbed/badges/develop/pipeline.svg)](https://gitlab.asist.aptima.com/asist/testbed/commits/develop)
| -------- | -------- |


# ASIST

The objective of this project is to design and build a generalizable, cloud
testbed suitable for experimentally evaluating the performance of AI and
AI+Human collaborations.

### QUICK SETUP OF STUDY 4 DEMO TESTBED (UNDER CONSTRUCTION)

Please read the release notes found in `releaseNotes.md`

Quick Setup - Linux

1. Pull the latest code from https://gitlab.asist.aptima.com/asist/adminless-testbed.git
2. Navigate to the Local directory, and run testbed_build_adminless.sh
3. On successful build, run testbed_up_adminless.sh
4. Verify that no container has failed to launch @ https://your-ip-address:9000/Logger/

5. Create some fake players by registering them with the Waiting Room system via http POST. These player names must match the         Minecraft Client's playername or the Minecraft Server will not allow the connection.

    Ex: curl -k -X POST https://your-ip-address:9000/WaitingRoom/enter_player -H 'Content-Type: application/json' -d '{"name":"Player32",              "participant_id":"102"}'
    
    This request creates a Player32 with PID 102

    Configuration of the number of players that make up a team can be defined in Local/WaitingRoom/WaitingRoomConfig.json. This defined number of players will only be selected in the Waiting Room once they have all completed the survey.

6. Open your web browser to https://your-ip-address:9000/WaitingRoom/player/Player32
7. Take the fake survey and wait until you have at least 15 pings.
8. Click "Proceed To Experiment" when you have been selected.
9. The player should now find themselves in the ClientMap. Once the server is loaded a dialog will pop up informing the user
that they can connect to the server
10. Go ahead and connect your Minecraft Client.
11. Once all the players selected in the Waiting Room have logged into the Minecraft Server, the mission loop will start.
12. The mission ends after the configured number of stages have been exceeded, at this point the player is given an exit survey.
13. Once the exit survey is complete, the player is asked whether they wish to Continue With or Leave the team. If you continue, then
the server will restart once all other players select "Continue with Team".  If even one player chooses to disband the team, then all
players are sent back to the waiting room, and the cycle repeats.

Quick Setup - Windows - TBD

### Installing the Minecraft client 

See `docs/ClientSetup.md` for instructions on how to set up the client.

### Developing Agents and Analytic Components
See [Agent Development Guide](Agents/agent_dev_guide.md)

### The ASIST CORE system diagram:
 ![ASIST system diagram](docs/asist_adminless_testbed_core_architecture.png "ASIST Adminless Core Testbed Diagram")

 ### The ASIST ENVIRONMENT system diagram:
 ![ASIST system diagram](docs/asist_adminless_testbed_environment.png "ASIST Adminless Testbed Environment Diagram")


