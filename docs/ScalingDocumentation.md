# Scaling Infrastructure Documentation

## Google Cloud information

Instance List: https://console.cloud.google.com/compute/instances?project=asist-2130

### SSO Instance

The admin/sso instance is responsible for the site login, waiting room, and the external monitoring service.

Internal IP: 10.150.0.29
External IP: 34.145.184.45 (socialai.games)

Instance name: asist2-east-testbed-login-server

Logger: https://www.socialai.games/Logger/
ExternalMonitoringService: https://www.socialai.games/ExternalMonitoringService/

### Minecraft Instances

The individual instances which teams of players are redirected to are contained in an instance group and run their own copies of the Minecraft server. If this is the first time a new instance image file is used after it is CI built, the google staging process during VM creation takes awhile. This is visible in the instance list.

Internal IP: varies, assigned by google
External IP: varies, assigned by google

Instance name: asist-instance-group-xxxxx

Logger: https://internal-or-externalip/Logger

#### Instance Template

Instance template list: https://console.cloud.google.com/compute/instanceTemplates/list?project=asist-2130

- **Full 32 core AC/ASI instance template:** asist2-east-testbed-cluster-external1-atlas-2
- **Light weight testing template (no AC/ASI):**  ScalingMetric__InstanceTemplate=asist2-east-testbed-cluster-external1

The docker-compose of the SSO Instance (docker-compose.sso.adminless.yml) has an environment variable in the scalingmetricservice which selects which instance template to use when spawning Minecraft instances

```
- ScalingMetric__InstanceTemplate=asist2-east-testbed-cluster-external1-atlas-2
```

The instance templates have startup-script as custom metadata which determine the launch settings to use. For example:

```
#!/bin/bash
cd /ASIST/adminless-testbed/Local
pwd

export ADMIN_STACK_IP=socialai.games
export ADMIN_STACK_PORT=443

export DB_API_TOKEN=...
export DB_API_ISSUER_KEY=...
export ADMIN_ID=admin
export ADMIN_PWD=...
export PGADMIN_DEFAULT_EMAIL=support@socialai.games
export PGADMIN_DEFAULT_PASSWORD=...
export POSTGRES_PASSWORD=...
export POSTGRES_USER=docker
export POSTGRES_DB=userdb
export DB_CONNECTION_PWD=...
export DB_CONNECTION_USER=docker
export DB_CONNECTION_HOST=postgres-db
export DB_CONNECTION_PORT=5432
export DB_CONNECTION_DATABASE=userdb
export EMAIL_PWD=...
export SSO_SECRET_KEY=...
export GCLOUD_KEYFILE=...
export GCLOUD_PROJECT_ID=asist-2130
export GCS_BACKUP_BUCKET=gs://study4_db_backups.aptima.com

git config --system --add safe.directory /ASIST/adminless-testbed  >>out_git.txt 2>&1
sleep 20
sh ./testbed_instance_up_adminless.sh -arv >>out.txt 2>&1
```

This script is located in the instance template settings on the Google cloud under Advanced Options -> Management -> Automation -> Startup script.

Observe that /ASIST/adminless-testbed/Local/out.txt on an instance will contain the log of an instance starting if you need to check it.

#### Building instance image

The CI build for creating the image used for Minecraft instances is a manual CI build which can be started with the small play icon here: https://gitlab.asist.aptima.com/asist/adminless-testbed/-/pipelines. The SSO instance which is the drive that is used to create the image is currently on the *scaling3* branch

The CI steps do the following:
- turn on runner if not already (which is the SSO instance at 10.150.0.29/socialai.games)
- pull the current branch (scaling3), run ./testbed_build_adminless.sh
- shut down VM for imaging, create image, bring SSO instance back up

Image file: https://console.cloud.google.com/compute/images?tab=images&project=asist-2130

asist2-east-testbed-instance-updated in the asist2-east-testbed-instance family

Newly creating Minecraft instances always use the most recent "asist2-east-testbed-instance-updated" image.

## Starting SSO Instance / socialai.games

- ssh yourusername@10.150.0.29
- sudo bash
- cd /ASIST/adminless-testbed/Local/
- edit docker-compose.sso.adminless.yml as needed to specify the high or low instance template you want to use
- edit WaitingRoom/WaitingRoomConfig.json to have 3 or one person teams as desired
- ./testbed_sso_up_adminless.sh
- Navigate to https://www.socialai.games/ (or https://www.socialai.games/SPSO/Login) and direct players to it.
- watch
    - instance list for new instances: https://console.cloud.google.com/compute/instances?project=asist-2130
    - instance group for these Minecraft instances to gather once spawned: https://console.cloud.google.com/compute/instanceGroups/details/us-east4-c/asist-instance-group?project=asist-2130
    - External Monitoring Service: https://www.socialai.games/ExternalMonitoringService/
    - Logger: https://www.socialai.games/Logger/container/
      - waiting room has logs about scaling metric (goes by 10)
      - scalingmetricservice brings up and down Minecraft instances
    - Metric value is also published to graph at
      - https://console.cloud.google.com/monitoring/metrics-explorer;duration=PT30M?pageState=%7B%22xyChart%22:%7B%22constantLines%22:%5B%5D,%22dataSets%22:%5B%7B%22plotType%22:%22LINE%22,%22targetAxis%22:%22Y1%22,%22timeSeriesFilter%22:%7B%22aggregations%22:%5B%7B%22crossSeriesReducer%22:%22REDUCE_MEAN%22,%22groupByFields%22:%5B%5D,%22perSeriesAligner%22:%22ALIGN_MEAN%22%7D%5D,%22apiSource%22:%22DEFAULT_CLOUD%22,%22crossSeriesReducer%22:%22REDUCE_MEAN%22,%22filter%22:%22metric.type%3D%5C%22custom.googleapis.com%2Fasist-2130%2Fscaling_metric%5C%22%20resource.type%3D%5C%22global%5C%22%22,%22groupByFields%22:%5B%5D,%22minAlignmentPeriod%22:%2260s%22,%22perSeriesAligner%22:%22ALIGN_MEAN%22%7D%7D%5D,%22options%22:%7B%22mode%22:%22COLOR%22%7D,%22y1Axis%22:%7B%22label%22:%22%22,%22scale%22:%22LINEAR%22%7D%7D%7D&project=asist-2130&pli=1
      - Google cloud does not control scaling, it is done manually, but the metric can be interesting to observe in graph form
    - Waiting room redirect has form of: `https://socialai.games/${this.websocketService.instanceInternalIP}/ClientMap/?player=${this.websocketService.assigned_name}&externalIP=${this.websocketService.instanceIP}&internalIP=${this.websocketService.instanceInternalIP}`
      - Example: https://socialai.games/10.150.0.56/ClientMap/?player=Scouter_B&externalIP=35.245.190.128&internalIP=10.150.0.56
      - SSO nginx file has a reverse proxy to the internal address of the Minecraft instance for its Client Map.
    - Minecraft instance Logger: https://instanceip/Logger/

## Stopping SSO Instance
- on 10.150.0.29 in /ASIST/adminless-testbed/Local/
  - ./testbed_down_adminless.sh
- remove any remaining Minecraft instances in instance group at
  -  https://console.cloud.google.com/compute/instanceGroups/details/us-east4-c/asist-instance-group?project=asist-2130

## Data Export location on Google Cloud

https://console.cloud.google.com/storage/browser/testbed-auto-exports;tab=objects?forceOnBucketsSortingFiltering=false&project=asist-2130&prefix=&forceOnObjectsSortingFiltering=true&pageState=(%22StorageObjectListTable%22:(%22f%22:%22%255B%255D%22,%22s%22:%5B(%22i%22:%22objectListDisplayFields%2FtimeCreated%22,%22s%22:%221%22),(%22i%22:%22displayName%22,%22s%22:%220%22)%5D))&pli=1

