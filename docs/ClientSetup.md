Minecraft Client installation instructions
==========================================

When it comes to installing a Minecraft client, you have three options - a free
one from Microsoft/Malmo, a free one from Forge as part of the 1.11.2 modding package, or a paid one from Microsoft(Mojang).


Free Version from Microsoft/Malmo
---------------------------------

### Requirements

- Minecraft Client version 1.11.2
- Minecraft Forge Mod
- Minecraft Malmo Mod or the ASIST mod
- All relevant dependencies for the above

### Windows instructions

- Download the Windows (or whichever binary is appropriate for your OS)
  pre-built binary from https://github.com/microsoft/malmo/releases
- Current experiment version is
  Malmo-0.37.0-Windows-64bit_withBoost_Python3.6.zip. This folder contains
  references to all relevant dependencies via a Powershell script already, so
  you do not have to download them yourself.
- Run the install script as ADMIN in /scripts directory of the extracted
  folder. This should download most of the dependencies required. It may take
  some time depending on your internet connection and machine specs. If you
  have trouble, visit https://github.com/microsoft/malmo.
- Run the launchClient.bat script in ./Minecraft one time. This will build
  Minecraft for the first time, along with its associated folders.
- You must have the same mods installed on the Minecraft client as installed in
  the Minecraft server in the testbed. If running the Malmo or ASIST mod,
  navigate to the Minecraft/run/mods folder, copy both the mod .jar files
  there. If you are running the Asist Mod, copy
  `Local/MinecraftServer/data/mods/asistmod-1.0.jar` and
  `Local/MinecraftServer/data/mods/asistmod-1.0-sources.jar` from this
  repository to the `./Minecraft/run/mods folder`. In the Minecraft client you
  can have both installed, but on the server (in the testbed) you must run one
  mod or the other, you cannot run them together (but the testbed will take
  care of that for you).
- Launch minecraft with the `launchClient.bat` script in ./Minecraft``
- Navigate to Multiplayer-->Direct Connect, then enter the IP and port number
  of the experiment and connect.

Paid version - Standard Mojang/Minecraft Account
------------------------------------------------

Requires Minecraft version 1.11.2 with a valid Mojang account

1. Install the Minecraft Launcher (https://www.minecraft.net/en-us/download/)
2. Install Minecraft version with Forge
    - http://files.minecraftforge.net/maven/net/minecraftforge/forge/index_1.11.2.html
    - Specific download link for Windows:
      https://adfoc.us/serve/sitelinks/?id=271228&url=https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.11.2-13.20.1.2588/forge-1.11.2-13.20.1.2588-installer-win.exe
3. Create a MinecraftHome directory, and a `mods` directory within the
   `MinecraftHome` directory. (**Note**: For macOS, you can run the following
   command instead: `mkdir -p ~/Library/Application\ Support/minecraft/mods`).
5. Copy the Asist Mod from the testbed directory `Local/data/mods/asistmod-<version>.jar` into the `mods` directory created above.
6. Launch the Minecraft Launcher
7. Go to the 'Installations' tab
    - Create an installation with the 1.11.2 Forge version. It should be at the
      bottom of the list.
    - Set the game installation directory to the MinecraftHome directory
      created above
8. Launch the installation from the 'Play' tab of the Minecraft Launcher.
9. Join the game using the multiplayer direct connection with an IP address and
port number.
