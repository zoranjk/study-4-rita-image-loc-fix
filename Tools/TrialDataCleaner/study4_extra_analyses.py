
import json, sys, traceback

def avgTimeFromFirstVoteToShop(lines)->float:
    firstVoteTimestamp = -1
    shopTransitionTimestamp = -1
    transitionsToShop = 0
    waitTimes = []
    for i in range(len(lines)):
        json_line = json.loads(lines[i])
            
        if 'msg' in json_line:

            msg = json_line['msg']
            data = json_line['data']

            if 'sub_type' in msg:
                sub_type = msg['sub_type']
                #print(sub_type)
                if sub_type == 'Event:CommunicationEnvironment':
                    if 'voted to go to the Shop!' in data['message']:
                        if firstVoteTimestamp == -1:
                            firstVoteTimestamp = data['elapsed_milliseconds']
                if sub_type == 'Event:MissionStageTransition' and data['mission_stage']=='SHOP_STAGE':
                    if data["transitionsToShop"]>1:                        
                        shopTransitionTimestamp = data['elapsed_milliseconds']
                        if shopTransitionTimestamp > 0:
                            print ( str(shopTransitionTimestamp) + "-" + str(firstVoteTimestamp))
                            waitTimes.append( shopTransitionTimestamp - firstVoteTimestamp)
                            shopTransitionTimestamp = -1
                            firstVoteTimestamp = -1
                        
                    else:
                        transitionsToShop = data["transitionsToShop"]
    
    print ( str(waitTimes))

    sum=0
    for num in waitTimes:
            sum+=num
    return sum/len(waitTimes)


def threePhaseBombsDefusedBySinglePlayer(lines)->dict:
    record = dict()
    for i in range(len(lines)):
        json_line = json.loads(lines[i])
            
        if 'msg' in json_line:

            msg = json_line['msg']
            data = json_line['data']

            if 'sub_type' in msg:
                sub_type = msg['sub_type']
                #print(sub_type)
                if sub_type == 'Event:ObjectStateChange':
                    id = data['id']                   
                    if id not in record:                        
                        record[id] = dict()
                    if 'currAttributes' in data:                        
                        attr = data['currAttributes']
                        if 'sequence' in attr:
                            seq = attr['sequence']
                            if 'sequence_index' in attr:
                                if attr['sequence_index'] == "0":                                
                                    if len( record[id].keys() ) == 0:
                                        for phase in seq:
                                            record[id][phase] = ''
                            if attr['outcome'] == "DEFUSED_DISPOSER":
                                for phase in seq:
                                    if record[id][phase] == '':
                                        record[id][phase] = data['triggering_entity']
                                print('BOMB DISPOSER ! ' + id)
                            if attr['outcome'] == 'PERTURBATION_FIRE_TRIGGER':                            
                                print('PERTURBATION ! ' + id)

                    if 'changedAttributes' in data:
                        changedAttributes = data['changedAttributes']
                        if 'sequence' in changedAttributes:
                            seq = changedAttributes['sequence'][0]
                            if len( record[id].keys() ) == 0:
                                for phase in seq:
                                    record[id][phase] = ''
                            record[id][ seq[0] ] = data['triggering_entity']
 

    print( " List of All Bomb Disposals\n")
    numSingleThreePhase = 0
    players = dict()
    for id in record:
        pids=set() 
        noNulls = True       
        for phase in record[id].keys():
            if len( record[id][phase] ) > 0:            
                pids.add(record[id][phase])
            else:
                noNulls = False
                break
        if (noNulls) and (len(pids) == 1) and (len( record[id].keys() )  == 3):
            numSingleThreePhase+=1
            pid = list(record[id].values())[0]
            if pid not in players:
                players[ pid ] = 0
            players[ pid ]+=1
            

        
        print( str(id) + " : " + str(record[id]) + " : " + str(pids) + " : " + str(len(pids)) )   
   
    
    return players

def soloDisposalsOnly(lines)->list:
    record = dict()
    for i in range(len(lines)):
        json_line = json.loads(lines[i])
            
        if 'msg' in json_line:

            msg = json_line['msg']
            data = json_line['data']

            if 'sub_type' in msg:
                sub_type = msg['sub_type']
                #print(sub_type)
                if sub_type == 'Event:ObjectStateChange':
                    id = data['id']                   
                    if id not in record:                        
                        record[id] = dict()
                    if 'currAttributes' in data:
                        attr = data['currAttributes']
                        if 'sequence' in attr:
                            seq = attr['sequence']
                            if 'sequence_index' in attr:
                                if attr['sequence_index'] == "0":                                    
                                    if len( record[id].keys() ) == 0:
                                        for phase in seq:
                                            record[id][phase] = ''
                            if attr['outcome'] == "DEFUSED_DISPOSER":
                                for phase in seq:
                                    if record[id][phase] == '':
                                        record[id][phase] = data['triggering_entity']
                                print('BOMB DISPOSER ! ' + id)
                            if attr['outcome'] == 'PERTURBATION_FIRE_TRIGGER':                            
                                print('PERTURBATION ! ' + id)

                    if 'changedAttributes' in data:
                        changedAttributes = data['changedAttributes']
                        if 'sequence' in changedAttributes:
                            seq = changedAttributes['sequence'][0]
                            if len( record[id].keys() ) == 0:
                                for phase in seq:
                                    record[id][phase] = ''
                            record[id][ seq[0] ] = data['triggering_entity']
    
    #print( " List of All Bomb Disposals\n")
    
    players = set()
    nonSoloDisposers = set()
    for id in record:
        pids=set() 
        noNulls = True       
        for phase in record[id].keys():
            pid = record[id][phase]
            if len( pid ) > 0:            
                pids.add(pid)
                players.add(pid)
            else:
                noNulls = False
                break
        if (noNulls):
            if len(list(record[id].values())) > 0:
                pid = list(record[id].values())[0]
                if (len(pids) > 1) :
                    #print("Non Solo Disposal : " + pid)
                    nonSoloDisposers.update(pids)
                    

        
        #print( str(id) + " : " + str(record[id]) + " : " + str(pids) + " : " + str(len(pids)) )
        #print( str(players))
    print ("Non SOLO disposers : " + str(nonSoloDisposers))
    print ("SOLO disposers : " + str(list(players.difference(nonSoloDisposers))) )
    return list(players.difference(nonSoloDisposers))


        


if __name__ == "__main__":

    print('------------------------------------------------------------------------')
    print('Supplied Arguments : ')
    print(sys.argv)
    print(len(sys.argv))
    print('---------------------------------')


    incoming_file = sys.argv[1]     

    f = open(incoming_file, 'r',encoding='utf-8')

    lines = f.readlines()

    print ( ' --------------------------------------------------------------------------- ' )

    print( " Number of 3 Phase Bombs Defused by Single Player and Which Player\n")    
    
    print ( str(threePhaseBombsDefusedBySinglePlayer(lines)) )

    print ( ' --------------------------------------------------------------------------- ' )

    print( " Players who Only ever disposed of bombs alone\n")

    print ( str(soloDisposalsOnly(lines)) )

    print ( ' --------------------------------------------------------------------------- ' )

    print( " Average Time from First Vote to Shop transition\n")

    print ( str(avgTimeFromFirstVoteToShop(lines)) )

    

    f.close()

   

    


 


 
