#!/usr/bin/env python3
"""
Module Docstring
"""

__author__ = "Aptima, Inc."
__version__ = "1.0.0"


import argparse
import configparser
import datetime
import json
import os
import random
import sys

import psycopg2
import requests
import urllib3
from logzero import logger
from psycopg2 import OperationalError
from psycopg2.extras import RealDictCursor
from pydantic import ValidationError

from trialModel import Trials
from userModel import Users

urllib3.disable_warnings()


def process_args(cmd_args, config_args):
    try:
        if config_args.has_section('postgres') is False:
            config_args.add_section('postgres')
        if config_args.has_section('http') is False:
            config_args.add_section('http')

        if 'direct' in cmd_args and cmd_args.direct is not None:
            config_args['postgres']['direct'] = str(cmd_args.direct)
        else:
            config_args['postgres']['direct'] = 'False'

        if 'start_date' in cmd_args and 'end_date' not in cmd_args:
            sys.exit('\nTerminating script due to a defined start time with an undefined end time.')

        if config_args.has_section('range') is False:
            config_args.add_section('range')

        previous_monday = datetime.date.today() - datetime.timedelta(
            days=datetime.date.today().weekday()) + datetime.timedelta(days=0, weeks=-1)
        following_sunday = previous_monday + datetime.timedelta(days=6)
        if 'start_date' in cmd_args and cmd_args.start_date is not None:
            config_args['range']['start-date'] = str(cmd_args.start_date + ' 00:0:1')
        else:
            config_args['range']['start-date'] = str(
                datetime.datetime.combine(previous_monday, datetime.time(hour=0, minute=0, second=1)))

        if 'end_date' in cmd_args and cmd_args.end_date is not None:
            config_args['range']['end-date'] = str(cmd_args.end_date + ' 23:59:59')
        else:
            config_args['range']['end-date'] = str(
                datetime.datetime.combine(following_sunday, datetime.time(hour=23, minute=59, second=59)))

        if 'pg_host' in cmd_args and cmd_args.pg_host is not None:
            config_args['postgres']['host'] = cmd_args.pg_host
        if 'pg_port' in cmd_args and cmd_args.pg_port is not None:
            config_args['postgres']['port'] = cmd_args.pg_port
        if 'pg_database' in cmd_args and cmd_args.pg_database is not None:
            config_args['postgres']['database'] = cmd_args.pg_database
        if 'pg_user' in cmd_args and cmd_args.pg_user is not None:
            config_args['postgres']['user'] = cmd_args.pg_user
        if 'pg_password' in cmd_args and cmd_args.pg_password is not None:
            config_args['postgres']['password'] = cmd_args.pg_password

        if 'http_token' in cmd_args and cmd_args.http_token is not None:
            config_args['http']['token'] = cmd_args.http_token
        if 'http_url_trials' in cmd_args and cmd_args.http_url_trials is not None:
            config_args['http']['url-trials'] = cmd_args.http_url_trials
        if 'http_url_users' in cmd_args and cmd_args.http_url_users is not None:
            config_args['http']['url-users'] = cmd_args.http_url_users

        connect(config_args)

    except Exception as ex:
        logger.error('Could not process invalid configuration or command line arguments!')
        logger.error(ex)


def connect(config_args):
    direct = config_args['postgres'].getboolean('direct')
    if direct is True:
        logger.info('Direct connection enabled')

        for c in ['host', 'port', 'database', 'user', 'password']:
            if config.has_option('postgres', c) is False:
                logger.error('"%s" is not defined in either config file or as an argument!', c)
                sys.exit('\nTerminating script due to undefined configuration variable.')

        host = config['postgres']['host']
        port = config['postgres']['port']
        database = config['postgres']['database']
        user = config['postgres']['user']
        password = config['postgres']['password']

        connection, cursor = None, None
        try:
            connection = psycopg2.connect(
                database=database, user=user, password=password, host=host, port=port
            )

            # Creating a cursor object using the cursor() method
            cursor = connection.cursor(cursor_factory=RealDictCursor)

            # Executing an MYSQL function using the execute() method
            cursor.execute('select version()')

            # Fetch a single row using fetchone() method.
            data = cursor.fetchone()
            logger.info('Connection established to: %s', json.dumps(data))

            cursor.execute(
                'SELECT "ParticipantId" as "participantId", "UserId" as "userId", "Password" as "password", "Email" as "email", '
                '"IsAdmin" as "isAdmin", "ParticipationCount" as "participationCount", "lastConsentTimestamp", "TeamsList" as "teamsList" from "Users"')
            result_users = cursor.fetchall()
            users = None
            try:
                users = Users(result_users)
            except ValidationError as e:
                logger.error(e.errors())

            cursor.execute(
                'SELECT "TrialId" as "trialId", "ExperimentId" as "experimentId", "TeamId" as "teamId", "Members" as "members", '
                '"ASICondition" as "asiCondition", "TeamScore" as "teamScore", '
                '"BombsTotal" as "bombsTotal", "TotalStoreTime" as "totalStoreTime", '
                '"TrialEndCondition" as "trialEndCondition", "StartTimestamp" as "startTimestamp", '
                '"ExperimentName" as "experimentName", "TrialName" as "trialName", "MissionVariant" as "missionVariant" '
                'from "Trials"')
            result_trials = cursor.fetchall()
            trials = None
            try:
                trials = Trials(result_trials)
            except ValidationError as e:
                logger.error(e.errors())

            if users is not None and trials is not None:
                selectWinners(users, trials, config_args)
            else:
                logger.error('Missing data for users or trials!')

        except OperationalError as ex:
            logger.error('Could not establish direct connection to Postgres database!')
            logger.error(ex)

        finally:
            # closing database connection.
            if connection:
                cursor.close()
                connection.close()
                logger.error("PostgreSQL connection is closed")
    else:
        logger.info('HTTPS connection enabled')
        for c in ['token', 'url-trials', 'url-users']:
            if config.has_option('http', c) is False:
                logger.error('"%s" is not defined in either config file or as an argument!', c)
                sys.exit('\nTerminating script due to undefined configuration variable.')

        url_users = config['http']['url-users']
        url_trials = config['http']['url-trials']
        token = config['http']['token']

        headers = {
            'Authorization': 'Bearer ' + token,
            'Content-Type': 'application/json'
        }

        try:
            response_users = requests.get(url_users, headers=headers, verify=False)
            if response_users.status_code != 200:
                logger.error('Error with: [%s] [%s: %s]', url_users, response_users.status_code, response_users.reason)
        except requests.exceptions.ConnectionError:
            logger.error('Connection to server refused for: [%s]', url_users)
            return
        try:
            users = Users(response_users.json())
        except ValidationError as e:
            logger.error(e.errors())
            return

        try:
            response_trials = requests.get(url_trials, headers=headers, verify=False)
            if response_trials.status_code != 200:
                logger.error('Error with: [%s] [%s: %s]', url_trials, response_trials.status_code, response_trials.reason)
        except requests.exceptions.ConnectionError:
            logger.error('Connection to server refused for: [%s]', url_trials)
            return
        try:
            trials = Trials(response_trials.json())
        except ValidationError as e:
            logger.error(e.errors())
            return

        selectWinners(users, trials, config_args)

def selectWinners(users, trials, config_args):
    start_date = datetime.datetime.strptime(config_args['range']['start-date'], '%Y-%m-%d %H:%M:%S')
    end_date = datetime.datetime.strptime(config_args['range']['end-date'], '%Y-%m-%d %H:%M:%S')
    logger.info('')
    logger.info('-------------------------------------------------------')
    logger.info('Start: [%s] End: [%s]', datetime.datetime.strftime(start_date, '%Y-%m-%d %H:%M:%S'),
                datetime.datetime.strftime(end_date, '%Y-%m-%d %H:%M:%S'))

    winners_list = [trial for trial in trials if isinstance(trial.startTimestamp,
                                                            datetime.datetime) and start_date <= trial.startTimestamp <= end_date]
    if len(winners_list) <= 0:
        logger.error('No potential winners found!')
        sys.exit('\nTerminating script due to a no potential winners found within the specified date range.')

    list.sort(winners_list, key=lambda t: t.teamScore)

    half = len(winners_list) // 2
    half_winners_list = winners_list[half:]  # winners_list[:half],

    positive_score = [trial for trial in half_winners_list if trial.teamScore > 0]

    winner = random.choice(positive_score)
    logger.info('')
    logger.info('Winning score:%s', winner.teamScore)
    logger.info('Winning participants: %s', winner.members)

    winning_participants = [user for user in users if user.participantId in winner.members]
    logger.info('')
    logger.info('=================================')
    logger.info('=  Winning participant details  =')
    logger.info('=================================')
    logger.info('Date of trial: %s', winner.startTimestamp)
    for count, user in enumerate(winning_participants):
        logger.info('Participant ID: [%s] User ID: [%s] Email: [%s] Admin: [%s] Participation: [%s]', user.participantId, user.userId, user.email, user.isAdmin, user.participationCount)


def read_default_config(config_default):
    logger.info('Looking for default configuration file.')
    if config_default is not None:
        if os.path.exists('config.ini'):
            logger.info('Found default configuration file: [%s].', 'config.ini')
            try:
                config.read('config.ini')
            except configparser.Error as ex:
                logger.error('config.ini is an invalid configuration file.')
                logger.error(ex)
                raise ex
        else:
            logger.info('Default configuration file not found.')
    else:
        logger.info('Default configuration file not found.')


if __name__ == "__main__":
    """ This is executed when run from the command line """
    parser = argparse.ArgumentParser(description='ASIST Team Prize script.')

    parser.add_argument('-f', '--file', help='configuration file location to use instead of the default location', action='store', dest='file')

    parser.add_argument('-d', '--direct', help='enabling direct mode flag will try and connect directly to postgres', action='store_true', dest='direct', default=False)

    parser.add_argument('--start-date', help='override default start date YYYY-MM-DD', action='store', dest='start_date')
    parser.add_argument('--end-date', help='override default end date YYYY-MM-DD', action='store', dest='end_date')

    parser.add_argument('--pg-host', help='postgres host address', action='store', dest='pg_host')
    parser.add_argument('--pg-port', help='postgres connection port number', action='store', dest='pg_port')
    parser.add_argument('--pg-database', help='postgres database name', action='store', dest='pg_database')
    parser.add_argument('--pg-user', help='postgres user name to authenticate', action='store', dest='pg_user')
    parser.add_argument('--pg-password', help='postgres password to authenticate', action='store', dest='pg_password')

    parser.add_argument('--http-url-trials', help='url to GET user data', action='store', dest='http_url_trials')
    parser.add_argument('--http-url-users', help='url to GET trial data', action='store', dest='http_url_users')
    parser.add_argument('--http-token', help='authentication token', action='store', dest='http_token')

    # Optional verbosity counter (eg. -v, -vv, -vvv, etc.)
    # parser.add_argument(
    #     '-v',
    #     '--verbose',
    #     action='count',
    #     default=0,
    #     help='Verbosity (-v, -vv, etc)')

    # Specify output of "--version"
    parser.add_argument(
        '--version',
        action='version',
        version='%(prog)s (version {version})'.format(version=__version__))

    args = parser.parse_args()

    config = configparser.ConfigParser()
    if 'file' in args and args.file is not None:
        try:
            with open(args.file) as f:
                logger.info('Looking for configuration file at %s.', args.file)
                config.read_file(f)
        except IOError:
            logger.error('%s is an invalid configuration file!', args.file)
            read_default_config(config)
    else:
        read_default_config(config)

    process_args(args, config)
