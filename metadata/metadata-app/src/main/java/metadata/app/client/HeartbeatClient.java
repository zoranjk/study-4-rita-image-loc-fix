package metadata.app.client;

import io.micronaut.http.annotation.Get;
import io.micronaut.http.client.annotation.Client;

@Client("/") 
public interface HeartbeatClient {

    @Get("/health") 
    String health();
}
