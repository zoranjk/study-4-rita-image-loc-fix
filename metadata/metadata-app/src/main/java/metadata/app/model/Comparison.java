package metadata.app.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Comparison {

	private String operator;
	private int value;
	
	@JsonCreator
	public Comparison(
			@JsonProperty("operator") String operator,
			@JsonProperty("value") int value
			) {
		this.operator = operator;
        this.value = value;       
    }

	@JsonProperty("operator")
	public String getOperator() {
		return operator;
	}
	@JsonProperty("operator")
	public void setOperator(String operator) {
		this.operator = operator;
	}
	@JsonProperty("value")
	public int getValue() {
		return value;
	}
	@JsonProperty("value")
	public void setValue(int value) {
		this.value = value;
	}
	
}