package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonMerge;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Map;

public class SummaryBomb {
    @JsonProperty(value="BOMBS_EXPLODED")
    @JsonMerge
    private BombDetail bombsExploded;
    @JsonProperty(value="BOMBS_DEFUSED")
    @JsonMerge
    private BombDetail bombsDefused;
    @JsonProperty(value="PHASES_DEFUSED")
    @JsonMerge
    private BombDetailRGB phasesDefused;

    public SummaryBomb() {
        this.bombsExploded = new BombDetail();
        this.bombsDefused = new BombDetail();
        this.phasesDefused = new BombDetailRGB();
    }

    public BombDetail getBombsExploded() {
        return bombsExploded;
    }

    public void setBombsExploded(BombDetail bombsExploded) {
        this.bombsExploded = bombsExploded;
    }

    public BombDetail getBombsDefused() {
        return bombsDefused;
    }

    public void setBombsDefused(BombDetail bombsDefused) {
        this.bombsDefused = bombsDefused;
    }

    public BombDetailRGB getPhasesDefused() {
        return phasesDefused;
    }

    public void setPhasesDefused(BombDetailRGB phasesDefused) {
        this.phasesDefused = phasesDefused;
    }

    public static class BombDetail {
        @JsonProperty(value="CHAINED")
        private int chanined;
        @JsonProperty(value="FIRE")
        private int fire;
        @JsonProperty(value="STANDARD")
        private int standard;

        public BombDetail() {
            this.chanined = 0;
            this.fire = 0;
            this.standard = 0;
        }

        public int getChanined() {
            return chanined;
        }

        public void setChanined(int chanined) {
            this.chanined = chanined;
        }

        public int getFire() {
            return fire;
        }

        public void setFire(int fire) {
            this.fire = fire;
        }

        public int getStandard() {
            return standard;
        }

        public void setStandard(int standard) {
            this.standard = standard;
        }
    }

    public static class BombDetailRGB {
        @JsonProperty(value="CHAINED")
        @JsonMerge
        private BombRGB chained;
        @JsonProperty(value="FIRE")
        @JsonMerge
        private BombRGB fire;
        @JsonProperty(value="STANDARD")
        @JsonMerge
        private BombRGB standard;

        public BombDetailRGB() {
            this.chained = new BombRGB();
            this.fire = new BombRGB();
            this.standard = new BombRGB();
        }

        public BombRGB getChained() {
            return chained;
        }

        public void setChained(BombRGB chained) {
            this.chained = chained;
        }

        public BombRGB getFire() {
            return fire;
        }

        public void setFire(BombRGB fire) {
            this.fire = fire;
        }

        public BombRGB getStandard() {
            return standard;
        }

        public void setStandard(BombRGB standard) {
            this.standard = standard;
        }
    }

    public static class BombRGB {
        @JsonProperty(value = "R")
        private int r;
        @JsonProperty(value = "G")
        private int g;
        @JsonProperty(value = "B")
        private int b;

        public BombRGB() {
            this.r = 0;
            this.g = 0;
            this.b = 0;
        }

        public int getR() {
            return r;
        }

        public void setR(int r) {
            this.r = r;
        }

        public int getG() {
            return g;
        }

        public void setG(int g) {
            this.g = g;
        }

        public int getB() {
            return b;
        }

        public void setB(int b) {
            this.b = b;
        }
    }
}
