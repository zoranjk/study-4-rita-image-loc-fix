package metadata.app.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FirstLookFile {

	private String source;
	private int totalDocuments;
	private List<MessageCount> messageCounts;
	
	@JsonCreator
	public FirstLookFile(
			@JsonProperty("source") String source,
			@JsonProperty("total_documents") int totalDocuments,
			@JsonProperty("message_counts") List<MessageCount> messageCounts
			) {
		this.source = source;
        this.totalDocuments = totalDocuments;
        this.messageCounts = messageCounts;   
    }

	@JsonProperty("source")
	public String getSource() {
		return source;
	}
	@JsonProperty("source")
	public void setSource(String source) {
		this.source = source;
	}
	@JsonProperty("total_documents")
	public int getTotalDocuments() {
		return totalDocuments;
	}
	@JsonProperty("total_documents")
	public void setTotalDocuments(int totalDocuments) {
		this.totalDocuments = totalDocuments;
	}
	@JsonProperty("message_counts")
	public List<MessageCount> getComparison() {
		return messageCounts;
	}
	@JsonProperty("message_counts")
	public void setComparison(List<MessageCount> messageCounts) {
		this.messageCounts = messageCounts;
	}	
}