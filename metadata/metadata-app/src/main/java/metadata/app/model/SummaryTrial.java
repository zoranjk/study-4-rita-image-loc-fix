package metadata.app.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonMerge;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@JsonFormat(with = JsonFormat.Feature.ACCEPT_CASE_INSENSITIVE_PROPERTIES)
public class SummaryTrial {
    @JsonProperty(value="StartTimestamp")
    private String startTimestamp;
    @JsonProperty(value="NumStoreVisits")
    private int numStoreVisits;
    @JsonProperty(value="TotalStoreTime")
    private int totalStoreTime;
    @JsonProperty(value="ASICondition")
    private String asiCondition;
    @JsonProperty(value="ExperimentName")
    private String experimentName;
    @JsonProperty(value="TeammatesRescued")
    private Map<String, Integer> teammatesRescued;
    @JsonProperty(value="LastActiveMissionTime")
    private String lastActiveMissionTime;
    @JsonProperty(value="TimesFrozen")
    @JsonMerge
    private Map<String, Integer> timesFrozen;
    @JsonProperty(value="TextChatsSent")
    @JsonMerge
    private Map<String, Integer> textChatsSent;
    @JsonProperty(value="BombSummaryPlayer")
    @JsonMerge
    private Map<String, SummaryBomb> bombSummaryPlayer;
    @JsonProperty(value="TeamScore")
    private int teamScore;
    @JsonProperty(value="MissionEndCondition")
    private String missionEndCondition;
    @JsonProperty(value="BombsExploded")
    @JsonMerge
    private ExplodedBomb bombsExploded;
    @JsonProperty(value="FlagsPlaced")
    @JsonMerge
    private Map<String, Integer> flagsPlaced;
    @JsonProperty(value="TrialEndCondition")
    private String trialEndCondition;
    @JsonProperty(value="TrialName")
    private String trialName;
    @JsonProperty(value="MissionVariant")
    private String missionVariant;
    @JsonProperty(value="FiresExtinguished")
    @JsonMerge
    private Map<String, Integer> firesExtinguished;
    @JsonProperty(value="DamageTaken")
    @JsonMerge
    private Map<String, Integer> damageTaken;
    @JsonProperty(value="NumCompletePostSurveys")
    @JsonMerge
    private Map<String, Integer> numCompletePostSurveys;
    @JsonProperty(value="TeamId")
    private String teamId;
    @JsonProperty(value="BombBeaconsPlaced")
    @JsonMerge
    private Map<String, Integer> bombBeaconsPlaced;
    @JsonProperty(value="BudgetExpended")
    @JsonMerge
    private Map<String, Integer> budgetExpended;
    @JsonProperty(value="NumFieldVisits")
    private int numFieldVisits;
    @JsonProperty(value="ExperimentId")
    private String experimentId;
    @JsonProperty(value="TrialId")
    private String trialId;
    @JsonProperty(value="CommBeaconsPlaced")
    @JsonMerge
    private Map<String, Integer> commBeaconsPlaced;
    @JsonProperty(value="BombsTotal")
    private int bombsTotal;
    @JsonProperty(value="Members")
    private List<String> members;

    public SummaryTrial() { }

    public SummaryTrial(List<String> members) {
        if (members != null) {
            members.sort(null);
            this.startTimestamp = "";
            this.numStoreVisits = 0;
            this.totalStoreTime = 0;
            this.asiCondition = "";
            this.experimentName = "";
            this.lastActiveMissionTime = "";
            this.teamScore = 0;
            this.missionEndCondition = "";
            this.trialEndCondition = "";
            this.trialName = "";
            this.missionVariant = "";
            this.teamId = "";
            this.numFieldVisits = 0;
            this.experimentId = "";
            this.trialId = "";
            this.bombsTotal = 0;
            this.members = members;
            members.forEach(member -> {
                this.teammatesRescued = new HashMap<>();
                this.teammatesRescued.put(member, 0);
                this.teammatesRescued.put("team", 0);
                this.timesFrozen = new HashMap<>();
                this.timesFrozen.put(member, 0);
                this.timesFrozen.put("team", 0);
                this.textChatsSent = new HashMap<>();
                this.textChatsSent.put(member, 0);
                this.textChatsSent.put("team", 0);
                this.bombSummaryPlayer = new HashMap<>();
                this.bombSummaryPlayer.put(member, new SummaryBomb());
                this.bombsExploded = new ExplodedBomb();
                this.flagsPlaced = new HashMap<>();
                this.flagsPlaced.put(member, 0);
                this.flagsPlaced.put("team", 0);
                this.firesExtinguished = new HashMap<>();
                this.firesExtinguished.put(member, 0);
                this.firesExtinguished.put("team", 0);
                this.damageTaken = new HashMap<>();
                this.damageTaken.put(member, 0);
                this.damageTaken.put("team", 0);
                this.numCompletePostSurveys = new HashMap<>();
                this.numCompletePostSurveys.put(member, 0);
                this.numCompletePostSurveys.put("team", 0);
                this.bombBeaconsPlaced = new HashMap<>();
                this.bombBeaconsPlaced.put(member, 0);
                this.bombBeaconsPlaced.put("team", 0);
                this.budgetExpended = new HashMap<>();
                this.budgetExpended.put(member, 0);
                this.budgetExpended.put("team", 0);
                this.commBeaconsPlaced = new HashMap<>();
                this.commBeaconsPlaced.put(member, 0);
                this.commBeaconsPlaced.put("team", 0);
            });
        }
    }

    public String getStartTimestamp() {
        return startTimestamp;
    }

    public void setStartTimestamp(String startTimestamp) {
        this.startTimestamp = startTimestamp;
    }

    public int getNumStoreVisits() {
        return numStoreVisits;
    }

    public void setNumStoreVisits(int numStoreVisits) {
        this.numStoreVisits = numStoreVisits;
    }

    public int getTotalStoreTime() {
        return totalStoreTime;
    }

    public void setTotalStoreTime(int totalStoreTime) {
        this.totalStoreTime = totalStoreTime;
    }

    public String getAsiCondition() {
        return asiCondition;
    }

    public void setAsiCondition(String asiCondition) {
        this.asiCondition = asiCondition;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public void setExperimentName(String experimentName) {
        this.experimentName = experimentName;
    }

    public Map<String, Integer> getTeammatesRescued() {
        return teammatesRescued;
    }

    public void setTeammatesRescued(Map<String, Integer> teammatesRescued) {
        this.teammatesRescued = teammatesRescued;
    }

    public String getLastActiveMissionTime() {
        return lastActiveMissionTime;
    }

    public void setLastActiveMissionTime(String lastActiveMissionTime) {
        this.lastActiveMissionTime = lastActiveMissionTime;
    }

    public Map<String, Integer> getTimesFrozen() {
        return timesFrozen;
    }

    public void setTimesFrozen(Map<String, Integer> timesFrozen) {
        this.timesFrozen = timesFrozen;
    }

    public Map<String, Integer> getTextChatsSent() {
        return textChatsSent;
    }

    public void setTextChatsSent(Map<String, Integer> textChatsSent) {
        this.textChatsSent = textChatsSent;
    }

    public Map<String, SummaryBomb> getBombSummaryPlayer() {
        return bombSummaryPlayer;
    }

    public void setBombSummaryPlayer(Map<String, SummaryBomb> bombSummaryPlayer) {
        this.bombSummaryPlayer = bombSummaryPlayer;
    }

    public int getTeamScore() {
        return teamScore;
    }

    public void setTeamScore(int teamScore) {
        this.teamScore = teamScore;
    }

    public String getMissionEndCondition() {
        return missionEndCondition;
    }

    public void setMissionEndCondition(String missionEndCondition) {
        this.missionEndCondition = missionEndCondition;
    }

    public ExplodedBomb getBombsExploded() {
        return bombsExploded;
    }

    public void setBombsExploded(ExplodedBomb bombsExploded) {
        this.bombsExploded = bombsExploded;
    }

    public Map<String, Integer> getFlagsPlaced() {
        return flagsPlaced;
    }

    public void setFlagsPlaced(Map<String, Integer> flagsPlaced) {
        this.flagsPlaced = flagsPlaced;
    }

    public String getTrialEndCondition() {
        return trialEndCondition;
    }

    public void setTrialEndCondition(String trialEndCondition) {
        this.trialEndCondition = trialEndCondition;
    }

    public String getTrialName() {
        return trialName;
    }

    public void setTrialName(String trialName) {
        this.trialName = trialName;
    }

    public String getMissionVariant() {
        return missionVariant;
    }

    public void setMissionVariant(String missionVariant) {
        this.missionVariant = missionVariant;
    }

    public Map<String, Integer> getFiresExtinguished() {
        return firesExtinguished;
    }

    public void setFiresExtinguished(Map<String, Integer> firesExtinguished) {
        this.firesExtinguished = firesExtinguished;
    }

    public Map<String, Integer> getDamageTaken() {
        return damageTaken;
    }

    public void setDamageTaken(Map<String, Integer> damageTaken) {
        this.damageTaken = damageTaken;
    }

    public Map<String, Integer> getNumCompletePostSurveys() {
        return numCompletePostSurveys;
    }

    public void setNumCompletePostSurveys(Map<String, Integer> numCompletePostSurveys) {
        this.numCompletePostSurveys = numCompletePostSurveys;
    }

    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public Map<String, Integer> getBombBeaconsPlaced() {
        return bombBeaconsPlaced;
    }

    public void setBombBeaconsPlaced(Map<String, Integer> bombBeaconsPlaced) {
        this.bombBeaconsPlaced = bombBeaconsPlaced;
    }

    public Map<String, Integer> getBudgetExpended() {
        return budgetExpended;
    }

    public void setBudgetExpended(Map<String, Integer> budgetExpended) {
        this.budgetExpended = budgetExpended;
    }

    public int getNumFieldVisits() {
        return numFieldVisits;
    }

    public void setNumFieldVisits(int numFieldVisits) {
        this.numFieldVisits = numFieldVisits;
    }

    public String getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(String experimentId) {
        this.experimentId = experimentId;
    }

    public String getTrialId() {
        return trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

    public Map<String, Integer> getCommBeaconsPlaced() {
        return commBeaconsPlaced;
    }

    public void setCommBeaconsPlaced(Map<String, Integer> commBeaconsPlaced) {
        this.commBeaconsPlaced = commBeaconsPlaced;
    }

    public int getBombsTotal() {
        return bombsTotal;
    }

    public void setBombsTotal(int bombsTotal) {
        this.bombsTotal = bombsTotal;
    }

    public List<String> getMembers() {
        return members;
    }

    public void setMembers(List<String> members) {
        this.members = members;
    }
}
