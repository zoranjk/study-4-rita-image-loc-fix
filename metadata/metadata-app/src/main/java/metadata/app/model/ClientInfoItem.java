package metadata.app.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientInfoItem {

	private String callsign;
	private String participantId;
	
	@JsonCreator
	public ClientInfoItem(
			@JsonProperty("callsign") String callsign,
			@JsonProperty("participant_id") String participantId
			) {
        this.callsign = callsign;
        this.participantId = participantId;
    }

	@JsonProperty("callsign")
	public String getCallsign() {
		return callsign;
	}
	@JsonProperty("callsign")
	public void setCallsign(String callsign) {
		this.callsign = callsign;
	}
	
	@JsonProperty("participant_id")
	public String getParticipantId() {
		return participantId;
	}
	@JsonProperty("participant_id")
	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}

}
