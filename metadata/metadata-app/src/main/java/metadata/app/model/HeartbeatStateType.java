package metadata.app.model;

public enum HeartbeatStateType {
	ok, warning, error
}
