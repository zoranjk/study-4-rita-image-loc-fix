package metadata.app.model;

public enum ReplayCompletedReasonType {
	ERROR, ABORTED, FINISHED, UNKNOWN
}
