package metadata.app.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DataTrial {
	private String name;
	private String date;
	private String experimenter;
	private List<String> subjects;
	private String trialNumber;
	private String groupNumber;
	private String studyNumber;
	private String condition;
	private List<String> notes;
	private String testbedVersion;
	private String experimentName;
	private String experimentDate;
	private String experimentAuthor;
	private String experimentMission;
	private String mapName;
	private String mapBlockFilename;
	private List<ClientInfoItem> clientInfo;
	private String teamId;
	private List<String> interventionAgents;
	private List<String> observers;

	@JsonCreator
	public DataTrial(
			@JsonProperty("name") String name,
			@JsonProperty("date") String date,
			@JsonProperty("experimenter") String experimenter,
			@JsonProperty("subjects") List<String> subjects,
			@JsonProperty("trial_number") String trialNumber,
			@JsonProperty("group_number") String groupNumber,
			@JsonProperty("study_number") String studyNumber,
			@JsonProperty("condition") String condition,
			@JsonProperty("notes") List<String> notes,
			@JsonProperty("testbed_version") String testbedVersion,
			@JsonProperty("experiment_name") String experimentName,
			@JsonProperty("experiment_date") String experimentDate,
			@JsonProperty("experiment_author") String experimentAuthor,
			@JsonProperty("experiment_mission") String experimentMission,
			@JsonProperty("map_name") String mapName,
			@JsonProperty("map_block_filename") String mapBlockFilename,
			@JsonProperty("client_info") List<ClientInfoItem> clientInfo,
			@JsonProperty("team_id") String teamId,
			@JsonProperty("intervention_agents") List<String> interventionAgents,
			@JsonProperty("observers") List<String> observers
			) {
		this.name = name;
		this.date = date;
		this.experimenter = experimenter;
		this.subjects = subjects == null ? new ArrayList<String>() : subjects;
		this.trialNumber = trialNumber;
		this.groupNumber = groupNumber;
		this.studyNumber = studyNumber;
		this.condition = condition;
		this.notes = notes == null ? new ArrayList<String>() : notes;
		this.testbedVersion = testbedVersion;
		this.experimentName = experimentName;
		this.experimentDate = experimentDate;
		this.experimentAuthor = experimentAuthor;
		this.experimentMission = experimentMission;
		this.mapName = mapName;
		this.mapBlockFilename = mapBlockFilename;
		this.clientInfo = clientInfo;
		this.teamId = teamId;
		this.interventionAgents = interventionAgents;
		this.observers = observers;
	}

	public DataTrial() {
		// TODO Auto-generated constructor stub
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}
	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("date")
	public String getDate() {
		return date;
	}
	@JsonProperty("date")
	public void setDate(String date) {
		this.date = date;
	}

	@JsonProperty("experimenter")
	public String getExperimenter() {
		return experimenter;
	}
	@JsonProperty("experimenter")
	public void setExperimenter(String experimenter) {
		this.experimenter = experimenter;
	}

	@JsonProperty("subjects")
	public List<String> getSubjects() {
		return subjects == null ? new ArrayList<String>() : subjects;
	}
	@JsonProperty("subjects")
	public void setSubjects(List<String> subjects) {
		this.subjects = subjects == null ? new ArrayList<String>() : subjects;
	}

	@JsonProperty("trial_number")
	public String getTrialNumber() {
		return trialNumber;
	}
	@JsonProperty("trial_number")
	public void setTrialNumber(String trialNumber) {
		this.trialNumber = trialNumber;
	}

	@JsonProperty("group_number")
	public String getGroupNumber() {
		return groupNumber;
	}
	@JsonProperty("group_number")
	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}

	@JsonProperty("study_number")
	public String getStudyNumber() {
		return studyNumber;
	}
	@JsonProperty("study_number")
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	@JsonProperty("condition")
	public String getCondition() {
		return condition;
	}
	@JsonProperty("condition")
	public void setCondition(String condition) {
		this.condition = condition;
	}

	@JsonProperty("notes")
	public List<String> getNotes() {
		return notes == null ? new ArrayList<String>() : notes;
	}
	@JsonProperty("notes")
	public void setNotes(List<String> notes) {
		this.notes = notes == null ? new ArrayList<String>() : notes;
	}

	@JsonProperty("testbed_version")
	public String getTestbedVersion() {
		return testbedVersion;
	}
	@JsonProperty("testbed_version")
	public void setTestbedVersion(String testbedVersion) {
		this.testbedVersion = testbedVersion;
	}

	@JsonProperty("experiment_name")
	public String getExperimentName() {
		return experimentName;
	}
	@JsonProperty("experiment_name")
	public void setExperimentName(String experimentName) {
		this.experimentName = experimentName;
	}

	@JsonProperty("experiment_date")
	public String getExperimentDate() {
		return experimentDate;
	}
	@JsonProperty("experiment_date")
	public void setExperimentDate(String experimentDate) {
		this.experimentDate = experimentDate;
	}

	@JsonProperty("experiment_author")
	public String getExperimentAuthor() {
		return experimentAuthor;
	}
	@JsonProperty("experiment_author")
	public void setExperimentAuthor(String experimentAuthor) {
		this.experimentAuthor = experimentAuthor;
	}

	@JsonProperty("experiment_mission")
	public String getExperimentMission() {
		return experimentMission;
	}
	@JsonProperty("experiment_mission")
	public void setExperimentMission(String experimentMission) {
		this.experimentMission = experimentMission;
	}

	@JsonProperty("map_name")
	public String getMapName() {
		return mapName;
	}
	@JsonProperty("map_name")
	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	@JsonProperty("map_block_filename")
	public String getMapBlockFilename() {
		return mapBlockFilename;
	}
	@JsonProperty("map_block_filename")
	public void setMapBlockFilename(String mapBlockFilename) {
		this.mapBlockFilename = mapBlockFilename;
	}

	@JsonProperty("client_info")
	public List<ClientInfoItem> getClientInfo() {
		return clientInfo;
	}
	@JsonProperty("client_info")
	public void setClientInfo(List<ClientInfoItem> clientInfo) {
		this.clientInfo = clientInfo;
	}

	@JsonProperty("team_id")
	public String getTeamId() {
		return teamId;
	}
	@JsonProperty("team_id")
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	@JsonProperty("intervention_agents")
	public List<String> getInterventionAgents() {
		return interventionAgents == null ? new ArrayList<String>() : interventionAgents;
	}
	@JsonProperty("intervention_agents")
	public void setInterventionAgents(List<String> interventionAgents) {
		this.interventionAgents = interventionAgents == null ? new ArrayList<String>() : interventionAgents;
	}

	@JsonProperty("observers")
	public List<String> getObservers() {
		return observers == null ? new ArrayList<String>() : observers;
	}
	@JsonProperty("observers")
	public void setObservers(List<String> observers) {
		this.observers = observers == null ? new ArrayList<String>() : observers;
	}
}
