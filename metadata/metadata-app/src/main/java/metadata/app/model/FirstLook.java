package metadata.app.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FirstLook {

	private String messageType;
	private String subType;
	private Comparison comparison;
	
	@JsonCreator
	public FirstLook(
			@JsonProperty("message_type") String messageType,
			@JsonProperty("sub_type") String subType,
			@JsonProperty("comparison") Comparison comparison
			) {
		this.messageType = messageType;
        this.subType = subType;
        this.comparison = comparison;   
    }

	@JsonProperty("message_type")
	public String getMessageType() {
		return messageType;
	}
	@JsonProperty("message_type")
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	@JsonProperty("sub_type")
	public String getSubType() {
		return subType;
	}
	@JsonProperty("sub_type")
	public void setSubType(String subType) {
		this.subType = subType;
	}
	@JsonProperty("comparison")
	public Comparison getComparison() {
		return comparison;
	}
	@JsonProperty("comparison")
	public void setComparison(Comparison comparison) {
		this.comparison = comparison;
	}	
}