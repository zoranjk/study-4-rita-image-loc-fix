package metadata.app.controller;

import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.MutableHttpResponse;
import io.micronaut.http.annotation.*;
import io.micronaut.http.server.types.files.StreamedFile;
import io.micronaut.http.server.types.files.SystemFile;
import jakarta.inject.Inject;
import metadata.app.model.ReplayBody;
import metadata.app.model.TrialStopOptions;
import metadata.app.service.DefaultExportDataHelperService;
import metadata.app.service.DefaultTimeWindowService;

import java.io.IOException;

@Controller("/export")
public class ExportDataHelperController {

    private final DefaultExportDataHelperService crudService;

    @Inject
    public ExportDataHelperController(DefaultExportDataHelperService crudService) {
        this.crudService = crudService;
    }

	@Post(value = "/clean")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public HttpResponse<?> clean(@Body TrialStopOptions trialStopOptions) throws IOException {
		boolean result = crudService.autoExport(trialStopOptions);
    	if (result) {
    		return HttpResponse.ok();
    	} else {
    		return HttpResponse.notFound();
    	}
	}
}
