package metadata.app.service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.core.type.Argument;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.rxjava2.http.client.RxHttpClient;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;
import metadata.app.Application;
import metadata.app.event.TrialStopEventEmitterBean;
import metadata.app.model.MessageTrial;
import metadata.app.model.SummaryTrial;
import metadata.app.model.TrialStopOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Singleton
public class DefaultExportDataHelperService implements ExportDataHelperService {
	private static final Logger logger = LoggerFactory.getLogger(DefaultExportDataHelperService.class);

	@Inject
	private ObjectMapper objectMapper;

	@Inject
	TrialStopEventEmitterBean trialStopEventEmitterBean;

	@Client("${asist.database}")
	@Inject
	RxHttpClient httpClient;

	private final String HEADER_METADATA_FILE_TYPE = ".metadata";

	@ExecuteOn(TaskExecutors.IO)
	@Override
	public boolean autoExport(TrialStopOptions trialStopEventOptions) {
		String sourceFolder = trialStopEventOptions.getSourceFolder();
		Path sourcesPath = Paths.get(Application.IS_WINDOWS ? System.getProperty("user.dir") : "/var/lib/metadata-app", sourceFolder);
		try {
			List<String> files = findFiles(sourcesPath);
			Map<String, Integer> tenure = new HashMap<>();
			Map<String, Integer> teamTenure = new HashMap<>();
			List<SummaryTrial> summaryTrials = httpClient.toBlocking().retrieve(HttpRequest.GET("/SSOService/trial/GetAllTrialRecords").bearerAuth(trialStopEventOptions.getToken()).contentType(MediaType.APPLICATION_JSON), Argument.listOf(SummaryTrial.class));
//			String summaryTrials2 = httpClient.toBlocking().retrieve(HttpRequest.GET("/SSOService/trial/GetAllTrialRecords").bearerAuth(trialStopEventOptions.getToken()).contentType(MediaType.APPLICATION_JSON));

			if (files.isEmpty()) {
				logger.info("No files found in the [{}] folder.", trialStopEventOptions.getSourceFolder());
			}
			for (String file : files) {
				long startTime = System.nanoTime();
				logger.info("Cleaning export file [{}]", file);
				TrialMessageFile trialMessageFile = extractMetadataFile(file, trialStopEventOptions.getRegenerate());
				logger.info("For trial [{}]", trialMessageFile.getMessageTrial().getMsg().getTrialId());
				if (trialMessageFile.getMessageTrial() != null) {
					// Set timestamp
					Path path = Paths.get(file);
					String filename = path.getFileName().toString();
					String timestamp = filename.substring(0, filename.indexOf("+"));
					trialStopEventOptions.setTimestamp(timestamp);
					// Set file path
					trialStopEventOptions.setPath(trialMessageFile.getPath());
					// Player Tenure
					Map<String, Integer> _tenure = new HashMap<>();
					Map<String, Integer> _teamTenure = new HashMap<>();
					trialMessageFile.getMessageTrial().getData().getClientInfo().forEach(client -> {
						tenure.merge(client.getParticipantId(), 1, Integer::sum);
						_tenure.put(client.getParticipantId(), tenure.get(client.getParticipantId()));
					});
					trialStopEventOptions.setTenure(_tenure);
					// Team Tenure
					teamTenure.merge(trialMessageFile.getMessageTrial().getData().getTeamId(), 1, Integer::sum);
					_teamTenure.put(trialMessageFile.getMessageTrial().getData().getTeamId(), teamTenure.get(trialMessageFile.getMessageTrial().getData().getTeamId()));
					trialStopEventOptions.setTeamTenure(_teamTenure);
					// Corrected Team Score
					SummaryTrial summaryTrial = summaryTrials.stream()
							.filter(st -> trialMessageFile.getMessageTrial().getMsg().getTrialId().equals(st.getTrialId()))
							.findAny()
							.orElse(null);
					logger.info(objectMapper.writeValueAsString(summaryTrial));
					trialStopEventOptions.setSummaryTrial(summaryTrial);
					trialStopEventOptions.setCorrectedTeamScore(summaryTrial != null ? summaryTrial.getTeamScore() : -1);
					// Send it.
					trialStopEventEmitterBean.publishTrialStopOptionsEvent(trialMessageFile.getMessageTrial(), trialStopEventOptions);
				}
				long endTime = System.nanoTime();
				long duration = (endTime - startTime);
				logger.info("Completed cleaning in [{}] seconds.", TimeUnit.SECONDS.convert(duration, TimeUnit.NANOSECONDS));
			}
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			logger.error(e.toString());
			logger.error(e.getMessage());
			return false;
		}
	}

	private List<String> findFiles(Path path)	throws IOException {
		if (!Files.isDirectory(path)) {
			throw new IllegalArgumentException("Path must be a directory!");
		}

		List<String> result;
		try (Stream<Path> walk = Files.walk(path)) {
			result = walk
					.filter(p -> !Files.isDirectory(p))
					.map(p -> p.toString().toLowerCase())
					.filter(f -> f.endsWith(".zip"))
					.collect(Collectors.toList());
		}

		return result;
	}

	private TrialMessageFile extractMetadataFile(String file, boolean regenerate) throws IOException {
		try (ZipFile zipFile = new ZipFile(file)) {
			Enumeration<? extends ZipEntry> entries = zipFile.entries();
			while (entries.hasMoreElements()) {
				ZipEntry entry = entries.nextElement();
				// Check if entry is a directory and a metadata file.
				if (!entry.isDirectory() && entry.getName().toLowerCase().endsWith(HEADER_METADATA_FILE_TYPE)) {
					Path path = null;
					if (!regenerate) {
						try (InputStream inputStream = zipFile.getInputStream(entry)) {
							String[] name = entry.getName().split("\\.(?=[^\\.]+$)");
							File tempFile = File.createTempFile(name[0], "." + name[1]);
							Files.copy(inputStream, tempFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
							path = tempFile.toPath();
						}
					}
					try (InputStream inputStream = zipFile.getInputStream(entry)) {
						BufferedReader bufferedReader =	new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8));
						while (bufferedReader.ready()) {
							String json = bufferedReader.readLine();
							JsonNode jsonNode = objectMapper.readTree(json);
							JsonNode jsonNodeHeader = jsonNode.get("header");
							JsonNode jsonNodeMsg = jsonNode.get("msg");
							if (jsonNodeHeader != null && jsonNodeMsg != null) {
								String message_type = jsonNodeHeader.get("message_type").asText();
								String sub_type = jsonNodeMsg.get("sub_type").asText();
								if (message_type.equalsIgnoreCase("trial")) {
									if (sub_type.equalsIgnoreCase("stop")) {
										return new TrialMessageFile(objectMapper.readValue(json, MessageTrial.class), path);
									}
								}
							}
						}
					}

				}
			}
		}
		return null;
	}

	private static class TrialMessageFile {
		private final MessageTrial messageTrial;
		private final Path path;
		public TrialMessageFile(MessageTrial messageTrial, Path path) {
			this.messageTrial = messageTrial;
			this.path = path;
		}
		public MessageTrial getMessageTrial() {
			return messageTrial;
		}

		public Path getPath() {
			return path;
		}
	}
}
