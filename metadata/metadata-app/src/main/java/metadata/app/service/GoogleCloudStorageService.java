package metadata.app.service;

import com.google.cloud.storage.Blob;

import io.micronaut.context.annotation.Property;
import io.micronaut.context.annotation.Requires;
import io.micronaut.management.endpoint.health.HealthEndpoint;
import io.micronaut.objectstorage.googlecloud.GoogleCloudStorageOperations;
import io.micronaut.objectstorage.request.UploadRequest;
import io.micronaut.objectstorage.response.UploadResponse;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
//@Requires(property = "gcp.credentials.encoded-key", notEquals="false")
public class GoogleCloudStorageService {
	
	public final GoogleCloudStorageOperations objectStorage;
	
	@Inject
	public GoogleCloudStorageService(GoogleCloudStorageOperations objectStorage) {
		this.objectStorage = objectStorage;
	}

	public UploadResponse<Blob> upload(UploadRequest objectStorageUpload) {
		return this.objectStorage.upload(objectStorageUpload);
	}

}
