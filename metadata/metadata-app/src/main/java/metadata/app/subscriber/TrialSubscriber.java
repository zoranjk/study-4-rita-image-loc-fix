package metadata.app.subscriber;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import jakarta.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import io.micronaut.context.annotation.Property;
import io.micronaut.core.io.ResourceLoader;
import io.micronaut.http.server.types.files.SystemFile;
import io.micronaut.mqtt.annotation.MqttSubscriber;
import io.micronaut.mqtt.annotation.Topic;
import io.vertx.core.file.CopyOptions;
import metadata.app.Application;
import metadata.app.event.TrialStopEventEmitterBean;
import metadata.app.model.Experiment;
import metadata.app.model.FirstLook;
import metadata.app.model.FirstLookFile;
import metadata.app.model.FirstLookReport;
import metadata.app.model.FirstLookTest;
import metadata.app.model.MessageCount;
import metadata.app.model.MessageTrial;
import metadata.app.model.Trial;
import metadata.app.service.DefaultTrialService;


@MqttSubscriber
public class TrialSubscriber {

	private static final Logger logger = LoggerFactory.getLogger(TrialSubscriber.class);
	private ObjectMapper objectMapper = new ObjectMapper();

    private final DefaultTrialService defaultTrialService;

    @Property(name = "asist.trialStopExport")
	private boolean METADATA_APP_TRIAL_STOP_EXPORT;

    @Property(name = "asist.trialStopIndex")
	private String METADATA_APP_TRIAL_STOP_INDEX;

    @Property(name = "asist.trialStopValidation")
	private boolean METADATA_APP_TRIAL_STOP_VALIDATION;

    @Property(name = "data.firstLookDataFile")
	private String FIRST_LOOK_DATA_FILE;

    @Inject
    ResourceLoader resourceLoader;

    @Inject
    TrialStopEventEmitterBean trialStopEventEmitterBean;

	@Inject
	public TrialSubscriber(DefaultTrialService defaultTrialService) {
		this.defaultTrialService = defaultTrialService;
	}

    @Topic("trial")
    public void receive(byte[] data) {

    	MessageTrial msgTrial;
		try {
			String message = new String(data, StandardCharsets.UTF_8);
			msgTrial = objectMapper.readValue(message, MessageTrial.class);

			String subType = msgTrial.getMsg().getSubType();
			String source = msgTrial.getMsg().getSource();

			if(subType.equals("create") || subType.equals("start")) {
				// Only create trials if they are not replays.
				if (!isUUID(msgTrial.getMsg().getReplayId())) {
					logger.info(MessageFormat.format("Message bus request from {0} to {1} trial [{2}].", source, subType, msgTrial.getMsg().getTrialId()));
					// Just need to pass in the id. This came from the control so id is expected to be correct.
					// No need to query to make sure id exists.
					Experiment tempExperiment = new Experiment(-1l, msgTrial.getMsg().getExperimentId(), msgTrial.getData().getExperimentName(), msgTrial.getData().getExperimentDate(), msgTrial.getData().getExperimentAuthor(), msgTrial.getData().getExperimentMission(), msgTrial.getData().getCondition());
					Trial trial = new Trial(
						-1,
						msgTrial.getMsg().getTrialId(),
						msgTrial.getData().getName(),
						msgTrial.getData().getDate(),
						msgTrial.getData().getExperimenter(),
						msgTrial.getData().getSubjects(),
						msgTrial.getData().getTrialNumber(),
						msgTrial.getData().getGroupNumber(),
						msgTrial.getData().getStudyNumber(),
						msgTrial.getData().getCondition(),
						msgTrial.getData().getNotes(),
						msgTrial.getData().getTestbedVersion(),
						tempExperiment,
						msgTrial.getData().getMapName(),
						msgTrial.getData().getMapBlockFilename(),
						msgTrial.getData().getClientInfo(),
						msgTrial.getData().getTeamId(),
						msgTrial.getData().getInterventionAgents(),
						msgTrial.getData().getObservers()
						);

					Trial newTrial = defaultTrialService.createTrial(trial);
					if (newTrial != null) {
						logger.info(MessageFormat.format("Trial {0} created over message bus.", newTrial.getTrialId()));
					} else {
						logger.info(MessageFormat.format("Trial {0} could not be created over message bus.", trial.getTrialId()));
					}
				}
			} else if(subType.equals("stop")) {
				logger.info(MessageFormat.format("Message bus request from {0} to {1} trial [{2}].", source, subType, msgTrial.getMsg().getTrialId()));
				trialStopEventEmitterBean.publishTrialStopEvent(msgTrial);
			}

    	} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
    }

    private boolean isUUID(String value) {
		if (value != null) {
			try {
				UUID.fromString(value.toString());
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

    private boolean isFirstLookValid(FirstLook firstLook, Integer count) {
    	if (count == null) {
    		return false;
    	}
    	switch (firstLook.getComparison().getOperator()) {
    	case ">=":
    		return firstLook.getComparison().getValue() >= count;
    	case "==":
    		return firstLook.getComparison().getValue() == count;
		default:
			return false;
    	}
    }

    private String removeExtension(String fileName) {
        int lastIndex = fileName.lastIndexOf('.');
        if (lastIndex != -1) {
            fileName = fileName.substring(0, lastIndex);
        }
        return fileName;
    }
}
