package metadata.app.event;

import metadata.app.model.MessageTrial;

public class TrialStopExportEvent {

	private final MessageTrial msgTrial;

	public TrialStopExportEvent(MessageTrial msgTrial) {
		this.msgTrial = msgTrial;
	}

    public MessageTrial getMsgTrial() {
        return msgTrial;
    }

}
