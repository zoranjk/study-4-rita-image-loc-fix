package metadata.app.event;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import jakarta.inject.Singleton;

import org.apache.lucene.search.TotalHits;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;

import io.micronaut.context.annotation.Property;
import io.micronaut.http.server.types.files.SystemFile;
import io.micronaut.runtime.event.annotation.EventListener;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.TaskScheduler;
import metadata.app.Application;
import metadata.app.model.FirstLook;
import metadata.app.model.FirstLookFile;
import metadata.app.model.FirstLookReport;
import metadata.app.model.FirstLookTest;
import metadata.app.model.MessageCount;
import metadata.app.model.MessageTrial;
import metadata.app.service.DefaultTrialService;

@Singleton
public class TrialStopEventListener {
	private static final Logger logger = LoggerFactory.getLogger(TrialStopEventListener.class);

	@Inject
	private RestHighLevelClient elasticsearchClient;

	@Property(name = "asist.trialStopIndex")
	private String METADATA_APP_TRIAL_STOP_INDEX;

	@Property(name = "asist.trialStopInitialDelay")
	private Duration METADATA_APP_TRIAL_STOP_INITIAL_DELAY;

	@Property(name = "asist.trialStopInterval")
	private Duration METADATA_APP_TRIAL_STOP_INTERVAL;

	@Property(name = "asist.trialStopTimeout")
	private Duration METADATA_APP_TRIAL_STOP_TIMEOUT;

	private long executionCounter = 0;

	private final TaskScheduler taskScheduler;
	private ScheduledFuture<?> scheduledFuture = null;

	private MessageTrial msgTrial = null;

	@Inject
    TrialStopExportEventEmitterBean trialStopExportEventEmitterBean;

	public TrialStopEventListener(@Named(TaskExecutors.SCHEDULED) TaskScheduler taskScheduler) {
		this.taskScheduler = taskScheduler;
	}

	@EventListener
	public void onTrialStopEvent(TrialStopEvent event) {
		msgTrial = event.getMsgTrial();
		if (event.getMsgTrial() == null) {
			logger.info("Trial stop event received but message was null!");
			return;
		}
		logger.info(MessageFormat.format("Trial stop event received for trial {0}.", msgTrial.getMsg().getTrialId()));
		executionCounter = 0;
		scheduledFuture = taskScheduler.scheduleWithFixedDelay(METADATA_APP_TRIAL_STOP_INITIAL_DELAY, METADATA_APP_TRIAL_STOP_INTERVAL, this::execute);
	}
	public void execute() {
		if (METADATA_APP_TRIAL_STOP_INITIAL_DELAY.plus(METADATA_APP_TRIAL_STOP_INTERVAL.multipliedBy(executionCounter)).compareTo(METADATA_APP_TRIAL_STOP_TIMEOUT) <= 0) {
			logger.info(MessageFormat.format("Detecting if trial stop has been indexed in elasticsearch for trial id {0}{1}.", msgTrial.getMsg().getTrialId(), msgTrial.getMsg().getReplayId() != null ? " replay id " + msgTrial.getMsg().getReplayId() : ""));
			final boolean exist = trialStopExist(msgTrial.getMsg().getTrialId(), msgTrial.getMsg().getReplayId()); // Do wee need replayId here as well for replays, otherwise will this be true for all trials that were replayed
			if (exist) {
				logger.info(MessageFormat.format("Trial stop has been indexed in elasticsearch for trial id {0}{1}!", msgTrial.getMsg().getTrialId(), msgTrial.getMsg().getReplayId() != null ? " replay id " + msgTrial.getMsg().getReplayId() : ""));
				cancel();
				trialStopExportEventEmitterBean.publishTrialStopExportEvent(msgTrial);
			}
			executionCounter++;
		} else {
			logger.info(MessageFormat.format("Trial stop event timeout reached for trial id {0}{1}!", msgTrial.getMsg().getTrialId(), msgTrial.getMsg().getReplayId() != null ? " replay id " + msgTrial.getMsg().getReplayId() : ""));
			cancel();
		}
	}

	// use this method to cancel the job
	public void cancel() {
		if (scheduledFuture != null) {
			boolean canceled = scheduledFuture.cancel(false);
			scheduledFuture = null;
			executionCounter = 0;
		}
	}

	public boolean trialStopExist(String trialId, String replayId) {
		// Check to see if the trialId is already present in elasticsearch.
		long totalMessageCount = 0;
		// Check to see if index already has a document with this trial id.
		SearchRequest searchRequest = new SearchRequest(METADATA_APP_TRIAL_STOP_INDEX);

//		MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("msg.trial_id.keyword", id);
		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
		boolQueryBuilder.must(QueryBuilders.matchQuery("msg.trial_id.keyword", trialId));
		boolQueryBuilder.must(QueryBuilders.matchQuery("msg.sub_type", "stop"));
		if (replayId == null) {
			boolQueryBuilder.mustNot(QueryBuilders.existsQuery("msg.replay_id"));
		} else {
			boolQueryBuilder.must(QueryBuilders.matchQuery("msg.replay_id.keyword", replayId));
		}
		boolQueryBuilder.must(QueryBuilders.matchQuery("header.message_type", "trial"));

		SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
		searchSourceBuilder.size(0);
		searchSourceBuilder.fetchSource(null, "message");
		searchSourceBuilder.query(boolQueryBuilder);

		searchRequest.source(searchSourceBuilder);

		try {
			SearchResponse searchResponse;
			searchResponse = elasticsearchClient.search(searchRequest, RequestOptions.DEFAULT);

			TotalHits totalHits = searchResponse.getHits().getTotalHits();
			totalMessageCount = totalHits.value;

		} catch (IOException e) {
			logger.error(e.getMessage());
		}

		if (totalMessageCount > 0) {
			return true;
		} else {
			return false;
		}
	}
}
