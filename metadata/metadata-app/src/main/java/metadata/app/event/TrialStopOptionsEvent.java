package metadata.app.event;

import metadata.app.model.MessageTrial;
import metadata.app.model.TrialStopOptions;

public class TrialStopOptionsEvent {

	private final MessageTrial msgTrial;
	private final TrialStopOptions trialStopOptions;

	public TrialStopOptionsEvent(MessageTrial msgTrial, TrialStopOptions trialStopOptions) {
		this.msgTrial = msgTrial;
		this.trialStopOptions = trialStopOptions;
	}

    public MessageTrial getMsgTrial() {
        return msgTrial;
    }
    public TrialStopOptions getTrialStopOptions() {
        return trialStopOptions;
    }

}
