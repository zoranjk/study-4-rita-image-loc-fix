package metadata.app.event;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import io.micronaut.context.event.ApplicationEventPublisher;
import metadata.app.model.MessageTrial;
import metadata.app.model.TrialStopOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class TrialStopEventEmitterBean {
    private static final Logger logger = LoggerFactory.getLogger(TrialStopEventEmitterBean.class);

    @Inject
    ApplicationEventPublisher<TrialStopEvent> trialStopEventPublisher;
    @Inject
    ApplicationEventPublisher<TrialStopOptionsEvent> trialStopOptionsEventPublisher;

    public void publishTrialStopEvent(MessageTrial msgTrial) {
        try {
            trialStopEventPublisher.publishEvent(new TrialStopEvent(msgTrial));
        } catch(Exception e) {
            logger.info(e.getMessage());
        }
    }

    // with options
    public void publishTrialStopOptionsEvent(MessageTrial msgTrial, TrialStopOptions trialStopEventOptions) {
        try {
            trialStopOptionsEventPublisher.publishEvent(new TrialStopOptionsEvent(msgTrial, trialStopEventOptions));
        } catch(Exception e) {
            logger.info(e.getMessage());
        }
    }

}
