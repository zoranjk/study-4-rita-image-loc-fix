package metadata.app.event;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import java.nio.file.Path;

import io.micronaut.context.event.ApplicationEventPublisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Singleton
public class TrialStopUploadEventEmitterBean {
    private static final Logger logger = LoggerFactory.getLogger(TrialStopUploadEventEmitterBean.class);

    @Inject
    ApplicationEventPublisher<TrialStopUploadEvent> eventPublisher;

    public void publishTrialStopUploadEvent(Path path, String id) {
        try {
            eventPublisher.publishEvent(new TrialStopUploadEvent(path, id));
        } catch(Exception e) {
            logger.info(e.getMessage());
        }
    }

}
