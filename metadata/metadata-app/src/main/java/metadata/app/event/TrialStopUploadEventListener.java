package metadata.app.event;

import java.text.MessageFormat;
import java.time.Duration;
import java.util.*;

import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.cloud.storage.Blob;

import io.micronaut.context.annotation.Property;
import io.micronaut.context.annotation.Requires;
import io.micronaut.core.io.ResourceLoader;
import io.micronaut.objectstorage.googlecloud.GoogleCloudStorageOperations;
import io.micronaut.objectstorage.request.UploadRequest;
import io.micronaut.objectstorage.response.UploadResponse;
import io.micronaut.runtime.event.annotation.EventListener;

@Singleton
@Requires(property = "gcp.credentials.encoded-key", notEquals="false")
public class TrialStopUploadEventListener {
	private static final Logger logger = LoggerFactory.getLogger(TrialStopUploadEventListener.class);
	private final GoogleCloudStorageOperations objectStorage;

	@Property(name = "asist.trialStopUpload")
	private boolean METADATA_APP_TRIAL_STOP_UPLOAD;

	@Property(name = "gcp.credentials.encoded-key")
	private String GCP_CREDENTIALS_ENCODEDKEY;

	@Inject
	ResourceLoader resourceLoader;

	@Property(name = "asist.trialStopInterval")
	private Duration METADATA_APP_TRIAL_STOP_INTERVAL;

	@Inject
	public TrialStopUploadEventListener(GoogleCloudStorageOperations objectStorage) {
		this.objectStorage = objectStorage;
	}

	@EventListener
	public void onTrialStopUploadEvent(TrialStopUploadEvent event) {
		if (METADATA_APP_TRIAL_STOP_UPLOAD) {
			Set<String> uploadedObjects = objectStorage.listObjects();
			logger.info("Checking to see if [{}] has already been uploaded.", event.getId());
			boolean exists = uploadedObjects.stream().anyMatch((uploadedObject) -> uploadedObject.contains(event.getId()));
			if (exists) {
				logger.info("Uploaded aborted, [{}] already exists!", event.getId());
			} else {
				logger.info("Uploading file {}.", event.getPath().toString());
				UploadRequest objectStorageUpload = UploadRequest.fromPath(event.getPath());
				UploadResponse<Blob> response = objectStorage.upload(objectStorageUpload);
				String responseETag = response.getETag();
				String responseKey = response.getKey();
				logger.info(MessageFormat.format("Uploaded ETag: [{0}] Key: [{1}].", responseETag, responseKey));
			}
		}
	}
}
