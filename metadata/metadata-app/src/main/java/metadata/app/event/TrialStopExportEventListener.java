package metadata.app.event;

import java.io.*;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import agent.tests.api.AgentTest;
import agent.tests.api.AgentTestPluginManager;
import agent.tests.api.AgentTestResultRow;
import com.fasterxml.jackson.databind.ObjectReader;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

import metadata.app.model.*;
import metadata.app.model.util.CustomMappingStrategy;
import metadata.app.publisher.AutoExportPublisher;
import org.pf4j.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.dockerjava.api.model.Container;
import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;

import io.micronaut.context.annotation.Property;
import io.micronaut.core.io.ResourceLoader;
import io.micronaut.http.server.types.files.SystemFile;
import io.micronaut.runtime.event.annotation.EventListener;
import metadata.app.Application;
import metadata.app.service.DefaultDockerService;
import metadata.app.service.DefaultReplayService;
import metadata.app.service.DefaultTrialService;

@Singleton
public class TrialStopExportEventListener {
	private static final Logger logger = LoggerFactory.getLogger(TrialStopExportEventListener.class);
	private ObjectMapper objectMapper = new ObjectMapper();

	private final DefaultTrialService defaultTrialService;
	private final DefaultReplayService defaultReplayService;
	private final DefaultDockerService defaultDockerService;
	private final AutoExportPublisher autoExportPublisher;

	@Property(name = "asist.trialStopExport")
	private boolean METADATA_APP_TRIAL_STOP_EXPORT;

	@Property(name = "asist.trialStopValidation")
	private boolean METADATA_APP_TRIAL_STOP_VALIDATION;

	@Property(name = "asist.trialStopMeasures")
	private boolean METADATA_APP_TRIAL_STOP_MEASURES;

	@Property(name = "asist.trialStopAgentTests")
	private boolean METADATA_APP_TRIAL_STOP_AGENT_TESTS;

	@Property(name = "asist.trialStopDockerLogs")
	private boolean METADATA_APP_TRIAL_STOP_DOCKER_LOGS;

	@Property(name = "asist.trialStopAgentLogs")
	private boolean METADATA_APP_TRIAL_STOP_AGENT_LOGS;

	@Property(name = "asist.trialStopUpload")
	private boolean METADATA_APP_TRIAL_STOP_UPLOAD;

	@Property(name = "asist.trialStopIndex")
	private String METADATA_APP_TRIAL_STOP_INDEX;

	@Property(name = "data.agentLogsFile")
	private String AGENT_LOGS_FILE;

	@Property(name = "data.firstLookDataFile")
	private String FIRST_LOOK_DATA_FILE;

	@Inject
	ResourceLoader resourceLoader;

	@Property(name = "asist.trialStopInterval")
	private Duration METADATA_APP_TRIAL_STOP_INTERVAL;

	private MessageTrial msgTrial = null;

	@Inject
    TrialStopUploadEventEmitterBean trialStopUploadEventEmitterBean;

	@Inject
	public TrialStopExportEventListener(DefaultTrialService defaultTrialService, DefaultReplayService defaultReplayService, DefaultDockerService defaultDockerService, AutoExportPublisher autoExportPublisher) {
		this.defaultTrialService = defaultTrialService;
		this.defaultReplayService = defaultReplayService;
		this.defaultDockerService = defaultDockerService;
		this.autoExportPublisher = autoExportPublisher;
	}

	@EventListener
	public void onTrialStopExportEvent(TrialStopExportEvent event) {
		logger.info("Export event received!");
		if (event.getMsgTrial() == null) {
			msgTrial = event.getMsgTrial();
			logger.info("Trial stop export event received but message was null!");
			try {
				autoExportPublisher.send(objectMapper.writeValueAsBytes(createAutoExportMessage(event.getMsgTrial(),AutoExportStateType.error))).subscribe(() -> {
					// handle completion
				}, throwable -> {
					// handle error
				});
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
			return;
		} else {
			performExport(event.getMsgTrial(), null);
		}
	}

	@EventListener
	public void onTrialStopExportOptionsEvent(TrialStopOptionsEvent event) {
		logger.info("Export event with options received!");
		if (event.getMsgTrial() == null) {
			msgTrial = event.getMsgTrial();
			logger.info("Trial stop export event received but message was null!");
			try {
				autoExportPublisher.send(objectMapper.writeValueAsBytes(createAutoExportMessage(event.getMsgTrial(), AutoExportStateType.error))).subscribe(() -> {
					// handle completion
				}, throwable -> {
					// handle error
				});
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		} else {
			SummaryTrial summaryTrial = event.getTrialStopOptions().getSummaryTrial();
			if (summaryTrial.getMissionEndCondition().isEmpty()) {
				if (summaryTrial.getTrialEndCondition().equals("OK_ALL_PLAYERS_FROZEN")) {
					summaryTrial.setMissionEndCondition("MISSION_STOP_ALL_PLAYERS_FROZEN");
				} else if (summaryTrial.getTrialEndCondition().equals("OK_ALL_BOMBS_REMOVED")) {
					summaryTrial.setMissionEndCondition("MISSION_STOP_ALL_BOMBS_REMOVED");
				}
			}
			// MISSION_STOP_ALL_BOMBS_REMOVED, MISSION_STOP_ALL_PLAYERS_FROZEN, or MISSION_STOP_TIMER_END
			if (summaryTrial.getMissionEndCondition().equals("MISSION_STOP_ALL_BOMBS_REMOVED") || summaryTrial.getMissionEndCondition().equals("MISSION_STOP_ALL_PLAYERS_FROZEN") || summaryTrial.getMissionEndCondition().equals("MISSION_STOP_TIMER_END")) {
				performExport(event.getMsgTrial(), event.getTrialStopOptions());
			} else {
				logger.info("Skipping trial [{}] due to invalid mission end condition [{}]!", event.getMsgTrial().getMsg().getTrialId(), summaryTrial.getMissionEndCondition());
			}
		}
	}

	private void performExport(MessageTrial msgTrial, TrialStopOptions trialStopOptions) {
		boolean _METADATA_APP_TRIAL_STOP_EXPORT = trialStopOptions == null ? this.METADATA_APP_TRIAL_STOP_EXPORT : trialStopOptions.isTrialStopExport();
		boolean _METADATA_APP_TRIAL_STOP_VALIDATION = trialStopOptions == null ? this.METADATA_APP_TRIAL_STOP_VALIDATION : trialStopOptions.isTrialStopValidation();
		boolean _METADATA_APP_TRIAL_STOP_MEASURES = trialStopOptions == null ? this.METADATA_APP_TRIAL_STOP_MEASURES : trialStopOptions.isTrialStopMeasures();
		boolean _METADATA_APP_TRIAL_STOP_AGENT_TESTS = trialStopOptions == null ? this.METADATA_APP_TRIAL_STOP_AGENT_TESTS : trialStopOptions.isTrialStopAgentTests();
		boolean _METADATA_APP_TRIAL_STOP_DOCKER_LOGS = trialStopOptions == null ? this.METADATA_APP_TRIAL_STOP_DOCKER_LOGS : trialStopOptions.isTrialStopDockerLogs();
		boolean _METADATA_APP_TRIAL_STOP_AGENT_LOGS = trialStopOptions == null ? this.METADATA_APP_TRIAL_STOP_AGENT_LOGS : trialStopOptions.isTrialStopAgentLogs();
		boolean _METADATA_APP_TRIAL_STOP_UPLOAD = trialStopOptions == null ? this.METADATA_APP_TRIAL_STOP_UPLOAD : trialStopOptions.isTrialStopUpload();
		boolean _REGENERATE_METADATA_FILE_USING_ELASTICSEARCH = trialStopOptions == null ? true : trialStopOptions.isTrialStopUpload();

		if (_METADATA_APP_TRIAL_STOP_EXPORT) {
			try {
				Instant instant = Instant.now();
				if (trialStopOptions == null) {
					autoExportPublisher.send(objectMapper.writeValueAsBytes(createAutoExportMessage(msgTrial, AutoExportStateType.begin))).subscribe(() -> {
						// handle completion
					}, throwable -> {
						// handle error
					});
				}
				File sourceFileMetadata = null;
				String id = "";
				if (_REGENERATE_METADATA_FILE_USING_ELASTICSEARCH) {
					SystemFile systemFileMetadata = null;
					String replayParentType = msgTrial.getMsg().getReplayParentType();
					if (replayParentType == null) { // This would test for live trials.
						id = msgTrial.getMsg().getTrialId();
						logger.info("Exporting {} from {} index alias.", id, METADATA_APP_TRIAL_STOP_INDEX);
						systemFileMetadata = defaultTrialService.exportFile(id, METADATA_APP_TRIAL_STOP_INDEX);
					} else { // This would test for replays of trial or replays.
						id = msgTrial.getMsg().getReplayId();
						logger.info("Exporting {} [{}] from {} index alias.", replayParentType, id, METADATA_APP_TRIAL_STOP_INDEX);
						systemFileMetadata = defaultReplayService.exportFile(id, METADATA_APP_TRIAL_STOP_INDEX);
					}

					if (systemFileMetadata == null) {
						throw new IOException("Could not create file due to trial or replay id!");
					}
					sourceFileMetadata = systemFileMetadata.getFile();
				} else {
					sourceFileMetadata = trialStopOptions.getPath().toFile();
					id = msgTrial.getMsg().getTrialId();
				}
				Path sourcePathMetadata = sourceFileMetadata.toPath();

				List<String> zipFilenameParts = new ArrayList<>();
				String text = "";
				if (trialStopOptions == null) {
					LocalDateTime date = LocalDateTime .now();
					DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyMMddHHmmss");
					text = date.format(formatter);
				} else {
					text = trialStopOptions.getTimestamp();
				}

				zipFilenameParts.add(text);
				zipFilenameParts.add(msgTrial.getData().getName());
				zipFilenameParts.add(msgTrial.getData().getCondition());

				logger.debug("Metadata source path: {}", sourcePathMetadata.toString());
				Path zipFilePath = Paths
						.get(Application.IS_WINDOWS ? System.getProperty("user.dir") : "/var/lib/metadata-app",
								"output", String.join("+", zipFilenameParts) + ".zip"); // removeExtension(sourceFileMetadata.getName())
				String zipFilename = zipFilePath.toString();
				logger.info("Creating zip file: {}", zipFilename);
				FileOutputStream fos = new FileOutputStream(zipFilename);
				ZipOutputStream zos = new ZipOutputStream(fos);
				logger.info("Adding file: {} to zip.", sourcePathMetadata.toString());
				ZipEntry zipEntry1 = new ZipEntry(sourceFileMetadata.getName());
				zos.putNextEntry(zipEntry1);

				FileInputStream fis = new FileInputStream(sourceFileMetadata);
				byte[] buffer = new byte[1024];
				int len;
				while ((len = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}
				fis.close();
				zos.closeEntry();
				logger.info("File: {} added.", sourcePathMetadata.toString());

				if (_METADATA_APP_TRIAL_STOP_VALIDATION) {
					long startTime = System.nanoTime();
					logger.info("Validating trial [{}] from file {}.",
							msgTrial.getMsg().getTrialId(), sourcePathMetadata.toString());
					logger.info("Generating message counts for file {}.", sourcePathMetadata.toString());
					Map<String, Map<String, Integer>> messageCountMap = new HashMap<String, Map<String, Integer>>();
					Optional<InputStream> resource = resourceLoader.getResourceAsStream(FIRST_LOOK_DATA_FILE);
					InputStream inputStream = resource.get();
					List<FirstLook> firstLookData = objectMapper.readValue(inputStream,
							new TypeReference<List<FirstLook>>() {
							});
					logger.trace("First Look Data File: {}",
							objectMapper.writeValueAsString(firstLookData));
					int lineCount = 0;
					BufferedReader bufferedReader =	new BufferedReader(new FileReader(sourcePathMetadata.toString()));
					while (bufferedReader.ready()) {
						String line = bufferedReader.readLine();
						lineCount++;
						JsonNode jsonNode = objectMapper.readTree(line);
						JsonNode jsonNodeHeader = jsonNode.get("header");
						JsonNode jsonNodeMsg = jsonNode.get("msg");
						if (jsonNodeHeader != null && jsonNodeMsg != null) {
							String lineMsgType = jsonNodeHeader.get("message_type").asText();
							String lineSubType = jsonNodeMsg.get("sub_type").asText();
							messageCountMap.putIfAbsent(lineMsgType, new HashMap<String, Integer>());
							messageCountMap.get(lineMsgType).merge(lineSubType, 1, Integer::sum);
						} else {
							logger.error("Invalid document detected: {}", line);
						}
					}
					bufferedReader.close();
					long endTime = System.nanoTime();
					long duration = (endTime - startTime);
					logger.info("Computed counts for [{}] messages in [{}] seconds.", lineCount,
							TimeUnit.SECONDS.convert(duration, TimeUnit.NANOSECONDS));
					logger.debug(objectMapper.writeValueAsString(messageCountMap));
					logger.info("Generating First Look Report file.");
					List<MessageCount> messageCounts = new ArrayList<MessageCount>();
					messageCountMap.forEach((key, value) -> {
						value.forEach((k, v) -> {
							MessageCount messageCount = new MessageCount(key, k, v);
							messageCounts.add(messageCount);
						});
					});
					FirstLookFile firstLookFile = new FirstLookFile(sourcePathMetadata.getFileName().toString(), lineCount,
							messageCounts);
					List<FirstLookTest> firstLookTests = new ArrayList<FirstLookTest>();
					firstLookData.forEach((firstLook) -> {
						Integer count = messageCountMap.containsKey(firstLook.getMessageType())
								&& messageCountMap.get(firstLook.getMessageType()).containsKey(firstLook.getSubType())
								? messageCountMap.get(firstLook.getMessageType()).get(firstLook.getSubType())
								: null;
						boolean result = messageCountMap.containsKey(firstLook.getMessageType())
								&& messageCountMap.get(firstLook.getMessageType()).containsKey(firstLook.getSubType()) && isFirstLookValid(firstLook, count);
						FirstLookTest firstLookTest = new FirstLookTest(result, count,
								MessageFormat.format("[{0}][{1}] {2} {3}", firstLook.getMessageType(),
										firstLook.getSubType(), firstLook.getComparison().getOperator(),
										firstLook.getComparison().getValue()),
								firstLook);
						firstLookTests.add(firstLookTest);
					});
					FirstLookReport firstLookReport = new FirstLookReport(id, instant.toString(),
							firstLookFile, firstLookTests);
					byte[] firstLookBytes = objectMapper.writeValueAsBytes(firstLookReport);
					logger.info("First Look Report file generated.");

					logger.info("Adding First Look Report to zip.");
					String firstLookFilename = "firstlook.json";
					ZipEntry zipEntry2 = new ZipEntry(firstLookFilename);
					zipEntry2.setSize(firstLookBytes.length);
					zos.putNextEntry(zipEntry2);
					zos.write(firstLookBytes);
					zos.closeEntry();
					logger.info("First Look Report: {} added.", firstLookFilename);
				}
				if (_METADATA_APP_TRIAL_STOP_MEASURES) {
					try {
						long startTime = System.nanoTime();
						logger.info("Pulling measures for trial [{}] from file {}.",
								msgTrial.getMsg().getTrialId(), sourcePathMetadata.toString());
						logger.info("Generating measures counts for file {}.", sourcePathMetadata.toString());
						List<MeasuresIntervention> measuresInterventions = new ArrayList<>();
						List<MeasuresIndividual> measuresIndividuals = new ArrayList<>();
						List<MeasuresTrial> measuresTrials = new ArrayList<>();
						List<MeasuresChat> measuresChats = new ArrayList<>();
						MeasuresTrial measuresTrial = new MeasuresTrial();
						///////////////////////////////////////////////////
						// Cheat detection ////////////////////////////////
						List<String> autoJumpList = new ArrayList<>();
						List<String> preReconPurchase = new ArrayList<>();
						boolean preReconMessageEncountered = false;
						Set<String> wirecutterInventory = new HashSet<>();
						Map<String, Map<String, String>> threePhaseBombs = new HashMap<>();
						long firstVoteTimestamp = -1;
						int shopStageCount = 0;
						int fieldStageCount = 0;
						List<Long> waitTimes = new ArrayList<>();
						///////////////////////////////////////////////////
						///////////////////////////////////////////////////
						String trialId = "";
						String teamId = "";
						boolean trialSummaryFound = false;
						int lineCount = 0;
						BufferedReader bufferedReader =	new BufferedReader(new FileReader(sourcePathMetadata.toString()));
						while (bufferedReader.ready()) {
							String line = bufferedReader.readLine();
							lineCount++;
							try {
								JsonNode jsonNode = objectMapper.readTree(line);
								JsonNode jsonNodeHeader = jsonNode.get("header");
								JsonNode jsonNodeMsg = jsonNode.get("msg");
								JsonNode jsonNodeData = jsonNode.get("data");
								String topic = jsonNode.has("topic") ? jsonNode.get("topic").asText() : "";
								if (jsonNodeHeader != null && jsonNodeMsg != null) {
									String message_type = jsonNodeHeader.get("message_type").asText();
									String sub_type = jsonNodeMsg.get("sub_type").asText();
									switch (message_type) {
										case "trial" -> {
											if (sub_type.equals("start")) {
												measuresTrial.setTrialId(jsonNodeMsg.get("trial_id").asText());
												if (jsonNodeData != null) {
//													measuresTrial.setMapId(jsonNodeData.get("map_name").asText());
//													StringJoiner stringJoiner1 = new StringJoiner(",", "", "");
//													jsonNodeData.withArray("client_info").forEach(interventionAgent -> {
//														stringJoiner1.add(interventionAgent.get("participant_id").asText());
//													});
//													measuresTrial.setPlayerId(stringJoiner1.toString());
//													StringJoiner stringJoiner2 = new StringJoiner(",", "", "");
//													jsonNodeData.withArray("intervention_agents").forEach(interventionAgent -> {
//														stringJoiner2.add(interventionAgent.asText());
//													});
//													measuresTrial.setAdvisor(stringJoiner2.toString());
													teamId = jsonNodeData.get("team_id").asText();
													trialId = jsonNodeMsg.get("trial_id").asText();
//													measuresTrial.setTeamId(teamId);
//													measuresTrial.setDate(jsonNodeData.get("date").asText());
												}
											}
											if (sub_type.equals("stop")) {
												if (jsonNodeData != null) {
//													measuresTrial.setComplete("true");
//													measuresTrial.setCompleteReason(jsonNodeData.get("trial_end_condition").asText());
												}
											}
										}
										case "event" -> {
											if (sub_type.equals("Event:MissionState")) {
												if (jsonNodeData != null) {
													String missionState = jsonNodeData.get("mission_state").asText();
													if (missionState.equals("Stop")) {
//														measuresTrial.setDuration(jsonNodeData.get("mission_timer").asText());
//														measuresTrial.setCompleteReason(jsonNodeData.get("state_change_outcome").asText());
													}
												}
											} else if(sub_type.equals("Event:MissionStageTransition")) {
												if (jsonNodeData != null) {
													String missionStage = jsonNodeData.get("mission_stage").asText();
													if (missionStage.equals("RECON_STAGE")) {
														preReconMessageEncountered = true;
													} else if (missionStage.equals("SHOP_STAGE")) {
														shopStageCount += 1;
														int transitionsToShop = jsonNodeData.get("transitionsToShop").asInt();
														if (transitionsToShop > 1) {
															long shopTransitionTimestamp = jsonNodeData.get("elapsed_milliseconds").asLong();
															if (shopTransitionTimestamp > 1) {
																waitTimes.add(shopTransitionTimestamp - firstVoteTimestamp);
																firstVoteTimestamp = -1;
															}
														}
													} else if (missionStage.equals("FIELD_STAGE")) {
														fieldStageCount += 1;
													}
												}
											}
										}
										case "agent" -> {
											if (sub_type.equals("measures")) {
												if (jsonNodeData != null) {
													JsonNode measureDataJsonNode = jsonNodeData.get("measure_data");
													if (measureDataJsonNode != null) {
														if (measureDataJsonNode.isArray()) {
															measureDataJsonNode = measureDataJsonNode.get(0); // Should only be one of these.
														}
														String measureValue = measureDataJsonNode.get("measure_value").asText();
														String measureId = measureDataJsonNode.get("measure_id").asText();
														measuresTrial.setMeasures(measureId, measureValue);
														//logger.info("Found measure {}: [{}].", measureId, measureValue);
													}
												}
											} else if (sub_type.equals("Intervention:Chat")) {
												if (topic.matches("^agent/intervention/.*/chat$")) {
													if (jsonNodeData != null) {
														MeasuresIntervention measuresIntervention = new MeasuresIntervention();
														String interventionId = jsonNodeData.get("id").asText();
														String content = jsonNodeData.get("content").asText();
														String agent = jsonNodeMsg.get("source").asText();
														String timestamp = jsonNodeData.get("created").asText();
														StringJoiner stringJoiner1 = new StringJoiner(",", "", "");
														jsonNodeData.withArray("receivers").forEach(receiver -> {
															stringJoiner1.add(receiver.asText());
														});
														String interventionType1 = "";
														if (jsonNodeData.get("explanation") != null) {
															if (jsonNodeData.get("explanation").get("info") != null) {
																if (jsonNodeData.get("explanation").get("info").get("intervention_class") != null) {
																	interventionType1 = jsonNodeData.get("explanation").get("info").get("intervention_class").asText();
																}
															}
														}

														measuresIntervention.setInterventionId(interventionId);
														measuresIntervention.setContent(content);
														measuresIntervention.setAgent(agent);
														measuresIntervention.setTimestamp(timestamp);
														measuresIntervention.setTrialId(trialId);
														measuresIntervention.setTeamId(teamId);
														measuresIntervention.setPlayerId(stringJoiner1.toString());
														measuresInterventions.add(measuresIntervention);
													}
												}
											} else if (sub_type.equals("Event:InterventionChat")) {
												if (topic.matches("^agent/intervention/.*/chat$")) {
													if (jsonNodeData != null) {
														MeasuresIntervention measuresIntervention = new MeasuresIntervention();
														String interventionId = jsonNodeData.get("id").asText();
														String content = jsonNodeData.get("content").asText();
														String agent = jsonNodeMsg.get("source").asText();
														String timestamp = jsonNodeData.get("created").asText();
														StringJoiner stringJoiner1 = new StringJoiner(",", "", "");
														jsonNodeData.withArray("receivers").forEach(receiver -> {
															stringJoiner1.add(receiver.asText());
														});
														String interventionType1 = "";
														if (jsonNodeData.get("explanation") != null) {
															if (jsonNodeData.get("explanation").get("info") != null) {
																if (jsonNodeData.get("explanation").get("info").get("intervention_class") != null) {
																	interventionType1 = jsonNodeData.get("explanation").get("info").get("intervention_class").asText();
																}
															}
														}

														measuresIntervention.setInterventionId(interventionId);
														measuresIntervention.setContent(content);
														measuresIntervention.setAgent(agent);
														measuresIntervention.setTimestamp(timestamp);
														measuresIntervention.setTrialId(trialId);
														measuresIntervention.setTeamId(teamId);
														measuresIntervention.setPlayerId(stringJoiner1.toString());
														measuresInterventions.add(measuresIntervention);
													}
												}
											}
										}
//										case "chat" -> {
//											if (sub_type.equals("Event:Chat")) {
//												if (jsonNodeData != null) {
//													MeasuresChat measuresChat = new MeasuresChat();
//													String sender = jsonNodeData.get("sender").asText();
//													String messageContent = jsonNodeData.get("text").asText();
//													String elapsedMilliseconds = jsonNodeData.get("elapsed_milliseconds").asText();
//													String missionTimer = jsonNodeData.get("mission_timer").asText();
//													String timestamp = jsonNodeHeader.get("timestamp").asText();
//													StringJoiner stringJoiner1 = new StringJoiner(",", "", "");
//													jsonNodeData.withArray("addressees").forEach(addressee -> {
//														stringJoiner1.add(addressee.asText());
//													});
//													measuresChat.setSender(sender);
//													measuresChat.setMessageContent(messageContent);
//													measuresChat.setElapsedMilliseconds(elapsedMilliseconds);
//													measuresChat.setMissionTimer(missionTimer);
//													measuresChat.setTimestamp(timestamp);
//													measuresChat.setTrialId(trialId);
//													measuresChat.setTeamId(teamId);
//													measuresChat.setReceivers(stringJoiner1.toString());
//													measuresChats.add(measuresChat);
//												}
//											}
//										}
										case "metadata" -> {
											if (sub_type.equals("Event:PlayerDataAggregated")) {
												if (jsonNodeData != null) {
													JsonNode preTrialSurveysDataJsonNode = jsonNodeData.get("PreTrialSurveys");
													TrialSurveys preTrialSurveys = objectMapper.treeToValue(preTrialSurveysDataJsonNode, TrialSurveys.class);
													if (preTrialSurveysDataJsonNode != null) {
														String participantId = preTrialSurveysDataJsonNode.get("participant_id").asText();
														if (!measuresIndividuals.contains(new MeasuresIndividual(participantId))) {
															measuresIndividuals.add(new MeasuresIndividual(participantId));
														}
														MeasuresIndividual measuresIndividual = measuresIndividuals.stream().filter(p -> p.getPlayerId().equals(participantId)).findAny().orElse(new MeasuresIndividual(participantId));
														measuresIndividual.setPreTrialSurveys(preTrialSurveys);
													}
												}
											}
										}
										case "simulator_event" -> {
											if (sub_type.equals("Event:SurveyResponse")) {
												if (jsonNodeData != null) {
//												JsonNode postTrialSurveysDataJsonNode = jsonNodeData.get("surveys");
													TrialSurveys postTrialSurveys = objectMapper.treeToValue(jsonNodeData, TrialSurveys.class);
													String participantId = jsonNodeData.get("participant_id").asText();
													if (!measuresIndividuals.contains(new MeasuresIndividual(participantId))) {
														measuresIndividuals.add(new MeasuresIndividual(participantId));
													}
													MeasuresIndividual measuresIndividual = measuresIndividuals.stream().filter(p -> p.getPlayerId().equals(participantId)).findAny().orElse(new MeasuresIndividual(participantId));
													measuresIndividual.setPostTrialSurveys(postTrialSurveys);
												}
											}
											else if (sub_type.equals("Event:CommunicationChat")) {
												if (jsonNodeData != null) {
													MeasuresChat measuresChat = new MeasuresChat();
													String messageContent = jsonNodeData.get("message").asText();
													String senderId = jsonNodeData.get("sender_id").asText();
													String timestamp = jsonNodeHeader.get("timestamp").asText();
													String messageId = jsonNodeData.get("message_id").asText();
													String environment = jsonNodeData.has("environment") ? jsonNodeData.get("environment").asText() : "";
													StringJoiner stringJoiner1 = new StringJoiner(",", "", "");
													jsonNodeData.withArray("recipients").forEach(addressee -> {
														stringJoiner1.add(addressee.asText());
													});
													measuresChat.setSenderId(senderId);
													measuresChat.setMessageContent(messageContent);
													measuresChat.setTimestamp(timestamp);
													measuresChat.setTrialId(trialId);
													measuresChat.setTeamId(teamId);
													measuresChat.setRecipients(stringJoiner1.toString());
													measuresChat.setMessageId(messageId);
													measuresChat.setEnvironment(environment);
													measuresChats.add(measuresChat);
												}
											}
											else if(sub_type.equals("Event:PlayerState")) {
												if (jsonNodeData != null) {
													int x = jsonNodeData.get("x").asInt();
													int y = jsonNodeData.get("y").asInt();
													int z = jsonNodeData.get("z").asInt();
													String participantId = jsonNodeData.get("participant_id").asText();
													if (x > 1 && x < 50) {
														if (z > 1 && z < 50) { // JUNGLE
															if (y >= 53) {
																autoJumpList.add(participantId);
															}
														} else if (z > 100 && z < 150) { // DESERT
															if (y >= 55) {
																autoJumpList.add(participantId);
															}
														} else if (z > 50 && z < 100) { // VILLAGE
															if (x < 18 || x >37) {
																if (z < 69 || z > 82) {
																	if (y >= 55) {
																		autoJumpList.add(participantId);
																	}
																}
															}
														}
													}
												}
											} else if(sub_type.equals("Event:UIClick")) {
												if (jsonNodeData != null) {
													String participantId = jsonNodeData.get("participant_id").asText();
													if (jsonNodeData.get("additional_info") != null) {
														String metaAction = jsonNodeData.get("additional_info").get("meta_action").asText();
														if (metaAction.equals("PROPOSE_TOOL_PURCHASE_+")) {
															if (!preReconMessageEncountered) {
																preReconPurchase.add(participantId);
															}
														}
													}
												}
											} else if(sub_type.equals("Event:InventoryUpdate")) {
												if (jsonNodeData != null) {
													if (jsonNodeData.get("currInv") != null) {
														jsonNodeData.get("currInv").fields().forEachRemaining((field) -> {
															String participantId = field.getKey();
															JsonNode participantInvNode = field.getValue();
															int rwc = participantInvNode.has("WIRECUTTERS_RED") ? participantInvNode.get("WIRECUTTERS_RED").asInt() : 0;
															int gwc = participantInvNode.has("WIRECUTTERS_GREEN") ? participantInvNode.get("WIRECUTTERS_GREEN").asInt() : 0;
															int bwc = participantInvNode.has("WIRECUTTERS_BLUE") ? participantInvNode.get("WIRECUTTERS_BLUE").asInt() : 0;
															if (rwc > 0 && gwc > 0 && bwc > 0) {
																wirecutterInventory.add(participantId);
															}
														});
													}
												}
											} else if(sub_type.equals("Event:ObjectStateChange")) {
												if (jsonNodeData != null) {
													String bombId = jsonNodeData.get("id").asText();
													if (!threePhaseBombs.containsKey(bombId)) {
														threePhaseBombs.put(bombId, new HashMap<>());
													}
													if (jsonNodeData.has("currAttributes")) {
														JsonNode jsonNodeCurrAttributes = jsonNodeData.get("currAttributes");
														if (jsonNodeCurrAttributes.has("sequence_index")) {
															int sequenceIndex = jsonNodeCurrAttributes.get("sequence_index").asInt();
															if (sequenceIndex == 0) {
																String sequence = jsonNodeCurrAttributes.get("sequence").asText();
																if (threePhaseBombs.get(bombId).keySet().isEmpty()) {
																	sequence.chars().forEachOrdered(phase -> {
																		threePhaseBombs.get(bombId).put(String.valueOf((char) phase), "");
																	});
																}
															}
														}
														String outcome = jsonNodeCurrAttributes.get("outcome").asText();
														if (outcome.equals("DEFUSED_DISPOSER")) {
															String sequence = jsonNodeCurrAttributes.get("sequence").asText();
															sequence.chars().forEachOrdered(phase -> {
																if (threePhaseBombs.get(bombId).get(String.valueOf((char) phase)).isEmpty()) {
																	String triggeringEntity = jsonNodeData.get("triggering_entity").asText();
																	threePhaseBombs.get(bombId).put(String.valueOf((char) phase), triggeringEntity);
																}
															});
														}
													}
													if (jsonNodeData.has("changedAttributes")) {
														JsonNode jsonNodeChangedAttributes = jsonNodeData.get("changedAttributes");
														if (jsonNodeChangedAttributes.has("sequence")) {
															if (jsonNodeChangedAttributes.get("sequence").isArray()) {
																String sequence = jsonNodeChangedAttributes.get("sequence").get(0).asText();
																if (threePhaseBombs.get(bombId).keySet().isEmpty()) {
																	sequence.chars().forEachOrdered(phase -> {
																		threePhaseBombs.get(bombId).put(String.valueOf((char) phase), "");
																	});
																}
																String triggeringEntity = jsonNodeData.get("triggering_entity").asText();
																threePhaseBombs.get(bombId).put(String.valueOf(sequence.charAt(0)), triggeringEntity);
															}
														}
													}
												}
											} else if (sub_type.equals("Event:CommunicationEnvironment")) {
												if (jsonNodeData != null) {
													String message = jsonNodeData.get("message").asText();
													if (message.contains("voted to go to the Shop!")) {
														if (firstVoteTimestamp == -1) {
                                                            firstVoteTimestamp = jsonNodeData.get("elapsed_milliseconds").asLong();
														}
													}
												}
											}
										}
										case "control" -> {
											if (sub_type.equals("Event:TrialSummary")) {
												trialSummaryFound = true;
												if (jsonNodeData != null) {
													// Grab the members so we can initialize correct players.
													SummaryTrial summaryTrial = objectMapper.treeToValue(jsonNodeData, SummaryTrial.class);
													//measuresTrial.setSummary(summaryTrial);
													if (summaryTrial.getMembers() != null) {
														// Create an existing object using the members.
														SummaryTrial existingSummaryTrial = new SummaryTrial(summaryTrial.getMembers());
														// Merge data into existing object.
														ObjectReader objectReader = objectMapper.readerForUpdating(existingSummaryTrial);
														SummaryTrial updatedSummaryTrial = objectReader.readValue(jsonNodeData);
														measuresTrial.setSummary(updatedSummaryTrial);
													} // else {
//
//													}
												}
											}
										}
									}
								} else {
									logger.error("Invalid document detected: {}", line);
								}
							} catch (Exception e) {
								logger.error(e.toString());
								logger.error(line);
//								e.printStackTrace();
							}
						}
						if (!trialSummaryFound) {
							if (trialStopOptions != null) {
								SummaryTrial summaryTrial = trialStopOptions.getSummaryTrial();
								if (summaryTrial.getMembers() != null) {
									// Create an existing object using the members.
									SummaryTrial existingSummaryTrial = new SummaryTrial(summaryTrial.getMembers());
									// Merge data into existing object.
									ObjectReader objectReader = objectMapper.readerForUpdating(existingSummaryTrial);
									SummaryTrial updatedSummaryTrial = trialStopOptions.getSummaryTrial();
									measuresTrial.setSummary(updatedSummaryTrial);
								}
							}
						}
						if (trialStopOptions != null) {
							measuresTrial.setPlayer1Tenure(trialStopOptions.getTenure().get(measuresTrial.getPlayer1()));
							measuresTrial.setPlayer2Tenure(trialStopOptions.getTenure().get(measuresTrial.getPlayer2()));
							measuresTrial.setPlayer3Tenure(trialStopOptions.getTenure().get(measuresTrial.getPlayer3()));
							measuresTrial.setTeamTenure(trialStopOptions.getTeamTenure().get(measuresTrial.getTeamId()));
							measuresTrial.setCorrectedTeamScore(trialStopOptions.getCorrectedTeamScore());
						}
						// Fix issues when field or store counts might not be zero
						if (trialStopOptions.getSummaryTrial().getNumFieldVisits() == 0) {
							measuresTrial.setNumFieldVisits(fieldStageCount);
						}
						if (trialStopOptions.getSummaryTrial().getNumStoreVisits() == 0) {
							measuresTrial.setNumStoreVisits(shopStageCount);
						}
						// Cheat detection
						autoJumpList.forEach(playerId -> {
							measuresTrial.addAutoJump(playerId);
						});
						preReconPurchase.forEach(playerId -> {
							measuresTrial.addPreReconPurchase(playerId);
						});
						wirecutterInventory.forEach(playerId -> {
							measuresTrial.addWirecutterInventory(playerId);
						});
						threePhaseBombs.forEach((bombId, phases) -> {
							if (phases.size() == 3) {
								Set<String> playerIds = new HashSet<>();
								phases.forEach((phase, playerId) -> {
									if (!playerId.isEmpty()) {
										playerIds.add(playerId);
									}
								});
								if (playerIds.size() == 1) {
									playerIds.forEach(playerId -> {
										measuresTrial.addThreePhaseBombs(playerId);
									});
								}
							}
						});
						Set<String> soloDisposers = new HashSet<>();
						Set<String> nonSoloDisposersSet = new HashSet<>();
						threePhaseBombs.forEach((bombId, phases) -> {
							Set<String> playerIds = new HashSet<>();
							AtomicBoolean noNulls = new AtomicBoolean(true);
							phases.forEach((phase, playerId) -> {
								if (!playerId.isEmpty()) {
									playerIds.add(playerId);
									soloDisposers.add(playerId);
								} else {
									noNulls.set(false);
                                }
							});
							if (noNulls.get()) {
								if (!phases.values().isEmpty()) {
									if (playerIds.size() > 1) {
										nonSoloDisposersSet.addAll(playerIds);
									}
								}
							}
						});
						soloDisposers.removeAll(nonSoloDisposersSet);
						measuresTrial.setSoloDisposers(String.join(", ", soloDisposers));
						if (!waitTimes.isEmpty()) {
							long sumWaitTime = 0;
							for (long waitTime : waitTimes) {
								sumWaitTime += waitTime;
							}
							long averageWaitTime = sumWaitTime / waitTimes.size();
							measuresTrial.setAverageWaitTime(averageWaitTime / 1000); // Convert milliseconds to seconds.
						}

						//logger.info(objectMapper.writeValueAsString(threePhaseBombs));
						//logger.info(objectMapper.writeValueAsString(soloDisposers));

						measuresTrials.add(measuresTrial);
						bufferedReader.close();

						long endTime = System.nanoTime();
						long duration = (endTime - startTime);
						logger.info("Computed counts for [{}] messages in [{}] seconds.", lineCount,
								TimeUnit.SECONDS.convert(duration, TimeUnit.NANOSECONDS));
//					logger.debug(objectMapper.writeValueAsString(messageCountMap));

						// Intervention Measures
						CustomMappingStrategy<MeasuresIntervention> beanStrategyMeasuresIntervention = new CustomMappingStrategy<>();
						beanStrategyMeasuresIntervention.setType(MeasuresIntervention.class);

						ByteArrayOutputStream byteArrayOutputStreamMeasuresIntervention = new ByteArrayOutputStream();
						OutputStreamWriter outputStreamWriterMeasuresIntervention = new OutputStreamWriter(byteArrayOutputStreamMeasuresIntervention);
						CSVWriter csvWriterMeasuresIntervention = new CSVWriter(outputStreamWriterMeasuresIntervention);

						StatefulBeanToCsv<MeasuresIntervention> beanToCsvInterventionMeasures = new StatefulBeanToCsvBuilder<MeasuresIntervention>(csvWriterMeasuresIntervention)
								.withMappingStrategy(beanStrategyMeasuresIntervention)
								.build();
						beanToCsvInterventionMeasures.write(measuresInterventions);
						outputStreamWriterMeasuresIntervention.flush();
						byte[] measuresInterventionBytes = byteArrayOutputStreamMeasuresIntervention.toByteArray();
						csvWriterMeasuresIntervention.close();
						outputStreamWriterMeasuresIntervention.close();
						byteArrayOutputStreamMeasuresIntervention.close();
						logger.info("Intervention Measures file generated.");

						logger.info("Adding Intervention Measures to zip.");
						String interventionMeasuresFilename = "intervention_measures.csv";
						ZipEntry zipEntry3 = new ZipEntry(interventionMeasuresFilename);
						zipEntry3.setSize(measuresInterventionBytes.length);
						zos.putNextEntry(zipEntry3);
						zos.write(measuresInterventionBytes);
						zos.closeEntry();
						logger.info("Intervention Measures: {} added.", interventionMeasuresFilename);

						// Individual Measures
						CustomMappingStrategy<MeasuresIndividual> beanStrategyMeasuresIndividual = new CustomMappingStrategy<>();
						beanStrategyMeasuresIndividual.setType(MeasuresIndividual.class);

						ByteArrayOutputStream byteArrayOutputStreamMeasuresIndividual = new ByteArrayOutputStream();
						OutputStreamWriter outputStreamWriterMeasuresIndividual = new OutputStreamWriter(byteArrayOutputStreamMeasuresIndividual);
						CSVWriter csvWriterMeasuresIndividual = new CSVWriter(outputStreamWriterMeasuresIndividual);

						StatefulBeanToCsv<MeasuresIndividual> beanToCsvIndividualMeasures = new StatefulBeanToCsvBuilder<MeasuresIndividual>(csvWriterMeasuresIndividual)
								.withMappingStrategy(beanStrategyMeasuresIndividual)
								.build();
						beanToCsvIndividualMeasures.write(measuresIndividuals);
						outputStreamWriterMeasuresIndividual.flush();
						byte[] measuresIndividualBytes = byteArrayOutputStreamMeasuresIndividual.toByteArray();
						csvWriterMeasuresIndividual.close();
						outputStreamWriterMeasuresIndividual.close();
						byteArrayOutputStreamMeasuresIndividual.close();
						logger.info("Individual Measures file generated.");

						logger.info("Adding Individual Measures to zip.");
						String individualMeasuresFilename = "individual_measures.csv";
						ZipEntry zipEntry4 = new ZipEntry(individualMeasuresFilename);
						zipEntry4.setSize(measuresIndividualBytes.length);
						zos.putNextEntry(zipEntry4);
						zos.write(measuresIndividualBytes);
						zos.closeEntry();
						logger.info("Individual Measures: {} added.", individualMeasuresFilename);

						// Trial Measures
						CustomMappingStrategy<MeasuresTrial> beanStrategyMeasuresTrial = new CustomMappingStrategy<>();
						beanStrategyMeasuresTrial.setType(MeasuresTrial.class);

						ByteArrayOutputStream byteArrayOutputStreamMeasuresTrial = new ByteArrayOutputStream();
						OutputStreamWriter outputStreamWriterMeasuresTrial = new OutputStreamWriter(byteArrayOutputStreamMeasuresTrial);
						CSVWriter csvWriterMeasuresTrial = new CSVWriter(outputStreamWriterMeasuresTrial);

						StatefulBeanToCsv<MeasuresTrial> beanToCsvTrialMeasures = new StatefulBeanToCsvBuilder<MeasuresTrial>(csvWriterMeasuresTrial)
								.withMappingStrategy(beanStrategyMeasuresTrial)
								.build();
						beanToCsvTrialMeasures.write(measuresTrials);
						outputStreamWriterMeasuresTrial.flush();
						byte[] measuresTrialBytes = byteArrayOutputStreamMeasuresTrial.toByteArray();
						csvWriterMeasuresTrial.close();
						outputStreamWriterMeasuresTrial.close();
						byteArrayOutputStreamMeasuresTrial.close();
						logger.info("Trial Measures file generated.");

						logger.info("Adding Trial Measures to zip.");
						String trialMeasuresFilename = "trial_measures.csv";
						ZipEntry zipEntry5 = new ZipEntry(trialMeasuresFilename);
						zipEntry5.setSize(measuresTrialBytes.length);
						zos.putNextEntry(zipEntry5);
						zos.write(measuresTrialBytes);
						zos.closeEntry();
						logger.info("Trial Measures: {} added.", trialMeasuresFilename);

						// Chat Measures
						CustomMappingStrategy<MeasuresChat> beanStrategyMeasuresChat = new CustomMappingStrategy<>();
						beanStrategyMeasuresChat.setType(MeasuresChat.class);

						ByteArrayOutputStream byteArrayOutputStreamMeasuresChat = new ByteArrayOutputStream();
						OutputStreamWriter outputStreamWriterMeasuresChat = new OutputStreamWriter(byteArrayOutputStreamMeasuresChat);
						CSVWriter csvWriterMeasuresChat = new CSVWriter(outputStreamWriterMeasuresChat);

						StatefulBeanToCsv<MeasuresChat> beanToCsvTrialChats = new StatefulBeanToCsvBuilder<MeasuresChat>(csvWriterMeasuresChat)
								.withMappingStrategy(beanStrategyMeasuresChat)
								.build();
						beanToCsvTrialChats.write(measuresChats);
						outputStreamWriterMeasuresChat.flush();
						byte[] measuresChatBytes = byteArrayOutputStreamMeasuresChat.toByteArray();
						csvWriterMeasuresChat.close();
						outputStreamWriterMeasuresChat.close();
						byteArrayOutputStreamMeasuresChat.close();
						logger.info("Chat Measures file generated.");

						logger.info("Adding Chat Measures to zip.");
						String chatMeasuresFilename = "chat_measures.csv";
						ZipEntry zipEntry6 = new ZipEntry(chatMeasuresFilename);
						zipEntry6.setSize(measuresChatBytes.length);
						zos.putNextEntry(zipEntry6);
						zos.write(measuresChatBytes);
						zos.closeEntry();
						logger.info("Chat Measures: {} added.", chatMeasuresFilename);
					} catch (Exception e) {
						logger.error(e.getMessage());
						e.printStackTrace();
					}
				}
				if (_METADATA_APP_TRIAL_STOP_AGENT_TESTS) {
					PluginManager pluginManager = null;
					try {
						long startTime = System.nanoTime();
						logger.info("Starting PluginManager [{}].", Paths.get(System.getProperty("user.dir"), "plugins"));
//						pluginManager = new AgentTestPluginManager();
						pluginManager = new AgentTestPluginManager(Paths.get(System.getProperty("user.dir"), "plugins"));
						logger.info("Loading plugins.");
						pluginManager.loadPlugins();
						logger.info("Starting plugins.");
						pluginManager.startPlugins();
						pluginManager.getPlugins().forEach(plugin -> {
							logger.debug("Plugin id: {}", plugin.getPluginId());
						});
						List<AgentTest> agentTests = pluginManager.getExtensions(AgentTest.class);
						logger.info("Found {} extensions for extension point '{}'", agentTests.size(), AgentTest.class.getName());
						int lineCount = 0;
						if (!agentTests.isEmpty()) {
							BufferedReader bufferedReader =	new BufferedReader(new FileReader(sourcePathMetadata.toString()));
							while (bufferedReader.ready()) {
								String line = bufferedReader.readLine();
								for (AgentTest agentTest : agentTests) {
									try {
										agentTest.evaluate(line);
									} catch (Exception e) {
										logger.error("{} exception: {}", agentTest.getAgentName(), e.getMessage());
									}
								}
								lineCount++;
							}
							bufferedReader.close();
							List<AgentTestResultRow> agentTestResultRows = new ArrayList<>();
							for (AgentTest agentTest : agentTests) {
								List<AgentTestResultRow> results = agentTest.getResults();
								agentTestResultRows.addAll(results);
//								results.forEach(result -> {
//									logger.info(result.toString());
//								});
							}
							long endTime = System.nanoTime();
							long duration = (endTime - startTime);
							logger.info("Finished agent testing for [{}] messages in [{}] seconds.", lineCount,
									TimeUnit.SECONDS.convert(duration, TimeUnit.NANOSECONDS));

							CustomMappingStrategy<AgentTestResultRow> beanStrategyAgentTestResultRow = new CustomMappingStrategy<>();
							beanStrategyAgentTestResultRow.setType(AgentTestResultRow.class);

							ByteArrayOutputStream byteArrayOutputStreamAgentTestResultRow = new ByteArrayOutputStream();
							OutputStreamWriter outputStreamWriterAgentTestResultRow = new OutputStreamWriter(byteArrayOutputStreamAgentTestResultRow);
							CSVWriter csvWriterAgentTestResultRow = new CSVWriter(outputStreamWriterAgentTestResultRow);

							StatefulBeanToCsv<AgentTestResultRow> beanToCsvTrialMeasures = new StatefulBeanToCsvBuilder<AgentTestResultRow>(csvWriterAgentTestResultRow)
									.withMappingStrategy(beanStrategyAgentTestResultRow)
									.build();
							beanToCsvTrialMeasures.write(agentTestResultRows);
							outputStreamWriterAgentTestResultRow.flush();
							byte[] measuresTrialBytes = byteArrayOutputStreamAgentTestResultRow.toByteArray();
							csvWriterAgentTestResultRow.close();
							outputStreamWriterAgentTestResultRow.close();
							byteArrayOutputStreamAgentTestResultRow.close();
//							logger.info("Agent Tests file generated.");
							logger.info("Adding Agent Tests to zip.");
							String agentTestsFilename = "agent_tests.csv";
							ZipEntry zipEntry7 = new ZipEntry(agentTestsFilename);
							zipEntry7.setSize(measuresTrialBytes.length);
							zos.putNextEntry(zipEntry7);
							zos.write(measuresTrialBytes);
							zos.closeEntry();
							logger.info("Trial Measures: {} added.", agentTestsFilename);
						} else {
							logger.info("No agent testing plugins detected.");
						}
					} catch (FileNotFoundException e) {
						throw new RuntimeException(e);
					} finally {
						if (pluginManager != null) {
							pluginManager.stopPlugins();
						}
					}
				}
				if (_METADATA_APP_TRIAL_STOP_DOCKER_LOGS) {
					long startTime = System.nanoTime();
					List<Container> containers = defaultDockerService.containerList();
					for (int i = 0; i < containers.size(); i++) {
						Container container = containers.get(i);
						String containerId = container.getId();
						String containerName = container.getNames()[0].startsWith("/") ? container.getNames()[0].substring(1) : container.getNames()[0];
						logger.info("Exporting docker container log {} [{}].", containerName, containerId);
						SystemFile systemFileDockerLogs = defaultDockerService.containerLogDownload(containerId);

						if (systemFileDockerLogs == null) {
							throw new IOException("Could not create file due to trial or replay id!");
						}

						File sourceFileDockerLogs = systemFileDockerLogs.getFile();
						Path sourcePathDockerLogs = sourceFileDockerLogs.toPath();

						logger.debug("Docker container log source path: {}", sourcePathDockerLogs.toString());

						logger.info("Adding file: {} to zip.", sourcePathDockerLogs.toString());
						ZipEntry zipEntry3 = new ZipEntry(MessageFormat.format("logs/docker/{0}/{1}", containerName, sourceFileDockerLogs.getName()));
						zos.putNextEntry(zipEntry3);

						FileInputStream fis3 = new FileInputStream(sourceFileDockerLogs);
						byte[] buffer3 = new byte[1024];
						int len3;
						while ((len3 = fis3.read(buffer3)) > 0) {
							zos.write(buffer3, 0, len3);
						}
						fis3.close();
						zos.closeEntry();
						logger.info("File: {} added.", sourcePathDockerLogs.toString());
					}
					logger.info("Docker log files exported successfully.");
					long endTime = System.nanoTime();
					long duration = (endTime - startTime);
					logger.info("Exported [{}] docker container logs in [{}] seconds.", containers.size(),
							TimeUnit.SECONDS.convert(duration, TimeUnit.NANOSECONDS));
				}
				if (_METADATA_APP_TRIAL_STOP_AGENT_LOGS) {
					logger.info("Exporting agent logs.");
					Optional<InputStream> resource = resourceLoader.getResourceAsStream(AGENT_LOGS_FILE);
					InputStream inputStream = resource.get();
					List<AgentLog> agentLogs = objectMapper.readValue(inputStream, new TypeReference<>() {});
					logger.trace("Agent Logs File: {}", objectMapper.writeValueAsString(agentLogs));
					agentLogs.forEach(agentLog -> {
						try {
							Path sourceFolderPath = Path.of(agentLog.getFolder());
							try (DirectoryStream<Path> ds = Files.newDirectoryStream(sourceFolderPath, agentLog.getWildcardDirFilter())) {
								for (Path p : ds) {
									Files.walkFileTree(p, new SimpleFileVisitor<Path>() {
										public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
											try {
												zos.putNextEntry(new ZipEntry(MessageFormat.format("logs/agents/{0}/{1}", agentLog.getName(), sourceFolderPath.relativize(file).toString())));
												Files.copy(file, zos);
												zos.closeEntry();
												logger.info("File: {} added.", file.toString());
											} catch (Exception e) {
												e.printStackTrace();
												logger.error(e.getMessage());
											}
											return FileVisitResult.CONTINUE;
										}
									});
								}
							} catch (Exception e) {
								e.printStackTrace();
								logger.error(e.getMessage());
							}
						} catch (Exception e) {
//							throw new RuntimeException(e);
							e.printStackTrace();
							logger.error(e.getMessage());
						}
					});
				}
				zos.close();
				logger.info("Zip file created.");

				if (_METADATA_APP_TRIAL_STOP_UPLOAD) {
					logger.info("Sending upload event to listeners for {}", zipFilePath.getFileName());
					trialStopUploadEventEmitterBean.publishTrialStopUploadEvent(zipFilePath, id);
				}

				if (trialStopOptions == null) {
					autoExportPublisher.send(objectMapper.writeValueAsBytes(createAutoExportMessage(msgTrial, AutoExportStateType.end))).subscribe(() -> {
						// handle completion
					}, throwable -> {
						// handle error
					});
				}

				sourcePathMetadata.toFile().deleteOnExit();

			} catch (Exception e) {
				e.printStackTrace();
				logger.error(e.getMessage());
				try {
					if (trialStopOptions == null) {
						autoExportPublisher.send(objectMapper.writeValueAsBytes(createAutoExportMessage(msgTrial, AutoExportStateType.error))).subscribe(() -> {
							// handle completion
						}, throwable -> {
							// handle error
						});
					}
				} catch (JsonProcessingException ex) {
					e.printStackTrace();
					logger.error(ex.getMessage());
				}
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		} else {
			try {
				if (trialStopOptions == null) {
					autoExportPublisher.send(objectMapper.writeValueAsBytes(createAutoExportMessage(msgTrial, AutoExportStateType.error))).subscribe(() -> {
						// handle completion
					}, throwable -> {
						// handle error
					});
				}
			} catch (JsonProcessingException e) {
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
	}

	private boolean isUUID(String value) {
		if (value != null) {
			try {
				UUID.fromString(value.toString());
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		return false;
	}

	private Object createAutoExportMessage(MessageTrial messageTrial, AutoExportStateType autoExportStateType) {
		String id = messageTrial.getMsg().getTrialId();
		if (isUUID(messageTrial.getMsg().getReplayId())) {
			id = messageTrial.getMsg().getReplayId();
		}
		Header header = new Header(Instant.now().toString(), "Export", "1.0");
		Msg msg = new Msg("Auto", "metadata-app", messageTrial.getMsg().getExperimentId(), id, "1.0", null, null, null);
		DataAutoExport dataAutoExport = new DataAutoExport(autoExportStateType);
		return new MessageAutoExport(header, msg, dataAutoExport);
	}

	private boolean isFirstLookValid(FirstLook firstLook, Integer count) {
		if (count == null) {
			return false;
		}
		switch (firstLook.getComparison().getOperator()) {
		case ">=":
			return firstLook.getComparison().getValue() >= count;
		case "==":
			return firstLook.getComparison().getValue() == count;
		default:
			return false;
		}
	}

	private String removeExtension(String fileName) {
		int lastIndex = fileName.lastIndexOf('.');
		if (lastIndex != -1) {
			fileName = fileName.substring(0, lastIndex);
		}
		return fileName;
	}
}
