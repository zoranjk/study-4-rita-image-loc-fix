package metadata.app.event;

import metadata.app.model.MessageTrial;

public class TrialStopEvent {
	
	private final MessageTrial msgTrial;
	
	public TrialStopEvent(MessageTrial msgTrial) {
		this.msgTrial = msgTrial;
	}
	
    public MessageTrial getMsgTrial() {
        return msgTrial;
    }

}
