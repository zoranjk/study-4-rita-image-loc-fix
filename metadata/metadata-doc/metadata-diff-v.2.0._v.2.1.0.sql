-- Diff code generated with pgModeler (PostgreSQL Database Modeler)
-- pgModeler version: 1.0.1
-- Diff date: 2023-05-17 20:16:22
-- Source model: metadata
-- Database: metadata
-- PostgreSQL version: 13.0

-- [ Diff summary ]
-- Dropped objects: 0
-- Created objects: 1
-- Changed objects: 0

SET search_path=public,pg_catalog;
-- ddl-end --


-- [ Created objects ] --
-- object: condition | type: COLUMN --
-- ALTER TABLE public.experiments DROP COLUMN IF EXISTS condition CASCADE;
ALTER TABLE public.experiments ADD COLUMN condition text;
-- ddl-end --


