-- Diff code generated with pgModeler (PostgreSQL Database Modeler)
-- pgModeler version: 0.9.3
-- Diff date: 2023-01-20 14:52:25
-- Source model: metadata
-- Database: metadata
-- PostgreSQL version: 13.0

-- [ Diff summary ]
-- Dropped objects: 0
-- Created objects: 3
-- Changed objects: 0
-- Truncated tables: 0

SET search_path=public,pg_catalog;
-- ddl-end --


-- [ Created objects ] --
-- object: map_name | type: COLUMN --
-- ALTER TABLE public.trials DROP COLUMN IF EXISTS map_name CASCADE;
ALTER TABLE public.trials ADD COLUMN map_name text;
-- ddl-end --

COMMENT ON COLUMN public.trials.map_name IS E'The map name.';
-- ddl-end --


-- object: map_block_filename | type: COLUMN --
-- ALTER TABLE public.trials DROP COLUMN IF EXISTS map_block_filename CASCADE;
ALTER TABLE public.trials ADD COLUMN map_block_filename text;
-- ddl-end --

COMMENT ON COLUMN public.trials.map_block_filename IS E'The map block file to use during trial.';
-- ddl-end --


-- object: client_info | type: COLUMN --
-- ALTER TABLE public.trials DROP COLUMN IF EXISTS client_info CASCADE;
ALTER TABLE public.trials ADD COLUMN client_info json;
-- ddl-end --

COMMENT ON COLUMN public.trials.client_info IS E'Client info parameters assigned by experimenter.';
-- ddl-end --


