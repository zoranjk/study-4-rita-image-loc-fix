-- Diff code generated with pgModeler (PostgreSQL Database Modeler)
-- pgModeler version: 1.0.1
-- Diff date: 2023-05-19 18:49:05
-- Source model: metadata
-- Database: metadata
-- PostgreSQL version: 13.0

-- [ Diff summary ]
-- Dropped objects: 0
-- Created objects: 3
-- Changed objects: 1

SET search_path=public,pg_catalog;
-- ddl-end --


-- [ Created objects ] --
-- object: team_id | type: COLUMN --
-- ALTER TABLE public.trials DROP COLUMN IF EXISTS team_id CASCADE;
ALTER TABLE public.trials ADD COLUMN team_id text;
-- ddl-end --

COMMENT ON COLUMN public.trials.team_id IS E'The team id.';
-- ddl-end --


-- object: intervention_agents | type: COLUMN --
-- ALTER TABLE public.trials DROP COLUMN IF EXISTS intervention_agents CASCADE;
ALTER TABLE public.trials ADD COLUMN intervention_agents text[];
-- ddl-end --

COMMENT ON COLUMN public.trials.intervention_agents IS E'A list of intervention agents for the trial.';
-- ddl-end --


-- object: observers | type: COLUMN --
-- ALTER TABLE public.trials DROP COLUMN IF EXISTS observers CASCADE;
ALTER TABLE public.trials ADD COLUMN observers text[];
-- ddl-end --

COMMENT ON COLUMN public.trials.observers IS E'A list of observers for the trial.';
-- ddl-end --




-- [ Changed objects ] --
COMMENT ON COLUMN public.experiments.condition IS E'The condition for this experiment.';
-- ddl-end --
