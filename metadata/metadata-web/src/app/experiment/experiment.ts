export interface Experiment {
  id: number;
  experiment_id: string;
  name: string;
  date: string;
  author: string;
  mission: string;
  condition: string;
}
