import { Component, Inject, OnInit } from '@angular/core';
import { Experiment } from '../../experiment/experiment';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { ExperimentService } from '../../experiment/experiment.service';
import { v4 as uuidv4 } from 'uuid';
import { Trial } from '../trial';
import { ClientInfo } from '../client-info';

@Component({
  selector: 'app-update-trial',
  templateUrl: './update-trial.component.html',
  styleUrls: ['./update-trial.component.scss']
})
export class UpdateTrialComponent implements OnInit {
  experiments: Experiment[];
  client_info: ClientInfo[] = [];
  updateTrialForm = this.formBuilder.group({
    id: ['', Validators.required],
    trial_id: ['', Validators.required],
    name: ['', Validators.required],
    date: ['', Validators.required],
    experimenter: ['', Validators.required],
    subjects: ['', Validators.required],
    trial_number: ['', Validators.required],
    group_number: ['', Validators.required],
    study_number: ['', Validators.required],
    condition: ['', Validators.required],
    notes: [''],
    testbed_version: ['', Validators.required],
    map_name: ['', Validators.required],
    map_block_filename: ['', Validators.required],
    client_info_input: [''],
    team_id: ['', Validators.required],
    intervention_agents: [''],
    observers: [''],
    experiment: ['', Validators.required]
  });
  uuidPattern: RegExp = new RegExp(/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/);
  clientInfoPattern: RegExp = new RegExp(/.+ : .+/);

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<UpdateTrialComponent>,
    private experimentService: ExperimentService,
    @Inject(MAT_DIALOG_DATA) public data: Trial) {
  }

  readExperiments(): void {
    this.experimentService.readExperiments()
      .subscribe(experiments => {
        this.experiments = experiments;
      });
  }

  onUpdateClick(): void {
    const dialogResult = this.updateTrialForm.value;
    dialogResult.client_info = this.client_info;

    const subjectsText = this.updateTrialForm.get('subjects').value;
    if (subjectsText !== '') {
      dialogResult.subjects = subjectsText.split(/[\r\n]+/);
    } else {
      dialogResult.subjects = [];
    }

    const notesText = this.updateTrialForm.get('notes').value;
    if (notesText !== '') {
      dialogResult.notes = notesText.split(/[\r\n]+/);
    } else {
      dialogResult.notes = [];
    }

    const interventionAgentsText = this.updateTrialForm.get('intervention_agents').value;
    if (interventionAgentsText !== '') {
      dialogResult.intervention_agents = interventionAgentsText.split(/[\r\n]+/);
    } else {
      dialogResult.intervention_agents = [];
    }

    const observersText = this.updateTrialForm.get('observers').value;
    if (observersText !== '') {
      dialogResult.observers = observersText.split(/[\r\n]+/);
    } else {
      dialogResult.observers = [];
    }

    this.dialogRef.close(dialogResult);
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onGenerateUUIDClick(): void {
    this.updateTrialForm.patchValue({
      trial_id: uuidv4()
    });
  }

  ngOnInit(): void {
    this.readExperiments();

    this.client_info = this.data.client_info;
    this.updateTrialForm.setValue({
      id: this.data.id,
      trial_id: this.data.trial_id,
      name: this.data.name,
      date: this.data.date,
      experimenter: this.data.experimenter,
      subjects: this.data.subjects.length > 0 ? this.data.subjects.join('\r\n') : this.data.subjects,
      trial_number: this.data.trial_number,
      group_number: this.data.group_number,
      study_number: this.data.study_number,
      condition: this.data.condition,
      notes: this.data.notes.length > 0 ? this.data.notes.join('\r\n') : this.data.notes,
      testbed_version: this.data.testbed_version,
      map_name: this.data.map_name,
      map_block_filename: this.data.map_block_filename,
      client_info_input: '',
      team_id: '',
      intervention_agents: this.data.intervention_agents.length > 0 ? this.data.intervention_agents.join('\r\n') : this.data.intervention_agents,
      observers: this.data.observers.length > 0 ? this.data.observers.join('\r\n') : this.data.observers,
      experiment: this.data.experiment
    });
  }

  compareFnExperiments(e1: Experiment, e2: Experiment): boolean {
    return e1 && e2 ? e1.id === e2.id : e1 === e2;
  }

  onAddClientInfoItem(): void {
    if (this.updateTrialForm.controls.client_info_input.value !== '') {
      const types = this.updateTrialForm.controls.client_info_input.value.split(' : ');
      const clientInfoType = {
        callsign: types[0],
        participant_id: types[1]
      };
      this.client_info.push(clientInfoType);
      this.updateTrialForm.controls.client_info_input.setValue('');
    }
  }

  onRemoveClientInfoItem(item: any): void {
    const index: number = this.client_info.findIndex(i => i.callsign === item.callsign && i.participant_id === item.participant_id);
    if (index > -1) {
      this.client_info.splice(index, 1);
    }
  }
}
