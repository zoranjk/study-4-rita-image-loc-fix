import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Trial } from '../trial';
import { v4 as uuidv4 } from 'uuid';
import { Experiment } from '../../experiment/Experiment';
import { ExperimentService } from '../../experiment/experiment.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { LoggingService } from '../../logging/logging.service';
import { JsonTrialComponent } from '../json-trial/json-trial.component';
import { environment } from '../../../environments/environment';
import { ClientInfo } from '../client-info';
import { IgnoreListItem } from '../../replay/replay';

@Component({
  selector: 'app-create-trial',
  templateUrl: './create-trial.component.html',
  styleUrls: ['./create-trial.component.scss']
})
export class CreateTrialComponent implements OnInit {
  experiments: Experiment[] = [];
  client_info: ClientInfo[] = [];
  createTrialForm = this.formBuilder.group({
    trial_id: ['', Validators.required],
    name: ['', Validators.required],
    date: ['', Validators.required],
    experimenter: ['', Validators.required],
    subjects: ['', Validators.required],
    trial_number: ['', Validators.required],
    group_number: ['', Validators.required],
    study_number: ['', Validators.required],
    condition: ['', Validators.required],
    notes: [''],
    testbed_version: [environment.testbedVersion, Validators.required],
    map_name: ['', Validators.required],
    map_block_filename: ['', Validators.required],
    client_info_input: [''],
    team_id: ['', Validators.required],
    intervention_agents: [''],
    observers: [''],
    experiment: ['', Validators.required],
    useMessageBus: [false]
  });
  uuidPattern: RegExp = new RegExp(/[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}/);
  clientInfoPattern: RegExp = new RegExp(/.+ : .+/);

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<CreateTrialComponent>,
    private experimentService: ExperimentService,
    public jsonDialog: MatDialog,
    private loggingService: LoggingService,
    @Inject(MAT_DIALOG_DATA) public data: Trial) {
  }

  readExperiments(): void {
    this.experimentService.readExperiments()
      .subscribe(experiments => {
        this.experiments = experiments;
      });
  }

  openJsonTrialDialog(): void {
    const dialogResult = this.createTrialForm.value;
    dialogResult.client_info = this.client_info;

    const subjectsText = this.createTrialForm.get('subjects').value;
    if (subjectsText !== '') {
      dialogResult.subjects = subjectsText.split(/[\r\n]+/);
    } else {
      dialogResult.subjects = [];
    }

    const notesText = this.createTrialForm.get('notes').value;
    if (notesText !== '') {
      dialogResult.notes = notesText.split(/[\r\n]+/);
    } else {
      dialogResult.notes = [];
    }

    const interventionAgentsText = this.createTrialForm.get('intervention_agents').value;
    if (interventionAgentsText !== '') {
      dialogResult.intervention_agents = interventionAgentsText.split(/[\r\n]+/);
    } else {
      dialogResult.intervention_agents = [];
    }

    const observersText = this.createTrialForm.get('observers').value;
    if (observersText !== '') {
      dialogResult.observers = observersText.split(/[\r\n]+/);
    } else {
      dialogResult.observers = [];
    }

    delete dialogResult.useMessageBus;

    const jsonDialogRef = this.jsonDialog.open(JsonTrialComponent, {
      // width: '250px',
      data: JSON.stringify(dialogResult, null, 2),
      panelClass: 'full-width-2-dialog'
    });

    jsonDialogRef.afterClosed().subscribe(result => {
      if (result) {
        try {
          const trial = JSON.parse(result.json) as Trial;
          // let experiment = {
          //   name: trial.experiment
          // }
          this.client_info = trial.client_info;
          const experiment = this.experiments.find(e => e.name === trial.experiment.name);
          this.createTrialForm.patchValue({
            trial_id: trial.trial_id,
            name: trial.name,
            date: trial.date,
            experimenter: trial.experimenter,
            subjects: trial.subjects.length > 0 ? trial.subjects.join('\r\n') : '',
            trial_number: trial.trial_number,
            group_number: trial.group_number,
            study_number: trial.study_number,
            condition: trial.condition,
            notes: trial.notes.length > 0 ? trial.notes.join('\r\n') : '',
            testbed_version: trial.testbed_version,
            map_name: trial.map_name,
            map_block_filename: trial.map_block_filename,
            client_info: trial.client_info,
            team_id: trial.team_id,
            intervention_agents: trial.intervention_agents.length > 0 ? trial.intervention_agents.join('\r\n') : '',
            observers: trial.observers.length > 0 ? trial.observers.join('\r\n') : '',
            experiment
          });
        } catch (e) {
          this.log(e);
        }
      }
    });
  }

  onCreateClick(): void {
    const dialogResult = this.createTrialForm.value;
    dialogResult.client_info = this.client_info;
    delete dialogResult.client_info_input;

    const subjectsText = this.createTrialForm.get('subjects').value;
    if (subjectsText !== '') {
      dialogResult.subjects = subjectsText.split(/[\r\n]+/);
    } else {
      dialogResult.subjects = [];
    }

    const notesText = this.createTrialForm.get('notes').value;
    if (notesText !== '') {
      dialogResult.notes = notesText.split(/[\r\n]+/);
    } else {
      dialogResult.notes = [];
    }

    const interventionAgentsText = this.createTrialForm.get('intervention_agents').value;
    if (interventionAgentsText !== '') {
      dialogResult.intervention_agents = interventionAgentsText.split(/[\r\n]+/);
    } else {
      dialogResult.intervention_agents = [];
    }

    const observersText = this.createTrialForm.get('observers').value;
    if (observersText !== '') {
      dialogResult.observers = observersText.split(/[\r\n]+/);
    } else {
      dialogResult.observers = [];
    }

    this.dialogRef.close(dialogResult);
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  onGenerateUUIDClick(): void {
    this.createTrialForm.patchValue({
      trial_id: uuidv4()
    });
  }

  toggleUseMessageBusChange(event: MatSlideToggleChange) {
    // if (event.checked) {
    //   this.createTrialForm.controls['trial_id'].reset('', {
    //     onlySelf: true
    //   });
    // } else {
    //   this.createTrialForm.controls['trial_id'].reset('', {
    //     onlySelf: true
    //   });
    // }
  }

  ngOnInit(): void {
    this.readExperiments();
  }

  compareFnExperiments(e1: Experiment, e2: Experiment): boolean {
    return e1 && e2 ? e1.name === e2.name : e1 === e2;
  }

  onAddClientInfoItem(): void {
    if (this.createTrialForm.controls.client_info_input.value !== '') {
      const types = this.createTrialForm.controls.client_info_input.value.split(' : ');
      const clientInfoType = {
        callsign: types[0],
        participant_id: types[1]
      };
      this.client_info.push(clientInfoType);
      this.createTrialForm.controls.client_info_input.setValue('');
    }
  }

  onRemoveClientInfoItem(item: any): void {
    const index: number = this.client_info.findIndex(i => i === item);
    if (index > -1) {
      this.client_info.splice(index, 1);
    }
  }

  /** Log a TrialService message with the MessageService */
  private log(message: string) {
    this.loggingService.add(`CreateTrialComponent: ${message}`);
  }

}
