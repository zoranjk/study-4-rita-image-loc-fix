export interface ReplayMessageCountMessage {
  replay_id: string;
  current_message_count: number;
  total_message_count: number;
}
