package com.aptima.plugin;

import agent.tests.api.AgentTest;
import agent.tests.api.AgentTestPluginManager;
import org.apache.commons.lang3.StringUtils;
import org.pf4j.PluginManager;
import org.pf4j.PluginWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;

public class DebugFramework {

    private static final Logger log = LoggerFactory.getLogger(DebugFramework.class);

    public static void main(String[] args) {
        // print logo
        printLogo();

        // create the plugin manager
        // the path should point to the metadata plugins folder
        // make sure to run 'mvn clean package' to create plugin jars prior to running this test
        Path pluginPath = Paths.get(System.getProperty("user.dir"),"../metadata-app/plugins");
        log.info("Plugin path: {}", pluginPath);
        PluginManager pluginManager = new AgentTestPluginManager(pluginPath);

        // load the plugins
        pluginManager.loadPlugins();

        // enable a disabled plugin
//        pluginManager.enablePlugin("welcome-plugin");

        // start (active/resolved) the plugins
        pluginManager.startPlugins();

        // retrieves the extensions for AgentTest extension point
        List<AgentTest> agentTests = pluginManager.getExtensions(AgentTest.class);
        log.info("Found {} extensions for extension point '{}'", agentTests.size(), AgentTest.class.getName());
        for (AgentTest agentTest : agentTests) {
            log.info(">>> {}", agentTest.getAgentName());
        }

        // print extensions from classpath (non plugin)
        log.info("Extensions added by classpath:");
        Set<String> extensionClassNames = pluginManager.getExtensionClassNames(null);
        for (String extension : extensionClassNames) {
            log.info("   {}", extension);
        }

        log.info("Extension classes by classpath:");
        List<Class<? extends AgentTest>> agentTestClasses = pluginManager.getExtensionClasses(AgentTest.class);
        for (Class<? extends AgentTest> agentTest : agentTestClasses) {
            log.info("   Class: {}", agentTest.getCanonicalName());
        }

        // print extensions ids for each started plugin
        List<PluginWrapper> startedPlugins = pluginManager.getStartedPlugins();
        for (PluginWrapper plugin : startedPlugins) {
            String pluginId = plugin.getDescriptor().getPluginId();
            log.info("Extensions added by plugin '{}}':", pluginId);
            extensionClassNames = pluginManager.getExtensionClassNames(pluginId);
            for (String extension : extensionClassNames) {
                log.info("   {}", extension);
            }
        }

        // print the extensions instances for Greeting extension point for each started plugin
        for (PluginWrapper plugin : startedPlugins) {
            String pluginId = plugin.getDescriptor().getPluginId();
            log.info("Extensions instances added by plugin '{}' for extension point '{}':", pluginId, AgentTest.class.getName());
            List<AgentTest> extensions = pluginManager.getExtensions(AgentTest.class, pluginId);
            for (Object extension : extensions) {
                log.info("   {}", extension);
            }
        }

        // print extensions instances from classpath (non plugin)
        log.info("Extensions instances added by classpath:");
        List<?> extensions = pluginManager.getExtensions((String) null);
        for (Object extension : extensions) {
            log.info("   {}", extension);
        }

        // print extensions instances for each started plugin
        for (PluginWrapper plugin : startedPlugins) {
            String pluginId = plugin.getDescriptor().getPluginId();
            log.info("Extensions instances added by plugin '{}':", pluginId);
            extensions = pluginManager.getExtensions(pluginId);
            for (Object extension : extensions) {
                log.info("   {}", extension);
            }
        }

        // stop the plugins
        pluginManager.stopPlugins();
        /*
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                pluginManager.stopPlugins();
            }

        });
        */
    }

    private static void printLogo() {
        log.info(StringUtils.repeat("#", 40));
        log.info(StringUtils.center("PF4J-DEMO", 40));
        log.info(StringUtils.repeat("#", 40));
    }

}
