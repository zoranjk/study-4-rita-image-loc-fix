package agent.tests.api;

import org.pf4j.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.file.Path;
import java.util.List;

public class AgentTestPluginManager extends DefaultPluginManager {
    private static final Logger log = LoggerFactory.getLogger(AgentTestPluginManager.class);

    // Use below code if you want to enable ServiceProviderExtensionFinder
//    @Override
//    protected ExtensionFinder createExtensionFinder() {
//      DefaultExtensionFinder extensionFinder = (DefaultExtensionFinder) super.createExtensionFinder();
//      extensionFinder.addServiceProviderExtensionFinder(); // to activate extensions not annotated
//
//      return extensionFinder;
//    }


    public AgentTestPluginManager(Path... pluginsRoots) {
        super(pluginsRoots);
    }

    public AgentTestPluginManager(List<Path> pluginsRoots) {
        super(pluginsRoots);
    }

    @Override
    protected PluginFactory createPluginFactory() {
        return new AgentTestPluginFactory();
    }

    @Override
    public void startPlugins() {
        for (PluginWrapper pluginWrapper : resolvedPlugins) {
            PluginState pluginState = pluginWrapper.getPluginState();
            if ((PluginState.DISABLED != pluginState) && (PluginState.STARTED != pluginState)) {
                try {
                    log.debug("Start plugin '{}'", getPluginLabel(pluginWrapper.getDescriptor()));
                    pluginWrapper.getPlugin().start();
                    pluginWrapper.setPluginState(PluginState.STARTED);
                    pluginWrapper.setFailedException(null);
                    startedPlugins.add(pluginWrapper);
                } catch (Exception | LinkageError e) {
                    pluginWrapper.setPluginState(PluginState.FAILED);
                    pluginWrapper.setFailedException(e);
                    log.error("Unable to start plugin '{}'", getPluginLabel(pluginWrapper.getDescriptor()));
//                    log.error("Unable to start plugin '{}'", getPluginLabel(pluginWrapper.getDescriptor()), e);
                } finally {
                    firePluginStateEvent(new PluginStateEvent(this, pluginWrapper, pluginState));
                }
            }
        }
    }

}
