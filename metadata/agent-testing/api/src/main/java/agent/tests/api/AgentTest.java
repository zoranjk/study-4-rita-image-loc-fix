package agent.tests.api;

import org.pf4j.ExtensionPoint;

import java.util.List;

public interface AgentTest extends ExtensionPoint {

    String getAgentName();
    void evaluate(String line);
    List<AgentTestResultRow> getResults();
}
