package agent.tests.api;

import org.pf4j.Plugin;

public abstract class AgentPlugin extends Plugin {

    protected final PluginContext context;

    protected AgentPlugin(PluginContext context) {
        super();

        this.context = context;
    }

}
