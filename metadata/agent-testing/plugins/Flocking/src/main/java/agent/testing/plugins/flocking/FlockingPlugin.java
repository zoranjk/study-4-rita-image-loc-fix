package agent.testing.plugins.flocking;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;

import java.text.MessageFormat;
import java.util.List;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class FlockingPlugin extends AgentPlugin {

    public FlockingPlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Extension
    public static class FlockingTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        boolean mission_running = false;
        int num_proximity_messages = 0;
        int elapsed_milliseconds_global = 0;
        @Override
        public String getAgentName() {
            return "AC_UCF_TA2_Flocking";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            if (topic != null) {
                if (topic.equalsIgnoreCase("observations/events/mission")) {
                    String missionState = jsonContext.read("$.data.mission_state");
                    if (missionState != null) {
                        mission_running = missionState.equalsIgnoreCase("start");
                    }
                } else if(mission_running) {
                    if (topic.equalsIgnoreCase("observations/state")) {
                        elapsed_milliseconds_global = jsonContext.read("$.data.elapsed_milliseconds_global");
                    } else if (topic.equalsIgnoreCase("agent/measure/AC_UCF_TA2_Flocking/flocking")) {
                        num_proximity_messages += 1;
                    }
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            int out = 0;
            boolean result1 = false;

            if (num_proximity_messages != 0) {
                result1 = (elapsed_milliseconds_global / num_proximity_messages <= 1000);
                out = (elapsed_milliseconds_global / num_proximity_messages);
            }

            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1} ms","# avg time per msg", out), "avg msg time <= 1 per second" );

            return this.getAgentTestResultRows();
        }
    }

}
