package agent.testing.plugins.cooperation;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;

import java.text.MessageFormat;
import java.util.List;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class CooperationPlugin extends AgentPlugin {

    public CooperationPlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Extension
    public static class CognitiveTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        int num_dyad_alignments = 0;
        int num_dyad_compliance = 0;
        @Override
        public String getAgentName() {
            return "AC_Cornell_TA2_Cooperation";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            if (topic != null) {
                if (topic.equalsIgnoreCase("agent/ac/dyad_alignment")) {
                    num_dyad_alignments += 1;
                } else if (topic.equalsIgnoreCase("agent/ac/dyad_compliance")) {
                    num_dyad_compliance += 1;
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            boolean result1 = (num_dyad_alignments >= 1);
            boolean result2 = (num_dyad_compliance >= 1);

            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1}","# dyad alignment messages", num_dyad_alignments), "# dyad alignment messages >= 1" );
            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1}","# dyad alignment compliance", num_dyad_compliance), "# dyad compliance messages >= 1" );

            return this.getAgentTestResultRows();
        }
    }

}
