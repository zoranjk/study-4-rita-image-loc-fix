package agent.testing.plugins.pyglfovagent;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;

import java.text.MessageFormat;
import java.util.List;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class PyGLFoVAgentPlugin extends AgentPlugin {

    public PyGLFoVAgentPlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Extension
    public static class CognitiveTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        boolean mission_running = false;
        int num_fov_messages = 0;
        int elapsed_milliseconds = 0;
        @Override
        public String getAgentName() {
            return "AC_CMU_TA1_PyGLFoVAgent";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            if (topic != null) {
                if (topic.equalsIgnoreCase("observations/events/mission")) {
                    String missionState = jsonContext.read("$.data.mission_state");
                    if (missionState != null) {
                        mission_running = missionState.equalsIgnoreCase("start");
                    }
                } else if(mission_running) {
                    if (topic.equalsIgnoreCase("observations/state")) {
                        elapsed_milliseconds = jsonContext.read("$.data.elapsed_milliseconds_global");
                    } else if (topic.equalsIgnoreCase("agent/pygl_fov/player/3d/summary")) {
                        num_fov_messages += 1;
                    }
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            int out = 0;
            boolean result1 = false;

            if (num_fov_messages != 0) {
                result1 = (elapsed_milliseconds / num_fov_messages <= 200);
                out = (elapsed_milliseconds / num_fov_messages);
            }

            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1}","# avg ms/msg in mission", out), "max threshold 200 ms/msg" );

            return this.getAgentTestResultRows();
        }
    }

}
