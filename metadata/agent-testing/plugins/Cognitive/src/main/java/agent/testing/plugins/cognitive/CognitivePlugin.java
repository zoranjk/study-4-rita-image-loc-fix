package agent.testing.plugins.cognitive;

import agent.tests.api.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import org.pf4j.Extension;

import java.text.MessageFormat;
import java.util.List;

/**
 * Location Monitor plugin.
 *
 * @author Aptima, Inc.
 */
public class CognitivePlugin extends AgentPlugin {

    public CognitivePlugin(PluginContext context) {
        super(context);
    }

    @Override
    public void start() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Override
    public void stop() {
        log.debug("{} started", getClass().getSimpleName() );
    }

    @Extension
    public static class CognitiveTest extends AgentResultHelper implements AgentTest {
        Configuration conf = Configuration.defaultConfiguration().addOptions(Option.SUPPRESS_EXCEPTIONS);
        int num_load_messages = 0;
        @Override
        public String getAgentName() {
            return "AC_CMUFMS_TA2_Cognitive";
        }

        @Override
        public void evaluate(String line) {
            DocumentContext jsonContext = JsonPath.using(conf).parse(line);
            String topic = jsonContext.read("$.topic");
            if (topic != null) {
                if (topic.equalsIgnoreCase("agent/measure/ac_cmufms_ta2_cognitive/load")) {
                    num_load_messages += 1;
                }
            }
        }

        @Override
        public List<AgentTestResultRow> getResults() {
            boolean result1 = (num_load_messages >= 80);

            this.addResult(MessageFormat.format("{0}_{1}", this.getAgentName(), this.getAgentTestResultRows().size()), result1, MessageFormat.format("{0} : {1}","# load messages", num_load_messages), "# load messages >= 80" );

            return this.getAgentTestResultRows();
        }
    }

}
