using Google.Api;
using Google.Api.Gax.ResourceNames;
using Google.Cloud.Monitoring.V3;
using Google.Protobuf.WellKnownTypes;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using ScalingMetricService.Models;
using ScalingMetricService.Services;
using static Google.Api.MetricDescriptor.Types;

namespace ScalingMetricService.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ScalingMetricController : ControllerBase
    {
        private readonly ILogger<ScalingMetricController> _logger;
        private IConfiguration Configuration { get; set; }
        private ScalingMetricConfiguration ScalingMetricConfiguration { get; set; }

        public ScalingMetricController(ILogger<ScalingMetricController> logger, IConfiguration configuration)
        {
            _logger = logger;

            Configuration = configuration;
            ScalingMetricConfiguration = new ScalingMetricConfiguration();
            configuration.GetSection("ScalingMetric").Bind(ScalingMetricConfiguration);
        }

        [HttpGet(Name = "CreateScalingMetric")]
        public IActionResult Get()
        {
            CreateMetric("asist-2130");

            return Ok();
        }

        [HttpPost(Name = "UpdateScalingMetric")]
        public IActionResult Post([FromQuery] int value)
        {
            if (ScalingMetricTimer.MetricValue != value)
            {
                _logger.LogInformation($"Received to MetricValue = {value}");

                ScalingMetricTimer.MetricValue = value;
            }

            return Ok();
        }

        private object CreateMetric(string projectId)
        {
            // Create client.
            MetricServiceClient metricServiceClient = MetricServiceClient.Create();

            // Prepare custom metric descriptor.      
            MetricDescriptor metricDescriptor = new MetricDescriptor();
            metricDescriptor.DisplayName = ScalingMetricConfiguration.DisplayName;
            metricDescriptor.Description = ScalingMetricConfiguration.Description;
            metricDescriptor.MetricKind = MetricKind.Gauge;
            metricDescriptor.ValueType = MetricDescriptor.Types.ValueType.Int64;
            metricDescriptor.Type = ScalingMetricConfiguration.MetricType;
            //metricDescriptor.Unit = "{USD}";
            //LabelDescriptor labels = new LabelDescriptor();
            //labels.Key = "store_id";
            //labels.ValueType = LabelDescriptor.Types.ValueType.String;
            //labels.Description = "The ID of the store.";
            //metricDescriptor.Labels.Add(labels);
            CreateMetricDescriptorRequest request = new CreateMetricDescriptorRequest
            {
                ProjectName = new ProjectName(projectId),
            };
            request.MetricDescriptor = metricDescriptor;
            // Make the request.
            MetricDescriptor response = metricServiceClient.CreateMetricDescriptor(request);
            Console.WriteLine("Done creating metric descriptor:");
            Console.WriteLine(JObject.Parse($"{response}").ToString());
            return 0;
        }
    }
}