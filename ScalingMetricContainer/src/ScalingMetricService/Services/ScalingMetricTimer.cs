﻿
using Google.Api.Gax.ResourceNames;
using Google.Api;
using Google.Cloud.Monitoring.V3;
using Google.Protobuf.WellKnownTypes;
using Newtonsoft.Json.Linq;
using Microsoft.Extensions.Configuration;
using ScalingMetricService.Models;

namespace ScalingMetricService.Services
{
    public class ScalingMetricTimer : IHostedService, IDisposable
    {
        private readonly ILogger<ScalingMetricTimer> _logger;
        private IConfiguration Configuration { get; set; }
        private ScalingMetricConfiguration ScalingMetricConfiguration { get; set; }
        private Timer? SynchronizerTimer { get; set; }
        static public bool IsEnabled { get; set; } = false;
        static public int MetricValue { get; set; }
        static private bool ScalingMetricRunning { get; set; }


        public ScalingMetricTimer(ILogger<ScalingMetricTimer> logger, IConfiguration configuration)
        {
            _logger = logger;
            SynchronizerTimer = null;
            ScalingMetricRunning = false;
            ScalingMetricConfiguration = new ScalingMetricConfiguration();
            configuration.GetSection("ScalingMetric").Bind(ScalingMetricConfiguration);
            MetricValue = ScalingMetricConfiguration.DefaultMetricValue;
        }

        public Task StartAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("ScalingMetricTimer: Timed Hosted Service running.");

            SynchronizerTimer = new Timer(ScalingMetricTimerAsync, null, TimeSpan.Zero, TimeSpan.FromSeconds(ScalingMetricConfiguration.TimerDelay));

            return Task.CompletedTask;
        }

        private void ScalingMetricTimerAsync(object? state)
        {
            if (!IsEnabled)
            {
                return;
            }
            try
            {
                if (ScalingMetricRunning)
                {
                    return;
                }

                ScalingMetricRunning = true;

                //_logger.LogInformation("ScalingMetricTimer : Started");

                WriteTimeSeriesData(ScalingMetricConfiguration.ProjectId, MetricValue);

                //_logger.LogInformation("ScalingMetricTimer : Finished");

                ScalingMetricRunning = false;
            }
            catch (Exception ex)
            {
                var message = $"Message : {ex.Message}. Inner Message : {ex.InnerException?.Message}";
                _logger.LogCritical(message);

                ScalingMetricRunning = false;
            }
        }
        private object WriteTimeSeriesData(string projectId, int value)
        {
            // Create client.
            MetricServiceClient metricServiceClient = MetricServiceClient.Create();
            // Initialize request argument(s).
            ProjectName name = new ProjectName(projectId);
            // Prepare a data point. 
            Point dataPoint = new Point();
            TypedValue scalingValue = new TypedValue();
            scalingValue.Int64Value = value;
            dataPoint.Value = scalingValue;
            Timestamp timeStamp = new Timestamp();
            timeStamp.Seconds = (long)(DateTime.UtcNow - DateTime.UnixEpoch).TotalSeconds;
            TimeInterval interval = new TimeInterval();
            interval.EndTime = timeStamp;
            dataPoint.Interval = interval;

            // Prepare custom metric.
            Metric metric = new Metric();
            metric.Type = ScalingMetricConfiguration.MetricType;

            // Prepare monitored resource.
            //MonitoredResource resource = new MonitoredResource();
            //resource.Type = "global";
            //resource.Labels.Add("project_id", projectId);

            // Create a new time series using inputs.
            TimeSeries timeSeriesData = new TimeSeries();
            timeSeriesData.Metric = metric;
            //timeSeriesData.Resource = resource;
            timeSeriesData.Points.Add(dataPoint);

            // Add newly created time series to list of time series to be written.
            IEnumerable<TimeSeries> timeSeries = new List<TimeSeries> { timeSeriesData };
            // Write time series data.
            metricServiceClient.CreateTimeSeries(name, timeSeries);
            //Console.WriteLine("Done writing time series data:");
            //Console.WriteLine(JObject.Parse($"{timeSeriesData}").ToString());
            return 0;
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("ScalingMetricTimer: Timed Hosted Service is stopping.");

            SynchronizerTimer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            SynchronizerTimer?.Dispose();
        }
    }
}

