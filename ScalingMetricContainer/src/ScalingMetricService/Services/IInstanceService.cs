﻿using Google.Api.Gax;
using Google.Cloud.Compute.V1;

namespace ScalingMetricService.Services
{
    public interface IInstanceService
    {
        public bool AddInstanceToGroup(string projectId, Instance instanceInfo, string instanceGroup, string zone);
        public Instance? GetInstanceInfo(string projectId, string instanceName, string zone);
        public string GenerateInstanceName(string instanceGroup);
        public bool StartNewInstance(string projectId, string name, string instanceTemplate, string zone);
        public int GetInstanceGroupSize(string projectId, string zone, string instanceGroup);
        public List<string> GetInstanceGroupNames(string projectId, string zone, string instanceGroup);
        public bool DeleteInstance(string projectId, Instance instance, string zone);

    }
}