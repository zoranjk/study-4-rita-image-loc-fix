﻿namespace ScalingMetricService.Models
{
    public class ScalingMetricConfiguration
    {
        public string GoogleCredentialsPath { get; set; }
        public string ProjectId { get; set; }
        public string Zone { get; set; }
        public string MetricType { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public int DefaultMetricValue { get; set; }
        public int MetricValuePerInstance { get; set; }
        public int TimerDelay { get; set; }
        public int InstanceCreationDelay { get; set; }
        public int InstanceDebounceTime { get; set; }
        public string InstanceGroup { get; set; }
        public string InstanceTemplate { get; set; }
    }
}
