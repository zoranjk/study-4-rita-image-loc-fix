# Scaling Metric Container

## Introduction

A microservice written in .Net 6.0 which sends an updated metric value to the Google cloud at a regular intervals for monitoring purposes. This scaling metric value, received from a POST to the service's REST API, is also used to send commands to the Google cloud to create instances when needed. Instances are placed in an instances group whose size is monitored and compared to the scaling metric. The REST API for this microservice also contains a DELETE endpoint to remove instances by specifying its external IP address.

## Appsetings.json

```json
  "ScalingMetric": {
    "GoogleCredentialsPath": "C:\\Projects\\ASIST\\docs\\asist-2130-5071cded2536.json",
    "ProjectId": "asist-2130",
    "Zone": "us-east4-c",
    "MetricType": "custom.googleapis.com/asist-2130/scaling_metric",
    "DisplayName": "ASIST Scaling Metric",
    "Description": "Directly related to the number of instances needed to support the teams wanting to use the ASIST testbed",
    "DefaultMetricValue": 10,
    "MetricValuePerInstance": 10,
    "TimerDelay": 10,
    "InstanceCreationDelay": 10,
    "InstanceDebounceTime": 20,
    "InstanceGroup": "asist-instance-group",
    "InstanceTemplate": "asist2-east-testbed-cluster-external1"
  }
```

**MetricValuePerInstance** Defaulted to 10 so that the metric value is 10x the number of instances needed. For example a metric value of 20 indicates that 2 instances are needed.
**Default Metric value** Defaulted to 10, so 10 = 1 instance needed at startup.
**TimerDelay** How often the set MetricValue is sent to the Google cloud. The instance group monitoring web site complains if the metric is not sent relatively frequently.
**InstanceCreationDelay** How often in seconds to check if a new instance needs to be created according to the metric value.
**InstanceDebounceTime** Interval in seconds to debounce a quickly changing metric value.
**InstanceGroup** Google instance group to place created instances in.
**InstanceTemplate** Google instance template to use from created instances.

## Google API Key

The Google API key is provided by a JSON file on the filesystem. The path to this file is provided in the GoogleCredentialsPath entry in appsettings.json.

### Generating a new Google API Key
- Go to the API Console.
- From the projects list, select a project or create a new one.
- If the APIs & services page isn't already open, open the left side menu and select APIs & services.
- On the left, choose Credentials.
Service Account = https://console.cloud.google.com/apis/credentials?project=asist-2130
- Click Create credentials and then select API key.
217403928716-compute@developer.gserviceaccount.com
Then look at "keys" tab

## REST API
### Creating the scaling metric
**GET** /ScalingMetric
This endpoint only needs to be called once to create the metric and the metric will not be listed on the google cloud web UIs before this is done.

### Update the scaling metric value sent each time period
**POST** /ScalingMetric
**[FromQuery]** int value

### Get internal IP of instance
**GET** /Instance/GetInternalIP/
**[FromQuery]** string ip
This endpoint returns the internal IP address for an instance given its external IP address "ip"

### Get internal IP of instance
**DELETE** /Instance/
**[FromQuery]** string ip
This endpoint shuts down and removes an instance given its external IP address "ip"


## MIG is set to not autoscale 
Autoscaling mode = Off (This service now creates instances bases on the scaling metric. It does not depend on Google to autoscale)

## Viewing ASIST Instances

https://console.cloud.google.com/compute/instances?project=asist-2130
https://console.cloud.google.com/compute/instanceGroups/list?project=asist-2130  (Monitoring Tab)

## Metric Viewer

Search for the "ASIST Scaling Metric"

https://console.cloud.google.com/monitoring/metrics-explorer?project=asist-2130

## Useful Documents

https://cloud.google.com/compute/docs/autoscaler/scaling-cloud-monitoring-metrics
https://cloud.google.com/monitoring/custom-metrics/creating-metrics#monitoring_delete_metric-csharp
https://cloud.google.com/monitoring/custom-metrics/diagnostics