export interface StateMessage {
  header: {
    timestamp: string;
    message_type: string;
    version: string;
  };
  msg: {
    sub_type: string;
    source: string;
    experiment_id: string;
    trial_id: string;
    version: string;
    replay_id: string;
    replay_parent_id: string;
    replay_parent_type: string;
  };
  data: {
    host_ip: string;
    state: string;
    services: {
      [service: string]: {
        source: string;
        timestamp: string;
        message_type: string;
        sub_type: string;
        state: string;
        topic: string;
      };
    };
    containers: {
      [container: string]: {
        container_id: string;
        container_name: string;
        image_id: string;
        status: string;
        state: string;
        created: string;
        timestamp: string;
        cpu_percent: string;
        mem_percent: string;
      };
    };
    instance_info: [{
      trial_id: string;
      trial_name: string;
      trial_created: string;
      experimenter: string;
      subjects: [string];
      trial_number: string;
      group_number: string;
      study_number: string;
      condition: string;
      notes: [string];
      experiment_id: string;
      experiment_name: string;
      experiment_created: string;
      experiment_author: string;
      experiment_mission: string;
      map_name: string;
      map_block_filename: string;
      players: [{
        callsign: string;
        participant_id: string;
      }];
      team_id: string;
      intervention_agents: [string];
      observers: [string];
      trial_start: string;
      trial_stop: string;
      auto_export_begin: string;
      auto_export_end: string;
      mission_timer: string;
      mission_elapsed_milliseconds: string;
      mission_name: string;
      mission_state: string;
      mission_state_change_outcome: string;
    }];
    instance_state: {
      ready: string;
      down: string;
      down_reason: string;
      message: string;
    }
  };
}
