export interface Properties {
  container_id: string;
  container_name: string;
  image_id: string;
  status: string;
  state: string;
  created: string;
  timestamp: string;
  cpu_percent: string;
  mem_percent: string;
}
