export interface Details {
  source: string;
  timestamp: string;
  message_type: string;
  sub_type: string;
  state: string;
  topic: string;
}
