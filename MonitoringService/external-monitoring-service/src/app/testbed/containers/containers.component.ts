import {Component, OnDestroy, OnInit} from '@angular/core';
import {Services} from '../services/services';
import {Containers} from './containers';
import {Subscription} from 'rxjs';
import {StateService} from '../state/state.service';
import {MatSelectionListChange} from '@angular/material/list';

@Component({
  selector: 'app-containers',
  templateUrl: './containers.component.html',
  styleUrls: ['./containers.component.scss']
})
export class ContainersComponent implements OnInit, OnDestroy {
  title: string;
  private containers: Containers;
  selectedTestbed: string;
  public containerNames: string[];

  private selectedTestbedSubscription: Subscription;

  constructor(private stateService: StateService) { }

  ngOnInit(): void {
    this.title = 'Containers';
    this.containerNames = [];
    this.selectedTestbedSubscription = this.stateService.selectedTestbed.subscribe(selectedTestbed => {
      this.selectedTestbed = selectedTestbed;
      this.containers = JSON.parse(this.stateService.getContainers(this.selectedTestbed)) as Containers;
      this.containerNames = Object.keys(this.containers).sort();
    });
  }

  ngOnDestroy(): void {
    this.selectedTestbedSubscription.unsubscribe();
  }

  public mapClass(containerName: string) {
    switch (this.containers[containerName].state) {
      case 'running':
        return 'container-running';
      default:
        return 'container-not-running';
    }
  }

  public containerSelectionChanged($event: MatSelectionListChange) {
    this.stateService.setSelectedContainer($event.source.selectedOptions.selected[0].value);
  }

}
