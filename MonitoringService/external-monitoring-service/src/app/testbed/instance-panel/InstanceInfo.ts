import {ClientInfo} from './ClientInfo';

export interface InstanceInfo {
  trial_id: string;
  trial_name: string;
  trial_created: string;
  experimenter: string;
  subjects: [string];
  trial_number: string;
  group_number: string;
  study_number: string;
  condition: string;
  notes: [string];
  experiment_id: string;
  experiment_name: string;
  experiment_created: string;
  experiment_author: string;
  experiment_mission: string;
  map_name: string;
  map_block_filename: string;
  players: [ClientInfo];
  team_id: string;
  intervention_agents: [string];
  observers: [string];
  trial_start: string;
  trial_stop: string;
  auto_export_begin: string;
  auto_export_end: string;
  mission_timer: string;
  mission_elapsed_milliseconds: string;
  mission_name: string;
  mission_state: string;
  mission_state_change_outcome: string;
}
