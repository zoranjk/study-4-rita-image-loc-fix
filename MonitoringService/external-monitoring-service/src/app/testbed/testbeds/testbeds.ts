import {Services} from '../services/services';
import {Containers} from '../containers/containers';
import {InstanceInfo} from '../instance-panel/InstanceInfo';
import {InstanceState} from '../instance-panel/InstanceState';

export interface Testbeds {
  [ip_address: string]: {
    state: string;
    services: Services;
    containers: Containers;
    instance_info: InstanceInfo[];
    instance_state: InstanceState;
  };
}
