import {Component, OnDestroy, OnInit} from '@angular/core';
import {IMqttMessage, MqttService} from "ngx-mqtt";
import {Subscription} from "rxjs";
import {StateMessage} from "./testbed/state-message";
import {StateService} from "./testbed/state/state.service";
import {MQTT_SERVICE_OPTIONS} from "./app.config";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  title = 'external-monitoring-service';
  isConnected: boolean;

  private onConnectSubscription: Subscription;
  private onCloseSubscription: Subscription;
  private stateSubscription: Subscription;

  constructor(
    private mqttService: MqttService,
    private stateService: StateService
  ) {
  }

  ngOnInit(): void {
    this.isConnected = false;
    this.onConnectSubscription = this.mqttService.onConnect.subscribe(() => {
      this.isConnected = true
      console.log('Connected!');
    });
    this.onCloseSubscription = this.mqttService.onClose.subscribe(() => {
      this.isConnected = false
      console.log('Connection closed!');
    });

    this.stateSubscription = this.mqttService.observe('state/+').subscribe((message: IMqttMessage) => {
      const topic = message.topic;
      if (topic.includes('/')) {
        const ip = topic.split('/').pop();
        const json = new TextDecoder('utf-8').decode(message.payload);
        const stateMessage = JSON.parse(json) as StateMessage;
        this.stateService.addState(stateMessage);
        console.log(`Received state from: ${ip}`);
      }
    });
  }

  connect() {
    if (!this.isConnected) {
      this.mqttService.connect(MQTT_SERVICE_OPTIONS);
    }
  }

  ngOnDestroy(): void {
    this.stateSubscription.unsubscribe();
    this.onConnectSubscription.unsubscribe();
    this.onCloseSubscription.unsubscribe();
  }
}
