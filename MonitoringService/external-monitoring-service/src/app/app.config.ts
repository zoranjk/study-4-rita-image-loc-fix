import {IMqttServiceOptions} from "ngx-mqtt";
import {environment} from "./environments/environment";

export const MQTT_SERVICE_OPTIONS: IMqttServiceOptions = {
  hostname: environment.mqttHost,
  port: environment.mqttPort,
  protocol: environment.mqttProtocol,
  path: environment.mqttPath
};
