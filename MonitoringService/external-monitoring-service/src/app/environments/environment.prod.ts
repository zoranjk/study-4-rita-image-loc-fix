export const environment = {
  production: true,
  mqttHost: window['env']['mqttHost'] || 'localhost',
  mqttPort: window['env']['mqttPort'] || 9000,
  mqttProtocol: window['env']['mqttProtocol'] || 'wss',
  mqttPath: window['env']['mqttPath'] || '/ExternalMQTT/',
  stateCombinedTopic: window['env']['stateCombinedTopic'] || 'testbeds/state',
  stateCombinedPeriod: window['env']['stateCombinedPeriod'] || 10000,
  testbedTimeout: window['env']['testbedTimeout'] || 30000
};
