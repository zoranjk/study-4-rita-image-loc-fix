(function(window) {
  window.env = window.env || {};

  // Environment variables
  window['env']['mqttHost'] = '${MQTT_HOST}';
  window['env']['mqttPort'] = '${MQTT_PORT}';
  window['env']['mqttProtocol'] = '${MQTT_PROTOCOL}';
  window['env']['mqttPath'] = '${MQTT_PATH}';
  window['env']['stateCombinedTopic'] = '${STATE_COMBINED_TOPIC}';
  window['env']['stateCombinedPeriod'] = '${STATE_COMBINED_PERIOD}';
  window['env']['testbedTimeout'] = '${TESTBED_TIMEOUT}';
})(this);
