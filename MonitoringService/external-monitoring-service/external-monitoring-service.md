# ExternalMonitoringService

Description of the external-monitoring-service env variables.

## ENV
The mode to run the application in. Typically, this is set to **production** when running from the docker container.

## MQTT_HOST

The ip address of the **external** mqtt host, for example `10.0.0.1`.

## MQTT_PORT

The web socket port number of the **external** mqtt host, for example `9002`.
