package asist.aptima.com.repository;

import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.model.MessageHeartbeat;
import asist.aptima.com.utility.SortingAndOrderArgumentsHeartbeat;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceInfo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

public interface InstanceInfoRepository {

    Optional<InstanceInfo> findById(long id);
    Optional<InstanceInfo> findByTrialId(String trialId);
    List<InstanceInfo> findAll(@NotNull SortingAndOrderArgumentsInstanceInfo args);
    InstanceInfo save(@NotBlank InstanceInfo instanceInfo);
    InstanceInfo update(@NotBlank InstanceInfo instanceInfo);
    void deleteById(long id);
}
