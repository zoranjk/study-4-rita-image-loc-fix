package asist.aptima.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.hypersistence.utils.hibernate.type.json.JsonType;
import io.micronaut.serde.annotation.Serdeable;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Serdeable
@Entity
@Table(name = "instance_info")
@TypeDef(name = "json", typeClass = JsonType.class)
public class InstanceInfo {

	private static final Logger logger = LoggerFactory.getLogger(InstanceInfo.class);

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonProperty("id")
    private Long id;

    @Version
    @JsonIgnore
    private Long version;

    @NotNull
    @Column(name="trial_id", nullable=false, unique=true)
    @JsonProperty("trial_id")
    private String trialId;

    @NotNull
    @Column(name="trial_name", nullable=false)
    @JsonProperty("trial_name")
    private String trialName;

    @NotNull
    @Column(name="trial_created", nullable=false)
    @JsonProperty("trial_created")
    private String trialCreated;

    @NotNull
    @Column(name="experimenter", nullable=false)
    @JsonProperty("experimenter")
    private String experimenter;

    @Type(type = "json")
    @Column(name="subjects", columnDefinition = "json")
    @JsonProperty("subjects")
    private String subjects;

    @NotNull
    @Column(name="trial_number", nullable=false)
    @JsonProperty("trial_number")
    private String trialNumber;

    @NotNull
    @Column(name="group_number", nullable=false)
    @JsonProperty("group_number")
    private String groupNumber;

    @NotNull
    @Column(name="study_number", nullable=false)
    @JsonProperty("study_number")
    private String studyNumber;

    @NotNull
    @Column(name="condition", nullable=false)
    @JsonProperty("condition")
    private String condition;

    @Type(type = "json")
    @Column(name="notes", columnDefinition = "json")
    @JsonProperty("notes")
    private String notes;

    @NotNull
    @Column(name="experiment_id", nullable=false)
    @JsonProperty("experiment_id")
    private String experimentId;

    @NotNull
    @Column(name="experiment_name", nullable=false)
    @JsonProperty("experiment_name")
    private String experimentName;

    @NotNull
    @Column(name="experiment_created", nullable=false)
    @JsonProperty("experiment_created")
    private String experimentCreated;

    @NotNull
    @Column(name="experiment_author", nullable=false)
    @JsonProperty("experiment_author")
    private String experimentAuthor;

    @NotNull
    @Column(name="experiment_mission", nullable=false)
    @JsonProperty("experiment_mission")
    private String experimentMission;

    @NotNull
    @Column(name="map_name", nullable=false)
    @JsonProperty("map_name")
    private String mapName;

    @NotNull
    @Column(name="map_block_filename", nullable=false)
    @JsonProperty("map_block_filename")
    private String mapBlockFilename;

    @Type(type = "json")
    @Column(name="players", columnDefinition = "json")
    @JsonProperty("players")
    private String players;

    @NotNull
    @Column(name="team_id", nullable=false)
    @JsonProperty("team_id")
    private String teamId;

    @Type(type = "json")
    @Column(name="intervention_agents", columnDefinition = "json")
    @JsonProperty("intervention_agents")
    private String interventionAgents;

    @Type(type = "json")
    @Column(name="observers", columnDefinition = "json")
    @JsonProperty("observers")
    private String observers;

    @NotNull
    @Column(name="trial_start", nullable=false)
    @JsonProperty("trial_start")
    private String trialStart;

    @NotNull
    @Column(name="trial_stop", nullable=false)
    @JsonProperty("trial_stop")
    private String trialStop;

    @NotNull
    @Column(name="auto_export_begin", nullable=false)
    @JsonProperty("auto_export_begin")
    private String autoExportBegin;

    @NotNull
    @Column(name="auto_export_end", nullable=false)
    @JsonProperty("auto_export_end")
    private String autoExportEnd;

    @NotNull
    @Column(name="mission_timer", nullable=false)
    @JsonProperty("mission_timer")
    private String missionTimer;

    @NotNull
    @Column(name="mission_elapsed_milliseconds", nullable=false)
    @JsonProperty("mission_elapsed_milliseconds")
    private String missionElapsedMilliseconds;

    @NotNull
    @Column(name="mission_name", nullable=false)
    @JsonProperty("mission_name")
    private String missionName;

    @NotNull
    @Column(name="mission_state", nullable=false)
    @JsonProperty("mission_state")
    private String missionState;

    @NotNull
    @Column(name="mission_state_change_outcome", nullable=false)
    @JsonProperty("mission_state_change_outcome")
    private String missionStateChangeOutcome;

    public InstanceInfo() {}

    public InstanceInfo(
    		@NotNull String trialId,
    		@NotNull String trialName,
    		@NotNull String trialCreated,
    		@NotNull String experimenter,
    		@NotNull String subjects,
    		@NotNull String trialNumber,
    		@NotNull String groupNumber,
    		@NotNull String studyNumber,
    		@NotNull String condition,
    		@NotNull String notes,
    		@NotNull String experimentId,
    		@NotNull String experimentName,
    		@NotNull String experimentCreated,
    		@NotNull String experimentAuthor,
    		@NotNull String experimentMission,
    		@NotNull String mapName,
    		@NotNull String mapBlockFilename,
    		@NotNull String players,
            @NotNull String teamId,
            @NotNull String interventionAgents,
            @NotNull String observers,
    		@NotNull String trialStart,
    		@NotNull String trialStop,
    		@NotNull String autoExportBegin,
    		@NotNull String autoExportEnd,
    		@NotNull String missionTimer,
    		@NotNull String missionElapsedMilliseconds,
    		@NotNull String missionName,
    		@NotNull String missionState,
    		@NotNull String missionStateChangeOutcome
                ) {
        this.trialId = trialId;
        this.trialName = trialName;
        this.trialCreated = trialCreated;
        this.experimenter = experimenter;
        this.subjects = subjects;
        this.trialNumber = trialNumber;
        this.groupNumber = groupNumber;
        this.studyNumber = studyNumber;
        this.condition = condition;
        this.notes = notes;
        this.experimentId = experimentId;
        this.experimentName = experimentName;
        this.experimentCreated = experimentCreated;
        this.experimentAuthor = experimentAuthor;
        this.experimentMission = experimentMission;
        this.mapName = mapName;
        this.mapBlockFilename = mapBlockFilename;
        this.players = players;
        this.teamId = teamId;
        this.interventionAgents = interventionAgents;
        this.observers = observers;
        this.trialStart = trialStart;
        this.trialStop = trialStop;
        this.autoExportBegin = autoExportBegin;
        this.autoExportEnd = autoExportEnd;
        this.missionTimer = missionTimer;
        this.missionElapsedMilliseconds = missionElapsedMilliseconds;
        this.missionName = missionName;
        this.missionState = missionState;
        this.missionStateChangeOutcome = missionStateChangeOutcome;
    }

    public InstanceInfo(
            @NotNull String trialId,
            @NotNull String trialName,
            @NotNull String trialCreated,
            @NotNull String experimenter,
            @NotNull String subjects,
            @NotNull String trialNumber,
            @NotNull String groupNumber,
            @NotNull String studyNumber,
            @NotNull String condition,
            @NotNull String notes,
            @NotNull String experimentId,
            @NotNull String experimentName,
            @NotNull String experimentCreated,
            @NotNull String experimentAuthor,
            @NotNull String experimentMission,
            @NotNull String mapName,
            @NotNull String mapBlockFilename,
            @NotNull String players,
            @NotNull String teamId,
            @NotNull String interventionAgents,
            @NotNull String observers,
            @NotNull String trialStart
    ) {
        this.trialId = trialId;
        this.trialName = trialName;
        this.trialCreated = trialCreated;
        this.experimenter = experimenter;
        this.subjects = subjects;
        this.trialNumber = trialNumber;
        this.groupNumber = groupNumber;
        this.studyNumber = studyNumber;
        this.condition = condition;
        this.notes = notes;
        this.experimentId = experimentId;
        this.experimentName = experimentName;
        this.experimentCreated = experimentCreated;
        this.experimentAuthor = experimentAuthor;
        this.experimentMission = experimentMission;
        this.mapName = mapName;
        this.mapBlockFilename = mapBlockFilename;
        this.players = players;
        this.teamId = teamId;
        this.interventionAgents = interventionAgents;
        this.observers = observers;
        this.trialStart = trialStart;
        this.trialStop = "";
        this.autoExportBegin = "";
        this.autoExportEnd = "";
        this.missionTimer = "";
        this.missionElapsedMilliseconds = "";
        this.missionName = "";
        this.missionState = "";
        this.missionStateChangeOutcome = "";
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrialId() {
        return trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

    public String getTrialName() {
        return trialName;
    }

    public void setTrialName(String trialName) {
        this.trialName = trialName;
    }

    public String getTrialCreated() {
        return trialCreated;
    }

    public void setTrialCreated(String trialCreated) {
        this.trialCreated = trialCreated;
    }

    public String getExperimenter() {
        return experimenter;
    }

    public void setExperimenter(String experimenter) {
        this.experimenter = experimenter;
    }

    public String getSubjects() {
        return subjects;
    }

    public void setSubjects(String subjects) {
        this.subjects = subjects;
    }

    public String getTrialNumber() {
        return trialNumber;
    }

    public void setTrialNumber(String trialNumber) {
        this.trialNumber = trialNumber;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getStudyNumber() {
        return studyNumber;
    }

    public void setStudyNumber(String studyNumber) {
        this.studyNumber = studyNumber;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(String experimentId) {
        this.experimentId = experimentId;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public void setExperimentName(String experimentName) {
        this.experimentName = experimentName;
    }

    public String getExperimentCreated() {
        return experimentCreated;
    }

    public void setExperimentCreated(String experimentCreated) {
        this.experimentCreated = experimentCreated;
    }

    public String getExperimentAuthor() {
        return experimentAuthor;
    }

    public void setExperimentAuthor(String experimentAuthor) {
        this.experimentAuthor = experimentAuthor;
    }

    public String getExperimentMission() {
        return experimentMission;
    }

    public void setExperimentMission(String experimentMission) {
        this.experimentMission = experimentMission;
    }

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    public String getMapBlockFilename() {
        return mapBlockFilename;
    }

    public void setMapBlockFilename(String mapBlockFilename) {
        this.mapBlockFilename = mapBlockFilename;
    }

    public String getPlayers() {
        return players;
    }

    public void setPlayers(String players) {
        this.players = players;
    }


    public String getTeamId() {
        return teamId;
    }

    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public String getInterventionAgents() {
        return interventionAgents;
    }

    public void setInterventionAgents(String interventionAgents) {
        this.interventionAgents = interventionAgents;
    }

    public String getObservers() {
        return observers;
    }

    public void setObservers(String observers) {
        this.observers = observers;
    }



    public String getTrialStart() {
        return trialStart;
    }

    public void setTrialStart(String trialStart) {
        this.trialStart = trialStart;
    }

    public String getTrialStop() {
        return trialStop;
    }

    public void setTrialStop(String trialStop) {
        this.trialStop = trialStop;
    }

    public String getAutoExportBegin() {
        return autoExportBegin;
    }

    public void setAutoExportBegin(String autoExportBegin) {
        this.autoExportBegin = autoExportBegin;
    }

    public String getAutoExportEnd() {
        return autoExportEnd;
    }

    public void setAutoExportEnd(String autoExportEnd) {
        this.autoExportEnd = autoExportEnd;
    }

    public String getMissionTimer() {
        return missionTimer;
    }

    public void setMissionTimer(String missionTimer) {
        this.missionTimer = missionTimer;
    }

    public String getMissionElapsedMilliseconds() {
        return missionElapsedMilliseconds;
    }

    public void setMissionElapsedMilliseconds(String missionElapsedMilliseconds) {
        this.missionElapsedMilliseconds = missionElapsedMilliseconds;
    }

    public String getMissionName() {
        return missionName;
    }

    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }

    public String getMissionState() {
        return missionState;
    }

    public void setMissionState(String missionState) {
        this.missionState = missionState;
    }

    public String getMissionStateChangeOutcome() {
        return missionStateChangeOutcome;
    }

    public void setMissionStateChangeOutcome(String missionStateChangeOutcome) {
        this.missionStateChangeOutcome = missionStateChangeOutcome;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\": \"" + id + "\"," +
                "\"trial_id\": \"" + trialId + "\"," +
                "\"trial_name\": \"" + trialName + "\"," +
                "\"trial_created\": \"" + trialCreated + "\"," +
                "\"experimenter\": \"" + experimenter + "\"," +
                "\"subjects\": \"" + subjects + "\"," +
                "\"trial_number\": \"" + trialNumber + "\"," +
                "\"group_number\": \"" + groupNumber + "\"," +
                "\"study_number\": \"" + studyNumber + "\"," +
                "\"condition\": \"" + condition + "\"," +
                "\"notes\": \"" + notes + "\"," +
                "\"experiment_id\": \"" + experimentId + "\"," +
                "\"experiment_name\": \"" + experimentName + "\"," +
                "\"experiment_created\": \"" + experimentCreated + "\"," +
                "\"experiment_author\": \"" + experimentAuthor + "\"," +
                "\"experiment_mission\": \"" + experimentMission + "\"," +
                "\"map_mame\": \"" + mapName + "\"," +
                "\"map_block_filename\": \"" + mapBlockFilename + "\"," +
                "\"players\": \"" + players + "\"," +
                "\"team_id\": \"" + teamId + "\"," +
                "\"intervention_agents\": \"" + interventionAgents + "\"," +
                "\"observers\": \"" + observers + "\"," +
                "\"trial_start\": \"" + trialStart + "\"," +
                "\"trial_stop\": \"" + trialStop + "\"," +
                "\"auto_export_begin\": \"" + autoExportBegin + "\"" +
                "\"auto_export_end\": \"" + autoExportEnd + "\"" +
                "\"mission_timer\": \"" + missionTimer + "\"" +
                "\"mission_elapsed_milliseconds\": \"" + missionElapsedMilliseconds + "\"" +
                "\"mission_name\": \"" + missionName + "\"" +
                "\"mission_state\": \"" + missionState + "\"" +
                "\"mission_state+change_outcome\": \"" + missionStateChangeOutcome + "\"" +
                "}";
    }
}
