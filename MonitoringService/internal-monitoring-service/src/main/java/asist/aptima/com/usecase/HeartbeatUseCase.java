package asist.aptima.com.usecase;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.task.CountdownTask;
import io.micronaut.context.annotation.Property;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.TaskScheduler;
import jakarta.inject.Named;
import jakarta.inject.Singleton;

@Singleton
public class HeartbeatUseCase {

	private static final Logger logger = LoggerFactory.getLogger(HeartbeatUseCase.class);
	
    @Property(name = "internal-monitoring-service.heartbeat.countdown.period")
	private Duration COUNTDOWN_PERIOD;
	
    protected final TaskScheduler taskScheduler;
    protected final CountdownUseCase countdownUseCase;
    //private Map<String, ScheduledFuture<?>> schedules = new HashMap<String, ScheduledFuture<?>>();
    protected final CountdownSchedules schedules;

    public HeartbeatUseCase(
    		CountdownUseCase countdownUseCase,
    		@Named(TaskExecutors.SCHEDULED) TaskScheduler taskScheduler,
    		CountdownSchedules schedules
    		) {  
        this.countdownUseCase = countdownUseCase;
        this.taskScheduler = taskScheduler;
        this.schedules = schedules;
    }

    public void received(String source) {
    	logger.debug("Scheduling countdown for [{}]", source);
        scheduleCountdown(source);
    }

    private void scheduleCountdown(String source) {
    	if (schedules.containsKey(source)) {
    		ScheduledFuture<?> schedule = schedules.remove(source);
    		schedule.cancel(false);
    	}
    	CountdownTask task = new CountdownTask(countdownUseCase, source, schedules);
        ScheduledFuture<?> schedule = taskScheduler.scheduleAtFixedRate(COUNTDOWN_PERIOD, COUNTDOWN_PERIOD, task);
        schedules.put(source, schedule);
    }
}
