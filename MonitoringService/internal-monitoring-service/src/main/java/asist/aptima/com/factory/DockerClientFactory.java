package asist.aptima.com.factory;

import asist.aptima.com.configuration.DockerClientConfigurationProperties;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.core.DefaultDockerClientConfig;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.DockerClientImpl;
import com.github.dockerjava.httpclient5.ApacheDockerHttpClient;
import com.github.dockerjava.transport.DockerHttpClient;
import io.micronaut.context.annotation.Factory;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Factory
public class DockerClientFactory {

    private static final Logger logger = LoggerFactory.getLogger(DockerClientFactory.class);

    @Singleton
    DockerClient dockerClient(DockerClientConfigurationProperties configuration) {
        logger.info("Docker Client configuration: host [{}] tlsVerify [{}].", configuration.getHost(), configuration.getTlsVerify());
        DockerClientConfig config = DefaultDockerClientConfig.createDefaultConfigBuilder().withDockerHost(configuration.getHost())
                .withDockerTlsVerify(configuration.getTlsVerify()).build();

        DockerHttpClient httpClient = new ApacheDockerHttpClient.Builder().dockerHost(config.getDockerHost())
                .sslConfig(config.getSSLConfig()).build();

        return DockerClientImpl.getInstance(config, httpClient);

    }
}
