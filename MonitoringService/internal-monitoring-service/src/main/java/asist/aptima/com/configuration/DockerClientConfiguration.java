package asist.aptima.com.configuration;

public interface DockerClientConfiguration {

    String getHost();
    boolean getTlsVerify();
}
