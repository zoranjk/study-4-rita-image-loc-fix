package asist.aptima.com.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.model.MessageHeartbeat;
import asist.aptima.com.utility.SortingAndOrderArgumentsHeartbeat;

public interface HeartbeatRepository {

    Optional<Heartbeat> findById(long id);
    Optional<Heartbeat> findBySource(String source);
    List<Heartbeat> findAll(@NotNull SortingAndOrderArgumentsHeartbeat args);
    Heartbeat save(@NotBlank MessageHeartbeat messageHeartbeat, String topic);
    int update(@NotBlank Heartbeat heartbeat);
    int updateState(long id, @NotBlank String state);
    void deleteById(long id);
}
