package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class MessageHeartbeat {
	
	@JsonProperty("header") 
	private Header header;
	@JsonProperty("msg") 
	private Msg msg;
	@JsonProperty("data") 
	private DataHeartbeat data;

	@JsonCreator
	public MessageHeartbeat(
			Header header,
			Msg msg,
			DataHeartbeat data
			) {
		this.header = header;
		this.msg = msg;
		this.data = data;
	}

	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	
	public Msg getMsg() {
		return msg;
	}
	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	public DataHeartbeat getData() {
		return data;
	}
	public void setData(DataHeartbeat data) {
		this.data = data;
	}
}