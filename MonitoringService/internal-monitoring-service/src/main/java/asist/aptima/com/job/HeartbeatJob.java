package asist.aptima.com.job;

import java.io.IOException;
import java.text.MessageFormat;
import java.time.Instant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import asist.aptima.com.client.HealthClient;
import asist.aptima.com.model.DataHeartbeat;
import asist.aptima.com.model.Header;
import asist.aptima.com.model.HealthStatus;
import asist.aptima.com.model.HeartbeatStateType;
import asist.aptima.com.model.MessageHeartbeat;
import asist.aptima.com.model.Msg;
import asist.aptima.com.publisher.StatusPublisher;
import io.micronaut.context.annotation.Property;
import io.micronaut.scheduling.annotation.Scheduled;
import io.micronaut.serde.ObjectMapper;
import jakarta.inject.Singleton;

@Singleton
public class HeartbeatJob {
	private static final Logger logger = LoggerFactory.getLogger(HeartbeatJob.class);
	private ObjectMapper objectMapper;
	
	private StatusPublisher statusPublisher;
    private HealthClient healthClient;
    
    @Property(name = "internal-monitoring-service.schema.header.version")
	private String INTERNAL_SCHEMA_HEADER_VERSION;
    
    @Property(name = "internal-monitoring-service.schema.header.messagetype")
	private String INTERNAL_SCHEMA_HEADER_MESSAGETYPE;
    
    @Property(name = "internal-monitoring-service.schema.msg.version")
	private String INTERNAL_SCHEMA_MSG_VERSION;
    
    @Property(name = "internal-monitoring-service.schema.msg.subtype")
	private String INTERNAL_SCHEMA_MSG_SUBTYPE;
    
    @Property(name = "internal-monitoring-service.schema.msg.source")
	private String INTERNAL_SCHEMA_MSG_SOURCE;
    
    @Property(name = "internal-monitoring-service.service.name")
	private String INTERNAL_SERVICE_NAME;
    
    private final String NOT_SET = "NOT_SET";
	
	public HeartbeatJob(ObjectMapper objectMapper, HealthClient healthClient, StatusPublisher statusPublisher) {
		this.objectMapper = objectMapper;
		this.healthClient = healthClient;
		this.statusPublisher = statusPublisher;
	}
	
	@Scheduled(fixedDelay = "${jobs.internal-monitoring-service.status.fixedDelay}", initialDelay = "${jobs.internal-monitoring-service.status.initialDelay}") 
    void execute() {
		try {
			logger.info("Task: Executing HeartbeatJob");

			String health = healthClient.health();
			HealthStatus healthStatus = objectMapper.readValue(health, HealthStatus.class);
			String status = healthStatus.getStatus();
			Header header = new Header(Instant.now().toString(), INTERNAL_SCHEMA_HEADER_MESSAGETYPE, INTERNAL_SCHEMA_HEADER_VERSION);
			Msg msg = new Msg(INTERNAL_SCHEMA_MSG_SUBTYPE, INTERNAL_SCHEMA_MSG_SOURCE, NOT_SET, NOT_SET, INTERNAL_SCHEMA_MSG_VERSION, null, null, null);
			DataHeartbeat dataHeartbeat = new DataHeartbeat(HeartbeatStateType.ok);
			MessageHeartbeat messageHeartbeat = new MessageHeartbeat(header, msg, dataHeartbeat);
			if (status.equals("UP")) {
				logger.trace(MessageFormat.format("Heartbeat: {0} [{1}]", INTERNAL_SERVICE_NAME, status));
				String topic = MessageFormat.format("status/{0}/heartbeats", INTERNAL_SERVICE_NAME);
				statusPublisher.send(topic, 2, objectMapper.writeValueAsBytes(messageHeartbeat)).complete(null);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
