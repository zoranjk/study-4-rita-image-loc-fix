package asist.aptima.com.appender;

import java.io.IOException;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Base64;
import java.util.Date;
import java.util.Base64.Encoder;

import asist.aptima.com.model.DataStatusError;
import asist.aptima.com.model.Header;
import asist.aptima.com.model.MessageStatusError;
import asist.aptima.com.model.Msg;
import asist.aptima.com.publisher.ErrorLogPublisher;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import io.micronaut.context.annotation.Property;
import io.micronaut.context.annotation.Requires;
import io.micronaut.serde.ObjectMapper;
import jakarta.inject.Inject;
import jakarta.inject.Singleton;

@Singleton
//@Requires(beans = ErrorLogPublisher.class)
public class MqttAppender extends UnsynchronizedAppenderBase<ILoggingEvent> { //UnsynchronizedAppenderBase
	
	private ObjectMapper objectMapper;
	@Inject
	private ErrorLogPublisher errorLogClient;
	
    @Property(name = "error.schema.header.version")
	private String ERROR_SCHEMA_HEADER_VERSION;
    
    @Property(name = "error.schema.header.messagetype")
	private String ERROR_SCHEMA_HEADER_MESSAGETYPE;
    
    @Property(name = "error.schema.msg.version")
	private String ERROR_SCHEMA_MSG_VERSION;
    
    @Property(name = "error.schema.msg.subtype")
	private String ERROR_SCHEMA_MSG_SUBTYPE;
    
    @Property(name = "error.schema.msg.source")
	private String ERROR_SCHEMA_MSG_SOURCE;
	
	private final String NOT_SET = "NOT_SET";
    
	public MqttAppender(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}
	
	@Override
	protected void append(ILoggingEvent eventObject) {
        String event = format(eventObject);        

    	Encoder encoder = Base64.getEncoder();
    	String encodedString = encoder.encodeToString(event.getBytes());

    	Header header = new Header(Instant.now().toString(), ERROR_SCHEMA_HEADER_MESSAGETYPE, ERROR_SCHEMA_HEADER_VERSION);
    	Msg msg = new Msg(ERROR_SCHEMA_MSG_SUBTYPE, ERROR_SCHEMA_MSG_SOURCE, NOT_SET, NOT_SET, ERROR_SCHEMA_MSG_VERSION, null, null, null);
    	DataStatusError dataStatusError = new DataStatusError(encodedString);
    	
    	MessageStatusError messageStatusError = new MessageStatusError(header, msg, dataStatusError);

        if (errorLogClient != null) {
    		try {
				this.errorLogClient.send(objectMapper.writeValueAsBytes(messageStatusError)).complete(null);
			} catch (IOException e) {
				addError(e.getMessage());
				e.printStackTrace();
			}       	
        }
        else {
        	System.out.println("MetadataLogClient is null!");
        	addError("MetadataLogClient is null!");
        }        	
		
	}

	// private final int DEFAULT_BUFFER_SIZE = 512;
    private DateFormat df = new SimpleDateFormat("HH:mm:ss.SSS");

    private String format(ILoggingEvent event) {    	
        return MessageFormat.format("{0} [{1}] {2} {3} - {4}",
        		df.format(new Date(event.getTimeStamp())),
        		event.getThreadName(),
        		event.getLevel().toString(),
        		event.getLoggerName(),
        		event.getFormattedMessage()
        		);
    }
}