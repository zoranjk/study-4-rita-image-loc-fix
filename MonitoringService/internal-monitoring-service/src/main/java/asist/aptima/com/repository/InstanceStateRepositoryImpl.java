package asist.aptima.com.repository;

import asist.aptima.com.configuration.ApplicationConfiguration;
import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.domain.InstanceState;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceInfo;
import asist.aptima.com.utility.SortingAndOrderArgumentsInstanceState;
import io.micronaut.context.annotation.Property;
import io.micronaut.transaction.annotation.ReadOnly;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton
public class InstanceStateRepositoryImpl implements InstanceStateRepository {

    private static final Logger logger = LoggerFactory.getLogger(InstanceStateRepositoryImpl.class);

    private static final List<String> VALID_PROPERTY_NAMES = Arrays.asList("ready", "down", "downReason");

    private final EntityManager entityManager;
    private final ApplicationConfiguration applicationConfiguration;

    public InstanceStateRepositoryImpl(EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @ReadOnly
    public Optional<InstanceState> findById(long id) {
        return Optional.ofNullable(entityManager.find(InstanceState.class, id));
    }

    @Override
    @ReadOnly
    public Optional<InstanceState> findByHostIp(String hostIp) {
        try {
            return entityManager.createQuery("SELECT i FROM InstanceState as i WHERE i.hostIp = :hostIp", InstanceState.class)
                    .setParameter("hostIp", hostIp)
                    .getResultList()
                    .stream()
                    .findFirst();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    @ReadOnly
    public List<InstanceState> findAll(@NotNull SortingAndOrderArgumentsInstanceState args) {
        String qlString = "SELECT i FROM InstanceState as i";
        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
            qlString += " ORDER BY i." + args.getSort().get() + ' ' + args.getOrder().get().toLowerCase();
        }
        TypedQuery<InstanceState> query = entityManager.createQuery(qlString, InstanceState.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

    @Override
    @Transactional
    public InstanceState save(@NotBlank InstanceState instanceState) {
        try {
            entityManager.persist(instanceState);
            return instanceState;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    @Transactional
    public InstanceState update(@NotBlank InstanceState instanceState) {
        try {
            return entityManager.merge(instanceState);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

    @Override
    @Transactional
    public void deleteById(long id) {
        findById(id).ifPresent(entityManager::remove);
    }
}
