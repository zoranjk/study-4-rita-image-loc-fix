package asist.aptima.com.service;

import asist.aptima.com.model.Header;
import asist.aptima.com.model.Msg;
import asist.aptima.com.publisher.GeneralPublisher;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.micronaut.core.annotation.NonNull;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ExecuteOn(TaskExecutors.IO)
@Singleton
public class TrialSimulator {
    private static final Logger logger = LoggerFactory.getLogger(TrialSimulator.class);
    private final ObjectMapper objectMapper;
    private final GeneralPublisher generalPublisher;

    public TrialSimulator(GeneralPublisher generalPublisher) {
        this.generalPublisher = generalPublisher;
        this.objectMapper = new ObjectMapper();
    }

    public void simulateTrial(@NonNull byte[] bytes, @NonNull String filename) {
        try {
            InputStream inputStream = new ByteArrayInputStream(bytes);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            List<Instant> lastInstant = new ArrayList<>();
            lastInstant.add(null);

            while (bufferedReader.ready()) {
                String json = bufferedReader.readLine();
                JsonNode sourceMap = objectMapper.readTree(json);
                if (sourceMap.has("@timestamp")) {
                    String timestamp = sourceMap.get("@timestamp").asText();
                    String topic = sourceMap.get("topic").asText();
                    Map<String, Object> message = new HashMap<>();
                    String strHeader = objectMapper.writeValueAsString(sourceMap.get("header"));
                    Header header = objectMapper.readValue(strHeader, Header.class);
                    message.put("header", header);
                    String strMsg = objectMapper.writeValueAsString(sourceMap.get("msg"));
                    Msg msg = objectMapper.readValue(strMsg, Msg.class);
                    msg.setReplayId(null);
                    msg.setReplayParentId(null);
                    msg.setReplayParentType(null);
                    message.put("msg", msg);
                    message.put("data", sourceMap.get("data"));

                    Instant instant = Instant.parse(timestamp);
                    long delay = ChronoUnit.MILLIS.between(lastInstant.get(0) != null ? lastInstant.get(0) : instant, instant);
                    if (delay > 15000)
                        delay = 15000;
                    lastInstant.set(0, instant);
                    if (delay < 0) {
                        delay = 0;
                    }
                    Thread.sleep(delay);

                    generalPublisher.send(topic, 2, objectMapper.writeValueAsBytes(message));
                } else {
                    logger.trace("Skipping message.");
                }
            }
            bufferedReader.close();
        } catch (IOException | InterruptedException e) {
            logger.error(e.getMessage());
        }
    }


}
