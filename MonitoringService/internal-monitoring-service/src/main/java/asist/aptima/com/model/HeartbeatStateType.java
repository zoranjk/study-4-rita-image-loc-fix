package asist.aptima.com.model;

public enum HeartbeatStateType {
	ok, warning, error
}
