package asist.aptima.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Serdeable
@Entity
@Table(name = "instance_state")
public class InstanceState {

	private static final Logger logger = LoggerFactory.getLogger(InstanceState.class);

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonProperty("id")
    private Long id;

    @Version
    @JsonIgnore
    private Long version;

    @NotNull
    @Column(name="host_ip", nullable=false, unique = true)
    @JsonProperty("host_ip")
    private String hostIp;

    @NotNull
    @Column(name="ready", nullable=false)
    @JsonProperty("ready")
    private String ready;

    @NotNull
    @Column(name="down", nullable=false)
    @JsonProperty("down")
    private String down;

    @NotNull
    @Column(name="down_reason", nullable=false)
    @JsonProperty("down_reason")
    private String downReason;


    public InstanceState() {}

    public InstanceState(
    		@NotNull String hostIp,
    		@NotNull String ready,
    		@NotNull String down,
    		@NotNull String downReason
                ) {
        this.hostIp = hostIp;
        this.ready = ready;
        this.down = down;
        this.downReason = downReason;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }

    public String getReady() {
        return ready;
    }

    public void setReady(String ready) {
        this.ready = ready;
    }

    public String getDown() {
        return down;
    }

    public void setDown(String down) {
        this.down = down;
    }

    public String getDownReason() {
        return downReason;
    }

    public void setDownReason(String downReason) {
        this.downReason = downReason;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\": \"" + id + "\"," +
                "\"host_ip\": \"" + hostIp + "\"," +
                "\"ready\": \"" + ready + "\"," +
                "\"down\": \"" + down + "\"," +
                "\"down_reason\": \"" + downReason + "\"" +
                "}";
    }
}
