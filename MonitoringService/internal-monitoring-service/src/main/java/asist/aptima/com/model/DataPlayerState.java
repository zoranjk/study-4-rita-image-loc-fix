package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataPlayerState {

	@JsonProperty("mission_timer")
	private String missionTimer;

	@JsonProperty("elapsed_milliseconds_global")
	private String elapsedMilliseconds;

	@JsonCreator
	public DataPlayerState(
			String missionTimer,
			String elapsedMilliseconds
			) {
		this.missionTimer = missionTimer;
		this.elapsedMilliseconds = elapsedMilliseconds;
	}

	public DataPlayerState() {
		// TODO Auto-generated constructor stub
	}

	public String getMissionTimer() {
		return missionTimer;
	}
	public void setMissionTimer(String missionTimer) {
		this.missionTimer = missionTimer;
	}

	public String getElapsedMilliseconds() {
		return elapsedMilliseconds;
	}
	public void setElapsedMilliseconds(String elapsedMilliseconds) {
		this.elapsedMilliseconds = elapsedMilliseconds;
	}

}
