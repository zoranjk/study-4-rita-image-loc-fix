package asist.aptima.com.publisher;

import java.util.concurrent.CompletableFuture;

import io.micronaut.mqtt.annotation.Qos;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.mqtt.v5.annotation.MqttPublisher;
import io.reactivex.Completable;

@MqttPublisher
public interface StatusPublisher {

	CompletableFuture<Void> send(@Topic String topic, @Qos int qos, byte[] data);
	
}