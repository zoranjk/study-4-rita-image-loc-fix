package asist.aptima.com.model;

public enum InstanceStateType {
	NOT_READY, WAITING_FOR_PLAYERS, PLAYERS_ALLOCATED, GAME_IN_PROGRESS, EXPORT_COMPLETED, FINISHED
}
