package asist.aptima.com.model;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class DataState {
	
	@JsonProperty("host_ip") 
    private String hostIp;

    @JsonProperty("state") 
    private HeartbeatStateType state;
    
	@JsonProperty("services") 
    private Map<String, DataStateHeartbeat> services;

    @JsonProperty("containers")
    private Map<String, DataStateContainer> containers;

    @JsonProperty("instance_info")
    private List<DataStateInstanceInfo> instanceInfo;

    @JsonProperty("instance_state")
    private DataStateInstanceState instanceState;

	@JsonCreator
	public DataState(
    		String hostIp,
    		HeartbeatStateType state,
    		Map<String, DataStateHeartbeat> services,
    		Map<String, DataStateContainer> containers,
            List<DataStateInstanceInfo> instanceInfo,
            DataStateInstanceState instanceState
			) {
        this.hostIp = hostIp;
        this.state = state;
        this.services = services;
        this.containers = containers;
        this.instanceInfo = instanceInfo;
        this.instanceState = instanceState;
	}
	
    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }
    
	public HeartbeatStateType getState() {
		return state;
	}
	public void setState(HeartbeatStateType state) {
		this.state = state;
	}
    
    public Map<String, DataStateHeartbeat> getServices() {
        return services;
    }

    public void setServices(Map<String, DataStateHeartbeat> services) {
        this.services = services;
    }

    public Map<String, DataStateContainer> getContainers() {
        return containers;
    }

    public void setContainers(Map<String, DataStateContainer> containers) {
        this.containers = containers;
    }

    public List<DataStateInstanceInfo> getInstanceInfo() {
        return instanceInfo;
    }

    public void setInstanceInfo(List<DataStateInstanceInfo> instanceInfo) {
        this.instanceInfo = instanceInfo;
    }

    public DataStateInstanceState getInstanceState() {
        return instanceState;
    }

    public void setInstanceState(DataStateInstanceState instanceState) {
        this.instanceState = instanceState;
    }
}
