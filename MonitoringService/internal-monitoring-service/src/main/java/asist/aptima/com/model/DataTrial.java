package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

import java.util.ArrayList;
import java.util.List;

@Serdeable
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataTrial {

	@JsonProperty("name")
	private String name;
	@JsonProperty("date")
	private String date;
	@JsonProperty("experimenter")
	private String experimenter;
	@JsonProperty("subjects")
	private List<String> subjects;
	@JsonProperty("trial_number")
	private String trialNumber;
	@JsonProperty("group_number")
	private String groupNumber;
	@JsonProperty("study_number")
	private String studyNumber;
	@JsonProperty("condition")
	private String condition;
	@JsonProperty("notes")
	private List<String> notes;
	@JsonProperty("testbed_version")
	private String testbedVersion;
	@JsonProperty("experiment_name")
	private String experimentName;
	@JsonProperty("experiment_date")
	private String experimentDate;
	@JsonProperty("experiment_author")
	private String experimentAuthor;
	@JsonProperty("experiment_mission")
	private String experimentMission;
	@JsonProperty("map_name")
	private String mapName;
	@JsonProperty("map_block_filename")
	private String mapBlockFilename;
	@JsonProperty("client_info")
	private List<ClientInfoItem> clientInfo;
	@JsonProperty("team_id")
	private String teamId;
	@JsonProperty("intervention_agents")
	private List<String> interventionAgents;
	@JsonProperty("observers")
	private List<String> observers;

	@JsonCreator
	public DataTrial(
			String name,
			String date,
			String experimenter,
			List<String> subjects,
			String trialNumber,
			String groupNumber,
			String studyNumber,
			String condition,
			List<String> notes,
			String testbedVersion,
			String experimentName,
			String experimentDate,
			String experimentAuthor,
			String experimentMission,
			String mapName,
			String mapBlockFilename,
			List<ClientInfoItem> clientInfo,
			String teamId,
			List<String> interventionAgents,
			List<String> observers
			) {
		this.name = name;
		this.date = date;
		this.experimenter = experimenter;
		this.subjects = subjects == null ? new ArrayList<String>() : subjects;
		this.trialNumber = trialNumber;
		this.groupNumber = groupNumber;
		this.studyNumber = studyNumber;
		this.condition = condition;
		this.notes = notes == null ? new ArrayList<String>() : notes;
		this.testbedVersion = testbedVersion;
		this.experimentName = experimentName;
		this.experimentDate = experimentDate;
		this.experimentAuthor = experimentAuthor;
		this.experimentMission = experimentMission;
		this.mapName = mapName;
		this.mapBlockFilename = mapBlockFilename;
		this.clientInfo = clientInfo;
		this.teamId = teamId;
		this.interventionAgents = interventionAgents;
		this.observers = observers;
	}

	public DataTrial() {
		// TODO Auto-generated constructor stub
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}

	public String getExperimenter() {
		return experimenter;
	}
	public void setExperimenter(String experimenter) {
		this.experimenter = experimenter;
	}

	public List<String> getSubjects() {
		return subjects == null ? new ArrayList<String>() : subjects;
	}
	public void setSubjects(List<String> subjects) {
		this.subjects = subjects == null ? new ArrayList<String>() : subjects;
	}

	public String getTrialNumber() {
		return trialNumber;
	}
	public void setTrialNumber(String trialNumber) {
		this.trialNumber = trialNumber;
	}

	public String getGroupNumber() {
		return groupNumber;
	}
	public void setGroupNumber(String groupNumber) {
		this.groupNumber = groupNumber;
	}

	public String getStudyNumber() {
		return studyNumber;
	}
	public void setStudyNumber(String studyNumber) {
		this.studyNumber = studyNumber;
	}

	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}

	public List<String> getNotes() {
		return notes == null ? new ArrayList<String>() : notes;
	}
	public void setNotes(List<String> notes) {
		this.notes = notes == null ? new ArrayList<String>() : notes;
	}

	public String getTestbedVersion() {
		return testbedVersion;
	}
	public void setTestbedVersion(String testbedVersion) {
		this.testbedVersion = testbedVersion;
	}

	public String getExperimentName() {
		return experimentName;
	}
	public void setExperimentName(String experimentName) {
		this.experimentName = experimentName;
	}

	public String getExperimentDate() {
		return experimentDate;
	}
	public void setExperimentDate(String experimentDate) {
		this.experimentDate = experimentDate;
	}

	public String getExperimentAuthor() {
		return experimentAuthor;
	}
	public void setExperimentAuthor(String experimentAuthor) {
		this.experimentAuthor = experimentAuthor;
	}

	public String getExperimentMission() {
		return experimentMission;
	}
	public void setExperimentMission(String experimentMission) {
		this.experimentMission = experimentMission;
	}

	public String getMapName() {
		return mapName;
	}
	public void setMapName(String mapName) {
		this.mapName = mapName;
	}

	public String getMapBlockFilename() {
		return mapBlockFilename;
	}
	public void setMapBlockFilename(String mapBlockFilename) {
		this.mapBlockFilename = mapBlockFilename;
	}

	public List<ClientInfoItem> getClientInfo() {
		return clientInfo;
	}
	public void setClientInfo(List<ClientInfoItem> clientInfo) {
		this.clientInfo = clientInfo;
	}

	public String getTeamId() {
		return teamId;
	}
	public void setTeamId(String teamId) {
		this.teamId = teamId;
	}

	public List<String> getInterventionAgents() {
		return interventionAgents == null ? new ArrayList<String>() : interventionAgents;
	}
	public void setInterventionAgents(List<String> interventionAgents) {
		this.interventionAgents = interventionAgents == null ? new ArrayList<String>() : interventionAgents;
	}

	public List<String> getObservers() {
		return observers == null ? new ArrayList<String>() : observers;
	}
	public void setObservers(List<String> observers) {
		this.observers = observers == null ? new ArrayList<String>() : observers;
	}
}
