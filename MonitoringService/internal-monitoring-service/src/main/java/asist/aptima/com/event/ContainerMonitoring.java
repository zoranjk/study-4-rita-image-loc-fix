package asist.aptima.com.event;

import io.micronaut.context.annotation.Property;
import io.micronaut.runtime.context.scope.Refreshable;

@Refreshable
public class ContainerMonitoring {

    @Property(name = "container.monitoring")
    private boolean CONTAINER_MONITORING;

    public boolean getContainerMonitoring() {
        return this.CONTAINER_MONITORING;
    }
}
