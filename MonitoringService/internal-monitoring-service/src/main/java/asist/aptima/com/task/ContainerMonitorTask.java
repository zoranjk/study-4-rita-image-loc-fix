package asist.aptima.com.task;

import asist.aptima.com.event.ContainerMonitoring;
import asist.aptima.com.repository.ContainerRepository;
import asist.aptima.com.usecase.ContainerSchedules;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.async.ResultCallbackTemplate;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Statistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.DecimalFormat;
import java.time.Instant;
import java.util.*;

public class ContainerMonitorTask implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(ContainerMonitorTask.class);

    private final DockerClient dockerClient;
    private final String containerId;
    private final ContainerSchedules schedules;
    private final ContainerRepository containerRepository;
    private final ContainerMonitoring containerMonitoring;

    public ContainerMonitorTask(DockerClient dockerClient, String containerId, ContainerSchedules schedules, ContainerRepository containerRepository, ContainerMonitoring containerMonitoring) {
        this.dockerClient = dockerClient;
        this.containerId = containerId;
        this.schedules = schedules;
        this.containerRepository = containerRepository;
        this.containerMonitoring = containerMonitoring;
    }

    @Override
    public void run() {
        List<Container> containers = dockerClient.listContainersCmd().withShowAll(true).withFilter("id", Collections.singleton(containerId)).exec();
        if (containers.size() == 0) {
            schedules.cancel(containerId);
            return;
        }
        Container container = containers.get(0);
        String containerName = container.getNames()[0].substring(1);
        String containerId = container.getId();
        try (StatsCallback statsCallback = dockerClient
                .statsCmd(containerId)
                .exec(new StatsCallback(containerId))) {
            statsCallback.awaitCompletion();
            logger.debug("Stop {} stats collection.", containerName);
            schedules.cancel(containerId);
        } catch (InterruptedException | IOException e) {
            throw new RuntimeException(e);
        }
        logger.debug("Completed {} stats collection!", containerName);
    }

    private class StatsCallback extends ResultCallbackTemplate<StatsCallback, Statistics> {
        private final Logger logger = LoggerFactory.getLogger(StatsCallback.class);
        private final DecimalFormat decimalFormat = new DecimalFormat("0.00");
        private final String containerId;
//        private int count = 1;
        public StatsCallback(String containerId) {
            this.containerId = containerId;
        }

        @Override
        public void onNext(Statistics stats) {
            List<Container> containers = dockerClient.listContainersCmd().withShowAll(true).withFilter("id", Collections.singleton(containerId)).exec();
//            if (containers.size() == 0) {
//                schedules.cancel(containerId);
//                return;
//            }
            Container container = containers.get(0);
            String containerName = container.getNames()[0].substring(1);
            // logger.info("Received stats [{}] #{}: {}",containerName , count++, stats);
            addContainer(container, stats);
        }

        private void addContainer(Container container, Statistics stats) {
            String containerId = container.getId();
            String containerName = container.getNames()[0].substring(1);
            String imageId = container.getImageId();
            String status = container.getStatus();
            String state = container.getState();
            long created = container.getCreated();
            String cpuPercent = "DISABLED";
            String memPercent = "DISABLED";

            if (containerMonitoring.getContainerMonitoring()) {
                if (state.equalsIgnoreCase("running")) {
                    // cpu_delta = cpu_stats.cpu_usage.total_usage - precpu_stats.cpu_usage.total_usage
                    // system_cpu_delta = cpu_stats.system_cpu_usage - precpu_stats.system_cpu_usage
                    // number_cpus = length(cpu_stats.cpu_usage.percpu_usage) or cpu_stats.online_cpus
                    // (cpu_delta / system_cpu_delta) * number_cpus * 100.0
                    long cpu_delta = (stats.getCpuStats().getCpuUsage() != null ? (stats.getCpuStats().getCpuUsage().getTotalUsage() != null ? stats.getCpuStats().getCpuUsage().getTotalUsage() : 0) : 0) -
                            (stats.getPreCpuStats().getCpuUsage() != null ? (stats.getPreCpuStats().getCpuUsage().getTotalUsage() != null ? stats.getPreCpuStats().getCpuUsage().getTotalUsage() : 0) : 0);
                    long system_cpu_delta = (stats.getCpuStats().getSystemCpuUsage() != null ? stats.getCpuStats().getSystemCpuUsage() : 0) -
                            (stats.getPreCpuStats().getSystemCpuUsage() != null ? stats.getPreCpuStats().getSystemCpuUsage() : 0);
                    long number_cpus = stats.getCpuStats().getOnlineCpus() != null ? stats.getCpuStats().getOnlineCpus() : (stats.getCpuStats().getCpuUsage().getPercpuUsage() != null ? stats.getCpuStats().getCpuUsage().getPercpuUsage().size() : 1);
                    cpuPercent = decimalFormat.format(((double) cpu_delta / system_cpu_delta) * (number_cpus * 100.0));

                    // used_memory = memory_stats.usage - memory_stats.stats.cache
                    // available_memory = memory_stats.limit
                    // (used_memory / available_memory) * 100.0
                    long used_memory = (stats.getMemoryStats().getUsage() != null ? stats.getMemoryStats().getUsage() : 0) -
                            (stats.getMemoryStats().getStats() != null ? (stats.getMemoryStats().getStats().getCache() != null ? stats.getMemoryStats().getStats().getCache() : 0) : 0);
                    long available_memory = stats.getMemoryStats().getLimit() != null ? stats.getMemoryStats().getLimit() : 0;
                    memPercent = decimalFormat.format(((double) used_memory / available_memory) * 100.0);
                }

                Optional<asist.aptima.com.domain.Container> existingDomainContainer = containerRepository.findByName(containerName);
                if (existingDomainContainer.isEmpty()) {
                    logger.debug("ADDING stats for [{}]", containerName);
                    asist.aptima.com.domain.Container newContainer = new asist.aptima.com.domain.Container(
                            containerId,
                            containerName,
                            imageId,
                            status,
                            state,
                            Instant.ofEpochSecond(created).toString(),
                            Instant.now().toString(),
                            cpuPercent,
                            memPercent
                    );
                    containerRepository.save(newContainer);
                } else {
                    logger.debug("UPDATING stats for [{}]", containerName);
                    asist.aptima.com.domain.Container updatedContainer = existingDomainContainer.get();
                    updatedContainer.setContainerId(containerId);
                    updatedContainer.setContainerName(containerName);
                    updatedContainer.setImageId(imageId);
                    updatedContainer.setStatus(status);
                    updatedContainer.setState(state);
                    updatedContainer.setCreated(Instant.ofEpochSecond(created).toString());
                    updatedContainer.setTimestamp(Instant.now().toString());
                    updatedContainer.setCpuPercent(cpuPercent);
                    updatedContainer.setMemPercent(memPercent);
                    containerRepository.update(updatedContainer);
                }
            } else {
//                if (schedules.schedulesSize() > 0) {
//                    schedules.cancelAll();
//                    containerRepository.deleteAll();
//                }
            }
        }
    }
}
