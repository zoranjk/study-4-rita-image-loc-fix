package asist.aptima.com.publisher;

import java.util.concurrent.CompletableFuture;

import org.reactivestreams.Publisher;

import io.micronaut.core.async.subscriber.Completable;
import io.micronaut.mqtt.annotation.Qos;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.mqtt.v5.annotation.MqttPublisher;

@MqttPublisher
public interface ErrorLogPublisher {

	@Topic(value = "${error.topic}", qos = 1)
	//@MqttProperty(name = "contentType", value = "text/plain")
	CompletableFuture<Void> send(byte[] data);
	
	@Topic("${error.topic}")
	//@MqttProperty(name = "contentType", value = "text/plain")
	CompletableFuture<Void> send(byte[] data, @Qos int qos);
	
}