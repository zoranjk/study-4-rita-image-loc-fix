package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

import java.util.ArrayList;
import java.util.List;

@Serdeable
public class DataStateInstanceInfo {

    @JsonProperty("trial_id")
    private String trialId;

    @JsonProperty("trial_name")
    private String trialName;

    @JsonProperty("trial_created")
    private String trialCreated;

    @JsonProperty("experimenter")
    private String experimenter;

    @JsonProperty("subjects")
    private List<String> subjects;

    @JsonProperty("trial_number")
    private String trialNumber;

	@JsonProperty("group_number")
    private String groupNumber;

	@JsonProperty("study_number")
    private String studyNumber;

	@JsonProperty("condition")
    private String condition;

    @JsonProperty("notes")
    private List<String> notes;

    @JsonProperty("experiment_id")
    private String experimentId;

    @JsonProperty("experiment_name")
    private String experimentName;

    @JsonProperty("experiment_created")
    private String experimentCreated;

    @JsonProperty("experiment_author")
    private String experimentAuthor;

    @JsonProperty("experiment_mission")
    private String experimentMission;

    @JsonProperty("map_name")
    private String mapName;

    @JsonProperty("map_block_filename")
    private String mapBlockFilename;

    @JsonProperty("players")
    private List<ClientInfoItem> players;

    @JsonProperty("team_id")
    private String teamId;

    @JsonProperty("intervention_agents")
    private List<String> interventionAgents;

    @JsonProperty("observers")
    private List<String> observers;

    @JsonProperty("trial_start")
    private String trialStart;

    @JsonProperty("trial_stop")
    private String trialStop;

    @JsonProperty("auto_export_begin")
    private String autoExportBegin;

    @JsonProperty("auto_export_end")
    private String autoExportEnd;

    @JsonProperty("mission_timer")
    private String missionTimer;

    @JsonProperty("mission_elapsed_milliseconds")
    private String missionElapsedMilliseconds;

    @JsonProperty("mission_name")
    private String missionName;

    @JsonProperty("mission_state")
    private String missionState;

    @JsonProperty("mission_state_change_outcome")
    private String missionStateChangeOutcome;

	@JsonCreator
	public DataStateInstanceInfo(
    		String trialId,
            String trialName,
            String trialCreated,
            String experimenter,
            List<String> subjects,
            String trialNumber,
    		String groupNumber,
    		String studyNumber,
    		String condition,
            List<String> notes,
    		String experimentId,
    		String experimentName,
    		String experimentCreated,
    		String experimentAuthor,
    		String experimentMission,
    		String mapName,
    		String mapBlockFilename,
            List<ClientInfoItem> players,
            String teamId,
            List<String> interventionAgents,
            List<String> observers,
            String trialStart,
            String trialStop,
            String autoExportBegin,
            String autoExportEnd,
            String missionTimer,
            String missionElapsedMilliseconds,
            String missionName,
            String missionState,
            String missionStateChangeOutcome
			) {
        this.trialId = trialId;
        this.trialName = trialName;
        this.trialCreated = trialCreated;
        this.experimenter = experimenter;
        this.subjects = subjects;
        this.trialNumber = trialNumber;
        this.groupNumber = groupNumber;
        this.studyNumber = studyNumber;
        this.condition = condition;
        this.notes = notes;
        this.experimentId = experimentId;
        this.experimentName = experimentName;
        this.experimentCreated = experimentCreated;
        this.experimentAuthor = experimentAuthor;
        this.experimentMission = experimentMission;
        this.mapName = mapName;
        this.mapBlockFilename = mapBlockFilename;
        this.players = players;
        this.teamId = teamId;
        this.interventionAgents = interventionAgents;
        this.observers = observers;
        this.trialStart = trialStart;
        this.trialStop = trialStop;
        this.autoExportBegin = autoExportBegin;
        this.autoExportEnd = autoExportEnd;
        this.missionTimer = missionTimer;
        this.missionElapsedMilliseconds = missionElapsedMilliseconds;
        this.missionName = missionName;
        this.missionState = missionState;
        this.missionStateChangeOutcome = missionStateChangeOutcome;
	}

    public String getTrialId() {
        return trialId;
    }

    public void setTrialId(String trialId) {
        this.trialId = trialId;
    }

    public String getTrialName() {
        return trialName;
    }

    public void setTrialName(String trialName) {
        this.trialName = trialName;
    }

    public String getTrialCreated() {
        return trialCreated;
    }

    public void setTrialCreated(String trialCreated) {
        this.trialCreated = trialCreated;
    }

    public String getExperimenter() {
        return experimenter;
    }

    public void setExperimenter(String experimenter) {
        this.experimenter = experimenter;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public String getTrialNumber() {
        return trialNumber;
    }

    public void setTrialNumber(String trialNumber) {
        this.trialNumber = trialNumber;
    }

    public String getGroupNumber() {
        return groupNumber;
    }

    public void setGroupNumber(String groupNumber) {
        this.groupNumber = groupNumber;
    }

    public String getStudyNumber() {
        return studyNumber;
    }

    public void setStudyNumber(String studyNumber) {
        this.studyNumber = studyNumber;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public List<String> getNotes() {
        return notes;
    }

    public void setNotes(List<String> notes) {
        this.notes = notes;
    }

    public String getExperimentId() {
        return experimentId;
    }

    public void setExperimentId(String experimentId) {
        this.experimentId = experimentId;
    }

    public String getExperimentName() {
        return experimentName;
    }

    public void setExperimentName(String experimentName) {
        this.experimentName = experimentName;
    }

    public String getExperimentCreated() {
        return experimentCreated;
    }

    public void setExperimentCreated(String experimentCreated) {
        this.experimentCreated = experimentCreated;
    }

    public String getExperimentAuthor() {
        return experimentAuthor;
    }

    public void setExperimentAuthor(String experimentAuthor) {
        this.experimentAuthor = experimentAuthor;
    }

    public String getExperimentMission() {
        return experimentMission;
    }

    public void setExperimentMission(String experimentMission) {
        this.experimentMission = experimentMission;
    }

    public String getMapName() {
        return mapName;
    }

    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    public String getMapBlockFilename() {
        return mapBlockFilename;
    }

    public void setMapBlockFilename(String mapBlockFilename) {
        this.mapBlockFilename = mapBlockFilename;
    }

    public List<ClientInfoItem> getPlayers() {
        return players;
    }

    public void setPlayers(List<ClientInfoItem> players) {
        this.players = players;
    }

    public String getTeamId() {
        return teamId;
    }
    public void setTeamId(String teamId) {
        this.teamId = teamId;
    }

    public List<String> getInterventionAgents() {
        return interventionAgents == null ? new ArrayList<String>() : interventionAgents;
    }
    public void setInterventionAgents(List<String> interventionAgents) {
        this.interventionAgents = interventionAgents == null ? new ArrayList<String>() : interventionAgents;
    }

    public List<String> getObservers() {
        return observers == null ? new ArrayList<String>() : observers;
    }
    public void setObservers(List<String> observers) {
        this.observers = observers == null ? new ArrayList<String>() : observers;
    }

    public String getTrialStart() {
        return trialStart;
    }

    public void setTrialStart(String trialStart) {
        this.trialStart = trialStart;
    }

    public String getTrialStop() {
        return trialStop;
    }

    public void setTrialStop(String trialStop) {
        this.trialStop = trialStop;
    }

    public String getAutoExportBegin() {
        return autoExportBegin;
    }

    public void setAutoExportBegin(String autoExportBegin) {
        this.autoExportBegin = autoExportBegin;
    }

    public String getAutoExportEnd() {
        return autoExportEnd;
    }

    public void setAutoExportEnd(String autoExportEnd) {
        this.autoExportEnd = autoExportEnd;
    }

    public String getMissionTimer() {
        return missionTimer;
    }

    public void setMissionTimer(String missionTimer) {
        this.missionTimer = missionTimer;
    }

    public String getMissionElapsedMilliseconds() {
        return missionElapsedMilliseconds;
    }

    public void setMissionElapsedMilliseconds(String missionElapsedMilliseconds) {
        this.missionElapsedMilliseconds = missionElapsedMilliseconds;
    }

    public String getMissionName() {
        return missionName;
    }

    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }

    public String getMissionState() {
        return missionState;
    }

    public void setMissionState(String missionState) {
        this.missionState = missionState;
    }

    public String getMissionStateChangeOutcome() {
        return missionStateChangeOutcome;
    }

    public void setMissionStateChangeOutcome(String missionStateChangeOutcome) {
        this.missionStateChangeOutcome = missionStateChangeOutcome;
    }

}
