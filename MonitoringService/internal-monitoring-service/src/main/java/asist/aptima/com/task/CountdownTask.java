package asist.aptima.com.task;

import asist.aptima.com.model.HeartbeatStateType;
import asist.aptima.com.usecase.CountdownSchedules;
import asist.aptima.com.usecase.CountdownUseCase;
import asist.aptima.com.usecase.HeartbeatUseCase;
import jakarta.inject.Inject;

public class CountdownTask implements Runnable {

    private final CountdownUseCase countdownUseCase;
    private final String source;
    private final CountdownSchedules schedules;

    public CountdownTask(CountdownUseCase countdownUseCase, String source, CountdownSchedules schedules) {
        this.source = source;
        this.countdownUseCase = countdownUseCase;
        this.schedules = schedules;
    }

    @Override
    public void run() {
    	HeartbeatStateType heartbeatStateType = countdownUseCase.update(source);
    	if (heartbeatStateType.equals(HeartbeatStateType.error)) {
    		schedules.cancel(source);
    	}
    }
}
