package asist.aptima.com.subscriber;

import asist.aptima.com.domain.InstanceInfo;
import asist.aptima.com.model.MessagePlayerState;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.repository.InstanceInfoRepository;
import io.micronaut.mqtt.annotation.MqttSubscriber;
import io.micronaut.mqtt.annotation.Topic;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import io.micronaut.serde.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;

@ExecuteOn(TaskExecutors.IO)
@MqttSubscriber
public class PlayerStateSubscriber {

	private static final Logger logger = LoggerFactory.getLogger(PlayerStateSubscriber.class);
	private final ObjectMapper objectMapper;
	private final InstanceInfoRepository instanceInfoRepository;

	public PlayerStateSubscriber(ObjectMapper objectMapper, HeartbeatRepository heartbeatRepository, InstanceInfoRepository instanceStateRepository) {
		this.objectMapper = objectMapper;
		this.instanceInfoRepository = instanceStateRepository;
	}

    @Topic("player/state")
    public void receive(byte[] data, @Topic String topic) {
		MessagePlayerState messagePlayerStateChange;
		try {
			String message = new String(data, StandardCharsets.UTF_8);
			messagePlayerStateChange = objectMapper.readValue(message, MessagePlayerState.class);

			String trialId = messagePlayerStateChange.getMsg().getTrialId();
			String missionTimer = messagePlayerStateChange.getData().getMissionTimer();
			String elapsedMilliseconds = messagePlayerStateChange.getData().getElapsedMilliseconds();
			Optional<InstanceInfo> instanceInfoOptional = instanceInfoRepository.findByTrialId(trialId);
			if (instanceInfoOptional.isEmpty()) {
				logger.error("Trial ID [{}] does not exist for Instance Info!", trialId);
			} else {
				logger.info("Updating Mission Timer [{}] for [{}].", missionTimer, trialId);
				InstanceInfo instanceInfo = instanceInfoOptional.get();
				instanceInfo.setMissionTimer(missionTimer);
				instanceInfo.setMissionElapsedMilliseconds(elapsedMilliseconds);
				instanceInfoRepository.update(instanceInfo);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			logger.error(e.getMessage());
		}
    }
}
