package asist.aptima.com.configuration;

import io.micronaut.context.annotation.ConfigurationProperties;

@ConfigurationProperties("application") 
public class ApplicationConfigurationProperties implements ApplicationConfiguration {

    private final int DEFAULT_MAX = 500;

    private int max = DEFAULT_MAX;

    @Override
    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }
}