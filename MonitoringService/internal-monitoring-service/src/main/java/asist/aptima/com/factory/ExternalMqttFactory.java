package asist.aptima.com.factory;

import java.time.LocalTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hivemq.client.mqtt.MqttClient;
import com.hivemq.client.mqtt.mqtt5.Mqtt5AsyncClient;
import asist.aptima.com.configuration.ExternalMqttConfigurationProperties;
import asist.aptima.com.subscriber.HeartbeatSubscriber;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;
import jakarta.inject.Singleton;

@Factory
public class ExternalMqttFactory {
	
	private static final Logger logger = LoggerFactory.getLogger(ExternalMqttFactory.class);
	
	@Singleton
	Mqtt5AsyncClient client(ExternalMqttConfigurationProperties configuration) {
		logger.debug("External MQTT Client configuration: id [{}] host [{}] port [{}]", configuration.getClientId(), configuration.getServerHost(), configuration.getServerPort());
		Mqtt5AsyncClient client = MqttClient.builder()
				.identifier(configuration.getClientId())
				.serverHost(configuration.getServerHost())
				.serverPort(configuration.getServerPort())
				.useMqttVersion5()
				.buildAsync();
		return client;
		//.addConnectedListener(
		//	context -> System.out.println("connected  " + LocalTime.now() + " host: " + context.getClientConfig().getServerHost()
 		//			+ " client: " + context.getClientConfig().getClientIdentifier()))
		//.addDisconnectedListener(context -> System.out.println("disconnected - " + " cause: " + context.getCause() + " source:" + context.getSource() + " " + LocalTime.now()))
		
	}
}
