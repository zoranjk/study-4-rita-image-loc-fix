package asist.aptima.com.controller;

import asist.aptima.com.domain.Heartbeat;
import asist.aptima.com.repository.HeartbeatRepository;
import asist.aptima.com.service.TrialSimulator;
import asist.aptima.com.utility.SortingAndOrderArgumentsHeartbeat;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.multipart.CompletedFileUpload;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@ExecuteOn(TaskExecutors.IO)
@Controller("/file")
public class FileController {

    private final TrialSimulator trialSimulator;

    FileController(TrialSimulator trialSimulator) {
        this.trialSimulator = trialSimulator;
    }

    @Post("/simulate")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public HttpResponse<?> simulateFile(CompletedFileUpload file) {
        try {
            trialSimulator.simulateTrial(file.getBytes(), file.getFilename());
            return HttpResponse.ok("Simulated!");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return HttpResponse.serverError("Error!");
        }
    }
}
