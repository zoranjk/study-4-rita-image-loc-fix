package asist.aptima.com.repository;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import asist.aptima.com.domain.Service;
import asist.aptima.com.utility.SortingAndOrderArgumentsService;

public interface ServiceRepository {

    Optional<Service> findById(long id);    
	Optional<Service> findBySource(String source);
    void deleteById(long id);
    List<Service> findAll(@NotNull SortingAndOrderArgumentsService args);
    Service save(@NotBlank Service service);
    Service update(@NotBlank Service service);
}
