package asist.aptima.com.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class MessageHosts {
	
	@JsonProperty("header") 
	private Header header;
	@JsonProperty("msg") 
	private Msg msg;
	@JsonProperty("data") 
	private DataHosts data;

	@JsonCreator
	public MessageHosts(
			Header header,
			Msg msg,
			DataHosts data
			) {
		this.header = header;
		this.msg = msg;
		this.data = data;
	}

	public Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	
	public Msg getMsg() {
		return msg;
	}
	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	public DataHosts getData() {
		return data;
	}
	public void setData(DataHosts data) {
		this.data = data;
	}
}