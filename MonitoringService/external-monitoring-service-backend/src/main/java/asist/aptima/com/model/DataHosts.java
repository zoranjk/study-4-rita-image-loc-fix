package asist.aptima.com.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;

import asist.aptima.com.domain.Host;
import io.micronaut.core.annotation.Introspected;
import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class DataHosts {
	
	@JsonProperty("hosts") 
    private List<DataHost> hosts;
	
	@JsonCreator
	public DataHosts(
			List<DataHost> hosts
			) {
		
        this.hosts = hosts;
        //this.hosts = List.copyOf(hosts);
	}

    public List<DataHost> getHosts() {
        return hosts;
    }
    public void setHost(List<DataHost> hosts) {
    	this.hosts = hosts;
    }
	
}
