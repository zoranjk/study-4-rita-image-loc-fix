package asist.aptima.com.model.model;

import asist.aptima.com.model.DataStatusError;
import asist.aptima.com.model.Header;
import asist.aptima.com.model.Msg;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class MessageStatusError {
	
	@JsonProperty("header")
	private asist.aptima.com.model.Header header;
	@JsonProperty("msg")
	private Msg msg;
	@JsonProperty("data")
	private DataStatusError data;

	@JsonCreator
	public MessageStatusError(
			asist.aptima.com.model.Header header,
			Msg msg,
			DataStatusError data
			) {
		this.header = header;
		this.msg = msg;
		this.data = data;
	}

	public asist.aptima.com.model.Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}
	
	public Msg getMsg() {
		return msg;
	}
	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	public DataStatusError getData() {
		return data;
	}
	public void setData(DataStatusError data) {
		this.data = data;
	}
}
