package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.serde.annotation.Serdeable;

import java.util.List;
import java.util.Map;

@Serdeable
public class DataHost {
	
	@JsonProperty("host_ip") 
    private String hostIp;

    @JsonProperty("state") 
    private HeartbeatStateType state;

    @JsonProperty("services")
    private Map<String, DataService> services;

    @JsonProperty("containers")
    private Map<String, DataContainer> containers;

    @JsonProperty("instance_info")
    private List<DataInstanceInfo> instanceInfo;

    @JsonProperty("instance_state")
    private DataInstanceState instanceState;


    @JsonCreator
	public DataHost(
    		String hostIp,
    		HeartbeatStateType state,
            @Nullable Map<String, DataService> services,
            @Nullable Map<String, DataContainer> containers,
            @Nullable List<DataInstanceInfo> instanceInfo,
            @Nullable DataInstanceState instanceState
			) {
        this.hostIp = hostIp;
        this.state = state;
        this.services = services;
        this.containers = containers;
        this.instanceInfo = instanceInfo;
        this.instanceState = instanceState;
	}
	
    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }
    
	public HeartbeatStateType getState() {
		return state;
	}
	public void setState(HeartbeatStateType state) {
		this.state = state;
	}
    
    public Map<String, DataService> getServices() {
        return services;
    }

    public void setServices(Map<String, DataService> services) {
        this.services = services;
    }

    public Map<String, DataContainer> getContainers() {
        return containers;
    }

    public void setContainers(Map<String, DataContainer> containers) {
        this.containers = containers;
    }

    public List<DataInstanceInfo> getInstanceInfo() {
        return instanceInfo;
    }

    public void setInstanceInfo(List<DataInstanceInfo> instanceInfo) {
        this.instanceInfo = instanceInfo;
    }

    public DataInstanceState getInstanceState() {
        return instanceState;
    }

    public void setInstanceState(DataInstanceState instanceState) {
        this.instanceState = instanceState;
    }
}
