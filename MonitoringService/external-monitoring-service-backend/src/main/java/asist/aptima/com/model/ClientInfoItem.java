package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
@JsonIgnoreProperties(ignoreUnknown = true)
public class ClientInfoItem {

	@JsonProperty("callsign")
	private String callsign;
	@JsonProperty("participant_id")
	private String participantId;
	
	@JsonCreator
	public ClientInfoItem(
			String callsign,
			String participantId
			) {
        this.callsign = callsign;
        this.participantId = participantId;
    }

	public String getCallsign() {
		return callsign;
	}
	public void setCallsign(String callsign) {
		this.callsign = callsign;
	}

	public String getParticipantId() {
		return participantId;
	}
	public void setParticipantId(String participantId) {
		this.participantId = participantId;
	}
}
