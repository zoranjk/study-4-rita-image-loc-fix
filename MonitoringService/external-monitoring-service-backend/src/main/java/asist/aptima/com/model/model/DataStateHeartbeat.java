package asist.aptima.com.model.model;

import asist.aptima.com.model.HeartbeatStateType;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class DataStateHeartbeat {
	
	@JsonProperty("source") 
    private String source;

	@JsonProperty("timestamp") 
    private String timestamp;
    
	@JsonProperty("message_type") 
    private String messageType;
    
	@JsonProperty("sub_type") 
    private String subType;
    
    @JsonProperty("state") 
    private HeartbeatStateType state;
    
    @JsonProperty("topic") 
    private String topic;

	@JsonCreator
	public DataStateHeartbeat(
    		String source,
    		String timestamp,
    		String messageType,
    		String subType,
    		HeartbeatStateType state,
    		String topic
			) {
        this.source = source;
        this.timestamp = timestamp;
        this.messageType = messageType;
        this.subType = subType;
        this.state = state;
        this.topic = topic;
	}
	
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
    
    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }
    
    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }
    
	public HeartbeatStateType getState() {
		return state;
	}
	public void setState(HeartbeatStateType state) {
		this.state = state;
	}
    
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }
}
