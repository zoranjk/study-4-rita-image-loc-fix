package asist.aptima.com.model.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class HealthStatus {
	
	@JsonProperty("name") 
    private String name;

    @JsonProperty("status") 
    private String status;

	@JsonCreator
	public HealthStatus(
    		String name,
    		String status
			) {
        this.name = name;
        this.status = status;
	}
	
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

}
