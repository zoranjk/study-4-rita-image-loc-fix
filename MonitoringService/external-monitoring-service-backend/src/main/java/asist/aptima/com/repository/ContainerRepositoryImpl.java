package asist.aptima.com.repository;

import asist.aptima.com.configuration.ApplicationConfiguration;
import asist.aptima.com.domain.Container;
import asist.aptima.com.domain.Service;
import asist.aptima.com.utility.SortingAndOrderArgumentsContainer;
import io.micronaut.context.ApplicationContext;
import io.micronaut.context.env.PropertySource;
import io.micronaut.core.util.CollectionUtils;
import io.micronaut.runtime.context.scope.refresh.RefreshEvent;
import io.micronaut.transaction.annotation.ReadOnly;
import jakarta.inject.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Singleton 
public class ContainerRepositoryImpl implements ContainerRepository {

    private static final Logger logger = LoggerFactory.getLogger(ContainerRepositoryImpl.class);

    private static final List<String> VALID_PROPERTY_NAMES = Arrays.asList("containerId", "containerName", "imageId", "status", "state", "created", "timestamp", "cpuPercent", "memPercent");

    private final EntityManager entityManager;
    private final ApplicationConfiguration applicationConfiguration;

    public ContainerRepositoryImpl(EntityManager entityManager, ApplicationConfiguration applicationConfiguration) {
        this.entityManager = entityManager;
        this.applicationConfiguration = applicationConfiguration;
    }

    @Override
    @ReadOnly 
    public Optional<Container> findById(long id) {
        return Optional.ofNullable(entityManager.find(Container.class, id));
    }
    
    @Override
    @ReadOnly 
    public Optional<Container> findByName(String name) {
        try {
            return entityManager.createQuery("SELECT c FROM Container as c WHERE c.containerName = :name", Container.class)
                    .setParameter("name", name)
                    .getResultList()
                    .stream()
                    .findFirst();
        } catch (Exception e) {
            logger.error(e.getMessage());
            return Optional.empty();
        }
    }

//    @Override
//    @Transactional
//    public Container save(
//            @NotBlank String containerId,
//            @NotBlank String containerName,
//            @NotBlank String imageId,
//            @NotBlank String status,
//            @NotBlank String state,
//            @NotBlank String created,
//            @NotBlank String timestamp,
//            @NotBlank String cpuPercent,
//            @NotBlank String memPercent
//    ) {
//        try {
//            Container container = new Container(
//                    containerId,
//                    containerName,
//                    imageId,
//                    status,
//                    state,
//                    created,
//                    timestamp,
//                    cpuPercent,
//                    memPercent
//            );
//            entityManager.persist(container);
//            return container;
//        } catch (Exception e) {
//            logger.error(e.getMessage());
//            return null;
//        }
//    }

    @Override
    @Transactional
    public Container save(@NotBlank Container container) {
        try {
            entityManager.persist(container);
            return container;
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
    }

//    @Override
//    @Transactional
//    public int update(
//            @NotBlank long id,
//            @NotBlank String containerId,
//            @NotBlank String containerName,
//            @NotBlank String imageId,
//            @NotBlank String status,
//            @NotBlank String state,
//            @NotBlank String created,
//            @NotBlank String timestamp,
//            @NotBlank String cpuPercent,
//            @NotBlank String memPercent) {
//        return entityManager.createQuery("UPDATE Container c SET container_id = :containerId, container_name = :containerName, image_id = :imageId, status = :status, state = :state, created = :created, timestamp = :timestamp , cpu_percent = :cpuPercent , mem_percent = :memPercent  where id = :id")
//                .setParameter("containerId", containerId)
//                .setParameter("containerName", containerName)
//                .setParameter("imageId", imageId)
//                .setParameter("status", status)
//                .setParameter("state", state)
//                .setParameter("created", created)
//                .setParameter("timestamp", timestamp)
//                .setParameter("cpuPercent", cpuPercent)
//                .setParameter("memPercent", memPercent)
//                .setParameter("id", id)
//                .executeUpdate();
//    }
@Override
@Transactional
public Container update(@NotBlank Container container) {
    try {
        return entityManager.merge(container);
    } catch (Exception e) {
        logger.error(e.getMessage());
        return null;
    }
}

    @Override
    @Transactional 
    public void deleteById(long id) {
        findById(id).ifPresent(entityManager::remove);
    }

    @Override
    @Transactional
    public void deleteAll() {
        try {
            entityManager.createQuery("DELETE FROM Container").executeUpdate();
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
    }

    @Override
    @ReadOnly
    public List<Container> findAll(@NotNull SortingAndOrderArgumentsContainer args) {
        String qlString = "SELECT c FROM Container as c";
        if (args.getOrder().isPresent() && args.getSort().isPresent() && VALID_PROPERTY_NAMES.contains(args.getSort().get())) {
            qlString += " ORDER BY h." + args.getSort().get() + ' ' + args.getOrder().get().toLowerCase();
        }
        TypedQuery<Container> query = entityManager.createQuery(qlString, Container.class);
        query.setMaxResults(args.getMax().orElseGet(applicationConfiguration::getMax));
        args.getOffset().ifPresent(query::setFirstResult);

        return query.getResultList();
    }

}
