package asist.aptima.com.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.micronaut.serde.annotation.Serdeable;


@Serdeable
@Entity
@Table(name = "container", uniqueConstraints={
	    @UniqueConstraint(columnNames = {"container_name", "host_fk"}) })
public class Container {

	private static final Logger logger = LoggerFactory.getLogger(Container.class);

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @JsonIgnore
    private Long id;

    @Version
    @JsonIgnore
    private Long version;

    @NotNull
    @Column(name="container_id", nullable=false)
    private String containerId;

    @NotNull
    @Column(name="container_name", nullable=false)
    private String containerName;

    @NotNull
    @Column(name="image_id", nullable=false)
    private String imageId;

    @NotNull
    @Column(name="status", nullable=false)
    private String status;

    @NotNull
    @Column(name="state", nullable=false)
    private String state;

    @NotNull
    @Column(name="created", nullable=false)
    private String created;

    @NotNull
    @Column(name="timestamp", nullable=false)
    private String timestamp;

    @NotNull
    @Column(name="cpu_percent", nullable=false)
    private String cpuPercent;

    @NotNull
    @Column(name="mem_percent", nullable=false)
    private String memPercent;

    @ManyToOne
    @JsonBackReference
    @JoinColumn(name="host_fk")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Host host;

    public Container() {}

    public Container(
            @NotNull String containerId,
            @NotNull String containerName,
            @NotNull String imageId,
            @NotNull String status,
            @NotNull String state,
            @NotNull String created,
            @NotNull String timestamp,
            @NotNull String cpuPercent,
            @NotNull String memPercent
    ) {
        this.containerId = containerId;
        this.containerName = containerName;
        this.imageId = imageId;
        this.status = status;
        this.state = state;
        this.created = created;
        this.timestamp = timestamp;
        this.cpuPercent = cpuPercent;
        this.memPercent = memPercent;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContainerId() {
        return containerId;
    }

    public void setContainerId(String containerId) {
        this.containerId = containerId;
    }

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCpuPercent() {
        return cpuPercent;
    }

    public void setCpuPercent(String cpuPercent) {
        this.cpuPercent = cpuPercent;
    }

    public String getMemPercent() {
        return memPercent;
    }

    public void setMemPercent(String memPercent) {
        this.memPercent = memPercent;
    }

    public Host getHost() {
        return host;
    }

    public void setHost(Host host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "{" +
                "\"id\": \"" + id + "\"," +
                "\"container_id\": \"" + containerId + "\"," +
                "\"container_name\": \"" + containerName + "\"," +
                "\"image_id\": \"" + imageId + "\"," +
                "\"status\": \"" + status + "\"," +
                "\"state\": \"" + state + "\"," +
                "\"created\": \"" + created + "\"," +
                "\"timestamp\": \"" + timestamp + "\"," +
                "\"cpu_percent\": \"" + cpuPercent + "\"," +
                "\"mem_percent\": \"" + memPercent + "\"," +
                "\"host\": \"" + host + "\"" +
                "}";
    }
}
