package asist.aptima.com.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import io.micronaut.serde.annotation.Serdeable;
//import jakarta.json.bind.annotation.JsonbCreator;
//import jakarta.json.bind.annotation.JsonbProperty;

@Serdeable
public class Header {
	
	@JsonProperty("timestamp")
	private String timestamp;
	@JsonProperty("message_type")
	private String messageType;
	@JsonProperty("version")
	private String version;
	
	@JsonCreator
	public Header(
			String timestamp,
			String messageType,
			String version
			) {
        this.timestamp = timestamp;
        this.messageType = messageType;
        this.version = version;
    }
	
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
	
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
}
