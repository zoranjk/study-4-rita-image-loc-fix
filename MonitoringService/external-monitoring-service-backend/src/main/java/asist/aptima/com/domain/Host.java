package asist.aptima.com.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.hypersistence.utils.hibernate.type.json.JsonType;
import io.micronaut.serde.annotation.Serdeable;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

import static javax.persistence.GenerationType.AUTO;

@Serdeable
@Entity
@Table(name = "host")
@TypeDef(name = "json", typeClass = JsonType.class)
public class Host {

    @Id
    @GeneratedValue(strategy = AUTO)
    @JsonIgnore
    private Long id;

    @Version
    @JsonIgnore
    private Long version;

    @NotNull
    @Column(name = "host_ip", nullable = false, unique = true)
    @JsonProperty("host_ip")
    private String hostIp;
    
    @NotNull
    @Column(name = "state", nullable = false)
    @JsonProperty("state")
    private String state;

    @Type(type = "json")
    @Column(name = "instance_info", columnDefinition = "json")
    @JsonProperty("instance_info")
    private String instanceInfo;

    @Type(type = "json")
    @Column(name = "instance_state", columnDefinition = "json")
    @JsonProperty("instance_state")
    private String instanceState;

//    @JsonIgnore
    @JsonManagedReference
    @OneToMany(mappedBy = "host", orphanRemoval = true, cascade = CascadeType.ALL)
    @JsonProperty("services")
    private Set<Service> services = new HashSet<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "host", orphanRemoval = true, cascade = CascadeType.ALL)
    @JsonProperty("containers")
    private Set<Container> containers = new HashSet<>();

    public Host() {}

    public Host(@NotNull String hostIp, @NotNull String state) {
        this.hostIp = hostIp;
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp;
    }
    
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getInstanceInfo() {
        return instanceInfo;
    }

    public void setInstanceInfo(String instanceInfo) {
        this.instanceInfo = instanceInfo;
    }

    public String getInstanceState() {
        return instanceState;
    }

    public void setInstanceState(String instanceState) {
        this.instanceState = instanceState;
    }

    public Set<Service> getServices() {
        return services;
    }

    public void setServices(Set<Service> services) {
        this.services = services;
    }

    public Set<Container> getContainers() { return containers; }

    public void setContainers(Set<Container> containers) {
        this.containers = containers;
    }

    @Override
    public String toString() {
    	StringBuilder stringBuilder = new StringBuilder();
    	stringBuilder.append("{");
    	stringBuilder.append("host_ip=").append(hostIp);
    	stringBuilder.append(", \"state:\"").append(state).append("\"");
    	stringBuilder.append(", \"instance_info\"").append(instanceInfo).append("\"");
    	stringBuilder.append(", \"instance_state\"").append(instanceState).append("\"");
		services.forEach(service -> {
			stringBuilder.append(", \"service\"").append(service.getSource()).append("\"");
		});
        containers.forEach(container -> {
            stringBuilder.append(", \"container='").append(container.getContainerName()).append("\"");
        });
		stringBuilder.append("}");
		return stringBuilder.toString();
    }
}
