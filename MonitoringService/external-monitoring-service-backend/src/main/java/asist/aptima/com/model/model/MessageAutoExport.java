package asist.aptima.com.model.model;

import asist.aptima.com.model.Header;
import asist.aptima.com.model.Msg;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.micronaut.serde.annotation.Serdeable;

@Serdeable
public class MessageAutoExport {

	@JsonProperty("header")
	private asist.aptima.com.model.Header header;
	@JsonProperty("msg")
	private Msg msg;
	@JsonProperty("data")
	private DataAutoExport data;

	@JsonCreator
	public MessageAutoExport(
			asist.aptima.com.model.Header header,
			Msg msg,
			DataAutoExport data
			) {
		this.header = header;
		this.msg = msg;
		this.data = data;
	}

	public MessageAutoExport() {
		// TODO Auto-generated constructor stub
	}

	public asist.aptima.com.model.Header getHeader() {
		return header;
	}
	public void setHeader(Header header) {
		this.header = header;
	}

	public Msg getMsg() {
		return msg;
	}
	public void setMsg(Msg msg) {
		this.msg = msg;
	}

	public DataAutoExport getData() {
		return data;
	}
	public void setData(DataAutoExport data) {
		this.data = data;
	}
}
