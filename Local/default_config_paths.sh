# set host directory to top level testbed folder
cd ..
host_directory="./"
echo "$host_directory"

cd ./Agents

###
cd ./AC_Aptima_TA3_measures
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING AC_Aptima_TA3_measures HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

###
cd ../AC_CMU_TA1_PyGLFoVAgent
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING AC_CMU_TA1_PyGLFoVAgent HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

# cd ../AC_CMU_TA2_BEARD
# sed -i "/HOST_DIRECTORY/d" ./settings.env
# echo '-----UPDATING AC_CMU_TA2_BEARD HOST PATH-------'
# echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

# cd ../AC_CMU_TA2_TED
# sed -i "/HOST_DIRECTORY/d" ./settings.env
# echo '-----UPDATING AC_CMU_TA2_TED HOST PATH-------'
# echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

###
cd ../AC_CMUFMS_TA2_Cognitive
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING AC_CMUFMS_TA2_Cognitive HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

# cd ../ac_cornell_ta2_asi-facework
# sed -i "/HOST_DIRECTORY/d" ./settings.env
# echo '-----UPDATING ac_cornell_ta2_asi-facework HOST PATH-------'
# echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

# cd ../ac_cornell_ta2_teamtrust
# sed -i "/HOST_DIRECTORY/d" ./settings.env
# echo '-----UPDATING ac_cornell_ta2_teamtrust HOST PATH-------'
# echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

###
cd ../AC_GALLUP_TA2_GEM
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING AC_GALLUP_TA2_GEM HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

# cd ../AC_IHMC_TA2_Dyad-Reporting
# sed -i "/HOST_DIRECTORY/d" ./settings.env
# echo '-----UPDATING AC_IHMC_TA2_Dyad-Reporting HOST PATH-------'
# echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

###
cd ../AC_IHMC_TA2_Joint-Activity-Interdependence
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING AC_IHMC_TA2_Joint-Activity-Interdependence HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

###
cd ../AC_IHMC_TA2_Location-Monitor
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING AC_IHMC_TA2_Location-Monitor HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

# cd ../AC_IHMC_TA2_Player-Proximity
# host_directory=$(pwd)/
# sed -i "/HOST_DIRECTORY/d" ./settings.env
# echo '-----UPDATING AC_IHMC_TA2_Player-Proximity HOST PATH-------'
# echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

###
cd ../AC_UAZ_TA1_DialogAgent
sed -i "/HOST_DIRECTORY/d" ./.env
echo '-----UPDATING AC_UAZ_TA1_DialogAgent HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./.env

###
cd ../AC_UCF_TA2_Flocking
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING AC_UCF_TA2_Flocking HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

# cd ../AC_UCF_TA2_PlayerProfiler
# sed -i "/HOST_DIRECTORY/d" ./settings.env
# echo '-----UPDATING AC_UCF_TA2_PlayerProfiler HOST PATH-------'
# echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

###
cd ../ASI_CMU_TA1_ATLAS
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING CMU_TA1_INTERVENTION_AGENT HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env

###
cd ../Rita_Agent
sed -i "/HOST_DIRECTORY/d" ./settings.env
echo '-----UPDATING RITA AGENT HOST PATH-------'
echo "HOST_DIRECTORY="$host_directory  >> ./settings.env








