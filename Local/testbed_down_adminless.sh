#!/bin/bash

set -e
set -u

# Get the top-level ASIST testbed repo directory. The pushd/popd commands use
# this directory, so that this script can be safely executed from any
# directory.
export root_dir="$( cd "$(dirname "${BASH_SOURCE[0]}" )/../" >/dev/null 2>&1 && pwd)"
cd $root_dir/Local

helpFunction()
{
    echo ""
    echo "Usage: $0 [-h] [-c]"
    echo -e "\t-h Display help text"
    echo -e "\t-c Stop all containers, including non-ASIST-related ones."
    exit 1
}

remove_all_containers="false"

while getopts "ch" opt
do
    case "$opt" in
        h ) helpFunction ;;
        c ) remove_all_containers="true";;
    esac
done

echo "Resetting HOST_PATH Paths back to './'"

chmod 777 ./default_config_paths.sh
./default_config_paths.sh

echo "Bring down ELK"
pushd ../ELK-Container
    docker compose down --remove-orphans
popd

echo "Bring down metadata server"
pushd ../metadata/metadata-docker
    docker compose down --remove-orphans
popd

echo "Bring down export/import dashboard"
pushd ../metadata/metadata-web
    docker compose down --remove-orphans
popd

echo "Bringing down internal monitoring service"
pushd ../MonitoringService/internal-monitoring-service/docker
    docker compose down --remove-orphans
popd

echo "Bring down Minecraft"
pushd ../Local
    docker compose -f docker-compose.adminless.yml down --remove-orphans
    echo Deleting Minecraft data volume
    # FIXME prompts user for removing write protected file.
    if [ -d MinecraftServer/data ]; then
        rm -r ./MinecraftServer/data
    fi
popd

################ AC_IHMC_TA2_Location-Monitor Agent ####################
echo "Bringing down the AC_IHMC_TA2_Location-Monitor Agent"
    pushd "$root_dir"/Agents/AC_IHMC_TA2_Location-Monitor;./agent.sh down
popd

################ AC_IHMC_TA2_Joint-Activity-Interdependence Agent ####################
echo "Bringing down the AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
    pushd "$root_dir"/Agents/AC_IHMC_TA2_Joint-Activity-Interdependence;./agent.sh down
popd

################ AC_CMUFMS_TA2_Cognitive Agent ####################
echo "Bringing down the AC_CMUFMS_TA2_Cognitive Agent"
    pushd "$root_dir"/Agents/AC_CMUFMS_TA2_Cognitive;./agent.sh down
popd

################ AC_UCF_TA2_Flocking Agent ####################
echo "Bringing down the AC_UCF_TA2_Flocking Agent"
    pushd "$root_dir"/Agents/AC_UCF_TA2_Flocking;./agent.sh down
popd

################ AC_UAZ_TA1_DialogAgent ####################
pushd "$root_dir"/Agents/AC_UAZ_TA1_DialogAgent
    ./agent.sh down
popd

################ AC_CMU_TA1_PyGLFoVAgent ###################
echo "Bringing down AC_CMU_TA1_PyGLFoVAgent"
pushd "$root_dir"/Agents/AC_CMU_TA1_PyGLFoVAgent
    ./down.sh
popd

################ ASI_CMU_TA1_ATLAS #########################
echo "Bringing down ASI_CMU_TA1_ATLAS"
pushd "$root_dir"/Agents/ASI_CMU_TA1_ATLAS
    ./down.sh
popd

################ AC_Cornell_TA2_Cooperation #########################
echo "Bringing down AC_Cornell_TA2_Cooperation"
    pushd "$root_dir"/Agents/AC_Cornell_TA2_Cooperation;./agent.sh down
popd

################   ASI_DOLL_TA1_Rita ############################################################
echo "Bringing down ASI_DOLL_TA1_Rita"
pushd "$root_dir"/Agents/Rita_Agent
    docker compose --env-file settings.env down --remove-orphans
popd


################  GEM  ############################################################
pushd "$root_dir"/Agents/AC_GALLUP_TA2_GEM
    ./agent.sh down
popd


################  AC_Aptima_TA3_measures  ############################################################
pushd "$root_dir"/Agents/AC_Aptima_TA3_measures
    ./agent.sh down
popd

#################################################################################################

echo "Bring down MQTT"
pushd ../mqtt
    docker compose down --remove-orphans
popd

if [ $remove_all_containers = "true" ]
then
    echo "Stopping all containers!"
        if [[ ! -z $(docker container ls -q ) ]]; then
                docker container stop $(docker container ls -q )
        fi
else
        echo "Additional containers were not removed."
fi

echo "Resetting Agent Volume Paths to default"



docker ps
exit 0
