Local testbed docker build instructions
=======================================

Prerequisites
-------------

* Docker: 
  * Docker: (Windows) Docker desktop for Windows V 4.0.0
  * Docker (Linux) 19.03.11
  * docker-compose: 1.29.2

* If pulling from the container registry:
    * Python V 3.83
    * Python Docker SDK 5.0.0

## Build Options
### Build from the ASIST Testbed Software Repository
* Clone or update a local directory with the master (or some other branch) branch from the software repository with the version tag you want to run.
* Change your default directory to Local.
* On Linux and MacOS, run the script `testbed_build.sh`. 
  The first build will take a while because the Minecraft container will build from scratch.
  Subsequent builds will be much quicker.
 
* On Windows machines, run the script
  `testbed_build.cmd` from a PowerShell window.  You may need to run with admin privledges depending on your machine configuration.

## Get Docker images from the ASIST Container Registry
### Manually Pull Pre-Built Docker Images from the Container Registry
All of the ASIST testbed containers have docker images in the [Gitlab Container Registry](https://gitlab.asist.aptima.com/asist/testbed/container_registry).
If you just want to update one docker image, you can just pull that image from the container registry and run the `testbed_up` script to bring up the testbed.
To obtain an image from the container registry use the docker pull command and give it
 the registry URL, the name of the image you want and the tag of the version of the image you want.

### Using the Python Testbed Configuration tool [in development]
* Clone or update a local directory with the branch from the software repository with the version tag you want to run.
* Make sure you have Python 3 and the Python Docker SDK in your environment. You can install the Docker library using Pip with `pip install docker`. 
You can find detailed installation instructions for Python at https://www.python.org/, and for the Python Docker SDK at https://pypi.org/project/docker/. 
* Change your default directory to Local.
* To pull and start up the whole testbed, first decide which version you want to run.
 For example, to pull the latest images for version 2 of the testbed, run `testbed_up.py template --all --tag 2`.
 Other templates besides "--all" will become available in future releases to provide pared-down testbed configurations - 
 for example, the minimum set of containers needed to play back and ingest replay data.
* You can also use the testbed_up.py script to pull and start individual testbed services.
 To see a list of services, run `testbed_up.py services -h`.
 As with templates, you must also provide a version number.
 For example, to start up the Metadata service and the Import/Export service, run `testbed_up.py services --metadata --import_export --tag 2`.
* Pull and start agents in the same way.
 Use `testbed_up.py agents -h` to see a list of agents you can start, and `testbed_up.py agents [agents] --tag <version_num>` to start them up.
