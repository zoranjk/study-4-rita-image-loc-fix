#!/bin/bash

set -e
set -u
set -o nounset

# Script to automatically launch the testbed on Linux and macOS systems.
# Usage: ./testbed_these_up.sh

# 4 RUNNING MODES:

# Single Instance, External IP  | args -> none :
# USE CASE: You are using a single machine but still wish to accept participants from outside your local network.
# This indicates that The Admin Docker Network and the Instance Docker Network are on the same machine.
# This also indicates that the IP address used to route participants to the various front end webpages will be
# Google Cloud's External IP for this instance - obtained via curl to the Google Cloud system
# This is the mode used for Pilot Tests

# Single Instance, Internal IP | args -> -i :
# USE CASE: You are debugging in your own network, on a single machine, with no external participants coming from outside.
# This indicates that The Admin Docker Network and the Instance Docker Network are on the same machine.
# This also indicates that the IP addresses used to route participants along the various front ends will be
# the internal IP of this machine - obtained with native bash functions
# This is the mode used for Debugging and Testing

# Scaling Instance, External IP | args -> -v :
# USE CASE: You intend to scale dynamically and accept participants from outside your local network.
# This indicates that The Admin Docker Network and the Instance Docker Network are on different machines.
# This also indicates that the IP address used to route participants to the various front end webpages will be
# Google Cloud's External IP for this instance - obtained via curl to the Google Cloud system
# Note that you must set the ADMIN_STACK_IP variable below manually to use this mode
# This is the mode used for Live Deployment

# Scaling Instance, Internal IP | args -> -iv :
# USE CASE: You intend to scale dynamically, but only accept participants from inside your local network.
# This indicates that The Admin Docker Network and the Instance Docker Network are on different machines.
# This also indicates that the IP addresses used to route participants along the various front ends will be
# the internal IP of this machine - obtained with native bash functions
# Note that you must set the ADMIN_STACK_IP variable below manually to use this mode
# This is the mode for Debugging and Testing Scaling specifically


# Get the top-level ASIST testbed repo directory. The pushd/popd commands use
# this directory, so that this script can be safely executed from any
# directory.
export root_dir="$( cd "$(dirname "${BASH_SOURCE[0]}" )/../" >/dev/null 2>&1 && pwd)"


# list of vars to set by export before running this script
# ADMIN_STACK_IP=
# ADMIN_STACK_PORT=
# INSTANCE_IP=
# DB_API_TOKEN=
# DB_API_ISSUER_KEY=
# ADMIN_ID=
# ADMIN_PWD=
# PGADMIN_DEFAULT_EMAIL=
# PGADMIN_DEFAULT_PASSWORD=
# POSTGRES_PASSWORD=
# POSTGRES_USER=
# POSTGRES_DB=
# DB_CONNECTION_PWD=
# DB_CONNECTION_USER=
# DB_CONNECTION_HOST=
# DB_CONNECTION_PORT=
# DB_CONNECTION_DATABASE=
# EMAIL_PWD=
# SSO_SECRET_KEY=


################################################################################################
############ Start Help Function use to determine which agents run #############################
################################################################################################
helpFunction()
{
    echo ""
    echo "Usage: $0 [-h] [-f] [-a] [-c] [-d] [-e] [-g] [-j] [-k] [-l] [-r] [-m]"
    echo -e "\t-h display help text"
    echo -e "\t-a Do not start up the ASI_CMU_TA1_ATLAS"
    echo -e "\t-c Do not start up the AC_CMUFMS_TA2_Cognitive Agent"
    echo -e "\t-d Do not start up the AC_UAZ_TA1_DialogAgent"    
    echo -e "\t-f Do not start up the AC_CMU_TA1_PyGLFoVAgent"
    echo -e "\t-g Do not start up the AC_GALLUP_TA2_GEM"
    echo -e "\t-i Set ADMIN_STACK_IP to INTERNAL IP IF USING GOOGLE CLOUD"
    echo -e "\t-j Do not start up the AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
    echo -e "\t-k Do not start up the AC_UCF_TA2_Flocking Agent"
    echo -e "\t-l Do not start up the AC_IHMC_TA2_Location-Monitor"
    echo -e "\t-r Do not start up the ASI_DOLL_TA1_Rita"
    echo -e "\t-m Start up the Aptima TA3 Measures agent"
    echo -e "By default all agents are started up."
    exit 1
}

dont_run_lm="false"
dont_run_jai="false"
dont_run_cog="false"
dont_run_flocking="false"
dont_run_rita="false"
dont_run_atlas="false"
dont_run_gallup="false"
dont_run_dialog="false"
dont_run_fov="false"
dont_run_measures="false"


INSTANCE_IP_INTERNAL_MODE=0
ADMIN_STACK_IP_FROM_LOCAL_VAR=0

while getopts "abcdefghijklmnopqrstuvwxyz" opt
do
        case "$opt" in

            h ) helpFunction ;;
            a ) dont_run_atlas="true";;
            c ) dont_run_cog="true";;
            d ) dont_run_dialog="true";;            
            f ) dont_run_fov="true";;
            g ) dont_run_gallup="true";;
            i ) INSTANCE_IP_INTERNAL_MODE=1;;
            j ) dont_run_jai="true";;
            k ) dont_run_flocking="true";;
            l ) dont_run_lm="true";;
            r ) dont_run_rita="true";;
            m ) dont_run_measures="true";;
            v ) ADMIN_STACK_IP_FROM_LOCAL_VAR=1;;
        esac
done
################################################################################################
########### END HELP FUNCTION ##################################################################
################################################################################################

echo "Determining version number"
pushd ..
    echo "Getting full testbed version"
    git describe --tags > version.txt
popd

# THESE VARIABLES SET THEMSELVES AUTOMATICALLY
# THIS IS THE IP OR DOMAIN NAME OF THIS PARTICULAR INSTANCE, BOTH INTERNAL AND EXTERNAL ARE SET
# AND THE -i FLAG PASSED TO THIS SCRIPT WILL USE INTERNAL MODE - PRODUCTION DEFAULTS TO EXTERNAL MODE
INSTANCE_INTERNAL_IP=$(hostname -I | grep -vE '^127\.|^172\.' | awk '{print $1}')
INSTANCE_EXTERNAL_IP=socialai.games
INSTANCE_IP=$INSTANCE_INTERNAL_IP

# SET THIS VARIABLE MANUALLY EVERYTIME YOU DEPLOY TO A VM
# THIS IS THE IP OR DOMAIN NAME OF THE ADMIN STACK MACHINE
ADMIN_STACK_IP=$INSTANCE_INTERNAL_IP


if [ $INSTANCE_IP_INTERNAL_MODE = 1 ]; then
    echo "USING THE INTERNAL IP FOR FRONT END REDIRECTION"
else
    echo "USING THE EXTERNAL IP FOR FRONT END REDIRECTION"    
    INSTANCE_IP=$INSTANCE_EXTERNAL_IP
fi

if [ $ADMIN_STACK_IP_FROM_LOCAL_VAR = 0 ]; then
    echo "USING THE SAME INSTANCE FOR THE ADMIN STACK"   
    ADMIN_STACK_IP=$INSTANCE_IP
else
    echo "USING THE DEFINED ADMIN_STACK_IP VARIABLE - MAKE SURE YOU SET IT CORRECTLY"
fi

echo "ADMIN_STACK_IP : "$ADMIN_STACK_IP
echo "ADMIN_STACK_PORT : "$ADMIN_STACK_PORT
echo "INTERNAL_INSTANCE_IP : "$INSTANCE_INTERNAL_IP
echo "EXTERNAL_INSTANCE_IP : "$INSTANCE_EXTERNAL_IP

echo "SETTING INSTANCE_IP TO : "$INSTANCE_IP

REV_TAG=$(cat ../version.txt)

echo "Testbed version:$REV_TAG"

echo "Create the asist network"
if docker network ls | grep -q "asist_net"
then
    echo "asist_net found ... no need to create it."
else
    echo "asist_net not found ... let's create it."
    docker network create asist_net
fi

#echo "Updating Agent Volume Paths for docker in docker control"
#./update_config_paths.sh

echo "Bringing up the MQTT broker"
pushd "$root_dir"/mqtt
    docker compose --compatibility up -d
    echo "Finished launching the Mosquitto container, waiting for 5 seconds to ensure everything works properly..."
    sleep 5
popd

# Helper function to set permissions while avoiding unnecessary entering of
# superuser password.
set_permissions() {
    local TARGET=$1
    local DESIRED_PERMISSIONS=$2
    local CURRENT_PERMISSIONS=""

    # The invocation for the 'stat' command is different on macOS and Linux
    # systems (unless coreutils is installed on macOS).
    if [[ "$OSTYPE" == "darwin"* ]]; then
      if [[ -x "$(which gstat)" ]]; then # With coreutils, gstat is the same as stat
        CURRENT_PERMISSIONS=$(gstat --format '%a' CLEAN_MAPS)
      else
        CURRENT_PERMISSIONS=$(stat -f "%A" CLEAN_MAPS)
      fi
    else
        CURRENT_PERMISSIONS=$(stat --format '%a' CLEAN_MAPS)
    fi

    if [[ ! $CURRENT_PERMISSIONS == "$DESIRED_PERMISSIONS" ]]; then
        echo "Changing permissions of $TARGET to $DESIRED_PERMISSIONS."
        if ! sudo chmod -R "$DESIRED_PERMISSIONS" "$TARGET"; then
            echo "Unable to perform the command 'chmod -R $DESIRED_PERMISSIONS $TARGET', "\
                "exiting now."
            exit 1
        fi
    fi
}



echo "Bringing up SSO STACK"
pushd "$root_dir"/Local

     if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS has BSD sed, which requires an extra argument with -i
        #sed -i '' "s/\"system_version\": .*/\"system_version\": \"${REV_TAG}\",/" AsistControl/appsettings.Production.json        
        sed -i '' "s/.*ADMIN_STACK_IP=.*/ADMIN_STACK_IP=${ADMIN_STACK_IP}/" .env
        sed -i '' "s/.*ADMIN_STACK_PORT=.*/ADMIN_STACK_PORT=${ADMIN_STACK_PORT}/" .env
        sed -i '' "s/.*INSTANCE_IP=.*/INSTANCE_IP=${INSTANCE_IP}/" .env
        sed -i '' "s/.*DB_API_TOKEN=.*/DB_API_TOKEN=${DB_API_TOKEN}/" .env

        sed -i '' "s/.*DB_API_ISSUER_KEY=.*/DB_API_ISSUER_KEY=${DB_API_ISSUER_KEY}/" .env
        sed -i '' "s/.*ADMIN_ID=.*/ADMIN_ID=${ADMIN_ID}/" .env
        sed -i '' "s/.*ADMIN_PWD=.*/ADMIN_PWD=${ADMIN_PWD}/" .env
        sed -i '' "s/.*PGADMIN_DEFAULT_EMAIL=.*/PGADMIN_DEFAULT_EMAIL=${PGADMIN_DEFAULT_EMAIL}/" .env
        sed -i '' "s/.*PGADMIN_DEFAULT_PASSWORD=.*/PGADMIN_DEFAULT_PASSWORD=${PGADMIN_DEFAULT_PASSWORD}/" .env
        sed -i '' "s/.*POSTGRES_PASSWORD=.*/POSTGRES_PASSWORD=${POSTGRES_PASSWORD}/" .env
        sed -i '' "s/.*POSTGRES_USER=.*/POSTGRES_USER=${POSTGRES_USER}/" .env
        sed -i '' "s/.*POSTGRES_DB=.*/POSTGRES_DB=${POSTGRES_DB}/" .env
        sed -i '' "s/.*DB_CONNECTION_PWD=.*/DB_CONNECTION_PWD=${DB_CONNECTION_PWD}/" .env
        sed -i '' "s/.*DB_CONNECTION_USER=.*/DB_CONNECTION_USER=${DB_CONNECTION_USER}/" .env
        sed -i '' "s/.*DB_CONNECTION_HOST=.*/DB_CONNECTION_HOST=${DB_CONNECTION_HOST}/" .env
        sed -i '' "s/.*DB_CONNECTION_PORT=.*/DB_CONNECTION_PORT=${DB_CONNECTION_PORT}/" .env
        sed -i '' "s/.*DB_CONNECTION_DATABASE=.*/DB_CONNECTION_DATABASE=${DB_CONNECTION_DATABASE}/" .env

        sed -i '' "s/.*GCLOUD_KEYFILE=.*/GCLOUD_KEYFILE=${GCLOUD_KEYFILE}/" .env
        sed -i '' "s/.*GCLOUD_PROJECT_ID=.*/GCLOUD_PROJECT_ID=${GCLOUD_PROJECT_ID}/" .env
        sed -i '' "s|.*GCS_BACKUP_BUCKET=.*|GCS_BACKUP_BUCKET=${GCS_BACKUP_BUCKET}|" .env

        sed -i '' "s/.*EMAIL_PWD=.*/EMAIL_PWD=${EMAIL_PWD}/" .env
        sed -i '' "s/.*SSO_SECRET_KEY=.*/SSO_SECRET_KEY=${SSO_SECRET_KEY}/" .env


        sed -i '' "s|.*MQTT_HOST=.*|MQTT_HOST=${ADMIN_STACK_IP}|" external-monitoring-service.env
        sed -i '' "s|.*MQTT_PORT=.*|MQTT_PORT=443|" external-monitoring-service.env
    else
        
        sed -i "s/.*ADMIN_STACK_IP=.*/ADMIN_STACK_IP=${ADMIN_STACK_IP}/" .env
        sed -i "s/.*ADMIN_STACK_PORT=.*/ADMIN_STACK_PORT=${ADMIN_STACK_PORT}/" .env
        sed -i "s/.*INSTANCE_IP=.*/INSTANCE_IP=${INSTANCE_IP}/" .env
        sed -i "s/.*DB_API_TOKEN=.*/DB_API_TOKEN=${DB_API_TOKEN}/" .env
        
        sed -i "s/.*DB_API_ISSUER_KEY=.*/DB_API_ISSUER_KEY=${DB_API_ISSUER_KEY}/" .env
        sed -i "s/.*ADMIN_ID=.*/ADMIN_ID=${ADMIN_ID}/" .env
        sed -i "s/.*ADMIN_PWD=.*/ADMIN_PWD=${ADMIN_PWD}/" .env
        sed -i "s/.*PGADMIN_DEFAULT_EMAIL=.*/PGADMIN_DEFAULT_EMAIL=${PGADMIN_DEFAULT_EMAIL}/" .env
        sed -i "s/.*PGADMIN_DEFAULT_PASSWORD=.*/PGADMIN_DEFAULT_PASSWORD=${PGADMIN_DEFAULT_PASSWORD}/" .env
        sed -i "s/.*POSTGRES_PASSWORD=.*/POSTGRES_PASSWORD=${POSTGRES_PASSWORD}/" .env
        sed -i "s/.*POSTGRES_USER=.*/POSTGRES_USER=${POSTGRES_USER}/" .env
        sed -i "s/.*POSTGRES_DB=.*/POSTGRES_DB=${POSTGRES_DB}/" .env
        sed -i "s/.*DB_CONNECTION_PWD=.*/DB_CONNECTION_PWD=${DB_CONNECTION_PWD}/" .env
        sed -i "s/.*DB_CONNECTION_USER=.*/DB_CONNECTION_USER=${DB_CONNECTION_USER}/" .env
        sed -i "s/.*DB_CONNECTION_HOST=.*/DB_CONNECTION_HOST=${DB_CONNECTION_HOST}/" .env
        sed -i "s/.*DB_CONNECTION_PORT=.*/DB_CONNECTION_PORT=${DB_CONNECTION_PORT}/" .env
        sed -i "s/.*DB_CONNECTION_DATABASE=.*/DB_CONNECTION_DATABASE=${DB_CONNECTION_DATABASE}/" .env
        sed -i "s/.*EMAIL_PWD=.*/EMAIL_PWD=${EMAIL_PWD}/" .env
        sed -i "s/.*SSO_SECRET_KEY=.*/SSO_SECRET_KEY=${SSO_SECRET_KEY}/" .env

        sed -i "s/.*GCLOUD_KEYFILE=.*/GCLOUD_KEYFILE=${GCLOUD_KEYFILE}/" .env
        sed -i "s/.*GCLOUD_PROJECT_ID=.*/GCLOUD_PROJECT_ID=${GCLOUD_PROJECT_ID}/" .env
        sed -i "s|.*GCS_BACKUP_BUCKET=.*|GCS_BACKUP_BUCKET=${GCS_BACKUP_BUCKET}|" .env

        sed -i "s|.*MQTT_HOST=.*|MQTT_HOST=${ADMIN_STACK_IP}|" external-monitoring-service.env
        sed -i "s|.*MQTT_PORT=.*|MQTT_PORT=443|" external-monitoring-service.env
    fi

    if ! sudo docker compose -f docker-compose.sso.adminless.yml up -d; then
        echo "Unable to launch the SSO stack. Exiting now"
        exit 1
    fi

popd

docker ps

echo "Testbed SSO successfully launched."
exit 0
