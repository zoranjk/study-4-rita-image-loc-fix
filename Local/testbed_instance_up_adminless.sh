#!/bin/bash

set -e
set -u
set -o nounset

# Script to automatically launch the testbed on Linux and macOS systems.
# Usage: ./testbed_instance_up_adminless.sh

# This script is intended to be called only from the export_env_vars_and_testbed_up.sh script.
# export_env_vars_and_testbed_up.sh script defines the environment variables used below. 
# It is especially important to define the ADMIN_STACK_IP variable so the system know where the Admin Stack resides


# Get the top-level ASIST testbed repo directory. The pushd/popd commands use
# this directory, so that this script can be safely executed from any
# directory.
export root_dir="$( cd "$(dirname "${BASH_SOURCE[0]}" )/../" >/dev/null 2>&1 && pwd)"

# This is a development token which will be changed on production. Performers should not use the token directly, but should use the
# variable that holds it and pass it to there ".env" docker compose settings files, to be used in their containers for DB call authorization 

################################################################################################
############ Start Help Function use to determine which agents run #############################
################################################################################################
helpFunction()
{
    echo ""
    echo "Usage: $0 [-h] [-f] [-a] [-c] [-d] [-e] [-g] [-j] [-k] [-l] [-r] [-m]"
    echo -e "\t-h display help text"
    echo -e "\t-a Do not start up the ASI_CMU_TA1_ATLAS"
    echo -e "\t-c Do not start up the AC_CMUFMS_TA2_Cognitive Agent"
    echo -e "\t-d Do not start up the AC_UAZ_TA1_DialogAgent"
    echo -e "\t-f Do not start up the AC_CMU_TA1_PyGLFoVAgent"
    echo -e "\t-g Do not start up the AC_GALLUP_TA2_GEM"
    echo -e "\t-i Set ADMIN_STACK_IP to INTERNAL IP IF USING GOOGLE CLOUD"
    echo -e "\t-j Do not start up the AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
    echo -e "\t-k Do not start up the AC_UCF_TA2_Flocking Agent"
    echo -e "\t-l Do not start up the AC_IHMC_TA2_Location-Monitor"
    echo -e "\t-r Do not start up the ASI_DOLL_TA1_Rita"
    echo -e "\t-m Start up the Aptima TA3 Measures agent"
    echo -e "\t-p Do not start up the AC_Cornell_TA2_Cooperation"
    echo -e "By default all agents are started up."
    exit 1
}

dont_run_lm="false"
dont_run_jai="false"
dont_run_cog="false"
dont_run_flocking="false"
dont_run_rita="false"
dont_run_atlas="false"
dont_run_cc="false"
dont_run_gallup="false"
dont_run_dialog="false"
dont_run_fov="false"
dont_run_measures="false"


INSTANCE_IP_INTERNAL_MODE=0
ADMIN_STACK_IP_FROM_LOCAL_VAR=0

while getopts "abcdefghijklmnopqrstuvwxyz" opt
do
        case "$opt" in

            h ) helpFunction ;;
            a ) dont_run_atlas="true";;
            c ) dont_run_cog="true";;
            d ) dont_run_dialog="true";;
            f ) dont_run_fov="true";;
            g ) dont_run_gallup="true";;
            i ) INSTANCE_IP_INTERNAL_MODE=1;;
            j ) dont_run_jai="true";;
            k ) dont_run_flocking="true";;
            l ) dont_run_lm="true";;
            r ) dont_run_rita="true";;
            m ) dont_run_measures="true";;
            p ) dont_run_cc="true";;
            v ) ADMIN_STACK_IP_FROM_LOCAL_VAR=1;;
        esac
done
################################################################################################
########### END HELP FUNCTION ##################################################################
################################################################################################

echo "Determining version number"
pushd ..
    echo "Getting full testbed version"
    git describe --tags > version.txt
popd

# THESE VARIABLES SET THEMSELVES AUTOMATICALLY
# THIS IS THE IP OR DOMAIN NAME OF THIS PARTICULAR INSTANCE, BOTH INTERNAL AND EXTERNAL ARE SET
# AND THE -i FLAG PASSED TO THIS SCRIPT WILL USE INTERNAL MODE - PRODUCTION DEFAULTS TO EXTERNAL MODE
INSTANCE_INTERNAL_IP=$(hostname -I | grep -vE '^127\.|^172\.' | awk '{print $1}')
INSTANCE_EXTERNAL_IP=$(curl ifconfig.me)

INSTANCE_IP=$ADMIN_STACK_IP

# ADMIN_STACK_PORT="443"

echo "ADMIN_STACK_IP : "$ADMIN_STACK_IP
echo "ADMIN_STACK_PORT : "$ADMIN_STACK_PORT
echo "INTERNAL_INSTANCE_IP : "$INSTANCE_INTERNAL_IP
echo "EXTERNAL_INSTANCE_IP : "$INSTANCE_EXTERNAL_IP

echo "SETTING INSTANCE_IP TO : "$INSTANCE_IP

REV_TAG=$(cat ../version.txt)
echo "Testbed version:$REV_TAG"

BUCKET_NAME="study4-auto-exports"
echo "Bucket name to use (will override GCP_DEFAULT_BUCKET setting in metadata-app.env file):$BUCKET_NAME"

echo "Create the asist network"
if docker network ls | grep -q "asist_net"
then
    echo "asist_net found ... no need to create it."
else
    echo "asist_net not found ... let's create it."
    docker network create asist_net
fi

#echo "Updating Agent Volume Paths for docker in docker control"
#./update_config_paths.sh

echo "Bringing up the MQTT broker"
pushd "$root_dir"/mqtt
    docker compose --compatibility up -d
    echo "Finished launching the Mosquitto container, waiting for 5 seconds to ensure everything works properly..."
    sleep 5
popd

echo "Bringing up the ELK stack"
pushd "$root_dir"/ELK-Container
    docker compose --compatibility up -d --build
    echo "Finished launching the ELK stack, waiting for 5 seconds to ensure "\
            "everything works properly..."
popd

echo "Bringing up the metadata server"
pushd "$root_dir"/metadata/metadata-docker
    echo "setting version number for metadata-app"
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS has BSD sed, which requires an extra argument with -i
        sed -i '' '/TESTBED_VERSION=/c\'$'\n'"TESTBED_VERSION=${REV_TAG}" metadata-app.env
		sed -i "/GCP_DEFAULT_BUCKET=/c\GCP_DEFAULT_BUCKET=${BUCKET_NAME}" metadata-app.env
    else
        sed -i "/TESTBED_VERSION=/c\TESTBED_VERSION=${REV_TAG}" metadata-app.env
		sed -i "/GCP_DEFAULT_BUCKET=/c\GCP_DEFAULT_BUCKET=${BUCKET_NAME}" metadata-app.env
    fi    
    docker compose --compatibility up -d
popd
echo "Bring up Import/Export dashboard"
pushd ../metadata/metadata-web
    echo "setting version number for Import-Export"
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS has BSD sed, which requires an extra argument with -i
        sed -i '' '/TESTBED_VERSION=/c\'$'\n'"TESTBED_VERSION=${REV_TAG}" metadata-web.env
    else
        sed -i "/TESTBED_VERSION=/c\TESTBED_VERSION=${REV_TAG}" metadata-web.env
    fi
    docker compose --compatibility up --build -d
popd

# Helper function to set permissions while avoiding unnecessary entering of
# superuser password.
set_permissions() {
    local TARGET=$1
    local DESIRED_PERMISSIONS=$2
    local CURRENT_PERMISSIONS=""

    # The invocation for the 'stat' command is different on macOS and Linux
    # systems (unless coreutils is installed on macOS).
    if [[ "$OSTYPE" == "darwin"* ]]; then
      if [[ -x "$(which gstat)" ]]; then # With coreutils, gstat is the same as stat
        CURRENT_PERMISSIONS=$(gstat --format '%a' CLEAN_MAPS)
      else
        CURRENT_PERMISSIONS=$(stat -f "%A" CLEAN_MAPS)
      fi
    else
        CURRENT_PERMISSIONS=$(stat --format '%a' CLEAN_MAPS)
    fi

    if [[ ! $CURRENT_PERMISSIONS == "$DESIRED_PERMISSIONS" ]]; then
        echo "Changing permissions of $TARGET to $DESIRED_PERMISSIONS."
        if ! sudo chmod -R "$DESIRED_PERMISSIONS" "$TARGET"; then
            echo "Unable to perform the command 'chmod -R $DESIRED_PERMISSIONS $TARGET', "\
                "exiting now."
            exit 1
        fi
    fi
}


echo "Bringing up Minecraft"
pushd "$root_dir"/Local

    echo "Copying over Minecraft data volume"
    mkdir -p ./MinecraftServer
    cp -r ./data ./MinecraftServer/data

    set_permissions CLEAN_MAPS 777
    set_permissions MinecraftServer 777

    echo "setting version number for ASIST Control Center"
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS has BSD sed, which requires an extra argument with -i
        sed -i '' "s/\"system_version\": .*/\"system_version\": \"${REV_TAG}\",/" AsistControl/appsettings.Production.json
        sed -i '' "s/.*ADMIN_STACK_IP=.*/ADMIN_STACK_IP=${ADMIN_STACK_IP}/" .env
        sed -i '' "s/.*ADMIN_STACK_PORT=.*/ADMIN_STACK_PORT=${ADMIN_STACK_PORT}/" .env
        sed -i '' "s/.*INSTANCE_IP=.*/INSTANCE_IP=${INSTANCE_IP}/" .env
        sed -i '' "s/.*DB_API_TOKEN=.*/DB_API_TOKEN=${DB_API_TOKEN}/" .env
        sed -i '' "s/.*HOST_IP=.*/HOST_IP=${INSTANCE_IP}/" internal-monitoring-service.env
        sed -i '' "s|.*EXTERNAL_MQTT_SERVER_HOST=.*|EXTERNAL_MQTT_SERVER_HOST=${ADMIN_STACK_IP}|" internal-monitoring-service.env
    else
        sed -i "s/\"system_version\": .*/\"system_version\": \"${REV_TAG}\",/" AsistControl/appsettings.Production.json
        sed -i "s/.*ADMIN_STACK_IP=.*/ADMIN_STACK_IP=${ADMIN_STACK_IP}/" .env
        sed -i "s/.*ADMIN_STACK_PORT=.*/ADMIN_STACK_PORT=${ADMIN_STACK_PORT}/" .env
        sed -i "s/.*INSTANCE_IP=.*/INSTANCE_IP=${INSTANCE_IP}/" .env
        sed -i "s/.*DB_API_TOKEN=.*/DB_API_TOKEN=${DB_API_TOKEN}/" .env
        sed -i "s/.*HOST_IP=.*/HOST_IP=${INSTANCE_IP}/" internal-monitoring-service.env
        sed -i "s|.*EXTERNAL_MQTT_SERVER_HOST=.*|EXTERNAL_MQTT_SERVER_HOST=${ADMIN_STACK_IP}|" internal-monitoring-service.env
    fi


    export EXTERNAL_IP=$INSTANCE_EXTERNAL_IP

    echo "Launching the MCRVM stack..."
    if ! sudo docker compose -f docker-compose.instance.adminless.yml up -d; then
        echo "Unable to launch the MCRVM stack! Exiting now."
        exit 1
    fi
    # loop permission script on mounted Minecraft volume
    if ps ax | grep permissions.sh;
    then
        echo "permissions.sh script already running"
    else
        echo "Initializing permissions.sh script"
        ./permissions.sh & disown
    fi

popd

################   AC_IHMC_TA2_Location-Monitor Agent ####################
if [ $dont_run_lm = "false" ]; then
    echo "Bringing up the AC_IHMC_TA2_Location-Monitor Agent"
    pushd "$root_dir"/Agents/AC_IHMC_TA2_Location-Monitor;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_IHMC_TA2_Location-Monitor Agent"
fi
################################################################################################

################   AC_IHMC_TA2_Joint-Activity-Interdependence Agent ####################
if [ $dont_run_jai = "false" ]; then
    echo "Bringing up the AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
    pushd "$root_dir"/Agents/AC_IHMC_TA2_Joint-Activity-Interdependence;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_IHMC_TA2_Joint-Activity-Interdependence Agent"
fi
#################################################################################################

################   AC_CMUFMS_TA2_Cognitive Agent ####################
if [ $dont_run_cog = "false" ]; then
    echo "Bringing up the AC_CMUFMS_TA2_Cognitive Agent"
    pushd "$root_dir"/Agents/AC_CMUFMS_TA2_Cognitive;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_CMUFMS_TA2_Cognitive Agent"
fi
#################################################################################################

################   AC_UCF_TA2_Flocking Agent ####################
if [ $dont_run_flocking = "false" ]; then
    echo "Bringing up the AC_UCF_TA2_Flocking Agent"
    pushd "$root_dir"/Agents/AC_UCF_TA2_Flocking;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_UCF_TA2_Flocking Agent"
fi
#################################################################################################

################# DIALOG AGENT ##################################################################
if [ $dont_run_dialog = "false" ]; then
    echo "Bringing up the UAZ Dialog Agent"
    pushd "$root_dir"/Agents/AC_UAZ_TA1_DialogAgent
        ./agent.sh upd
    popd
else
    echo "Skipping bringing up the UAZ Dialog agent"
fi

################   ASI_DOLL_TA1_Rita ############################################################
if [ $dont_run_rita = "false" ]; then
    echo "Bringing up the Doll/MIT Rita Agent"
    pushd "$root_dir"/Agents/Rita_Agent
        echo "$PWD: Starting Rita Agent"
        dte=$(date "+%B-%Y-%d")
        log_dir="logs-${dte}"
        export SERVICE_LOGS_DIR="$log_dir/"
        echo "${SERVICE_LOGS_DIR} Log dirs for this instantiation of Rita"
        docker compose --env-file settings.env up -d
    popd
else
    echo "Skipping bringing up the Doll/MIT Rita agent"
fi
#################################################################################################
################# AC_CMU_TA1_PyGLFoVAgent ##################################
if [ $dont_run_fov = "false" ]; then
        echo "Bringing up CMU-TA1 PyGLFoVAgent"
        pushd "$root_dir"/Agents/AC_CMU_TA1_PyGLFoVAgent
                echo "$PWD: Staring PyGLFoVAgent"
                ./up.sh
        popd
else
        echo "Skipping bringing up PyGLFoVAgent"
fi
################# ASI_CMU_TA1_ATLAS ########################################
if [ $dont_run_atlas = "false" ]; then
    echo "Bringing up CMU-TA1 ATLAS Agent"
    pushd "$root_dir"/Agents/ASI_CMU_TA1_ATLAS
        echo "$PWD: Starting ATLAS"
        ./up.sh
    popd
else
    echo "Skipping bringing up ATLAS"
fi

################# AC_Cornell_TA2_Cooperation ########################################
if [ $dont_run_cc = "false" ]; then
    echo "Bringing up the AC_Cornell_TA2_Cooperation Agent"
    pushd "$root_dir"/Agents/AC_Cornell_TA2_Cooperation;./agent.sh upd
    popd
else
    echo "Skipping bring up AC_Cornell_TA2_Cooperation Agent"
fi

################# AC_Aptima_TA3_measures ########################################
if [ $dont_run_measures = "false" ]; then
    echo "Bringing up AC_Aptima_TA3_measures Agent"
    pushd "$root_dir"/Agents/AC_Aptima_TA3_measures
        echo "$PWD: Starting Measures Agent"
        ./agent.sh upd
    popd
else
    echo "Skipping bringing up Measures Agent"
fi

################  GEM  ############################################################
if [ $dont_run_gallup = "false" ]; then
    echo "Bringing up the GEM Agent"
    pushd "$root_dir"/Agents/AC_GALLUP_TA2_GEM
        echo "$PWD: Starting GEM Agent"
        dte=$(date "+%B-%Y-%d")
        log_dir="logs-${dte}"
        export SERVICE_LOGS_DIR="$log_dir/"
        echo "${SERVICE_LOGS_DIR} Log dirs for this instantiation of GEM"
        docker compose --env-file settings.env up -d
    popd
else
    echo "Skipping bringing up the GEM agent"
fi
#################################################################################################
#
##

################# internal-monitoring-service ########################################
if ! sudo docker compose -f docker-compose.instance.monitor.yml up -d; then
    echo "Unable to launch internal-monitoring-service! Exiting now."
    exit 1
fi

docker ps

echo "Testbed instance successfully launched."
exit 0
