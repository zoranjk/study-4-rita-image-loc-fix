docker network create asist_net

#@echo off
echo Bring up Mosquitto Broker
pushd ..\mqtt
docker compose up -d 
sleep 5

echo Bring up Core Stack
pushd ..\Local
docker compose -f docker compose.sso.adminless.yml up -d
popd
docker ps