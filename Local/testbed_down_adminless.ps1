#@echo off
echo Bring down ELK
pushd ..\ELK-Container
docker compose down --remove-orphans
popd
echo Bring down metadata server
pushd ..\metadata\metadata-docker
docker compose down --remove-orphans
popd

echo Bring down Import/Export dashboard
pushd ..\metadata\metadata-web
docker compose down --remove-orphans
popd

echo Bring down Minecraft
pushd ..\Local
docker compose -f docker-compose.adminless.yml down --remove-orphans
popd

echo Bring down Mosquitto
pushd ..\mqtt
docker compose down --remove-orphans
popd
#echo Stop the rest of the containers
#for /f "tokens=*" %%c in ('docker container ls -q') do docker container stop %%c

docker network remove asist_net

docker ps


