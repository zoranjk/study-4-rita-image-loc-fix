﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsistDataIngester.Models
{
    public class MessageTopicInfo
    {
        public string Topic { get; set; }
        public string Schema { get; set; }
        public string Description { get; set; }
    }
}
