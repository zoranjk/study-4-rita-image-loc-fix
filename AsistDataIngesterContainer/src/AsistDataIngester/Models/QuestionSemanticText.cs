using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsistDataIngester.Models
{
    public class QuestionSemanticText
    {
        public string QID { get; set; }
        public string QuestionText { get; set; }
        public string DataExportTag { get; set; }
    }
}
