﻿using System;
namespace AsistDataIngester.Models
{
    public class ExportFileInfo
    {
        public string FileId { get; set; }
        public double PercentComplete { get; set; }
        public string Status { get; set; }
    }
}
