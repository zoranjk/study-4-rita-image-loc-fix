// ASIST
const mqtt = require('mqtt');
const http = require('http');
const scaling_manager = require('./scaling_manager')

const config = require('./WaitingRoomConfig.json');

isMqttConnected = false;

globalSocket = null;

socketManager = null;

let globalSocketMap = new Map();

let callSignMap = new Map();

let players = [];

module.exports = {

    mqttInit:function( ){
        
        if( !isMqttConnected){
            
            // DEV
            //const connectionString = "localhost";
            
            // PROD
            const connectionString = process.env.mqtt_host
                        
            // doesn't store it's own clientId for some reason
            
            client  = mqtt.connect("mqtt://"+connectionString+":1883", { clientId:"WaitingRoomBackEnd" } );            
            
            client.clientId = "WaitingRoomNodeServer";

            // The availableHostIPs object is a map between:
            // key as a string: The ip address of an available ASIST/Minecraft testbed instance ready to accept players
            // value as a boolean: True if ready to accept a new team of players
            //
            // Example:
            //
            // { 
            //      "10.0.150.1": true,
            //      "10.0.150.2": false
            // }
                            
            client.availableHostIPs = {};
            client.numBusyInstances = 0;

            client.on("message", (topic,message)=>{
                //console.log("Topic : " + topic);
                //console.log("Message : " + message);
                parseTopic(topic,message,client);

                //console.log("object.qos : " + object.qos);
                //console.log("object.payload : " + object.payload);
            });
            
            client.on("connect",function(){	
                console.log("mqtt connected : " + client.connected);
                console.log("mqtt clientId : " + client.clientId);
                //console.log( client.constructor.name);
                isMqttConnected=true;
            });

            // client.on("disconnect",function(){	
            //     console.log("mqtt connected : " + client.connected);        
            //     isMqttConnected=false;
            // });

            client.on("close",function(){	
                console.log("mqtt connected : " + client.connected);        
                isMqttConnected=false;
            });            
 
            client.subscribe('trial');
            client.subscribe('observations/events/mission');
            client.subscribe('testbeds/state');
            client.subscribe('control/bounce')
            client.subscribe('status/clientmap/ready_for_team')
            client.subscribe('control/bounce/complete')               
            
        }
        else {

            console.log('Page refreshed, mqtt client still connected.');

        }

        return client;
    },

    generateAsistMqttMessage:function(message_type,message_version,sub_type,sub_type_version,data){
        const d = new Date().toISOString();

        const header = this.generateStandardAsistHeader(message_type,message_version,d)
        const msg = this.generateStandardAsistMsg(sub_type,sub_type_version,d)        

        const message = {
            "header": header,
            "msg" : msg,
            "data" : data
        }

        return message;

    },

    generateStandardAsistHeader:function(message_type,version,timestamp){
        
        const header = {
            "timestamp": timestamp,
            "message_type": message_type,
            "version": version
        } 
        
        return header;
    },

    generateStandardAsistMsg:function(sub_type,version,timestamp){
        const msg = {
            "experiment_id":"NOT_SET",
            "trial_id":"NOT_SET",
            "timestamp": timestamp,
            "source":"simulator",
            "sub_type": sub_type,
            "version":version
        } 
        
        return msg;
    },

    linkSocketMap:function(socketMap){

        console.log('Passing Socket Map after each User Authentication')

        globalSocketMap = socketMap;        

        for (let key of globalSocketMap.keys()) {
            console.log(key)
        }
    }   

};


const parseTopic = function( topic,message,client){
    
    splitString = String(topic).split('/')
    
    console.log('Topic: ' + topic);

    if (topic === 'trial'){         
        
        processTrial(message);
        
    }
    else if (topic === 'observations/events/mission') {

        processMission(message);
         
    }
    else if (topic === "status/clientmap/ready_for_team") {

        const jsonMessage = JSON.parse(message);
        
        const bool = jsonMessage['ready']

        scaling_manager.clientMapReadyForNewTeam = bool;
        
    }     
    else if (topic === 'testbeds/state') {

        processState(message,client);
        
    }
    
}

const processTrial=function( message ){    

    // process observation
    try {
        
        const jsonMessage = JSON.parse(message);
        
        const sub_type = jsonMessage['msg']['sub_type']

        

        if (sub_type == "start") { 
        
            console.log("Received trial start message!");
            
            const client_info = jsonMessage['data']['client_info']

            if (client_info != null){               
                
            }
        }
        else if (sub_type == "end" || sub_type == "stop") {            
            console.log("Received trial end message!");
            const client_info = jsonMessage['data']['client_info']
            if (client_info != null) {
                
            }
        }
    }
    catch(error) {
        console.log(error)        
    }    
}

const processMission = function(message) {     
        
    try{

        const jsonMessage = JSON.parse(message);
        const missionState = jsonMessage['data']['mission_state'];

        console.log("Received MissionState Message.") 
            
    }
    catch(sally){
        console.log(sally)
    }        
        
}

// process VM State messsage
const processState=function( message, client ){    
    try {        
        const jsonMessage = JSON.parse(message);

        if (jsonMessage != null && jsonMessage.data != null && jsonMessage.data.hosts != null) {
            const hosts = Object.keys(client.availableHostIPs);
            var numBusyInstances = 0;
            hosts.forEach( (host) => {
                if(Object.keys(jsonMessage.data.hosts).some(function(k){ return jsonMessage.data.hosts[k].host_ip == host; }) == false){
                    delete client.availableHostIPs[host];
                }                
            });

            jsonMessage.data.hosts.forEach( (host) => {
                if (host.instance_state != null && host.instance_state.message != null && host.instance_state.message != 'NOT_READY' && host.instance_state.message != 'FINISHED' &&
                 host.instance_state.message != 'WAITING_FOR_PLAYERS' && !host.instance_state.down) {
                    numBusyInstances++;
                    if (client.availableHostIPs[host.host_ip] != null) {
                        delete client.availableHostIPs[host.host_ip];
                    }
                }
                                
                if (host.host_ip != null && host.state == 'ok' && host.instance_state != null && host.instance_state.message == 'WAITING_FOR_PLAYERS' && client.availableHostIPs[host.host_ip] == null) {
                    client.availableHostIPs[host.host_ip] = true;
                    console.log('Found Ready IP: ' + host.host_ip);                 
                }

                if (host.instance_state != null && host.host_ip != null && host.instance_state.down) {
                    console.log("Sending message to terminate instance: " + host.host_ip);

                    // Send post
                    var options = {
                        hostname: config.scaling_metric.scaling_metric_service,
                        port:'80',
                        path:'/Instance?ip=' + host.host_ip,
                        method: 'DELETE',
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    };
                    
                    var request = http.request(options, function(req) {
                    });
                    request.on('error', function(err) {
                        // Handle error
                        console.log("Error terminating instance: " + err.message);
                    });
                    
                    request.end();                    
                }
            });

            client.numBusyInstances = numBusyInstances;
        }

        if(config.scaling_mode){
            console.log("Available Host IPs");
            console.log("==================");
            console.log(JSON.stringify( client.availableHostIPs ));
            console.log("Number of non waiting instances: " + client.numBusyInstances);
        }
       
    }
    catch(error) {
        console.log(error);        
    }    
};


