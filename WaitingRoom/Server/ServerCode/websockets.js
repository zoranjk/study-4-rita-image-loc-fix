const waiting_room = require('./waiting_room');
const backup = require('./backup_data');
const config = require('./WaitingRoomConfig.json');
const socketIO = require('socket.io');
const mqtt = require('./mqtt_manager');
const scaling_manager = require('./scaling_manager')

const socketManagerInstance = null;

class SocketManager {

    constructor(server,mqttClient){
        this.client_sockets = new Map();
        this.mqtt_client = mqttClient;
        this.socket_io = null;
        this.waiting_room_instance = null;
        this.bounceInProgress = false;
        this.options = {
          transport:['websocket'],
          allowUpgrades:true
        }
        this.mqtt_client.on("message", (topic,message)=>{
            if(topic === 'control/bounce'){   
                console.log(" Setting bounceInProgress to true")             
                this.bounceInProgress = true;                
            }
            else if(topic === 'control/bounce/complete'){
                console.log(" Setting bounceInProgress to false")
                this.socket_io.emit('bounceComplete',{})
                this.bounceInProgress = false;                
            }
        });
        this.init_socket_io(server,this.options)
        this.update_player_states_for_all()

        
        
    }

    init_socket_io(server,options){

        // Initiate Websocket service
        this.socket_io = socketIO(server, options);

        this.socket_io.on('connection', (socket) => {

            console.log( " Connection from : " + socket.client.id + " / " + socket.handshake.query.loggeduser)

            socket.is_connected = true;
            socket.pings_connected = 0;

            socket.on("disconnect", (reason) => {
                // socket is read-only when disconnected I believe
                
                //CLIENT DISCONNECTED
                const name = socket.playername
                console.log( "Client Socket disconnected :" + socket.id + " : "+ name)            
                if( this.waiting_room_instance.waiting_players.has(name)){                    
                    const player = this.waiting_room_instance.waiting_players.get(name)
                    player.disconnectedTimestamp = Date.now()
                    if( player.status === waiting_room.PlayerWaitingState.PROCEEDING_TO_TRIAL){
                        console.log( name + " Proceeded to Experiment. Non-Error disconnect.")                        
                        waiting_room_instance.remove_waiting_player(name);
                        this.client_sockets.delete(name)
                    }
                    else{
                        player.status = waiting_room.PlayerWaitingState.REGISTERED_DISCONNECTED
                        
                    }
                    player.socket = waiting_room.SocketState.DISCONNECTED
                }               
                
            }); 
            
            socket.on('logout', (name)=>{
                console.log('Removing '+ name + ' from the Waiting Room.')
                this.waiting_room_instance.remove_waiting_player(name)
                this.update_player_list_for_all();

            });
    
            socket.on('assign_name_to_socket', (data) => {
                console.log(data);
                const name = data['name'];
                socket.playername = name
                this.client_sockets.set(name, socket);                
                if( this.waiting_room_instance != null){
                    if( this.waiting_room_instance.waiting_players.has(name)){
                        const player = this.waiting_room_instance.waiting_players.get(name)
                        if( player.status = waiting_room.PlayerWaitingState.REGISTERED_DISCONNECTED){             
                            
                            player.status = waiting_room.PlayerWaitingState.REGISTERED_CONNECTED
                            player.socket = waiting_room.SocketState.CONNECTED                            
                        } 
                        const admin_stack_ip = process.env.admin_stack_ip;                
                        const admin_stack_port = process.env.admin_stack_port;
                        config.admin_stack_ip = admin_stack_ip
                        config.admin_stack_port = admin_stack_port
                        config.db_api_token = process.env.DB_API_TOKEN;
                                             
                        socket.emit('config',config);                       
                    }
                    else{
                        console.log(name + " connected but was not an expected player ... ");

                        const admin_stack_ip = process.env.admin_stack_ip;                
                        const admin_stack_port = process.env.admin_stack_port;
                        config.admin_stack_ip = admin_stack_ip
                        config.admin_stack_port = admin_stack_port
                                             
                        socket.emit('redirectToLogin',config);
                    }                   
                    this.update_player_list_for_all()                    
                } 
            });

            socket.on('reconnectSocket', (data) => {
                console.log(data);
                const name = data['name'];
                socket.playername = name
                this.client_sockets.set(name, socket);                
                if( this.waiting_room_instance != null){
                    if( this.waiting_room_instance.waiting_players.has(name)){
                        const player = this.waiting_room_instance.waiting_players.get(name)
                        player.disconnectedTimestamp = -1;
                        if( player.status = waiting_room.PlayerWaitingState.REGISTERED_DISCONNECTED){             
                            
                            player.status = waiting_room.PlayerWaitingState.REGISTERED_CONNECTED
                            player.socket = waiting_room.SocketState.CONNECTED                            
                        }                        
                        socket.emit('config',config);                       
                    }                   
                    this.update_player_list_for_all()                    
                } 
            });
            
            socket.on('KeepAlive', (name) => {

                const waiting_player = this.waiting_room_instance.waiting_players.get(name)
                if(waiting_player != null & waiting_player != undefined){

                    if ( this.client_sockets.has(name) ){

                        if( waiting_player.participant_id === 'NOT_SET'){
                            waiting_player.participant_id = backup.pidMap.get(name);
                        }
    
                        waiting_player.lastPingMilliseconds = Date.now();               
                        this.socket_io.emit('pings_connected',{"name":name,"pings_connected":++this.client_sockets.get(name).pings_connected})
                        this.update_player_list_for_one(socket);
                        //this.update_player_states_for_all(); 
                        socket.emit('bounceInProgress',this.bounceInProgress)
                    }
                    else {
                        //console.log(name + " socket not found");
                        //console.log(JSON.stringify(this.client_sockets));
                    } 

                }       
            });

            socket.on('intro_dialog_ack', (data)=>{
                const player_name = data
                if(this.waiting_room_instance.waiting_players.has(player_name)){
                    this.waiting_room_instance.waiting_players.get(player_name).intro_dialog_ack=true;                       
                }
            })

            socket.on('entrance_survey_taken', (data)=>{
                const player_name = data
                if(this.waiting_room_instance.waiting_players.has(player_name)){
                    const player_obj = this.waiting_room_instance.waiting_players.get(player_name)
                    player_obj.entrance_survey_taken=true;                    
                }
                
            }) 
            
            socket.on('publishSurveyObject', (data)=>{
                
                console.log(data) 
                
                const surveyObjectMessage = mqtt.generateAsistMqttMessage("simulator_event","1.2","Event:SurveyResponse","0.0.0",data)
                this.mqtt_client.publish( "surveyresponse", JSON.stringify(surveyObjectMessage));                
                
            })
            
            socket.on('proceedingToExperiment', (player_name)=>{
                
                console.log(player_name + " Proceeding to Experiment.")
                const waiting_player =  this.waiting_room_instance.waiting_players.get(player_name);
                if(waiting_player!=null && waiting_player!=undefined){
                    waiting_player.status = waiting_room.PlayerWaitingState.PROCEEDING_TO_TRIAL
                    this.waiting_room_instance.remove_waiting_player(player_name)
                }
            })             
        });

    }

    link_waiting_room(waiting_room_instance){

        this.waiting_room_instance = waiting_room_instance
        console.log("Waiting Room Linked to Socket Manager.")
        setInterval( ()=>{
            this.waiting_room_instance.should_start_experiment();
        },1000)
        
    }
    
    update_player_list_for_one(socket){
        try{
            if (this.waiting_room_instance != null) {
                const players = this.waiting_room_instance.get_waiting_players()
                const playersObj = []
                players.forEach( (playerInfo,name) =>{
                    playersObj.push(playerInfo)
                })
                socket.emit('player_list',playersObj)
                
            }            
        }
        catch(error){
            console.log(error)
        }        
    }

    update_player_list_for_all(){
        try{
            if (this.waiting_room_instance != null) {
                const players = this.waiting_room_instance.get_waiting_players()
                const playersObj = []
                players.forEach( (playerInfo) =>{
                    playersObj.push(playerInfo)
                })
                this.socket_io.emit('player_list',playersObj)
                
            }            
        }
        catch(error){
            console.log(error)
        }        
    }

    update_player_states_for_all(){

        setInterval( () =>{

            try {
            
                if (this.waiting_room_instance != null) {
                    const players = this.waiting_room_instance.get_waiting_players()
                    const playersObj = []
                    players.forEach( (playerInfo,name) =>{
                        playersObj.push( playerInfo.getJsonModel() )
                    })
                    
                    this.socket_io.emit('update_player_states', playersObj )
                }  
    
            }
            catch(error){
                console.log(error)
            }         

        },1000)
         
    }

    print_socket_map(){
        console.log( this.client_sockets )
    }
}

module.exports= {
    SocketManager,
    socketManagerInstance
}

