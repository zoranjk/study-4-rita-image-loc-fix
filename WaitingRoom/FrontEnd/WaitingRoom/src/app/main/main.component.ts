import { AfterContentInit, AfterViewInit, ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { PlayerManagerService } from '../Services/player-manager.service';
import { ResizingService } from '../Services/resizing.service';
import { WebsocketService } from '../Services/websocket.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { IntroDialogComponent } from '../intro-dialog/intro-dialog.component';
import { DomSanitizer } from '@angular/platform-browser';

import { SessionService } from '../Services/session.service';
import { Player } from '../Interfaces/PlayerModels';
import { ParticipantdbService } from '../Services/participantdb.service';
import { Subscription } from 'rxjs';
import { Trial } from '../Interfaces/Trial';

@Component({
  selector: 'app-main',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})

export class MainComponent implements OnInit,AfterViewInit,AfterContentInit, OnDestroy {

  faqUrl;
 
  colors=["red","blue","yellow","green","cyan"]
  trials: Trial[] = [];
  teamSubscription: Subscription = new Subscription;
  interval: any;
  selectedTrial: Trial | undefined;
  selectedTrialRank: number = 0;
  bombPhasesDisarmed: number = 0;
  
  constructor( 
    public websocketService:WebsocketService, 
    public sessionService:SessionService, 
    public playerManager:PlayerManagerService, 
    public resizingService:ResizingService,
    public participantDBService:ParticipantdbService, 
    public dialog: MatDialog,
    public sanitizer: DomSanitizer,
    private cdr: ChangeDetectorRef 
    ) {
    this.faqUrl = sanitizer.bypassSecurityTrustResourceUrl(location.origin + '/SPSO/FAQ?noheader=true');
  }

  getColor(i:number){

    return this.colors[i%(this.colors.length-1)];
  }

  ngOnInit(): void {    
    this.loadTeamData()
    this.interval = setInterval(() => { 
      this.loadTeamData()
    }, 180000);
    this.interval = setInterval(() => { 
      this.cdr.markForCheck();
    }, 3000);
  }

  ngAfterViewInit(): void {
    
    this.pulseSurveyButton();
    this.greetingDialog()    
    
  }

  ngAfterContentInit():void {
   
    // this.playerManager.players.set("TestPlayer",{
    //   name: "TestPlayer",
    //   participant_id: 'p100',
    //   pings_connected: 0,
    //   status: 'DISCONNECTED',
    //   socket: '',
    //   intro_dialog_ack: false,
    //   entrance_survey_taken: false,
    //   exit_survey_taken: false
    // })

  }

  ngOnDestroy(): void {
    this.teamSubscription.unsubscribe();
  }

  loadTeamData() {
    this.teamSubscription = this.participantDBService.getTeamData().subscribe(trials => {
      this.trials = [...trials]
    });

  }

  pulseSurveyButton(){
    
    const survey_button_element = document.getElementById("take-survey-button") as HTMLButtonElement
    
    if(survey_button_element){
      survey_button_element.animate([      
        // keyframes
        { color: 'White'},
        { color: 'Blue'},
        //{ transform: 'scale(1)' },      
        //{ transform: 'scale(1.5)' },
        { color: 'White'}

        //{ transform: 'scale(1)' },
        
        ], {
        // timing options
        duration: 2000,
        iterations: Infinity
      });
    }
    
  }

  proceedToExperiment() {
    
    this.websocketService.socket.emit('proceedingToExperiment',this.websocketService.assigned_name)    
    this.playerManager.players.delete(this.websocketService.assigned_name) 
    this.reroute_page();



    // DO THIS TO PASS DATA
    // this.dialogRef.close({
    //   trialDTO: this.trialDTO,
    //   experimentDTO: this.experimentDTO,
    // });

  }

  reroute_page():void{   
    
    // ADD PLAYER NAME AS URL PARAMETER AFTER TESTING AND UPDATING CLIENTMAP TO RECEIVE THAT ROUTE 
    let url = 'https://'+document.location.host+'/ClientMap/?player='+this.websocketService.assigned_name
    if(this.websocketService.config.scaling_mode == true){
      url = `https://${document.location.host}/${this.websocketService.instanceInternalIP}/ClientMap/?player=${this.websocketService.assigned_name}&externalIP=${this.websocketService.instanceIP}&internalIP=${this.websocketService.instanceInternalIP}`
    }
    
    window.location.href = url
  }

  logout(){
    alert( "You will now be removed from the Waiting List and return to the Login Page.")
    
    this.websocketService.socket.emit("logout",this.websocketService.assigned_name)
    setTimeout(()=>{
      const url = 'https://'+document.location.host+'/SPSO/Login'
      window.location.href = url
    },5000)
   
  }

  greetingDialog(){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.autoFocus = true;
    dialogConfig.disableClose = true;

    const dialogRef = this.dialog.open( IntroDialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe( from_dialog => {
      this.takeSurvey();
        if (from_dialog !== null) {
                   console.log(from_dialog)
                   
        }

      }
    );
  }

  
  takeSurvey(){
    
    this.playerManager.survey_session_active=true;

    this.participantDBService.getPlayerInformationForSurvey(this.playerManager.participant_id)
  }

  isPlayerReady(player:Player):Boolean{

    if(player.status !== null && player.status !== undefined ){
      if(player.status?.localeCompare("SELECTED_READY_TO_PLAY")==0){
        return true;
      }
      else if(player.status?.localeCompare("READY_TO_PLAY_ON_SELECTION")==0){
        return true;
      }
    }   
    return false
  }

  isFormingTeam(player:Player):Boolean{
    if(player.status !== null && player.status !== undefined ){
      if(player.status?.localeCompare("READY_TO_PLAY_ON_SELECTION")==0){
        if(player.team?.localeCompare("NOT_SET")==0 ){
          return true;
        }
        
      } 
    }  
    return false
  }
  isTeamFormed(player:Player):Boolean{
    //console.log( player.team );
    if(player.team !== null && player.team !== undefined ){
      if(player.team?.localeCompare("NOT_SET")!=0 ){
        return true;
      }     
    }
    return false
  }

  isLoadingServer(player:Player):Boolean{
    if(player.status !== null && player.status !== undefined ){
      if(player.status?.localeCompare("READY_TO_PLAY_ON_SELECTION")==0){
        if( this.isTeamFormed(player)){
          return true;
        }      
      }
    }    
    return false
  }

  isServerLoaded(player:Player):Boolean{
    if(player.status !== null && player.status !== undefined ){
      if(player.status?.localeCompare("SELECTED_READY_TO_PLAY")==0){
        return true;
      }
    }    
    return false
  }

}


