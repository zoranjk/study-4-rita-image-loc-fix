import { SurveysObject } from "./SurveyModels";

export interface Player {

    name:string;
    participant_id:string;
    pings_connected:number;
    status:string;
    socket:string;
    team:string;
    intro_dialog_ack:boolean;
    entrance_survey_taken:boolean;
    exit_survey_taken:boolean;

}

export interface User {

    userId?: string,
    participantId? : string,
    participationCount? : string,    
    preTrialSurveys? : SurveysObject,
    postTrialSurveys? : SurveysObject,
    optionalSurveys? : SurveysObject,          
    teamsList? : string    
}



export interface Team {    
    name: string,
    teamPlays: number,
    users: string
}

export interface TeamData {
    teamId: string,
    teamPlays: string[],
    teamScore: number,
    members: string[]
}