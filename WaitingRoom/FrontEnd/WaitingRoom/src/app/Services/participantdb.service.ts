import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../Interfaces/PlayerModels';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { SurveyComponent } from '../survey/survey.component';
import { SessionService } from './session.service';
import { PlayerManagerService } from './player-manager.service';
import { Observable } from 'rxjs';
import { Trial } from '../Interfaces/Trial';


@Injectable({
  providedIn: 'root'
})
export class ParticipantdbService {

  
  
  constructor( 
    public http:HttpClient,
    public dialog: MatDialog,
    public sessionService:SessionService,
    public playerManagerService:PlayerManagerService
  
  ) {

    playerManagerService.getPlayerInfoEmitter.subscribe( ()=>{

      this.getPlayerInformation(this.playerManagerService.participant_id)

    });
  }

  getPlayerInformation(pid:string){
    const headers = new HttpHeaders(
      { 
        'Content-Type': 'application/json', 
        'Authorization': 'Bearer ' + this.sessionService.bearerToken
      }
    );
    const url:string = window.location.origin +'/SSOService/user/GetPlayerStatsForAgentsById/'+pid;
    this.http.get( url,{headers: headers} ).subscribe({
      next: (v) => {
        console.log("Getting player information just after pid was set");
        console.log(v);       
        this.playerManagerService.aggregatedPlayerData = v as User;
      },
      error: (e) => console.error(e),
      complete: () => {} 
      }      
    )
  }

  getPlayerInformationForSurvey( pid:string){
    const headers = new HttpHeaders(
      { 
        'Content-Type': 'application/json', 
        'Authorization': 'Bearer ' + this.sessionService.bearerToken
      }
    );
    const url:string = window.location.origin +'/SSOService/user/GetPlayerStatsForAgentsById/'+pid;
    this.http.get( url,{headers: headers} ).subscribe({
      next: (v) => {
        console.log(v);
        //this.sessionService.surveysObject = this.player.preTrialSurveys as SurveysObject
        this.sessionService.setActiveSurveyIndex( v as User );

      },
      error: (e) => console.error(e),
      complete: () => {

        if(this.sessionService.activeSurveyIndex < 6){

          const dialogConfig = new MatDialogConfig();
          dialogConfig.autoFocus = true;
          dialogConfig.maxWidth='90vw';
          dialogConfig.maxHeight='90vh';
          dialogConfig.id = "SurveyDialog";
          const dialogRef = this.dialog.open( SurveyComponent, dialogConfig);

          dialogRef.afterClosed().subscribe( from_dialog => {
              if (from_dialog !== null) {
                        console.log(from_dialog)
              }
            }
          );

          } 
        } 
      }      
    )
  }

  getTeamData(): Observable<Trial[]> {
    const headers = new HttpHeaders(
      { 
        'Content-Type': 'application/json', 
        'Authorization': 'Bearer ' + this.sessionService.bearerToken
      }
    );
    return this.http.get<Trial[]>(window.location.origin + `/SSOService/trial/GetTopTrialRecords`, {headers: headers})
  }

}
