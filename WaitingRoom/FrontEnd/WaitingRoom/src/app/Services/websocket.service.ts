import { EventEmitter, Injectable } from '@angular/core';
import { Socket } from 'ngx-socket-io';
import { Player } from '../Interfaces/PlayerModels';
import { SessionService } from './session.service';
import { Config } from '../Interfaces/ConfigModel';

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {

  public assigned_name:string = "NOT SET";

  public config:Config = {
    "db_api_token":"NOT_SET"
  };
  
  player_name_emitter:EventEmitter<string> = new EventEmitter();
  player_ready_on_selection_emitter:EventEmitter<boolean> = new EventEmitter();
  player_selected_to_team_emitter:EventEmitter<boolean> = new EventEmitter();
  player_selected_emitter:EventEmitter<boolean> = new EventEmitter();
  survey_reminder_emitter:EventEmitter<boolean> = new EventEmitter();
  experiment_abandoned_emitter:EventEmitter<boolean> = new EventEmitter();
 
  player_list_emitter:EventEmitter<Player[]> = new EventEmitter();
  update_player_states_emitter:EventEmitter<Player[]> = new EventEmitter();
  
  update_player_pings_emitter:EventEmitter<{"name":string,"pings_connected":number}> = new EventEmitter();
  public instanceIP:string = '';
  public instanceInternalIP:string = '';

  public bounceInProgress = false;

  public disconnected:boolean =true;

  public timeToDisconnect = 0;

  public sessionEstablishedOnce= false;

  constructor(
    public socket:Socket,
    public sessionService:SessionService
    ) {

    

    console.log("WebsocketService Constructor");
    
    const path = document.location.pathname.split('/'); 
    console.log( path );
    const name = path[path.length-1]    
    this.assigned_name = name;
    socket.emit('assign_name_to_socket',{"name":name})
    console.log("Name emitted to server : " + name)
    
    this.player_name_emitter.emit(name)    
    
    this.keepAlivePing();

    socket.on('connect',()=> {
      console.log("Connected to WebSocket.")      
      
      //figure this resync out
      // CHECK if this particular session of the WaitingRoom has already been established once
      // If so than this connection was a reconnect (not refresh). 
      if(this.sessionEstablishedOnce = true){
        console.log("This was a socket reconnect!")
        socket.emit('reconnectSocket',{"name":name,"reconnect":true})
      }
      
      
      this.disconnected = false;
           
    });

    socket.on('disconnect',( reason:any)=> {
      console.log(reason);
      this.disconnected = true;
      setTimeout( ()=>{
        if( this.disconnected ){
          alert( "You have disconnected for 1 minute. Your client is considered timed out. You will now be removed from the Waiting List and returned to the Login Page.")    
          setTimeout(()=>{
            const url = 'https://'+document.location.host+'/SPSO/Login'
            window.location.href = url
          },3000)
        }
      },this.config.disconnectTimeout)
      setInterval( ()=>{
        if( this.disconnected ){
          this.timeToDisconnect!-=1000;
        }
        else{
          this.timeToDisconnect = this.config.disconnectTimeout!;
        }
      },1000)
    });    

    socket.on('config',( config:Config )=>{ console.log( config);
      this.config = config;
      this.sessionService.bearerToken = config.db_api_token;
      this.timeToDisconnect = this.config.disconnectTimeout!;
      this.disconnected = false;
      
    })   

    socket.on('bounceInProgress',( bool:boolean )=>{
      
      this.bounceInProgress = bool
      if(this.bounceInProgress){
        console.log("Bounce In Progress ...")
      }
      
      if( bool == false && this.sessionService.proceedDialogQueued == true){
        this.sessionService.proceedDialogQueued = false;
        this.player_selected_emitter.emit(true);
      }
            
    });
    
    socket.on('player_list',(data:Player[])=>{
      console.log(data)
      this.player_list_emitter.emit( data )
      this.disconnected = false;
    });

    socket.on('update_player_states', ( data:Player[])=>{
      //console.log("Updating Player States ... ")
      //console.log(data)
      this.update_player_states_emitter.emit( data )
      this.disconnected = false;
    })

    socket.on('reroute_to_clientmap',(ip:string)=>{ 
      const url = 'https://'+ip+'/ClientMap/player/'+ this.assigned_name;
      window.location.href = url     
    })

    socket.on('pings_connected',( data:{"name":string,"pings_connected":number} )=>{
      this.update_player_pings_emitter.emit(data);
      this.disconnected = false;
    })

    socket.on('ready_to_play_on_selection',( data:any ) => {
      console.log(data);
      this.player_ready_on_selection_emitter.emit(true);
      this.disconnected = false;
    })

    socket.on('player_selected_to_team',( data:any ) => {
      console.log("Selected to Team : " +data);
      this.player_selected_to_team_emitter.emit(true);
      this.disconnected = false;
    })

    socket.on('experiment_abandoned', ()=>{
      this.experiment_abandoned_emitter.emit(true);
      this.disconnected = false;
    })

    socket.on('selected_to_play',( instanceIP:string ) => {
      console.log('selected_to_play on ' + instanceIP);
      this.instanceIP = instanceIP;
      this.player_selected_emitter.emit(true);
      this.disconnected = false;
    });

    socket.on('instance_internal_ip',( instanceInternalIP:string ) => {
      console.log('instanceInternalIP: ' + instanceInternalIP);
      this.instanceInternalIP = instanceInternalIP;
    });

    socket.on('survey_reminder',(data:any)=>{      
      console.log(data);      
      this.survey_reminder_emitter.emit(true);
      this.disconnected = false;
    });

    this.socket.on("redirectToLogin",(config:Config)=>{

      let url = 'https://'+document.location.host+'/SPSO/Login';
      if(this.config.scaling_mode == true){
        url = `https://${config.admin_stack_ip}:${config.admin_stack_port}/SPSO/Login`
      }
      
      window.location.href = url

    });

  }

  private async keepAlivePing(){ 
    setInterval( ()=> {
        this.socket.emit('KeepAlive', this.assigned_name)             
    }, 1000 );        
  }

}
