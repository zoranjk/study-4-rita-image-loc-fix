import { TestBed } from '@angular/core/testing';

import { ParticipantdbService } from './participantdb.service';

describe('ParticipantdbService', () => {
  let service: ParticipantdbService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ParticipantdbService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
