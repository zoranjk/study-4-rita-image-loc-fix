import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { SurveysObject } from '../Interfaces/SurveyModels';
import surveysJsonFile from './AsistStudy4SurveyObject_Pre_Req_v3.json'
import { User } from '../Interfaces/PlayerModels';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  admin_stack_ip = "host.docker.internal"
  activeSurveyIndex = 0;
  devMode = false;
  surveysObject:any = {
    "participant_id":"NotSet",
    "surveys":[]
  };  
  proceedDialogQueued = false;
  bearerToken:string="NOT_SET";

  constructor() {   
    
    this.surveysObject = (surveysJsonFile as unknown as SurveysObject);
  }

  getAssetPath( target:string ):string{
    if(environment.production){      
      return window.location.origin+'/WaitingRoom/assets/'+target;
    }
    else{      
      return window.location.origin+'/assets/'+target;      
    }
  }

  setActiveSurveyIndex( player:User){

    if(player.preTrialSurveys == null){
      this.activeSurveyIndex = 0;
    }
    else{
      this.activeSurveyIndex = player.preTrialSurveys.surveys.length;
    }   
  }
}

