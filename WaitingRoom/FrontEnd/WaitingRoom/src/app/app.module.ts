import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { SocketIoModule,SocketIoConfig } from 'ngx-socket-io';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog'
import { MatRadioModule } from '@angular/material/radio'
import { MatSliderModule } from '@angular/material/slider'
import { MatProgressBarModule } from '@angular/material/progress-bar'
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner'
import { MatTabsModule } from '@angular/material/tabs'
import { MatDividerModule } from '@angular/material/divider';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TutorialPageComponent } from './tutorial-page/tutorial-page.component';
import { IntroDialogComponent } from './intro-dialog/intro-dialog.component';
import { ReadyDialogComponent } from './ready-dialog/ready-dialog.component';
import { SurveyComponent } from './survey/survey.component';
import { HttpClientModule } from '@angular/common/http';

const socket_io_config: SocketIoConfig = { url: 'wss://' + document.location.host , options: { transports: ['websocket'], path: '/WaitingRoom/socket.io' } };

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    TutorialPageComponent,
    IntroDialogComponent,
    ReadyDialogComponent,
    SurveyComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatButtonModule,
    MatCardModule,
    MatDialogModule,
    MatProgressBarModule,
    MatRadioModule,
    MatSliderModule,
    MatTabsModule,
    FormsModule,
    HttpClientModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    // DEBUG
    //SocketIoModule.forRoot({ url: 'ws://' + document.location.host, options: { transports: ['websocket'] }}),
    // PROD
    SocketIoModule.forRoot(socket_io_config),
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
