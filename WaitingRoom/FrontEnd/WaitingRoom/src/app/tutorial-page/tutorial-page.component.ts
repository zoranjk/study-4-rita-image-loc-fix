import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tutorial-page',
  templateUrl: './tutorial-page.component.html',
  styleUrls: ['./tutorial-page.component.scss']
})
export class TutorialPageComponent implements OnInit {

  page = 1

  constructor() { }

  ngOnInit(): void {
  }

  nextPage(){
    this.page++;
    this.resetScroll();
  }

  back(){
    this.page--;
    this.resetScroll();
  }

  resetScroll(){
    const div = document.getElementById('page-container') as HTMLDivElement;
    div.scrollTo(0,0)
    
  }

}
