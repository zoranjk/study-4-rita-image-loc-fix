
# Waiting Room Instructions

## About
The Waiting Room consists of an Angular Front End and a NodeJS Backend. It's purpose is to offer a Game Lobby experience in which players can take surveys and wait to be teamed up with other participants who are logged into the system.
  

## Building Docker container
  

The container can be built in Windows by running the **build_and_dockerize.ps1** script. This script can be easily adapted to run on Linux and Mac, as in the **build_and_dockerize.sh** script.
  

## Configuration  

The Waiting Room Service configuration is done in the **Local/WaitingRoom/WaitingRoomConfig.json** file

            {
                # Shows certain debugging information on the Front End UI
                "DebugMode": true,
                # Sets the Team Assembly parameters
                "team_assembly":{
                    # Set the number of players that make up a team
                    "players_per_team":3, 
                    # Set how the teams get formed - currently only FIFO is implemented   
                    "team_assembly_strategies":[
                        "FIFO",
                        "ADMIN"
                    ],
                    "team_assembly_strategy":"FIFO",
                    # Set the callsigns the system uses - this is where the callsigns are defined, nowhere else even though it may seem some other files have the same functionality
                    "callsigns":["Alpha","Bravo","Delta"]
                },
                # Set whether the system is running in scaling or single instance mode
                "scaling_mode": false,
                # Set the values the Google Cloud system requires to scale       
                "scaling_metric": {
                    "metric_value_per_instance": 10,
                    "scaling_metric_service": "scalingmetricservice"
                },
                # The set of ASI Advisor Conditions used in Study 4. If you have not built or obtained ASI code to represent one of these agents, feel free to remove them from this array and they will no longer appear in the Condition Cycles
                "advisor_conditions":[
                    "NO_ADVISOR",
                    "ASI_DOLL_TA1_RITA",
                    "ASI_CMU_TA1_ATLAS"
                ],
                # Set the timeout in milliseconds for kicking a players whose websocket was disconnected
                "disconnectTimeout":60000,
                # automatically set by Local/export_env_vars_and_testbed_up.sh
                "admin_stack_ip":"SET_BY_ENV_FILE",
                # automatically set by Local/export_env_vars_and_testbed_up.sh
                "admin_stack_port":"SET_BY_ENV_FILE",
                # automatically set by Local/export_env_vars_and_testbed_up.sh
                "db_api_token":"SET_BY_ENV_FILE"            
            }

## Debug environment

In order to use this debugging environment you will need to pre-build the Waiting Room, ClientMap, and the AsistControl services

Once the Waiting Room system has been built, a convenient debugging environment can be started  by running `docker compose up`. 

Assuming you are debugging @ localhost, once you're docker compose command has completed, you may check it's success by visiting the Logger at https://localhost:9000/Logger and inspecting each container's logs.

You will now need to send an https request to the Waiting Room backend to tell it that you are about to access the webpage. This can be done in windows with the following command, assuming you are running @ localhost:

`Invoke-WebRequest -UseBasicParsing https://localhost:9000/WaitingRoom/enter_player -ContentType "application/json" -Method POST -Body '{"name":"MinecraftTest","participant_id":"P000001"}'`

Once you receive a successful response (200) you can navigate to https://localhost:9000/WaitingRoom/player/MinecraftTest and continue debugging