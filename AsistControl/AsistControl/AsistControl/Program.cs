using AsistControl;

var builder = WebApplication.CreateBuilder(new WebApplicationOptions
{
    ApplicationName = typeof(Program).Assembly.FullName,
    ContentRootPath = Directory.GetCurrentDirectory(),    
    WebRootPath = "customwwwroot",
    
});

// Add services to the container.
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
//builder.Services.AddSwaggerGen();
// Legacy Setup Ported from MalmoControl
builder.WebHost.UseUrls("http://*:5002");

builder.WebHost.ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var env = hostingContext.HostingEnvironment;
                    config.AddJsonFile($"appsettings.{env.EnvironmentName}.json",
                          optional: false, reloadOnChange: true);                    
                });

//builder.WebHost.ConfigureLogging((hostingContext, logging) =>
//{
//    // Requires `using Microsoft.Extensions.Logging;`
//    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
//    logging.AddConsole();
//    logging.AddDebug();
//    logging.AddEventSourceLogger();
//});

// End Legacy Setup

// Manually create an instance of the Startup class
var startup = new Startup(builder.Configuration);

// Manually call ConfigureServices()
startup.ConfigureServices(builder.Services);

var app = builder.Build();

// Fetch all the dependencies from the DI container 
// var hostLifetime = app.Services.GetRequiredService<IHostApplicationLifetime>();
// As pointed out by DavidFowler, IHostApplicationLifetime is exposed directly on ApplicationBuilder

// Call Configure(), passing in the dependencies
startup.Configure(app,app.Environment, app.Lifetime);


// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    //app.UseSwagger();
    //app.UseSwaggerUI();
}
//app.UseHttpsRedirection();
//app.UseAuthorization();
app.MapControllers();
app.Run();
