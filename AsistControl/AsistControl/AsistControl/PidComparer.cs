﻿using System.Collections;
using System.Numerics;
using System.Text;

namespace AsistControl
{
    public class PidComparer : Comparer<string>
    {            

        public override int Compare(string? x, string? y)
        {
            string xVal = "";
            string yVal = "";
            bool skipLetters = true;

            for (int i = 0; i < x.Length; i++)
            {
                if ((x[i] != 'P') && (x[i] != 'p') && (x[i] != '0'))
                {
                    if (skipLetters)
                    {

                    }
                    else
                    {
                        skipLetters = false;
                        xVal += x[i];
                    }

                }
            }

            skipLetters = true;

            for (int i = 0; i < y.Length; i++)
            {
                if ((y[i] != 'P') && (y[i] != 'p') && (y[i] != '0'))
                {
                    if (skipLetters)
                    {

                    }
                    else
                    {
                        skipLetters = false;
                        yVal += y[i];
                    }

                }
            }

            int xNum = Int32.Parse(xVal);
            int yNum = Int32.Parse(yVal);

            if (xNum > yNum) return 1;
            else if (xNum == yNum) return 0;
            else if (xNum < yNum) return -1;

            return 0;
        }
    }
}
