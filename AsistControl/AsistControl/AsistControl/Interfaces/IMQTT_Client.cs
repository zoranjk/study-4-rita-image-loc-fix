﻿
namespace AsistControl.Interfaces
{
    public interface IMQTT_Client
    {
        Task connect();

        void publish(string messageText, string topic);

        void Setup();

        string lastAgentMessage {get;set;}
        
        string lastExperimentMessage {get;set;}

        string lastTrialMessage {get;set;}
    }
}
