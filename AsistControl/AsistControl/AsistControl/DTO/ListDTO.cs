﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsistControl.DTO
{
    public class ListDTO
    {
         public List<string> Data {get; set; } = new List<string>();
    }
}
