﻿namespace AsistControl.DTO
{
    public class WaitingRoomDTO
    {
        public List<WaitingParticipant>? waitingParticipants { get; set; }
        public WaitingRoomDTO() {
        
        }
    }

    public class WaitingParticipant : Participant
    {

        public int? time_waiting_milli { get; set; }

        public WaitingParticipant() {           

        }
    }
}
