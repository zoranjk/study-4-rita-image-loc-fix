﻿using AsistControl.Interfaces;
using System.Text.Json;

namespace AsistControl.DTO
{
    public class ExperimentDTO
    {
        public CommonHeaderDTO header {get;set;} = new CommonHeaderDTO();
        public StartExperimentMessageDTO msg {get;set;} = new StartExperimentMessageDTO();        
        public StartExperimentDataDTO data {get;set;} = new StartExperimentDataDTO();

        public ExperimentDTO() {

            header.message_type = "experiment";
            msg.experiment_id = Guid.NewGuid().ToString();

        }

        public ExperimentDTO(string author, string name, string condition)
        {

            header.message_type = "experiment";
            msg.experiment_id = Guid.NewGuid().ToString();
            data.author = author;
            data.name = name;

            string agent = Environment.GetEnvironmentVariable("asi");

            if (agent != null && (agent.CompareTo(String.Empty) != 0))
            {                
                data.condition = agent;
            }
            else
            {
                data.condition = condition;

            }
            Console.WriteLine("Experiment Condition : " + data.condition);


           

        }


        public string convertToJsonString(){

            return JsonSerializer.Serialize(this);
        }
    }
   
    public class StartExperimentMessageDTO {
       
        public string sub_type {get;set;} = "create";
        public string source {get;set;} = "gui";
        public string timestamp {get;set;} = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff")+'Z';        
        public string experiment_id {get;set;} = "NOT SET";
        public string version {get;set;} = "1.1";
        public string? replay_parent_type {get;set;}
        public string? replay_parent_id {get;set;}
        public string? replay_id {get;set;}

    }

    public class StartExperimentDataDTO { 
        
        public string date {get;set;} =  DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff")+'Z';
        public string name {get;set;} = "NOT SET";
        public string author {get;set;} = "NOT SET";
        public string mission {get;set;} = "Study 4 MISSION A";
        public string condition { get; set; } = "NOT_SET";

    }
   

}
