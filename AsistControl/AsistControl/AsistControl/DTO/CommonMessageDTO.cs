﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsistControl.DTO
{
    public class CommonMessageDTO
    {
        public string experiment_id { get; set; } = "NOT_SET";
        public string trial_id { get; set; } = "NOT_SET";
        public string timestamp { get; set; } = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
        public string source { get; set; } = "simulator";
        public string sub_type { get; set; } = "NOT_SET";
        public string version { get; set; } = "NOT_SET";
        public string? replay_parent_type { get; set; }
        public string? replay_parent_id { get; set; }
        public string? replay_id { get; set; }
    }
}
