﻿using System.Text.Json;

namespace AsistControl.DTO
{
    public class TrialDTO
    {
        public CommonHeaderDTO header {get;set;}
        public StartTrialMessageDTO msg {get;set;}        
        public StartTrialDataDTO data {get;set;}

        public TrialDTO() { 
        
            header = new CommonHeaderDTO();
            header.message_type = "trial";
            msg = new StartTrialMessageDTO();
            data = new StartTrialDataDTO();
        
        }

        public TrialDTO( TeamInfoDTO teamInfoDto, ExperimentDTO experimentDto)
        {

            header = new CommonHeaderDTO();
            header.message_type = "trial";
            msg = new StartTrialMessageDTO(experimentDto);
            data = new StartTrialDataDTO( teamInfoDto,experimentDto);

        }

       
        public TrialDTO DeepCopy()
        {           
            TrialDTO copy = new TrialDTO();
            copy.header = new CommonHeaderDTO();
            copy.header.version = header.version;
            copy.header.timestamp = header.timestamp;
            copy.header.message_type= header.message_type;
            
            copy.msg = new StartTrialMessageDTO();
            copy.msg.version = msg.version;
            copy.msg.timestamp = msg.timestamp;
            copy.msg.source =   msg.source;
            copy.msg.experiment_id = msg.experiment_id;
            copy.msg.trial_id = msg.trial_id;
            copy.msg.sub_type = msg.sub_type;
            copy.msg.replay_id = msg.replay_id;
            copy.msg.replay_parent_id = msg.replay_parent_id;
            copy.msg.replay_parent_type = msg.replay_parent_type;

            copy.data = new StartTrialDataDTO();
            copy.data.trial_end_condition = data.trial_end_condition;
            copy.data.date = data.date;
            copy.data.name = data.name;
            copy.data.team_id = data.team_id;
            copy.data.experimenter= data.experimenter;
            copy.data.subjects = data.subjects.Select(p => p).ToList();
            copy.data.notes = data.notes.Select(p => p).ToList();
            copy.data.testbed_version = data.testbed_version;
            copy.data.trial_number = data.trial_number;
            copy.data.group_number = data.group_number;
            copy.data.study_number = data.study_number;
            copy.data.condition = data.condition;
            copy.data.experiment_name = data.experiment_name;
            copy.data.experiment_date = data.experiment_date;
            copy.data.experiment_author = data.experiment_author;
            copy.data.experiment_mission = data.experiment_mission;
            copy.data.map_name = data.map_name;
            copy.data.map_block_filename = data.map_block_filename;
            copy.data.client_info = data.client_info.Select( info => info.DeepCopy() ).ToList();
            copy.data.intervention_agents = data.intervention_agents.Select(a => a).ToList();
            copy.data.observers = data.observers.Select(a => a).ToList();

            return copy;
        }

        public string convertToJsonString(){
            
             return JsonSerializer.Serialize(this);
            
        }
    }

    public class StartTrialMessageDTO
    {

        public string experiment_id { get; set; } = "Not Set";
        public string trial_id { get; set; } = Guid.NewGuid().ToString();
        public string sub_type { get; set; } = "start";
        public string source { get; set; } = "gui";
        public string timestamp { get; set; } = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
        public string version { get; set; } = "0.1";
        public string? replay_parent_type { get; set; }
        public string? replay_parent_id { get; set; }
        public string? replay_id { get; set; }


        public StartTrialMessageDTO(){}
        public StartTrialMessageDTO(ExperimentDTO experimentDTO)
        {
            this.experiment_id = experimentDTO.msg.experiment_id;
        }
    }


    public class StartTrialDataDTO
    {                
        public string date { get; set; } = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
        public string name { get; set; } = "NOT SET ";
        public string team_id { get; set; } = "NOT SET ";
        public string experimenter { get; set; } = "NOT SET ";
        public List<string?> subjects { get; set; } = new List<string?>();
        public List<string?> notes { get; set; } = new List<string?>();
        public string testbed_version { get; set; } = "NOT SET"; // should make this pull automatically from appsettings
        public string trial_number { get; set; } = "NOT SET ";
        public string group_number { get; set; } = "NOT SET ";
        public string study_number { get; set; } = "NOT SET ";
        public string condition { get; set; } = "NOT SET ";
        public string experiment_name { get; set; } = "NOT SET ";
        public string experiment_date { get; set; } = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
        public string experiment_author { get; set; } = "NOT SET ";
        public string experiment_mission { get; set; } = "NOT SET ";
        public string map_name { get; set; } = "NOT SET ";
        public string map_block_filename { get; set; } = "NOT SET ";        
        public string trial_end_condition { get; set; } = Experiment.TrialEndCondition.NOT_SET.ToString();
        public List<ClientInfoDTO> client_info { get; set; } = new List<ClientInfoDTO>();
        public List<string> intervention_agents { get; set; } = new List<string>();
        public List<string> observers { get; set; } = new List<string>();

        // WITH GUI CONSTRUCTOR
        public StartTrialDataDTO() { }

        // HEADLESS CONSTRUCTOR
        public StartTrialDataDTO(TeamInfoDTO teamInfoDto, ExperimentDTO experimentDto)
        {

            Console.WriteLine("TeamInfoDto from TrialDTO constructor : " + teamInfoDto.convertToJsonString());
            name = teamInfoDto.TeamId + "_" + teamInfoDto.NumTrials;
            team_id = teamInfoDto.TeamId;
            experimenter = "ADMINLESS";
            subjects = teamInfoDto.Members!.Select(p => p.participant_id).ToList();
            // notes
            // trial_number
            // group_number
            // study_number
            condition = experimentDto.data.condition;
            experiment_name = experimentDto.data.name;
            experiment_author = experimentDto.data.author;
            experiment_mission = experimentDto.data.mission;            
            client_info = teamInfoDto.Members!.Select(p => new ClientInfoDTO(p)).ToList();
                        
            // THESE ARE SET IN THE CONTROLLER WHEN WE GET THE ACTUAL START TRIAL MSG
            // map_name
            // map_block_filename
            // intervention_agents
        }

    }

    public class ClientInfoDTO
    {
        public ClientInfoDTO() { }

        public ClientInfoDTO(SelectedParticipant participant)
        {
            this.playername = participant.name!;
            this.participant_id = participant.participant_id!;
            this.callsign = participant.callsign!;
        }

        public ClientInfoDTO DeepCopy()
        {
            ClientInfoDTO dto = new ClientInfoDTO();
            dto.playername = this.playername;
            dto.participant_id= this.participant_id;
            dto.callsign = this.callsign;
            return dto;
        }

        public string? playername { get; set; } = null;
        public string callsign { get; set; } = "NOT SET ";
        public string participant_id { get; set; } = "NOT SET ";


    }


}

