﻿using Microsoft.VisualBasic;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace AsistControl.DTO
{
    public class TeamInfoDTO
    {
        public string TeamId = "NOT_SET";

        public int NumTrials = 0;

        public List<SelectedParticipant>? Members = new List<SelectedParticipant>();

        public TeamInfoDTO() { }

        // INCOMING
        // Incoming JsonArray of
        // this.name = "NOT_SET",
        //this.participant_id ="NOT_SET",
        //this.socket = "DISCONNECTED",
        //this.callsign = "NOT_SET"
        //this.continue_with_team = true   

        public TeamInfoDTO(JsonArray selected_participants)
        {
            Console.WriteLine("Creating TeamInfoDTO ... ");
            // callsigns should come from a config
            //string[] callsigns = {"RED", "GREEN", "BLUE"};
            List<string> pids = new List<string>();
            for( int i = 0; i < selected_participants.Count; i++)
            {                
                JsonObject o = JsonSerializer.Deserialize<JsonObject>(selected_participants.ElementAt(i))!;
                SelectedParticipant p = new SelectedParticipant();
                p.name = (string)o["name"]!;
                p.callsign = (string)o["callsign"]!;
                p.participant_id = (string)o["participant_id"]!;
                pids.Add(p.participant_id);
                p.email = (string)o["email"]!;
                Members.Add(p);
            }
            pids.Sort(comparePids);
            TeamId = string.Join("_",pids);
           
           
        }

        public string convertToJsonString() {
            JsonArray p_array = new JsonArray();
            foreach( SelectedParticipant p in Members!) {
                p_array.Add(JsonSerializer.Serialize(p));
            }
            JsonObject jsonObject = new JsonObject
            {
                ["participants"] = p_array,
                ["teamid"]= TeamId

            };
            return jsonObject.ToString(); 

        }

        public int comparePids(string x, string y)
        {
            //Console.WriteLine(x);
            //Console.WriteLine(y);
            
            string xVal = "";
            string yVal = "";
            bool skipLetters = true;

            for (int i = 0; i < x.Length; i++)
            {
                if (skipLetters)
                {
                    if ((x[i] == 'P') || (x[i] == 'p') || (x[i] == '0'))
                    {
                        //Console.WriteLine("Skipping X : " + x[i]);
                    }
                    else
                    {
                        skipLetters = false;
                        xVal += x[i];
                        //Console.WriteLine("Writing X : " + x[i]);
                    }
                }
                else
                {
                    //Console.WriteLine("Writing X : " + x[i]);                    
                    xVal += x[i];
                }
            }

            skipLetters = true;

            for (int i = 0; i < y.Length; i++)
            {
                if (skipLetters)
                {
                    if  ((y[i] == 'P') || (y[i] == 'p') || (y[i] == '0'))
                    {
                        //Console.WriteLine("Skipping Y : " + y[i]);
                    }
                    else
                    {
                        skipLetters = false;
                        yVal += y[i];
                        //Console.WriteLine("Writing Y : " + y[i]);
                    }
                }
                else
                {
                    //Console.WriteLine("Writing Y : " + y[i]);
                    yVal += y[i];
                }
            }
            //Console.WriteLine(xVal + " comp " + yVal);
            int xNum = Int32.Parse(xVal);
            int yNum = Int32.Parse(yVal);
            //Console.WriteLine(xNum + " comp " + yNum);

            if (xNum > yNum) return 1;
            else if (xNum == yNum) return 0;
            else if (xNum < yNum) return -1;

            return 0;
        }

    }

    public class SelectedParticipant : Participant
    {
        public string? callsign { get; set; }       

    }   
}
