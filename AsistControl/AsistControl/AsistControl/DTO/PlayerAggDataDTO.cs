﻿using System.Text.Json;

namespace AsistControl.DTO
{
    public class PlayerAggDataDTO
    {
        public CommonHeaderDTO header {get;set;}
        public CommonMessageDTO msg {get;set;}        
        public UserMessageBusDTO data {get;set;}

        public PlayerAggDataDTO() { 
        
            header = new CommonHeaderDTO();
            header.message_type = "metadata";
            msg = new CommonMessageDTO();
            msg.sub_type = "Event:PlayerDataAggregated";
            msg.version = "0.0.0";
            data = new UserMessageBusDTO();
        
        }

        public PlayerAggDataDTO(UserMessageBusDTO data )
        {

            header = new CommonHeaderDTO();
            header.message_type = "metadata";
            msg = new CommonMessageDTO();
            msg.sub_type = "Event:PlayerDataAggregated";
            msg.version = "0.0.0";
            this.data = data;

        }

        public string convertToJsonString(){
            
             return JsonSerializer.Serialize(this);
            
        }
    }

   

}

