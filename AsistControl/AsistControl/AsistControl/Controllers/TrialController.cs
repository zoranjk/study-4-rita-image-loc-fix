﻿using AsistControl.DTO;
using AsistControl.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Text.Json.Nodes;
using System.Text.Json;
using AsistControl.DTO.DBModels;

namespace AsistControl.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrialController : ControllerBase
    {
        private IMQTT_Client _client;
        private IConfiguration _config;
        private IExperiment _experiment;


        public TrialController(IMQTT_Client client, IConfiguration config, IExperiment experiment)
        {

            _client = client;
            _config = config;
            _experiment = experiment;
            
        }

        private string randomlySelectMapLayout()
        {
            string layout = null;

            string file = Environment.GetEnvironmentVariable("map_blocks_file");

            Console.WriteLine(_config.GetSection("About")["system_version"]);
            Console.WriteLine( JsonSerializer.Serialize(_config.GetSection("MapBlockLayouts").Get<List<string>>()));
            if (_experiment.MapBlocksLayouts == null || _experiment.MapBlocksLayouts.Count == 0)
            {
                _experiment.MapBlocksLayouts = _config.GetSection("MapBlockLayouts").Get<List<string>>();
            }

            if (file != null && (file.CompareTo(String.Empty)!=0))
            {
                Console.WriteLine("using Env Var Layout override : " + layout);
                layout = file;                
            }
            else
            {   

                int index = new Random().Next(0, _experiment.MapBlocksLayouts.Count);
                layout = _experiment.MapBlocksLayouts.ElementAt(index);
                _experiment.MapBlocksLayouts.RemoveAt(index);

                if (_experiment.MapBlocksLayouts.Count == 0)
                {
                    _experiment.MapBlocksLayouts = _config.GetSection("MapBlockLayouts").Get<List<string>>();
                }

            }
            

            Console.WriteLine("Layout Selected : " + layout);
            Console.WriteLine( JsonSerializer.Serialize(_experiment.MapBlocksLayouts) );
            return layout;
        }

        [HttpGet("[action]")]
        public IActionResult getTrialId()
        {

            StringDTO stringDTO = new StringDTO();
            stringDTO.Data = _experiment.trial_id.ToString();

            return Ok(stringDTO);

        }

        [HttpGet("[action]")]
        public IActionResult getTrialRecord()
        {           

            return Ok(_experiment.trialSummaryDB);

        }

        // Adminless uses primarily the other version of this below, but we will keep
        // the overload in case we want to create a specific TrialDTO object

        [HttpPut("[action]")]
        public IActionResult StartTrial(TrialDTO dto)
        {

           
            Console.WriteLine("Starting Trial with :  " + dto.convertToJsonString().ToString());

            dto.header.message_type = "trial";

            dto.msg.trial_id = Guid.NewGuid().ToString();
            dto.data.experiment_date = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
            dto.data.testbed_version = _config.GetSection("About")["system_version"];
            _experiment.trialDTO = dto; 
            _experiment.experiment_id = dto.msg.experiment_id;
            _experiment.trial_id = dto.msg.trial_id;

            // make a copy with its own reference
            TrialDTO copy = dto.DeepCopy();
            // clear out playernames
            copy.data.client_info.ForEach(client => {
                client.playername = null;
            });
            //publish to message bus
            try
            {
                _client.publish(copy.convertToJsonString().ToString(), "trial");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            return Ok(copy);
        }

        //Adminless            
        [HttpPost("[action]")]
        public async Task<IActionResult> StartTrial( JsonArray obj )
        {
            // WE NOW HAVE TO CREATE THIS TRIAL DTO USING PARTICIPANT INFORMATION WE OBTAIN FROM THE TEAM FORMATION
            // PROCESS 

            _experiment.playerArrayToStartTrial = obj;

            Console.WriteLine(JsonSerializer.Serialize(obj));
            
            // TeamInfoDTO takes a json array of selected participants
            // if we get the new team stats from the db or somewhere else between trials, this teaminfodto can be updated then
            _experiment.teamInfoDTO = new TeamInfoDTO(obj);

            

            string teamRecordString = await NonAPIExperimentControl.getTeamRecordFromDBasTeamDB(_experiment);
            if (teamRecordString != null )
            {
                Console.WriteLine($"{teamRecordString}");
                _experiment.teamInfoDTO.NumTrials = _experiment.teamDB.TeamPlays.Count();               
                Console.WriteLine( _experiment.teamDB.TeamPlays.Count() );
            }
            else
            {
                Console.WriteLine("TeamId Not found in DB. Assuming this is a new team.");
            }

            TrialDTO dto = new TrialDTO( _experiment.teamInfoDTO, _experiment.experimentDTO);
            dto.data.testbed_version = _config.GetSection("About")["system_version"];
            //dto.data.map_name = "Dragon_1.1_3D";
            dto.data.map_name = _config.GetSection("Minecraft")["MapName"];

            // THIS WILL CYCLE THROUGH EACH POSSIBILITY UNTIL ALL ARE EXHAUSTED AND THEN REPOPULATE
            dto.data.map_block_filename = randomlySelectMapLayout();

            List<string> agents = new List<string>();
            try {
                string agent = Environment.GetEnvironmentVariable("asi");
                
                if (agent != null && (agent.CompareTo(String.Empty) != 0))
                {
                    Console.WriteLine("using Env Var Agent override : " + agent);
                    agents.Add(agent);
                }
                else
                {
                    agents.Add(_experiment.experimentDTO.data.condition);

                }
                Console.WriteLine("Agent : " + agent);
                
            }
            catch (Exception e) {
                Console.WriteLine(e.StackTrace);
            }
           
            dto.data.intervention_agents = agents;
            
            Console.WriteLine("Starting Trial with :  " + dto.convertToJsonString().ToString());           

            _experiment.experiment_id = dto.msg.experiment_id;
            _experiment.trial_id = dto.msg.trial_id;

            // store dto in experiment singleton
            _experiment.trialDTO = dto;
            // make a copy with its own reference
            TrialDTO copy = dto.DeepCopy();
            // clear out playernames
            copy.data.client_info.ForEach(client => {
                client.playername = null;
            });

            Console.WriteLine("Stored Experiment Trial DTO :  " + _experiment.trialDTO.convertToJsonString().ToString());

            //publish to message bus
            try
            {
                _client.publish(copy.convertToJsonString().ToString(), "trial");
                
                // This is now necessary to give the ClientMap TrialInfo separately from the trial start message
                TrialInfo trialInfoObject = new TrialInfo(_experiment);                
                _client.publish(trialInfoObject.convertToJsonString(), "control/response/getTrialInfo");       
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            // GET AND PUBLISH AGGREGATED PLAYER DATA FOR EACH PLAYER            
            string? ip = Environment.GetEnvironmentVariable("admin_stack_ip");
            string? port = Environment.GetEnvironmentVariable("admin_stack_port");
            if (port == null)
            {
                port = "443";
            }

            Console.WriteLine("ADMIN_STACK_IP : " + ip);
            Console.WriteLine("ADMIN_STACK_PORT : " + port);

            if (ip.CompareTo("localhost") == 0) {
                // this only works for Windows
                ip = "host.docker.internal";
            }

            string aggregatePlayerString = await NonAPIExperimentControl.grabAggregatePlayerData(_experiment, obj, _client);
            //Console.WriteLine(aggregatePlayerString);  

            string trialUpdateReturned = await NonAPIExperimentControl.updateOrCreateTrialRecord(_experiment, true);
            Console.WriteLine(trialUpdateReturned);
            string teamUpdateReturned = await NonAPIExperimentControl.updateOrCreateTeamRecord(_experiment);
            Console.WriteLine(teamUpdateReturned);

            return Ok(dto);
        }


        [HttpGet("[action]")]
        public IActionResult StopTrial()
        {

            TrialDTO dto = _experiment.trialDTO;
            string timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';

            dto.header.timestamp = timestamp;
            dto.msg.timestamp = timestamp;
            dto.msg.sub_type = "stop";

            _experiment.trialDTO = dto;

            Console.WriteLine("Stopping Trial with id :  " + _experiment.trial_id);

            //publish to message bus
            try
            {
                _client.publish(_experiment.trialDTO.convertToJsonString().ToString(), "trial");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            Console.WriteLine(" Stop Trial received ... PRE container kill async task");
            // Task.Run() i async so the method wil return before it finishes and prevent the error where Trial Stop message is not
            // published

            Console.WriteLine(" Stop Trial received ... POST container async task");

            return Ok(_experiment.trialDTO);
        }
    }
}
