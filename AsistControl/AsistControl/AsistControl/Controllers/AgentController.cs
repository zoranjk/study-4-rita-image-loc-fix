﻿using AsistControl.DTO;
using AsistControl.Interfaces;
using Microsoft.AspNetCore.Mvc;


namespace AsistControl.Controllers
{
    [Produces("Application/json")]
    [Consumes("Application/json")]
    [Route("api/[controller]")]
    [ApiController]

    public class AgentController : ControllerBase
    {
        
        private IMQTT_Client _client;
        private IConfiguration _config;
        private IExperiment _experiment;
        private Dictionary<string, UpDown> agentScripts;
        private ILogger<AgentController> _logger;


        public AgentController(IMQTT_Client client, IConfiguration config, IExperiment experiment, ILogger<AgentController> logger)
        {

            _logger = logger;
            _client = client;
            _config = config;
            _experiment = experiment;          

            IConfigurationSection a = config.GetSection("AgentShellScripts");

            agentScripts = new Dictionary<string, UpDown>();

            foreach (var child in a.GetChildren())
            {
                agentScripts.Add(child.Key, new UpDown(child.GetValue<string>("up"), child.GetValue<string>("down")));

            }
        }        

        [HttpPut("[action]")]
        public IActionResult LaunchAgent( AgentControlDTO dto ){

            try
            {
                string? admin_stack_ip = Environment.GetEnvironmentVariable("admin_stack_ip");
                string? instance_ip = Environment.GetEnvironmentVariable("instance_ip");
                string? db_api_token = Environment.GetEnvironmentVariable("DB_API_TOKEN");

                string agentCommand = "NOT_SET";

                if ( dto.agent_id!.ToLower().Contains("rita"))
                {
                    agentCommand = "cd Agents && cd Rita_Agent && dos2unix rita_up.sh && bash rita_up.sh";
                    
                }
                else if (dto.agent_id!.ToLower().Contains("atlas"))
                {                    
                    agentCommand = "cd Agents && cd ASI_CMU_TA1_ATLAS && dos2unix up.sh && bash up.sh";
                }

                /*
                //BashHelper.Bash("echo \"Running Test\"");
                //string result = BashHelper.Bash("cd Agents && cd Rita_Agent && echo $PWD");
                string result = BashHelper.Bash("cd Agents && cd Rita_Agent && bash rita_up.sh");
                Console.WriteLine(result);
                */

                //string result = BashHelper.Bash("cd Agents && cd Rita_Agent && bash rita_up.sh");
                
               
                string command =
                " echo $pwd "
                + " && ADMIN_STACK_IP=" + admin_stack_ip
                + " && export ADMIN_STACK_IP"
                + " && INSTANCE_IP=" + instance_ip
                + " && export INSTANCE_IP"
                + " && DB_API_TOKEN="+db_api_token
                + " && export DB_API_TOKEN"
                + " && "+ agentCommand;

                BashHelper.Bash(command);
               
                
            }
            catch (Exception e)
            {
                _logger.LogInformation(e.StackTrace);
                return Ok(e.StackTrace);
            }
            return Ok(dto);
            
        }     

        internal class UpDown
        {
            public string up;
            public string down;
            internal UpDown(string up, string down)
            {
                this.up = up;
                this.down = down;
            }
        }
    }
}