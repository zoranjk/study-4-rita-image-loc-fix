﻿
using AsistControl.DTO;
using AsistControl.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Nodes;

namespace AsistControl.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExperimentController : ControllerBase
    {
        private IMQTT_Client _client;
        private IConfiguration _config;
        private IExperiment _experiment;
        

        public ExperimentController(IMQTT_Client client, IConfiguration config, IExperiment experiment){
            
            _client = client;            
            _config = config;
            _experiment= experiment;
        }       

        [HttpGet("[action]")]
        public IActionResult getExperimentId(){

            StringDTO stringDTO = new StringDTO();
            stringDTO.Data = _experiment.experimentDTO.msg.experiment_id.ToString();

            return Ok(stringDTO);
            
        }

        [HttpGet("[action]")]
        public IActionResult getIds(){

            IdDTO idDTO = new IdDTO();
            idDTO.trial_id = _experiment.trialDTO.msg.trial_id;
            idDTO.experiment_id = _experiment.experimentDTO.msg.experiment_id;


            return Ok(idDTO);
            
        }

        
        [HttpGet("[action]")]
        public async Task<IActionResult> fetchExperiments(  ){            
            
            string url = _config.GetSection("MetadataServer")["url"];
            Console.WriteLine ( "Attempting to contact : " + url );
            JsonObject responseObject = new JsonObject(){ 
                ["experiments"] = null
            };
            using (var handler = new HttpClientHandler())
            {
                handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
                using ( var response = await new HttpClient(handler).GetAsync(url) )
                {
                    try{                 
                         
                        responseObject["experiments"] = await response.Content.ReadAsStringAsync();
                        //Console.WriteLine(responseObject["experiments"]);
                    }  
                    catch(HttpRequestException e)
                    {
                        Console.WriteLine("\nException Caught!");	
                        Console.WriteLine("Message :{0} ",e.Message);
                    }
                }
            }            

            return Ok(responseObject);
        
        }
       

        // NON-HEADLESS
        
        [HttpPost("[action]")]
        public IActionResult createExperiment(ExperimentDTO dto){

            Console.WriteLine("Starting Experiment with :  " + dto);            

            // keep last Experiment message client
            _client.lastExperimentMessage = dto.convertToJsonString();

            //publish to message bus
            try {                
                _client.publish(_client.lastExperimentMessage.ToString(),"experiment");
            }
            catch(Exception e){ 
                Console.WriteLine(e);                
            }

            return Ok(dto);
            
        }

        // HEADLESS - MAKE ONE OF THESE FOR EACH TEAM SESSION - TRIALS ORGANIZED UNDER THIS EXPERIMENT
        // UNTIL THE TEAM DISBANDS
        // EXAMPLE https://localhost:5002/api/Experiment/CreateExperiment?author=Ed&name=Test11&condition=ASI_DOLL_TA1_RITA
        [HttpGet("[action]")]
        public async Task<IActionResult> createExperiment( [Required]string author, [Required]string name, [Required] string condition)
        {
            
            ExperimentDTO dto = new ExperimentDTO(author, name, condition);

            _experiment.experimentDTO = dto;

            Console.WriteLine("Creating and Selecting Experiment with :  " + dto);

            // keep last Experiment message client
            _client.lastExperimentMessage = dto.convertToJsonString();

            //publish to message bus
            try
            {
                _client.publish(_client.lastExperimentMessage.ToString(), "experiment");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

            /*
            bool off = bool.Parse(_config.GetSection("MetadataServer")["off_for_core_testing"]);
            if (off) {

                Console.WriteLine(" METADATA SERVICES ARE OFF FOR TESTING - SO NO DB IS STORING THIS CREATED EXPERIMENT!");                
            }
            else
            {

                string url = _config.GetSection("MetadataServer")["url"];
                Console.WriteLine("Attempting to contact : " + url);
                JsonObject responseObject = new JsonObject()
                {
                    ["experiments"] = null
                };

                
                using (var handler = new HttpClientHandler())
                {
                    handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
                    using (var response = await new HttpClient(handler).GetAsync(url))
                    {
                        try
                        {
                            responseObject["experiments"] = await response.Content.ReadFromJsonAsync<JsonArray>();
                            //Console.WriteLine(responseObject["experiments"]);
                        }
                        catch (HttpRequestException e)
                        {
                            Console.WriteLine("\nException Caught!");
                            Console.WriteLine("Message :{0} ", e.Message);
                        }
                    }
                }
            }
            */

            return Ok(dto);

        }

        [HttpGet("[action]")]
        public IActionResult KillContainer()  {
            
            Console.WriteLine(" Kill Trial received ... PRE container stop async task");
            // Task.Run() i async so the method wil return before it finishes and prevent the error where Trial Stop message is not
            // published
            Task.Run(() =>
            {

                // Bring down Minecraft Server

                // STOP Current Minecraft server instance
                string stopCommand = "docker kill minecraft-server0";
                Console.WriteLine("Executing Bash Command : " + stopCommand);
                //BashHelper.Bash(stopCommand);

                // UPDATE SERVER.PROPERTIES
                string server_properties_text = System.IO.File.ReadAllText("minecraft_data/server.properties");
                Console.Write(server_properties_text);
                string[] lines = server_properties_text.Split('\n');
                string? current_map = null;

                foreach (string line in lines)
                {

                    // if the string starts with levelname, return the altered string with the last "newline" character removed
                    if (line.StartsWith("level-name="))
                    {
                        current_map = line;
                        break;
                    }
                }

                string deleteMapCommand = "rm -r minecraft_data/" + current_map!.Substring(11) + "/";
                //BashHelper.Bash(deleteMapCommand);            

                string copyMapCommand = "(cd CLEAN_MAPS && find " + current_map.Substring(11) + "/ -type f -exec install -Dm 777 \"{}\" \"../minecraft_data/{}\" \\;)";
                //string copyMapCommand = "cp -r CLEAN_MAPS/" + current_map.Substring(11) + "/ minecraft_data/" + current_map.Substring(11) + "/";

                //BashHelper.Bash(copyMapCommand);

                BashHelper.Bash(stopCommand + " && sleep 3 &&" + deleteMapCommand + " && sleep 1 && " + copyMapCommand);

                _client.publish("", "status/minecraft/stopped");
            });

            Console.WriteLine(" Kill Trial received ... POST container async task");

            JsonObject responseObject = new JsonObject()
            {
                ["container_stopped"] = true
            };

            return Ok(responseObject);
        }
    }
   
}