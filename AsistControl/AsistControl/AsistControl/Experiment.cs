﻿using AsistControl.DTO;
using AsistControl.DTO.DBModels;
using AsistControl.Interfaces;
using System.Text.Json.Nodes;



namespace AsistControl
{
    public class Experiment:IExperiment
    {
        public enum TrialEndCondition { 

            NOT_SET,
            ERROR_MINECRAFT_CRASH,
            ERROR_MINECRAFT_DISCONNECT,
            ERROR_CLIENTMAP_DISCONNECT,
            ERROR_PLAYER_LEFT,
            ERROR_PROCEED_TIMEOUT,
            OK_TIMER_END,
            OK_ALL_BOMBS_REMOVED,
            OK_ALL_PLAYERS_FROZEN
        
        }
        
        public JsonArray playerArrayToStartTrial { get; set; } = new JsonArray(); 
        public string experiment_id{ get;set; } = System.Guid.Empty.ToString();

        public string trial_id{ get;set; } =  System.Guid.Empty.ToString();

        public string mission_name{ get;set; } = "Not Set";

        public List<string> MapBlocksLayouts { get; set; } = new List<string>();

        public MissionDTO missionDTO{ get;set; } = new MissionDTO();

        public ExperimentDTO experimentDTO{get;set;} = new ExperimentDTO();
        
        public TrialDTO trialDTO{get;set;} = new TrialDTO();

        public TeamInfoDTO teamInfoDTO { get; set; } = new TeamInfoDTO();

        public TrialSummaryDB trialSummaryDB { get; set; } = new TrialSummaryDB();

        public TeamDB teamDB { get; set; } = new TeamDB();

        public WaitingRoomDTO waitingRoomDTO { get; set; } = new WaitingRoomDTO();
        
    }
}
