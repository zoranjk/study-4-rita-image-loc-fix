# AsistControl Instructions

  

## About
  

The AsistControl docker service is the main point of Experiment and Trial Control in the Single Instance Testbed Stack. It handles creating and starting an Experiment as well as creating, starting and stopping a Trial.
  

It also provides functionality for summarizing the events of the Trial in the Trial Database, launching and relaunching the Minecraft Server, and launching and relaunching testbed agents
  

## Building Docker container
  

The container can be built in Windows by running the **publish_build_docker.ps1** script. This script can be easily adapted to run on Linux and Mac.
  

## Configuration

Various configurations can be changed in the appsettings.*.json files. 
During live deployment, this file lives in **Local/AsistControl/appsettings.Production.json** .  

	{
		# Setting Log Levels in .Net Core 2.0 +
		"Logging": {

			"LogLevel": {

				"Default": "Debug",

				"System": "Debug",

				"Microsoft": "Debug"

			},

			"Console": {

				"LogLevel": {

					"Default": "Debug"

				}

			}

		},
		# Settings for the mqtt broker connection
		"Mqtt": {

			"host": "mosquitto",

			"port": 1883,

			"clientID": "AsistControl"

		},
		# Settings for connection to the metadata server to create experiments
		"MetadataServer": {

			"url": "http://metadata-app:8080/experiments",

			"off_for_core_testing": "false"

		},
		# Information about the system
		"About": {

			"system_name": "ASIST Testbed",

			"system_description": "The ASIST testbed provides an environment for research into human/AI teams.",

			"darpa_acknolwedgement": "The ASIST project is sponsored by DARPA",

			"system_version": "V3.2.0-01_11_2023-1242-gd8b561e4e",

			"aptima_legal_notice": "Copyright 2023, Aptima Inc.",

			"other_notices": [

			"Minecraft",

			"Malmo",

			"Elastic",

			"PMEngine",

			"MQTT"

			]

		},
		# Mod information
		"Mod": {

			"name": "asist"

		},
		# Minecraft Map Name
		"Minecraft": {

			"MapName": "Dragon_1.1_3D"

		},
		# Not used ... callsigns are defined the in the ClientMap
		"CallSignList": [

			"Alpha",

			"Bravo",

			"Delta"

		],
		# Study 4 Map Variations, can be found in Local/data/mods
		"MapBlockLayouts": [

			"MapBlocks_Orion_1.9.csv",

			"MapBlocks_Orion_1.10b.csv",

			"MapBlocks_Orion_1.11b.csv",

			"MapBlocks_Orion_1.12b.csv",

			"MapBlocks_Orion_1.13.csv",

			"MapBlocks_Orion_1.14.csv",

			"MapBlocks_Orion_1.15b.csv",

			"MapBlocks_Orion_1.16b.csv",

			"MapBlocks_Orion_1.17b.csv",

			"MapBlocks_Orion_1.18b.csv"

		],
		# Not used in Study 4, defined the Mission type in Study 3
		"MissionList": [

			{

			"MissionName": "Hands-on Training",

			"MapName": "Saturn_2.9_3D_Training",

			"MapBlockFilename": "MapBlocks_Training_Saturn_2.4_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Hands-on Training DEV",

			"MapName": "Saturn_2.9_3D_Training_Dev",

			"MapBlockFilename": "MapBlocks_Training_Saturn_2.4_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_A",

			"MapName": "Saturn_2.6_3D",

			"MapBlockFilename": "MapBlocks_SaturnA_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_A_Blackout",

			"MapName": "Saturn_2.6_3D",

			"MapBlockFilename": "MapBlocks_SaturnA_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_A_Rubble",

			"MapName": "Saturn_2.6_3D",

			"MapBlockFilename": "MapBlocks_SaturnA_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_B",

			"MapName": "Saturn_2.6_3D",

			"MapBlockFilename": "MapBlocks_SaturnB_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_B_Blackout",

			"MapName": "Saturn_2.6_3D",

			"MapBlockFilename": "MapBlocks_SaturnB_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_B_Rubble",

			"MapName": "Saturn_2.6_3D",

			"MapBlockFilename": "MapBlocks_SaturnB_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_C",

			"MapName": "Saturn_2.6_3D",

			"MapBlockFilename": "MapBlocks_SaturnC_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_D",

			"MapName": "Saturn_2.6_3D",

			"MapBlockFilename": "MapBlocks_SaturnD_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_A-Dev",

			"MapName": "Saturn_2.6DEV_3D",

			"MapBlockFilename": "MapBlocks_SaturnA_2.1_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_B-Dev",

			"MapName": "Saturn_2.6DEV_3D",

			"MapBlockFilename": "MapBlocks_SaturnB_2.0_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_C-Dev",

			"MapName": "Saturn_2.6DEV_3D",

			"MapBlockFilename": "MapBlocks_SaturnC_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Saturn_D-Dev",

			"MapName": "Saturn_2.6DEV_3D",

			"MapBlockFilename": "MapBlocks_SaturnD_2.3_xyz.csv",

			"MapInfoFilename": null

			},

			{

			"MissionName": "Competency_Prescreen",

			"MapName": "Saturn_Session1_CompetencyPrescreen",

			"MapBlockFilename": null,

			"MapInfoFilename": null

			}

		],
		# Repository credentials for Docker Image registry
		"RegistryCredentials": {

			"username": "",

			"password": ""

		},
		# Not used in Study 4, was used to start and stop ASI agents in Study3
		"AgentShellScripts": {

			"ASI_CMU_TA1_ATLAS": {

			"up": "cd ./Agents/ASI_CMU_TA1_ATLAS && echo 'Bringing up the CMU TA1 Atlas Agent' && docker compose --env-file settings.env up -d",

			"down": "cd ./Agents/ASI_CMU_TA1_ATLAS && echo 'Bringing down the CMU TA1 Atlas Agent' && docker compose --env-file settings.env down"

			},

			"atomic_agent": {

			"up": " cd ./Agents/atomic_agent && echo 'Bringing up the Atomic Agent' && docker compose --env-file settings.env up -d",

			"down": "cd ./Agents/atomic_agent && echo 'Bringing down the Atomic Agent' && docker compose --env-file settings.env down"

			},

			"ASI_CRA_TA1_psicoach": {

			"up": "cd ./Agents/ASI_CRA_TA1_psicoach && echo 'Bringing up the CRA PSI-Coach Agent' && docker compose --env-file settings.env up -d",

			"down": "cd ./Agents/ASI_CRA_TA1_psicoach && echo 'Bringing down the CRA PSI-Coach Agent' && docker compose --env-file settings.env down"

			},

			"ASI_DOLL_TA1_RITA": {

			"up": "cd ./Agents/Rita_Agent && chmod 777 ./rita_up.sh && ./rita_up.sh",

			"down": "cd ./Agents/Rita_Agent && echo 'Bringing down the Rita Agent' && docker compose --env-file settings.env down"

			},

			"sift_asistant": {

			"up": "cd ./Agents/SIFT_Asistant_Agent && echo 'Bringing up the SIFT Asistant Agent' && docker compose --env-file settings.env up -d",

			"down": "cd ./Agents/SIFT_Asistant_Agent && echo 'Bringing down the SIFT Asistant Agent' && docker compose --env-file settings.env down"

			}

		}

	}