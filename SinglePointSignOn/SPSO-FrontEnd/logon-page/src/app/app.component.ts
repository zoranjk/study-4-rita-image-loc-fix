import { HttpClient} from '@angular/common/http';
import { Component } from '@angular/core';
import { ActivationEnd, Router } from '@angular/router';
import { GeoLocation } from './models/geolocation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'logon-page';
  noheader = false;
  country = "NOT_SET";

  constructor(
    private router: Router,
    public http:HttpClient
  ) { 

    this.http.get("https://jsonip.com/").subscribe(
      {   next:(res:GeoLocation)=>{
            console.log(res);
            this.country = res.country;
          },
          error:(err)=>{console.log(err)},
          complete:()=>{
            if(this.country.localeCompare("NOT_SET")!=0){
              if(this.country.localeCompare("US")!=0){
                alert(" SocialAI.Games is currently only available to participants located in the United States. We apologize for the inconvenience. You will now be redirected to https://artificialsocialintelligence.org.")
                window.location.href = 'https://artificialsocialintelligence.org';
              }
            }
          }
    });
  

    /*
    this.http.get("https://geolocation-db.com/json").subscribe( {
      next:(next)=>{
        console.log(next)
      },
      error:(err)=>{
        console.log(err)
      },
      complete: ()=>{}
    });
    */

    //https://geolocation-db.com/jsonp/108.49.220.102
    

  }

  ngOnInit() {
    this.router.events.subscribe(event => {
      if(event instanceof ActivationEnd) {
        const code = event.snapshot.queryParams['noheader'];
        if (code) {
          this.noheader = true;
        }
      }
    })
  }
}
