import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router'
import { MatDialog } from '@angular/material/dialog'
import { ConsentFormDialogComponent } from 'src/app/consent-form-dialog/consent-form-dialog.component';
import { Observable, Subscription } from 'rxjs';
import { Trial } from 'src/app/models/trial';
import { HttpHeaders } from '@angular/common/http';
import { AccountRecoveryComponent } from '../account-recovery/account-recovery.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, OnDestroy {

  username;
  password;
  hide = true;

  loggedIn: boolean = false;
  currentUser: User;
  showErrorMsg: boolean = false;
  trials: Trial[] = [];
  teamSubscription: Subscription;
  interval: any;

  constructor(
    private authService: AuthService, 
    private titleService: Title, 
    private router: Router, 
    private route: ActivatedRoute,
    public dialog: MatDialog) { 
    titleService.setTitle("Login");
  }

  ngOnDestroy(): void {
    this.teamSubscription.unsubscribe();
  }

  ngOnInit(): void {
    if(this.authService.currentUser != null) {
      this.loggedIn = true;
      this.currentUser = this.authService.currentUser;
    }

    this.loadTeamData()
    this.interval = setInterval(() => { 
      this.loadTeamData()
    }, 180000);


  }

  loadTeamData() {
    this.teamSubscription = this.authService.getTrialData().subscribe(trials => {
      this.trials = [...trials]
    });
  }

  login() {
    const credentials: User = {
      userId: this.username,
      password: this.password
    };

    this.getAuth(credentials);
  }

  getAuth(credentials: User) {
    this.authService.getAuth(credentials).subscribe((response: {authentication, user:User, token:string}) => {
      // this.loggedIn = value;
      console.log(response)
      this.showErrorMsg = !response.authentication;
      if (response.authentication) {
        // GET RETURNED TOKEN
        this.authService.bearerToken = response.token;
        this.authService.headers = new HttpHeaders(
          { 'Content-Type': 'application/json',
           'Authorization': 'Bearer '+ response.token
          });  
        console.log( this.authService.headers );     
        this.authService.getCurrentUser(response?.user?.participantId);
        
        if (response.user.isAdmin) {         
          this.router.navigate(['../Administration'], { relativeTo: this.route });            
        } else {
          this.redirect(response.user, '../Download');
        }
      }
    });
  }

  redirect(user: User, dest: string) {
    this.authService.getCurrentUser(user.participantId)
    let dialogRef = this.dialog.open(ConsentFormDialogComponent, { disableClose: true });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.router.navigate([dest], { relativeTo: this.route }); 
        // SEND CONSENT FORM TIMESTAMP
        this.authService.updateConsentTimestamp(user.participantId); 
      } else {
        location.reload();
      }
    });
  }

  accountRecovery(){

    let dialogRef = this.dialog.open(AccountRecoveryComponent, { disableClose: false });
    

  }
}
