import { Component, OnInit } from '@angular/core';
import { NewsService } from './news.service';
import {News} from "./news";

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  newsItems: News[];

  constructor(
    private newsService: NewsService,
  ) { }

  ngOnInit(): void {
    this.newsService.readNews().subscribe(newsItems => {
      this.newsItems = newsItems;
      }
    )
  }

}
