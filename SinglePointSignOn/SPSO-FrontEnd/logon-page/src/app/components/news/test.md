### Hybrid Game Night on Thursday August 31!

Join us at hybrid game night at ASU Polytechnic Campus Library on 8/31, from 5 pm to 8 pm PT. Food provided!  Online players, including those not at ASU, are also welcome to play the game with on-site players.

### Updated Rewards!

Starting 8/28, members of the top 3 performing teams for that week will each receive a $25 gift card (limit $75 per unique team). An additional team will also be selected from the top 50% of scoring teams for that week (excluding the top 3 performing teams previously mentioned) and each member of that team will receive $25.

### AI Advisors!

Two new ASI advisors will be launched on August 29th. Keep an eye out for their advice while you play! They will be regularly updated so come back and check out how well the new ASI advisors are doing.

As a reminder, the server is available 24/7, but the game requires 3 players to form a team. We encourage participation from 5 pm to 8 pm PT (8 pm - 11 pm ET) each day to have more players online at the same time to form teams. Or, you can bring two friends to play with you at any time you choose.

For technical issues, you can email [support@socialai.games](mailto:support@socialai.games?subject=SocialAI.games%20Support.) or request a Zoom appointment to get help at the link below:

[Click for Calendly Link](https://calendly.com/asist-asu/asist-minecraft-study-zoom-support?month=2023-08)



Join our Kickoff Day Competition on Aug 18 from 9am-9pm PT / 12pm-12am ET!

Be among the first 50 players to finish 2 full games and earn a $5 Amazon gift card!

Compete, collaborate, and win big!

The top team wins Amazon gift cards worth $300 ($100 for each team member).

Be part of groundbreaking research while having a blast. Don’t miss it!

During the Kickoff Day, real-time technical support will be provided at the following Zoom Link:

[Click for Zoom Link]("https://asu.zoom.us/j/82510834899)
