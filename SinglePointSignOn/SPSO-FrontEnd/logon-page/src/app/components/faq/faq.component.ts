import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router'; 

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  noheader = false;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.route.queryParams
      .subscribe(params => {
        this.noheader = params['noheader'];
      }
    );
  }

  createSpsoUrl(path: string) {
    return location.toString().substring(0,location.toString().indexOf("FAQ")) + path
  }



}
