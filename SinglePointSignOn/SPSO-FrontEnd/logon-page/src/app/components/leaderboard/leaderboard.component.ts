import { Component, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Subscription } from 'rxjs';
import { Trial } from 'src/app/models/trial';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})

export class LeaderboardComponent implements OnInit , OnDestroy{

  trials: Trial[] = [];
  teamSubscription: Subscription;
  interval: any;

  constructor(titleService: Title, private authService: AuthService) { 
    titleService.setTitle("Leaderboard");
  }
  
  ngOnDestroy(): void {
    this.teamSubscription.unsubscribe();
  }

  ngOnInit(): void {
    this.loadTeamData()
    this.interval = setInterval(() => { 
      this.loadTeamData()
    }, 180000);
  }

  loadTeamData() {
    this.teamSubscription = this.authService.getTrialData().subscribe(trials => {
      this.trials = [...trials]
    });
  }


}
