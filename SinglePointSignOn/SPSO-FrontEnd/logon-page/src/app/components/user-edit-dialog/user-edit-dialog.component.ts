import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';
import { User } from 'src/app/models/user';
import { AuthService } from 'src/app/services/auth.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { Inject } from '@angular/core';
import { InfoDialogComponent } from '../info-dialog/info-dialog.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-user-edit-dialog',
  templateUrl: './user-edit-dialog.component.html',
  styleUrls: ['./user-edit-dialog.component.css']
})
export class UserEditDialogComponent implements OnInit {

  @Inject(MAT_DIALOG_DATA) public user: User

  isLoading = false
  adminAuth = false
  currentUser: User
  editForm

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private _snackBar: MatSnackBar,
    private dialogRef: MatDialogRef<UserEditDialogComponent>) { }

  ngOnInit(): void {
    this.currentUser = this.authService.currentUser
    if (this.currentUser.isAdmin) {
      this.editForm = this.fb.group({
        userId: [this.user.userId, this.validUsername()],
        participantId: [this.user.participantId],
        password: [this.user.password],
        email: [this.user.email, Validators.email]
      })
      this.adminAuth = true
    }
  }

  showInfo() {
    this.dialog.open(InfoDialogComponent);

  }

  validUsername(): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
      let re = /^\b\w{3,16}\b$/;
      let namestr = this.editForm?.get('userId')?.value?.toString();
      let valid = re.exec(namestr!) !== null || namestr! == ''
      return valid ? null : {invalid: true}
    }

  }

  getErrorMessage() {
    return this.editForm.get('email')?.hasError('email') ? 'Not a valid email' : '';
  }

  update(updated) {
    console.log("Updated User Object")
    console.log(updated);
    let newUser: User = this.user
    newUser.userId = updated.userId != '' ? updated.userId : this.user.userId
    //newUser.participantId = updated.participantId != '' ? updated.participantId : this.user.participantId
    newUser.password = updated.password != '' ? updated.password : this.user.password
    newUser.email = updated.email != '' ? updated.email : this.user.email
    
    this.isLoading = true;
    this.authService.updateUserProfile(updated.participantId, newUser, this.currentUser.isAdmin).subscribe({
      error: (e) => {
        console.error(e),
        this._snackBar.open('Error updating user \"' + this.user.participantId + '\"', 'Ok', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this.isLoading = false;
      },
      complete: () => {
        console.log('Complete'),
        this._snackBar.open('User \"' + this.user.participantId +'\" updated!', 'Ok', {
          horizontalPosition: 'center',
          verticalPosition: 'top',
        });
        this.dialogRef.close();
      }
    })
  }
}
