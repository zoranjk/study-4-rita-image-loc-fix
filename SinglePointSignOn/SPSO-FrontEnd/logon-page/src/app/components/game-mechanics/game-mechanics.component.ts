import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from 'src/app/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-game-mechanics',
  templateUrl: './game-mechanics.component.html',
  styleUrls: ['./game-mechanics.component.scss']
})
export class GameMechanicsComponent implements OnInit {


  constructor(
    private authService: AuthService, 
    private titleService: Title, 
    private router: Router, 
    private route: ActivatedRoute,
    public dialog: MatDialog) { 
    titleService.setTitle("Game Mechanics");
  }

  ngOnInit(): void {
  }


}
