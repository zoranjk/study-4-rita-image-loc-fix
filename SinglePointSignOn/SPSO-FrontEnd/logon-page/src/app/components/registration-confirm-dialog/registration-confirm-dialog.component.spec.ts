import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistrationConfirmDialogComponent } from './registration-confirm-dialog.component';

describe('RegistrationConfirmDialogComponent', () => {
  let component: RegistrationConfirmDialogComponent;
  let fixture: ComponentFixture<RegistrationConfirmDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegistrationConfirmDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RegistrationConfirmDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
