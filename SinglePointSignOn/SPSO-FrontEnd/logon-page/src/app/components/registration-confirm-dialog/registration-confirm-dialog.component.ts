import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-registration-confirm-dialog',
  templateUrl: './registration-confirm-dialog.component.html',
  styleUrls: ['./registration-confirm-dialog.component.css']
})
export class RegistrationConfirmDialogComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {username: string}) { }

  ngOnInit(): void {
  }

}
