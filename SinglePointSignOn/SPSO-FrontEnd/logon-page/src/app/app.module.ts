import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule, NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { MatSnackBarModule} from '@angular/material/snack-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatTabsModule} from '@angular/material/tabs';

import { FlexLayoutModule } from '@angular/flex-layout';
import { RegistrationHomeComponent } from './components/registration-home/registration-home.component';
import { LoginComponent } from './components/login/login.component';
import { InfoDialogComponent } from './components/info-dialog/info-dialog.component';
import { MatDialogModule} from '@angular/material/dialog';
import { UserListDialogComponent } from './components/user-list-dialog/user-list-dialog.component';
import { MatTableModule} from '@angular/material/table';
import { DownloadPageComponent } from './components/download-page/download-page.component';
import { GameMechanicsComponent } from './components/game-mechanics/game-mechanics.component';
import { LeaderboardComponent } from './components/leaderboard/leaderboard.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { ConsentFormDialogComponent } from './consent-form-dialog/consent-form-dialog.component';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import { AdminPageComponent } from './components/admin-page/admin-page.component';
import { UserEditDialogComponent } from './components/user-edit-dialog/user-edit-dialog.component';
import { RegistrationConfirmDialogComponent } from './components/registration-confirm-dialog/registration-confirm-dialog.component';
import { FaqComponent } from './components/faq/faq.component';
import { AccountRecoveryComponent } from './components/account-recovery/account-recovery.component';
import { NewsComponent } from './components/news/news.component';

import { MarkdownModule } from 'ngx-markdown';


@NgModule({
  declarations: [
    AppComponent,
    RegistrationHomeComponent,
    LoginComponent,
    InfoDialogComponent,
    UserListDialogComponent,
    DownloadPageComponent,
    GameMechanicsComponent,
    LeaderboardComponent,
    ConsentFormDialogComponent,
    AdminPageComponent,
    UserEditDialogComponent,
    RegistrationConfirmDialogComponent,
    FaqComponent,
    AccountRecoveryComponent,
    NewsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule,
    NoopAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatInputModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatDialogModule,
    MatTableModule,
    MatCheckboxModule,
    MatProgressBarModule,
    BrowserAnimationsModule,
    MarkdownModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
