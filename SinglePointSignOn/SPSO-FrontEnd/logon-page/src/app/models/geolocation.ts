
export interface GeoLocation
{
    "ip": string, 
    "country": string, 
    "geo-ip": string, 
    "API Help": string
}