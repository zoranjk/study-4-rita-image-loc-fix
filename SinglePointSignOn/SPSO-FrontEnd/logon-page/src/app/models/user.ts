export interface User {
    id?: string,
    userId?: string,
    password?: string,
    email?: string,
    isAdmin?: boolean,
    participantId? : string,
    surveys?: string,
    successfulBombDisposals?: number,
    unsuccessfulBombDisposals?: number
    teams?: Team[],
}

export interface UserInfo {
    id: string,
    name: string,
    email: string,
    participant_id : string,
    surveys: string,
    successfulBombDisposals: number,
    unsuccessfulBombDisposals: number
    teams: Team[],
}

export interface Team {
    teamId: string,
    teamPlays: string[],
    teamScore: number,
    members: string[]
}