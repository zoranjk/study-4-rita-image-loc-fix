import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsentFormDialogComponent } from './consent-form-dialog.component';

describe('ConsentFormDialogComponent', () => {
  let component: ConsentFormDialogComponent;
  let fixture: ComponentFixture<ConsentFormDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsentFormDialogComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConsentFormDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
