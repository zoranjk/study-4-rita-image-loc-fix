export const environment = {
  production: true,
  ssoServicePath: "https://localhost:9000/SSOService/",
  waitingRoomPath: "https://localhost:9000/"
};
