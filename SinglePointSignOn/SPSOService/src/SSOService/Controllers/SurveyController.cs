﻿using ConsoleApp.PostgreSQL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SSOService.Database.DBModels;
using SSOService.DTO;
using SSOService.Helpers;
using System.Text.Json;

namespace SSOService.Controllers
{
    [Produces("Application/json")]
    [Consumes("Application/json")]
    [Route("survey")]
    [ApiController]
    [Authorize]
    public class SurveyController : Controller
    {
        private readonly SSODBContext _db;
        private readonly ILogger _logger;

        public SurveyController(IConfiguration configuration, SSODBContext context, ILogger<UserController> logger)
        {
            _db = context;          
            _logger = logger;
        }

        [HttpPut("UpdatePreTrialSurveyData/{id}")]
        public async Task<ActionResult<User>> UpdatePreTrialSurveyData(string id, SurveyDTO surveyDTO)
        {
            try
            {
                string pid = id;
            
                Console.Write("UpdatePreTrialSurveyData for PID : " + pid);
                Console.Write("SurveyDTO : " + JsonSerializer.Serialize(surveyDTO));

                
                User? user = await _db.Users!.FindAsync(pid);
                if (user is null) return NotFound();               

                surveyDTO.completedTimestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
                    
                if (user.PreTrialSurveys != null) { 
                    Console.Write("PreTrialSurveys Key Not Null");
                    user.PreTrialSurveys.participant_id = pid;
                    List<SurveyDTO> surveys = user.PreTrialSurveys.surveys;
                    if (surveys != null)
                    {
                        surveys.Add(surveyDTO);
                    }
                    else
                    {
                        surveys = new List<SurveyDTO> { surveyDTO };
                    }
                    user.PreTrialSurveys.surveys = surveys;                    

                }
                else  {
                    Console.Write("PreTrialSurveys Key Null");
                    user.PreTrialSurveys = new SurveysDTO();
                    user.PreTrialSurveys.participant_id = pid;
                    user.PreTrialSurveys.surveys.Add(surveyDTO);
                   
                }               

                _db.Entry(user).Property(b => b.PreTrialSurveys).IsModified = true;
                _db.SaveChanges();
                return Ok(user);
            } 
            catch (Exception e)
            {
                return BadRequest(e);
            }

        }

        [HttpPut("UpdatePostTrialSurveyData/{id}")]
        public async Task<ActionResult<User>> UpdatePostTrialSurveyData(string id, SurveyDTO surveyDTO)
        {
            string pid = id;
            Console.Write("UpdatePostTrialSurveyData for PID : " + pid);
            if (pid != null)
            {
                User? user = await _db!.Users!.FindAsync(pid);
                if (user is null) return NotFound();
                try
                {                  
                    surveyDTO.completedTimestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
                    if (user.PostTrialSurveys != null)
                    {
                        user.PostTrialSurveys.participant_id = pid;
                        List<SurveyDTO> surveys = user.PostTrialSurveys.surveys;
                        if (surveys != null)
                        {
                            surveys.Add(surveyDTO);
                        }
                        else
                        {
                            surveys = new List<SurveyDTO> { surveyDTO };
                        }
                        user.PostTrialSurveys.surveys = surveys;
                    }
                    else
                    {
                        user.PostTrialSurveys = new SurveysDTO();
                        user.PostTrialSurveys.participant_id = pid;
                        user.PostTrialSurveys.surveys.Add(surveyDTO);
                    }
                    _db.Entry(user).Property(b => b.PostTrialSurveys).IsModified = true;
                    _db.SaveChanges();
                }
                catch (Exception e)
                {
                    return BadRequest(e);
                }
            }
            return Ok();
        }       
    }
}
