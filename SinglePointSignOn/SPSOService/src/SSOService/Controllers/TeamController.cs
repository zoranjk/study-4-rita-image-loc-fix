﻿using ConsoleApp.PostgreSQL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SSOService.Database.DBModels;
using SSOService.DTO;
using System.Text.Json.Nodes;

namespace SSOService.Controllers
{

    [Produces("Application/json")]
    [Consumes("Application/json")]
    [Route("team")]
    [ApiController]
    [Authorize]
    public class TeamController : Controller
    {
        private readonly SSODBContext _db;
        public TeamController(SSODBContext context)
        {
            _db = context;
        }

        [Route("teams")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Team>>> GetTeams()
        {
            return await _db.Teams!.AsNoTracking().ToListAsync();

        }

        [Route("topTeams")]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Team>>> GetTopTeams()
        {
            return await _db.Teams!.AsNoTracking().Where(s => s.TeamScore != 0).OrderByDescending(s => s.TeamScore).Take(10).ToListAsync();

        }

        [HttpGet("GetTeamById/{id}")]
        public async Task<ActionResult<Team>> GetTeamById(string id)
        {
            try {

                Team? team = await _db.Teams!.AsNoTracking().FirstOrDefaultAsync(i => i.TeamId == id);

                if (team == null)
                {
                    JsonObject error = new JsonObject()
                    {
                        ["not_found"] = "TeamId : " + id + " Not Found."
                    };
                    return NotFound(error);

                }
                return Ok(team);

            }
            catch (Exception ex) { return BadRequest(ex.Message); }
           
        }

        [HttpGet("NeverProceeded/{id}")]
        public async Task<ActionResult<string>> NeverProceeded(string id)
        {
            try
            {
                string timeStamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
                Team? team = await _db.Teams!.FirstOrDefaultAsync(i => i.TeamId == id);

                if (team == null)
                {
                    Team newTeam = new Team();
                    newTeam.TeamId = id;
                    newTeam.Members = id.Split("_").ToList();
                    newTeam.TeamScore = 0;
                    newTeam.NeverProceeded.Add(timeStamp);
                    _db.Teams.Add(newTeam);

                    team = newTeam;

                }
                else {
                    
                    team.NeverProceeded.Add(timeStamp);

                    
                }

                await _db.SaveChangesAsync();

                JsonObject success = new JsonObject()
                {
                    [team.TeamId] = timeStamp
                };

                return Ok(success);

            }
            catch (Exception ex) { 
                return BadRequest(ex.Message); 
            }

        }

        [HttpPut("updateStatsByTeamId/{id}")]
        public async Task<ActionResult<Team>> UpdateStatsByTeamId(string id, Team teamDB)
        {
            try {
              
                Team? existingTeam = await _db.Teams!.FindAsync(id);
                string newTrialId = teamDB.TeamPlays.First();
                JsonObject? success = null;
                if (existingTeam == null)
                {
                    Team newTeam = new Team();
                    newTeam.TeamId = teamDB.TeamId;
                    newTeam.Members = teamDB.Members;
                    newTeam.TeamScore = teamDB.TeamScore;
                    // we only send one
                    newTeam.TeamPlays.Add(newTrialId);
                    _db.Teams.Add(newTeam);
                    success = new JsonObject()
                    {
                        ["success"] = "TeamId : " + id + " Record Created."
                    };
                }
                else
                {
                    if(!existingTeam.TeamPlays.Contains(newTrialId)) {
                        existingTeam.TeamPlays.Add(newTrialId);
                    }                    
                    existingTeam.TeamScore += teamDB.TeamScore;
                    
                    success = new JsonObject()
                    {
                        ["success"] = "TeamId : " + id + " Record Updated."
                    };
                }

                await _db.SaveChangesAsync();                

                return Ok(success);

            }
            catch(Exception ex) { 
                
                return BadRequest($"{ex.Message}"); 
            
            }            
        }

        [HttpGet("DeleteTeamById/{id}")]
        public async Task<ActionResult<Team>> DeleteTeamById(string id)
        {
            try {

                Team? existingTeam = await _db.Teams!.FindAsync(id);
                if (existingTeam != null)
                {
                    _db.Teams.Remove(existingTeam);
                    await _db.SaveChangesAsync();
                    return Ok(existingTeam);
                }

                JsonObject error = new JsonObject()
                {
                    ["not_found"] = "TeamId : " + id + " Not Found."
                };
                return NotFound(error);             


            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            };
            
        }

    }
    
}
