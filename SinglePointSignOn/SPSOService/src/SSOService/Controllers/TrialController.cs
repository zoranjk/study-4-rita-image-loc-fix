﻿using ConsoleApp.PostgreSQL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SSOService.Database.DBModels;
using System.Security.Cryptography;
using System.Text.Json;
using System.Text.Json.Nodes;
using System.Xml.Linq;


// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SSOService.Controllers
{

    [Produces("Application/json")]
    [Consumes("Application/json")]
    [Route("trial")]
    [ApiController]
    [Authorize]
    public class TrialController : ControllerBase
    {
        private readonly SSODBContext _db;
        private readonly IConfiguration _config;
        private readonly ILogger _logger;

        public TrialController(IConfiguration configuration, SSODBContext context, ILogger<UserController> logger)
        {
            _db = context;
            _logger = logger;
            _config = configuration;
        }
        

        [HttpGet("GetAllTrialRecords")]
        public async Task<ActionResult<Trial>> GetAllTrialRecords()
        {
            try
            {
                var trials = _db.Trials;

                if (trials != null)
                {
                    return Ok(trials);
                }

                JsonObject error = new JsonObject()
                {
                    ["not_found"] = "There was an error retreiving all the Trial Records."
                };
                return NotFound(error);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [AllowAnonymous]
        [HttpGet("GetTopTrialRecords")]
        public async Task<ActionResult<IEnumerable<Trial>>> GetTopTrialRecords()
        {
            try
            {
                var trials = _db.Trials;
                var users = _db.Users;

                List<Trial> Trials = new List<Trial>();
                if (trials != null)
                {

                    //var topTrials = await trials.Where(s => s.TeamScore != 0).OrderByDescending(s => s.TeamScore).Take(500).ToListAsync();

                    var topTrials = await trials
                        .Where((s) =>

                            (new List<String>(){
                                " OK_TIMER_END",
                                "OK_ALL_BOMBS_REMOVED",
                                "OK_ALL_PLAYERS_FROZEN"
                            }
                            .Contains(s.TrialEndCondition))

                            ||

                            (new List<String>(){
                                "MISSION_STOP_TIMER_END",
                                "MISSION_STOP_ALL_PLAYERS_FROZEN",
                                "MISSION_STOP_ALL_BOMBS_REMOVED"
                            }
                            .Contains(s.MissionEndCondition))

                            ||

                            (new List<String>() { "7", "8", "9" }
                            .Contains(
                                s.LastActiveMissionTime.Replace(" ", string.Empty).Substring(0,1)
                            ))

                            ||

                            (new List<String>() { "10", "11", "12", "13" }
                            .Contains(
                                s.LastActiveMissionTime.Replace(" ", string.Empty).Substring(0, 2)
                            ))



                        )
                        .OrderByDescending(s => s.TeamScore)
                        .Take(1000)
                        .ToListAsync();

                    foreach (var trial in topTrials)
                    {
                        List<string> ObscuredMembers = new List<string>();

                        foreach (var member in trial.Members)
                        {
                            if (users != null)
                            {
                                if (await users.Where(u => u.ParticipantId == member).FirstOrDefaultAsync() is User mu)
                                {
                                    ObscuredMembers.Add(mu?.UserId[..3] + "***" + mu?.UserId[^3..]);
                                }

                            }
                        }
                        trial.Members = ObscuredMembers;
                        Trials.Add(trial);

                    }
                    return Trials;
                }

                JsonObject error = new JsonObject()
                {
                    ["not_found"] = "There was an error retreiving all the Trial Records."
                };
                return NotFound(error);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet("GetTrialByTrialId/{id}")]
        public async Task<ActionResult<Trial>> GetTrialByTrialId(string id)
        {
            try
            {
                Trial? data = await _db.Trials!.FindAsync(id);

                if (data != null)
                {
                    return Ok(data);
                }

                JsonObject error = new JsonObject()
                {
                    ["not_found"] = "TrialId : " + id + " Not Found."
                };
                return NotFound(error);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut("CreateTrialRecord/{id}")]
        public async Task<ActionResult<Trial>> CreateTrialRecord(string id, Trial data)
        {
            Console.Write("Creating Record for TrialData : " + id);

            try
            {
                if (data != null)
                {
                    Trial trial = await _db.Trials.FindAsync(id);
                    if (trial == null)
                    {
                        await _db.Trials!.AddAsync(data);
                        trial = await _db.Trials.FindAsync(id);
                    }
                    else
                    {
                        trial = data;
                    }

                    _db.Entry(data).Property(b => b!.BombSummaryPlayer).IsModified = true;
                    _db.Entry(data).Property(b => b!.BombsExploded).IsModified = true;
                    _db.Entry(data).Property(b => b!.DamageTaken).IsModified = true;
                    _db.Entry(data).Property(b => b!.FlagsPlaced).IsModified = true;
                    _db.Entry(data).Property(b => b!.BudgetExpended).IsModified = true;
                    _db.Entry(data).Property(b => b!.TeammatesRescued).IsModified = true;
                    _db.Entry(data).Property(b => b!.TimesFrozen).IsModified = true;
                    _db.Entry(data).Property(b => b!.CommBeaconsPlaced).IsModified = true;
                    _db.Entry(data).Property(b => b!.BombBeaconsPlaced).IsModified = true;
                    _db.Entry(data).Property(b => b!.FiresExtinguished).IsModified = true;   

                    await _db.SaveChangesAsync();




                }
              
                
                return Ok(data);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("UpdateTrialRecord/{id}")]
        public async Task<ActionResult<Trial>> UpdateTrialRecord(string id, Trial incoming)
        {
            Console.WriteLine("Updating Record for TrialData : " + id);
            
            Console.WriteLine( JsonSerializer.Serialize(incoming) );

            //Trial incoming = JsonSerializer.Deserialize<Trial>(JsonSerializer.Serialize(data));
            Trial trial = null;
            try
            {
                if (incoming != null)
                {
                    trial = await _db.Trials.FindAsync(id);

                    Console.WriteLine(" Found matching Trial Record .");

                    if (trial == null)
                    {

                        Console.WriteLine(" Creating a new Trial Record .");
                        await _db.Trials!.AddAsync(incoming);
                        trial = await _db.Trials.FindAsync(id);

                        for (int i = 0; i < trial.Members.Count; i++)
                        {
                            User user = await _db.Users.FindAsync(trial.Members[i]);
                            if( user != null && (user!.TrialsList.Contains(id) == false) )
                            {
                                user.TrialsList.Add(id);
                                user.ParticipationCount = user.TrialsList.Count;
                            }
                           
                        }
                    }
                    else
                    {
                        Console.WriteLine(" Updating an existing Trial Record via Remove/Add .");
                        _db.Trials!.Remove(trial);
                        _db.Trials!.Add(incoming);
                        trial = await _db.Trials.FindAsync(id);

                    }

                    //_db.Entry(trial).Property(b => b!.BombsDefused).IsModified = true;
                    //_db.Entry(trial).Property(b => b!.BombsExploded).IsModified = true;
                    //_db.Entry(trial).Property(b => b!.DamageTaken).IsModified = true;
                    //_db.Entry(trial).Property(b => b!.BudgetExpended).IsModified = true;
                    //_db.Entry(trial).Property(b => b!.TeammatesRescued).IsModified = true;
                    //_db.Entry(trial).Property(b => b!.CommBeaconsPlaced).IsModified = true;

                    await _db.SaveChangesAsync();
                    
                }
                return Ok(trial);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}
