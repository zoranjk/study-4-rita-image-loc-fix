﻿using ConsoleApp.PostgreSQL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SSOService.Database.DBModels;
using System.Text.Json.Nodes;

namespace SSOService.Controllers
{
    [Produces("Application/json")]
    [Consumes("Application/json")]
    [Route("agent/storage")]
    [ApiController]
    [Authorize]
    public class AgentStorageController : Controller
    {
        private readonly SSODBContext _db;
        private readonly IConfiguration _config;
        private readonly ILogger _logger;

        public AgentStorageController(IConfiguration configuration, SSODBContext context, ILogger<UserController> logger) {
            _db = context;           
            _logger = logger;
            _config = configuration;
        }

        
        [HttpGet("GetAllAgentData/{id}")]
        public async Task<ActionResult<IEnumerable<AgentStorageData>>> GetAllAgentData(string id)
        {
            List<AgentStorageData> data = new List<AgentStorageData>();
            await _db.AgentStorage!.AsNoTracking().ForEachAsync((AgentStorageData d) =>
            {
                if(d.AgentId.CompareTo(id) == 0)
                {
                    data.Add(d);
                }
            });

            if (data.Count > 0)
            {
                return Ok(data);
            }

            JsonObject error = new JsonObject()
            {
                ["not_found"] = "There is no data present for the AgentId : " + id 
            };
            return NotFound(error);
        }

        [HttpGet("GetAllStorageIds/{id}")]
        public async Task<ActionResult<IEnumerable<string>>> GetAllStorageIds(string id)
        {
            List<string> data = new List<string>();
            await _db.AgentStorage!.AsNoTracking().ForEachAsync((AgentStorageData d) =>
            {
                if (d.AgentId.CompareTo(id) == 0)
                {
                    data.Add(d.StorageId);
                }
            });

            if (data.Count > 0)
            {
                return Ok(data);
            }

            JsonObject error = new JsonObject()
            {
                ["not_found"] = "There is no data present for the AgentId : " + id
            };

            return NotFound(error);
        }

        [HttpGet("GetDataByStorageId/{id}")]
        public async Task<ActionResult<AgentStorageData>> GetDataByStorageId(string id)
        {
            AgentStorageData data = null;
            await _db.AgentStorage!.AsNoTracking().ForEachAsync((AgentStorageData d) =>
            {
                if (d.StorageId.CompareTo(id) == 0)
                {
                    data = d;
                }
            });

            if (data != null) {
                return Ok(data);
            }
            
            JsonObject error = new JsonObject()
            {
                ["not_found"] = "StorageId : " + id + " Not Found."
            };
            return NotFound( error );
        }

        [HttpPut("StoreAgentData/{id}")]
        public async Task<ActionResult<AgentStorageData>> StoreAgentData(string id, AgentStorageData data)
        {
            Console.Write("StoreAgentData for AgentID : " + id);
            
            try
            {
                if (data != null)
            {
                    data.DateLastUpdated = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.ffff") + 'Z';
                
                    AgentStorageData? dbData = await _db.AgentStorage!.FindAsync(data.StorageId);
                
                    if (dbData != null)
                    {
                        Console.WriteLine(data.StorageId + " Found!");
                        Console.WriteLine("Rmoving it.");
                        _db.AgentStorage!.Remove(dbData);


                    }
                    else
                    {
                        Console.WriteLine(data.StorageId + " is new!");
                        Console.WriteLine("Creating a record.");
                         
                    }
                    _db.AgentStorage.Add(data);
                    //_db.Entry(dbData).Property(b => b!.Data).IsModified = true;
                    _db.SaveChanges();
                }              

                return Ok();
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet("DeleteAgentDataByStorageId/{id}")]
        public async Task<ActionResult<AgentStorageData>> DeleteAgentDataByStorageId(string id)
        {
            
            AgentStorageData? existingData = await _db.AgentStorage!.FindAsync(id);
           

            if (existingData != null)
            {
                _db.AgentStorage!.Remove(existingData);
                _db.SaveChanges();
                return Ok();
            }

            JsonObject error = new JsonObject()
            {
                ["not_found"] = "StorageId : " + id + " Not Found."
            };
            return NotFound(error);
            
        }

        [HttpGet("DeleteAllAgentData/{id}")]
        public async Task<ActionResult<AgentStorageData>> DeleteAllAgentData(string id)
        {

            List<AgentStorageData> dataList = new List<AgentStorageData>();
            await _db.AgentStorage!.AsNoTracking().ForEachAsync((AgentStorageData d) =>
            {
                if (d.AgentId.CompareTo(id) == 0)
                {
                    dataList.Add(d);
                }
            });

            if (dataList.Count > 0)
            {
                dataList.ForEach((data) => {
                    _db.AgentStorage!.Remove(data);
                });
                JsonObject success = new JsonObject()
                {
                    ["success"] = dataList.Count() + " Records Removed for AgentId : " + id
                };
                return Ok(success);
            }

            JsonObject error = new JsonObject()
            {
                ["not_found"] = "There is no data present for the AgentId : " + id
            };
            return NotFound(error);

        }

    }
}
