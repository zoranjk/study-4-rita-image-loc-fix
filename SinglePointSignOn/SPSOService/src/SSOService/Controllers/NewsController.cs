﻿using ConsoleApp.PostgreSQL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SSOService.Database.DBModels;
using SSOService.DTO;
using SSOService.Helpers;
using System.Security.Cryptography;
using System.Text.Json.Nodes;

namespace SSOService.Controllers
{

    [Produces("Application/json")]
    [Consumes("Application/json")]
    [Route("news")]
    [ApiController]
    [Authorize]
    public class NewsController : ControllerBase
    {
        private readonly SSODBContext _db;
        public NewsController(SSODBContext context)
        {
            _db = context;
        }

        [HttpPost()]
        public async Task<ActionResult<News>> CreateNews(NewsDTO newsDTO)
        {
            Guid id = Guid.NewGuid();
            Console.Write("Creating New Record for News : " + id);

            try
            {
                var newNews = new News
                {
                    NewsId = id,
                    CreationDate = (DateTime)(newsDTO.CreationDate == null ? DateTime.UtcNow : DateTimeOffset.Parse(newsDTO.CreationDate).UtcDateTime),
                    Content = newsDTO.Content
                };

                _db.News.Add(newNews);
                await _db.SaveChangesAsync();

                return Created($"/news/{newNews.NewsId}", newNews);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<News>>> RetrieveNews()
        {
            return await _db.News!.AsNoTracking().OrderByDescending(e => e.CreationDate).ToListAsync();

        }

        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<News>> RetrieveNewsById(Guid id)
        {
            News news;
            try
            {
                Console.WriteLine("Getting News : " + id.ToString());

                news = await _db.News.AsNoTracking().FirstOrDefaultAsync(i => i.NewsId == id);
                if (news == null)
                {
                    return NotFound();
                }

                return Ok(news);

            }
            catch (Exception e)
            {
                return BadRequest(e);

            }

        }

        [HttpPut("{id}")]
        public async Task<ActionResult<News>> EditNews(Guid id, [FromBody] NewsDTO newsDTO)
        {
            Console.Write("Editing Record for News : " + id.ToString());

            try
            {
                News news = await _db.News.FindAsync(id);
                if (newsDTO != null)
                {
                    if (news != null)
                    {
                        news.CreationDate = (DateTime)(newsDTO.CreationDate == null ? DateTime.UtcNow : DateTimeOffset.Parse(newsDTO.CreationDate).UtcDateTime);
                        news.Content = newsDTO.Content == null ? "" : newsDTO.Content;
                    }
                    else
                    {
                        return NotFound();
                    }

                    _db.Entry(news).Property(b => b!.CreationDate).IsModified = true;
                    _db.Entry(news).Property(b => b!.Content).IsModified = true;

                    await _db.SaveChangesAsync();
                }
                return Ok(news);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult<News>> DeleteNews(Guid id)
        {
            if (await _db.News.FindAsync(id) is News news)
            {
                _db.News.Remove(news);
                await _db.SaveChangesAsync();
                return Ok(news);
            }

            return NotFound();
        }

    }
    
}
