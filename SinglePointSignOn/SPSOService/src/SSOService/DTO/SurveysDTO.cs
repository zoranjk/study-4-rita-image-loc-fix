﻿using System.Text.Json;
using System.Text.Json.Nodes;

namespace SSOService.DTO
{
    public class SurveysDTO
    { 
        public string participant_id { get; set; }
        public List<SurveyDTO> surveys { get; set; } = new List<SurveyDTO>();

        public SurveysDTO() { }

        public SurveysDTO(string participant_id) { 
            this.participant_id = participant_id;
        }
    }

    public class SurveyDTO
    {
        public string survey_name { get; set; }
        public string survey_id { get; set; }

        
        public List<SurveyItemDTO> data { get; set; }

        public bool complete { get; set; }

        public string? completedTimestamp { get; set; }
    }
    public class SurveyItemDTO
    {
        public string? qid { get; set; }
        public string? input_type { get; set; }
        public string? image_asset { get; set; }
        public List<string>? labels { get; set; }
        public string? response { get; set; }
        public string? text { get; set; }
    }
}
