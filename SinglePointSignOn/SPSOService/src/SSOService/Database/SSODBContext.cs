using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SSOService.Data;
using SSOService.Database.DBModels;
using SSOService.Helpers;

namespace ConsoleApp.PostgreSQL
{
    public class SSODBContext : DbContext
    {
        IConfiguration _config ;
        public SSODBContext(DbContextOptions<SSODBContext> options, IConfiguration _configuration)
        : base(options) {
            _config = _configuration;

         }
        public DbSet<User>? Users { get; set; }
        public DbSet<Team>? Teams { get; set; }
        public DbSet<Trial>? Trials { get; set; }
        public DbSet<News>? News { get; set; }

        public DbSet<AgentStorageData>? AgentStorage { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("Users").HasKey("ParticipantId");
            modelBuilder.Entity<Team>().ToTable("Teams").HasKey("TeamId");
            modelBuilder.Entity<Trial>().ToTable("Trials").HasKey("TrialId");
            modelBuilder.Entity<News>().ToTable("News").HasKey("NewsId");
            modelBuilder.Entity<AgentStorageData>().ToTable("AgentStorage").HasKey("StorageId");
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            Console.WriteLine("Calling SSODBContext.OnConfiguring");

            // set password from env file
            string adminId = Environment.GetEnvironmentVariable("ADMIN_ID");
            string adminPwd = Environment.GetEnvironmentVariable("ADMIN_PWD");

            string DB_CONNECTION_PWD = Environment.GetEnvironmentVariable("DB_CONNECTION_PWD");
            string DB_CONNECTION_USER = Environment.GetEnvironmentVariable("DB_CONNECTION_USER");
            string DB_CONNECTION_HOST = Environment.GetEnvironmentVariable("DB_CONNECTION_HOST");
            string DB_CONNECTION_PORT = Environment.GetEnvironmentVariable("DB_CONNECTION_PORT");
            string DB_CONNECTION_DATABASE = Environment.GetEnvironmentVariable("DB_CONNECTION_DATABASE");
            string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");


            /*             
             - DB_CONNECTION_PWD=${DB_CONNECTION_PWD}
             - DB_CONNECTION_USER=${DB_CONNECTION_USER}
             - DB_CONNECTION_HOST=${DB_CONNECTION_HOST}
             - DB_CONNECTION_PORT=${DB_CONNECTION_PORT}
             - DB_CONNECTION_DATABASE=${DB_CONNECTION_DATABASE}

            */
            string connexString = "host=" + DB_CONNECTION_HOST + ";port=" + DB_CONNECTION_PORT + ";database=" + DB_CONNECTION_DATABASE + ";username=" + DB_CONNECTION_USER + ";password=" + DB_CONNECTION_PWD;

            Console.WriteLine("--------------------------------------------------------------------------------");
            Console.WriteLine(connexString);
            Console.WriteLine("--------------------------------------------------------------------------------");

            optionsBuilder.UseNpgsql(
                    connexString,
                    providerOptions => providerOptions.EnableRetryOnFailure()
                )
                .EnableSensitiveDataLogging(); 
            
        }

        public void MigrateStoredVolumeData()
        {
            // THERE'S GOT TO BE A HOOK TO ONLY DO THIS ONCE THE CONNECTION IS ESTABLISHED

            try
            {
                Database.Migrate();

                //var keyConfig = _config.GetSection("Keys").Get<Keys>();

                // set password from env file
                string adminId = Environment.GetEnvironmentVariable("ADMIN_ID");
                string adminPwd = Environment.GetEnvironmentVariable("ADMIN_PWD");

                IQueryable<User> admin =

                    from a in Users
                    where String.Compare(a.UserId, adminId) == 0
                    select a;

                Console.WriteLine("Count : " + admin.Count());

                if (admin.Count() == 0)
                {
                    string SSO_SECRET_KEY = Environment.GetEnvironmentVariable("SSO_SECRET_KEY");
                    Console.WriteLine("Admin user does not exist - creating one. ");
                    User a = new User { UserId = adminId, Password = EncryptPassword.Encrypt(adminPwd,SSO_SECRET_KEY), IsAdmin = true, Email = "", ParticipantId = adminId };
                    Users!.Add(a);

                    admin =

                       from b in Users
                       where String.Compare(a.UserId, "admin") == 0
                       select b;

                    Console.WriteLine("Count : " + admin.Count());
                }
                else
                {
                    
                    Console.WriteLine("Admin user already exists - not creating one. ");
                }
                this.SaveChanges();

            }catch(Exception ex) {
                Console.WriteLine(ex.StackTrace);
            }
           

        }

        

        

        /*
         
        var strategy = db.Database.CreateExecutionStrategy();

        strategy.Execute(
            () =>
            {
                using var context = new BloggingContext();
                using var transaction = context.Database.BeginTransaction();

                context.Blogs.Add(new Blog { Url = "http://blogs.msdn.com/dotnet" });
                context.SaveChanges();

                context.Blogs.Add(new Blog { Url = "http://blogs.msdn.com/visualstudio" });
                context.SaveChanges();

                transaction.Commit();
            });
         
         
         */
    }

}