﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace SSOService.Database.DBModels
{
    public class AgentStorageData
    {
        public string AgentId { get; set; } = "NOT_SET";
        public string StorageId { get; set; } = "NOT_SET";
        public string DateLastUpdated { get; set; }  = "NOT_SET";
        public List<string> Tags { get; set; } = new List<string>();

        [Column(TypeName = "jsonb")]
        public JsonDocument? Data { get; set; } = null;   
    }
}
