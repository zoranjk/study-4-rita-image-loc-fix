﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Text.Json;

namespace SSOService.Database.DBModels
{
    public class Trial
    {
        public string ExperimentId { get; set; } = "NOT_SET";
        public string ExperimentName { get; set; } = "NOT_SET";
        public string TrialId { get; set; } = "NOT_SET";
        public string TrialName { get; set; } = "NOT_SET";
        public string MissionVariant { get; set; } = "NOT_SET";
        public string StartTimestamp { get; set; } = "NOT_SET";
        public string TeamId { get; set; } = "NOT_SET";
        public List<string> Members { get; set; } = new List<string>();
        public string ASICondition { get; set; } = "NOT_SET";
        public int TeamScore { get; set; } = 0;
        public JsonDocument? BombSummaryPlayer { get; set; }        
        public int BombsTotal { get; set; } = 0;
        public JsonDocument? BombsExploded { get; set; }
        public JsonDocument? DamageTaken { get; set; }
        public JsonDocument? BombBeaconsPlaced { get; set; }
        public JsonDocument? CommBeaconsPlaced { get; set; }
        public JsonDocument? TimesFrozen { get; set; }
        public JsonDocument? FlagsPlaced { get; set; }
        public JsonDocument? TeammatesRescued { get; set; }
        public JsonDocument? BudgetExpended { get; set; }
        public JsonDocument? FiresExtinguished { get; set; }        
        public JsonDocument? TextChatsSent { get; set; }
        public string LastActiveMissionTime { get; set; } = "NOT_SET";
        public int NumStoreVisits { get; set; } = 0;
        public int NumFieldVisits { get; set; } = 0;
        public JsonDocument? NumCompletePostSurveys { get; set; }
        public long TotalStoreTime { get; set; } = 0L;
        public string MissionEndCondition { get; set; } = "NOT_SET";
        public string TrialEndCondition { get; set; } = "NOT_SET";


    }
}
