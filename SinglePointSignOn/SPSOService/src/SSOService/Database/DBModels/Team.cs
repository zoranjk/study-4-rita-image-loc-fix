﻿namespace SSOService.Database.DBModels
{
    public class Team
    {
        public string TeamId { get; set; } = "NOT_SET";      
        public List<string> TeamPlays { get; set; } = new List<string>();
        public int TeamScore { get; set; } = 0;
        public List<string> Members { get; set; } = new List<string>();
        // this holds timestamps
        public List<string> NeverProceeded { get; set; } = new List<string>();
    }
}
