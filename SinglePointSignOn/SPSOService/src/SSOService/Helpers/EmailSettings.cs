﻿namespace SSOService.Helpers
{
    public class EmailSettings
    {
        public string Server { get; set; }
        public string FromName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public int Port { get; set; }
    }
}
