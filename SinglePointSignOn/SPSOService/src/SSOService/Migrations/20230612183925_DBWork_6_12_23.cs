﻿using System.Text.Json;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SSOService.Migrations
{
    public partial class DBWork_6_12_23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<JsonDocument>(
                name: "BombPhasesDefused",
                table: "Trials",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ExperimentName",
                table: "Trials",
                type: "text",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<JsonDocument>(
                name: "FlagsPlaced",
                table: "Trials",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddColumn<JsonDocument>(
                name: "TimesFrozen",
                table: "Trials",
                type: "jsonb",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "TrialName",
                table: "Trials",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BombPhasesDefused",
                table: "Trials");

            migrationBuilder.DropColumn(
                name: "ExperimentName",
                table: "Trials");

            migrationBuilder.DropColumn(
                name: "FlagsPlaced",
                table: "Trials");

            migrationBuilder.DropColumn(
                name: "TimesFrozen",
                table: "Trials");

            migrationBuilder.DropColumn(
                name: "TrialName",
                table: "Trials");
        }
    }
}
