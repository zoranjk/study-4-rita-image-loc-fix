﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SSOService.Migrations
{
    public partial class NeverProceeded2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
               name: "NeverProceeded",
               table: "Teams");

            migrationBuilder.AddColumn<List<string>>(
               name: "NeverProceeded",
               table: "Teams",
               type: "text[]",
               nullable: false,
               defaultValue: new List<string>()
            );
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "NeverProceeded",
                table: "Teams");
        }
    }
}
