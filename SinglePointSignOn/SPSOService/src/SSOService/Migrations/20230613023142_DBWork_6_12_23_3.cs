﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SSOService.Migrations
{
    public partial class DBWork_6_12_23_3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BombsDefused",
                table: "Trials",
                newName: "BombSummaryPlayer");

            migrationBuilder.RenameColumn(
                name: "BombPhasesDefused",
                table: "Trials",
                newName: "BombBeaconsPlaced");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "BombSummaryPlayer",
                table: "Trials",
                newName: "BombsDefused");

            migrationBuilder.RenameColumn(
                name: "BombBeaconsPlaced",
                table: "Trials",
                newName: "BombPhasesDefused");
        }
    }
}
