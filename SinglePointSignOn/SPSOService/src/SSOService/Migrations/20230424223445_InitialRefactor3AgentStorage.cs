﻿using System.Collections.Generic;
using System.Text.Json;
using Microsoft.EntityFrameworkCore.Migrations;
using SSOService.DTO;

#nullable disable

namespace SSOService.Migrations
{
    public partial class InitialRefactor3AgentStorage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AgentStorage",
                columns: table => new
                {
                    StorageId = table.Column<string>(type: "text", nullable: false),
                    AgentId = table.Column<string>(type: "text", nullable: false),
                    DateLastUpdated = table.Column<string>(type: "text", nullable: false),
                    Tags = table.Column<List<string>>(type: "text[]", nullable: false),
                    Data = table.Column<JsonDocument>(type: "jsonb", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AgentStorage", x => x.StorageId);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    TeamId = table.Column<string>(type: "text", nullable: false),
                    TeamPlays = table.Column<List<string>>(type: "text[]", nullable: false),
                    Members = table.Column<List<string>>(type: "text[]", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.TeamId);
                });

            migrationBuilder.CreateTable(
                name: "Trials",
                columns: table => new
                {
                    TrialId = table.Column<string>(type: "text", nullable: false),
                    ExperimentId = table.Column<string>(type: "text", nullable: false),
                    TeamId = table.Column<string>(type: "text", nullable: false),
                    Members = table.Column<List<string>>(type: "text[]", nullable: false),
                    ASICondition = table.Column<string>(type: "text", nullable: false),
                    TeamScore = table.Column<int>(type: "integer", nullable: false),
                    BombsDefused = table.Column<JsonDocument>(type: "jsonb", nullable: true),
                    BombsTotal = table.Column<int>(type: "integer", nullable: false),
                    BombsExploded = table.Column<JsonDocument>(type: "jsonb", nullable: true),
                    DamageTaken = table.Column<JsonDocument>(type: "jsonb", nullable: true),
                    CommBeaconsPlaced = table.Column<JsonDocument>(type: "jsonb", nullable: true),
                    TeammatesRescued = table.Column<JsonDocument>(type: "jsonb", nullable: true),
                    BudgetExpended = table.Column<JsonDocument>(type: "jsonb", nullable: true),
                    TotalStoreTime = table.Column<int>(type: "integer", nullable: false),
                    TrialEndCondition = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trials", x => x.TrialId);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    ParticipantId = table.Column<string>(type: "text", nullable: false),
                    UserId = table.Column<string>(type: "text", nullable: true),
                    Password = table.Column<string>(type: "text", nullable: false),
                    Email = table.Column<string>(type: "text", nullable: false),
                    IsAdmin = table.Column<bool>(type: "boolean", nullable: false),
                    ParticipationCount = table.Column<int>(type: "integer", nullable: false),
                    PreTrialSurveys = table.Column<SurveysDTO>(type: "jsonb", nullable: true),
                    PostTrialSurveys = table.Column<SurveysDTO>(type: "jsonb", nullable: true),
                    lastConsentTimestamp = table.Column<string>(type: "text", nullable: true),
                    TeamsList = table.Column<List<string>>(type: "text[]", nullable: false),
                    SuccessfulBombDisposals = table.Column<int>(type: "integer", nullable: false),
                    UnsuccessfulBombDisposals = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.ParticipantId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AgentStorage");

            migrationBuilder.DropTable(
                name: "Teams");

            migrationBuilder.DropTable(
                name: "Trials");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
