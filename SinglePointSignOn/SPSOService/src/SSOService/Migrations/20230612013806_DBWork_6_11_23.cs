﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SSOService.Migrations
{
    public partial class DBWork_6_11_23 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TeamScore",
                table: "Teams",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TeamScore",
                table: "Teams");
        }
    }
}
