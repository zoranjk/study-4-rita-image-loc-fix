class InstallConfig {
  String? minecraftInstallationDirBackup;
  String? minecraftInstallationDir;
  String? librariesInstallationPath;
  String? modInstallationPath;
  String? forgeInstallerPath;

  InstallConfig(
      {this.minecraftInstallationDirBackup,
      this.minecraftInstallationDir,
      this.librariesInstallationPath,
      this.modInstallationPath,
      this.forgeInstallerPath});

  InstallConfig.fromJson(Map<String, dynamic> json) {
    minecraftInstallationDirBackup = json['minecraft_installation_dir_backup'];
    minecraftInstallationDir = json['minecraft_installation_dir'];
    librariesInstallationPath = json['libraries_installation_path'];
    modInstallationPath = json['mod_installation_path'];
    forgeInstallerPath = json['forge_installer_path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['minecraft_installation_dir_backup'] =
        this.minecraftInstallationDirBackup;
    data['minecraft_installation_dir'] = this.minecraftInstallationDir;
    data['libraries_installation_path'] = this.librariesInstallationPath;
    data['mod_installation_path'] = this.modInstallationPath;
    data['forge_installer_path'] = this.forgeInstallerPath;
    return data;
  }
}
