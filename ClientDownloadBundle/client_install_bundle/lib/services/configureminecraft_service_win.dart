import 'dart:convert';
import 'dart:io';

import 'package:client_install_bundle/main.dart';
import 'package:client_install_bundle/models/progress_bar_data.dart';
import 'package:file/src/interface/file_system_entity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:shell/shell.dart';
import 'dart:isolate';
import "dart:async";
import 'package:path/path.dart' as path;
import 'package:glob/glob.dart';
import 'package:glob/list_local_fs.dart';

import 'configureminecraft_service.dart';
import '../models/install_config.dart';
import '../models/progress_button_data.dart';
import 'progressbar_service.dart';
import 'progressbutton_service.dart';
import 'progresslog_service.dart';

Future<String> get _localPath async {
  return Directory.current.path;
}

// Data Model
class ConfigureMinecraftServiceWin extends ConfigureMinecraftService {
  ConfigureMinecraftServiceWin() {
    installRunning = false;
  }

  @override
  doInstallIsolate(SendPort sendPort) async {
    var duration = const Duration(seconds: 5);
    int taskNum = 0;
    List<String> installTaskList = [
      'Read Configuration',
      'Backup Minecraft directory',
      'Extract Java',
      'Install Forge',
      'Install ASIST Mod'
    ];
    bool javaFound = false;
    String javaPath = "java.exe";

    try {
      ProgressBarData progressBarData =
          ProgressBarData('Overall Progress', 0, 'Task Progress', 0);

      if (kDebugMode) {
        print("Running install");
      }

      sendPort.send(progressBarData);
      sendPort.send('Starting Install\n\n');

      // Read Configuration
      progressBarData.label1 =
          'Overall Progress: (Task ${taskNum + 1} / ${installTaskList.length})';
      progressBarData.percentage2 = 0;
      progressBarData.label2 = installTaskList[taskNum++];
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      InstallConfig? installConfig = await loadInstallConfig(sendPort);
      if (installConfig == null) {
        throw Exception('Unable to load configuration');
      }

      sendPort.send('Current Configuration:\n');
      sendPort.send('${installConfig.minecraftInstallationDirBackup}\n');
      sendPort.send('${installConfig.minecraftInstallationDir}\n');
      sendPort.send('${installConfig.librariesInstallationPath}\n');
      sendPort.send('${installConfig.modInstallationPath}\n');
      sendPort.send('${installConfig.forgeInstallerPath}\n');
      sendPort.send('\n');

      progressBarData.percentage1 += 1 / installTaskList.length;
      progressBarData.percentage2 = 1;
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      String appData = '';
      if (Platform.environment['USERPROFILE'] != null) {
        appData = Platform.environment['USERPROFILE']!;
      }

      // Test for Minecraft
      if (!(await Directory('$appData${installConfig.minecraftInstallationDir}')
          .exists())) {
        sendPort.send('Minecraft is not installed in the default folder:\n');
        sendPort.send(
            'Please change the installation path in the config.json file to the location of your minecraft installation\n');
        throw Exception('Minecraft installation not found');
      }

      // Test for Minecraft backup directory
      progressBarData.label1 =
          'Overall Progress: (Task ${taskNum + 1} / ${installTaskList.length})';
      progressBarData.percentage2 = 0;
      progressBarData.label2 = installTaskList[taskNum++];
      sendPort.send(progressBarData);

      var sourceDir =
          Directory('$appData${installConfig.minecraftInstallationDir}');
      var destDir =
          Directory('$appData${installConfig.minecraftInstallationDirBackup}');

      if (await Directory(
              '$appData${installConfig.minecraftInstallationDirBackup}')
          .exists()) {
        sendPort.send(
            'You already have a backup directory ... skipping backup\n\n');
        sleep(const Duration(seconds: 1));

        // Remove any existing .Minecraft directory from a previous install attempt
        sendPort.send(
            'Remove-Item -Path $appData${installConfig.minecraftInstallationDir} -Recurse\n');

        if (kDebugMode) {
          print(
              'Remove-Item -Path $appData${installConfig.minecraftInstallationDir} -Recurse');
        }

        Process process = await Process.start('powershell', [
          '-c',
          'Remove-Item -Path $appData${installConfig.minecraftInstallationDir} -Recurse'
        ]);
        await process.stdout.transform(utf8.decoder).forEach(sendPort.send);
        int exitCode = await process.exitCode;
        if (exitCode != 0) {
          if (kDebugMode) {
            print('Exit code = $exitCode');
          }
        }
      } else {
        // Create backup of minecraft directory
        sendPort.send('Creating Backup of Minecraft directory\n');

        sendPort.send(
            'Rename-Item -Path "${sourceDir.path}" -NewName "${destDir.path}"\n');
        Process process = await Process.start('powershell', [
          '-c',
          'Rename-Item -Path "${sourceDir.path}" -NewName "${destDir.path}"'
        ]);
        await process.stdout.transform(utf8.decoder).forEach((message) {
          sendPort.send(message);
        });
        int exitCode = await process.exitCode;
        if (exitCode != 0) {
          if (kDebugMode) {
            print('Exit code = $exitCode');
          }
          throw Exception('Could not backup Minecraft directory');
        }
      }

      // Create empty Minecraft dir
      sendPort.send('Create new .minecraft directory\n');
      sourceDir.createSync();

      // Copy needed files back to Minecraft dir
      sendPort.send(
          'Copy-Item -Path ${destDir.path}\\* -Destination ${sourceDir.path}\\\n');

      Process process = await Process.start('powershell', [
        '-c',
        'Copy-Item -Path ${destDir.path}\\* -Destination ${sourceDir.path}\\'
      ]);
      await process.stdout.transform(utf8.decoder).forEach((message) {
        sendPort.send(message);
      });
      int exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
        throw Exception('Could not backup Minecraft directory');
      }

      progressBarData.percentage1 += 1 / installTaskList.length;
      progressBarData.percentage2 = 1;
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      // Java
      progressBarData.label1 =
          'Overall Progress: (Task ${taskNum + 1} / ${installTaskList.length})';
      progressBarData.percentage2 = 0;
      progressBarData.label2 = installTaskList[taskNum++];
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      Directory tempDir = Directory.systemTemp.createTempSync();
      String tempPath = tempDir.path;
      sendPort.send(
          'Expand-Archive -Path ${prefix}assets/runtime.zip -DestinationPath "$tempPath"\n');
      process = await Process.start('powershell', [
        '-c',
        'Expand-Archive -Path ${prefix}assets/runtime.zip -DestinationPath "$tempPath"'
      ]);
      await process.stdout.transform(utf8.decoder).forEach((message) {
        sendPort.send(message);
        //javaFound = true;
      });
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
        throw Exception('Could not extract Java runtime');
      }

      javaPath =
          '$tempPath\\runtime\\jre-legacy\\windows-x64\\jre-legacy\\bin\\java.exe';
      sendPort.send(
          'Get-Command "${javaPath}" -ErrorAction Stop | Select-Object Path\n');
      process = await Process.start('powershell', [
        '-c',
        'Get-Command "${javaPath}" -ErrorAction Stop | Select-Object Path'
      ]);
      await process.stdout.transform(utf8.decoder).forEach((message) {
        sendPort.send(message);
        javaFound = true;
      });
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
        //throw Exception('Could not check for Java');
      }

      if (!javaFound) {
        javaPath =
            "C:\\Program Files (x86)\\Minecraft Launcher\\runtime\\java-runtime-alpha\\windows-x64\\java-runtime-alpha\\bin\\java.exe";
        sendPort.send(
            'Get-Command "${javaPath}" -ErrorAction Stop | Select-Object Path\n');
        Process process = await Process.start('powershell', [
          '-c',
          'Get-Command "${javaPath}" -ErrorAction Stop | Select-Object Path'
        ]);
        await process.stdout.transform(utf8.decoder).forEach((message) {
          sendPort.send(message);
          javaFound = true;
        });
        int exitCode = await process.exitCode;
        if (exitCode != 0) {
          if (kDebugMode) {
            print('Exit code = $exitCode');
          }
          //throw Exception('Could not check for Java');
        }
      }

      if (!javaFound) {
        javaPath =
            "C:\\Program Files (x86)\\Minecraft Launcher\\runtime\\jre-legacy\\windows-x64\\jre-legacy\\bin\\java.exe";
        sendPort.send(
            'Get-Command "${javaPath}" -ErrorAction Stop | Select-Object Path\n');
        Process process = await Process.start('powershell', [
          '-c',
          'Get-Command "${javaPath}" -ErrorAction Stop | Select-Object Path'
        ]);
        await process.stdout.transform(utf8.decoder).forEach((message) {
          sendPort.send(message);
          javaFound = true;
        });
        int exitCode = await process.exitCode;
        if (exitCode != 0) {
          if (kDebugMode) {
            print('Exit code = $exitCode');
          }
          //throw Exception('Could not check for Java');
        }
      }

      if (!javaFound) {
        sendPort.send(
            'It appears do not have Java installed! This experiment requires Minecraft Java.\n');
        throw Exception('Java not found');
      } else {
        sendPort.send('Java found at: ${javaPath}\n');
      }
      sendPort.send('\n');

      progressBarData.percentage1 += 1 / installTaskList.length;
      progressBarData.percentage2 = 1;
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      // Forge
      progressBarData.label1 =
          'Overall Progress: (Task ${taskNum + 1} / ${installTaskList.length})';
      progressBarData.percentage2 = 0;
      progressBarData.label2 = installTaskList[taskNum++];
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      // Copy version and libraries to minecraft directory
      // Libraries
      sendPort.send(
          'Copy-Item -Path ${prefix}assets\\libraries\\* -Destination $appData${installConfig.minecraftInstallationDir}\\libraries -Recurse\n');

      if (kDebugMode) {
        print(
            'Copy-Item -Path ${prefix}assets\\libraries\\* -Destination $appData${installConfig.minecraftInstallationDir}\\libraries -Recurse');
      }

      process = await Process.start('powershell', [
        '-c',
        'New-Item -Path $appData${installConfig.minecraftInstallationDir}\\libraries -Type Directory -Force'
      ]);
      //await process.stdout.transform(utf8.decoder).forEach(sendPort.send);
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
      }

      process = await Process.start('powershell', [
        '-c',
        'Copy-Item -Path ${prefix}assets/libraries/* -Destination $appData${installConfig.minecraftInstallationDir}\\libraries -Recurse -Force'
      ]);
      await process.stdout.transform(utf8.decoder).forEach(sendPort.send);
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
        throw Exception('Could not Copy-Item libraries');
      }

      // Versions
      sendPort.send(
          'Copy-Item -Path ${prefix}assets\\versions\\* -Destination $appData${installConfig.minecraftInstallationDir}\\versions -Recurse\n');

      if (kDebugMode) {
        print(
            'Copy-Item -Path ${prefix}assets\\versions\\* -Destination $appData${installConfig.minecraftInstallationDir}\\versions -Recurse');
      }

      process = await Process.start('powershell', [
        '-c',
        'New-Item -Path $appData${installConfig.minecraftInstallationDir}\\versions -Type Directory -Force'
      ]);
      //await process.stdout.transform(utf8.decoder).forEach(sendPort.send);
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
      }

      process = await Process.start('powershell', [
        '-c',
        'Copy-Item -Path ${prefix}assets/versions/* -Destination $appData${installConfig.minecraftInstallationDir}\\versions -Recurse -Force'
      ]);
      await process.stdout.transform(utf8.decoder).forEach(sendPort.send);
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
        throw Exception('Could not Copy-Item versions');
      }

      // java -jar $configs.forge_installer_path
      sendPort.send(
          'Please select "OK" in the FORGE Mod System installer popup and complete the embedded sub-installer\n\n');

      var jarFile = path.basename('${installConfig.forgeInstallerPath}');
      sendPort.send('"${javaPath}" -jar ${prefix}assets/$jarFile\n');
      process = await Process.start('powershell',
          ['-c', '& "${javaPath}"', '-jar ${prefix}assets/$jarFile']);
      await process.stdout.transform(utf8.decoder).forEach((message) {
        sendPort.send(message);
      });
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }

        // Remove Temp Directory
        tempDir.deleteSync(recursive: true);

        throw Exception('Unable to install Forge');
      }

      // Remove Temp Directory
      tempDir.deleteSync(recursive: true);

      progressBarData.percentage1 += 1 / installTaskList.length;
      progressBarData.percentage2 = 1;
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      // Mod
      progressBarData.label1 =
          'Overall Progress: (Task ${taskNum + 1} / ${installTaskList.length})';
      progressBarData.percentage2 = 0;
      progressBarData.label2 = installTaskList[taskNum++];
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      sendPort.send('Copying ASIST mod to mod directory\n');
      var modDir = Directory('$appData${installConfig.modInstallationPath}');
      if (await Directory('$appData${installConfig.modInstallationPath}')
          .exists()) {
        sendPort.send(
            'Mod folder found at: $appData${installConfig.modInstallationPath}\n');
      } else {
        sendPort.send(
            'Mod folder not found at $appData${installConfig.modInstallationPath}\n');
        sendPort.send('Creating folder\n');
        modDir.createSync();
      }

      sendPort.send(
          'Remove-Item -Path $appData${installConfig.modInstallationPath}\\asistmod*.jar\n');

      if (kDebugMode) {
        print(
            'Remove-Item -Path $appData${installConfig.modInstallationPath}\\asistmod*.jar');
      }

      process = await Process.start('powershell', [
        '-c',
        'Remove-Item -Path $appData${installConfig.modInstallationPath}/asistmod*.jar'
      ]);
      await process.stdout.transform(utf8.decoder).forEach(sendPort.send);
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
        throw Exception('Could not Remove-Item');
      }

      sendPort.send('Copying mod from: ${prefix}assets/asistmod*.jar\n');

      var assetDir = Directory('${prefix}assets');
      assetDir.listSync(recursive: false).forEach((var modFileEntry) {
        if (path.basename(modFileEntry.path).startsWith('asistmod') &&
            path.basename(modFileEntry.path).endsWith('.jar')) {
          sendPort.send('Copying ${path.basename(modFileEntry.path)}\n');
          var modFile = File(modFileEntry.path);
          modFile.copy(
              '$appData${installConfig.modInstallationPath}/${path.basename(modFileEntry.path)}');
        }
      });
      sendPort.send('\n');

      progressBarData.percentage1 += 1 / installTaskList.length;
      progressBarData.percentage2 = 1;
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));
    } on Exception catch (e) {
      sendPort.send(e);
    }
  }

  @override
  Future<InstallConfig?> loadInstallConfig(SendPort? sendPort) async {
    if (this.installConfig != null) {
      return this.installConfig;
    }

    prefix = './data/flutter_assets/';
    String configJson = '';

    if (defaultTargetPlatform == TargetPlatform.macOS) {
      sendPort?.send(
          'Checking for configuration at: assets/installation_config_mac.json\n');
      try {
        configJson =
            await rootBundle.loadString("assets/installation_config_mac.json");
      } on Exception catch (_) {
        return null;
      }
    } else {
      sendPort?.send(
          'Checking for configuration at: ${prefix}assets/installation_config.json\n');

      var file = File('${prefix}assets/installation_config.json');

      try {
        configJson = file.readAsStringSync();
      } on Exception catch (_) {
        prefix = '';
        file = File('${prefix}assets/installation_config.json');

        sendPort?.send(
            'Checking for configuration at: ${prefix}assets/installation_config.json\n');

        try {
          configJson = file.readAsStringSync();
        } on Exception catch (_) {}
      }
    }

    Map<String, dynamic> configMap = jsonDecode(configJson);
    InstallConfig installConfig = InstallConfig.fromJson(configMap);
    this.installConfig = installConfig;
    return installConfig;
  }

  @override
  doUninstallIsolate(SendPort sendPort) async {
    var duration = const Duration(seconds: 5);
    int taskNum = 0;
    List<String> installTaskList = [
      'Read Configuration',
      'Remove ASIST Minecraft directory',
      'Restore Minecraft directory backup'
    ];

    try {
      ProgressBarData progressBarData =
          ProgressBarData('Overall Progress', 0, 'Task Progress', 0);

      if (kDebugMode) {
        print("Running uninstall");
      }

      sendPort.send(progressBarData);
      sendPort.send('Starting Uninstall\n\n');

      // Read Configuration
      progressBarData.label1 =
          'Overall Progress: (Task ${taskNum + 1} / ${installTaskList.length})';
      progressBarData.percentage2 = 0;
      progressBarData.label2 = installTaskList[taskNum++];
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      InstallConfig? installConfig = await loadInstallConfig(sendPort);
      if (installConfig == null) {
        throw Exception('Unable to load configuration');
      }

      sendPort.send('Current Configuration:\n');
      sendPort.send('${installConfig.minecraftInstallationDirBackup}\n');
      sendPort.send('${installConfig.minecraftInstallationDir}\n');
      sendPort.send('${installConfig.librariesInstallationPath}\n');
      sendPort.send('${installConfig.modInstallationPath}\n');
      sendPort.send('${installConfig.forgeInstallerPath}\n');
      sendPort.send('\n');

      progressBarData.percentage1 += 1 / installTaskList.length;
      progressBarData.percentage2 = 1;
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      String appData = '';
      if (Platform.environment['USERPROFILE'] != null) {
        appData = Platform.environment['USERPROFILE']!;
      }

      // Test for Minecraft
      if (!(await Directory('$appData${installConfig.minecraftInstallationDir}')
          .exists())) {
        sendPort.send('Minecraft is not installed in the default folder:\n');
        sendPort.send(
            'Please change the installation path in the config.json file to the location of your minecraft installation\n');
        throw Exception('Minecraft installation not found');
      }

      // Remove Minecraft directory',
      progressBarData.label1 =
          'Overall Progress: (Task ${taskNum + 1} / ${installTaskList.length})';
      progressBarData.percentage2 = 0;
      progressBarData.label2 = installTaskList[taskNum++];
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      sendPort.send(
          'Remove-Item -Path $appData${installConfig.minecraftInstallationDir} -Recurse\n');

      if (kDebugMode) {
        print(
            'Remove-Item -Path $appData${installConfig.minecraftInstallationDir} -Recurse');
      }

      Process process = await Process.start('powershell', [
        '-c',
        'Remove-Item -Path $appData${installConfig.minecraftInstallationDir} -Recurse'
      ]);
      await process.stdout.transform(utf8.decoder).forEach(sendPort.send);
      int exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
        throw Exception('Could not Remove-Item');
      }
      sendPort.send('\n');

      progressBarData.percentage1 += 1 / installTaskList.length;
      progressBarData.percentage2 = 1;
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      // Move Restore Minecraft directory backup
      progressBarData.label1 =
          'Overall Progress: (Task ${taskNum + 1} / ${installTaskList.length})';
      progressBarData.percentage2 = 0;
      progressBarData.label2 = installTaskList[taskNum++];
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));

      sendPort.send(
          'Rename-Item -Path $appData${installConfig.minecraftInstallationDirBackup} -NewName $appData${installConfig.minecraftInstallationDir}');

      if (kDebugMode) {
        print(
            'Rename-Item -Path $appData${installConfig.minecraftInstallationDirBackup} -NewName $appData${installConfig.minecraftInstallationDir}');
      }

      process = await Process.start('powershell', [
        '-c',
        'Rename-Item -Path $appData${installConfig.minecraftInstallationDirBackup} -NewName $appData${installConfig.minecraftInstallationDir}'
      ]);
      await process.stdout.transform(utf8.decoder).forEach(sendPort.send);
      exitCode = await process.exitCode;
      if (exitCode != 0) {
        if (kDebugMode) {
          print('Exit code = $exitCode');
        }
        throw Exception('Could not Rename-Item');
      }
      sendPort.send('\n');

      progressBarData.percentage1 += 1 / installTaskList.length;
      progressBarData.percentage2 = 1;
      sendPort.send(progressBarData);
      sleep(const Duration(seconds: 1));
    } on Exception catch (e) {
      sendPort.send(e);
    }
  }
}
