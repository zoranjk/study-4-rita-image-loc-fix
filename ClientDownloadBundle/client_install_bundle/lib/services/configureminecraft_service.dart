import 'dart:convert';
import 'dart:io';

import 'package:client_install_bundle/main.dart';
import 'package:client_install_bundle/models/progress_bar_data.dart';
import 'package:file/src/interface/file_system_entity.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:shell/shell.dart';
import 'dart:isolate';
import "dart:async";
import 'package:path/path.dart' as path;
import 'package:glob/glob.dart';
import 'package:glob/list_local_fs.dart';

import '../models/install_config.dart';
import '../models/progress_button_data.dart';
import 'progressbar_service.dart';
import 'progressbutton_service.dart';
import 'progresslog_service.dart';

Future<String> get _localPath async {
  return Directory.current.path;
}

// Data Model
abstract class ConfigureMinecraftService {
  bool installRunning = false;
  bool runUninstall = false;
  bool hadError = false;
  final progressLogService = getIt.get<ProgressLogService>();
  final progressBarService = getIt.get<ProgressBarService>();
  final progressButtonService = getIt.get<ProgressButtonService>();
  Shell shell = Shell();
  String prefix = '';
  InstallConfig? installConfig;

  ConfigureMinecraftService() {
    installRunning = false;
  }

  startConfiguration(bool runUninstall) async {
    if (!installRunning) {
      installRunning = true;
      hadError = false;
      this.runUninstall = runUninstall;
      ReceivePort receivePort = ReceivePort();
      ReceivePort exitPort = ReceivePort();
      Completer c1 = new Completer();

      String localPath = await _localPath;

      Isolate installIsolate;

      ProgressButtonData progressButtonData =
          ProgressButtonData('Please wait', false, false);
      progressButtonService.setProgressButtonData(progressButtonData);

      if (runUninstall) {
        installIsolate = await Isolate.spawn<SendPort>(
            doUninstallIsolate, receivePort.sendPort,
            paused: true);
      } else {
        installIsolate = await Isolate.spawn<SendPort>(
            doInstallIsolate, receivePort.sendPort,
            paused: true);
      }

      installIsolate.addOnExitListener(exitPort.sendPort);

      exitPort.listen((message) {
        if (message == null) {
          // A null message means the isolate exited
          if (!hadError) {
            if (kDebugMode) {
              print(runUninstall ? "Uninstall complete" : "Install complete");
            }
            progressLogService.addLine(
                runUninstall ? 'Uninstall complete\n' : 'Install complete\n');

            progressButtonData = ProgressButtonData('Finish', true, false);
            progressButtonService.setProgressButtonData(progressButtonData);
          } else {
            // Error
            if (kDebugMode) {
              print(runUninstall
                  ? "Uninstall encountered error"
                  : "Install encountered error");
            }
            progressLogService.addLine(runUninstall
                ? 'Uninstall encountered error\n'
                : 'Install encountered error\n');

            progressButtonData = ProgressButtonData('Exit', true, true);
            progressButtonService.setProgressButtonData(progressButtonData);
          }
          installRunning = false;
        }
      });

      if (installIsolate.pauseCapability != null) {
        installIsolate.resume(installIsolate.pauseCapability!);
      }

      await for (var message in receivePort) {
        if (message is String) {
          progressLogService.addLine(message);
        } else if (message is ProgressBarData) {
          progressBarService.setProgressBarData(message);
        } else if (message is ProgressButtonData) {
          progressButtonService.setProgressButtonData(message);
        } else if (message is Exception) {
          Exception ex = message;
          hadError = true;
          progressLogService.addLine(ex.toString());
          progressLogService.addLine('\n');
        }
      }
    }
  }

  Future<InstallConfig?> loadInstallConfig(SendPort? sendPort);

  doInstallIsolate(SendPort sendPort);

  doUninstallIsolate(SendPort sendPort);

  int copyDirectory(Directory source, Directory destination, int fileCount,
      int numFiles, SendPort sendPort, ProgressBarData progressBarData) {
    source.listSync(recursive: false).forEach((var entity) {
      if (entity is Directory) {
        var newDirectory = Directory(
            path.join(destination.absolute.path, path.basename(entity.path)));
        newDirectory.createSync();

        fileCount = copyDirectory(entity.absolute, newDirectory, fileCount,
            numFiles, sendPort, progressBarData);
      } else if (entity is File) {
        entity
            .copySync(path.join(destination.path, path.basename(entity.path)));
        fileCount++;

        if (fileCount % 20 == 0) {
          progressBarData.label2 =
              'Backup Minecraft directory ($fileCount / $numFiles)';
          progressBarData.percentage2 = fileCount / numFiles;
          sendPort.send(progressBarData);
        }
      }
    });

    return fileCount;
  }

  int dirStatSync(Directory dir) {
    int fileNum = 0;
    try {
      if (dir.existsSync()) {
        dir.listSync(recursive: true, followLinks: false).forEach((entity) {
          if (entity is File) {
            fileNum++;
          }
        });
      }
    } catch (e) {
      print(e.toString());
    }

    return fileNum;
  }
}
