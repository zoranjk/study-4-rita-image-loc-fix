import 'package:rxdart/rxdart.dart';

import '../models/progress_bar_data.dart';

// Data Model
class ProgressBarService {
  final BehaviorSubject<ProgressBarData> _progressBarData =
      BehaviorSubject.seeded(
          ProgressBarData('Overall Progress', 0, 'Task Progress', 0));

  Stream get stream$ => _progressBarData.stream;
  ProgressBarData get current => _progressBarData.value;

  setProgressBarData(ProgressBarData progressBarData) {
    _progressBarData.add(progressBarData);
  }
}
