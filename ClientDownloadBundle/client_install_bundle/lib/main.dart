import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:glob/glob.dart';
import 'package:glob/list_local_fs.dart';
import 'package:path/path.dart' as path;

import 'models/install_config.dart';
import 'services/configureminecraft_service.dart';
import 'services/configureminecraft_service_win.dart';
import 'services/configureminecraft_service_mac.dart';
import 'services/progressbutton_service.dart';
import 'services/progresslog_service.dart';
import 'wizard_page2.dart';
import 'services/progressbar_service.dart';

final getIt = GetIt.instance;

void main() {
  getIt.registerSingleton<ProgressLogService>(ProgressLogService());
  getIt.registerSingleton<ProgressBarService>(ProgressBarService());
  getIt.registerSingleton<ProgressButtonService>(ProgressButtonService());
  if (Platform.isMacOS) {
    getIt.registerSingleton<ConfigureMinecraftService>(
        ConfigureMinecraftServiceMac());
  } else if (Platform.isWindows) {
    getIt.registerSingleton<ConfigureMinecraftService>(
        ConfigureMinecraftServiceWin());
  }

  runApp(MyApp());
}

bool runUninstall = false;

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  Future<bool> getRunUninstallState() async {
    final configureMinecraftService = getIt.get<ConfigureMinecraftService>();
    InstallConfig? installConfig =
        await configureMinecraftService.loadInstallConfig(null);
    if (installConfig == null) {
      throw ('Configuration can not be loaded');
    }

    if (kDebugMode) {
      print('Config: ${installConfig.minecraftInstallationDirBackup}');
    }

    String appData = '';
    if (Platform.isMacOS) {
      if (Platform.environment['HOME'] != null) {
        appData = Platform.environment['HOME']!;
      }
    } else if (Platform.isWindows) {
      if (Platform.environment['USERPROFILE'] != null) {
        appData = Platform.environment['USERPROFILE']!;
      }
    }

    if (Directory('$appData${installConfig.minecraftInstallationDirBackup}')
        .existsSync()) {
      if (kDebugMode) {
        print(
            'Looking for mods in: $appData${installConfig.modInstallationPath}');
      }

      // Look for assist mod
      var endLoop = false;
      var assetDir = Directory('${configureMinecraftService.prefix}assets');
      assetDir.listSync(recursive: false).forEach((var modFileEntry) {
        if (!endLoop &&
            path.basename(modFileEntry.path).startsWith('asistmod') &&
            path.basename(modFileEntry.path).endsWith('.jar')) {
          var modFile = File(modFileEntry.path);

          if (File(
                  '$appData${installConfig.modInstallationPath}/${path.basename(modFile.path)}')
              .existsSync()) {
            if (kDebugMode) {
              print('Found mod file: ${path.basename(modFile.path)}');
            }
            runUninstall = true;
          } else {
            if (kDebugMode) {
              print('Missing mod file: ${path.basename(modFile.path)}');
            }
            runUninstall = false;
            endLoop = true;
          }
        }
      });
    }
    return Future<bool>.value(runUninstall);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: FutureBuilder(
          future: getRunUninstallState(),
          builder:
              (BuildContext context, AsyncSnapshot<bool> runUninstallState) {
            if (runUninstallState.connectionState == ConnectionState.done) {
              if (!runUninstallState.hasData) {
                return const Center(child: Text('Error: No config found.'));
              } else {
                runUninstall = (runUninstallState.data == true) ? true : false;
                return MaterialApp(
                  title: 'Flutter Demo',
                  theme: ThemeData(
                    primarySwatch: Colors.blue,
                  ),
                  home: MyHomePage(
                    title: runUninstall
                        ? 'ASIST Minecraft Uninstaller'
                        : 'ASIST Minecraft Installer',
                  ),
                );
              }
            }
            return const Center(
              child: CircularProgressIndicator(),
            );
          },
        ));
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: const AppBody(),
    );
  }
}

class AppBody extends StatelessWidget {
  const AppBody({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

    final configureMinecraftService = getIt.get<ConfigureMinecraftService>();

    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              child: Container(
                alignment: Alignment.topLeft,
                child: Text(runUninstall
                    ? '''This will uninstall your installation of the ASIST Minecraft environment and restore the state of your minecraft environment.
                    '''
                    : '''This Installer will check if you have Minecraft and Java installed.

If so, it will proceed to make a copy of your minecraft directory to keep it safe, and keep that copy until the experiment session is over.

It will then install Minecraft Forge, as well as the Mod and and libraries the mod requires in the correct spots in your Minecraft Installation.

You can restore your original Minecraft Installation after completion by rerunning the installer.'''),
              ),
            ), //Container
            Align(
              alignment: Alignment.bottomRight,
              child: ElevatedButton(
                onPressed: () {
                  configureMinecraftService.startConfiguration(runUninstall);

                  if (kDebugMode) {
                    print('Navigating to Install/Uninstall Page');
                  }

                  Navigator.of(context).push(
                    CustomPageRoute(
                      builder: (context) => const WizardPage2(),
                    ),
                  );
                  // ledState(true);
                },
                style: style,
                child: Text(runUninstall ? 'Uninstall' : 'Install'),
              ),
            ),
          ],
        ));
  }
}

class CustomPageRoute extends MaterialPageRoute {
  CustomPageRoute({builder}) : super(builder: builder);

  @override
  Duration get transitionDuration => const Duration(milliseconds: 0);
}
