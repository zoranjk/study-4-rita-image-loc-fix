# client_install_bundle

Builds the flutter based client installer for ASIST/Minecraft

# Checking Flutter install

flutter doctor

# Building 

flutter build windows

(builds to adminless-testbed\ClientDownloadBundle\client_install_bundle\build\windows\runner\Release)

# Debug

flutter -d windows run

# Wrap flutter release directory in single exe installer
- Uses Nullsoft Scriptable Install System (Version 3.08 or greater)
- Recommend NSIS VS Code extension to edit and build the .nsi installer script file
  - Set path to makensisw.exe in Extension's VS COde settings
  - With .nsi displayed in VS Code, build with cntr-shift B
- Script file: adminless-testbed\ClientDownloadBundle\client_install_bundle\AsistSingleFileInstaller.nsi
- Uses files in: adminless-testbed\ClientDownloadBundle\client_install_bundle\build\windows\runner\Release
- Builds to: adminless-testbed\ClientDownloadBundle\client_install_bundle\AsistClientInstaller.exe
