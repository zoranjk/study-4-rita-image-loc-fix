# WINDOWS VERSION

# This Script will check if you have Minecraft and Java installed
# If so, it will proceed to make a copy of your minecraft directory to keep it safe, and keep that copy until
# experiment session is over.
# It will then install Minecraft Forge, as well as the Mod and and libraries the mod requires in the correct spots in your
# Minecraft Installation.
# You can restore your original Minecraft Installation after completion.

# read in CONFIGS
Write-Warning ( "Reverting your Minecraft installation back to it's original form ..." )
Write-Host ("Reading Installation Configs ")
$configs = Get-Content "./installation_config.json" | ConvertFrom-Json
Write-Host( $configs )

# TO CHECK

# Minecraft
$minecraft_installation_dir_backup = $env:USERPROFILE + $configs.minecraft_installation_dir_backup
$minecraft_installation_dir = $env:USERPROFILE + $configs.minecraft_installation_dir
#$libraries_installation_path = $env:USERPROFILE + $configs.libraries_installation_path
$mod_installation_path = $env:USERPROFILE + $configs.mod_installation_path


if(Test-Path -Path $minecraft_installation_dir){
    
    Write-Warning ("1. Minecraft installation found at : " + $minecraft_installation_dir)

    if(Test-Path -Path $minecraft_installation_dir_backup){
    
        Write-Warning ("2. Minecraft backup found at : " + $minecraft_installation_dir_backup)
        Write-Warning ("3. Deleting the ASIST experiment installation.")
        Remove-Item -r -fo $minecraft_installation_dir 

        Rename-Item $minecraft_installation_dir_backup $minecraft_installation_dir        
    }
    else{
        Write-Warning("2. No backup file was found. We cannot revert back to a non-existant installation.")
        Write-Host( "Press any key to continue")

        Read-Host 
        Exit
    }
}

else{
    
    Write-Warning ( "Looks like Minecraft is not installed in the default folder." )
    Write-Warning ( "Go ahead and change the installation path in the config.json file to the location of your minecraft installation")
}

Write-Warning ( "ASIST CLIENT BUNDLE successfully uninstalled! Minecraft has been reverted to its pre-experiment form.  Thank you for your time!" )

Write-Host( "Press any key to continue")

Read-Host 