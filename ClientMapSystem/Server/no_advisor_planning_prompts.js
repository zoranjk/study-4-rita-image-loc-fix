const config = require('./ClientMapConfig.json');
const planning_messages_array = [

    // {     
    //     "id": "<uuid for the intervention>",
    //     "created": new Date().toUTCString(),
    //     "start": -1,
    //     "duration": 10000,
    //     "content": "Now, consider the information you collected during your recon, as well as the guidance given in the pre-mission brief. Create a plan for how your team will coordinate, dispose of the bombs, and deal with any threats. I will ask you a few questions to get you started. Use the chat box to discuss your answers with your teammates, then select one of the options for each question. There are 4 prompts following this one. The prompts will each be active for 45 seconds. Please take the time to answer each prompt before proceeding to the Field. ",
    //     "receivers": [],
    //     "explanation": {},
    //     "response_options":[
    //             "Okay, we're ready to start planning."
    //         ]        
    // },
    {     
        "id": "<uuid for the intervention>",
        "created": new Date().toUTCString(),
        "start": -1,
        "duration": 10000,
        "content": "Discuss (in chat) with your team how you plan to deal with fire. Which of the following options most closely aligns with your plan?",
        "receivers": [],
        "explanation": {},
        "response_options":[
            "Assign one person to be primarily responsible to put out all the fires",
            "Distribute responsibility among the team",
            "Return to the shop to re-plan when fire breaks out",
            "I want to skip this question"
            ]        
    },
    {     
        "id": "<uuid for the intervention>",
        "created": new Date().toUTCString(),
        "start": -1,
        "duration": 10000,
        "content": "How does your team plan to help injured teammates that are frozen?",
        "receivers": [],
        "explanation": {},
        "response_options":[
            "Assign one person to be primarily responsible for helping them",
            "Closest person should help",
            "The player with the most health should help",
            "I want to skip this question"
            ]        
    },
    {     
        "id": "<uuid for the intervention>",
        "created": new Date().toUTCString(),
        "start": -1,
        "duration": 10000,
        "content": "How will your team respond to each other's beacons that are placed in the field?",
        "receivers": [],
        "explanation": {},
        "response_options":[
            "Closest person should go to help right away",
            "One person should be assigned to monitor beacons, and respond when beacons are placed",
            "Evaluate beacons as a group when we return to the shop",
            "I want to skip this question"
            ]        
    }
    //,
    // {     
    //     "id": "<uuid for the intervention>",
    //     "created": new Date().toUTCString(),
    //     "start": -1,
    //     "duration": 10000,
    //     "content": "Feel free to continue your planning session if you think it will help your team. You should use the planning map, select tools, then click on “Finish Planning and Shop Session” under the Shop:Tools tab.",
    //     "receivers": [],
    //     "explanation": {},
    //     "response_options":[
    //         "Ok"
    //         ]        
    // }

]  

class NoAdvisorPromptGenerator{
    constructor(mqtt_manager){
        this.mqtt_manager = mqtt_manager        
        this.automated_planning_messages = JSON.parse(JSON.stringify(planning_messages_array))        
        this.sent_index = -1;
        this.runningProtocol = false;
        for(let i = 0; i<this.automated_planning_messages.length; i++){
            this.automated_planning_messages[i].id= "NO_ADVISOR_PROMPT_"+i;
            this.automated_planning_messages[i].content= "["+(i+1)+"/"+(this.automated_planning_messages.length)+"] " + this.automated_planning_messages[i].content;
        }              
    }

    reset(){

        this.automated_planning_messages = JSON.parse(JSON.stringify(planning_messages_array))        
        this.sent_index = -1;
        this.runningProtocol = false;
        for(let i = 0; i<this.automated_planning_messages.length; i++){
            this.automated_planning_messages[i].id= "NO_ADVISOR_PROMPT_"+i;
            this.automated_planning_messages[i].content= "["+(i+1)+"/"+(this.automated_planning_messages.length)+"] " + this.automated_planning_messages[i].content;
        }   

    }

    startMessageRoutine(player_pid_array){
        this.runningProtocol = true;
        console.log("Beginning NO ADVISOR message routine.")
        let timeoutMilli = 0
        for(let i = 0; i<this.automated_planning_messages.length; i++){
            this.automated_planning_messages[i].receivers = player_pid_array;
            setTimeout(()=>{
                if(this.runningProtocol){
                    console.log("Sending NO ADVISOR_"+i+" message.")
                    const message = this.mqtt_manager.generateAsistMqttMessage("agent","1.2","Intervention:Chat","1.0.0",this.automated_planning_messages[i])
                    message.msg.source ="NO_ADVISOR"
                    this.mqtt_manager.publish("agent/intervention/NO_ADVISOR/chat", JSON.stringify(message))
                }
                
            },timeoutMilli)
            timeoutMilli += config.no_advisor_prompt_interval
        }   

    }

}






module.exports = {
    NoAdvisorPromptGenerator
}