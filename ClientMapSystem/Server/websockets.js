const config = require('./ClientMapConfig.json');
const socketIO = require('socket.io');
const mqtt = require('./mqtt_manager');
const { Player } = require('./player_manager');
const StateManager = require('./state_manager');
const trial_info_manager = require('./trial_info_manager')
const http = require('./http_functions')


class SocketManager {

    constructor(server,pm, stateKeeperInstance){
        this.thisInstance = this;
        this.client_sockets = new Map();
        this.socketid_to_name = new Map();        
        this.socket_io = null;
        this.player_manager = pm;
        this.stateKeeperInstance = stateKeeperInstance
        this.options = {
          transport:['websocket'],
          allowUpgrades:true
        }
        this.init_socket_io(server,this.options)
    }    

    linkMqttManager(mm){
        this.mqtt_manager = mm
    }

    sendMessageToOneSocketAndStore(message_hook_name,message,playername){  
        
        this.stateKeeperInstance.addPlayerMessageToStorageAndSend(message_hook_name,message,playername);

    }

    sendMessageToAllSocketsAndStore(message_hook_name,message){  
        
        for(let [k,v] of this.client_sockets){

            this.stateKeeperInstance.addPlayerMessageToStorageAndSend(message_hook_name,message,k);

        }

    }

    init_socket_io(server,options){

        // Initiate Websocket service
        this.socket_io = socketIO(server, options);

        this.socket_io.on('connection', (socket) => {            
            
            console.log( "Connection from : " + socket.client.id )
            if( socket.hasOwnProperty('playername') ) {
                console.log( "Connection from : " + socket.client.id + " / " + socket.playername)
                this.player_manager.player_map.get(socket.playername).disconnectTimestamp = -1;
            }

            socket.on("disconnect", (reason) => {
                try{
                    const name = socket.playername
                    //CLIENT DISCONNECTED
                    console.log( name  +" : Client Socket disconnected.")
                    console.log( reason );
                    console.log( socket.playername)
                    const player = this.player_manager.player_map.get(name)
                    if( player!==null && player!=undefined){
                        
                        player.socket = "DISCONNECTED"
                        player.disconnectTimestamp = Date.now();
                     
                    }
                    
                }
                catch(error){
                    console.log(error)
                }                          
            });             
           
            socket.on('assign_name_to_socket', (payload) => {

                console.log( payload );
            
                const name = payload['name']

                const clientMessageStore = payload['receivedMessages']

                socket.playername = name
            
                const settings = config;

                const admin_stack_ip = process.env.admin_stack_ip;                
                const admin_stack_port = process.env.admin_stack_port;                

                console.log("ADMIN_STACK_IP : " + admin_stack_ip);
                console.log("ADMIN_STACK_PORT : " + admin_stack_port);

                settings.admin_stack_ip = admin_stack_ip;
                settings.admin_stack_port = admin_stack_port;
                settings.db_api_token = process.env.DB_API_TOKEN;
            
                if(name !== undefined && name !== null && name.length>0 ){
                  
                    console.log('Clientmap connection successful for : ' + name + '. Assigning socket.');
                    let refresh = false;
                    if(this.client_sockets.has(name)){
                        console.log("Player : " + name+" had a socket previously - updating socket.")
                        refresh = true;
                        const playerState = this.stateKeeperInstance.client_app_state_holder.get(name)
                        playerState.all_received_message_ids_on_client = clientMessageStore
                        if(clientMessageStore.length>0){                
                            console.log("Reconnected Socket, Syncing missed messages (subset of all) ")            
                            const diffArray = playerState.computeStateDiff();
                            if( diffArray.length > 0 && diffArray!==null && diffArray != undefined){
                                console.log("State Diff Detected. Syncing messages:" + diffArray);
                                playerState.syncMessages( diffArray, this.thisInstance, name )
                            }       
                        }
                        else{
                            console.log("Reconnected Socket, Syncing missed messages (all) ")
                            playerState.retoreFullMessageStore( this.thisInstance , name )
                        }
                        
                    }
                   
                    if( this.player_manager.player_map.has(name) ){
                        this.player_manager.player_map.get(name).disconnectTimestamp = -1;
                        this.player_manager.player_map.get(name).socket = "CONNECTED"
                        this.client_sockets.set(name,socket)
                    }
                    else{
                        console.log("Unexpected Player - Redirecting.")
                        socket.emit("redirectToLogin",{response:false, token:'aUniqueToken', config: settings, refresh:false})
                    }
                    // we do not not create a player here - that happends on the /incoming_players endpoint
                    
                    this.player_manager.printPlayers()
                    if(trial_info_manager.trial_running === false){
                        if( this.player_manager.areAllPlayersConnected() ){
                            if(trial_info_manager.experiment_created === false){
                                trial_info_manager.all_players_present = true
                                this.player_manager.startExperiment()
                                trial_info_manager.experiment_created = true;
                                console.log("Experiment created for this team.")
                                trial_info_manager.trial_running = true;
                            }                     
                        } 
                    }
                    else{
                        console.log(" Trial already running. Player socket simply refreshed - will not check if trial should start.")
                    }                               
                    
                    // this is the one message we will not store in StateKeeper, as it is essentially the foundational handshake
                    socket.emit('authenticationResponse', {response:true, token:'aUniqueToken', config: settings, refresh:refresh})
                }
                else {
                  console.log("Player Name was undefined or null. Please resubmit this conneciton attempt.");
                }      
            });

            socket.on('confirm_leave_team',( playername )=>{
                
                const name = playername
                console.log(name)
                
                const player = this.player_manager.player_map.get(name);
                if ( player!=undefined && player != null) {

                    player.confirmed_left = true
                    player.continue_with_team = false
                    this.player_manager.printPlayers();

                    console.log(trial_info_manager.team_health)
                    
                    if( trial_info_manager.team_health === "DISSOLVE_TEAM_PLAYER_CHOICE"){

                        console.log( name + " Confirmed they are leaving the Team due to another member's vote to leave the team.")                        
                        
                        const num_players = this.player_manager.player_map.size;
                        let num_published = 0;
                        this.player_manager.player_map.forEach( (v,k)=>{
                            if( v.surveysPublished === true ){
                                num_published++;
                            }
                        })

                        console.log( num_published +"/"+num_players+" players have finished their surveys.")
    
                        if( this.player_manager.haveAllPlayersLeft() ){
                            console.log( "All player confirmed left.")                          
                            setTimeout(()=>{
                                this.mqtt_manager.sendTrialStop(false) 
                            },500);                                              
                        }
                    }        
                    
                    else if ( trial_info_manager.team_health === "DISSOLVE_TEAM_PLAYER_LEFT" ){
    
                        console.log( name +  " : Confirming player is leaving trial due to another player leaving prematurely.")                        
                        
                        if(this.player_manager.haveAllPlayersLeft() ){
                            console.log( "All player confirmed left.")                           
                           
                            const obj={
                                "reason":trial_info_manager.team_health
                            }
                            this.mqtt_manager.publish( "control/status/premature_mission_stop",JSON.stringify(obj));
                            
                            setTimeout(()=>{
                                this.mqtt_manager.sendTrialStop(false) 
                            },3000);                           
                        } 
    
                    }               
                    else if( trial_info_manager.team_health === "DISSOLVE_TEAM_MINECRAFT_DISCONNECT"){
    
                        console.log( name +" : Confirming a player is leaving due to another player disconnecting from Minecraft.")                 
                        
                        if( this.player_manager.haveAllPlayersLeft() ){
                            console.log( "All player confirmed left.")                           
                            
                            const obj={
                                "reason":trial_info_manager.team_health
                            }
                            this.mqtt_manager.publish( "control/status/premature_mission_stop",JSON.stringify(obj));
                            
                            setTimeout(()=>{
                                this.mqtt_manager.sendTrialStop(false) 
                            },3000);                            
                        }    
                    }
                    else if( trial_info_manager.team_health === "DISSOLVE_TEAM_CLIENTMAP_DISCONNECT"){
    
                        console.log( name +" : Confirming a player is leaving due to another player disconnecting from the ClientMap.")                 
                        
                        if( this.player_manager.haveAllPlayersLeft() ){
                            console.log( "All player confirmed left.")                          
                            
                            const obj={
                                "reason":trial_info_manager.team_health
                            }
                            this.mqtt_manager.publish( "control/status/premature_mission_stop",JSON.stringify(obj));
                            
                            setTimeout(()=>{
                                this.mqtt_manager.sendTrialStop(false) 
                            },3000);                            
                        }    
                    }                  
                }
                		
            });

            socket.on(("fetchTrialSummary"),()=>{

               http.fetchTrialSummary(this, socket.playername)

            })

            socket.on('leave_team',(data)=>{
                console.log("Received Leave Team Message.");
                console.log(trial_info_manager.team_health);
                if( trial_info_manager.team_health === "OK" || trial_info_manager.team_health === "CONTINUE_WITH_TEAM" ){
                    trial_info_manager.team_health = "DISSOLVE_TEAM_PLAYER_CHOICE" 
                    const name = data.name
                    console.log( name + " Voted to dissolve the team.")
                    const player = this.player_manager.player_map.get(name)
                    if ( player != null && player != undefined) {
                        player.continue_with_team = false
                        player.confirmed_left = true;

                        console.log( name + " Dissolving Team. Sending dissolve message to the team sockets.")              
                        this.sendMessageToAllSocketsAndStore("dissolve_team",{"reason":trial_info_manager.team_health})

                        console.log( " Checking if we can send instance down because all players left ... ")                    
                        
                        if( this.player_manager.haveAllPlayersLeft() ){
                            
                            console.log( "All player confirmed left.")
                            setTimeout(()=>{
                                this.mqtt_manager.sendTrialStop(false) 
                            },3000);                        
                        }
                        else{
                            setTimeout(()=>{                               
                                console.log( "10 minute timeout after first Player Disbanded Team properly at the End of Trial but server is still up ... sending Trial Stop again.")                    
                                this.mqtt_manager.sendTrialStop(false)
                            },600000);                           
                        }		
                    }
                }                
            });

            socket.on('leave_team_during_trial',(data)=>{
                if( trial_info_manager.team_health === "OK" || trial_info_manager.team_health === "CONTINUE_WITH_TEAM" ){
                    trial_info_manager.team_health = "DISSOLVE_TEAM_PLAYER_LEFT"
                    const name = data.name
                    console.log( name + " Left the team during a trial.")
                    const player = this.player_manager.player_map.get(name)
                    if ( player != null && player != undefined) {
                        player.continue_with_team = false
                        player.confirmed_left = true
                        this.player_manager.printPlayers();
                        console.log( name + " Dissolving Team. Sending dissolve message to the team sockets.")              
                        this.sendMessageToAllSocketsAndStore("dissolve_team",{"reason":trial_info_manager.team_health})
                        if( this.player_manager.haveAllPlayersLeft() ){
                            this.mqtt_manager.publish( "control/status/premature_mission_stop","{}");                            
                            setTimeout(()=>{
                                this.mqtt_manager.sendTrialStop(false) 
                            },3000);
                        }
                        else{                           
                            setTimeout(()=>{
                                const obj={
                                    "reason":trial_info_manager.team_health
                                }
                                console.log( "3 minute timeout after player left during trial ... sending Trial Stop.")                    
                                this.mqtt_manager.publish( "control/status/premature_mission_stop",JSON.stringify(obj));
                            },180000);
                            setTimeout(()=>{
                                this.mqtt_manager.sendTrialStop(false) 
                            },183000);
                        }
                    }
                }                
            });

            socket.on('continue_team',(data)=>{
                const name = data.name
                console.log( name + "  Voted to Continue With Current Team.")
                const player = this.player_manager.player_map.get(name); 
                if(player!=null && player!=undefined){
                    this.player_manager.player_map.get(name).continue_with_team = true

                    let allVotedContinue = true;

                    for (let [k, v] of this.player_manager.player_map) {
                        console.log("Continue With Team : " + k + " : " + v.continue_with_team )
                        if (v.continue_with_team === false){
                            allVotedContinue = false;
                            break;
                        }
                    }

                    console.log( "All Voted To Continue With Team : " + allVotedContinue )
                    
                    if(allVotedContinue){
                        
                        console.log( "All players voted to stay with the team.")
                        
                        trial_info_manager.team_health="CONTINUE_WITH_TEAM";

                        // trigger trial stop                       
                        
                        console.log( "Team Sticking together and all surveys published!")
                        
                        this.mqtt_manager.sendTrialStop(true) 
                                     
                    }
                }  
                
            })

            socket.on('trialStopAck',(data)=>{
                //this.stateKeeperInstance.clearPlayerStates()
                this.stateKeeperInstance.clearPlayerState(data['name'])
            })

            // Comes in the form --> {"content":content, "callSign":callSign, "participant_id":pid}
            socket.on('storeChat',(data)=>{

                if( trial_info_manager.trial_info.participant_ids != null && trial_info_manager.trial_info.participant_ids != undefined ) {                                               
                
                    this.sendMessageToAllSocketsAndStore("storeChat",data)

                    const addressees = []
                    //console.log(trial_info_manager.trial_info.participant_ids)
                    const pids = new Map( Object.entries(trial_info_manager.trial_info.participant_ids))
                    pids.forEach( (v,k)=>{
                        const value = JSON.stringify(v)
                        //console.log(v)
                        if( v === data.participant_id ){
                            //console.log("matched " + v)
                        }
                        else{
                            addressees.push(v)
                        }
                    } );
                    
                    const chatMessageData = {
                        "environment":"SHOP_GUI",
                        "sender_id": data.participant_id,
                        "recipients":addressees,
                        "message":data.record[1],
                        "message_id": "COMM_CHAT" + trial_info_manager.store_chat_id++
                    }

                    const chatMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:CommunicationChat","1.0.0",chatMessageData)

                    this.mqtt_manager.publish( "communication/chat", JSON.stringify(chatMessage));
                }
            })

             // Data is built on the frontend
             socket.on('itemDiscard',(data)=>{ 
             
                const itemDiscardData = data

                const itemDiscardMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:ItemDiscard","0.0.0",itemDiscardData)

                this.mqtt_manager.publish( "item/discard", JSON.stringify(itemDiscardMessage));
                
            })

            // Data is built on the frontend
            socket.on('interventionResponse',(data)=>{ 
             
                const agentResponseData = data

                const agentResponseMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:InterventionResponse","0.0.0",agentResponseData)

                this.mqtt_manager.publish( "agent/intervention/"+data.agent_id+"/response", JSON.stringify(agentResponseMessage));
                
            })

            socket.on('iconUpdate',(data)=>{ 
             
                const uiClickData = data

                const uiClickMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:UIClick","0.0.0",uiClickData)

                this.sendMessageToAllSocketsAndStore("iconUpdate",data)

                this.mqtt_manager.publish( "ui/click", JSON.stringify(uiClickMessage));
                
            })

            // const proposedPurchaseObject = {
            //     "action":action,
            //     "itemCode":itemCode,
            //     "callSign":callSignCode,
            //     "participant_id":this.sessionService.participant_id,
            //     "element_id": id,
            //     "parent_id":"StoreInteractionContainer",
            //     "type": "BUTTON",
            //     "x": target.clientLeft,
            //     "y": target.clientTop,
            //     "additional_info":{
            //       "meta_action":metaAction
            //     }
            //   }
            socket.on("proposedPurchase", (message) => {

                const data = message;

                this.sendMessageToAllSocketsAndStore("proposedPurchase",data)
                
                const proposedPurchaseMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:UIClick","0.0.0",data)
                this.mqtt_manager.publish( "ui/click", JSON.stringify(proposedPurchaseMessage));
            });

            socket.on("flagPlaced", (d) => {

                const data = d
                this.sendMessageToAllSocketsAndStore("flagPlaced",data)
                
                const flagPlacedMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:UIClick","0.0.0",data)
                this.mqtt_manager.publish( "ui/click", JSON.stringify(flagPlacedMessage));
            });

            socket.on("bombPlaced", (message) => {

                const data = {
                    "element_id": message.element_id,
                    "parent_id": message.parent_id,
                    "participant_id": message.participant_id,
                    "type": message.type,
                    "x" : message.x,
                    "y" : message.y,
                    "additional_info":{                        
                        "meta_action": message.additional_info.meta_action,
                        "meta_type" : message.additional_info.meta_type,
                        "call_sign_code" : message.additional_info.call_sign_code,
                        "bomb_index" : message.additional_info.bomb_index
                    }
                }

                // send messag not data, message has extra info not meant for the bus
                this.sendMessageToAllSocketsAndStore("bombPlaced",message)
                
                const bombPlacedMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:UIClick","0.0.0",data)
                this.mqtt_manager.publish( "ui/click", JSON.stringify(bombPlacedMessage));
            });

            socket.on("undoFlagPlaced", (message) => {

                const data = {
                    "element_id": message.element_id,
                    "parent_id": message.parent_id,
                    "participant_id": message.participant_id,
                    "type": message.type,
                    "x" : message.x,
                    "y" : message.y,
                    "additional_info":{                        
                        "meta_action": message.additional_info.meta_action,
                        "meta_type" : message.additional_info.meta_type,
                        "call_sign_code" : message.additional_info.call_sign_code,
                        "flag_index" : message.additional_info.flag_index
                    }
                }

                // send messag not data, message has extra info not meant for the bus
                this.sendMessageToAllSocketsAndStore("undoFlagPlaced",message)
                
                const undoFlagPlacedMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:UIClick","0.0.0",data)
                this.mqtt_manager.publish( "ui/click", JSON.stringify(undoFlagPlacedMessage));
            });

            socket.on("undoBombPlaced", (message) => {

                const data = {
                    "element_id": message.element_id,
                    "parent_id": message.parent_id,
                    "participant_id": message.participant_id,
                    "type": message.type,
                    "x" : message.x,
                    "y" : message.y,
                    "additional_info":{                        
                        "meta_action": message.additional_info.meta_action,
                        "meta_type" : message.additional_info.meta_type,
                        "call_sign_code" : message.additional_info.call_sign_code,
                        "bomb_index" : message.additional_info.bomb_index
                    }
                }

                // send messag not data, message has extra info not meant for the bus
                this.sendMessageToAllSocketsAndStore("undoBombPlaced",message)
                
                const undoBombPlacedMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:UIClick","0.0.0",data)
                this.mqtt_manager.publish( "ui/click", JSON.stringify(undoBombPlacedMessage));
            });
            
            // {
            //     "participant_id":this.sessionService.participant_id,
            //     "element_id": id,
            //     "parent_id":"StoreInteractionContainer",
            //     "type": "BUTTON",
            //     "x": target.clientLeft,
            //     "y": target.clientTop,
            //     "additional_info":{
            //       "meta_action":"VOTE_LEAVE_STORE",        
            //       "call_sign_code":this.sessionService.callSignCode
            //     }
            //   }
          
            socket.on('readyToLeaveStore', (message) =>{

                const data = {
                    "element_id": message.element_id,
                    "parent_id": message.parent_id,
                    "participant_id": message.participant_id,
                    "type": message.type,
                    "x" : message.x,
                    "y" : message.y,
                    "additional_info":{                        
                        "meta_action": message.additional_info.meta_action,                        
                        "call_sign_code" : message.additional_info.call_sign_code
                    }
                }
                
                const readyToLeaveStoreMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:UIClick","0.0.0",data)
                this.mqtt_manager.publish( "ui/click", JSON.stringify(readyToLeaveStoreMessage));

            });

            socket.on('publishSurveyObject', (data)=>{
                
                const surveyObjectMessage = this.mqtt_manager.generateAsistMqttMessage("simulator_event","1.2","Event:SurveyResponse","0.0.0",data)
                
                this.mqtt_manager.publish( "player/survey/response", JSON.stringify(surveyObjectMessage)); 
                
                const participant_id = data.participant_id;

                console.log(" Sent Survey Response from : " + participant_id)

                const num_players = this.player_manager.player_map.size;
                let num_published = 0;

                this.player_manager.player_map.forEach( (v,k)=>{
                    if(v.participant_id === participant_id){
                        v.surveysPublished = true;
                    }
                    if( v.surveysPublished === true ){
                        num_published++;
                    }
                })
                
                console.log( num_published +"/"+num_players+" players have finished their surveys.")
                
                if( num_players === num_published ){

                    if(trial_info_manager.team_health !== "OK" && trial_info_manager.team_health !== "CONTINUE_WITH_TEAM" ){
                        
                        console.log( "Team Dissolving and all surveys published!")
                        this.mqtt_manager.sendTrialStop(false) 

                    }                    
                }
                 
            })
            
            /**
            const clientStateObject = {
                playername : this.sessionService.assignedName,
                receivedMessages : this.sessionService.messageIdsReceivedByClient
            }
             */
            socket.on('KeepAlive', (clientStateObject) => {
                const name = clientStateObject.playerName
                const player = this.player_manager.player_map.get(name) 
                if ( player != null && player != undefined ){
                    player.lastPingMilliseconds = Date.now() 
                    player.disconnectTimestamp = -1
                    //console.log("KeepAliveClientStateObject : ")
                    //console.log(JSON.stringify(clientStateObject))                    
                    const playerState = this.stateKeeperInstance.client_app_state_holder.get(name)
                    playerState.all_received_message_ids_on_client = clientStateObject.receivedMessages 
                    //console.log( " playerState.all_received_message_ids_on_client :" + playerState.all_received_message_ids_on_client );
                    //console.log( " playerState.all_stored_message_ids_on_server :" + playerState.all_stored_message_ids_on_server );
                    
                    const diffArray = playerState.computeStateDiff();
                    if( diffArray.length > 0 && diffArray!==null && diffArray != undefined){
                        console.log("State Diff Detected. Syncing messages:" + diffArray);
                        playerState.syncMessages( diffArray, this.thisInstance, name )
                    }                                      
                }                
            });                         
        });

    }

    print_socket_map(){
        console.log( this.client_sockets )
    }
}

module.exports= {
    SocketManager
}

