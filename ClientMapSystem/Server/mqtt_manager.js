// ASIST
const mqtt = require('mqtt');
const config = require('./ClientMapConfig.json');
const noAdvisorUtilities = require('./no_advisor_planning_prompts');
const trial_info_manager = require('./trial_info_manager')
const ws = require('./websockets');
const { json } = require('express');

global_client_sockets=new Map()


class MqttManager{
    
    constructor(sm,pm,stateKeeperInstance){
        this.socket_manager = sm
        this.player_manager = pm
        this.isMqttConnected = false
        this.client = null;
        this.experiment_id = "NOT_SET";
        this.trial_id = "NOT_SET";
        this.stateKeeperInstance = stateKeeperInstance
        this.noAdvisorInstance = null
        this.mqttInit();
    }

    mqttInit(){

        if( !this.isMqttConnected){

            const connectionString = process.env.mqtt_host
                        
            // doesn't store it's own clientId for some reason

            // PROD
            this.client = mqtt.connect("mqtt://"+connectionString+":1883", { clientId:"ClientMapBackEnd" } );

            // DEV
            //const client  = mqtt.connect("mqtt://localhost:1883", { clientId:"ClientMapBackEnd" } );
            
            this.client.clientId = "ClientMapBackEnd";
            

            this.client.on("message", (topic,message)=>{
                //console.log("Topic : " + topic);
                //console.log("Message : " + message);
                this.parseTopic(topic,message);
                //console.log("object.qos : " + object.qos);
                //console.log("object.payload : " + object.payload);
            });
            
            this.client.on("connect",function(){
                
                this.isMqttConnected=true;
                console.log("MqttConnected : " + this.isMqttConnected)
            });

            this.client.on("close",function(){
                this.isMqttConnected=false;	
                console.log("MqttConnected : " + this.isMqttConnected)       
                
            });
        
            this.client.subscribe('observations/state');
            this.client.subscribe('trial');
            this.client.subscribe('control/response/getTrialInfo');
            this.client.subscribe('control/clientmap/team_budget')
            this.client.subscribe('observations/events/mission');
            this.client.subscribe("observations/events/stage_transition");
            this.client.subscribe('socket_test');
            this.client.subscribe('status/minecraft/loading');
            this.client.subscribe('status/minecraft/player_timeout');
            this.client.subscribe('status/minecraft/crash');
            this.client.subscribe('status/minecraft/stopped');  
            this.client.subscribe('item/used');   
            this.client.subscribe('environment/created/single'); 
            this.client.subscribe('environment/removed/single'); 
            this.client.subscribe('communication/environment');  
            this.client.subscribe('agent/intervention/+/chat'); 
            this.client.subscribe('player/inventory/update'); 
            this.client.subscribe('team/budget/update');
            this.client.subscribe('export/auto')
            this.client.subscribe('control/bounce/complete')   
            
        }
        else {
            console.log('Page refreshed, mqtt client still connected.');
        }

        return this.client;

    }

    publish(topic,message){

        this.client.publish(topic,message);

    }

    generateAsistMqttMessage(message_type,message_version,sub_type,sub_type_version,data){
        const d = new Date().toISOString();

        const header = this.generateStandardAsistHeader(message_type,message_version,d)
        const msg = this.generateStandardAsistMsg(sub_type,sub_type_version,d)        

        const message = {
            "header": header,
            "msg" : msg,
            "data" : data
        }

        return message;

    }

    generateStandardAsistHeader(message_type,version,timestamp){
        
        const header = {
            "timestamp": timestamp,
            "message_type": message_type,
            "version": version
        } 
        
        return header;
    }

    generateStandardAsistMsg(sub_type,version,timestamp){
        const msg = {
            "experiment_id":trial_info_manager.trial_info.experiment_id,
            "trial_id":trial_info_manager.trial_info.trial_id,
            "timestamp": timestamp,
            "source":"simulator",
            "sub_type": sub_type,
            "version":version
        } 
        
        return msg;
    }

    testSocketManager(){   
       
        this.socket_manager.sendMessageToAllSocketsAndStore("socket_test","Socket is working from MQTTManager as direct, non-broadcast.");
        
    }

    sendTrialStop( continue_with_team ){
        if( !trial_info_manager.trial_stop_in_progress){            
            trial_info_manager.trial_stop_in_progress = true;
            console.log( "----------------------SENDING TRIAL STOP!----------------------")
            console.log( "----------------------TEAM HEALTH : "+ trial_info_manager.team_health+"---------------------");
            const obj = {
                "reason":trial_info_manager.team_health,
                "continue_with_team":continue_with_team
            }
            // SEND TRIAL STOP                    
            this.publish("control/status/trigger_trial_stop",JSON.stringify(obj));           
        }
        else{
            console.log( "Received Trial Stop but one is already in progress...")
            console.log( "----------------------TEAM HEALTH : "+ trial_info_manager.team_health+"---------------------");
        }
           
    }


    parseTopic( topic, message){
        
        try{

            const trimmed = topic.trim()
            switch(true) {
                case trimmed === 'observations/state':
                    this.processPosition(message);
                    break;
                case trimmed === 'control/bounce/complete':
                    const obj = {
                        "ready":true
                    }
                    this.publish("status/clientmap/ready_for_team",JSON.stringify(obj));
                    break;
                case trimmed === 'export/auto':
                    const jsonMessage = JSON.parse(message);
                    
                    const state = jsonMessage.data.state;
                    console.log( 'Received Export message with state : ' + state);
                    console.log( "trial_info_manager.team_health = " + trial_info_manager.team_health); 
                    
                    if( state === 'error' || state === 'end' ){
                        
                        trial_info_manager.trial_stop_in_progress = false
                        trial_info_manager.store_chat_id = 0; 
                        trial_info_manager.trial_info = "NOT_SET"                        

                        if(trial_info_manager.team_health === 'OK' || trial_info_manager.team_health === 'CONTINUE_WITH_TEAM' ){
                            
                            console.log( 'Finished Waiting for Export during Team Continue - preserving instance');
                            
                            this.player_manager.player_map.forEach( (v,k)=>{
                                v.surveysPublished = false
                            })                                                        
                            trial_info_manager.team_health = 'OK';

                            // IF TEAM IS CONTINUING OR RECOVERING FROM CRASH, ASISTCONTROLA HANDLES IT So the below is not necessary
                            
                            // if( trial_info_manager.crash_recovery_in_progress === false ){

                            //      // We should only do this in NON-Crash Situations
                            //     setTimeout( ()=>{
                            //         this.player_manager.startTrial()                                                                  
                            //         //trial_info_manager.trial_running = true; 
                            //     },3000)                                
                            
                                
                            //     setTimeout( ()=>{
                            //         console.log("Requesting trial info 3 seconds after starting trial.");
                            //         this.publish("control/request/getTrialInfo","{}")
                            //     },6000)

                            // }
                            // else{
                            //     // LET ASISTCONTROL HANDLE CRASH RECOVERY BECAUSE WE NEED TO BOUNCE THE MINECRAFT SERVER TOO.
                            //     trial_info_manager.crash_recovery_in_progress = false;
                            
                            // }

                           
                             
                           
                        }
                        else{
                            console.log('Finished Waiting for Export. Team dissolution reason : ' + trial_info_manager.team_health);
                            console.log("Cleaning up trial info just before mock-bounce/vm-recycle")
                            trial_info_manager.trial_running = false
                            this.player_manager.player_map.clear(); 
                            this.socket_manager.client_sockets.clear();
                            trial_info_manager.advisor_condition = "NOT_SET"
                            trial_info_manager.experiment_created = false;
                            trial_info_manager.all_players_present = false;

                            setTimeout(()=>{
                                this.publish( "control/bounce", "{}"); 
                            },500); 
                            setTimeout(()=>{                           
                                // SEND INSTANCE DOWN
                                const downMessage = {
                                    "reason":trial_info_manager.team_health                            
                                }                            
                                const message = JSON.stringify(this.generateAsistMqttMessage("control","1.2","Control:InstanceDown","0.0.0",downMessage))                                                                                    
                                this.publish( "control/scaling/instance_down", message );
                                trial_info_manager.team_health = 'OK';
                            },500);
                        }

                    }
                    else if( state === 'begin'){
                        console.log("Auto Export Starting!.")
                    }
                    
                    break;
                case trimmed === 'socket_test':
                    this.testSocketManager()
                    break;
                case trimmed === 'status/minecraft/loading':
                    this.processMinecraftLoading(message);
                    break;                
                case trimmed === 'status/minecraft/player_timeout':
                    this.processPlayerTimeout(message);
                    break;
                case trimmed === 'status/minecraft/crash':
                    console.log(" Received Minecraft Crash Message");
                    break;
                case trimmed === 'trial':
                    this.processTrial(message);
                    break;
                case trimmed === 'control/response/getTrialInfo':
                    console.log("Received Trial Info Message :")
                    const m = JSON.parse(message);
                    trial_info_manager.trial_info = m;
                    console.log(trial_info_manager.trial_info)                    
                    this.socket_manager.sendMessageToAllSocketsAndStore("trialInfo",m);
                    break;
                case trimmed === 'control/clientmap/team_budget':
                    console.log("Received Team Budget.")
                    this.socket_manager.sendMessageToAllSocketsAndStore("teamBudget",JSON.parse(message));
                    break;
                case trimmed === 'observations/events/mission':
                    this.processMission(message);
                    break;
                case trimmed === 'observations/events/stage_transition':
                    this.processStageTransition(message);
                    break;
                case trimmed === 'item/used':
                    this.processItemUsed(message);     
                    break;
                case trimmed === 'environment/created/single':
                    this.processEnvironmentCreatedSingle(message);     
                    break;
                case trimmed === 'environment/removed/single':
                    this.processEnvironmentRemovedSingle(message);     
                    break;
                case trimmed === 'communication/environment':
                    this.processCommunicationEnvironment(message);     
                    break;
                case trimmed === 'player/inventory/update':
                    this.processPlayerInventoryUpdate(message);     
                    break;
                case trimmed === 'team/budget/update':
                    this.processTeamBudgetUpdate(message);     
                    break;     
                case (trimmed.split('/')[0] === 'agent') && (trimmed.split('/')[1] === 'intervention') && (trimmed.split('/')[3] ==='chat'):
                    this.processAgentIntervention(message); 
                    break;    
                default:
                    console.log("No Topic Matched with pattern : " + topic)
            }

        }catch(e){
            console.log(e)
        }        
    }

    processTeamBudgetUpdate(message){
        console.log("Received Team Budget Update.");        
        const jsonMessage = JSON.parse(message);
        this.socket_manager.sendMessageToAllSocketsAndStore('budgetUpdate',jsonMessage)        
    }

    processItemUsed(message){
        console.log("Received Item Used Message.");        
        const jsonMessage = JSON.parse(message);
        let item_name = jsonMessage.data.item_name;
        item_name = item_name.toLowerCase();         
        if(item_name.includes('sensor')){            
            this.socket_manager.sendMessageToAllSocketsAndStore('bombSensor',jsonMessage) 
        }
    }

    processPlayerTimeout(message){
       
        const jsonMessage = JSON.parse(message);
        const name = jsonMessage.name;
        console.log("Received Minecraft Player Timeout Message for : " + name);       
        
        const player = this.player_manager.player_map.get(name)
        if( player != null && player != undefined){

            player.confirmed_left=true
            player.continue_with_team=false
            if( trial_info_manager.team_health === "OK" || trial_info_manager.team_health === "CONTINUE_WITH_TEAM" ){
                
                trial_info_manager.team_health = "DISSOLVE_TEAM_MINECRAFT_DISCONNECT";
                const obj={
                    "reason":trial_info_manager.team_health
                }                
                this.socket_manager.sendMessageToAllSocketsAndStore("dissolve_team",obj)

                if( this.player_manager.haveAllPlayersLeft() ){
                    
                    console.log( " Yes ... Bringing down instance.") 
                                           
                    this.publish( "control/status/premature_mission_stop",JSON.parse(obj));
                    
                    setTimeout(()=>{
                        this.sendTrialStop(false)
                    },3000);           
                }
                else{
                    setTimeout(()=>{
                        console.log( "3 minute timeout after Minecraft disconnect ... sending Trial Stop.")                    
                        this.publish( "control/status/premature_mission_stop",JSON.stringify(obj));                        
                    },180000);                   
                    setTimeout(()=>{
                        this.sendTrialStop(false) 
                    },183000);
                }
            }           
        }        
    }

    processCommunicationEnvironment(message){
        console.log("Received Communication Environment Message.");        
        const jsonMessage = JSON.parse(message);
        const sender_type = jsonMessage.data.sender_type;  
        if(sender_type.includes('beacon')){            
            this.socket_manager.sendMessageToAllSocketsAndStore('beaconUpdated',jsonMessage)
        }
    }

    processPlayerInventoryUpdate(message){

        console.log("Received Player Inventory Update Message.");
        const jsonMessage = JSON.parse(message);
        console.log(jsonMessage.data.currInv);        
        this.socket_manager.sendMessageToAllSocketsAndStore('inventoryUpdate',jsonMessage)

    }

    processAgentIntervention(message){

        console.log("Received Agent Intervention Message.");
        const jsonMessage = JSON.parse(message);
        
        this.socket_manager.sendMessageToAllSocketsAndStore('agentIntervention',jsonMessage)

    }

    processEnvironmentCreatedSingle(message){
        console.log("Received Environment Created Single Message.");
        const jsonMessage = JSON.parse(message);
        const itemName = jsonMessage.data.obj.type;
        if(itemName.includes('beacon')){
            console.log("Received Beacon Placed Single Message : " + itemName);
            this.socket_manager.sendMessageToAllSocketsAndStore('beaconPlaced',jsonMessage)
        } 
    }

    processEnvironmentRemovedSingle(message){
        console.log("EnvironmentRemovedMessage!")
        const jsonMessage = JSON.parse(message);
        const itemName = jsonMessage.data.obj.type;
        if(itemName.includes('beacon')){
            this.socket_manager.sendMessageToAllSocketsAndStore('beaconRemoved',jsonMessage)
        } 
    }
      
    processStageTransition( message ){
        
        const jsonMessage = JSON.parse(message);
        this.socket_manager.sendMessageToAllSocketsAndStore('stageTransition',jsonMessage)
        console.log("Stage Transition Message : " + JSON.stringify(jsonMessage))
        console.log("Advisor Condition : "  +trial_info_manager.advisor_condition)
        if(trial_info_manager.advisor_condition === "NO_ADVISOR"){
            console.log(" NO ADVISOR CONDITION MATCHED ");
            if(jsonMessage.data.mission_stage === "SHOP_STAGE" && jsonMessage.data.transitionsToShop === 1){
                console.log(" FIRST TRANSITION TO SHOP - TRIGGERING NO ADVISOR MESSAGES.");
                this.noAdvisorInstance = new noAdvisorUtilities.NoAdvisorPromptGenerator(this)
                const pidArray = Object.values(trial_info_manager.trial_info.participant_ids)
                console.log( pidArray )
                this.noAdvisorInstance.startMessageRoutine(pidArray);
            }
        }
    }
    
    processPosition( message ){
    
        // process observation
        try {

            const jsonMessage = JSON.parse(message);
    
            const x = Math.floor(jsonMessage['data']['x']);
            const y = Math.floor(jsonMessage['data']['y']);
            const z = Math.floor(jsonMessage['data']['z']);
            const pid = jsonMessage['data']['participant_id']
            //const playername = this.player_manager.player_map.get()
    
            const mission_timer = jsonMessage['data']['mission_timer']

            const posObject = {
                "x_pos":x,
                "y_pos":y,
                "z_pos":z,
                "mission_timer":mission_timer,
                "participant_id":pid
            }
            
            //console.log(posObject)
            // send all positions to all subscribed sockets
            this.socket_manager.socket_io.emit('positionUpdate_global', posObject);
            // send only players position to all subscribed named sockets
            this.socket_manager.socket_io.emit('positionUpdate_'+pid, posObject);                
                
        } catch (error) {
            console.log(error)
        }
    }

    processMinecraftLoading( message ){
        try {
            
            const jsonMessage = JSON.parse(message);
            
            console.log( jsonMessage )

            //this.socket_manager.sendMessageToAllSocketsAndStore('minecraft_loading',jsonMessage)

            this.socket_manager.sendMessageToAllSocketsAndStore('minecraft_loading',jsonMessage)
        
        }
        catch(error) {
            console.log(error)        
        }    
    }    
    
    processTrial( message ){
        try {
            console.log( "Trial Message : ")            
            const jsonMessage = JSON.parse(message);
            console.log( jsonMessage )
            const msg = jsonMessage['msg']
            const sub_type = msg['sub_type']    
            if (sub_type === "start") {  
                console.log("Received trial start message!");                
                this.experiment_id = msg["experiment_id"];
                this.trial_id = msg["trial_id"];                
                trial_info_manager.trial_running = true;
                trial_info_manager.advisor_condition = jsonMessage['data']['intervention_agents'][0]
                this.socket_manager.sendMessageToAllSocketsAndStore('trial', jsonMessage)               
            }            
            else if (sub_type === "end" || sub_type === "stop") {
                console.log("Received trial Stop message!");                
                trial_info_manager.trial_info = "NOT_SET"
                trial_info_manager.store_chat_id = 0; 
                trial_info_manager.trial_running = false;
                if(this.noAdvisorInstance !== null){
                    this.noAdvisorInstance.reset();
                }                
                this.socket_manager.sendMessageToAllSocketsAndStore('trial', jsonMessage)
                 
            }
            
            
        }
        catch(error) {
            console.log(error)        
        }    
    }
    
    processMission(message) {
        
       
        console.log("Received MissionState Message.")   
            
        try{
            
            const jsonMessage = JSON.parse(message);
           
            if(jsonMessage.data.state_change_outcome == "MISSION_STOP_SERVER_CRASHED"){
                console.log("ServerCrash:Resetting all Trial Info Manager setting except for experiment_created.")
                trial_info_manager.trial_info = "NOT_SET"
                trial_info_manager.store_chat_id = 0;
                trial_info_manager.trial_running = false;
                trial_info_manager.team_health = 'OK';
                trial_info_manager.crash_recovery_in_progress = true;
                // MOD SENDS status/minecraft/stopped message with crash true/false to AsistControl, which then bounces the 
                // Minecraft Server, exports the broken trial, and starts a new trial.            
            }
            this.socket_manager.sendMessageToAllSocketsAndStore('missionState',jsonMessage)
    
        }
        catch(sally){

            console.log(sally)

        }       
            
    }
}

module.exports = {

    MqttManager

};




