const TrialStateEnums = 
{
    NotSet: "NotSet",
    Started: "Started",
    TrialInfoReceived: "TrialInfoReceived",
    MinecraftLoaded: "MissionLoaded",
    Recon: "Recon",
    Field: "Field",
    Shop: "Shop",                
    PostTrialSurveys:"PostTrialSurveys",
    TeamContinue:"TeamContinue",
    TeamDissolvingOk:"TeamDissolvingOk",
    TeamDissolvingError:"TeamDissolvingError",
    Export:"Export",                
    Stopped:"Stopped",
    CleanUp:"CleanUp"
}

class StoredMessage{
    constructor( message_hook_name, message, message_id){
        this.time_sent = Date.now();
        this.message_hook_name =message_hook_name;
        this.message = message;
        this.message_id = message_id;
    }
}

class PlayerState{

    constructor(playername, socket_mananger){

        this.instance_id = Date.now();        
        console.log("Created new PlayerState object for "+ playername +" with id : " + this.instance_id)
        this.playername=playername
        this.socket_manager=socket_mananger
        // index is message_id, keep a list of id sent vs id's received on the client
        // during ping check Union-Intersection of lists, send the missing ones. A refresh will send
        // the important TrialState object first + some subset of messages that correspond roughly 
        // to when the player disconnected
        this.all_stored_messages = []
        this.all_stored_message_ids_on_server = []
        this.all_received_message_ids_on_client = []
        this.trial_start_message_index = -1;
        this.trial_info_message_index = -1;
        this.minecraft_loaded_message_index = -1;
        this.mission_start_message_index = -1;
        this.last_stage_transition_message_index = -1;
        this.mission_stop_message_index = -1;
        this.dissolve_team_message_index = -1;
        this.trial_stop_message_index = -1;
    }

    computeStateDiff = function(){
        //console.log("Computing State Diff for instance id " + this.instance_id)
        return this.all_stored_message_ids_on_server
        .filter(x => !this.all_received_message_ids_on_client.includes(x))
        //.concat(this.all_received_message_ids_on_client.filter(x => !this.all_stored_message_ids_on_server.includes(x)));
    }

    syncMessages( diffArray, socket_manager, playername ){
        console.log("Restoring PlayerState partially for " + playername + "with instance id " + this.instance_id)
        for(let i = 0; i<diffArray.length; i++ ){
            setTimeout(()=>{    
                const storedMessage = this.all_stored_messages[ diffArray[i] ]
                if(storedMessage !== null && storedMessage !== undefined && storedMessage.message_hook_name !== null && storedMessage.message_hook_name !== undefined ){
                    socket_manager.client_sockets.get(playername).emit( storedMessage.message_hook_name, storedMessage.message);
                }           
                
                //console.log("Resyncing " + playername + " Message : " + diffArray[i] + " Hook Name : " + storedMessage.message_hook_name + " message : " + storedMessage.message );
            },10);
        }
        console.log("Completed Non Reload Sync for "+ playername);
    }

    retoreFullMessageStore( socket_manager, playername ){

        console.log("Restoring PlayerState fully for " + playername + "with instance id " + this.instance_id)
        for(let i = 0; i<this.all_stored_messages.length; i++ ){
            setTimeout(()=>{    
                const storedMessage = this.all_stored_messages[ i ]            
                socket_manager.client_sockets.get(playername).emit( storedMessage.message_hook_name, storedMessage.message);
                //console.log("Resyncing " + playername + " Message : " + diffArray[i] + " Hook Name : " + storedMessage.message_hook_name + " message : " + storedMessage.message );
            },10);
        }
        console.log("Completed Page Reload Sync for "+ playername);
    }   

}

class TrialState{


    constructor(){        
        this.trial_info_object = {};
        this.trial_state = TrialStateEnums.NotSet;
        this.team_health = 'OK';
        this.trial_stop_in_progress = false        
    }    

}


class StateKeeper{
    
    
    init( player_json_array, socket_manager ){ 
        
        this.instance_id = Date.now();
        
        this.client_app_state_holder = new Map();
        
        this.socket_manager = socket_manager;

        player_json_array.forEach( (p) =>{           
           // holds key value of playername to array of StoredMessages 
           this.client_app_state_holder.set(p['name'] ,new PlayerState(p['name'], this.socket_manager) )            
        })

        this.trial_state = new TrialState();

        

        console.log( " Created New StateKeeper Instance with ID " + this.instance_id );
            
    }

    clearPlayerStates(){
        for (let [k, v] of this.client_app_state_holder) {
            console.log("Clearing PlayerState for " + k + "with instance id " + v.instance_id)
            v.all_stored_messages = []
            v.all_stored_message_ids_on_server = []
            v.all_received_message_ids_on_client = []
            v.trial_start_message_index = -1;
            v.trial_info_message_index = -1;
            v.minecraft_loaded_message_index = -1;
            v.mission_start_message_index = -1;
            v.last_stage_transition_message_index = -1;
            v.mission_stop_message_index = -1;
            v.dissolve_team_message_index = -1;
            v.trial_stop_message_index = -1;
        }
              
    }

    clearPlayerState( playername){
        const state = this.client_app_state_holder.get(playername) 
        if(state != null && state != undefined){
            console.log("Clearing PlayerState for " + playername + "with instance id " + state.instance_id)
            state.all_stored_messages = []
            state.all_stored_message_ids_on_server = []
            state.all_received_message_ids_on_client = []
            state.trial_start_message_index = -1;
            state.trial_info_message_index = -1;
            state.minecraft_loaded_message_index = -1;
            state.mission_start_message_index = -1;
            state.last_stage_transition_message_index = -1;
            state.mission_stop_message_index = -1;
            state.dissolve_team_message_index = -1;
            state.trial_stop_message_index = -1;
        }
        
        
              
    }

    addPlayerMessageToStorageAndSend(message_hook_name,message,playername){

        const playerState = this.client_app_state_holder.get(playername)
        
        if(playerState != null && playerState!=undefined){
            
            const index = playerState.all_stored_messages.length
            const wrapper = {
                message_id:index,
                message:message
            }
            //console.log( "All Stored Messages Length PRE : " + index)
            playerState.all_stored_messages.push( new StoredMessage( message_hook_name,wrapper,index) )
            //console.log( "All Stored Messages Length POST : " + index)
            //console.log(JSON.stringify( playerState.all_stored_messages))
            playerState.all_stored_message_ids_on_server.push(index)            

            if(message_hook_name === 'trial' ){
                if(message.msg.sub_type === 'start'){                    
                    this.trial_start_message_index = index;                
                }
                else if(message.msg.sub_type === 'stop'){
                    this.trial_stop_message_index = index;
                }                
            }
            else if(message_hook_name === 'trialInfo' ){
                this.trial_info_message_index = index;
            }
            else if(message_hook_name === 'minecraft_loading'){
                if(message.loaded === 100){
                    this.minecraft_loaded_message_index = index;
                }                
            }
            else if(message_hook_name === 'missionState'){
                if(message.msg.sub_type === 'Start'){                    
                    this.mission_start_message_index = index;                
                }
                else if(message.msg.sub_type === 'Stop'){
                    this.mission_stop_message_index = index;
                }
            }
            else if(message_hook_name === 'stageTransition'){
                this.last_stage_transition_message_index = index;
            }
            else if(message_hook_name === 'dissolve_team'){
                this.dissolve_team_message_index = index;
            }
            

           

            console.log( JSON.stringify(wrapper) );
            
            this.socket_manager.client_sockets.get(playername).emit(message_hook_name,wrapper);
        }        
    }    
}





module.exports={
    
    StateKeeper,
    StoredMessage,
    TrialState,
    PlayerState,
    TrialStateEnums
    
}

