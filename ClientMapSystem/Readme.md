# ClientMapSystem Instructions

## About
The ClientMap service is composed of an Angular Front End UI, with NodeJS backend integration. Its purpose is to offer a "Cognitive Artifact" in which teams of participants may communicate, markup a common map for planning , and engage in various in-game activities that make up part of the experiment design. This system is hooked directly into the message bus so that all relevant actions can be recorded as part of the game data.
  

## Building Docker container
  

The container can be built in Windows by running the **build_and_dockerize.ps1** script. This script can be easily adapted to run on Linux and Mac.
  

## Configuration  

The Client Map configuration is done in the **Local/ClientMap/ClientMapConfig.json** file

		{
			# Should players be able to see their teammates in the ClientMap
			"showGlobalPositions":false,
			# automatically set by Local/export_env_vars_and_testbed_up.sh
			"admin_stack_ip":"SET_BY_ENV_FILE",
            # automatically set by Local/export_env_vars_and_testbed_up.sh
			"admin_stack_port":"SET_BY_ENV_FILE",
			# automatically set by Local/export_env_vars_and_testbed_up.sh
			"db_api_token":"SET_BY_ENV_FILE",
			# The Minecraft Map name
			"map_name":"Dragon_1.1_3D",
			# The timeout in milliseconds if a player's websocket is disconnected
			"player_timeout_ms":180000,
			# The list of callsigns to use in the experiment - these are actually set in the Local/WaitingRoom/WaitingRoomConfig.json file, not here ... they are not used in this context
			"callSigns":["Alpha","Bravo","Delta"],
			# The time between each NO_ADVISOR condition planning prompt
			"no_advisor_prompt_interval": 45000,
			# The time to wait for all players to proceed to experiment, if threshold is exceeded the game is considered abandoned and all players return to the Waiting Room
			"experiment_proceed_timeout": 180000

		}
## Debug environment

Once the ClientMap Service has been built, you may start a simple debugging environment by running `docker compose up`. In order for the ClientMap to accept your navigation to the page you must send it an https request first. 

In the Windows environment, if you intended to use a Minecraft Account with the name "MinecraftTest", this would require you to send this https request to the ClientMap backend before accessing the webpage. This is assuming you are testing on localhost.

`Invoke-WebRequest -UseBasicParsing https://localhost:9000/ClientMap/incoming_players -ContentType "application/json" -Method POST -Body '[{"name":"MinecraftTest","participant_id":"P000007","socket":"CONNECTED","callsign":"Alpha"}]'`

If you are using Linux or Mac, you may send the equivalent curl request.

On a successful return code (200), you may access the webpage @ https://localhost:9000/ClientMap/?player=MinecraftTest

Note that you will still need to mimic all relevant system hooks for the system to proceed.

This should begin with a Trial Start Message.

Once it has been sent, you may start the Minecraft Server independently.

Once the Minecraft Server finished initialiazation, send the TrialInfo Response message and continue debugging.