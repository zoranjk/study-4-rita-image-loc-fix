New-Variable -Name "date" -Visibility Public -Value $(Get-Date)
echo $date
cd .\AngularApp\ClientMap
ng build -c production
cd ..\..\
docker build -t client_map --build-arg CACHE_BREAKER=$date .
