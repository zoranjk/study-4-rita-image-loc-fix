export class FlagData {

    icon_id:string = "NOT_SET"
	meta_action = "PLANNING_FLAG_UPDATE";
	meta_type = "PLANNING_FLAG"
	content:string = "NOT_SET"	
    priority:number = 0
    schedule_minute = 0
    schedule_second = 0
    // one for each callsign
    //responses:Map<string,{text:string}> = new Map<string,{text:string}>();   

    setFlagData(iconId:string,content:string,priority:number,schedule_minute:number,schedule_second:number){

        this.icon_id = iconId;
        this.content = content;      
        this.priority = priority;
        this.schedule_minute = schedule_minute
        this.schedule_second = schedule_second

    }



}