import { MatTabGroup } from "@angular/material/tabs";
import { BeaconService } from "../Services/beacon.service";
import { IconSelectorService } from "../Services/icon-selector.service";
import { SessionService } from "../Services/session.service";
import { BombBeaconData, HazardBeaconData } from "./BeaconData"
import { IconType } from "./Enums";
import { FlagData } from "./FlagData"

export class RenderedIconWrapper {

    node:HTMLDivElement;
    x_percentage:number;
    y_percentage:number;
    owner_pid:string;
    owner_callsign:string;
    type:IconType;
    index:number;    
    
    data:BombBeaconData|HazardBeaconData|FlagData;
    responses:Map<string,string> = new Map<string,string>();
    

    constructor(
        type:IconType,
        node:HTMLDivElement,
        owner_pid:string,
        owner_callsign:string,
        x_percentage:number,
        y_percentage:number,
        public sessionService:SessionService
    )
    
    {
    
        this.owner_pid = owner_pid;    
        this.owner_callsign = owner_callsign;
        this.type = type;        
    
       
    
        //flag
        if(type == 2 ){
        
            this.type = IconType.FLAG;        
            this.index = sessionService.iconSelectorService.flagKeeperMap.get(this.owner_pid).length;
            this.data = new FlagData();
    
        }
        //beacon
        else if (type == 1){


            this.type = IconType.HAZARD_BEACON;        
            this.index = this.sessionService.beaconService.beaconKeeperMap.get(this.owner_pid).length;
            this.data = new HazardBeaconData();

        } 
        else if (type == 0){


            this.type = IconType.BOMB_BEACON;        
            this.index = this.sessionService.beaconService.beaconKeeperMap.get(this.owner_pid).length;
            this.data = new BombBeaconData();

        }       

        const mapKeys = Array.from(this.sessionService.iconSelectorService.flagKeeperMap.keys());
        mapKeys.forEach( key =>{
           this.responses.set(key,"");
        });

        this.node = node;        
        this.x_percentage = x_percentage;
        this.y_percentage = y_percentage;        
        
        node.addEventListener('click',()=>{
            this.onIconClicked();
        })
    
    }

    onIconClicked(){
        //console.log("Setting tabIndex to 1");
        //const element = document.getElementById('tabs') as HTMLElement;
        //element.getAttribute('selectedIndex');
        //element.setAttribute('selectedIndex', '1');

        console.log(this.node.id)
        const top = this.node.style.top
        const left = this.node.style.left
        //console.log(this.sessionService.iconSelectorService.currentSelectedIcon.node.id)
        if(             
            this.sessionService.iconSelectorService.currentSelectedIcon === null  
            || 
            this.node.id.localeCompare(this.sessionService.iconSelectorService.currentSelectedIcon.node.id)!=0    
        ){
            this.sessionService.iconSelectorService.setCurrentIcon(this);
            
            if(this.sessionService.tabIndex !== '1'){
                
                this.sessionService.tabIndex = '1';
               
            }  
            
            setTimeout( ()=>{
    
                if(this.node.style.top !== top || this.node.style.left !== left){
                    this.node.style.top = top;
                    this.node.style.left = left;
                }
               
            },100)

            
            
            
        }
        
       
                
    }
}