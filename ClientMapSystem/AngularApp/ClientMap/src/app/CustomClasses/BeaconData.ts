

export class BombBeaconData {

    icon_id:string = "NOT_SET"
	meta_action = "PLANNING_BEACON_UPDATE"
	meta_type = "PLANNING_BEACON"
	sequence:string = "NOT_SET"
	chained_id:string = "NOT_SET"
	bomb_id :string = "NOT_SET"
    fuse_length = " NOT_SET"
    priority:number = 0

    
    setBombBeaconData(iconId:string,bombId:string,chainedId:string,sequence:string,priority:number){

        this.icon_id = iconId;
        this.bomb_id = bombId;
        this.chained_id = chainedId;
        this.sequence = sequence;
        this.priority = priority;

    }


}

export class HazardBeaconData {

    icon_id:string = "NOT_SET"
	meta_action = "PLANNING_BEACON_UPDATE"
	meta_type = "PLANNING_BEACON"
	message:string = "NOT_SET"	
    priority:number = 0 

    
    setBombData(iconId:string,message:string,priority:number){

        this.icon_id = iconId;
        this.message = message;
        this.priority = priority;

    }


}