export enum AppMode  {

    LOADING,
    PLAY,
    EXIT

}

export enum IconType{
    BOMB_BEACON,
    HAZARD_BEACON,
    FLAG    
}

export function getIconTypeName(iconType:IconType):string{

        if(iconType == 0 ){
            return "BOMB_BEACON"
        }
        else if(iconType == 1 ){
            return "HAZARD_BEACON"
        }
        else if(iconType == 2 ){
            return "FLAG"
        }
}