import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamContinuePromptComponent } from './team-continue-prompt.component';

describe('TeamContinuePromptComponent', () => {
  let component: TeamContinuePromptComponent;
  let fixture: ComponentFixture<TeamContinuePromptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamContinuePromptComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TeamContinuePromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
