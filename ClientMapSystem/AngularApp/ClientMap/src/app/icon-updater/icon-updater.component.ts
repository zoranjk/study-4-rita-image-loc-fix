import { AfterContentInit, Component, Input} from '@angular/core';
import { BombBeaconData, HazardBeaconData } from '../CustomClasses/BeaconData';
import { getIconTypeName, IconType } from '../CustomClasses/Enums';

import { FlagData } from '../CustomClasses/FlagData';
import { RenderedIconWrapper } from '../CustomClasses/RenderedIconWrapper';
import { IconSelectorService } from '../Services/icon-selector.service';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';
import { RightClickMenuService } from '../Services/right-click-menu.service';

@Component({
  selector: 'app-icon-updater',
  templateUrl: './icon-updater.component.html',
  styleUrls: ['./icon-updater.component.scss']
})
export class IconUpdaterComponent implements AfterContentInit {

  @Input() icon:RenderedIconWrapper 
  test = "1"

  responseContent;

  minuteArray=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
  secondArray=[0,15,30,45]
  flagOptions=[
    "",
    "I will dispose bombs in this area",
    "I will search this area",
    "Let's group up here",
    "I need a Red Wirecutter",
    "I need a Green Wirecutter",
    "I need a Blue Wirecutter"

  ]
  responseOptions=[
    "",
    "I like this idea",
    "I'll meet you here to help",
    "Someone other than me should help"
  ]

  hazardBeaconResponseOptions=[
    "",
    "I’m on my way now",
    "Someone other than me should help",
    "I can handle this myself",
    "I can provide support"
  ]
  constructor( 
    public sessionService:SessionService,
    public iconSelectorService:IconSelectorService,
    public websocketService:WebsocketService,
    public rightClickMenuService:RightClickMenuService
  ) {

      this.responseContent = iconSelectorService.currentSelectedIcon.responses.get(sessionService.participant_id)
      
    }

  ngAfterContentInit(): void {

    //this.responseContent = this.icon.data.responses.get(this.sessionService.participant_id)

  }

  syncIconData(){        
    const pid = this.sessionService.participant_id;    
    let iconUpdateObject = {
      
    };
    if(this.icon.type==0){      
      const bombBeaconData = (this.icon.data as BombBeaconData);      
      iconUpdateObject = {
        "participant_id":pid,
        "element_id": this.icon.node.id,
        "parent_id":"PlanningInteractionContainer",
        "type": getIconTypeName(IconType.BOMB_BEACON),
        "x": this.icon.node.style.top,
        "y": this.icon.node.style.left,
        "additional_info":{
          "owner" : this.icon.owner_pid,
          "meta_action":"PLANNING_BEACON_UPDATE",
          "meta_type":"PLANNING_BEACON",
          "call_sign_code":this.sessionService.callSignCode,
          "index": this.icon.index,
          "x_percentage": this.icon.x_percentage,
          "y_percentage": this.icon.y_percentage,
          "bomb_id" : bombBeaconData.bomb_id,
          "chained_id" : bombBeaconData.chained_id,
          "remaining_sequence" : bombBeaconData.sequence,
          "fuse_start_minute" : bombBeaconData.fuse_length,
          "prioritization" : bombBeaconData.priority,
          "responses": {}
         }
       }
    }
    if(this.icon.type==1){      
      const hazardBeaconData = (this.icon.data as HazardBeaconData);      
      iconUpdateObject = {        
        "participant_id":pid,
        "element_id": this.icon.node.id,
        "parent_id":"PlanningInteractionContainer",
        "type": getIconTypeName(IconType.HAZARD_BEACON),
        "x": this.icon.node.style.top,
        "y": this.icon.node.style.left,
        "additional_info":{
          "owner" : this.icon.owner_pid,
          "meta_action":"PLANNING_BEACON_UPDATE",
          "meta_type":"PLANNING_BEACON",
          "call_sign_code":this.sessionService.callSignCode,
          "index": this.icon.index,
          "x_percentage": this.icon.x_percentage,
          "y_percentage": this.icon.y_percentage,
          "message" : hazardBeaconData.message,
          "prioritization" : hazardBeaconData.priority,
          "responses": {}
         }
       }
    }
    if(this.icon.type==2){      
      const flagData = (this.icon.data as FlagData);      
      iconUpdateObject = {
        "owner" : this.icon.owner_pid,
        "participant_id":pid,
        "element_id": this.icon.node.id,
        "parent_id":"PlanningInteractionContainer",
        "type": getIconTypeName(IconType.FLAG),
        "x": this.icon.node.style.top,
        "y": this.icon.node.style.left,
        "additional_info":{
          "owner" : this.icon.owner_pid,
          "meta_action":"PLANNING_FLAG_UPDATE",
          "meta_type":"PLANNING_FLAG",
          "call_sign_code":this.sessionService.callSignCode,
          "index": this.icon.index,
          "x_percentage": this.icon.x_percentage,
          "y_percentage": this.icon.y_percentage,
          "content" : flagData.content,
          "prioritization" : flagData.priority,
          "schedule_minute": flagData.schedule_minute,
          "schedule_second":flagData.schedule_second,
          "responses": {}
         }
       }
    } 

    const responseObject = {};
    
    this.icon.responses.forEach((v,k)=>{
      responseObject[k] = v
    });

    iconUpdateObject['additional_info']['responses'] = responseObject;
    
    //console.log(iconUpdateObject)
    
    this.websocketService.socket.emit("iconUpdate",iconUpdateObject);
  }

  selectIconResponse(event){
    //console.log( event )
    //console.log( event.target.selectedIndex )
    const response = event.target[event.target.selectedIndex].innerText
    this.iconSelectorService.currentSelectedIcon.responses.set(this.sessionService.participant_id,response)

    this.syncIconData();
  }

   selectPriority(event){
    this.rightClickMenuService.updateIconPriority( Number(event.value as string) );    
   }

  selectFlagMode(event){
    //console.log( event )
    //console.log( event.target.selectedIndex )
    const flagText = event.target[event.target.selectedIndex].innerText;
    (this.iconSelectorService.currentSelectedIcon.data as FlagData).content  = flagText;

    this.syncIconData();
  }

  deleteFlag(event:PointerEvent){
    if( this.rightClickMenuService.menu_open){
      alert("Please Select a Priority for the current Icon before attempting to delete it.")
      return;
    }
    else{

      const undoFlagObject:any  = this.iconSelectorService.undoFlagPlacement(event,this.sessionService.participant_id,this.sessionService.callSignCode);      
      this.websocketService.socket.emit('undoFlagPlaced',undoFlagObject);
      this.iconSelectorService.currentSelectedIcon = null;

    }
   
  }

  

}
