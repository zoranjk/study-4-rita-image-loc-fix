import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IconUpdaterComponent } from './icon-updater.component';

describe('IconUpdaterComponent', () => {
  let component: IconUpdaterComponent;
  let fixture: ComponentFixture<IconUpdaterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IconUpdaterComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(IconUpdaterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
