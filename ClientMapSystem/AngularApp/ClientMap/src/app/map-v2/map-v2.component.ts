import { AfterContentInit, AfterViewChecked, Component, ElementRef,Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';
import { environment } from 'src/environments/environment';
import { PositionEvent } from '../EventModels/event_models';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';
import { RightClickMenuService } from '../Services/right-click-menu.service';
import { RenderedIconWrapper } from '../CustomClasses/RenderedIconWrapper';
import { BeaconService } from '../Services/beacon.service';
import {BombSensorService } from '../Services/bomb-sensor.service';
import { IconSelectorService } from '../Services/icon-selector.service';
import { IconType,getIconTypeName } from '../CustomClasses/Enums';
import { MatButtonToggleChange } from '@angular/material/button-toggle';

@Component({
  selector: 'app-map-v2',
  templateUrl: './map-v2.component.html',
  styleUrls: ['./map-v2.component.scss']
})
export class MapV2Component implements OnInit,AfterContentInit,AfterViewChecked,OnChanges {

  @Input() width;
  @Input() height;
  @Input() quadrantWidth;
  @Input() quadrantHeight;
  
  @ViewChild('mapImage') mapImage:ElementRef ;

  playerElementRed: HTMLImageElement = null;
  playerElementGreen: HTMLImageElement = null;
  playerElementBlue: HTMLImageElement = null;
  playerRadius = 0;
  playerX = 0;
  playerY = 0;

  x = 0;
  z = 0;

  gridLayer:HTMLDivElement = null;
  gridDivs = [];
  renderedDivs:HTMLDivElement[] = null;

  mapWidth;
  mapHeight;

  gridWidth;
  gridHeight;

  yLabelRange = ['A','B','C','D','E','F','G','H','I','J','K']
  xLabelRange = ['1','2','3','4','5','6','7','8','9','10']

  yLabels=[]
  xLabels=[]

  labelSpacingDivs=[]

  lastPlayerPosition:PositionEvent = null;

  constructor(
    public webSocketService: WebsocketService,
    public sessionService: SessionService,
    public beaconService: BeaconService,
    public bombSensorService : BombSensorService,
    public iconSelectorService : IconSelectorService,
    public rightClickMenuService: RightClickMenuService
  ) { }

  ngOnInit(): void {
    this.gridLayer = document.getElementById('GridLayer') as HTMLDivElement;
    window.addEventListener('resize',()=>{
      this.resizeEverything()
    })
  }

  ngAfterContentInit(){
    
    this.webSocketService.positionEmitter.subscribe( (message:PositionEvent) => {

      this.updatePlayerPosition(message);
  
    });

    this.iconSelectorService.incomingFlagEmitter.subscribe((message:any)=>{

      setTimeout( ()=>{

        const x_percentage = message.additional_info.x_percentage;
        const y_percentage = message.additional_info.y_percentage;
        const flag_index = message.additional_info.flag_index;
        // this works because the gridlayer and map share a common corner at the top left of the screen
        // do not change this common corner it allows for synchronized flag placing using percentages
        const x = (this.mapImage.nativeElement as HTMLImageElement).clientWidth*(x_percentage/100)
        const y = (this.mapImage.nativeElement as HTMLImageElement).clientHeight*(y_percentage/100)
        //console.log("Incoming Flag in Map V2 :" )
        //console.log("Flag Index  : " + flag_index )
        //console.log("mapWidth = " + (this.mapImage.nativeElement as HTMLImageElement).clientWidth)
        //console.log("mapHeight = " + (this.mapImage.nativeElement as HTMLImageElement).clientHeight)
        //console.log("x_percentage : " + x_percentage )
        //console.log("y_percentage : " + y_percentage )
        //console.log("calc_x : " + x )
        //console.log("calc_y : " + y )
        const element = document.getElementById(message.element_id);
        
        if(element === null || element === undefined){
          this.setFlag(message.participant_id,message.element_id,flag_index,x,y,x_percentage,y_percentage,false)
        }

      },500)
      
      
    })

    

    // pid,itemId,itemName,x,y,z,place/remove
    this.beaconService.beaconEmitter.subscribe( (message:[string,string,string,number,number,number,boolean])=>{
      // check if placing or removing
      if(message[6]){        
        this.setBeacon(message)
      }
      else{
        // remove
        const beaconDiv = document.getElementById(message[1]);
        //console.log("Removing : " + message[1]);
        this.gridLayer.removeChild(beaconDiv);
        // We want to remove the ref to this div in BeaconKeeper, but first it needs to
        // store each beacon based on the Item Id, not in an array but in a map ... so it 
        // will be a map of maps.
      }
    } );
    // pid,itemId,itemName,x,y,z,place/remove
    this.webSocketService.bombSensorEmitter.subscribe( (message:[number,number,number,string])=>{
       
        this.setBombSensor(message);
    } );

    // setTimeout(()=>{
    //   this.beaconService.testBeaconPlacedMessage();
    // },1000)
    
  }

  ngOnChanges(changes: SimpleChanges) {
    //console.log(this.width,this.height);   
    if(this.mapImage){
      //this.grid.nativeElement.style.width = this.width +"px";
      //this.grid.nativeElement.style.height = this.height +"px";

      this.resizeEverything()
    }
  }

  ngAfterViewInit(){
    //console.log(this.width,this.height);
    //this.grid.nativeElement.style.width = this.width +"px";
    //this.grid.nativeElement.style.height = this.height +"px";

   
    
    // const map_image_element = document.getElementById("Grid") as HTMLImageElement;
     
    if(environment.production){
      //this.grid.nativeElement.src = 'https://'+document.location.host+'/ClientMap/assets/grid.png'
      this.mapImage.nativeElement.src = `${location.protocol}//${location.host}${location.pathname}assets/map_image.png`
    }
    else{
      //this.grid.nativeElement.src = 'https://'+document.location.host+'/assets/grid.png'
      this.mapImage.nativeElement.src = 'https://'+document.location.host+'/assets/map_image.png'      
    }
    
  
    for (let i = 0; i < this.quadrantHeight; i++) {
      for (let j = 0; j < this.quadrantWidth; j++){
        
        const obj = {
          'axis':'',
          'label':'',
          'id':i+","+j
        }
        if( j == this.quadrantWidth-1 ){
          obj['axis']='y';
          obj['label']=this.yLabelRange[i];
        }
        if(i == this.quadrantHeight-1){
          obj['axis']='x';
          obj['label']=this.xLabelRange[j];
          if( j == this.quadrantWidth-1 ){            
            obj['label']='';
          }
        }
        
        this.gridDivs.push(obj);

        this.resizeEverything()

      }
    }
    
    if( this.gridLayer){
      //this.gridLayer.style.setProperty('grid-template-columns', 'repeat(10,1fr)' );
      this.gridLayer.style.setProperty('grid-template-columns', 'repeat('+this.quadrantWidth+',1fr)' );
    }
    
  }

  //called after every onChanges check
  ngAfterViewChecked(){
    
  }

  
  
  rightClickMap(event:PointerEvent){
    
    event.preventDefault()

    if( this.rightClickMenuService.menu_open){
      alert("Please Select a Priority for the previously placed Icon before placing a new one.")
      return;
    }

    //console.log(event)    
    //console.log("mapWidth = " + this.mapImage.nativeElement.style.width)
    const pid = this.sessionService.participant_id;
    const callSign = this.sessionService.callSign;
    //const pid = 'P000008';
    const x = event.clientX;
    const y = event.clientY;

    const rightClickMenu = document.getElementById("rightClickMenu");
    rightClickMenu.style.top = y - ((this.mapImage.nativeElement as HTMLImageElement).clientHeight/11) +'px';
    rightClickMenu.style.left = x+'px';
    rightClickMenu.style.display = 'block';
    this.rightClickMenuService.menu_open = true;
    
    const x_percentage = 100*(x/(this.mapImage.nativeElement as HTMLImageElement).clientWidth);
    const y_percentage = 100*(y/(this.mapImage.nativeElement as HTMLImageElement).clientHeight); 
    const flagIndex = this.iconSelectorService.flagKeeperMap.get(pid).length;
    const flagWrapper = this.setFlag(pid,"flag_"+callSign+'_'+flagIndex.toString(),1,x,y,x_percentage,y_percentage,true);
    const flagPlacedObject = { 
      "participant_id":pid,
      "element_id": flagWrapper.node.id,
      "parent_id":"Map",
      "type": getIconTypeName(IconType.FLAG),
      "x": x,
      "y": y,
      "additional_info":{          
        "meta_action":"PLANNING_FLAG_PLACED",
        "meta_type":"PLANNING_FLAG",
        "call_sign_code":this.sessionService.callSignCode,
        "flag_index": flagIndex,
        "x_percentage":x_percentage,
        "y_percentage": y_percentage
      }
    }
    this.webSocketService.socket.emit("flagPlaced",flagPlacedObject);    
        
    
  }
  

  updatePlayerPosition( message:PositionEvent ): void{

    if ( message.x_pos>=0 && message.x_pos<=50 && message.z_pos>=0 && message.z_pos<=150 ) {
      
      this.lastPlayerPosition = message;      
      try {
    
        const callSignCode = this.sessionService.callSignCode;
        
        let playerElement:HTMLImageElement = null;
    
        if (callSignCode === 0){              
          if( this.playerElementRed == null ){  
            this.playerElementRed = document.getElementById('PlayerRed') as HTMLImageElement;      
            this.playerElementRed.src = `${document.location.protocol}//${document.location.host}${document.location.pathname}assets/player_red.png`;
            if ( this. playerRadius == null ){
              this.playerRadius = this.playerElementRed.clientWidth;
            }
          }
          playerElement = this.playerElementRed               
        }
        else if (callSignCode === 1){        
          if( this.playerElementGreen == null ){  
            this.playerElementGreen = document.getElementById('PlayerGreen') as HTMLImageElement;      
            this.playerElementGreen.src = `${document.location.protocol}//${document.location.host}${document.location.pathname}assets/player_green.png`;                 
            if ( this. playerRadius == null ){
              this.playerRadius = this.playerElementGreen.clientWidth;
            }          
          }  
          playerElement = this.playerElementGreen
        }
        else if (callSignCode === 2){
          if( this.playerElementBlue == null ){  
            this.playerElementBlue = document.getElementById('PlayerBlue') as HTMLImageElement;      
            this.playerElementBlue.src = `${document.location.protocol}//${document.location.host}${document.location.pathname}assets/player_blue.png`;       
            if ( this. playerRadius == null ){
              this.playerRadius = this.playerElementBlue.clientWidth;
            }
          } 
          playerElement = this.playerElementBlue     
        }
        
        
          // x: 0 - 50 (x-axis)
          // y: 0 - 150  (z-axis)

          // when x=1, z=150 dot should be at bottom left of map
          // when z 1, x = 50 dot shoudl be at top right

          // BELOW IS CUSTOMIZATION TO THE SPECIFIC GRID IMAGE WE ARE USING
          const percentXMinecraft = message.x_pos/50;
          const percentZMinecraft = message.z_pos/150;
          const percentLeftHTML = percentXMinecraft*(this.mapImage.nativeElement as HTMLImageElement).clientWidth;
          const percentTopHTML = percentZMinecraft*(this.mapImage.nativeElement as HTMLImageElement).clientHeight;
      
          playerElement.style.zIndex = '100';
          playerElement.style.position = 'relative';       
          playerElement.style.left = (percentLeftHTML - (playerElement.width/2))+'px';
          playerElement.style.top =(percentTopHTML - (playerElement.height/2)) +'px';       
        }
        
      
        
      catch(error){
        console.log("Failure parsing position message on playerPositionUpdate : ", error);    
      }
      
    }
  
  }

  resizeEverything(){    

    if(this.gridLayer){

      this.gridLayer.style.width = this.width +"px";
      this.gridLayer.style.height = this.height +"px";

    }   
    
    this.mapWidth = (this.width - (this.width/this.quadrantWidth) );
    this.mapHeight = this.height - (this.height/this.quadrantHeight);
    
    if(this.mapImage){
      this.mapImage.nativeElement.style.width = this.mapWidth +"px";
      this.mapImage.nativeElement.style.height = this.mapHeight + (this.mapHeight*.025) +"px";
    }
    
    if(this.lastPlayerPosition != null){
      this.updatePlayerPosition(this.lastPlayerPosition)
    }    

    this.adjustGridAlignment()

    this.adjustFlagIcons();

    this.adjustBeacons();

    this.adjustSensors();

  }

  adjustFlagIcons(){
    this.iconSelectorService.flagKeeperMap.forEach((v,k)=>{
      v.forEach((flag:RenderedIconWrapper)=>{ 
        
        const x = (this.mapImage.nativeElement as HTMLImageElement).clientWidth*(flag.x_percentage/100);
        const y = (this.mapImage.nativeElement as HTMLImageElement).clientHeight*(flag.y_percentage/100);
        
        // Does this adjust the rendered html node (ref) or just a copy (value)
        flag.node.style.left = x+'px';
        flag.node.style.top = y+'px';        

      });      
    });
  }

  adjustBeacons(){
    this.beaconService.beaconKeeperMap.forEach((v,k)=>{
      v.forEach((beacon:RenderedIconWrapper)=>{ 
        
        const x = (this.mapImage.nativeElement as HTMLImageElement).clientWidth*(beacon.x_percentage/100);
        const y = (this.mapImage.nativeElement as HTMLImageElement).clientHeight*(beacon.y_percentage/100);

        // Does this adjust the rendered html node (ref) or just a copy (value)
        beacon.node.style.left = (x - (beacon.node.clientWidth/2))+'px';
        beacon.node.style.top = (y - (beacon.node.clientHeight/2))+'px';       
        
        // const node = document.getElementById(beacon.node.id);
        // node.style.left = (x - (beacon.node.clientWidth/2))+'px';
        // node.style.top = (y - (beacon.node.clientHeight/2))+'px'; 

      });      
    });
  }

  adjustSensors(){
    this.bombSensorService.bombSensorMap.forEach((v,k)=>{
      
      const sensorElement = v.node;
        
      const x = (this.mapImage.nativeElement as HTMLImageElement).clientWidth*(v.x_percentage/100);
      const y = (this.mapImage.nativeElement as HTMLImageElement).clientHeight*(v.y_percentage/100);
      
      // Does this adjust the rendered html node (ref) or just a copy (value)
      sensorElement.style.left = (x - (sensorElement.clientWidth/2))+'px';
      sensorElement.style.top = (y - (sensorElement.clientHeight/2))+'px';       
      
      this.animatePulse(sensorElement,sensorElement.style.transform);
            
    });
  }

  adjustGridAlignment(){
    if((this.renderedDivs == null) || (this.renderedDivs.length == 0 )){
      this.renderedDivs = Array.from(document.getElementsByClassName('GridDivs') as HTMLCollectionOf<HTMLDivElement>);
    }
    else{
      for( let div of this.renderedDivs){
        div.style.height = (this.height/(this.quadrantHeight)) + 'px';
        //div.style.width = (this.width/(this.quadrantWidth)) + 'px';
      }   
    }
  }

  setBeacon(message:[string,string,string,number,number,number,boolean]){


    const pid = message[0];
    const id = message[1];
    const callSign = this.sessionService.getCallSignFromPid(pid);
    const itemName = message[2];
    const hazardType:boolean = itemName.toLocaleLowerCase().includes('hazard');
    const percentXMinecraft = message[3]/50;
    const percentZMinecraft = message[5]/150;

    console.log(percentXMinecraft)
    console.log(percentZMinecraft)
    
    const div = document.createElement('div') as HTMLDivElement;    
    div.style.position = 'absolute';
    div.style.cursor = 'pointer';
    div.id = id;
    
    const img = document.createElement('img') as HTMLImageElement;
    img.id = div.id+"iconImage";
    img.src = this.sessionService.getAssetPath("Beacons/"+itemName+".png");     
    img.style.width = '1.5rem';
    img.style.height = '1.5rem';
    img.style.zIndex = '12';
    img.style.position = 'relative';
    div.appendChild(img);  
    
    const node = this.gridLayer.appendChild(div);
    node.id = message[1];
    img.id = node.id+"iconImage";
    
    if(hazardType){

      const bubbleDiv = document.createElement('div') as HTMLDivElement;
      bubbleDiv.id = id+"bubbleDiv";
      let textBubbleAssetPath = this.sessionService.getAssetPath("Beacons/textBubble.png")
      bubbleDiv.style.backgroundImage ="url("+textBubbleAssetPath+")";
      
      bubbleDiv.style.position = 'relative';
     
      if(percentZMinecraft<=.10){

        textBubbleAssetPath = this.sessionService.getAssetPath("Beacons/textBubbleFlipped.png")
        bubbleDiv.style.backgroundImage ="url("+textBubbleAssetPath+")";

        bubbleDiv.style.top = '-1.5rem';
        bubbleDiv.style.left = '-2.2rem';
        
      }
      else{
        bubbleDiv.style.top = '-8.5rem';
        bubbleDiv.style.left = '-2.2rem';
      }
      
      
      // orient bubbles based on edges of map-parent      
      
      bubbleDiv.style.width = '16rem';
      bubbleDiv.style.height = '6rem';
      bubbleDiv.style.backgroundSize= '16rem 5rem';
      bubbleDiv.style.backgroundPosition= 'center';
      bubbleDiv.style.textAlign= 'center';
      bubbleDiv.style.backgroundRepeat='no-repeat';
      bubbleDiv.style.zIndex = '13';

      const bubbleText1 = document.createElement('p') as HTMLParagraphElement;
      bubbleText1.innerText = callSign;    
      bubbleText1.style.fontSize = '1.1rem';
      bubbleText1.style.position = 'relative';
      bubbleText1.style.top = '.75rem';
      if(percentZMinecraft<=.10){
        bubbleText1.style.top='1.75rem';
      }
      bubbleText1.style.fontFamily = 'system-ui';
      bubbleText1.style.fontWeight = '400';
      bubbleText1.style.color = this.sessionService.getColorforCallsign(callSign);
      bubbleText1.style.zIndex = '14';

      bubbleDiv.appendChild(bubbleText1);

      const bubbleText2 = document.createElement('p') as HTMLParagraphElement;
      bubbleText2.innerText = "...";
      bubbleText2.id = id+"bubbleText";
      bubbleText2.style.fontSize = '1.1rem';
      bubbleText2.style.position = 'relative';
      bubbleText2.style.top = '-.3rem';
      if(percentZMinecraft<=.10){
        bubbleText2.style.top='1rem';
      }
      bubbleText2.style.fontFamily = 'system-ui';
      bubbleText2.style.fontWeight = '600';
      bubbleText2.style.zIndex = '14';

      bubbleDiv.appendChild(bubbleText2);
      const bubbleDivNode = div.appendChild(bubbleDiv);

      bubbleDivNode.animate([      
          // keyframes
          
          { transform: 'scale(1)' },
          { transform: 'scale(1.5)' },
          { transform: 'scale(1)' },
          
          ], {
          // timing options
          duration: 1000,
          iterations: 1
        });
      
      
      setTimeout( ()=>{

        bubbleDivNode.style.display='none';

      },15000);
      
      node.addEventListener('mouseenter',()=>{
        this.iconSelectorService.currentHoveredOverIcon = beaconWrapper;
        bubbleDivNode.style.display='block'
      });
  
      node.addEventListener('mouseleave',()=>{
        this.iconSelectorService.currentHoveredOverIcon = null;
        bubbleDivNode.style.display='none'
      });

    }
    else{
      // BOMB TYPE
      const numWrapper = document.createElement('div') as HTMLDivElement;
      numWrapper.style.width ="1.5rem";
      numWrapper.style.height ="1.5rem";
      numWrapper.style.backgroundColor = this.sessionService.getColorforCallsign(this.sessionService.getCallSignFromPid(pid));
      numWrapper.style.borderRadius = "1rem";
      numWrapper.style.textAlign ="center";
      numWrapper.style.position = 'relative';
      numWrapper.style.left = ".4rem";
      numWrapper.style.top = "-1.75rem"; 
      numWrapper.style.zIndex = '14';   
      const p_count = document.createElement('p') as HTMLParagraphElement;      
      p_count.style.color="white";
      // NEEDS TO BE THE BOMBID WHICH IS SET WITH COMMUNICATION ENVIRONMENT
      p_count.innerText = "#-1";
      p_count.style.fontSize = ".8rem";
      /////////////////////////////////////////////////////////////////////
      p_count.style.position = 'relative';
      p_count.style.top = '.2rem';  
      p_count.id = id+"bombId";
      numWrapper.style.zIndex = '15';  
      numWrapper.appendChild(p_count);

      div.appendChild(numWrapper);
      
      node.addEventListener('mouseenter',()=>{      
        this.iconSelectorService.currentHoveredOverIcon = beaconWrapper;
      });
  
      node.addEventListener('mouseleave',()=>{
        this.iconSelectorService.currentHoveredOverIcon = null; 
      });
    
    }

   

    // BELOW IS CUSTOMIZATION TO THE SPECIFIC GRID IMAGE WE ARE USING
   
    const percentLeftHTML = percentXMinecraft*(this.mapImage.nativeElement as HTMLImageElement).clientWidth;
    const percentTopHTML = percentZMinecraft*(this.mapImage.nativeElement as HTMLImageElement).clientHeight;    
    
    
    div.style.top = (percentTopHTML-(img.clientHeight/2))+'px';
    div.style.left = (percentLeftHTML-(img.clientWidth/2))+'px';

    let beaconWrapper;
    if(itemName.includes("bomb")){
      beaconWrapper = new RenderedIconWrapper(IconType.BOMB_BEACON,node,pid,callSign,percentXMinecraft*100,percentZMinecraft*100, this.sessionService);
    }
    else if(itemName.includes("hazard")){
      beaconWrapper = new RenderedIconWrapper(IconType.HAZARD_BEACON,node,pid,callSign,percentXMinecraft*100,percentZMinecraft*100, this.sessionService);
    }    

    this.beaconService.beaconKeeperMap.get(message[0]).push(beaconWrapper);

    

    return node;
  }

  // index indicates which player to set the flag for, as this sets yours and other players flags
  setFlag( pid:string,element_id:string, priority:number, x:number,y:number,x_percentage:number,y_percentage:number,setActive:boolean):RenderedIconWrapper {
    //console.log(`setFlag in Map V2 @ ${x} , ${y}` )    
    const callSign = this.sessionService.getCallSignFromPid(pid);
    //console.log( this.iconSelectorService.flagKeeperMap );
    //console.log(this.iconSelectorService.flagKeeperMap.get(pid) );    
  
    const wrapper = document.createElement('div') as HTMLDivElement;
    wrapper.style.position = 'absolute';
    wrapper.style.height = '4rem';
    wrapper.style.zIndex = '10';
    wrapper.style.top = y+'px';
    wrapper.style.left = x+'px';
    const img = document.createElement('img') as HTMLImageElement;
    img.src = this.sessionService.getAssetPath("Planning/Icons/flag.png");  
    img.style.width = '3rem'; 
    img.style.height = '3rem';
    img.style.marginBottom = "-1rem";    
    const numWrapper = document.createElement('div') as HTMLDivElement;
    numWrapper.style.width ="1.5rem";
    numWrapper.style.height ="1.5rem";
    numWrapper.style.backgroundColor = this.sessionService.getColorforCallsign(this.sessionService.getCallSignFromPid(pid));
    numWrapper.style.borderRadius = "1rem";
    numWrapper.style.textAlign ="center";
    numWrapper.style.position = 'relative';
    numWrapper.style.left = ".4rem";
    numWrapper.style.top = "-1.75rem";    
    const p_count = document.createElement('p') as HTMLParagraphElement;      
    p_count.style.color="white";
    p_count.innerText = priority.toString();
    p_count.style.position = 'relative';
    p_count.style.top = '.2rem';    
    numWrapper.appendChild(p_count);
    
    const p_player = document.createElement('p') as HTMLParagraphElement;
    p_player.style.position='relative';
    p_player.innerText = callSign[0];
    p_player.style.font="icon";
    p_player.style.fontSize="1rem";
    p_player.style.color="white";
    p_player.style.marginBottom = "-1.25rem";
    p_player.style.top = ".1rem";
    p_player.style.left = "1.2rem";
    p_player.style.zIndex = '11';
    wrapper.appendChild(p_player);
    wrapper.appendChild(img);
    wrapper.appendChild(numWrapper);      
            
      
    const node = this.gridLayer.appendChild(wrapper);
    node.id = element_id;
    p_count.id = element_id+"_priority";
    img.id = node.id+"iconImage";
    const flagWrapper = new RenderedIconWrapper(IconType.FLAG,node,pid,callSign,x_percentage,y_percentage, this.sessionService);

    this.iconSelectorService.flagKeeperMap.get(pid).push(flagWrapper);
    //console.log(this.iconSelectorService.flagKeeperMap);
    if(setActive){
      this.iconSelectorService.setCurrentIcon(flagWrapper);
    }
    this.rightClickMenuService.currentActiveIcon = flagWrapper;
    //console.log( "RightClickMenueService.currentActiveIcon :")
    //console.log( this.rightClickMenuService.currentActiveIcon)
    
    node.addEventListener('mouseenter',()=>{      
      this.iconSelectorService.currentHoveredOverIcon = flagWrapper;
    });

    node.addEventListener('mouseleave',()=>{      
      this.iconSelectorService.currentHoveredOverIcon = null;          
    });
    return flagWrapper;
  }  

  setBombSensor(pos:[number,number,number,string]){

    const wrapper = document.createElement('div') as HTMLDivElement;
    wrapper.id = pos[3];
    wrapper.style.position = 'absolute';
    wrapper.style.zIndex = '10';
    
    const img = document.createElement('img') as HTMLImageElement;
    img.src = this.sessionService.getAssetPath("Icons/bomb_sensor_beacon.png");  
    img.style.width = '3rem'; 
    img.style.height = '3rem'; 
    
    wrapper.appendChild(img);
      
    const node = this.gridLayer.appendChild(wrapper);    
    
    const percents: [number,number] = this.placeMinecraftXYZAtHTMLTopLeft([pos[0],pos[1],pos[2]],node);

    this.animatePulse(node,node.style.transform)

    //this.bombSensorService.bombSensorMap.set(node.id, new RenderedIconWrapper(node,this.sessionService.participant_id,this.sessionService.callSign,percents[0],percents[1], this.iconSelectorService) ); 

    
  }

  animatePulse(element:HTMLElement, transformString){
    //const transformString = `translate(${toX}px,${toY}px)`;
    const animation = element.animate([      
      // keyframes
      
      { transform: transformString+'scale(1)' },
      { transform: transformString+'scale(1.5)' },
      { transform: transformString+'scale(1)' },
      
      ], {
      // timing options
      duration: 1000,
      iterations: 10
    });

    animation.addEventListener("finish", (event) => {

      this.gridLayer.removeChild(element)
      this.bombSensorService.bombSensorMap.delete(element.id)
    
    })
  
  }

  placeMinecraftXYZAtHTMLTopLeft(pos:[number,number,number], div:HTMLElement):[number,number]{
    // BELOW IS CUSTOMIZATION TO THE SPECIFIC GRID IMAGE WE ARE USING
    const percentXMinecraft = pos[0]/50;
    const percentZMinecraft = pos[2]/150;
    
    const pixelsLeftHTML = percentXMinecraft*(this.mapImage.nativeElement as HTMLImageElement).clientWidth;
    const pixelsTopHTML = percentZMinecraft*(this.mapImage.nativeElement as HTMLImageElement).clientHeight;


    div.style.left = (pixelsLeftHTML-(div.clientWidth/2))+'px';
    div.style.top = (pixelsTopHTML-(div.clientHeight/2))+'px';


    return [percentXMinecraft,percentZMinecraft];
     
  }
  
  setFlagPriority(event:MatButtonToggleChange){
    //console.log( " SET FLAG PRIORITY IN MAP V2")
    //console.log(event);

    // do the work to update this particular flag
    this.rightClickMenuService.updateIconPriority( Number(event.value as string) );
    //this.rightClickMenuService.currentActiveIcon.data.priority = Number(event.value as string)

    // dispose
    event.source.buttonToggleGroup.writeValue(undefined);
    document.getElementById("rightClickMenu").style.display='none';
    this.rightClickMenuService.menu_open = false;
  }

  unsetIconUpdateMode(){
    //console.log("Map Clicked")
    this.iconSelectorService.clearIconSelection();
  }

}
