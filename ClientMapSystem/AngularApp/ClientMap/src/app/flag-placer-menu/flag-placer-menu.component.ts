import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-flag-placer-menu',
  templateUrl: './flag-placer-menu.component.html',
  styleUrls: ['./flag-placer-menu.component.scss']
})
export class FlagPlacerMenuComponent implements OnInit {

  selectedPriority;

  @Output() priorityEmitter: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
   
  }

  selectPriority(event){
    //console.log(event);
    this.priorityEmitter.emit(event);

  }
}

