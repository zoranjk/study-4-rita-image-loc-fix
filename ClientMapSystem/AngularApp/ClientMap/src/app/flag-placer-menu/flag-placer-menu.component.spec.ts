import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlagPlacerMenuComponent } from './flag-placer-menu.component';

describe('FlagPlacerMenuComponent', () => {
  let component: FlagPlacerMenuComponent;
  let fixture: ComponentFixture<FlagPlacerMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlagPlacerMenuComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FlagPlacerMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
