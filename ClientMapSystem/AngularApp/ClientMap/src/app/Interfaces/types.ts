
export interface Intervention {
  start: string;
  end: string;
  type: string;
  id: number;
  status: string;
}

export interface Position {
  x:number;
  y:number;
  z:number;
}

export interface TextIntervention extends Intervention {
  text: string;
  receiver: string;
  time: string;

}
  
export interface HeatmapIntervention extends Intervention {
  topLeft : Position;
  bottomRight: Position;
}

export interface ClientInfo{
  playername : string;
  callsign : string;
  participant_id : string;
  unique_id:string;
}

export interface ClientMapConfig{
  showGlobalPositions?: boolean,
  admin_stack_ip?:string,
  admin_stack_port?:string,
  db_api_token?:string,
  player_timeout_ms?:number,
  callSigns?:string[]
}