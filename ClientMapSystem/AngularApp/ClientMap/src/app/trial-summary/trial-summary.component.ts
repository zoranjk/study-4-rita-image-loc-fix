import { Component } from '@angular/core';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-trial-summary',
  templateUrl: './trial-summary.component.html',
  styleUrls: ['./trial-summary.component.scss']
})
export class TrialSummaryComponent {

  //summaryFetched = false;

  constructor(
    public sessionService:SessionService, 
    public websocketService:WebsocketService,
    public dialogRef: MatDialogRef<TrialSummaryComponent>
  ){

    /*
    websocketService.trialSummaryEmitter.subscribe( (bool)=>{
      this.summaryFetched = bool
    });
    */

  }

  close(){
    this.dialogRef.close()
  }

  fetchTrialSummaryValue(...keys){

      const keysLength = keys.length;
      
      let baseObject:any = this.sessionService.trialSummary;
      console.log( baseObject )

      let broken = false;
      
      for( let i =0; i<keysLength; i++){
        console.log( keys[i] )
        if(keys[i] in baseObject){
          baseObject = baseObject[keys[i]]
          console.log( baseObject )          
          if(baseObject === null || baseObject === undefined ){
            broken = true;
            break;
          }
        }
        else{
          broken = true;
          break;
        }   
      }    
      
      if(broken){ return 0;}
      
      
      return baseObject;
       
  }

}
