import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdvisorWidgetComponent } from './advisor-widget.component';

describe('AdvisorWidgetComponent', () => {
  let component: AdvisorWidgetComponent;
  let fixture: ComponentFixture<AdvisorWidgetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdvisorWidgetComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AdvisorWidgetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
