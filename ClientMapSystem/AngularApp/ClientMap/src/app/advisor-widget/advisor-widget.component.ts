import { Component, OnInit } from '@angular/core';
import { AdvisorService } from '../Services/advisor.service';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';

@Component({
  selector: 'app-advisor-widget',
  templateUrl: './advisor-widget.component.html',
  styleUrls: ['./advisor-widget.component.scss']
})
export class AdvisorWidgetComponent implements OnInit {

  thumbsUpImagePath:string;
  thumbsDownImagePath:string;
  checkmarkImagePath:string;


  constructor( public advisorService:AdvisorService, public sessionService:SessionService, public websocketService:WebsocketService) { 
    this.thumbsUpImagePath = sessionService.getAssetPath("Icons/thumbs_up.png")
    this.thumbsDownImagePath = sessionService.getAssetPath("Icons/thumbs_down.png")
    this.checkmarkImagePath = sessionService.getAssetPath("Icons/checkmark.png")
  }

  ngOnInit(): void {
    
  }

  selectResponse(event:{source:{},value:string},interventionIndex:number){
    //console.log(event.source);
    //console.log(Number.parseInt(event.value));
    this.advisorService.receivedChats[interventionIndex].response_index = Number.parseInt(event.value)
    //console.log(this.advisorService.receivedChats)

    // set ui to show this has been answered
    //document.getElementById("checkmark"+interventionIndex).style.display="inline-block";
    //document.getElementById("radiogroup"+interventionIndex).style.display="none";


    const data = this.advisorService.receivedChats[interventionIndex];
    const agentResponseData = {
      intervention_id:data.id,
      participant_id: this.sessionService.participant_id,
      response_index: data.response_index,
      agent_id: this.advisorService.advisorId      
    }
    
    this.websocketService.socket.emit('interventionResponse', agentResponseData);
    
    setTimeout(()=>{
      this.advisorService.snackbarRef.dismiss();
    
    },500) 
  }

}
