import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mission-brief',
  templateUrl: './mission-brief.component.html',
  styleUrls: ['./mission-brief.component.scss']
})
export class MissionBriefComponent implements OnInit {

  assetPaths=[
    "missionbrief.png"
  ]

  server_url;

  constructor(
    public dialogRef: MatDialogRef<MissionBriefComponent>,
    @Inject(MAT_DIALOG_DATA) public data,
    public sessionService:SessionService,
    public websocketService:WebsocketService,
    private route: ActivatedRoute ) { }

  ngOnInit(): void {

    this.server_url = this.route.snapshot.queryParams["externalIP"];
      
    if (this.server_url == null) {
      this.server_url = window.location.hostname;
    }
  }

  close(){
    this.dialogRef.close();
  }

  setReadyToLeaveStore(event:PointerEvent){

    const target:HTMLButtonElement = event.target as HTMLButtonElement;
    const id =  target.id;

    const readyToLeaveStoreObject = {      
      
      "participant_id":this.sessionService.participant_id,
      "element_id": id,
      "parent_id":"StoreInteractionContainer",
      "type": "BUTTON",
      "x": target.clientLeft,
      "y": target.clientTop,
      "additional_info":{
        "meta_action":"VOTE_LEAVE_STORE",        
        "call_sign_code":this.sessionService.callSignCode
      }
    }

    this.websocketService.socket.emit("readyToLeaveStore",readyToLeaveStoreObject);
    target.innerText = "Waiting on Teammates ..."

    this.dialogRef.close();

  }


}
