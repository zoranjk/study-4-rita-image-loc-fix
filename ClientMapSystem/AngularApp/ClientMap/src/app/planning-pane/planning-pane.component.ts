import { AfterContentInit, Component, OnInit } from '@angular/core';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';
import { IconSelectorService } from '../Services/icon-selector.service';
import { AdvisorService } from '../Services/advisor.service';
import { BeaconService } from '../Services/beacon.service';

@Component({
  selector: 'app-planning-pane',
  templateUrl: './planning-pane.component.html',
  styleUrls: ['./planning-pane.component.scss']
})
export class PlanningPaneComponent implements OnInit, AfterContentInit {

  iconPaths = [
    "Planning/Icons/flag.png",
    "Planning/Icons/bomb.png"
  ];

  constructor(
    public sessionService:SessionService,
    public beaconService: BeaconService, 
    public websocketService:WebsocketService,
    public iconSelectorService:IconSelectorService,
    public advisorService:AdvisorService
  ) { }

  ngOnInit(): void {
    //this.iconPaths.push(this.sessionService.getAssetPath("Planning/Icons/flag.png"));
  }

  ngAfterContentInit(){

  }

  undoFlagPlacement(event:PointerEvent){
    const target:HTMLButtonElement = event.target as HTMLButtonElement;
    const id =  target.id;
    const pid =this.sessionService.participant_id;
    const flagIndex = this.iconSelectorService.flagKeeperMap.get(pid).length-1;
    const div:HTMLDivElement = document.getElementById('GridLayer') as HTMLDivElement;
    div.removeChild(this.iconSelectorService.flagKeeperMap.get(this.sessionService.participant_id).pop().node)
    //node.id = "flag_"+callSign+'_'+flagIndex.toString();
    const undoFlagObject = {
       
      "participant_id":this.sessionService.participant_id,
      "element_id": id,
      "parent_id":"Map",
      "type": "BUTTON",
      "x": target.clientLeft,
      "y": target.clientTop,
      "additional_info":{
        "meta_action":"UNDO_PLANNING_FLAG_PLACED",
        "meta_type":"WAYPOINT_FLAG", 
        "call_sign_code":this.sessionService.callSignCode,      
        "flag_index" : flagIndex
      }      
     
    }
    this.websocketService.socket.emit('undoFlagPlaced',undoFlagObject);
  }

  

  proceedToShop(event){
    this.sessionService.tabIndex = "2"
  }

  testBeacon(){
    this.beaconService.beaconEmitter.emit(['P000007','ITEMSTACK0_ITEM0','block_beacon_hazard',26,51,130,true]);
    this.beaconService.beaconEmitter.emit(['P000007','ITEMSTACK1_ITEM0','block_beacon_bomb',26,51,140,true]);

    // const itemId = message.data.sender_id;
   
    // const addInfo = message.data.additional_info;

    let message:any = {
      
        "sender_id": 'ITEMSTACK0_ITEM0',
        "additional_info":{
          "text": "I need a Green Wirecutter!"
        }
     
    }
    this.beaconService.updateBeaconFromMinecraftMessage(message);

    message = {
      
        "sender_id": 'ITEMSTACK1_ITEM0',
        "additional_info":{
          "bomb_id": "BOMB10",
          "chained_id":"BOMB7",
          "fuse_start_minute":"2",
          "remaining_sequence":"RGB"
        }
      
    }
    this.beaconService.updateBeaconFromMinecraftMessage(message);
  }

  testFlag(){
    let message:any = {
      "participant_id":"P000009",
      "additional_info":{
        "x_percentage":10,
        "y_percentage":20,
        "text": "",
        "priority":0
      }
    };
    this.iconSelectorService.incomingFlagEmitter.emit(message);

    message = {    

        "participant_id":"P000009",
        "type":"WAYPOINT_FLAG",
        "element_id": "flag_Delta_0",
        "additional_info":{
          "content":"Let's Rally Here!",
          "priority":1
        }      
     
    };

    this.iconSelectorService.processIncomingIconUpdate(message);


  }
  
 

}
