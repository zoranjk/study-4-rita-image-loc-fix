import { ComponentFixture, TestBed } from '@angular/core/testing';

import { StorePaneComponent } from './store-pane.component';

describe('StorePaneComponent', () => {
  let component: StorePaneComponent;
  let fixture: ComponentFixture<StorePaneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ StorePaneComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(StorePaneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
