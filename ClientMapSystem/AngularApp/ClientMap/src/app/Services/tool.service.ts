import { Injectable } from '@angular/core';
import { InventoryUpdate } from '../EventModels/event_models';
import { SessionService } from './session.service';

@Injectable({
  providedIn: 'root'
})
export class ToolService {

  itemAssets:[string,string, number,string,string][] = [
    ['Store/Icons/item_wirecutters_red.png',"WIRECUTTERS_RED",1,"LEFT-CLICK BOMB",
    "Use this tool on a bomb to defuse a sequence phase of the matching color."],
    ['Store/Icons/item_wirecutters_green.png',"WIRECUTTERS_GREEN",1,"LEFT-CLICK BOMB",
    "Use this tool on a bomb to defuse a sequence phase of the matching color."],
    ['Store/Icons/item_wirecutters_blue.png',"WIRECUTTERS_BLUE",1,"LEFT-CLICK BOMB",
    "Use this tool on a bomb to defuse a sequence phase of the matching color."],
    ['Store/Icons/block_beacon_bomb.png',"BOMB_BEACON",0,"RIGHT-CLICK BOMB TO PLACE,LEFT CLICK ANY BEACON TO BREAK THAT BEACON",
    "Use this tool by right clicking on a bomb. The beacon will be placed either on top of the bomb or on the ground near the bomb. You can view this beacon's information in the ClientMap Info tab by hovering over the icon that was generated when you placed the beacon."],
    ['Store/Icons/block_beacon_hazard.png',"HAZARD_BEACON",0,"RIGHT-CLICK GROUND TO PLACE,LEFT CLICK ANY BEACON TO BREAK THAT BEACON",
    "Use this tool by right clicking on the ground to place it. You will be presented with a set of options to communicate to your team. Select any option, and that option will broadcast to your team. You can view this beacon's information in the ClientMap Info tab by hovering over the icon that was generated when you placed the beacon."],
    ['Store/Icons/item_bomb_sensor.png',"BOMB_SENSOR",2,"RIGHT-CLICK WHILE HOLDING",
    "Use this tool by right clicking when it is in your hand. A red target will appear on the ClientMap for 10 seconds, showing you the bomb nearest to your location."],
    ['Store/Icons/item_fire_extinguisher.png',"FIRE_EXTINGUISHER",1,"RIGHT-CLICK FIRE",
    "Use this tool by right clicking a block that is on fire. It covers a 3 x 3 block area with each use."],
    ['Store/Icons/block_bomb_disposer.png',"BOMB_DISPOSER",10,"RIGHT-CLICK BOMB",
    "Use this tool by right clicking on a bomb. This will dispose of any bomb at any time. It may come in handy when you're in danger or in a hurry."],
    ['Store/Icons/item_bomb_ppe.png',"BOMB_PPE",3,"RIGHT-CLICK WHILE HOLDING TO EQUIP",
    "Use this tool by right clicking when it is in your hand. This will equip the armor and protect you from 1 explosion."]
  ];

 
  
  // assetPath, name, cost, usage, description
  toolInfoMap:Map<number,[string,string,number,string,string]> = new Map<number,[string,string,number,string,string]>();

  constructor( 
    public sessionService:SessionService
   ) { 

    for(let i=0; i<this.itemAssets.length; i++){
      this.toolInfoMap.set(i,
        [
          this.sessionService.getAssetPath(this.itemAssets[i][0]),
          this.itemAssets[i][1],
          this.itemAssets[i][2],
          this.itemAssets[i][3],
          this.itemAssets[i][4] 
        ]
      )
    }

  }

  processInventoryUpdate(message:any){
    
       
  }    
}

