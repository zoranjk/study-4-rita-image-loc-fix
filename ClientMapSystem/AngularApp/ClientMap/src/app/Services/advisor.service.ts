import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef } from '@angular/material/snack-bar';
import { AdvisorWidgetComponent } from '../advisor-widget/advisor-widget.component';
import { AgentIntervention, AgentInterventionData } from '../EventModels/event_models';
import { SessionService } from './session.service';
import { WebsocketService } from './websocket.service';


@Injectable({
  providedIn: 'root'
})
export class AdvisorService {

  receivedChats:AgentInterventionData[]=[]
  advisorId:string;
  snackbarRef: MatSnackBarRef<AdvisorWidgetComponent> = null;

  constructor( 
    public websocketService:WebsocketService, 
    public sessionService:SessionService,
    public snackbar:MatSnackBar) {

    // const message:AgentIntervention= {
    //   "header":{
    //     "timestamp":"",
    //     "message_type": "",
    //     "version" : ""
    //   },
    //   "msg":{
    //     "trial_id":"",
    //     "experiment_id":"",
    //     "source":"TestAgent",
    //     "sub_type":"",
    //     "version":""
    //   },
    //   "data":{
    //     "content":"Sup.",
    //     "id":"1",
    //     "receivers":[this.sessionService.participant_id],
    //     "start":-1,
    //     "response_options":["hi","what?"]
    //   }
    // }

    // this.processAgentIntervention( message );



   }

  public processAgentIntervention(message:AgentIntervention){
    this.advisorId = message.msg.source;
    this.receivedChats.push(message.data)
    if(message.data.response_options == null || (message.data.response_options as []).length == 0){
      message.data.response_options = ["Ok"]
    }
    
    const chatContainer = document.getElementById("chatContainerAdvisor") as HTMLDivElement;
    if(chatContainer != null){
      chatContainer.scrollTop = chatContainer.scrollHeight;
    } 
    
    const config:MatSnackBarConfig<AdvisorWidgetComponent> = new MatSnackBarConfig()
    config.verticalPosition = 'top';
    config.panelClass = "custom-snack";
    
    this.snackbarRef = this.snackbar.openFromComponent(AdvisorWidgetComponent,config);   
    
  }

  public testIntervention(){

    const message:AgentIntervention= {
      "header":{
        "timestamp":"",
        "message_type": "",
        "version" : ""
      },
      "msg":{
        "trial_id":"",
        "experiment_id":"",
        "source":"TestAgent",
        "sub_type":"",
        "version":""
      },
      "data":{
        "content":"Sup.",
        "id":"1",
        "receivers":[this.sessionService.participant_id],
        "start":-1,
        "response_options":[
          "You like?",
          "Why you no like?",
          "You some kinda wise guy?"
        ]
      }
    }

    this.processAgentIntervention( message );

  }

  getLatestIntervention():AgentInterventionData{

    return this.receivedChats[this.receivedChats.length-1]
  }
}
