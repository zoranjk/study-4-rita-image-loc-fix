import { EventEmitter, Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ClientMapConfig } from '../Interfaces/types';
import { BeaconService } from './beacon.service';
import { IconSelectorService } from './icon-selector.service';
import { AppMode } from '../CustomClasses/Enums'
import surveysJsonFile from './AsistStudy4SurveyObject_Post.json'
import { SurveysObject } from '../Interfaces/SurveyModels';
import { ConnectionService } from './connection.service';


@Injectable({
  providedIn: 'root'
})
export class SessionService {

  shouldSync:boolean = false;

  appMode:number = AppMode.LOADING;
  
  // THE PURPOSE OF THIS CLASS IS TO PERSIST THE APP STATE ACROSS RELOADS OR ERRORS IN THE LOCAL BROWSER SESSION OBJECT
  // ALLOWS UP TO 5MB OF DATA WITH AN EXPIRATION SETTING

  // JSON OBJECT REPRESENTING CLIENTMAP SETTINGS
  config:ClientMapConfig = { };
  admin_host:string = 'NOT_SET';
  bearerToken:string = 'NOT_SET';
  faqIframeSrc:string = 'NOT_SET';
  trialSummary
  // HOLDS ALL PLAYER INFO FOR THE CURRENT SESSION (TRIAL)
  callSignMap:Map<string,string> = new Map<string,string>();
  pidMap:Map<string,string> = new Map<string,string>();

  trialMessage = null;
  trialInfoMessage = null;
  trialInfoOnce = false;
  
  participant_id = "NOT_SET";
  callSign = "NOT_SET";
  callSignArray:string[]=[]
  colorsForCallSignsArray=['red','mediumseagreen','dodgerblue']
  callSignCode:number = -1;
  assignedName = "NOT_SET";

  client_info=[];

  receivedChats:[string,string][]=[];

  toolCounts:[number,string,number,number][][]=[];

  tabIndex:string="0";

  teamBudget = 0;

  numTransitionsToShop = 0;
  numTransitionsToField = 0;

  mission_stage = 'pre_mission';  

  surveysObject:any = {
    "participant_id":"NotSet",
    "surveys":[]
  };  

  activeSurveyIndex = 0;
  player_left_first = false;
  team_is_dissolving = false;
  post_trial_surveys_open = false;
  recoveredStoredDataEmitter:EventEmitter<any> = new EventEmitter();
  messageIdsReceivedByClient:number[] = [];

  constructor(
    public iconSelectorService:IconSelectorService,
    public beaconService:BeaconService
  ) { 

    this.surveysObject = (surveysJsonFile as unknown as SurveysObject);
    

    //TESTING ONLY
    // this.assignedName = 'Bulbousonions13';
    // this.participant_id='P000001';
    // this.iconSelectorService.flagKeeperMap.set(this.participant_id,[]);  
    // this.iconSelectorService.flagKeeperMap.set('P000007',[]);  
    // this.iconSelectorService.flagKeeperMap.set('P000009',[]);
    // this.beaconService.beaconKeeperMap.set(this.participant_id,[]);  
    // this.beaconService.beaconKeeperMap.set('P000007',[]);  
    // this.beaconService.beaconKeeperMap.set('P000009',[]);    
    // this.callSignArray=['Alpha',"Bravo","Delta"]
    // this.callSignCode=1;
    // this.callSign = this.callSignArray[this.callSignCode]
    // this.callSignMap.set('Bulbousonions13','Alpha');
    // this.callSignMap.set('Bob','Bravo');
    // this.callSignMap.set('Julie','Delta');
    // this.pidMap.set('Bulbousonions13','P000001');
    // this.pidMap.set('Bob','P000007');
    // this.pidMap.set('Julie','P000009');
    // this.receivedChats.push(["Alpha","Hi"]);
    // this.receivedChats.push(["Bravo","Hi"]);
    // this.receivedChats.push(["Delta","Hi"]);
    // this.appMode = AppMode.PLAY;
    // this.trialSummary = {
    //   "ExperimentId": "21859b4b-b210-46b4-b8b6-ef7e6ff51185",
    //   "ExperimentName": "P000007_P000001_P000009_Thu, 06 Jul 2023 17:57:15 GMT",
    //   "TrialId": "8a5762a8-75e7-4846-89fe-becb9595468b",
    //   "TrialName": "P000001_P000007_P000009_0",
    //   "MissionVariant": "MapBlocks_Orion_1.13.csv",
    //   "StartTimestamp": "2023-07-06T17:57:16.1545Z",
    //   "TeamId": "P000001_P000007_P000009",
    //   "Members": [
    //     "P000007",
    //     "P000001",
    //     "P000009"
    //   ],
    //   "ASICondition": "ASI_CMU_TA1_ATLAS",
    //   "TeamScore": 236,
    //   "BombSummaryPlayer": {
    //     "P000001": {
    //       "PHASES_DEFUSED": {
    //         "STANDARD": {
    //           "R": 4,
    //           "G": 3,
    //           "B": 0
    //         },
    //         "CHAINED": {
    //           "R": 2,
    //           "G": 2,
    //           "B": 0
    //         },
    //         "FIRE": {
    //           "R": 2,
    //           "G": 2,
    //           "B": 0
    //         }
    //       },
    //       "BOMBS_DEFUSED": {
    //         "STANDARD": 3,
    //         "CHAINED": 3,
    //         "FIRE": 5
    //       },
    //       "BOMBS_EXPLODED": {
    //         "STANDARD": 0,
    //         "CHAINED": 0,
    //         "FIRE": 0
    //       }
    //     },
    //     "P000007": {
    //       "PHASES_DEFUSED": {
    //         "STANDARD": {
    //           "R": 0,
    //           "G": 3,
    //           "B": 4
    //         },
    //         "CHAINED": {
    //           "R": 0,
    //           "G": 1,
    //           "B": 1
    //         },
    //         "FIRE": {
    //           "R": 0,
    //           "G": 0,
    //           "B": 1
    //         }
    //       },
    //       "BOMBS_DEFUSED": {
    //         "STANDARD": 5,
    //         "CHAINED": 1,
    //         "FIRE": 1
    //       },
    //       "BOMBS_EXPLODED": {
    //         "STANDARD": 0,
    //         "CHAINED": 0,
    //         "FIRE": 0
    //       }
    //     },
    //     "P000009": {
    //       "PHASES_DEFUSED": {
    //         "STANDARD": {
    //           "R": 0,
    //           "G": 0,
    //           "B": 0
    //         },
    //         "CHAINED": {
    //           "R": 0,
    //           "G": 0,
    //           "B": 0
    //         },
    //         "FIRE": {
    //           "R": 0,
    //           "G": 0,
    //           "B": 0
    //         }
    //       },
    //       "BOMBS_DEFUSED": {
    //         "STANDARD": 0,
    //         "CHAINED": 0,
    //         "FIRE": 0
    //       },
    //       "BOMBS_EXPLODED": {
    //         "STANDARD": 0,
    //         "CHAINED": 1,
    //         "FIRE": 2
    //       }
    //     }
    //   },
    //   "BombsTotal": 54,
    //   "BombsExploded": {
    //     "EXPLODE_TOOL_MISMATCH": 3,
    //     "EXPLODE_TIME_LIMIT": 0,
    //     "EXPLODE_CHAINED_ERROR": 0,
    //     "EXPLODE_FIRE": 0,
    //     "TOTAL_EXPLODED": 3
    //   },
    //   "DamageTaken": {
    //     "Team": 59.469994,
    //     "P000009": 57,
    //     "P000007": 2.4699936
    //   },
    //   "FiresExtinguished": {
    //     "Team": 105,
    //     "P000001": 37,
    //     "P000007": 35,
    //     "P000009": 33
    //   },
    //   "BombBeaconsPlaced": {
    //     "Team": 17,
    //     "P000001": 5,
    //     "P000007": 7,
    //     "P000009": 5
    //   },
    //   "CommBeaconsPlaced": {
    //     "Team": 23,
    //     "P000009": 5,
    //     "P000007": 11,
    //     "P000001": 7
    //   },
    //   "TimesFrozen": {
    //     "Team": 3,
    //     "P000009": 3
    //   },
    //   "FlagsPlaced": {
    //     "Team": 6,
    //     "P000009": 3,
    //     "P000001": 1,
    //     "P000007": 2
    //   },
    //   "TeammatesRescued": {
    //     "Team": 2,
    //     "P000007": 2
    //   },
    //   "BudgetExpended": {
    //     "Team": 176,
    //     "P000009": 71,
    //     "P000001": 56,
    //     "P000007": 49
    //   },
    //   "TotalStoreTime": 383141,
    //   "TrialEndCondition": "OK_TIMER_END"
    // }

    // setTimeout(()=>{
    //   this.beaconService.testBeaconPlacedMessage( );
    // },1000)
    
   
  }

  getAssetPath( target:string ):string{
    if(environment.production){      
      return `${document.location.protocol}//${document.location.host}${document.location.pathname}assets/${target}`;
    }
    else{      
      return 'https://'+document.location.host+'/assets/'+target;      
    }
  }


  getCallSignFromPid(pid:string):string{
    let out:string = "";
    this.pidMap.forEach((v,k)=>{
      if(v.localeCompare(pid)==0){
        out = this.callSignMap.get(k) as string;
      }
    });

    
    return out;
  }

  getCallSignCodeFromCallSign(callSign:string):number{
    let out = -1
    for(let i=0; i<this.callSignArray.length; i++){
      if(callSign.localeCompare(this.callSignArray[i])==0){
        out = i;
      }
    }
    
    return out;
  }

  syncLocalStorage(){ 
    //console.log("Syncing Local Storage");
    const storageObject = {
      "config":this.config,
      "callSignMap":[...this.callSignMap],
      "pidMap":[...this.pidMap],
      "trialMessage":this.trialMessage,
      "trialInfoMessage": this.trialInfoMessage,
      "participant_id": this.participant_id,
      "callSign": this.callSign,
      "callSignArray": this.callSignArray,
      "callSignCode": this.callSignCode,
      "receivedChats": this.receivedChats,
      "toolCounts": this.toolCounts,
      "tabIndex": this.tabIndex,
      "teamBudget": this.teamBudget,
      "numTransitionsToShop": this.numTransitionsToShop,
      "numTransitionsToField": this.numTransitionsToField,
      "missionStage": this.mission_stage,
      "appMode" : this.appMode


    }   
    // storageObject["config"]= this.config;
    // storageObject["callSignMap"],JSON.stringify(Object.fromEntries(this.callSignMap),this.replacer);
    // storageObject["pidMap"],JSON.stringify( this.pidMap, this.replacer) ;
    // storageObject["trialMessage"],JSON.stringify( this.trialMessage) ;
    // storageObject["trialInfoMessage"],JSON.stringify( this.trialInfoMessage) ;
    // storageObject["participantId"],JSON.stringify( this.participant_id) ;
    // storageObject["callSign"],JSON.stringify( this.callSign) ;
    // storageObject["callSignArray"],JSON.stringify( this.callSignArray);
    // storageObject["callSignCode"],JSON.stringify( this.callSignCode) ;       
    // storageObject["receivedChats"],JSON.stringify( this.receivedChats) ;
    // storageObject["toolCounts"],JSON.stringify( this.toolCounts) ;
    // storageObject["tabIndex"],JSON.stringify( this.tabIndex) ;
    // storageObject["teamBudget"],JSON.stringify( this.teamBudget) ;
    // storageObject["numTransitionsToShop"],JSON.stringify( this.numTransitionsToShop) ;
    // storageObject["numTransitionsToField"],JSON.stringify( this.numTransitionsToField) ;
    // storageObject["missionStage"],JSON.stringify( this.mission_stage) ;
    // storageObject["appMode"],JSON.stringify( this.appMode as number ) ;
        
    
    localStorage.setItem( this.assignedName, JSON.stringify( storageObject ) )    
    // localStorage.setItem("config",JSON.stringify( this.config) );
    // localStorage.setItem("callSignMap",JSON.stringify( this.callSignMap,this.replacer) );
    // localStorage.setItem("pidMap",JSON.stringify( this.pidMap, this.replacer) );
    // localStorage.setItem("trialMessage",JSON.stringify( this.trialMessage) );
    // localStorage.setItem("trialInfoMessage",JSON.stringify( this.trialInfoMessage) );
    // localStorage.setItem("participantId",JSON.stringify( this.participant_id) );
    // localStorage.setItem("callSign",JSON.stringify( this.callSign) );
    // localStorage.setItem("callSignArray",JSON.stringify( this.callSignArray) );
    // localStorage.setItem("callSignCode",JSON.stringify( this.callSignCode) );
    // localStorage.setItem("assignedName",JSON.stringify( this.assignedName) );    
    // localStorage.setItem("receivedChats",JSON.stringify( this.receivedChats) );
    // localStorage.setItem("toolCounts",JSON.stringify( this.toolCounts) );
    // localStorage.setItem("tabIndex",JSON.stringify( this.tabIndex) );
    // localStorage.setItem("teamBudget",JSON.stringify( this.teamBudget) );
    // localStorage.setItem("numTransitionsToShop",JSON.stringify( this.numTransitionsToShop) );
    // localStorage.setItem("numTransitionsToField",JSON.stringify( this.numTransitionsToField) );
    // localStorage.setItem("missionStage",JSON.stringify( this.mission_stage) );
    // localStorage.setItem("appMode",JSON.stringify( this.appMode as number ) );

    //console.log("PostStringify : " )
    //console.log( localStorage[this.assignedName])
  
          
  } 
  
  recoverFromLocalStorage(){ 
    
    const storageObject = JSON.parse(localStorage.getItem( this.assignedName ) as string);

    console.log(storageObject)
    console.log(storageObject["callSignMap"])
   
   
   // Maps first cus they're annoying.
    new Map<string,string>( storageObject["callSignMap"] as [string,string][] ).forEach((v,k)=>{
      this.callSignMap.set(k,v);
    })
    console.log(this.callSignMap); 
    new Map<string,string>( storageObject["pidMap"] as [string,string][] ).forEach((v,k)=>{
      this.pidMap.set(k,v);
    })
    console.log(this.pidMap);

    
    this.config = storageObject["config"] as ClientMapConfig;    
    console.log(this.config);
    this.trialMessage = storageObject["trialMessage"];
    console.log(this.trialMessage);
    this.trialInfoMessage = storageObject["trialInfoMessage"];
    console.log(this.trialInfoMessage);
    this.participant_id = storageObject["participant_id"] as string;
    console.log(this.participant_id);
    this.callSign = storageObject["callSign"] as string;
    console.log(this.callSign); 
    this.callSignArray = storageObject["callSignArray"] as string[];
    console.log(this.callSignArray)
    this.callSignCode = storageObject["callSignCode"] as number;
    console.log(this.callSignCode);  
    this.receivedChats = storageObject["receivedChats"] as [string,string][];
    console.log(this.receivedChats);
    this.toolCounts = storageObject["toolCounts"] as [number,string,number,number][][];
    console.log(this.toolCounts);
    this.tabIndex = storageObject["tabIndex"] as string;
    console.log(this.tabIndex); 
    this.teamBudget = storageObject["teamBudget"] as number;
    console.log(this.teamBudget);
    this.numTransitionsToField = storageObject["numTransitionsToField"] as number;
    console.log(this.numTransitionsToField);
    this.numTransitionsToShop = storageObject["numTransitionsToShop"] as number;
    console.log(this.numTransitionsToShop);
    this.mission_stage = storageObject["missionStage"] as string;
    console.log(this.mission_stage);
    this.appMode = storageObject["appMode"] as number;
    console.log(this.appMode);
    

    // reset icon maps
    this.pidMap.forEach( (v:string,k) =>{
      this.iconSelectorService.flagKeeperMap.set(v,[]);      
      this.beaconService.beaconKeeperMap.set(v,[]);      
    });
    console.log(this.iconSelectorService.flagKeeperMap);
    console.log(this.beaconService.beaconKeeperMap);

    console.log("Set callsign and pid : [" + this.callSignCode + "] : "+ this.callSign + " : " + this.participant_id)

    
    setTimeout( ()=>{
      this.recoveredStoredDataEmitter.emit(true);
      console.log("Drawing Tools and resetting counts");
    },1000);      
  }

  clearLocalStorage(){
    localStorage.clear();
  }

  
  resetAppState(){
    this.player_left_first = false;
    this.resetChatsAndTransitions();
    this.clearAllIcons();
    this.activeSurveyIndex = 0;    
    //this.team_is_dissolving = false;
    //this.toolCounts = []
    this.appMode = AppMode.LOADING
    
    //this.teamBudget = 0;
  }

  resetChatsAndTransitions(){
    this.numTransitionsToField = 0;
    this.numTransitionsToShop = 0;
    this.receivedChats = [] 
    this.mission_stage = 'pre_mission';  
  }

  clearAllIcons(){
    const gridLayer = document.getElementById('GridLayer') as HTMLElement;
    
    this.iconSelectorService.flagKeeperMap.forEach( (v,k)=>{
      v.forEach( (wrapper)=>{
        const node = document.getElementById(wrapper.node.id)
        if( node != null){
          console.log("Removing Flag : " + wrapper.node.id);
          gridLayer.removeChild(node)
        }        
      });          
    });   
    this.beaconService.beaconKeeperMap.forEach( (v,k)=>{
      v.forEach( (wrapper)=>{
        const node = document.getElementById(wrapper.node.id)
        if( node != null){
          console.log("Removing Beacon : " + wrapper.node.id);
          gridLayer.removeChild(node)
        }        
      });          
    });

    this.pidMap.forEach( (v:string,k) =>{
      this.iconSelectorService.flagKeeperMap.set(v,[]);          
      this.beaconService.beaconKeeperMap.set(v,[]);        
      if(k.localeCompare(this.assignedName)==0){
        this.participant_id = v;                    
      }
    });
  }

  replacer(key:any, value:any) {
    if(value instanceof Map) {
      return {
        dataType: 'Map',
        value: Array.from(value.entries()), // or with spread: value: [...value]
      };
    } else {
      return value;
    }
  }
  
  reviver(key:any, value:any) {
    if(typeof value === 'object' && value !== null) {
      if (value.dataType === 'Map') {
        return new Map(value.value);
      }
    }
    return value;
  }

  mapFromJsonObject ( obj:any ){

    const map: Map<string,string> = new Map<string,string>();
    Object.keys(obj).forEach( (key)=>{
      map.set(key, obj[key])
    })

    return map;
  }

  getColorforCallsign( callSign:string):string{
    let index = 0;
    for(let i =0; i<this.callSignArray.length; i++){
      if( this.callSignArray[i].localeCompare(callSign)==0 ){
        index = i;
      }
    }

    return this.colorsForCallSignsArray[index]
  }
   
  getColorforPid( pid:string):string{
    
    let name = null;
    
    this.pidMap.forEach( (v,k)=>{
      if(v.localeCompare(this.assignedName)==0){
        name=v;
      }
    })
    
    let callSign = null;
    callSign = this.callSignMap.get(name);
    let index = 0;
    if(callSign != null ){
      
      for(let i =0; i<this.callSignArray.length; i++){
        if( this.callSignArray[i].localeCompare(callSign)==0 ){
          index = i;
        }
      }
    }

    console.log( " Setting color for : " + callSign );

    return this.colorsForCallSignsArray[index]
  }
}

 

