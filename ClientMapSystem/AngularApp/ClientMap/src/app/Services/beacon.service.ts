import { EventEmitter, Injectable } from '@angular/core';
import { BombBeaconData, HazardBeaconData } from '../CustomClasses/BeaconData';
import { RenderedIconWrapper } from '../CustomClasses/RenderedIconWrapper';

@Injectable({
  providedIn: 'root'
})
export class BeaconService {
  
  beaconKeeperMap:Map<string,RenderedIconWrapper[]> = new Map<string,RenderedIconWrapper[]>();
  beaconEmitter:EventEmitter<any> = new EventEmitter<any>();
  constructor( 
    
  ) { 

   

    

  }

  testBeaconPlacedMessage(  ){

    console.log("Testing Beacon Placed ... ")
    
    const message = {
      "header": {
        "message_type": "simulator_event",
        "version": "1.2",
        "timestamp": "2023-06-02T15:25:16.835Z"
      },
      "msg": {
        "trial_id": "9780ad2a-6f12-4e2f-b5cd-2c163f79b9dd",
        "experiment_id": "fc1fc3d0-3a96-449d-829c-5448d1d4c4b2",
        "sub_type": "Event:EnvironmentCreatedSingle",
        "source": "MINECRAFT_SERVER",
        "version": "0.0.0"
      },  
      "data": {
        "mission_timer": "2 : 49",
        "triggering_entity": "P000007",
        "elapsed_milliseconds": 91072,
        "obj": {
          "currAttributes": {
            "bomb_id": "",
            "uses_remaining": "1"
          },
          "mission_timer": "2 : 49",
          "triggering_entity": "P000011",
          "elapsed_milliseconds": 91069,
          "x": 40,
          "y": 54,
          "z": 16,
          "id": "ITEMSTACK48_ITEM5",
          "type": "block_beacon_hazard"
        }
      }  
    }
    // Event:EnvironmentCreatedSingle Message
    const pid = message.data.triggering_entity;
    const itemId = message.data.obj.id;
    const itemName = message.data.obj.type;
    const x = message.data.obj.x;
    const y = message.data.obj.y;
    const z = message.data.obj.z;
    //const linkedMessage = message.data.currAttributes.linked_message
    const placed = true;

    console.log("Placing beacon " +itemId)
    this.beaconEmitter.emit([pid,itemId,itemName,x,y,z,placed]);

    const mess = {
      "header": {
          "message_type": "simulator_event",
          "version": "1.2",
          "timestamp": "2023-06-02T15:27:50.961Z"
        },
        "msg": {
          "trial_id": "9780ad2a-6f12-4e2f-b5cd-2c163f79b9dd",
          "experiment_id": "fc1fc3d0-3a96-449d-829c-5448d1d4c4b2",
          "sub_type": "Event:CommunicationEnvironment",
          "source": "MINECRAFT_SERVER",
          "version": "1.0.0"
        },
        "@timestamp": "2023-06-02T15:27:50.961Z",
        "data": {
          "mission_timer": "2 : 47",          
          "elapsed_milliseconds": 245196,
          "additional_info": {
            "text": "This is a Fire Bomb!"
          },
          "recipients": [
            "P000007",
            "P000008",
            "P000009"
          ],
          "sender_type": "block_beacon_hazard",
          "message_id": "COMM_ENV29",
          "message": "This is a Fire Bomb!",
          "sender_x": 20,
          "sender_y": 52,
          "sender_z": 45,
          "sender_id": "ITEMSTACK48_ITEM5"
        }  
      }

    this.updateBeaconFromMinecraftMessage(mess);

  }

  processBeaconPlacedMessage( message ){
    // Event:EnvironmentCreatedSingle Message
    const pid = message.data.triggering_entity;
    const itemId = message.data.obj.id;
    const itemName = message.data.obj.type;
    const x = message.data.obj.x;
    const y = message.data.obj.y;
    const z = message.data.obj.z;
    //const linkedMessage = message.data.currAttributes.linked_message
    const placed = true;
    this.beaconEmitter.emit([pid,itemId,itemName,x,y,z,placed]);

  }
  processBeaconRemovedMessage( message ){
    // Event:EnvironmentRemovedSingle Message
    const pid = message.data.triggering_entity;
    const itemId = message.data.obj.id;
    const itemName = message.data.obj.type;
    const x = message.data.obj.x;
    const y = message.data.obj.y;
    const z = message.data.obj.z;
    const placed = false;
    this.beaconEmitter.emit([pid,itemId,itemName,x,y,z,placed]);

  }

  updateBeaconFromMinecraftMessage(message){
    
    setTimeout( ()=>{

      const itemId = message.data.sender_id;     
      let beaconObject:RenderedIconWrapper;

      const arrayOfWrapperArrays: Array<RenderedIconWrapper[]> = Array.from(this.beaconKeeperMap.values());
      for(let i = 0; i< arrayOfWrapperArrays.length; i++){
        let shouldBreak = false;
        if( !shouldBreak){
          const wrapperArray: RenderedIconWrapper[] = arrayOfWrapperArrays [i];
          for(let j = 0; j< wrapperArray.length; j++){
            //console.log(itemId);
            //console.log(wrapperArray[j].node.id);
            if( itemId.localeCompare(wrapperArray[j].node.id) == 0 ){  
              //console.log("BEACON MATCHED!"); 
              const addInfo = message.data.additional_info;          
              beaconObject = (wrapperArray[j]);       
              if( (message.data.sender_type as string).localeCompare("block_beacon_bomb")==0 ){             
                (beaconObject.data as BombBeaconData).bomb_id = addInfo.bomb_id;
                (beaconObject.data as BombBeaconData).sequence = addInfo.remaining_sequence;
                (beaconObject.data as BombBeaconData).chained_id = addInfo.chained_id;
                (beaconObject.data as BombBeaconData).fuse_length = addInfo.fuse_start_minute;
                const bombIdPar = document.getElementById(itemId+"bombId");
                const bombIdString = addInfo.bomb_id as string;
                const bombId = bombIdString.slice(4,bombIdString.length)
                bombIdPar.innerHTML = '#'+bombId;    
              }
              else{              
                (beaconObject.data as HazardBeaconData).message = addInfo.text;
                const bubbleText = document.getElementById(itemId+"bubbleText");
                bubbleText.innerHTML = addInfo.text;
              }
              //console.log(beaconObject)
              shouldBreak = true;
              break;
            }
          }      
        }
        else{
          break;
        }
      } 
    },500);    
  }
}
