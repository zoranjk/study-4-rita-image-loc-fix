import { ThisReceiver } from '@angular/compiler';
import { AfterViewChecked, AfterViewInit, Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-map-labels',
  templateUrl: './map-labels.component.html',
  styleUrls: ['./map-labels.component.scss']
})
export class MapLabelsComponent implements OnInit, AfterViewInit, AfterViewChecked, OnChanges {

  @Input() mapWidth:number;
  @Input() mapHeight:number;
  @Input() quadrantWidth:number;
  @Input() quadrantHeight:number;

  gridDivs:HTMLDivElement[] = null;

  yLabelRange = ['A','B','C','D','E','F','G','H','I','J','K']
  xLabelRange = ['1','2','3','4','5','6','7','8','9','10']

  yLabels=[]
  xLabels=[]

  labelSpacingDivs=[]

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    this.adjustGridAlignment() 
  }

  ngOnInit(): void {

    window.addEventListener('resize',()=>{
      this.adjustGridAlignment(); 
    })
    
    this.yLabels = this.yLabelRange.slice(0,this.quadrantHeight)
    this.xLabels = this.xLabelRange.slice(0,this.quadrantWidth) 
    
    for(let i = 0; i<this.quadrantHeight+1; i++){
      for(let j = 0; j< this.quadrantWidth+1; j++){
        const obj = {
          'axis':'',
          'label':'',
          'id':i+","+j
        }
        if( j == this.quadrantWidth ){
          obj['axis']='y';
          obj['label']=this.yLabelRange[i];
        }
        if(i == this.quadrantHeight){
          obj['axis']='x';
          obj['label']=this.xLabelRange[j];
          if( j == this.quadrantWidth ){            
            obj['label']='';
          }
        }
        
        this.labelSpacingDivs.push(obj)       
        
      }
    }

    const labelGrid = document.getElementById('labelGrid');
    labelGrid.style.setProperty('grid-template-columns', 'repeat('+(this.quadrantWidth+1)+',1fr)' );
    

  }

  ngAfterViewInit(){
    this.adjustGridAlignment()  
  }

  ngAfterViewChecked(): void {
    this.adjustGridAlignment()
  }

  adjustGridAlignment(){
    if((this.gridDivs == null) || (this.gridDivs.length == 0 )){
      this.gridDivs = Array.from(document.getElementsByClassName('GridDivs') as HTMLCollectionOf<HTMLDivElement>);
    }
    else{
      for( let div of this.gridDivs){
        div.style.height = ((this.mapHeight/this.quadrantHeight)*.9625) + 'px';
        div.style.width = ((this.mapWidth/this.quadrantWidth)*.9325) + 'px';
      }   
    }
  }

}
