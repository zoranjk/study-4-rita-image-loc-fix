import { Component} from '@angular/core';
import { MatDialog} from '@angular/material/dialog';
import { WebsocketService } from '../Services/websocket.service';


import { SessionService } from '../Services/session.service';
import { IconSelectorService } from '../Services/icon-selector.service';




@Component({
  selector: 'app-interaction-pane',
  templateUrl: './interaction-pane.component.html',
  styleUrls: ['./interaction-pane.component.scss']
})
export class InteractionPaneComponent{


 
  constructor( 
    public sessionService:SessionService, 
    public websocketService: WebsocketService, 
    public iconSelectorService: IconSelectorService, 
    public dialog: MatDialog) { 

  }


  emitDebugMode(){

    //this.websocketService.debugMode = this.debug;

    //console.log("Debug Mode : " + this.websocketService.debugMode);

  }
  

  isBombBeaconDataType():boolean{

    if ('bomb_id' in this.iconSelectorService.currentHoveredOverIcon){
      return true;
    }
    return false;
  }
  
}

