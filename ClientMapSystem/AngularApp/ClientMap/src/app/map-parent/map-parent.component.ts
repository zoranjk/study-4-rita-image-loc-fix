import { AfterViewInit, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';
import { HttpClient } from '@angular/common/http';
import { ConnectionService } from '../Services/connection.service';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { TeamDissolvePromptComponent } from '../team-dissolve-prompt/team-dissolve-prompt.component';

@Component({
  selector: 'app-map-parent',
  templateUrl: './map-parent.component.html',
  styleUrls: ['./map-parent.component.scss']
})
export class MapParentComponent implements AfterViewInit {

  encapsulation: ViewEncapsulation.None  



  constructor( 
    public websocketService:WebsocketService,
    public connectionService:ConnectionService,
    public sessionService:SessionService,
    public dialog: MatDialog,
    public http:HttpClient) { }

  ngOnInit(): void {

  }

  

  ngAfterViewInit(){    
    this.websocketService.socketInit();
  }

  tabFocusChange(event){
    setTimeout( ()=>{

      console.log(event)
      const chatDivs = document.getElementsByClassName('chatbox')
      for( let i =0; i< chatDivs.length; i++){
        let chatDiv = chatDivs.item(i)
        chatDiv.scrollTo( 0,chatDiv.scrollHeight )
      } 
    },300)
    
  }

  leaveGame(){

    this.sessionService.player_left_first = true;    

    const dialogConfig = new MatDialogConfig();
            dialogConfig.autoFocus = true;
            dialogConfig.maxWidth='90vw';
            dialogConfig.maxHeight='90vh';
            dialogConfig.id = "TeamDissolveDialog";
            dialogConfig.disableClose = true;
            dialogConfig.data = {
              "reason":"DISSOLVE_TEAM_PLAYER_LEFT"
            };
            const dialogRef = this.dialog.open(TeamDissolvePromptComponent, dialogConfig )   

  }

  scrollChatDown(){
    setTimeout(()=>{
      const chatContainer = document.getElementById("chatContainer") as HTMLDivElement;
      if(chatContainer != null){
        chatContainer.scrollTop = chatContainer.scrollHeight;
      }
    },1000)
  }

}
