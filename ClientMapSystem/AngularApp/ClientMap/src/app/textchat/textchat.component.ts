import { Component, OnInit } from '@angular/core';
import { SessionService } from '../Services/session.service';
import { WebsocketService } from '../Services/websocket.service';

@Component({
  selector: 'app-textchat',
  templateUrl: './textchat.component.html',
  styleUrls: ['./textchat.component.scss']
})
export class TextchatComponent implements OnInit {

  

  constructor( 
    public sessionService:SessionService, 
    public websocketService:WebsocketService) { }

  ngOnInit(): void {
  }

  onInputEnter(event:KeyboardEvent){
    //console.log(event);
    const callsign = this.sessionService.callSign;
    const target:HTMLInputElement = event.target as HTMLInputElement;
    const content = target.value;
    const record = [callsign, content];
    console.log(content);
    target.value = "";
    

    
    const chatObject = {
      "record":record,
      "callSign":this.sessionService.callSign,
      "participant_id":this.sessionService.participant_id
    }
    this.websocketService.socket.emit("storeChat",chatObject);

  }

  setStyle(callSign:string): any {
    
    return {
      'margin-left':'1rem', 
      'color': this.sessionService.getColorforCallsign(callSign),
      'font-size':'1.15rem' 
    };
}

}
